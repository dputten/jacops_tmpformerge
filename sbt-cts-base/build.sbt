
import sbt._

//import CscBasePlugin.autoImport._
//import autoImport._
//import csc.CscBuild._

name := "sbt-csc-base"

organization := "com.csc.sbt"

sbtPlugin := true

resolvers := NexusHelper.getResolvers("cts_sensor")

externalResolvers <<= resolvers map { rs => Resolver.withDefaultResolvers(rs, mavenCentral = NexusHelper.useMavenCentral) }

offline := NexusHelper.isOffline

//resolvers := Seq("CSC Repo" at "http://cscappamd911.nl.emea.csc.com:8082/nexus/content/groups/group_cts_sensor/")

//externalResolvers <<= resolvers map { rs => Resolver.withDefaultResolvers(rs, mavenCentral = false) }

//addSbtPlugin("org.scalariform" % "sbt-scalariform" % "1.6.0")

//addSbtPlugin("com.typesafe.sbt" % "sbt-multi-jvm" % "0.3.11")

addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "0.8.0") //this is the highest version supported by java 6

lazy val cscSbt = Project(
  id = "sbt-csc-base",
  base = file(".")
)
