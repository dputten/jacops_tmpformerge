
import sbt._

//TODO: use NexusHelper from src/main/scala plugin, and not have this copy
object NexusHelper {
  lazy val useNexus: Boolean = System.getProperty("nexus") != null

  def isOffline: Boolean = !useNexus

  def useMavenCentral: Boolean = !useNexus

  def getResolvers(cscGroup: String): Seq[Resolver] = useNexus match {
    case true => Seq("CSC Repo" at s"http://cscappamd911.nl.emea.csc.com:8082/nexus/content/groups/$cscGroup/")
    case false => Nil
  }
}
