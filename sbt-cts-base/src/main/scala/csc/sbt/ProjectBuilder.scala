package csc.sbt

import sbt.Keys._
import sbt._


/**
 * Created by carlos on 01/11/16.
 */

class ProjectBuilder(defaultSettings: Seq[sbt.Def.Setting[_]]) {

  def proj(name: String, deps: Seq[ModuleID]): Project =
    createProject(name, deps, Nil)

  def distProj(projName: String,
               deps: Seq[ModuleID],
               distSettings: DistSettings,
               pkgMappingSettings: Seq[sbt.Setting[_]] = Nil): Project = {

    val extraSettings: Seq[sbt.Setting[_]] = Dist.distSettingsFor(distSettings) ++ commonPackageMappings ++ pkgMappingSettings

    createProject(projName, deps, extraSettings)
  }

  def simpleDistProj(projName: String,
                     deps: Seq[ModuleID],
                     pkgBaseDir: String,
                     pkgMappingSettings: Seq[sbt.Setting[_]] = Nil): Project = {

    val extraSettings: Seq[sbt.Setting[_]] =
      Dist.simpleDistSettings(pkgBaseDir) ++ commonPackageMappings ++ pkgMappingSettings

    createProject(projName, deps, extraSettings)
  }

  def createProject(name: String,
                    deps: Seq[ModuleID],
                    extraSettings: Seq[sbt.Setting[_]] = Nil) =
    Project(
      id = name,
      base = file(name),
      settings = defaultSettings ++ Seq(
        libraryDependencies ++= deps
      ) ++ extraSettings ++ Defaults.projectTasks
    ).configs(IntegrationTest).settings(Defaults.itSettings: _*)

  def commonPackageMappings: Seq[sbt.Setting[_]] = Nil
}
