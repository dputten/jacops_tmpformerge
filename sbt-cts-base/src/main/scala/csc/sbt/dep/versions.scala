
package csc.sbt.dep

import sbt._

trait CommonVersions extends CDH5Versions {

  def project: String = "jacops-1.0.0.2" //project version

  val scala = "2.11.8"

  val Camel = "2.10.0"
  val Logback = "1.0.0"
  val Netty = "3.2.7.Final"
  val Scalatest = "3.0.0"
  val Slf4j = "1.6.4"
  //val cscConfig = "2.0.1-SNAPSHOT"
  val cscImageFunc = "2.4.1-JACOPS"
  val Akka = "2.3.15"
  val Curator = "2.6.0"
  val JaiImage = "1.1.3"
  val commonsIo = "2.4"
//  val commonsIo = "2.0.1"
  val Jassh = "0.9.19"

}

trait CDH5Versions {

  val HBase     = "0.98.1-cdh5.1.2"
  val Hadoop    = "2.3.0-cdh5.1.2"
  //val Zookeeper = "3.4.5"
  val Zookeeper = "3.4.5-cdh5.1.2"  // Zookeeper version included from CDH4 implicitly

}

trait VersionContext {

  def v: CommonVersions

}

trait CommonsDependency extends VersionContext {

  val commonsVersion = v.project

  val cscDConfig    = "com.csc.traffic"           % "dconfig"               % commonsVersion // exclude ("org.apache.hadoop.zookeeper", "zookeeper")  //exclude ("com.google.guava", "guava")  // CSC
  val cscDLog       = "com.csc.traffic"           % "dlog"                  % commonsVersion // exclude ("org.apache.hadoop.zookeeper", "zookeeper")  //exclude ("com.google.guava", "guava")   // CSC
  val cscDLogActor  = "com.csc.traffic"           % "dlog-actor"            % commonsVersion // exclude ("org.apache.hadoop.zookeeper", "zookeeper")  //exclude ("com.google.guava", "guava")   // CSC
  val cscCurator = "com.csc.traffic" % "curator-utils" % commonsVersion exclude ("com.csc.traffic", "curator-test-utils")
  val cscTestCurator = "com.csc.traffic" % "curator-test-utils" % commonsVersion % "it,test" // exclude ("com.csc.traffic", "test-util")
  val cscConfig = cscDConfig

  val akkaUtil =    "com.csc.traffic"             % "akka-util"             % commonsVersion
  val amqpUtil =    "com.csc.traffic"             % "amqp-util"             % commonsVersion exclude ("com.csc.traffic", "test-util")
  val testUtil =    "com.csc.traffic"             % "test-util"             % commonsVersion % "it,test"

}


trait Dependency extends VersionContext with UnclassifiedDependency {

  // Compile
  val log4j = "log4j" % "log4j" % "1.2.16" // ApacheV2
  val netty = "org.jboss.netty" % "netty" % v.Netty // ApacheV2
  val slf4jApi = "org.slf4j" % "slf4j-api" % v.Slf4j // MIT
  //todo remove this and make sure that an older version isn't included in classpath  looks like akka-camel introduces this
  val slf4jLog4j = "org.slf4j" % "slf4j-log4j12" % v.Slf4j // MIT
  val cscImageFunc = "com.csc.traffic" % "imagefunc" % v.cscImageFunc
  val akkaActor = "com.typesafe.akka" %% "akka-actor" % v.Akka //ApacheV2
  val akkaCamel = "com.typesafe.akka" %% "akka-camel" % v.Akka //ApacheV2
  val akkaKernel = "com.typesafe.akka" %% "akka-kernel" % v.Akka % "compile"
  val akkaSlf4j = akkaSlf4jUnclassified % "compile"
  val akkaAgent = "com.typesafe.akka" %% "akka-agent" % v.Akka % "compile"
  val akkaRemote = "com.typesafe.akka" %% "akka-remote" % v.Akka //ApacheV2
  val amqpClient = "com.github.sstone" %% "amqp-client" % "1.5"
  val rabbitMqClient = "com.rabbitmq" % "amqp-client" % "3.5.2" //% "runtime"

  val logback = "ch.qos.logback" % "logback-classic" % v.Logback % "compile"
  val jna = "net.java.dev.jna" % "jna" % "3.2.5" % "compile"
  val ftpserver = "org.apache.ftpserver" % "ftplet-api" % "1.0.4" // % "compile"

  val comonDbcp = "commons-dbcp" % "commons-dbcp" % "1.4" % "compile"
  val comonCompress = "org.apache.commons" % "commons-compress" % "1.1" % "compile"
  val commonsCodec = "commons-codec" % "commons-codec" % "1.4" // ApacheV2
  val commonsLang = "commons-lang" % "commons-lang" % "2.6"
  val commonsIO = "commons-io" % "commons-io" % v.commonsIo
  val commonImage = "org.apache.commons" % "commons-imaging" % "1.0.1-CSC"
  val httpClient = "org.apache.httpcomponents" % "httpclient" % "4.2.5"

  val liftJson = "net.liftweb" %% "lift-json" % "2.6.3" % "compile"
  val liftjson = liftJson //handy alias
  val arm = "com.jsuereth" %% "scala-arm" % "1.4"
  val curatorTest = "org.apache.curator" % "curator-test" % v.Curator // ApacheV2
  val curator = "org.apache.curator" % "curator-recipes" % v.Curator exclude ("org.apache.curator", "curator") // ApacheV2
  val scalatest = "org.scalatest" %% "scalatest" % v.Scalatest

  val JaiImage = "com.sun.media" % "jai-codec" % v.JaiImage
  val JaiCore = "javax.media" % "jai_core" % v.JaiImage
  val JaiLib = "javax.media.jai" % "mlibwrapper-jai" % v.JaiImage
  val exifExtractor = "com.drewnoakes" % "metadata-extractor" % "2.7.0-CSC"

  val hbasewd = "com.sematext.hbasewd" % "hbasewd_2.9.1" % "0.2.0-CSC.1"
  val jassh = "fr.janalyse" %% "janalyse-ssh" % v.Jassh
  val jcraft = "com.jcraft" % "jsch" % "0.1.49"
  val commonsNet = "commons-net" % "commons-net" % "3.1"
  val Sanselan = "org.apache.sanselan" % "sanselan" % "0.97-incubator"

  val activeMQ      = "org.apache.activemq"         % "activemq-core"          % "5.4.1"      // ApacheV2

  val jodaTime      = "joda-time"                   % "joda-time"               % "2.2"
  val jodaConv      = "org.joda"                    % "joda-convert"            % "1.3.1"

  val activeMQ_camel= "org.apache.activemq"         % "activemq-camel"         % "5.4.1"      // ApacheV2
  //val dacolian      = "com.csc.traffic"             % "imagefunc"              % "2.3.0"      // CSC
  val squeryl       = "org.squeryl"                 %% "squeryl"                % "0.9.5-7"
  val mysqlDriver   = "mysql"                       % "mysql-connector-java"    % "5.1.10"
  //val camelMina     = "org.apache.camel"            % "camel-mina"             % v.Camel      // ApacheV2
  //val mina          = "org.apache.mina"             % "mina-core"              % "1.1.7"      // ApacheV2
  val javaMail      = "javax.mail"                  % "mail"                   % "1.4"        // ApacheV2
  val javaxTrans    = "javax.transaction"           % "jta"                    % "1.0.1B"      % "provided->default"
  val scalaarm      = "com.jsuereth"                %% "scala-arm"             % "1.4"        // ApacheV2
  val stream        = "org.apache.camel"            % "camel-stream"           % v.Camel       // ApacheV2
  val scala_csv     = "com.github.tototoshi"        %% "scala-csv"              % "1.0.0"
  val zip4j         = "net.lingala.zip4j"           % "zip4j"                   % "1.3.1"

  val commonsLogging= "commons-logging"             % "commons-logging-api"    % "1.1"        // ApacheV2


}

trait RuntimeDependency extends VersionContext {

  val camelCore = "org.apache.camel" % "camel-core" % v.Camel // ApacheV2
  val camelFtp = "org.apache.camel" % "camel-ftp" % v.Camel % "runtime"
  val camelMina = "org.apache.camel" % "camel-mina" % v.Camel % "runtime"
  val camelJetty = "org.apache.camel" % "camel-jetty" % v.Camel % "runtime"
  val logback = "ch.qos.logback" % "logback-classic" % v.Logback % "runtime" // MIT
  val mina = "org.apache.mina" % "mina-core" % "1.1.7" % "runtime"

}

trait TestDependency extends VersionContext with UnclassifiedDependency {

  val junit = junitUnclassified % "it,test" // Common Public License 1.0
  val junitC = junitUnclassified
  val logback = "ch.qos.logback" % "logback-classic" % v.Logback % "it,test" // EPL 1.0 / LGPL 2.1
  val scalatest = scalatestUnclassified % "it,test" // ApacheV2
  val scalatestC = scalatestUnclassified
  val akkaTestKit = akkaTestKitUnclassified % "it,test" // ApacheV2
  val akkaCamel = "com.typesafe.akka" %% "akka-camel" % v.Akka % "it,test"
  val camelMina = "org.apache.camel" % "camel-mina" % v.Camel % "it,test"
  val transact = "javax.transaction" % "jta" % "1.1" % "it,test->default"
  val transactC = "javax.transaction" % "jta" % "1.1" % "compile"
  val persistence = "javax.persistence" % "persistence-api" % "1.0" % "it,test->default"
  val persistenceC = "javax.persistence" % "persistence-api" % "1.0" % "compile"
  val commonsIO = "commons-io" % "commons-io" % v.commonsIo % "it,test"
  val liftJson = "net.liftweb" %% "lift-json" % "2.6+" % "it,test"
  val mockJavaMail= "org.jvnet.mock-javamail"     % "mock-javamail"          % "1.9"        % "test" // ApacheV2

  val akkaSlf4j     = akkaTestKitUnclassified % "test"


  def testSet = Seq(junit, akkaTestKit, scalatest)
}

trait UnclassifiedDependency extends VersionContext {

  val akkaTestKitUnclassified = "com.typesafe.akka" %% "akka-testkit" % v.Akka
  val akkaSlf4jUnclassified = "com.typesafe.akka" %% "akka-slf4j" % v.Akka
  val junitUnclassified = "junit" % "junit" % "4.5"
  val scalatestUnclassified = "org.scalatest" %% "scalatest" % v.Scalatest // ApacheV2

}


trait CalderaDependency extends VersionContext {

  val hadoopCommon         = ("org.apache.hadoop"    % "hadoop-common"        % v.Hadoop
    exclude ("hsqldb", "hsqldb")
    exclude ("net.sf.kosmosfs", "kfs")
    exclude ("org.eclipse.jdt", "core")
    exclude ("net.java.dev.jets3t", "jets3t")
    exclude ("oro", "oro")
    exclude ("jdiff", "jdiff")
    exclude ("org.apache.lucene", "lucene-core")
    exclude ("org.slf4j", "slf4j-api")
    exclude ("org.slf4j", "slf4j-log4j12"))

  val hadoopHdfs     = ("org.apache.hadoop"    % "hadoop-hdfs"          % v.Hadoop
    exclude ("hsqldb", "hsqldb")
    exclude ("net.sf.kosmosfs", "kfs")
    exclude ("org.eclipse.jdt", "core")
    exclude ("net.java.dev.jets3t", "jets3t")
    exclude ("oro", "oro")
    exclude ("jdiff", "jdiff")
    exclude ("org.apache.lucene", "lucene-core")
    exclude ("org.slf4j", "slf4j-api")
    exclude ("org.slf4j", "slf4j-log4j12"))

  val hadoopAuth = ("org.apache.hadoop"    % "hadoop-auth"          % v.Hadoop)
  val hadoopMRClientJobClient  = "org.apache.hadoop"    % "hadoop-mapreduce-client-jobclient" % v.Hadoop classifier "tests"

  val hbaseClient       = "org.apache.hbase"     % "hbase-client"         % v.HBase
  val hbaseCommon       = "org.apache.hbase"     % "hbase-common"         % v.HBase
  val hbaseCompat2      = "org.apache.hbase"     % "hbase-hadoop2-compat" % v.HBase
  val hbaseCompat       = "org.apache.hbase"     % "hbase-hadoop-compat"  % v.HBase
  val hbaseServer       = "org.apache.hbase"     % "hbase-server"         % v.HBase
  val hbasePrefixTree   = "org.apache.hbase"     % "hbase-prefix-tree"    % v.HBase
  val hbaseThrift       = "org.apache.hbase"     % "hbase-thrift"         % v.HBase
  val zookeeper         = "org.apache.zookeeper" % "zookeeper"            % v.Zookeeper

}
