package csc.sbt

import sbt.ModuleID

package object dep {

  implicit class ModuleExt(private val mod: ModuleID)  {

    def c: ModuleID = mod % "compile"
    def r: ModuleID = mod % "runtime"
    def p: ModuleID = mod % "provided"
    def t: ModuleID = mod % "test"
  }

}