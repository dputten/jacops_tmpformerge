package csc.sbt

import sbt._
import sbt.Keys._
import sbt.Package.ManifestAttributes

object CommonSettings {

  lazy val excludeTestNames = SettingKey[Seq[String]]("exclude-test-names")
  lazy val excludeTestTags = SettingKey[Seq[String]]("exclude-test-tags")
  lazy val includeTestTags = SettingKey[Seq[String]]("include-test-tags")
  lazy val topProjectFolder = SettingKey[File]("top-project-folder")

  lazy val defaultExcludedTags = Seq("timing")

  lazy val ivyLocalResolver = Resolver.file("localivy", file(Path.userHome.absolutePath + "/.ivy2/local"))(Resolver.ivyStylePatterns)

}

trait CommonSettings {

  import CommonSettings._

  lazy val tagTitle = {
    val tag = csc.sbt.Git.tag
    if (tag.isEmpty) "" else " GITtag: " + tag
  }

  lazy val gitVers = "GITcommit: " + csc.sbt.Git.commitHash + tagTitle

  lazy val baseSettings = Defaults.defaultSettings ++ Publish.settings

  lazy val publishSettings = Publish.versionSettings ++ Seq(
    Publish.defaultPublishTo in ThisBuild <<= crossTarget / "repository"
  )

  lazy val parentSettings = baseSettings ++ Seq(
    publishArtifact in Compile := true
  )

  //cannot define the scalariform settings here, otherwise a ClassCastException is thrown in the client project
  //so each client project must define this (copy & paste)
  def formatSettings: Seq[sbt.Setting[_]]

  lazy val commonSettings = baseSettings ++ formatSettings ++ compilerSettings ++ testSettings ++ Seq(
    packageOptions := Seq(
      ManifestAttributes(
        ("GIT-Version", gitVers)
      )
    )
  )

  lazy val compilerSettings = Seq(
    // compile options
    scalacOptions ++= Seq("-encoding", "UTF-8", "-deprecation", "-unchecked") ++ (
      if (true || (System getProperty "java.runtime.version" startsWith "1.7")) Seq() else Seq("-optimize")), // -optimize fails with jdk7

    javacOptions in (Compile, compile) ++= Seq("-Xlint:unchecked", "-Xlint:deprecation"), //compile:compile specific, otherwise javadoc breaks
    //javacOptions  ++= Seq("-Xlint:unchecked", "-Xlint:deprecation"), //this would pass -Xlint option to javadoc, which doesn' recognize it

    ivyScala := ivyScala.value map { _.copy(overrideScalaVersion = true) }, //force scala version (not allowing 2.9.1.1) //TODO is it a must?

    // to fix scaladoc generation
    fullClasspath in doc in Compile <<= fullClasspath in Compile,
    autoCompilerPlugins := true,

    ivyLoggingLevel in ThisBuild := UpdateLogging.Quiet
  )

  lazy val testSettings = Seq(
    // add filters for tests excluded by name
    testOptions in Test <++= excludeTestNames map { _.map(exclude => Tests.Filter(test => !test.contains(exclude))) },

    // add arguments for tests excluded by tag - includes override excludes (opposite to scalatest)
    testOptions in Test <++= (excludeTestTags, includeTestTags) map { (excludes, includes) =>
      val tags = (excludes.toSet -- includes.toSet).toSeq
      if (tags.isEmpty) Seq.empty else Seq(Tests.Argument("-l", tags.mkString(" ")))
    },

    // add arguments for tests included by tag
    testOptions in Test <++= includeTestTags map { tags =>
      if (tags.isEmpty) Seq.empty else Seq(Tests.Argument("-n", tags.mkString(" ")))
    },

    // show full stack traces
    testOptions in Test += Tests.Argument("-oF")

  )

}

