package csc.sbt

import java.io.File

import sbt._
import sbt.Keys._
import com.typesafe.sbt.SbtNativePackager._

import NativePackagerKeys._

object Dist extends Dist {

  type Mapping = (File, String)
  type FilFilter = File => Boolean
  type LibFilter = Mapping => Boolean

  val distClean = TaskKey[Unit]("clean", "Removes Akka microkernel directory")

  val bootClass = SettingKey[String]("boot-class", "Kernel boot class to use in start script")
  val appStartMemory = SettingKey[String]("app-start-memory", "Starting amount of memory (in -Xms syntax) to be used by the app")
  val appMaxMemory = SettingKey[String]("app-max-memory", "Max amount of memory (in -Xmx syntax) to be used by the app")
  val appSysProps = SettingKey[List[String]]("app-system-props", "App specific system properties")

  val targetAppFolder = SettingKey[Option[String]]("target-app-folder", "Target local folder for apps")
  val copyApp = TaskKey[Unit]("copy-app", "Copies app .tar.gz file to a local folder")

  val targetAppFolderProperty = "TARGET_APP_FOLDER"
}

trait Dist {

  import Dist._

  def convertLibMapping(mapping: (File, String)): (File, String) = (mapping._1, s"lib/${mapping._1.getName}")

  def processLibMappings(source: Seq[(File, String)], ownJarFilename: String): Seq[(File, String)] = {
    //changing the name of the target file (no group in jar name)
    val converted = source map { m => convertLibMapping(m) }

    val allExclusionFilters = Seq(
      filenameExcludeFilter("slf4j-log4j"), //TODO csilva: why is this needed?
      filenameExcludeFilter(ownJarFilename) //remove the jar of the current project (will be added to 'deploy')
    )
    converted.filter(compositeLibFilter(allExclusionFilters))
  }

  private def commonDistSettings(pkgBaseDir: String): Seq[sbt.Setting[_]] = Seq(
    packageName in Universal := pkgBaseDir, //we need to do this to set the main folder inside the package
    scriptClasspathOrdering := {
      //overriding scriptClasspathOrdering, the best place to override the libs
      val ownJarFilename = (packageBin in Compile).value.getName
      processLibMappings(scriptClasspathOrdering.value, ownJarFilename)
    },
    dist := {
      //overriding dist
      val input = (packageZipTarball in Universal).value //creates .tgz
      val output = distFile(name.value)
      IO.move(input, output) //moves package file (to top project folder)
      output
    },
    distClean := {
      val file = distFile(name.value)
      if (file.exists()) {
        streams.value.log.info(s"Deleting file ${file.getName}")
        file.delete()
      }
    },
    targetAppFolder := Option(System.getenv(targetAppFolderProperty)),
    copyApp := {
      targetAppFolder.value match {
        case None =>  streams.value.log.warn(s"No '$targetAppFolderProperty' env property defined. Nothing will be copied")
        case Some(path) =>
          val folder = new File(path)
          (folder.exists(), folder.isDirectory) match {
            case (true, true) =>
              val file = distFile(name.value)
              if (file.exists()) {
                streams.value.log.info(s"Copying file ${file.getName} to $path")
                IO.copyFile(file, folder / file.getName, true)
              } else {
                streams.value.log.warn(s"App file '${file.getPath}' does not exist. Nothing will be copied")
              }
            case (true, false) => streams.value.log.warn(s"'$path' is not a folder. Nothing will be copied")
            case _ => streams.value.log.warn(s"Folder '$path' does not exist. Nothing will be copied")
          }
      }
    }
  )

  def simpleDistSettings(pkgName: String): Seq[sbt.Setting[_]] =
    packageArchetype.java_application ++ commonDistSettings(pkgName)

  def distSettingsFor(distSettings: DistSettings): Seq[sbt.Setting[_]] =
    packageArchetype.akka_application ++
      Seq(
        appStartMemory := distSettings.startMem,
        appMaxMemory := distSettings.maxMem,
        appSysProps := distSettings.appSysProps,
        bootClass := distSettings.bootClass,
        mainClass := None, //force no main class
        makeBashScript := None, //make sure no automatic bash script is included
        makeBatScript := None //make sure no automatic bat script is included
      ) ++ commonDistSettings(distSettings.output)

  def distFile(projectName: String): File = new File(".", s"${projectName.toLowerCase}.tar.gz")

  /**
   * @param expr
   * @return filter excluding all filenames matching (using 'contains') the given expression
   */
  def filenameExcludeFilter(expr: String): LibFilter = !_._1.getName.contains(expr)

  def compositeLibFilter(filters: Seq[LibFilter]): LibFilter = mapping => {
    val excluded = filters.exists( f => !f(mapping) ) //check if any filter returns false
    !excluded //we keep it id not excluded by any
  }

  /**
   * @param base base folder or file
   * @param target base path of the mapping
   * @return mappings for all files found from a given base
   */
  def getFileMappings(base: File, target: String, filter: FilFilter = defaultFileFilter): Seq[Mapping] = {
    val basePath = base.getPath

    def pathFor(file: File): String = if (base.isDirectory) {
      target + file.getPath.substring(basePath.length)
    } else {
      target + "/" + file.getName
    }

    if (base.exists()) {
      val pf = PathFinder(base)
      pf.***.get.filter(filter) map { f =>
        f -> pathFor(f)
      }
    } else Nil
  }

  def getMultiFileMappings(base: File, paths: Seq[String], target: String, filter: FilFilter = defaultFileFilter): Seq[Mapping] = {
    val all = for (path <- paths) yield getFileMappings(base / path, target, filter) //.flatten
    all.flatten
  }

  def createLaucherFile(name: String, bootclass: String, startMem: String, maxMem: String, sysProps: List[String]): File = {
    val startFile = File.createTempFile("bootLauncher", "")
    IO.copyFile(file("config/startAkkaKernel"), startFile)
    replaceInFiles("startAkkaKernel", name, startFile,":")
    replaceInFiles("csc.startup.Boot", bootclass, startFile,":")
    //we replace the whole mem setting, assuming it is 1536M: not the cleanest, but the safest (and we can even comment this replace)
    replaceInFiles("-Xms1536M", "-Xms" + startMem, startFile,":")
    replaceInFiles("-Xmx1536M", "-Xmx" + maxMem, startFile,":")
    replaceInFiles("-DappSpecificProps", sysProps.mkString(" "), startFile,":")
    setExecutable(startFile, true)
    startFile
  }

  /**
   * replace the find string to the replace string in all the files described in files
   * @param find: The search string
   * @param replace: The replacement
   * @param file: The files where the replacement has to be done
   */
  def replaceInFiles(find: String, replace: String, file: File, sep:String) {
    val osName = System.getProperty("os.name")
    val isMac = osName == "Mac OS X" || osName.startsWith("Darwin")

    //use sed -i 's/1.0/1.1/g' amigoboras
    val inPlaceFlag = if (isMac) " -i \'\' " else " -i "
    val cmd = ("sed " + inPlaceFlag + " \'s"+sep+"%s"+sep+"%s"+sep+"g\' %s").format(find, replace, file.toString)

    val exit = Process(Array[String]("/bin/sh", "-c", cmd)).run().exitValue()
  }

  private def setExecutable(target: File, executable: Boolean): Option[String] = {
    val success = target.setExecutable(executable, false)
    if (success) None else Some("Couldn't set permissions of " + target)
  }

  val soFileExtension = ".so"

  val defaultFileFilter: FilFilter = _.isFile

  val soFileFilter = extensionFileFilter(soFileExtension)

  def extensionFileFilter(ext: String*): FilFilter =
    file => ext.exists(file.getName.endsWith(_))

}

case class DistSettings(output: String, bootClass: String, startMem: String, maxMem: String, appSysProps: List[String] = Nil)

object PackageMappings {

  import Dist._

  def lib64Contents(baseFolderKey: SettingKey[java.io.File], extraFileExtensions: Seq[String] = Nil) = baseFolderKey map { dir =>
    val extensions: Seq[String] = extraFileExtensions :+ soFileExtension
    val filter = extensionFileFilter(extensions:_*)
    getFileMappings(dir / "lib64", "lib64", filter) //'lib64' contents
  }

  def lib32Contents(baseFolderKey: SettingKey[java.io.File], folder: String, extraFileExtensions: Seq[String] = Nil) = baseFolderKey map { dir =>
    val extensions: Seq[String] = extraFileExtensions :+ soFileExtension
    val filter = extensionFileFilter(extensions:_*)
    getFileMappings(dir / folder, "lib32", filter) //'lib32' contents
  }

//  val akkaAppLauncher = (name, bootClass, appStartMemory, appMaxMemory, appSysProps) map {
//    (name, bootClass, startMem, maxMem, appSysProps) =>
//    val nameLower = name.toLowerCase
//    createLaucherFile(nameLower, bootClass, startMem, maxMem, appSysProps) -> s"bin/$nameLower" // executable script on 'bin'
//  }

  val akkaAppLauncher = (name, bootClass, appStartMemory, appMaxMemory, appSysProps) map {
    (name, bootClass, startMem, maxMem, appSysProps) =>
      val nameLower = name.toLowerCase
      createLaucherFile(nameLower, bootClass, startMem, maxMem, appSysProps) -> s"bin/$nameLower" // executable script on 'bin'
  }

  val packageJarInDeploy = packageBin in Compile map { jar =>
    jar -> s"deploy/${jar.name}" // 'deploy' contents
  }

  val multiConfig = sourceDirectory map { dir =>
    // 'config' contents, from 3 possible locations
    getMultiFileMappings(dir, Seq("config", "main/config", "main/resources"), "config")
  }

  def loggingConfig(targetLogFile: Option[String]) = target map { dir =>
    val pathFinder: PathFinder = file("config") * "*.xml"
    pathFinder.get map { f =>
      val name = f.getName
      val sourceFile = if (name == "logback.xml" && targetLogFile.isDefined) {
        val out = File.createTempFile("logback", null)
        IO.copyFile(f, out)
        replaceInFiles("registration.log", targetLogFile.get, out, ":")
        out
      } else f
      sourceFile -> s"config/$name"
    }
  }

  val binScripts = sourceDirectory map { dir =>
    getFileMappings(dir / "bin", "bin") // 'bin' contents
  }

  val mainScripts = sourceDirectory map { dir =>
    getFileMappings(dir / "main/scripts", "bin") // 'bin' contents
  }

  val emptyLogs = target map { _ =>
    file(sys.props("java.io.tmpdir")) -> "logs" //empty folder 'logs'
  }

}
