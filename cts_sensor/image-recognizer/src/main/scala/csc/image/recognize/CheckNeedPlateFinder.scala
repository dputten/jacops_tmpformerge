/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.image.recognize

import akka.actor.{ ActorLogging, ActorRef, Actor }
import imagefunctions.LicensePlate

class CheckNeedPlateFinder(recognizer: ActorRef, createResponse: ActorRef) extends Actor with ActorLogging {
  def receive = {
    case msg: RecognizeImageProcessMessage ⇒ {
      val responses = msg.responses

      val skipPlateFinder = msg.request.options.get(RecognizeOptions.icrFlow).getOrElse(RecognizeOptions.icrFlow_SKIP).equalsIgnoreCase(RecognizeOptions.icrFlow_SKIP)
      if (skipPlateFinder) {
        log.info("%s: Skip plateFinder is set => createResponse".format(msg.request.msgId))
        createResponse ! msg
      } else if (responses.isEmpty) {
        log.info("%s: Didn't found a plate result => do extra recognition step".format(msg.request.msgId))
        recognizer ! msg
      } else {
        val foundLicense = responses.values.find(result ⇒ {
          if (result.plate.isDefined) {
            val plate = result.plate.get
            val options = msg.request.options

            plate.plateStatus == LicensePlateResponse.PLATESTATUS_PLATE_FOUND && !(
              plate.recognizer == Recognizer.INTRADA && plate.plateCountryCode == LicensePlate.PLATE_NETHERLANDS &&
              plate.plateConfidence < RecognizeOptions.intradaPlateReliableLimit(options))

          } else {
            false
          }
        })
        if (foundLicense.isDefined) {
          createResponse ! msg
        } else {
          recognizer ! msg
        }
      }
    }
  }
}