/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.image.recognize

import csc.vehicle.message.{ ImageFormat, Priority }

import akka.actor.ActorRef
import csc.amqp.EnumerationSerializer
import csc.amqp.adapter.CorrelatedMessage
import net.liftweb.json.{ DefaultFormats, Formats }

object RecognizeOptions {
  val arhModule = "arh.module"
  val arhGroup = "arh.group"
  val arhLowestLicence = "arh.license.lowest"
  val arhCreateLicenseSnippet = "arh.license.snippet"
  val arhPostFixLicenseSnippet = "arh.license.postfix"
  val intradaSettingsFile = "intrada.settings"
  val intradaPostFixLicenseSnippet = "intrada.license.postfix"
  val intradaCreateLicenseSnippet = "intrada.license.snippet"

  val secondCheckMinimalPlateConfidence = "2nd.check.minPlateConf"
  val secondCheckMinimalCountryConfidence = "2nd.check.minCountryConf"

  val intradaPlateReliableLimit = "intrada.license.confidence.reliable"
  val intradaCountryReliableLimit = "intrada.country.confidence.reliable"
  val arhPlateReliableLimit = "arh.license.confidence.reliable"
  val arhCountryReliableLimit = "arh.country.confidence.reliable"

  val icrFlow = "icr.flow"
  val icrFlow_SKIP = "SKIP"
  val icrFlow_NORMAL = "NORMAL"
  val icrFlow_SIMULATE = "SIMULATE"
  val icrCreateLicenseSnippet = "icr.license.snippet"
  val icrPostFixLicenseSnippet = "icr.license.postfix"
  val icrDebug = "icr.debug"
  val icrFitNum = "icr.fitNum"
  val icrCameraType = "icr.camera.type"
  val icrImageRotate = "icr.image.rotate"
  val icrImageConversion = "icr.image.convertion"
  val icrImageSharpen = "icr.image.sharpen"

  val icrSimReturn = "icr.simulationMode.defaultReturnStatus"
  val icrSimStatus = "icr.simulationMode.defaultPlateStatus"
  val icrSimPlate = "icr.simulationMode.defaultPlateNumber"
  val icrSimPlateType = "icr.simulationMode.defaultPlateType"
  val icrSimPlateConf = "icr.simulationMode.defaultPlateConfidence"
  val icrSimCountry = "icr..simulationMode.defaultPlateCountryCode"

  def secondCheckMinimalPlateConfidence(options: Map[String, String]): Int = {
    getConfidence(options, secondCheckMinimalPlateConfidence, 10)
  }

  def secondCheckMinimalCountryConfidence(options: Map[String, String]): Int = {
    getConfidence(options, secondCheckMinimalCountryConfidence, 10)
  }

  def intradaPlateReliableLimit(options: Map[String, String]): Int = {
    getConfidence(options, intradaPlateReliableLimit, 75)
  }

  def intradaCountryReliableLimit(options: Map[String, String]): Int = {
    getConfidence(options, intradaCountryReliableLimit, 75)
  }
  def arhPlateReliableLimit(options: Map[String, String]): Int = {
    getConfidence(options, arhPlateReliableLimit, 60)
  }
  def arhCountryReliableLimit(options: Map[String, String]): Int = {
    getConfidence(options, arhCountryReliableLimit, 60)
  }

  private def getConfidence(options: Map[String, String], optionName: String, default: Int): Int = {
    options.get(optionName).flatMap(
      str ⇒ try { Some(str.toInt) } catch { case t: Throwable ⇒ None }).getOrElse(default)
  }
}

case class RecognizeImageRequest(msgId: String,
                                 priority: Priority.Value,
                                 imageFile: String,
                                 imageFormat: ImageFormat.Value,
                                 options: Map[String, String],
                                 time: Long = System.currentTimeMillis()) extends CorrelatedMessage

case class RecognizeImageResponse(msgId: String,
                                  imageFile: String,
                                  plate: Option[LicensePlateResponse],
                                  time: Long = System.currentTimeMillis()) extends CorrelatedMessage

case class LicensePlateResponse(
  recognizer: String,
  returnStatus: Int,
  plateStatus: Int,
  plateNumber: String,
  rawPlateNumber: String,
  plateConfidence: Int,
  plateCountryCode: String,
  plateCountryConfidence: Int,
  licenseSnippetFileName: String,
  plateXOffset: Int,
  plateYOffset: Int,
  plateTL: Option[ImagePoint],
  plateTR: Option[ImagePoint],
  plateBL: Option[ImagePoint],
  plateBR: Option[ImagePoint],
  plateType: Option[String] = None) {

  def hasPlateResult: Boolean = {
    plateStatus == LicensePlateResponse.PLATESTATUS_PLATE_FOUND &&
      !plateNumber.isEmpty &&
      !plateCountryCode.isEmpty
  }
}

object LicensePlateResponse {
  val PLATESTATUS_PLATE_FOUND = 0
  val PLATESTATUS_INVALID_IMAGE_DESC = -1
  val PLATESTATUS_POOR_GREY_IMAGE = -2
  val PLATESTATUS_NO_PLATE_FOUND = -3

  val PLATE_UNKNOWN = "Unknown"

  val PLATE_TYPE_UNKNOWN = "Unknown"
  val PLATE_TYPE_NORMAL = "Normal"
  val PLATE_TYPE_SQUARE = "Square"
  val PLATE_TYPE_SMALL = "Small"
}

case class ImagePoint(x: Int, y: Int)

case class RecognizeImageProcessMessage(requester: ActorRef,
                                        request: RecognizeImageRequest,
                                        responses: Map[String, RecognizeImageResponse])

case class PlateFinderSimulationMode(enabled: Boolean = false,
                                     defaultReturnStatus: Int = 0,
                                     defaultPlateStatus: Int = LicensePlateResponse.PLATESTATUS_PLATE_FOUND,
                                     defaultPlateType: Option[String] = None,
                                     defaultPlateNumber: String = "",
                                     defaultPlateConfidence: Int = 1,
                                     defaultPlateCountryCode: String = "")

trait RecognizerFormats {
  val formats: Formats = DefaultFormats + new EnumerationSerializer(Priority, ImageFormat)
}

