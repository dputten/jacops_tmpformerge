/*
 * Copyright © CSC
 * Date: Jun 25, 2010
 * Time: 11:16:37 AM
 */
package csc.image.recognize

import java.io.File
import csc.akka.logging.DirectLogging
import imagefunctions._
import csc.vehicle.message.ImageFormat

/**
 * The interface used to recognize Images.
 */
trait Recognizer {
  /**
   * Do the actual recognition of the license plate.
   * @param imageFile the images used to recognize the license from.
   * @param format indicates the type of the image
   */
  def recognizeImage(imageFile: File, format: ImageFormat.Value, options: Map[String, String]): LicensePlate

  /**
   *  Get the performance points used in the logging.
   */
  def getPerformancePoints(): Tuple2[Int, Int]

  /**
   * get the name of the Recognizer.
   */
  def getName(): String

  def stop()
}
object Recognizer {
  val ARH = "ARH"
  val INTRADA = "INTRADA"
  val ICR_PLATEFINDER = "ICR"
  val ICR_PLATEREADER = "ICR"

  def noPlate(recognizer: String): LicensePlate = {
    val plate = new LicensePlate()
    plate.setPlateStatus(LicensePlate.PLATESTATUS_NO_PLATE_FOUND)
    plate.setLicenseSnippetFileName("")
    plate.setPlateConfidence(0)
    plate.setPlateCountryCode(LicensePlate.PLATE_UNKNOWN)
    plate.setPlateNumber("")
    plate.setPlateCountryConfidence(0)
    plate.setPlateXOffset(0)
    plate.setPlateYOffset(0)
    plate.setRecognizer(recognizer)
    plate.setReturnStatus(0)
    plate
  }
}

import Recognizer._

/**
 * The ARH implementation of the Recognizer.
 */
class ArhRecognizer extends Recognizer with DirectLogging {

  private def getBoolean(options: Map[String, String], option: String, default: Boolean): Boolean = {
    val strOpt = options.get(option)
    strOpt.map(_.toLowerCase == "true").getOrElse(default)
  }
  /**
   * Do the actual recognizion of the licenseplate  .
   * @param imageFile the imges used to recognize the license from.
   * @param format indicates if the image is compressed
   */
  def recognizeImage(imageFile: File, format: ImageFormat.Value, options: Map[String, String]): LicensePlate = {
    val imageFunction = ImageFunctionsFactory.getARHFunctions()

    log.info("processing imageFile: " + imageFile)
    log.info("processing path: " + imageFile.getAbsolutePath)

    //get the ARH supported options
    val module = options.get(RecognizeOptions.arhModule).getOrElse(null)
    val group = options.get(RecognizeOptions.arhGroup).getOrElse(null)
    val lastLicence = getBoolean(options, RecognizeOptions.arhLowestLicence, false)
    val createSnippet = getBoolean(options, RecognizeOptions.arhCreateLicenseSnippet, true)
    val snippetFile = if (createSnippet) {
      imageFile.getAbsolutePath + options.get(RecognizeOptions.arhPostFixLicenseSnippet).getOrElse("_plate_arh.tif")
    } else {
      null
    }
    try {
      format match {
        case ImageFormat.TIF_BAYER ⇒ imageFunction.bayerToArh(imageFile.getAbsolutePath, snippetFile, lastLicence, module, group)
        case ImageFormat.TIF_RGB   ⇒ imageFunction.rgbToArh(imageFile.getAbsolutePath, snippetFile, lastLicence, module, group)
        case ImageFormat.TIF_GRAY  ⇒ imageFunction.rgbToArh(imageFile.getAbsolutePath, snippetFile, lastLicence, module, group)
        case ImageFormat.JPG_RGB   ⇒ imageFunction.jpgToArh(imageFile.getAbsolutePath, snippetFile, lastLicence, module, group)
        case ImageFormat.JPG_GRAY  ⇒ imageFunction.jpgToArh(imageFile.getAbsolutePath, snippetFile, lastLicence, module, group)
        case _                     ⇒ noPlate(getName())
      }
    } catch {
      case ex: ImageFunctionsException ⇒ noPlate(getName())
    }
  }
  /**
   *  Get the performance points used in the logging.
   */
  def getPerformancePoints(): Tuple2[Int, Int] = {
    (33, 34)
  }

  /**
   * get the name of the Recognizer.
   */
  def getName(): String = {
    Recognizer.ARH
  }
  def stop() {}
}

/**
 * The ARH implementation of the Recognizer.
 */
class IntradaRecognizer extends Recognizer {

  private def getBoolean(options: Map[String, String], option: String, default: Boolean): Boolean = {
    val strOpt = options.get(option)
    strOpt.map(_.toLowerCase == "true").getOrElse(default)
  }

  /**
   * Do the actual recognizion of the licenseplate  .
   * @param imageFile the imges used to recognize the license from.
   * @param format indicates if the image is compressed
   */
  def recognizeImage(imageFile: File, format: ImageFormat.Value, options: Map[String, String]): LicensePlate = {
    val imageFunction = ImageFunctionsFactory.getIntradaFunctions

    //get the ARH supported options
    val settingFile = options.get(RecognizeOptions.intradaSettingsFile).getOrElse(null)
    val createSnippet = getBoolean(options, RecognizeOptions.intradaCreateLicenseSnippet, true)
    val snippetFile = if (createSnippet) {
      imageFile.getAbsolutePath + options.get(RecognizeOptions.intradaPostFixLicenseSnippet).getOrElse("_plate_intrada.tif")
    } else {
      null
    }

    try {
      imageFunction.imageToIntrada(imageFile.getAbsolutePath, snippetFile, settingFile)
    } catch {
      case ex: ImageFunctionsException ⇒ noPlate(getName())
    }
  }
  /**
   *  Get the performance points used in the logging.
   */
  def getPerformancePoints(): Tuple2[Int, Int] = {
    (31, 32)
  }

  /**
   * get the name of the Recognizer.
   */
  def getName(): String = {
    Recognizer.INTRADA
  }
  def stop() {}
}
