/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.image.recognize

import akka.actor.{ ActorLogging, Actor }
import imagefunctions.LicensePlate

/**
 * Use only intrada results when they are reliable or ARH has no result.
 * An exception is made for German licences, because it needs the ARH raw license plate
 * When ARH and intrada have the same license the raw of ARH is included in the intrada result
 * Otherwise the arh result is still used.
 */
class MergeARHIntrada extends Actor with ActorLogging {
  def receive = {
    case msg: RecognizeImageProcessMessage ⇒ {
      val options = msg.request.options
      val arhPlateOpt = msg.responses.get(Recognizer.ARH).flatMap(_.plate)
      val intradaPlateOpt = msg.responses.get(Recognizer.INTRADA).flatMap(_.plate)

      val response = (arhPlateOpt, intradaPlateOpt) match {
        case (Some(arhPlate), Some(intradaPlate)) if (arhPlate.hasPlateResult && intradaPlate.hasPlateResult) ⇒
          bothPlateResults(msg, arhPlate, intradaPlate)
        case (Some(arhPlate), Some(intradaPlate)) if (intradaPlate.hasPlateResult) ⇒
          onlyIntradaResult(msg, arhPlate, intradaPlate)
        case (Some(arhPlate), Some(intradaPlate)) if (arhPlate.hasPlateResult) ⇒ {
          log.info("%s: Only Arh found license. use arh".format(msg.request.msgId))
          RecognizeImageResponse(msg.request.msgId, msg.request.imageFile, Some(arhPlate), now)
        }
        case (Some(arhPlate), Some(intradaPlate)) if (arhPlate.returnStatus == 0 && arhPlate.plateStatus == LicensePlateResponse.PLATESTATUS_PLATE_FOUND) ⇒ {
          log.info("%s: ARH and Intrada returned empty license. use arh".format(msg.request.msgId))
          RecognizeImageResponse(msg.request.msgId, msg.request.imageFile, Some(arhPlate), now)
        }
        case (Some(arhPlate), Some(intradaPlate)) if (intradaPlate.returnStatus == 0 && intradaPlate.plateStatus == LicensePlateResponse.PLATESTATUS_PLATE_FOUND) ⇒ {
          log.info("%s: ARH and Intrada returned empty license. use intrada".format(msg.request.msgId))
          RecognizeImageResponse(msg.request.msgId, msg.request.imageFile, Some(intradaPlate), now)
        }
        case (Some(arhPlate), Some(intradaPlate)) ⇒ {
          log.info("%s: ARH and Intrada returned empty license. check platefinder".format(msg.request.msgId))
          createResponseUsingPlateFinder(msg)
        }
        case (None, Some(intradaPlate)) ⇒
          onlyIntradaPlate(msg, intradaPlate)
        case (Some(arhPlate), None) ⇒ {
          if (arhPlate.returnStatus == 0 && arhPlate.plateStatus == LicensePlateResponse.PLATESTATUS_PLATE_FOUND) {
            log.info("%s: Only Arh returned empty license. use arh".format(msg.request.msgId))
            RecognizeImageResponse(msg.request.msgId, msg.request.imageFile, Some(arhPlate), now)
          } else {
            createResponseUsingPlateFinder(msg)
          }
        }
        case _ ⇒ {
          log.info("%s: ARH and Intrada didn't found licenses. check platefinder".format(msg.request.msgId))
          createResponseUsingPlateFinder(msg)
        }
      }
      msg.requester ! response
    }
  }

  private def createResponseUsingPlateFinder(msg: RecognizeImageProcessMessage): RecognizeImageResponse = {
    val plateFinderResult = msg.responses.get(Recognizer.ICR_PLATEFINDER).flatMap(_.plate)
    plateFinderResult match {
      case Some(icrPlate) ⇒ {
        if (icrPlate.returnStatus == 0 && icrPlate.plateStatus == LicensePlateResponse.PLATESTATUS_PLATE_FOUND) {
          log.info("%s: PlateFinder found license with conf %d. use Platefinder result".format(msg.request.msgId, icrPlate.plateConfidence))
          //use platefinder results
          RecognizeImageResponse(msg.request.msgId, msg.request.imageFile, Some(icrPlate), now)
        } else if (icrPlate.returnStatus >= 0 && icrPlate.plateStatus == LicensePlateResponse.PLATESTATUS_NO_PLATE_FOUND) {
          log.info("%s: PlateFinder didn't found licenses. use Platefinder result".format(msg.request.msgId))
          //use platefinder results
          RecognizeImageResponse(msg.request.msgId, msg.request.imageFile, Some(icrPlate), now)
        } else {
          log.info("%s: PlateFinder had problems with image [%d,%d]. use empty result".format(msg.request.msgId, icrPlate.returnStatus, icrPlate.plateStatus))
          RecognizeImageResponse(msg.request.msgId, msg.request.imageFile, None, now)
        }
      }
      case None ⇒ {
        log.info("%s: No PlateFinder result. use empty result".format(msg.request.msgId))
        RecognizeImageResponse(msg.request.msgId, msg.request.imageFile, None, now)
      }
    }
  }

  private def bothPlateResults(msg: RecognizeImageProcessMessage,
                               arhPlate: LicensePlateResponse,
                               intradaPlate: LicensePlateResponse): RecognizeImageResponse = {

    val options = msg.request.options

    if (intradaPlate.hasPlateResult &&
      intradaPlate.plateConfidence >= RecognizeOptions.intradaPlateReliableLimit(options) &&
      intradaPlate.plateCountryCode == LicensePlate.PLATE_NETHERLANDS && //Only NL can be replaced
      intradaPlate.plateCountryCode == arhPlate.plateCountryCode &&
      intradaPlate.plateNumber == arhPlate.plateNumber) {

      log.info("%s: ARH and Intrada found licenses. use intrada because of high confidence %d".format(msg.request.msgId, intradaPlate.plateConfidence))
      //use intradaResult
      val correctedPlate = correctIntradaConfidencesToARH(intradaPlate, options)
      RecognizeImageResponse(msg.request.msgId, msg.request.imageFile, Some(correctedPlate), now)
    } else {
      if (intradaPlate.plateCountryCode != LicensePlate.PLATE_NETHERLANDS) {
        log.info("%s: ARH and Intrada found licenses. use arh because of intrada country is not NL %d".format(msg.request.msgId, intradaPlate.plateConfidence))
      } else if (intradaPlate.plateConfidence >= RecognizeOptions.intradaPlateReliableLimit(options)) {
        log.info("%s: ARH and Intrada found licenses. use arh because of low confidence intrada %d".format(msg.request.msgId, intradaPlate.plateConfidence))
      } else {
        log.info("%s: ARH and Intrada found licenses. but were not equal arh=(%s,%s) intrada=(%s,%s)".format(msg.request.msgId, arhPlate.plateNumber, arhPlate.plateCountryCode, intradaPlate.plateNumber, intradaPlate.plateCountryCode))
      }
      //use arh
      RecognizeImageResponse(msg.request.msgId, msg.request.imageFile, Some(arhPlate), now)
    }
  }

  private def onlyIntradaResult(msg: RecognizeImageProcessMessage,
                                arhPlate: LicensePlateResponse,
                                intradaPlate: LicensePlateResponse): RecognizeImageResponse = {

    val options = msg.request.options

    //use intradaResult when country is NL and high confidence
    if (intradaPlate.plateCountryCode == LicensePlate.PLATE_NETHERLANDS &&
      intradaPlate.plateConfidence >= RecognizeOptions.intradaPlateReliableLimit(options)) {
      log.info("%s: Only Intrada found license with country NL. use intrada".format(msg.request.msgId))
      val correctedPlate = correctIntradaConfidencesToARH(intradaPlate, options)
      RecognizeImageResponse(msg.request.msgId, msg.request.imageFile, Some(correctedPlate), now)
    } else {
      log.info("%s: Only Intrada found license with conf %d and country %s. use arh".format(msg.request.msgId, intradaPlate.plateConfidence, intradaPlate.plateCountryCode))
      RecognizeImageResponse(msg.request.msgId, msg.request.imageFile, Some(arhPlate), now)
    }
  }

  private def onlyIntradaPlate(msg: RecognizeImageProcessMessage, intradaPlate: LicensePlateResponse): RecognizeImageResponse = {

    val options = msg.request.options

    if (intradaPlate.plateCountryCode == LicensePlate.PLATE_NETHERLANDS &&
      intradaPlate.plateConfidence >= RecognizeOptions.intradaPlateReliableLimit(options)) {
      log.info("%s: Only Intrada found license with country NL. use intrada".format(msg.request.msgId))
      val correctedPlate = correctIntradaConfidencesToARH(intradaPlate, options)
      RecognizeImageResponse(msg.request.msgId, msg.request.imageFile, Some(correctedPlate), now)
    } else {
      log.info("%s: Only Intrada found license with conf %d and country %s. check platefinder".format(msg.request.msgId, intradaPlate.plateConfidence, intradaPlate.plateCountryCode))
      createResponseUsingPlateFinder(msg)
    }
  }

  /**
   * Get the raw plate information of the ARH only when both plate numbers results are the same
   * @param arhPlate the plate result of arh
   * @param intradaPlate the plate result of intrada
   * @return The raw platenumber of arh when plates are the same otherwise None
   */
  private def getRawLicense(arhPlate: LicensePlateResponse, intradaPlate: LicensePlateResponse): Option[String] = {
    //check if arh plate is the same as the intrada
    if (arhPlate.plateNumber == intradaPlate.plateNumber) {
      Some(arhPlate.rawPlateNumber)
    } else {
      None
    }
  }

  /**
   * Correct the intrada confidence to aprouch the ARH confidences.
   * because arh is more reliable at lower confidences.
   * For example ARH with confidence 60 is almost the same as intrada with confidence 80
   * Therefor we change the confidences: The delta between the confidence and this reliable limit
   * will be the same after this correction.
   *
   * @param plate the intarada plate result
   * @param options the options containing the limits
   * @return the corrected plate
   */
  private def correctIntradaConfidencesToARH(plate: LicensePlateResponse, options: Map[String, String]): LicensePlateResponse = {
    val intradaPlateReliableLimit = RecognizeOptions.intradaPlateReliableLimit(options)
    val intradaCountryReliableLimit = RecognizeOptions.intradaCountryReliableLimit(options)
    val arhPlateReliableLimit = RecognizeOptions.arhPlateReliableLimit(options)
    val arhCountryReliableLimit = RecognizeOptions.arhCountryReliableLimit(options)

    val difPlate = plate.plateConfidence - intradaPlateReliableLimit
    val difCountry = plate.plateCountryConfidence - intradaCountryReliableLimit

    var correctedPlate = checkConfidenceBoundries(arhPlateReliableLimit + difPlate)
    val correctedCountry = checkConfidenceBoundries(arhCountryReliableLimit + difCountry)
    plate.copy(plateConfidence = correctedPlate, plateCountryConfidence = correctedCountry)
  }

  protected def now: Long = System.currentTimeMillis()

  /**
   * confidence must be between 0 and 100
   * @param conf
   * @return
   */
  private def checkConfidenceBoundries(conf: Int): Int = {
    if (conf > 100) {
      100
    } else if (conf < 0) {
      0
    } else {
      conf
    }
  }
}