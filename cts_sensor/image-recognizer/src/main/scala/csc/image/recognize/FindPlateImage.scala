/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.image.recognize

import base.{ SectionGuard, Timing, FavorCriticalProcess }
import akka.actor.{ ActorLogging, Actor, ActorRef }
import akka.event.LoggingReceive
import java.io.File
import imagefunctions.{ Point, ImageFunctionsException }
import imagefunctions.icr.LicenseData
import org.slf4j.LoggerFactory

class FindPlateImage(sectionGuard: Option[FavorCriticalProcess],
                     nextStep: Option[ActorRef],
                     plateFinder: PlateFinder) extends Actor with Timing with ActorLogging {

  override def preRestart(reason: Throwable, message: Option[Any]) {
    super.preRestart(reason, message)
    log.error(reason, "Restart FindPlateImage due to %s processing %s".format(reason.getMessage, message))
  }

  def receive = LoggingReceive {
    case msg: RecognizeImageRequest ⇒ {
      performanceLogInterval(log, 35, 36, "PlateFinder", msg.msgId,
        {
          val response = recognize(msg)
          nextStep match {
            case Some(actor) ⇒ actor ! RecognizeImageProcessMessage(
              requester = sender,
              request = msg,
              responses = Map(Recognizer.ICR_PLATEFINDER -> response))
            case None ⇒ sender ! response
          }
        })
    }
    case msg: RecognizeImageProcessMessage ⇒ {
      performanceLogInterval(log, 37, 38, "Platefinder", msg.request.msgId,
        {
          val response = recognize(msg.request)
          nextStep match {
            case Some(actor) ⇒ {
              val responses = msg.responses + (Recognizer.ICR_PLATEFINDER -> response)
              actor ! msg.copy(responses = responses)
            }
            case None ⇒ sender ! response
          }
        })
    }
  }

  private def recognize(msg: RecognizeImageRequest): RecognizeImageResponse = {
    val file = new File(msg.imageFile)
    var plateResult: Option[LicenseData] = None
    def recognizeLicense() { plateResult = Option(plateFinder.recognizeImage(file, msg.imageFormat, msg.options)) }

    try {
      SectionGuard.process(sectionGuard, msg.priority, recognizeLicense())
    } catch {
      case e: ImageFunctionsException ⇒ {
        log.error("PlateFinder returned " + e.getMessage)
      }
    }

    if (log.isInfoEnabled) {
      plateResult match {
        case Some(licenseResult) ⇒ {
          log.info("PlateFinder: eventId=[%s], found%s license plate in file %s, state=[%d][%s]license=[%s] license conf=[%d] country=[%s] type=[%d][%s]".format(
            msg.msgId,
            if (licenseResult.getStatus == 0) "" else " No",
            file,
            licenseResult.getStatus, licenseResult.getStatusString,
            licenseResult.getPlate, licenseResult.getConfidence,
            licenseResult.getCountry, licenseResult.getType, licenseResult.getTypeString))
        }
        case None ⇒ {
          log.info("PlateFinder: eventId=[%s], platefinder could not find license plate in file %s.".format(msg.msgId, file))
        }
      }
    }
    val simulationMode = FindPlateImage.createSimulationMode(msg.options)
    RecognizeImageResponse(msgId = msg.msgId, imageFile = msg.imageFile,
      plate = FindPlateImage.createLicensePlateResult(plateResult, msg.msgId, simulationMode), time = now)
  }

  def now: Long = System.currentTimeMillis()
}

object FindPlateImage {
  def createLicensePlateResult(licensePlate: Option[LicenseData], msgId: String = "", simulationMode: PlateFinderSimulationMode = PlateFinderSimulationMode()): Option[LicensePlateResponse] = {
    val result = licensePlate.map(plate ⇒ {
      val TL = createPoint(plate.getPlateTL)
      LicensePlateResponse(recognizer = Recognizer.ICR_PLATEFINDER,
        returnStatus = plate.getReturnStatus,
        plateStatus = plate.getStatus,
        plateNumber = createString(plate.getPlate),
        rawPlateNumber = createString(plate.getPlate),
        plateConfidence = plate.getConfidence,
        plateCountryCode = createString(plate.getCountry),
        plateCountryConfidence = 0,
        licenseSnippetFileName = createString(plate.getLicenseSnippet),
        plateXOffset = TL.map(_.x) getOrElse (0),
        plateYOffset = TL.map(_.y) getOrElse (0),
        plateTL = TL,
        plateTR = createPoint(plate.getPlateTR),
        plateBL = createPoint(plate.getPlateBL),
        plateBR = createPoint(plate.getPlateBR),
        plateType = createType(plate.getType))
    })

    //overrule platefinder result if simulation is enabled
    result match {
      case Some(x) if x.plateStatus != LicensePlateResponse.PLATESTATUS_PLATE_FOUND && simulationMode.enabled ⇒
        println("platefinder Some result overruled %s".format(msgId))
        Some(x.copy(
          returnStatus = simulationMode.defaultReturnStatus,
          plateStatus = simulationMode.defaultPlateStatus,
          plateNumber = if (x.plateNumber.isEmpty) simulationMode.defaultPlateNumber else x.plateNumber,
          plateConfidence = simulationMode.defaultPlateConfidence,
          plateType = if (x.plateType.isEmpty) simulationMode.defaultPlateType else x.plateType,
          plateCountryCode = if (x.plateCountryCode.isEmpty) simulationMode.defaultPlateCountryCode else x.plateCountryCode))
      case None if simulationMode.enabled ⇒
        println("platefinder None result overruled %s".format(msgId))
        Some(LicensePlateResponse(recognizer = Recognizer.ICR_PLATEFINDER,
          returnStatus = simulationMode.defaultReturnStatus,
          plateStatus = simulationMode.defaultPlateStatus,
          plateNumber = simulationMode.defaultPlateNumber,
          rawPlateNumber = simulationMode.defaultPlateNumber,
          plateConfidence = simulationMode.defaultPlateConfidence,
          plateCountryCode = simulationMode.defaultPlateCountryCode,
          plateCountryConfidence = 0,
          licenseSnippetFileName = "",
          plateXOffset = 0,
          plateYOffset = 0,
          plateTL = None,
          plateTR = None,
          plateBL = None,
          plateBR = None,
          plateType = simulationMode.defaultPlateType))
      case x ⇒ x
    }
  }

  private def createString(str: String): String = {
    if (str != null && str.size > 0) {
      str
    } else {
      ""
    }
  }
  private def createType(plateType: Int): Option[String] = {
    plateType match {
      case LicenseData.TYPE_NORMAL ⇒ Some(LicensePlateResponse.PLATE_TYPE_NORMAL)
      case LicenseData.TYPE_SQUARE ⇒ Some(LicensePlateResponse.PLATE_TYPE_SQUARE)
      case LicenseData.TYPE_SMALL  ⇒ Some(LicensePlateResponse.PLATE_TYPE_SMALL)
      case other                   ⇒ None
    }
  }
  private def createTypeSimulate(plateType: String): Option[String] = {
    plateType match {
      case p if (plateType.equalsIgnoreCase(LicensePlateResponse.PLATE_TYPE_NORMAL)) ⇒ Some(LicensePlateResponse.PLATE_TYPE_NORMAL)
      case p if (plateType.equalsIgnoreCase(LicensePlateResponse.PLATE_TYPE_SQUARE)) ⇒ Some(LicensePlateResponse.PLATE_TYPE_SQUARE)
      case p if (plateType.equalsIgnoreCase(LicensePlateResponse.PLATE_TYPE_SMALL)) ⇒ Some(LicensePlateResponse.PLATE_TYPE_SMALL)
      case other ⇒ None
    }
  }
  def createSimulationMode(options: Map[String, String]): PlateFinderSimulationMode = {
    val log = LoggerFactory.getLogger(FindPlateImage.getClass)
    var simulate = PlateFinderSimulationMode()

    val simulateEnabled = options.get(RecognizeOptions.icrFlow).getOrElse("").equalsIgnoreCase(RecognizeOptions.icrFlow_SIMULATE)
    simulate = simulate.copy(enabled = simulateEnabled)

    options.get(RecognizeOptions.icrSimReturn).foreach(value ⇒ {
      try {
        simulate = simulate.copy(defaultReturnStatus = value.toInt)
      } catch {
        case ex: Exception ⇒ {
          log.error("Value %s for option %s isn't an Integer".format(value, RecognizeOptions.icrSimReturn))
        }
      }
    })
    options.get(RecognizeOptions.icrSimStatus).foreach(value ⇒ {
      try {
        simulate = simulate.copy(defaultPlateStatus = value.toInt)
      } catch {
        case ex: Exception ⇒ {
          log.error("Value %s for option %s isn't an Integer".format(value, RecognizeOptions.icrSimStatus))
        }
      }
    })
    options.get(RecognizeOptions.icrSimPlate).foreach(value ⇒ {
      simulate = simulate.copy(defaultPlateNumber = value)
    })
    options.get(RecognizeOptions.icrSimPlateType).foreach(value ⇒ {
      simulate = simulate.copy(defaultPlateType = createTypeSimulate(value))
    })
    options.get(RecognizeOptions.icrSimPlateConf).foreach(value ⇒ {
      try {
        simulate = simulate.copy(defaultPlateConfidence = value.toInt)
      } catch {
        case ex: Exception ⇒ {
          log.error("Value %s for option %s isn't an Integer".format(value, RecognizeOptions.icrSimPlateConf))
        }
      }
    })
    options.get(RecognizeOptions.icrSimCountry).foreach(value ⇒ {
      simulate = simulate.copy(defaultPlateCountryCode = value)
    })
    simulate
  }

  private def createPoint(point: Point): Option[ImagePoint] = {
    if (point != null) {
      Some(ImagePoint(point.getX, point.getY))
    } else {
      None
    }
  }

}