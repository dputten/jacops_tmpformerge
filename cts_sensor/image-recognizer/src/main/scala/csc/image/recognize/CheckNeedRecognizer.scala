/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.image.recognize

import akka.actor.{ ActorLogging, ActorRef, Actor }
import imagefunctions.LicensePlate

class CheckNeedRecognizer(recognizer: ActorRef) extends Actor with ActorLogging {
  def receive = {
    case msg: RecognizeImageProcessMessage ⇒ {
      val options = msg.request.options
      val minimalPlateConfidence = RecognizeOptions.secondCheckMinimalPlateConfidence(options)
      val minimalCountryConfidence = RecognizeOptions.secondCheckMinimalCountryConfidence(options)
      val responses = msg.responses
      if (responses.isEmpty) {
        log.info("%s: Couldn't find a plate result => do extra recognition step".format(msg.request.msgId))
        recognizer ! msg
      } else {
        val (recognizerName, result) = responses.head
        result.plate match {
          case Some(plate) if (plate.hasPlateResult &&
            plate.plateConfidence >= minimalPlateConfidence &&
            plate.plateCountryConfidence >= minimalCountryConfidence) ⇒ {
            log.info("%s: First recognizer %s has found a reliable plate => Send response".format(msg.request.msgId, recognizerName))
            msg.requester ! result
          }
          case Some(plate) ⇒ {
            //second recognizer can only be done when the first didn't found a plate And country OR
            // when the country is NL
            if (((plate.plateNumber.isEmpty || plate.plateNumber == LicensePlate.PLATE_UNKNOWN) &&
              (plate.plateCountryCode.isEmpty || plate.plateCountryCode == LicensePlate.PLATE_UNKNOWN)) ||
              (plate.plateCountryCode == LicensePlate.PLATE_NETHERLANDS)) {
              log.info("%s: First recognizer %s couldn't find a plate => do extra recognition step".format(msg.request.msgId, recognizerName))
              recognizer ! msg
            } else {
              log.info("%s: First recognizer %s couldn't find a reliable plate, countryCode: %s => Send response".format(msg.request.msgId, recognizerName, plate.plateCountryCode))
              msg.requester ! result
            }
          }
          case None ⇒ {
            log.info("%s: First recognizer %s couldn't find a plate => do extra recognition step".format(msg.request.msgId, recognizerName))
            recognizer ! msg
          }
        }
      }
    }
  }
}