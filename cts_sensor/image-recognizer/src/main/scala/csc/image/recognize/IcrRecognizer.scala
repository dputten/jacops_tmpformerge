package csc.image.recognize

import java.io.File

import csc.akka.logging.DirectLogging
import csc.vehicle.message.ImageFormat
import imagefunctions.LicensePlate
import imagefunctions.pocicr.PlateScanMethod
import imagefunctions.pocicr.impl.IcrFunctionsImpl

/**
 * Recognizer that uses ICR Platereader
 *
 * Created by carlos on 26/01/16.
 */
class IcrRecognizer(nativeDebug: Boolean = false) extends Recognizer with DirectLogging {

  val icr = IcrFunctionsImpl.getInstance()

  /**
   * Do the actual recognition of the license plate.
   * @param imageFile the images used to recognize the license from.
   * @param format indicates the type of the image
   */
  override def recognizeImage(imageFile: File, format: ImageFormat.Value, options: Map[String, String]): LicensePlate = {

    val method = scanMethod(imageFile)
    val plate = icr.jpg2Icr(imageFile.getAbsolutePath, method, nativeDebug, false, false)
    plate.setLicenseSnippetFileName("")
    plate.setRecognizer(getName())
    plate.setPlateCountryConfidence(plate.getPlateConfidence) //country confidence same as plate, always

    plate
  }

  def scanMethod(imageFile: File): PlateScanMethod = {
    val filename = imageFile.getName
    val noExtension = filename.substring(0, filename.lastIndexOf('.'))
    if (noExtension.endsWith("_R")) PlateScanMethod.RIGHT
    else if (noExtension.endsWith("_L")) PlateScanMethod.LEFT
    else {
      log.info("Cannot infer scan area from filename {}. Using 'Dynamic' scan method", filename)
      PlateScanMethod.DYNAMIC
    }
  }

  override def stop(): Unit = ()

  /**
   * get the name of the Recognizer.
   */
  override def getName(): String = Recognizer.ICR_PLATEFINDER

  /**
   * Get the performance points used in the logging.
   */
  override def getPerformancePoints(): (Int, Int) = {
    (33, 34) //todo csilva ???
  }
}
