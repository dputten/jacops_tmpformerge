/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.image.recognize

import imagefunctions.icr.impl.IcrFunctionsImpl
import imagefunctions.ImageFunctionsException
import csc.vehicle.message.ImageFormat
import java.io.File
import imagefunctions.icr.{ IcrFunctions, LicenseData }
import sun.reflect.generics.reflectiveObjects.NotImplementedException

object PlateFinder extends PlateFinder {

  lazy val imageFunction: IcrFunctions = {
    //ImageFunctionsFactory.getIcrFunctions //this cannot be used as it causes issues with POCICR
    IcrFunctionsImpl.getInstance() //ugly but efficient and truly lazy
  }

}

trait PlateFinder {

  def imageFunction: IcrFunctions

  def recognizeImage(imageFile: File, format: ImageFormat.Value, options: Map[String, String]): LicenseData = {

    //get the ARH supported options
    val strOpt = options.get(RecognizeOptions.icrCreateLicenseSnippet)
    val createSnippet = strOpt.map(_.toLowerCase == "true").getOrElse(false)
    val snippetFile = if (createSnippet) {
      imageFile.getAbsolutePath + options.get(RecognizeOptions.icrPostFixLicenseSnippet).getOrElse("_plate_icr.tif")
    } else {
      null
    }
    try {
      format match {
        case ImageFormat.TIF_BAYER ⇒ throw new NotImplementedException()
        case ImageFormat.TIF_RGB   ⇒ throw new NotImplementedException()
        case ImageFormat.TIF_GRAY  ⇒ throw new NotImplementedException()
        case ImageFormat.JPG_RGB   ⇒ imageFunction.jpg2Icr(imageFile.getAbsolutePath, snippetFile, createOptions(options))
        case ImageFormat.JPG_GRAY  ⇒ imageFunction.jpg2Icr(imageFile.getAbsolutePath, snippetFile, createOptions(options))
        case _ ⇒ {
          val plate = new LicenseData()
          plate.setConfidence(0)
          plate.setCountry("")
          plate.setPlate("")
          plate.setStatus(LicenseData.STATUS_NO_PLATE_FOUND)
          plate.setType(LicenseData.TYPE_UNKNOWN)
          plate.setLicenseSnippet("")
          plate
        }
      }
    } catch {
      case ex: ImageFunctionsException ⇒ {
        val plate = new LicenseData()
        plate.setReturnStatus(ex.getStatus)
        plate.setConfidence(0)
        plate.setCountry("")
        plate.setPlate("")
        plate.setStatus(LicenseData.STATUS_NO_PLATE_FOUND)
        plate.setType(LicenseData.TYPE_UNKNOWN)
        plate.setLicenseSnippet("")
        plate

      }
    }
  }

  def createOptions(options: Map[String, String]): java.util.HashMap[String, String] = {
    val optList = new java.util.HashMap[String, String]()
    options.foreach {
      case (RecognizeOptions.icrDebug, value) ⇒ optList.put(IcrFunctions.OPTION_DEBUG, value)
      case (RecognizeOptions.icrFitNum, value) ⇒ optList.put(IcrFunctions.OPTION_FITNUM, value)
      case (RecognizeOptions.icrCameraType, value) ⇒ optList.put(IcrFunctions.OPTION_CAMERA_TYPE, value)
      case (RecognizeOptions.icrImageRotate, value) ⇒ optList.put(IcrFunctions.OPTION_IMAGE_ROTATE, value)
      case (RecognizeOptions.icrImageConversion, value) ⇒ optList.put(IcrFunctions.OPTION_IMAGE_SHARPEN, value)
      case (RecognizeOptions.icrImageSharpen, value) ⇒ optList.put(IcrFunctions.OPTION_IMAGE_CONVERSION_METHOD, value)
      case _ ⇒ //noop
    }
    optList
  }
}
