/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.image.recognize

import java.io.File

import base.{ FavorCriticalProcess, SectionGuard, Timing }
import imagefunctions.{ ImageFunctionsException, LicensePlate, Point }
import akka.actor._
import akka.event.LoggingReceive
import csc.amqp.adapter.{ Ping, Pong }

/**
 * Recognize Image.
 * Makes use of a worker to actually do the recognition, and not keep PingRequest messages waiting in the mailbox.
 * @param recognizer The recognizer to be used.
 * @param sectionGuard The sectionGuard.
 */
class RecognizeImage(recognizer: Recognizer, sectionGuard: Option[FavorCriticalProcess], nextStep: Option[ActorRef]) extends Actor with Timing with ActorLogging {
  require(recognizer != null, "missing recognizer")

  val worker = context.actorOf(Props(new RecognizeImageWorker(recognizeWithLogging, self)))

  private var workerAlive = true
  context.watch(worker)

  override def postStop() {
    recognizer.stop()
  }

  override def preRestart(reason: Throwable, message: Option[Any]) {
    super.preRestart(reason, message)
    log.error(reason, "Restart RecognizeImage %s due to %s processing %s".format(recognizer.getName(), reason.getMessage, message))
  }

  def receive = LoggingReceive {
    case Terminated(actor) ⇒ if (actor == worker) workerAlive = false
    case RecognitionOutcome(client, request, response) ⇒ request match {
      case msg: RecognizeImageRequest ⇒ nextStep match {
        case Some(actor) ⇒ actor ! RecognizeImageProcessMessage(
          requester = client,
          request = msg,
          responses = Map(recognizer.getName() -> response))
        case None ⇒ client ! response
      }
      case msg: RecognizeImageProcessMessage ⇒ nextStep match {
        case Some(actor) ⇒ {
          val responses = msg.responses + (recognizer.getName() -> response)
          actor ! msg.copy(responses = responses)
        }
        case None ⇒ client ! response
      }
    }
    case msg: RecognizeImageRequest        ⇒ worker forward msg
    case msg: RecognizeImageProcessMessage ⇒ worker forward msg
    case Ping ⇒ workerAlive match {
      case true  ⇒ sender ! Pong
      case false ⇒ log.warning("Worker no longer alive. Not responding to Ping")
    }
  }

  private def recognizeWithLogging(msg: RecognizeImageRequest): RecognizeImageResponse = {
    var response: RecognizeImageResponse = null
    val perfPoints = recognizer.getPerformancePoints
    performanceLogInterval(log, perfPoints._1, perfPoints._2, "Recognizer " + recognizer.getName, msg.msgId,
      {
        response = recognize(msg)
      })
    response
  }

  private def recognize(msg: RecognizeImageRequest): RecognizeImageResponse = {
    // return the images for the first candidate type that returns a non-empty list.
    val file = new File(msg.imageFile)
    var plateResult: Option[LicensePlate] = None
    def recognizeLicense() { plateResult = Option(recognizer.recognizeImage(file, msg.imageFormat, msg.options)) }

    try {
      SectionGuard.process(sectionGuard, msg.priority, recognizeLicense())
    } catch {
      case e: ImageFunctionsException ⇒ {
        log.error(recognizer.getName + " returned " + e.getMessage)
      }
    }
    if (log.isInfoEnabled) {
      plateResult match {
        case Some(licenseResult) ⇒ {
          log.info("Recognizer: eventId=[%s], %s recognizer found license plate in file %s, license=[%s][%s] license conf=[%d] country=[%s] country conf=[%d]".format(
            msg.msgId,
            recognizer.getName,
            file,
            licenseResult.getPlateNumber, licenseResult.getRawPlateNumber(), licenseResult.getPlateConfidence,
            licenseResult.getPlateCountryCode, licenseResult.getPlateCountryConfidence))
        }
        case None ⇒ {
          log.info("Recognizer: eventId=[%s], %s recognizer could not find license plate in file %s.".format(msg.msgId, recognizer.getName, file))
        }
      }
    }
    RecognizeImageResponse(msgId = msg.msgId, imageFile = msg.imageFile,
      plate = TranslatePlate.createLicensePlateResult(plateResult), time = System.currentTimeMillis())
  }

}

case class RecognitionOutcome(client: ActorRef, request: Any, response: RecognizeImageResponse)

class RecognizeImageWorker(execute: RecognizeImageRequest ⇒ RecognizeImageResponse, finalizer: ActorRef) extends Actor {
  def receive = {
    case msg: RecognizeImageRequest ⇒
      val client = sender
      val response = execute(msg)
      finalizer ! RecognitionOutcome(client, msg, response)
    case msg: RecognizeImageProcessMessage ⇒
      val client = sender
      val response = execute(msg.request)
      finalizer ! RecognitionOutcome(client, msg, response)
  }
}

object TranslatePlate {
  def createLicensePlateResult(licensePlate: Option[LicensePlate]): Option[LicensePlateResponse] = {
    licensePlate.map(plate ⇒ {
      val raw = if (plate.getRawPlateNumber != null) {
        plate.getRawPlateNumber
      } else {
        ""
      }
      LicensePlateResponse(recognizer = plate.getRecognizer,
        returnStatus = plate.getReturnStatus,
        plateStatus = plate.getPlateStatus,
        plateNumber = plate.getPlateNumber,
        rawPlateNumber = raw,
        plateConfidence = plate.getPlateConfidence,
        plateCountryCode = plate.getPlateCountryCode,
        plateCountryConfidence = plate.getPlateCountryConfidence,
        licenseSnippetFileName = plate.getLicenseSnippetFileName,
        plateXOffset = plate.getPlateXOffset,
        plateYOffset = plate.getPlateYOffset,
        plateTL = createPoint(plate.getPlateTL),
        plateTR = createPoint(plate.getPlateTR),
        plateBL = createPoint(plate.getPlateBL),
        plateBR = createPoint(plate.getPlateBR))
    })
  }
  private def createPoint(point: Point): Option[ImagePoint] = {
    if (point != null) {
      Some(ImagePoint(point.getX, point.getY))
    } else {
      None
    }
  }

}