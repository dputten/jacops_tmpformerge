/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.image.recognize

import base.FavorCriticalProcess
import csc.akka.logging.DirectLogging
import org.scalatest._
import akka.actor.{ ActorRef, ActorSystem }
import java.util.Date

import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import csc.vehicle.message.{ ImageFormat, Priority }
import imagefunctions.icr.{ IcrFunctions, LicenseData }
import java.util

import imagefunctions.ImageFunctionsException

import scala.concurrent.duration._

class FindPlateImageTest extends TestKit(ActorSystem("FindPlateImageTest"))
  with WordSpecLike
  with DirectLogging
  with MustMatchers
  with BeforeAndAfterEach
  with BeforeAndAfterAll {

  val triggerTime = new Date()
  val dummyTime: Long = 0
  var mockICR: MockIrcFunctions = null
  var probe: TestProbe = null

  override def afterAll() {
    system.shutdown
  }

  override protected def beforeEach(): Unit = {
    probe = TestProbe()
    mockICR = new MockIrcFunctions()
  }

  def createActor(sectionGuard: Option[FavorCriticalProcess] = None,
                  nextStep: Option[ActorRef] = None): FindPlateImage =
    new FindPlateImage(sectionGuard, nextStep, mockICR) {
      override def now: Long = dummyTime //making sure response timestamps are fixed
    }

  /**
   * This test uses the ImageFunctionsTestImpl which is a mock of the ImageFunctionsTestImpl ImageFunctions implementation.
   * The mock is setup to behave correctly (a good scenario)
   */
  "RecognizeImage" must {

    "Process good scenario without nextstep" in {
      val recognizeImageRef = TestActorRef(createActor())

      //create message
      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.JPG_RGB, Map())
      probe.send(recognizeImageRef, msg)

      val plate = FindPlateImage.createLicensePlateResult(Some(mockICR.createData()))
      val expectedMsg = RecognizeImageResponse(msg.msgId, msg.imageFile, plate, dummyTime)
      val result = probe.expectMsg(500 millis, expectedMsg)
    }
    "Process good scenario with nextstep" in {
      val nextStep = TestProbe()
      val recognizeImageRef = TestActorRef(createActor(nextStep = Some(nextStep.ref)))

      //create message
      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.JPG_RGB, Map())
      probe.send(recognizeImageRef, msg)

      val plate = FindPlateImage.createLicensePlateResult(Some(mockICR.createData()))
      val expectedResponse = RecognizeImageResponse(msg.msgId, msg.imageFile, plate, dummyTime)

      val expectedMsg = RecognizeImageProcessMessage(probe.ref, msg, Map(Recognizer.ICR_PLATEFINDER -> expectedResponse))
      val result = nextStep.expectMsg(500 millis, expectedMsg)
    }
    "Process good scenario with guard" in {
      val recognizeImageRef = TestActorRef(createActor(sectionGuard = Option(new FavorCriticalProcess(1))))

      //create message
      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.JPG_RGB, Map())
      probe.send(recognizeImageRef, msg)

      val plate = FindPlateImage.createLicensePlateResult(Some(mockICR.createData()))
      val expectedMsg = RecognizeImageResponse(msg.msgId, msg.imageFile, plate, dummyTime)
      val result = probe.expectMsg(500 millis, expectedMsg)
    }
    "Process Exception" in {
      mockICR.exception = Some(new ImageFunctionsException(-2, "failed Image read"))
      val recognizeImageRef = TestActorRef(createActor(sectionGuard = Option(new FavorCriticalProcess(1))))

      //create message
      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.JPG_RGB, Map())
      probe.send(recognizeImageRef, msg)

      val data = new LicenseData()
      data.setReturnStatus(-2)
      data.setStatus(LicenseData.STATUS_NO_PLATE_FOUND)
      val plate = FindPlateImage.createLicensePlateResult(Some(data))
      val expectedMsg = RecognizeImageResponse(msg.msgId, msg.imageFile, plate, dummyTime)
      val result = probe.expectMsg(500 millis, expectedMsg)
    }
    "Process empty result" in {
      mockICR.plate = ""
      val recognizeImageRef = TestActorRef(createActor())
      //create message
      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.JPG_RGB, Map())

      probe.send(recognizeImageRef, msg)
      //test if message are received
      val plate = FindPlateImage.createLicensePlateResult(Some(mockICR.createData()))
      val expectedMsg = RecognizeImageResponse(msg.msgId, msg.imageFile, plate, dummyTime)
      val result = probe.expectMsg(500 millis, expectedMsg)
    }
    "Process plate not found" in {
      mockICR.status = LicenseData.STATUS_NO_PLATE_FOUND
      mockICR.plate = ""
      val recognizeImageRef = TestActorRef(createActor())
      //create message

      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.JPG_RGB, Map())

      probe.send(recognizeImageRef, msg)
      //test if message are received
      val plate = FindPlateImage.createLicensePlateResult(Some(mockICR.createData()))
      val expectedMsg = RecognizeImageResponse(msg.msgId, msg.imageFile, plate, dummyTime)
      val result = probe.expectMsg(500 millis, expectedMsg)
    }
    "Process Simulate when plate not found" in {
      mockICR.status = LicenseData.STATUS_NO_PLATE_FOUND
      mockICR.plate = ""
      mockICR.plateType = 0
      val probe = TestProbe()
      val recognizeImageRef = TestActorRef(createActor())
      //create message
      val options = Map(RecognizeOptions.icrFlow -> RecognizeOptions.icrFlow_SIMULATE)

      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.JPG_RGB, options)

      probe.send(recognizeImageRef, msg)
      //test if message are received
      val simulationMode = PlateFinderSimulationMode()
      val plate = Some(LicensePlateResponse(recognizer = Recognizer.ICR_PLATEFINDER,
        returnStatus = simulationMode.defaultReturnStatus,
        plateStatus = simulationMode.defaultPlateStatus,
        plateNumber = simulationMode.defaultPlateNumber,
        rawPlateNumber = simulationMode.defaultPlateNumber,
        plateConfidence = simulationMode.defaultPlateConfidence,
        plateCountryCode = simulationMode.defaultPlateCountryCode,
        plateCountryConfidence = 0,
        licenseSnippetFileName = "",
        plateXOffset = 0,
        plateYOffset = 0,
        plateTL = None,
        plateTR = None,
        plateBL = None,
        plateBR = None,
        plateType = simulationMode.defaultPlateType))

      val expectedMsg = RecognizeImageResponse(msg.msgId, msg.imageFile, plate, dummyTime)
      val result = probe.expectMsg(500 millis, expectedMsg)
    }
  }
}

class MockIrcFunctions extends PlateFinder with IcrFunctions {

  override def imageFunction: IcrFunctions = this

  var returnStatus = 0
  var status = LicenseData.STATUS_PLATE_FOUND
  var plate = "12ABCD"
  var plateType = LicenseData.TYPE_NORMAL
  var exception: Option[ImageFunctionsException] = None

  def reset() {
    returnStatus = 0
    status = LicenseData.STATUS_PLATE_FOUND
    plate = "12ABCD"
    plateType = LicenseData.TYPE_NORMAL
    exception = None
  }

  def jpg2Icr(fileName: String, snippetName: String, options: util.HashMap[String, String]): LicenseData = {
    exception match {
      case Some(ex) ⇒ throw ex
      case None     ⇒ createData()
    }
  }
  def createData(): LicenseData = {
    val data = new LicenseData()
    data.setReturnStatus(returnStatus)
    data.setStatus(status)
    data.setPlate(plate)
    data.setType(plateType)
    data
  }

}