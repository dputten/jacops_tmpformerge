/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.image.recognize

import java.io.File
import base._
import imagefunctions.{ ImageFunctionsException, LicensePlate, Point }
import csc.vehicle.message._

import scala.concurrent.duration._
import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import org.scalatest.BeforeAndAfterAll
import csc.akka.logging.DirectLogging
import java.util.Date

import org.scalatest._
import csc.amqp.adapter.{ Ping, Pong }

/**
 * Test the RecognizeImage class
 */

class RecognizeImageTest extends TestKit(ActorSystem("RecognizeImageTest"))
  with WordSpecLike with DirectLogging with MustMatchers with BeforeAndAfterAll {

  val triggerTime = new Date()

  override def afterAll() {
    system.shutdown
  }
  /**
   * This test uses the ImageFunctionsTestImpl which is a mock of the ImageFunctionsTestImpl ImageFunctions implementation.
   * The mock is setup to behave correctly (a good scenario)
   */
  "RecognizeImage" must {

    "Process good scenario" in {
      val probe = TestProbe()
      val recognizer = new TestRecognizer()
      val recognizeImageRef = TestActorRef(new RecognizeImage(
        recognizer = recognizer,
        sectionGuard = None,
        nextStep = None))

      //create message
      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, Map())
      probe.send(recognizeImageRef, msg)

      val plate = TranslatePlate.createLicensePlateResult(Some(recognizer.recognizeImage(imageFile = new File("TestFile.tif"), format = ImageFormat.TIF_BAYER, options = Map())))

      probe.expectMsgType[RecognizeImageResponse](500 millis) match {
        case RecognizeImageResponse(msg.msgId, msg.imageFile, plate, _) ⇒ //success
      }
    }
    "Process good scenario with guard" in {
      val probe = TestProbe()
      val recognizer = new TestRecognizer()
      val recognizeImageRef = TestActorRef(new RecognizeImage(
        recognizer = recognizer,
        sectionGuard = Option(new FavorCriticalProcess(1)),
        nextStep = None))

      //create message
      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, Map())
      probe.send(recognizeImageRef, msg)

      val plate = TranslatePlate.createLicensePlateResult(Some(recognizer.recognizeImage(imageFile = new File("TestFile.tif"), format = ImageFormat.TIF_BAYER, options = Map())))
      probe.expectMsgType[RecognizeImageResponse](500 millis) match {
        case RecognizeImageResponse(msg.msgId, msg.imageFile, plate, _) ⇒ //success
      }
    }
    "Process Exception" in {
      val probe = TestProbe()
      val recognizer = new TestRecognizer()
      recognizer.simulateErrors = true
      val recognizeImageRef = TestActorRef(new RecognizeImage(
        recognizer = recognizer,
        sectionGuard = Option(new FavorCriticalProcess(1)),
        nextStep = None))

      //create message
      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, Map())
      probe.send(recognizeImageRef, msg)

      probe.expectMsgType[RecognizeImageResponse](500 millis) match {
        case RecognizeImageResponse(msg.msgId, msg.imageFile, None, _) ⇒ //success
      }
    }
    "Process empty result" in {
      val recognizer = new TestRecognizer
      recognizer.simulateEmpty = true
      val probe = TestProbe()
      val recognizeImageRef = TestActorRef(new RecognizeImage(
        recognizer = recognizer,
        sectionGuard = None,
        nextStep = None))
      //create message
      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, Map())

      probe.send(recognizeImageRef, msg)
      //test if message are received
      val plate = TranslatePlate.createLicensePlateResult(Some(recognizer.recognizeImage(imageFile = new File("TestFile.tif"), format = ImageFormat.TIF_BAYER, options = Map())))
      probe.expectMsgType[RecognizeImageResponse](500 millis) match {
        case RecognizeImageResponse(msg.msgId, msg.imageFile, plate, _) ⇒ //success
      }
    }
    "Process plate not found" in {
      val recog = new TestRecognizer
      recog.simulateEmpty = true
      recog.simulatePlateNotFound = true
      val probe = TestProbe()
      val recognizeImageRef = TestActorRef(new RecognizeImage(
        recognizer = recog,
        sectionGuard = None,
        nextStep = None))
      //create message

      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, Map())

      probe.send(recognizeImageRef, msg)
      //test if message are received
      val plate = TranslatePlate.createLicensePlateResult(Some(recog.recognizeImage(imageFile = new File("TestFile.tif"), format = ImageFormat.TIF_BAYER, options = Map())))
      probe.expectMsgType[RecognizeImageResponse](500 millis) match {
        case RecognizeImageResponse(msg.msgId, msg.imageFile, plate, _) ⇒ //success
      }
    }
    "not block processing RecognizeImageRequest, processing a posterior Ping almost immediately" in {
      val probe = TestProbe()
      val recognizer = new TestRecognizer
      val delay: Long = 2000

      recognizer.simulateDelay = Some(delay)

      val recognizeImageRef = TestActorRef(new RecognizeImage(
        recognizer = recognizer,
        sectionGuard = Option(new FavorCriticalProcess(1)),
        nextStep = None))

      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, Map())

      val time = System.currentTimeMillis()
      probe.send(recognizeImageRef, msg) //slow recognition goes first
      probe.send(recognizeImageRef, Ping) //ping request goes after
      val elapsed = System.currentTimeMillis() - time

      //confirming Pong arrives before the slow RecognizeImageResponse
      val pong = probe.expectMsgType[Any]
      pong must be(Pong)
      probe.expectMsgType[RecognizeImageResponse]
    }
  }

  "RecognizeImage multistep" must {

    "Process good scenario firstStep" in {
      val probe = TestProbe()
      val nextStep = TestProbe()
      val recognizer = new TestRecognizer()
      val recognizeImageRef = TestActorRef(new RecognizeImage(
        recognizer = recognizer,
        sectionGuard = None,
        nextStep = Some(nextStep.ref)))

      //create message
      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, Map())
      probe.send(recognizeImageRef, msg)

      val plate = TranslatePlate.createLicensePlateResult(Some(recognizer.recognizeImage(imageFile = new File("TestFile.tif"), format = ImageFormat.TIF_BAYER, options = Map())))

      val actual = nextStep.expectMsgType[RecognizeImageProcessMessage](500 millis)
      val responseTime = actual.responses(recognizer.getName()).time //aligning the response time

      val expectedMsg = RecognizeImageProcessMessage(
        requester = probe.ref,
        request = msg,
        responses = Map(recognizer.getName() -> RecognizeImageResponse(msg.msgId, msg.imageFile, plate, responseTime)))

      actual must be(expectedMsg)
    }
    "Process good scenario secondStep" in {
      val probe = TestProbe()

      val nextStep = TestProbe()
      val recognizer = new TestRecognizer()
      val recognizeImageRef = TestActorRef(new RecognizeImage(
        recognizer = recognizer,
        sectionGuard = None,
        nextStep = Some(nextStep.ref)))

      //create message
      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, Map())
      val plate = TranslatePlate.createLicensePlateResult(Some(recognizer.recognizeImage(imageFile = new File("TestFile.tif"), format = ImageFormat.TIF_BAYER, options = Map())))
      val processMes = RecognizeImageProcessMessage(
        requester = probe.ref,
        request = msg,
        responses = Map("Step1" -> RecognizeImageResponse(msg.msgId, msg.imageFile, plate)))
      recognizeImageRef ! processMes

      val actual = nextStep.expectMsgType[RecognizeImageProcessMessage](500 millis)
      val responseTime1 = actual.responses("Step1").time //aligning the response time
      val responseTime2 = actual.responses(recognizer.getName()).time //aligning the response time

      val expectedMsg = RecognizeImageProcessMessage(
        requester = probe.ref,
        request = msg,
        responses = Map(
          "Step1" -> RecognizeImageResponse(msg.msgId, msg.imageFile, plate, responseTime1),
          recognizer.getName() -> RecognizeImageResponse(msg.msgId, msg.imageFile, plate, responseTime2)))
      actual must be(expectedMsg)
    }
    "Process Exception" in {
      val probe = TestProbe()
      val nextStep = TestProbe()
      val recognizer = new TestRecognizer()
      recognizer.simulateErrors = true
      val recognizeImageRef = TestActorRef(new RecognizeImage(
        recognizer = recognizer,
        sectionGuard = Option(new FavorCriticalProcess(1)),
        nextStep = Some(nextStep.ref)))

      //create message
      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, Map())
      probe.send(recognizeImageRef, msg)

      val actual = nextStep.expectMsgType[RecognizeImageProcessMessage](500 millis)
      val responseTime = actual.responses(recognizer.getName()).time //aligning the response time

      val expectedMsg = RecognizeImageProcessMessage(
        requester = probe.ref,
        request = msg,
        responses = Map(recognizer.getName() -> RecognizeImageResponse(msg.msgId, msg.imageFile, None, responseTime)))

      actual must be(expectedMsg)
    }
    "Process empty result" in {
      val recognizer = new TestRecognizer
      recognizer.simulateEmpty = true
      val probe = TestProbe()
      val nextStep = TestProbe()
      val recognizeImageRef = TestActorRef(new RecognizeImage(
        recognizer = recognizer,
        sectionGuard = None,
        nextStep = Some(nextStep.ref)))
      //create message
      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, Map())

      probe.send(recognizeImageRef, msg)
      //test if message are received
      val plate = TranslatePlate.createLicensePlateResult(Some(recognizer.recognizeImage(imageFile = new File("TestFile.tif"), format = ImageFormat.TIF_BAYER, options = Map())))

      val actual = nextStep.expectMsgType[RecognizeImageProcessMessage](500 millis)
      val responseTime = actual.responses(recognizer.getName()).time //aligning the response time

      val expectedMsg = RecognizeImageProcessMessage(
        requester = probe.ref,
        request = msg,
        responses = Map(recognizer.getName() -> RecognizeImageResponse(msg.msgId, msg.imageFile, plate, responseTime)))

      actual must be(expectedMsg)
    }
  }

}

/**
 *  The Test implementation of the Recognizer.
 */
class TestRecognizer extends Recognizer {
  var simulateErrors: Boolean = false
  var simulateEmpty: Boolean = false
  var simulatePlateNotFound: Boolean = false
  var simulateDelay: Option[Long] = None

  def cleanErrors {
    simulateErrors = false
    simulateEmpty = false
    simulatePlateNotFound = false
  }

  /**
   * Do the actual recognizion of the licenseplate  .
   * @param imageFile the imges used to recognize the license from.
   */
  def recognizeImage(imageFile: File, format: ImageFormat.Value, options: Map[String, String]): LicensePlate = {
    if (simulateErrors) {
      throw new ImageFunctionsException(1, "Simulated error")
    }

    simulateDelay map { millis ⇒
      Thread.sleep(millis) //simulation recognition with some delay
    }

    val plate = new LicensePlate
    if (simulateEmpty) {
      plate.setLicenseSnippetFileName("")
      plate.setPlateNumber("")
      plate.setRecognizer(LicensePlate.ICR_RECOGNIZER)
      plate.setPlateCountryCode(LicensePlate.PLATE_UNKNOWN)
      plate.setPlateConfidence(0)
      plate.setReturnStatus(0)
      plate.setPlateStatus(getPlateStatus())
      plate.setPlateCountryConfidence(0)
      if (!simulatePlateNotFound) {
        plate.setPlateTL(new Point(100, 100))
        plate.setPlateTR(new Point(400, 100))
        plate.setPlateBL(new Point(100, 400))
        plate.setPlateBR(new Point(400, 400))
      }
    } else {
      plate.setLicenseSnippetFileName("snippet.tif")
      plate.setPlateNumber("12abcd")
      plate.setRecognizer(LicensePlate.ICR_RECOGNIZER)
      plate.setPlateCountryCode(LicensePlate.PLATE_NETHERLANDS)
      plate.setPlateConfidence(90)
      plate.setReturnStatus(0)
      plate.setPlateStatus(getPlateStatus())
      plate.setPlateCountryConfidence(90)
      plate.setPlateTL(new Point(100, 100))
      plate.setPlateTR(new Point(400, 100))
      plate.setPlateBL(new Point(100, 400))
      plate.setPlateBR(new Point(400, 400))
    }
    return plate
  }

  def getPlateStatus(): Short = {
    if (simulatePlateNotFound) {
      LicensePlate.PLATESTATUS_NO_PLATE_FOUND
    } else {
      LicensePlate.PLATESTATUS_PLATE_FOUND
    }
  }

  /**
   *  Get the performance points used in the logging.
   */
  def getPerformancePoints(): Tuple2[Int, Int] = {
    (88, 99)
  }

  /**
   * get the name of the Recognizer.
   */
  def getName(): String = {
    "TEST"
  }

  def stop() {}
}

