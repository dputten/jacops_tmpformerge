/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.image.recognize

import org.scalatest._
import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import csc.vehicle.message.{ ImageFormat, Priority }
import imagefunctions.LicensePlate

class MergeARHIntradaTest extends TestKit(ActorSystem("MergeARHIntradaTest"))
  with WordSpecLike with MustMatchers with BeforeAndAfterAll {

  override def afterAll() {
    system.shutdown
  }

  def createActor(): MergeARHIntrada = new MergeARHIntrada {
    override protected def now: Long = 0 //making sure response time is fixed (for comparison tests)
  }

  "MergeARHIntrada" must {
    "return arh result when no results found" in {
      val probe = TestProbe()
      val actorRef = TestActorRef(createActor())
      val request = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, Map())
      val arhResponse = RecognizeImageResponse(request.msgId, request.imageFile, None, 0)
      val intradaResponse = RecognizeImageResponse(request.msgId, request.imageFile, None, 0)
      val msg = new RecognizeImageProcessMessage(requester = probe.ref, request = request,
        responses = Map(
          Recognizer.ARH -> arhResponse,
          Recognizer.INTRADA -> intradaResponse))
      actorRef ! msg
      probe.expectMsg(arhResponse)

    }
    "return intrada results when only intrada has result with high conf" in {
      val probe = TestProbe()
      val actorRef = TestActorRef(createActor())
      val request = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, Map())
      val arhResponse = RecognizeImageResponse(request.msgId, request.imageFile, None, 0)
      val intradaPlate = createLicensePlateResult(Recognizer.INTRADA).copy(plateConfidence = 80, plateCountryConfidence = 80)
      val intradaResponse = RecognizeImageResponse(request.msgId, request.imageFile, Some(intradaPlate), 0)
      val msg = new RecognizeImageProcessMessage(requester = probe.ref, request = request,
        responses = Map(
          Recognizer.ARH -> arhResponse,
          Recognizer.INTRADA -> intradaResponse))
      actorRef ! msg

      val expectedPlate = intradaPlate.copy(plateConfidence = 65, plateCountryConfidence = 65)
      val expectedResponse = intradaResponse.copy(plate = Some(expectedPlate))
      probe.expectMsg(expectedResponse)

    }
    "return intrada results when only intrada has result with high conf and foreigner" in {
      val probe = TestProbe()
      val actorRef = TestActorRef(createActor())
      val request = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, Map())
      val arhResponse = RecognizeImageResponse(request.msgId, request.imageFile, None, 0)
      val intradaPlate = createLicensePlateResult(Recognizer.INTRADA).copy(plateConfidence = 80, plateCountryConfidence = 80, plateCountryCode = "DE")
      val intradaResponse = RecognizeImageResponse(request.msgId, request.imageFile, Some(intradaPlate), 0)
      val msg = new RecognizeImageProcessMessage(requester = probe.ref, request = request,
        responses = Map(
          Recognizer.ARH -> arhResponse,
          Recognizer.INTRADA -> intradaResponse))
      actorRef ! msg

      val expectedResponse = intradaResponse.copy(plate = None)
      probe.expectMsg(expectedResponse)
    }
    "return empty results when only intrada has result with low conf" in {
      val probe = TestProbe()
      val actorRef = TestActorRef(createActor())
      val request = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, Map())
      val arhResponse = RecognizeImageResponse(request.msgId, request.imageFile, None, 0)
      val intradaPlate = createLicensePlateResult(Recognizer.INTRADA)
      val intradaResponse = RecognizeImageResponse(request.msgId, request.imageFile, Some(intradaPlate), 0)
      val msg = new RecognizeImageProcessMessage(requester = probe.ref, request = request,
        responses = Map(
          Recognizer.ARH -> arhResponse,
          Recognizer.INTRADA -> intradaResponse))
      actorRef ! msg

      val expectedResponse = intradaResponse.copy(plate = None)
      probe.expectMsg(expectedResponse)

    }
    "return arh result when only arh has result" in {
      val probe = TestProbe()
      val actorRef = TestActorRef(createActor())
      val request = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, Map())
      val arhPlate = createLicensePlateResult(Recognizer.ARH).copy(plateNumber = "")
      val arhResponse = RecognizeImageResponse(request.msgId, request.imageFile, Some(arhPlate), 0)
      val intradaResponse = RecognizeImageResponse(request.msgId, request.imageFile, None, 0)
      val msg = new RecognizeImageProcessMessage(requester = probe.ref, request = request,
        responses = Map(
          Recognizer.ARH -> arhResponse,
          Recognizer.INTRADA -> intradaResponse))
      actorRef ! msg
      probe.expectMsg(arhResponse)
    }
    "return arh result when two results and both are empty" in {
      val probe = TestProbe()
      val actorRef = TestActorRef(createActor())
      val request = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, Map())
      val arhPlate = createLicensePlateResult(Recognizer.ARH).copy(plateNumber = "")
      val arhResponse = RecognizeImageResponse(request.msgId, request.imageFile, Some(arhPlate), 0)
      val intradaPlate = createLicensePlateResult(Recognizer.INTRADA).copy(plateNumber = "")
      val intradaResponse = RecognizeImageResponse(request.msgId, request.imageFile, Some(intradaPlate), 0)
      val msg = new RecognizeImageProcessMessage(requester = probe.ref, request = request,
        responses = Map(
          Recognizer.ARH -> arhResponse,
          Recognizer.INTRADA -> intradaResponse))
      actorRef ! msg
      val expectedResponse = intradaResponse.copy(plate = Some(arhPlate))
      probe.expectMsg(expectedResponse)
    }
    "return arh result when two results and intrada is empty" in {
      val probe = TestProbe()
      val actorRef = TestActorRef(createActor())
      val request = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, Map())
      val arhPlate = createLicensePlateResult(Recognizer.ARH)
      val arhResponse = RecognizeImageResponse(request.msgId, request.imageFile, Some(arhPlate), 0)
      val intradaPlate = createLicensePlateResult(Recognizer.INTRADA).copy(plateNumber = "")
      val intradaResponse = RecognizeImageResponse(request.msgId, request.imageFile, Some(intradaPlate), 0)
      val msg = new RecognizeImageProcessMessage(requester = probe.ref, request = request,
        responses = Map(
          Recognizer.ARH -> arhResponse,
          Recognizer.INTRADA -> intradaResponse))
      actorRef ! msg
      probe.expectMsg(arhResponse)
    }
    "return intrada result when two results and arh is empty" in {
      val probe = TestProbe()
      val actorRef = TestActorRef(createActor())
      val request = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, Map())
      val arhPlate = createLicensePlateResult(Recognizer.ARH).copy(plateNumber = "")
      val arhResponse = RecognizeImageResponse(request.msgId, request.imageFile, Some(arhPlate), 0)
      val intradaPlate = createLicensePlateResult(Recognizer.INTRADA)
      val intradaResponse = RecognizeImageResponse(request.msgId, request.imageFile, Some(intradaPlate), 0)
      val msg = new RecognizeImageProcessMessage(requester = probe.ref, request = request,
        responses = Map(
          Recognizer.ARH -> arhResponse,
          Recognizer.INTRADA -> intradaResponse))
      actorRef ! msg

      val expectedResponse = intradaResponse.copy(plate = Some(arhPlate))
      probe.expectMsg(expectedResponse)

    }
    "return arh result when two plate results and intrada confidence is low" in {
      val probe = TestProbe()
      val actorRef = TestActorRef(createActor())
      val request = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, Map())
      val arhPlate = createLicensePlateResult(Recognizer.ARH)
      val arhResponse = RecognizeImageResponse(request.msgId, request.imageFile, Some(arhPlate), 0)
      val intradaPlate = createLicensePlateResult(Recognizer.INTRADA)
      val intradaResponse = RecognizeImageResponse(request.msgId, request.imageFile, Some(intradaPlate), 0)
      val msg = new RecognizeImageProcessMessage(requester = probe.ref, request = request,
        responses = Map(
          Recognizer.ARH -> arhResponse,
          Recognizer.INTRADA -> intradaResponse))
      actorRef ! msg
      probe.expectMsg(arhResponse)
    }
    "return intrada result when two plate results and intrada confidence is high" in {
      val probe = TestProbe()
      val actorRef = TestActorRef(createActor())
      val request = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, Map())
      val arhPlate = createLicensePlateResult(Recognizer.ARH)
      val arhResponse = RecognizeImageResponse(request.msgId, request.imageFile, Some(arhPlate), 0)
      val intradaPlate = createLicensePlateResult(Recognizer.INTRADA).copy(plateConfidence = 80,
        plateCountryConfidence = 80)
      val intradaResponse = RecognizeImageResponse(request.msgId, request.imageFile, Some(intradaPlate), 0)
      val msg = new RecognizeImageProcessMessage(requester = probe.ref, request = request,
        responses = Map(
          Recognizer.ARH -> arhResponse,
          Recognizer.INTRADA -> intradaResponse))
      actorRef ! msg

      val expectedPlate = intradaPlate.copy(plateConfidence = 65, plateCountryConfidence = 65)
      val expectedResponse = intradaResponse.copy(plate = Some(expectedPlate))
      probe.expectMsg(expectedResponse)
    }
    "return arh result when two plate results with country DE" in {
      val probe = TestProbe()
      val actorRef = TestActorRef(createActor())
      val request = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, Map())
      val arhPlate = createLicensePlateResult(Recognizer.ARH).copy(plateCountryCode = LicensePlate.PLATE_GERMANY)
      val arhResponse = RecognizeImageResponse(request.msgId, request.imageFile, Some(arhPlate), 0)
      val intradaPlate = createLicensePlateResult(Recognizer.INTRADA).copy(
        plateCountryCode = LicensePlate.PLATE_GERMANY,
        plateConfidence = 80)
      val intradaResponse = RecognizeImageResponse(request.msgId, request.imageFile, Some(intradaPlate), 0)
      val msg = new RecognizeImageProcessMessage(requester = probe.ref, request = request,
        responses = Map(
          Recognizer.ARH -> arhResponse,
          Recognizer.INTRADA -> intradaResponse))
      actorRef ! msg
      probe.expectMsg(arhResponse)
    }
    "return arh result when two plate results and intrada confidence is high but different with arh" in {
      val probe = TestProbe()
      val actorRef = TestActorRef(createActor())
      val request = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, Map())
      val arhPlate = createLicensePlateResult(Recognizer.ARH)
      val arhResponse = RecognizeImageResponse(request.msgId, request.imageFile, Some(arhPlate), 0)
      val intradaPlate = createLicensePlateResult(Recognizer.INTRADA).copy(
        plateNumber = "MKL1234",
        plateConfidence = 80)
      val intradaResponse = RecognizeImageResponse(request.msgId, request.imageFile, Some(intradaPlate), 0)
      val msg = new RecognizeImageProcessMessage(requester = probe.ref, request = request,
        responses = Map(
          Recognizer.ARH -> arhResponse,
          Recognizer.INTRADA -> intradaResponse))
      actorRef ! msg
      probe.expectMsg(arhResponse)
    }
    "return platefinder result when two plate results has no plate found" in {
      val probe = TestProbe()
      val actorRef = TestActorRef(createActor())
      val request = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, Map())
      val arhPlate = createLicensePlateResult(Recognizer.ARH).copy(plateStatus = LicensePlateResponse.PLATESTATUS_NO_PLATE_FOUND)
      val arhResponse = RecognizeImageResponse(request.msgId, request.imageFile, Some(arhPlate), 0)
      val intradaPlate = createLicensePlateResult(Recognizer.INTRADA).copy(plateStatus = LicensePlateResponse.PLATESTATUS_NO_PLATE_FOUND)
      val intradaResponse = RecognizeImageResponse(request.msgId, request.imageFile, Some(intradaPlate), 0)
      val icrPlate = createLicensePlateResult(Recognizer.ICR_PLATEFINDER).copy()
      val icrResponse = RecognizeImageResponse(request.msgId, request.imageFile, Some(icrPlate), 0)

      val msg = new RecognizeImageProcessMessage(requester = probe.ref, request = request,
        responses = Map(
          Recognizer.ARH -> arhResponse,
          Recognizer.INTRADA -> intradaResponse,
          Recognizer.ICR_PLATEFINDER -> icrResponse))
      actorRef ! msg
      probe.expectMsg(icrResponse)
    }
    "return None result when all plate results has no plate found" in {
      val probe = TestProbe()
      val actorRef = TestActorRef(createActor())
      val request = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, Map())
      val arhPlate = createLicensePlateResult(Recognizer.ARH).copy(plateStatus = LicensePlateResponse.PLATESTATUS_NO_PLATE_FOUND)
      val arhResponse = RecognizeImageResponse(request.msgId, request.imageFile, Some(arhPlate), 0)
      val intradaPlate = createLicensePlateResult(Recognizer.INTRADA).copy(plateStatus = LicensePlateResponse.PLATESTATUS_NO_PLATE_FOUND)
      val intradaResponse = RecognizeImageResponse(request.msgId, request.imageFile, Some(intradaPlate), 0)
      val icrPlate = createLicensePlateResult(Recognizer.ICR_PLATEFINDER).copy(plateType = Some(LicensePlateResponse.PLATE_TYPE_NORMAL))
      val icrResponse = RecognizeImageResponse(request.msgId, request.imageFile, Some(icrPlate), 0)

      val msg = new RecognizeImageProcessMessage(requester = probe.ref, request = request,
        responses = Map(
          Recognizer.ARH -> arhResponse,
          Recognizer.INTRADA -> intradaResponse,
          Recognizer.ICR_PLATEFINDER -> icrResponse))
      actorRef ! msg
      probe.expectMsg(icrResponse)
    }
  }

  def createLicensePlateResult(recognizer: String): LicensePlateResponse = {
    LicensePlateResponse(recognizer = recognizer,
      returnStatus = 0,
      plateStatus = LicensePlateResponse.PLATESTATUS_PLATE_FOUND,
      plateNumber = "12XYZ3",
      rawPlateNumber = "12_XYZ_3",
      plateConfidence = 70,
      plateCountryCode = LicensePlate.PLATE_NETHERLANDS,
      plateCountryConfidence = 70,
      licenseSnippetFileName = "LicenseSnippetFileName",
      plateXOffset = 10,
      plateYOffset = 10,
      plateTL = null,
      plateTR = null,
      plateBL = null,
      plateBR = null,
      plateType = None)
  }
}