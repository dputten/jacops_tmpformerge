/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.image.recognize

import org.scalatest._
import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import csc.vehicle.message.{ ImageFormat, Priority }

import scala.concurrent.duration._
import imagefunctions.LicensePlate

class CheckNeedPlateFinderTest extends TestKit(ActorSystem("CheckNeedPlateFinderTest"))
  with WordSpecLike with MustMatchers with BeforeAndAfterAll {

  val recognizeOptions = Map(RecognizeOptions.icrFlow -> RecognizeOptions.icrFlow_NORMAL)

  override def afterAll() {
    system.shutdown
  }

  "CheckNeedPlateFinderTest" must {
    "Send reply to createResponse" in {
      val probe = TestProbe()

      val nextStep = TestProbe()
      val recognizer = TestProbe()
      val actorRef = TestActorRef(new CheckNeedPlateFinder(recognizer.ref, nextStep.ref))

      //create message
      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, recognizeOptions)
      val plate = createLicensePlateResult(Recognizer.ARH)
      val arhResponse = RecognizeImageResponse(msg.msgId, msg.imageFile, Some(plate))
      val intradaPlate = createLicensePlateResult(Recognizer.INTRADA).copy(plateStatus = LicensePlateResponse.PLATESTATUS_PLATE_FOUND)
      val intradaResponse = RecognizeImageResponse(msg.msgId, msg.imageFile, Some(intradaPlate))

      val processMsg = RecognizeImageProcessMessage(
        requester = probe.ref,
        request = msg,
        responses = Map(Recognizer.ARH -> arhResponse,
          Recognizer.INTRADA -> intradaResponse))
      actorRef ! processMsg

      nextStep.expectMsg(500 millis, processMsg)
      recognizer.expectNoMsg(100 millis)
    }
    "Send reply to recognizer step when no plate found" in {
      val probe = TestProbe()

      val nextStep = TestProbe()
      val recognizer = TestProbe()
      val actorRef = TestActorRef(new CheckNeedPlateFinder(recognizer.ref, nextStep.ref))

      //create message
      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, recognizeOptions)
      val plate = createLicensePlateResult(Recognizer.ARH)
      val arhResponse = RecognizeImageResponse(msg.msgId, msg.imageFile, Some(plate))
      val intradaPlate = createLicensePlateResult(Recognizer.INTRADA)
      val intradaResponse = RecognizeImageResponse(msg.msgId, msg.imageFile, Some(intradaPlate))

      val processMsg = RecognizeImageProcessMessage(
        requester = probe.ref,
        request = msg,
        responses = Map(Recognizer.ARH -> arhResponse,
          Recognizer.INTRADA -> intradaResponse))
      actorRef ! processMsg

      recognizer.expectMsg(500 millis, processMsg)
      nextStep.expectNoMsg(100 millis)
    }
    "Send reply to createResponse step when no plate found and icr.flow is set to skip" in {
      val probe = TestProbe()

      val nextStep = TestProbe()
      val recognizer = TestProbe()
      val actorRef = TestActorRef(new CheckNeedPlateFinder(recognizer.ref, nextStep.ref))

      //create message
      val options = Map(RecognizeOptions.icrFlow -> RecognizeOptions.icrFlow_SKIP)
      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, options)
      val plate = createLicensePlateResult(Recognizer.ARH)
      val arhResponse = RecognizeImageResponse(msg.msgId, msg.imageFile, Some(plate))
      val intradaPlate = createLicensePlateResult(Recognizer.INTRADA)
      val intradaResponse = RecognizeImageResponse(msg.msgId, msg.imageFile, Some(intradaPlate))

      val processMsg = RecognizeImageProcessMessage(
        requester = probe.ref,
        request = msg,
        responses = Map(Recognizer.ARH -> arhResponse,
          Recognizer.INTRADA -> intradaResponse))
      actorRef ! processMsg

      nextStep.expectMsg(500 millis, processMsg)
      recognizer.expectNoMsg(100 millis)
    }
    "Send reply to createResponse step when no plate found and icr.flow is set to default" in {
      val probe = TestProbe()

      val nextStep = TestProbe()
      val recognizer = TestProbe()
      val actorRef = TestActorRef(new CheckNeedPlateFinder(recognizer.ref, nextStep.ref))

      //create message
      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, Map())
      val plate = createLicensePlateResult(Recognizer.ARH)
      val arhResponse = RecognizeImageResponse(msg.msgId, msg.imageFile, Some(plate))
      val intradaPlate = createLicensePlateResult(Recognizer.INTRADA)
      val intradaResponse = RecognizeImageResponse(msg.msgId, msg.imageFile, Some(intradaPlate))

      val processMsg = RecognizeImageProcessMessage(
        requester = probe.ref,
        request = msg,
        responses = Map(Recognizer.ARH -> arhResponse,
          Recognizer.INTRADA -> intradaResponse))
      actorRef ! processMsg

      nextStep.expectMsg(500 millis, processMsg)
      recognizer.expectNoMsg(100 millis)
    }
    "Send reply to next step when empty plate found" in {
      val probe = TestProbe()

      val nextStep = TestProbe()
      val recognizer = TestProbe()
      val actorRef = TestActorRef(new CheckNeedPlateFinder(recognizer.ref, nextStep.ref))

      //create message
      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, recognizeOptions)
      val plate = createLicensePlateResult(Recognizer.ARH)
      val arhResponse = RecognizeImageResponse(msg.msgId, msg.imageFile, Some(plate))
      val intradaPlate = createLicensePlateResult(Recognizer.INTRADA).copy(plateStatus = LicensePlateResponse.PLATESTATUS_PLATE_FOUND,
        plateNumber = "",
        rawPlateNumber = "")
      val intradaResponse = RecognizeImageResponse(msg.msgId, msg.imageFile, Some(intradaPlate))

      val processMsg = RecognizeImageProcessMessage(
        requester = probe.ref,
        request = msg,
        responses = Map(Recognizer.ARH -> arhResponse,
          Recognizer.INTRADA -> intradaResponse))
      actorRef ! processMsg

      nextStep.expectMsg(500 millis, processMsg)
      recognizer.expectNoMsg(100 millis)
    }
    "Send reply to recognizer step when error" in {
      val probe = TestProbe()

      val nextStep = TestProbe()
      val recognizer = TestProbe()
      val actorRef = TestActorRef(new CheckNeedPlateFinder(recognizer.ref, nextStep.ref))

      //create message
      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, recognizeOptions)
      val plate = createLicensePlateResult(Recognizer.ARH).copy(returnStatus = -1)
      val arhResponse = RecognizeImageResponse(msg.msgId, msg.imageFile, Some(plate))
      val intradaPlate = createLicensePlateResult(Recognizer.INTRADA).copy(returnStatus = -2)
      val intradaResponse = RecognizeImageResponse(msg.msgId, msg.imageFile, Some(intradaPlate))

      val processMsg = RecognizeImageProcessMessage(
        requester = probe.ref,
        request = msg,
        responses = Map(Recognizer.ARH -> arhResponse,
          Recognizer.INTRADA -> intradaResponse))
      actorRef ! processMsg

      recognizer.expectMsg(500 millis, processMsg)
      nextStep.expectNoMsg(100 millis)
    }
    "Send reply to next step when no responses" in {
      val probe = TestProbe()

      val nextStep = TestProbe()
      val recognizer = TestProbe()
      val actorRef = TestActorRef(new CheckNeedPlateFinder(recognizer.ref, nextStep.ref))

      //create message
      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, recognizeOptions)
      val processMsg = RecognizeImageProcessMessage(
        requester = probe.ref,
        request = msg,
        responses = Map())
      actorRef ! processMsg
      recognizer.expectMsg(500 millis, processMsg)
      nextStep.expectNoMsg(100 millis)
    }
    "Send reply to recognizer when intrada with low conf" in {
      val probe = TestProbe()

      val nextStep = TestProbe()
      val recognizer = TestProbe()
      val actorRef = TestActorRef(new CheckNeedPlateFinder(recognizer.ref, nextStep.ref))

      //create message
      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, recognizeOptions)
      val plate = createLicensePlateResult(Recognizer.ARH)
      val arhResponse = RecognizeImageResponse(msg.msgId, msg.imageFile, Some(plate))
      val intradaPlate = createLicensePlateResult(Recognizer.INTRADA).copy(plateStatus = LicensePlateResponse.PLATESTATUS_PLATE_FOUND, plateCountryCode = "NL", plateConfidence = 30)
      val intradaResponse = RecognizeImageResponse(msg.msgId, msg.imageFile, Some(intradaPlate))

      val processMsg = RecognizeImageProcessMessage(
        requester = probe.ref,
        request = msg,
        responses = Map(Recognizer.ARH -> arhResponse,
          Recognizer.INTRADA -> intradaResponse))
      actorRef ! processMsg

      recognizer.expectMsg(500 millis, processMsg)
      nextStep.expectNoMsg(100 millis)
    }
    "Send reply to next step when intrada with country != NL" in {
      val probe = TestProbe()

      val nextStep = TestProbe()
      val recognizer = TestProbe()
      val actorRef = TestActorRef(new CheckNeedPlateFinder(recognizer.ref, nextStep.ref))

      //create message
      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, recognizeOptions)
      val plate = createLicensePlateResult(Recognizer.ARH)
      val arhResponse = RecognizeImageResponse(msg.msgId, msg.imageFile, Some(plate))
      val intradaPlate = createLicensePlateResult(Recognizer.INTRADA).copy(plateStatus = LicensePlateResponse.PLATESTATUS_PLATE_FOUND, plateCountryCode = "DE")
      val intradaResponse = RecognizeImageResponse(msg.msgId, msg.imageFile, Some(intradaPlate))

      val processMsg = RecognizeImageProcessMessage(
        requester = probe.ref,
        request = msg,
        responses = Map(Recognizer.ARH -> arhResponse,
          Recognizer.INTRADA -> intradaResponse))
      actorRef ! processMsg

      nextStep.expectMsg(500 millis, processMsg)
      recognizer.expectNoMsg(100 millis)
    }
  }
  def createLicensePlateResult(recognizer: String): LicensePlateResponse = {
    LicensePlateResponse(recognizer = recognizer,
      returnStatus = 0,
      plateStatus = LicensePlateResponse.PLATESTATUS_NO_PLATE_FOUND,
      plateNumber = "12XYZ3",
      rawPlateNumber = "12_XYZ_3",
      plateConfidence = 80,
      plateCountryCode = LicensePlate.PLATE_NETHERLANDS,
      plateCountryConfidence = 70,
      licenseSnippetFileName = "LicenseSnippetFileName",
      plateXOffset = 10,
      plateYOffset = 10,
      plateTL = null,
      plateTR = null,
      plateBL = null,
      plateBR = null,
      plateType = None)
  }

}