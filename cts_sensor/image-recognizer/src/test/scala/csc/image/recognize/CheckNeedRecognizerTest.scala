/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.image.recognize

import org.scalatest._
import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import csc.vehicle.message.{ ImageFormat, Priority }
import java.io.File

import scala.concurrent.duration._

class CheckNeedRecognizerTest extends TestKit(ActorSystem("CheckNeedRecognizerTest"))
  with WordSpecLike with MustMatchers with BeforeAndAfterAll {

  val recognizer = new TestRecognizer()

  override def afterAll() {
    system.shutdown
  }

  "CheckNeedRecognizer" must {
    "Send reply to sender" in {
      recognizer.cleanErrors
      val probe = TestProbe()

      val nextStep = TestProbe()
      val actorRef = TestActorRef(new CheckNeedRecognizer(nextStep.ref))

      //create message
      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, Map())
      val plate = TranslatePlate.createLicensePlateResult(Some(recognizer.recognizeImage(imageFile = new File("TestFile.tif"), format = ImageFormat.TIF_BAYER, options = Map())))
      val resp = RecognizeImageResponse(msg.msgId, msg.imageFile, plate)
      val processMsg = RecognizeImageProcessMessage(
        requester = probe.ref,
        request = msg,
        responses = Map("Step1" -> resp))
      actorRef ! processMsg

      val result = probe.expectMsg(500 millis, resp)
    }
    "Send reply to next step when no plate found" in {
      recognizer.cleanErrors
      recognizer.simulatePlateNotFound = true
      val probe = TestProbe()

      val nextStep = TestProbe()
      val actorRef = TestActorRef(new CheckNeedRecognizer(nextStep.ref))

      //create message
      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, Map())
      val plate = TranslatePlate.createLicensePlateResult(Some(recognizer.recognizeImage(imageFile = new File("TestFile.tif"), format = ImageFormat.TIF_BAYER, options = Map())))
      val processMsg = RecognizeImageProcessMessage(
        requester = probe.ref,
        request = msg,
        responses = Map("Step1" -> RecognizeImageResponse(msg.msgId, msg.imageFile, plate)))
      actorRef ! processMsg

      val result = nextStep.expectMsg(500 millis, processMsg)
    }
    "Send reply to next step when unreliable plate license found" in {
      recognizer.cleanErrors
      val probe = TestProbe()

      val nextStep = TestProbe()
      val actorRef = TestActorRef(new CheckNeedRecognizer(nextStep.ref))

      //create message
      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, Map())
      var plate = TranslatePlate.createLicensePlateResult(Some(recognizer.recognizeImage(imageFile = new File("TestFile.tif"), format = ImageFormat.TIF_BAYER, options = Map())))
      plate = plate.map(p ⇒ p.copy(plateConfidence = 9))
      val processMsg = RecognizeImageProcessMessage(
        requester = probe.ref,
        request = msg,
        responses = Map("Step1" -> RecognizeImageResponse(msg.msgId, msg.imageFile, plate)))
      actorRef ! processMsg

      val result = nextStep.expectMsg(500 millis, processMsg)
    }
    "Send reply to sender when unreliable plate license found but country not NL" in {
      recognizer.cleanErrors
      val probe = TestProbe()

      val nextStep = TestProbe()
      val actorRef = TestActorRef(new CheckNeedRecognizer(nextStep.ref))

      //create message
      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, Map())
      var plate = TranslatePlate.createLicensePlateResult(Some(recognizer.recognizeImage(imageFile = new File("TestFile.tif"), format = ImageFormat.TIF_BAYER, options = Map())))
      plate = plate.map(p ⇒ p.copy(plateConfidence = 9, plateCountryCode = "DE"))
      val processMsg = RecognizeImageProcessMessage(
        requester = probe.ref,
        request = msg,
        responses = Map("Step1" -> RecognizeImageResponse(msg.msgId, msg.imageFile, plate)))
      actorRef ! processMsg

      probe.expectMsg(500 millis, processMsg.responses.get("Step1").get)
    }
    "Send reply to next step when unreliable country license " in {
      recognizer.cleanErrors
      val probe = TestProbe()

      val nextStep = TestProbe()
      val actorRef = TestActorRef(new CheckNeedRecognizer(nextStep.ref))

      //create message
      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, Map())
      var plate = TranslatePlate.createLicensePlateResult(Some(recognizer.recognizeImage(imageFile = new File("TestFile.tif"), format = ImageFormat.TIF_BAYER, options = Map())))
      plate = plate.map(p ⇒ p.copy(plateCountryConfidence = 9))
      val processMsg = RecognizeImageProcessMessage(
        requester = probe.ref,
        request = msg,
        responses = Map("Step1" -> RecognizeImageResponse(msg.msgId, msg.imageFile, plate)))
      actorRef ! processMsg

      val result = nextStep.expectMsg(500 millis, processMsg)
    }
    "Send reply to Sender when unreliable country license but country not NL" in {
      recognizer.cleanErrors
      val probe = TestProbe()

      val nextStep = TestProbe()
      val actorRef = TestActorRef(new CheckNeedRecognizer(nextStep.ref))

      //create message
      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, Map())
      var plate = TranslatePlate.createLicensePlateResult(Some(recognizer.recognizeImage(imageFile = new File("TestFile.tif"), format = ImageFormat.TIF_BAYER, options = Map())))
      plate = plate.map(p ⇒ p.copy(plateCountryConfidence = 9, plateCountryCode = "DE"))
      val processMsg = RecognizeImageProcessMessage(
        requester = probe.ref,
        request = msg,
        responses = Map("Step1" -> RecognizeImageResponse(msg.msgId, msg.imageFile, plate)))
      actorRef ! processMsg

      probe.expectMsg(500 millis, processMsg.responses.get("Step1").get)
    }
    "Send reply to next step when empty plate found" in {
      recognizer.cleanErrors
      recognizer.simulateEmpty = true
      val probe = TestProbe()

      val nextStep = TestProbe()
      val actorRef = TestActorRef(new CheckNeedRecognizer(nextStep.ref))

      //create message
      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, Map())
      val plate = TranslatePlate.createLicensePlateResult(Some(recognizer.recognizeImage(imageFile = new File("TestFile.tif"), format = ImageFormat.TIF_BAYER, options = Map())))
      val processMsg = RecognizeImageProcessMessage(
        requester = probe.ref,
        request = msg,
        responses = Map("Step1" -> RecognizeImageResponse(msg.msgId, msg.imageFile, plate)))
      actorRef ! processMsg

      val result = nextStep.expectMsg(500 millis, processMsg)
    }
    "Send reply to next step when error" in {
      recognizer.cleanErrors
      recognizer.simulateErrors = true
      val probe = TestProbe()

      val nextStep = TestProbe()
      val actorRef = TestActorRef(new CheckNeedRecognizer(nextStep.ref))

      //create message
      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, Map())
      val processMsg = RecognizeImageProcessMessage(
        requester = probe.ref,
        request = msg,
        responses = Map("Step1" -> RecognizeImageResponse(msg.msgId, msg.imageFile, None)))
      actorRef ! processMsg

      val result = nextStep.expectMsg(500 millis, processMsg)
    }
    "Send reply to next step when no responses" in {
      val probe = TestProbe()

      val nextStep = TestProbe()
      val actorRef = TestActorRef(new CheckNeedRecognizer(nextStep.ref))

      //create message
      val msg = RecognizeImageRequest("ID", Priority.NONCRITICAL, "TestFile.tif", ImageFormat.TIF_BAYER, Map())
      val processMsg = RecognizeImageProcessMessage(
        requester = probe.ref,
        request = msg,
        responses = Map())
      actorRef ! processMsg
      val result = nextStep.expectMsg(500 millis, processMsg)
    }
  }

}