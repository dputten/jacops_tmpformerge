package csc.vehicle.message

import csc.amqp.{ EnumerationSerializer, JsonSerializers }
import csc.amqp.adapter.CorrelatedMessage
import net.liftweb.json.{ DefaultFormats, Formats }

case class ClassifyVehicleRequest(msgId: String,
                                  imagePath: String,
                                  position: Point,
                                  time: Long = System.currentTimeMillis()) extends CorrelatedMessage

case class ClassifyVehicleResponse(msgId: String,
                                   category: Option[ValueWithConfidence[VehicleCategory.Value]],
                                   time: Long = System.currentTimeMillis()) extends CorrelatedMessage

trait ClassifierFormats {

  val formats: Formats = DefaultFormats + new EnumerationSerializer(VehicleCategory)

}

