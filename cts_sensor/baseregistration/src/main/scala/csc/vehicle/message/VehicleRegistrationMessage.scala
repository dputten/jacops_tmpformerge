/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.message

import java.util.Date

import csc.amqp.EnumerationSerializer
import csc.common.Extensions._
import csc.vehicle.message.TriggerArea.TriggerArea
import net.liftweb.json.{ FullTypeHints, DefaultFormats, Formats }

/**
 * Priorities
 */
object Priority extends Enumeration {
  val CRITICAL, NONCRITICAL = Value
}

/**
 * The different types of images
 */
object VehicleImageType extends Enumeration {
  type VehicleImageType = Value
  val Overview, OverviewMasked, OverviewRedLight, OverviewSpeed, License, RedLight, MeasureMethod2Speed = Value
}

/**
 * Defines all the pairs (VehicleImageType, ImageVersion) that need to be stored
 */
object StoreImage {
  import csc.vehicle.message.ImageVersion._
  import csc.vehicle.message.VehicleImageType._

  val overviewOriginal = (Overview, Original)
  val overviewMaskedOriginal = (OverviewMasked, Original)
  val licenseOriginal = (License, Original)
  val redLightOriginal = (RedLight, Original)
  val mm2sOriginal = (MeasureMethod2Speed, Original)

  val all = List(overviewOriginal, overviewMaskedOriginal, licenseOriginal, redLightOriginal, mm2sOriginal)

  /**
   * @param msg
   * @return list with all the images are are meant to be stored and exist
   */
  def getAllImages(msg: VehicleRegistrationMessage) =
    for (tp ← all; image ← msg.getJpgImageFile(tp._1, tp._2)) yield image
}

/**
 * Possible Categories
 */
object VehicleCategory extends Enumeration {
  type VehicleCategory = Value
  val Pers = Value(1)
  val Large_Truck = Value(4)

  val Unknown = Value(6)
  val Motor = Value(10)
  val Car = Value(7)
  val Van = Value(11)
  val CarTrailer = Value(2)
  val Bus = Value(5)
  val Truck = Value(3)
  val TruckTrailer = Value(8)
  val SemiTrailer = Value(9)
}

/**
 * All value which are used with a confidence
 */
case class ValueWithConfidence[T](value: T, confidence: Int = 0)

/**
 * Float value which are used with a confidence.
 * Separate from the generic ValueWithConfidence because lift-json does not deserialize
 * that correctly for AnyVal types.
 */
// TODO Find better serializer to remove this hack.
case class ValueWithConfidence_Float(value: Float, confidence: Int = 0)

trait CameraUnit {
  def id: String
  def name: String
  def system: String
  def gantry: String
}

/**
 * Representation of a Lane
 * @param laneId
 * @param name
 * @param gantry
 * @param system
 * @param sensorGPS_longitude
 * @param sensorGPS_latitude
 */
case class Lane(laneId: String,
                name: String,
                gantry: String,
                system: String,
                sensorGPS_longitude: Double,
                sensorGPS_latitude: Double,
                bpsLaneId: Option[String] = None,
                incomingDirection: Option[Boolean] = None) extends CameraUnit {

  override def id: String = laneId

  def isIncoming = incomingDirection getOrElse false
}

case class Camera(cameraId: String, gantry: String, system: String) extends CameraUnit {
  override def id: String = cameraId
  override def name: String = cameraId
}

/**
 * This Class represents a period in time
 */
case class PeriodRange(start: Long, end: Long) {
  /**
   * Has the given period an overlap with this Period
   * @param p1 given period
   * @return true when there is an overlap
   *         false when there is not a overlap
   */
  def matchPeriod(p1: PeriodRange): Boolean = {
    (p1.start < end && start < p1.end) || p1.start == end || start == p1.end
  }
  override def toString(): String = {
    val stDate = new Date(start)
    val endDate = new Date(end)
    val str = new StringBuffer()
    str.append("PeriodRange [")
    str.append(stDate.toString("dd-MM-yyyy HH:mm:ss.SSS z", "UTC"))
    str.append(", ")
    str.append(endDate.toString("dd-MM-yyyy HH:mm:ss.SSS z", "UTC"))
    str.append("]")
    str.toString
  }

}

/**
 * Class used to join different sensorEvents
 * @param timestamp
 * @param matchRange  Range where possible other Events can match
 * @param triggerSource Identify source
 */
case class SensorEvent(timestamp: Long, matchRange: PeriodRange, triggerSource: String)

/**
 * Represents the direction of a vehicle
 */
object Direction extends Enumeration {
  val Incoming = Value(-1, "Incoming")
  val Outgoing = Value(1, "Outgoing")
}

/**
 *  collection of file formats supported by this application
 */
object ImageFormat extends Enumeration {
  val TIF_BAYER, TIF_RGB, TIF_GRAY, JPG_RGB, JPG_GRAY = Value

  def toJpg(format: ImageFormat.Value): ImageFormat.Value = format match {
    case ImageFormat.TIF_BAYER ⇒ ImageFormat.JPG_RGB
    case ImageFormat.TIF_RGB   ⇒ ImageFormat.JPG_RGB
    case ImageFormat.TIF_GRAY  ⇒ ImageFormat.JPG_GRAY
    case ImageFormat.JPG_RGB   ⇒ ImageFormat.JPG_RGB
    case ImageFormat.JPG_GRAY  ⇒ ImageFormat.JPG_GRAY
  }

  def toTif(format: ImageFormat.Value): ImageFormat.Value = format match {
    case ImageFormat.TIF_BAYER ⇒ ImageFormat.TIF_RGB
    case ImageFormat.TIF_RGB   ⇒ ImageFormat.TIF_RGB
    case ImageFormat.TIF_GRAY  ⇒ ImageFormat.TIF_GRAY
    case ImageFormat.JPG_RGB   ⇒ ImageFormat.TIF_RGB
    case ImageFormat.JPG_GRAY  ⇒ ImageFormat.TIF_GRAY
  }
}

/**
 * The different versions of an image
 */
object ImageVersion extends Enumeration {
  type ImageVersion = Value
  val Original, Decorated, ForDecoration, Snippet = Value

  def getImageVersionForString(string: String): Option[ImageVersion.Value] = {
    ImageVersion.values.find(value ⇒ value.toString.toUpperCase == string.toUpperCase.trim)
  }
}

/**
 * Representation of a registration file
 * @ param uriFile The URI of the file
 * @ param shaFile The SHA checksum of the file
 */
trait RegistrationFile {
  def uri: String
  def sha: Option[String] // = None
  require(uri != null, "Missing uri")

}

//abstract class RegistrationFile(val uriFile: String, val shaFile: Option[String] = None) {
//  require(uriFile != null, "Missing uri")
//}

/**
 * File produced by the RelaySensorData actor
 * @param uri The URI of the file
 * @param sha The SHA checksum of the file
 */
case class RegistrationRelay(uri: String, sha: Option[String] = None) extends RegistrationFile //(uri, sha)

/**
 * File produced by the CollectIRoseData actor
 * @param uri The URI of the json file
 * @param sha The SHA checksum of the file
 */
case class RegistrationJsonFile(uri: String, sha: Option[String] = None) extends RegistrationFile //(uri, sha)

/**
 * File representing the Exif data from a jpeg file.
 * @param uri The URI of the Exif file
 * @param imageType The type of the image this data came from
 * @param sha The SHA checksum of the file
 */
case class RegistrationExifFile(
  uri: String,
  imageType: VehicleImageType.Value,
  sha: Option[String] = None) extends RegistrationFile //(uri, sha)

/**
 * Representation of a registration file that is no longer needed
 * @param uri The URI of the json file
 * @param sha The SHA checksum of the file
 */
case class ObsoleteRegistrationFile(uri: String, sha: Option[String] = None) extends RegistrationFile //(uri, sha)

/**
 * File received in by Honac sensor
 * @param uri The URI of the file
 * @param sha The SHA checksum of the file
 */
case class RegistrationArchive(uri: String, sha: Option[String] = None) extends RegistrationFile //(uri, sha)

/**
 * Directory containing relay data
 * @param uri
 */
case class RelayDirectory(uri: String) {
  require(uri != null, "Missing uri")
}

/**
 * Position of a license plate in an image
 * @param TLx Top left x coordinate
 * @param TLy          y
 * @param TRx Top right x coordinate
 * @param TRy           y
 * @param BLx Bottom left x coordinate
 * @param BLy             y
 * @param BRx Bottom right x coordinate
 * @param BRy              y
 */
case class LicensePosition(TLx: Int, TLy: Int, TRx: Int, TRy: Int, BLx: Int, BLy: Int, BRx: Int, BRy: Int)

/**
 * @param uri The URI of the file
 * @param imageFormat The format of the image
 * @param imageType The type of the image
 * @param imageVersion The version of the image
 * @param timestamp The creation time of the image (in milliseconds)
 * @param offset The extra wait time which is used to synchronize to the VRI
 * @param timeYellow The yellow time of the image
 * @param timeRed The red time of the image
 * @param sha The SHA checksum of the file
 */
case class RegistrationImage(
  uri: String,
  imageFormat: ImageFormat.Value,
  imageType: VehicleImageType.Value,
  imageVersion: ImageVersion.Value,
  timestamp: Long,
  offset: Long = 0L,
  timeYellow: Option[Long] = None,
  timeRed: Option[Long] = None,
  sha: Option[String] = None,
  imageTimeStamp: Option[Long] = None,
  metaInfo: Option[Seq[ExifTagInfo]] = None) extends RegistrationFile /*(uri, sha)*/ {

  def matches(obj: RegistrationImage): Boolean = imageType == obj.imageType && imageVersion == obj.imageVersion
}

case class RegistrationLicenseData(
  license: Option[ValueWithConfidence[String]] = None,
  licensePosition: Option[LicensePosition] = None,
  licenseSourceImage: Option[VehicleImageType.Value] = None,
  rawLicense: Option[String] = None,
  licenseType: Option[String] = None,
  recognizedBy: Option[String] = None,
  country: Option[ValueWithConfidence[String]] = None) {

  def toLicensePlateData(): Option[LicensePlateData] = {
    val lpd = LicensePlateData(license,
      licensePosition,
      licenseSourceImage,
      rawLicense,
      licenseType,
      recognizedBy,
      country)
    Some(lpd)
  }

}

object TriggerArea extends Enumeration {
  type TriggerArea = Value
  val LEFT, RIGHT = Value

  def filanemaSuffix(v: TriggerArea) = "_" + v.toString.charAt(0)
}

/**
 * @param area trigger area of the image
 * @param matchesOriginal true if area matches exactly the original image trigger
 */
case class ImageTriggerData(area: TriggerArea, matchesOriginal: Boolean)

case class VehicleData(length: Option[ValueWithConfidence[Float]] = None,
                       direction: Option[Direction.Value] = None,
                       speed: Option[ValueWithConfidence[Float]] = None,
                       category: Option[ValueWithConfidence[VehicleCategory.Value]] = None) {

  def merge(other: VehicleData): VehicleData = VehicleData(
    length.orElse(other.length),
    direction.orElse(other.direction),
    speed.orElse(other.speed),
    category.orElse(other.category))
}

case class TimeInfo(systemTime: Long, imageTime: Option[Long]) {
  lazy val diff: Option[Long] = imageTime map (systemTime - _)
}

case class Point(x: Int, y: Int) {
  def toAwt = new java.awt.Point(x, y)
}

/**
 * message that is used to collect all the data of a Vehicle detection
 */
case class VehicleRegistrationMessage(
  lane: Lane,
  event: SensorEvent,
  eventId: Option[String] = None,
  eventTimestamp: Option[Long] = None,
  priority: Priority.Value = Priority.NONCRITICAL,
  licenseData: RegistrationLicenseData = RegistrationLicenseData(),
  appliedMask: Option[Seq[Point]] = None,
  appliedMaskCropped: Boolean = false,
  vehicleData: VehicleData = VehicleData(),
  files: List[RegistrationFile] = Nil,
  replacedFiles: List[RegistrationFile] = Nil,
  directory: Option[RelayDirectory] = None,
  jsonMessage: Option[String] = None,
  interval1Ms: Option[Long] = None,
  interval2Ms: Option[Long] = None,
  imageTriggerData: Option[ImageTriggerData] = None,
  timeInfo: Option[TimeInfo] = None,
  status: RegistrationStatus = RegistrationStatus.initial,
  certificationData: Option[CertificationData] = None) {

  //time is not lazy, so if no eventTimestamp gets as close as possible to 'would be' time
  val time: Long = eventTimestamp getOrElse (System.currentTimeMillis())
  lazy val id: String = eventId.getOrElse("Unknown")

  def serialNr: Option[String] = certificationData flatMap (_.serialNr)

  def appliedMaskAwt: Option[Seq[java.awt.Point]] = appliedMask map { s ⇒ s map (_.toAwt) }

  def replaceImage(img: RegistrationImage): VehicleRegistrationMessage = {

    var replaced: Option[RegistrationFile] = None

    def replaceFile(input: List[RegistrationFile], accu: List[RegistrationFile]): List[RegistrationFile] = input match {
      case Nil ⇒ accu
      case head :: tail ⇒ head match {
        case image: RegistrationImage if img.matches(image) ⇒
          replaced = Some(image)
          (accu :+ img) ++ tail
        case head ⇒ replaceFile(tail, accu :+ head)
      }
    }

    val newFiles = replaceFile(files, Nil)

    this.copy(files = newFiles, replacedFiles = replacedFiles ++ replaced)
  }

  /**
   * Get a file with specified attributes
   * @param format: The format of the file
   * @param imageType: The type of the file
   * @param imageVersion: The version of the file
   * @return The requested RegistrationFile
   */
  def getImageFile(format: ImageFormat.Value, imageType: VehicleImageType.Value, imageVersion: ImageVersion.Value): Option[RegistrationImage] = {

    files.find(img ⇒ img match {
      case image: RegistrationImage ⇒ (imageType == image.imageType && image.imageFormat == format && image.imageVersion == imageVersion)
      case _                        ⇒ false
    }).asInstanceOf[Option[RegistrationImage]]
  }
  /**
   * Get a jpg file with specified type.
   * Preferred is the colored version and when not present return the gray version
   * @param imageType: The type of the file
   * @return The requested RegistrationImage and when threa are no jpg versions return None
   */
  def getJpgImageFile(imageType: VehicleImageType.Value, imageVersion: ImageVersion.Value): Option[RegistrationImage] = {
    val imageList = getImageFiles(imageType, imageVersion)
    //check if there is a color Jpg first
    //and else get the gray version
    imageList.find(_.imageFormat == ImageFormat.JPG_RGB).orElse(imageList.find(_.imageFormat == ImageFormat.JPG_GRAY))
  }

  /**
   * Get a Relay file
   * @return The requested RegistrationRelay
   */
  def getRelayFile(): Option[RegistrationRelay] = {
    files.collectFirst { case f: RegistrationRelay ⇒ f }
  }

  /**
   * Get all imageFiles with specified type
   * @param imageType: The type of the image file
   * @param imageVersion: The version of the image file
   * @return The requested RegistrationFile
   */
  def getImageFiles(imageType: VehicleImageType.Value, imageVersion: ImageVersion.Value): Seq[RegistrationImage] = {
    files.collect { case f: RegistrationImage if (f.imageType == imageType) && (f.imageVersion == imageVersion) ⇒ f }
  }

  /**
   * Get all imageFiles with specified type
   * @param imageType: The type of the image file
   * @param imageVersion: The version of the image file
   * @param imageFormat: The format of the image file
   * @return The requested RegistrationFile
   */
  def getImageFiles(imageType: VehicleImageType.Value, imageVersion: ImageVersion.Value, imageFormat: ImageFormat.Value): Seq[RegistrationImage] = {
    files.collect { case f: RegistrationImage if (f.imageType == imageType) && (f.imageVersion == imageVersion) && (f.imageFormat == imageFormat) ⇒ f }
  }

  /**
   * Get the RegistrationExifFile file with given imageType
   * @param imageType: The type of the image file
   * @return The requested RegistrationExifFile
   */
  def getExifFile(imageType: VehicleImageType.Value): Option[RegistrationExifFile] = {
    files.collectFirst { case f: RegistrationExifFile if f.imageType == imageType ⇒ f }
  }

  def hasLicensePlate: Boolean = licenseData.license.isDefined

  /**
   * Create a VehicleMeta Object from this message
   * @param images list of RegistrationImage to add
   * @return requested VehicleMetadata object
   */
  def createVehicleMeta(images: List[RegistrationImage]): VehicleMetadata = {

    def createVehicleImage(image: RegistrationImage): VehicleImage = {
      new VehicleImage(
        timestamp = image.timestamp,
        offset = image.offset,
        uri = image.uri,
        imageType = image.imageType,
        checksum = image.sha.getOrElse(""),
        timeYellow = image.timeYellow,
        timeRed = image.timeRed)
    }

    def createValueWithConfidence_Float(valueWithConfidence: Option[ValueWithConfidence[Float]]): Option[ValueWithConfidence_Float] = {
      valueWithConfidence match {
        case Some(vwc) ⇒ Some(new ValueWithConfidence_Float(vwc.value, vwc.confidence))
        case None      ⇒ None
      }
    }

    val eventTime = eventTimestamp.getOrElse(0L)
    val cat = vehicleData.category.map(catValue ⇒ new ValueWithConfidence[String](catValue.value.toString, catValue.confidence))

    val eventDate = new Date(eventTime)
    new VehicleMetadata(
      recordVersion = Version.withCertificationData,
      lane = lane,
      eventId = eventId.getOrElse("Unknown"),
      eventTimestamp = eventTime,
      eventTimestampStr = Some(eventDate.toString("dd-MM-yyyy HH:mm:ss.SSS z", "UTC")),
      length = createValueWithConfidence_Float(vehicleData.length),
      category = cat,
      speed = createValueWithConfidence_Float(vehicleData.speed),
      images = images.map(createVehicleImage), //converting the images
      licensePlateData = licenseData.toLicensePlateData(), //converting the plate data
      direction = vehicleData.direction,
      interval1Ms = interval1Ms,
      interval2Ms = interval2Ms,
      jsonMessage = jsonMessage,
      systemTimestamp = timeInfo map (_.systemTime),
      status = status,
      certificationData = certificationData)
  }

  def getLicensePositionCorners(): Option[Seq[Point]] = licenseData.licensePosition match {
    case Some(licensePosition) ⇒
      val vertices = Seq(
        new Point(licensePosition.TLx, licensePosition.TLy),
        new Point(licensePosition.TRx, licensePosition.TRy),
        new Point(licensePosition.BRx, licensePosition.BRy),
        new Point(licensePosition.BLx, licensePosition.BLy))
      Some(vertices)
    case None ⇒ None
  }
}

trait VehicleRegistrationFormats {

  val formats: Formats =
    DefaultFormats +
      new EnumerationSerializer(Priority, VehicleImageType, VehicleCategory, TriggerArea, ImageFormat, ImageVersion) +
      FullTypeHints(List(classOf[RegistrationFile], classOf[ExifValue]))

}
