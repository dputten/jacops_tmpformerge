/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.message

import scala.math.pow
import csc.vehicle.message.RegistrationStatusFlag.RegistrationStatusFlag

/**
 * Class represents an Image
 * @param timestamp The creation time of the image (in milliseconds)
 * @param offset The offset of the image to sync with VRI
 * @param uri URI of name of the image
 * @param imageType Type of image
 * @param checksum The checksum of the image
 * @param timeYellow The yellow time of the image
 * @param timeRed The red time of the image
 * @param imageVersion version of the image
 */
case class VehicleImage(timestamp: Long, offset: Long, uri: String, imageType: VehicleImageType.Value,
                        checksum: String, timeYellow: Option[Long] = None, timeRed: Option[Long] = None,
                        imageVersion: Option[ImageVersion.Value] = None)

object RegistrationStatusFlag extends Enumeration {
  type RegistrationStatusFlag = Value

  val clockDifference = Value(0)
  val criticalClockDifference = Value(1)
  val incomingDirection = Value(2)
  val testMode = Value(4)
}

case class Version(value: Int)

object Version {
  //val withLicensePlateData = Version(1)
  //val withRecordVersion = Version(2)
  val withCertificationData = Version(3)
}

import RegistrationStatusFlag._

object RegistrationStatus {

  def initial = RegistrationStatus(0)

  def valueOf(flag: RegistrationStatusFlag): Long = pow(2, flag.id).toLong

  def apply(flags: RegistrationStatusFlag*): RegistrationStatus = {
    val list = flags.toSet.toList //eliminates duplicates
    val values: List[Long] = list map (valueOf(_)) //gets values for flags
    val total = values.foldLeft(0.toLong)(_ + _) //total
    RegistrationStatus(total)
  }
}

import RegistrationStatus._

case class RegistrationStatus(flags: Long) {

  def get(flag: RegistrationStatusFlag): Boolean = (RegistrationStatus.valueOf(flag) & flags) > 0
  def set(flag: RegistrationStatusFlag): RegistrationStatus = copy(flags = flags | RegistrationStatus.valueOf(flag))
  def merge(other: RegistrationStatus): RegistrationStatus = RegistrationStatus(flags | other.flags)

}

/**
 * Class used to store the VehicleRegistration metadata as JSON
 * @param recordVersion indicates the version of the structure, related to one value of MetadataVersion
 * @param lane  The lane Information
 * @param eventId The EventId
 * @param eventTimestamp  The time of registration
 * @param eventTimestampStr The time of the registration in string format used for testing
 * @param length the found length
 * @param category the found category
 * @param speed the found speed
 * @param images List of images
 * @param licensePlateData will contain all the license plate fields, for version > 1
 * @param direction direction
 */
case class VehicleMetadata(
  recordVersion: Version,
  lane: Lane,
  eventId: String,
  eventTimestamp: Long,
  eventTimestampStr: Option[String], //Only used for testing
  length: Option[ValueWithConfidence_Float] = None,
  category: Option[ValueWithConfidence[String]] = None,
  speed: Option[ValueWithConfidence_Float] = None,
  images: Seq[VehicleImage],
  licensePlateData: Option[LicensePlateData] = None,
  direction: Option[Direction.Value] = None,
  interval1Ms: Option[Long] = None,
  interval2Ms: Option[Long] = None,
  jsonMessage: Option[String] = None,
  systemTimestamp: Option[Long] = None,
  status: RegistrationStatus = RegistrationStatus.initial,
  certificationData: Option[CertificationData] = None) {

  def getImage(imageType: VehicleImageType.Value, imageVersion: ImageVersion.Value): Option[VehicleImage] = {
    images.find { vi ⇒ vi.imageType == imageType && vi.imageVersion == Some(imageVersion) }
  }

  /*
   * The serial number of the measurement unit.
   * Now moved to CertificationData
   */
  def serialNr: String = certificationData flatMap { _.serialNr } getOrElse ("")
}

case class LicensePlateData(license: Option[ValueWithConfidence[String]] = None,
                            licensePosition: Option[LicensePosition] = None, //license position (absolute)
                            sourceImageType: Option[VehicleImageType.Value] = None,
                            rawLicense: Option[String] = None,
                            licenseType: Option[String] = None,
                            recognizedBy: Option[String] = None,
                            country: Option[ValueWithConfidence[String]] = None)

case class CertificationData(componentChecksumMap: Map[String, String] = Map.empty,
                             componentSerialNumberMap: Map[String, String] = Map.empty) {

  //serialNr is now included in the serial number map
  def serialNr: Option[String] = componentSerialNumberMap.get(CertificationData.defaultSerialNumComponent)

  def withSerialNumber(sn: String): CertificationData = {
    copy(componentSerialNumberMap = componentSerialNumberMap.updated(CertificationData.defaultSerialNumComponent, sn))
  }

}

object CertificationData {

  val defaultSerialNumComponent = "serialNr"

  def merge(first: Option[CertificationData], second: Option[CertificationData]): Option[CertificationData] = {
    (first, second) match {
      case (None, None)    ⇒ None
      case (Some(c), None) ⇒ Some(c)
      case (None, Some(c)) ⇒ Some(c)
      case (Some(c1), Some(c2)) ⇒
        val ccMap = c1.componentChecksumMap ++ c2.componentChecksumMap
        val csnMap = c1.componentSerialNumberMap ++ c2.componentSerialNumberMap
        Some(CertificationData(ccMap, csnMap))
    }
  }

}