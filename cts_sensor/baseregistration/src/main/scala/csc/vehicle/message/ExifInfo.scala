/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.message

/**
 * Different value types
 */
trait ExifValue
case class ExifValueString(str: String) extends ExifValue
case class ExifValueBytes(byteArray: Seq[Byte]) extends ExifValue
case class ExifValueIntegers(array: Seq[Int]) extends ExifValue
case class ExifValueLongs(array: Seq[Long]) extends ExifValue
case class ExifValueShorts(array: Seq[Short]) extends ExifValue
case class ExifValueRational(array: Seq[Rational]) extends ExifValue
case class Rational(numerator: Int, denominator: Int)

/**
 * Representation of one exif record
 * @param directory the directory where the record was found
 * @param tagName the tag name of the record, this is only presentation the real definition is the tagType
 * @param tagType the id or type of the record
 * @param value the value of the record
 */
case class ExifTagInfo(directory: String, tagName: String, tagType: Int, value: ExifValue)

abstract class TagValue[T] {
  def value: T
}
case class TagValueByte(val value: Int) extends TagValue[Int] //unsigned, so we need int to hold the value
case class TagValueShort(val value: Int) extends TagValue[Int] //unsigned, so we need int to hold the value
case class TagValueLong(val value: Long) extends TagValue[Long]
case class TagValueByteArray(val value: Array[Byte]) extends TagValue[Array[Byte]]

/**
 *
 * @param tagType
 * @param value
 */
case class MakerNoteInfo(tagType: Int, value: TagValue[_])

/**
 * Subset of the directory names returned from readExif
 */
object ExifTagInfo {
  val directoryJpeg = "JPEG"
  val directoryJfif = "JFIF"
  val directoryJpegComment = "JPEGCOMMENT"
  val directoryExif_SubIFD = "EXIF SUBIFD"
  val directoryExif_IFD0 = "EXIF IFD0"
}
