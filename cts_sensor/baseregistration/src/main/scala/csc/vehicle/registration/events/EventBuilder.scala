/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration.events

import java.util.Date
import csc.base.SensorDevice.SensorDevice
import csc.base.SensorPosition.SensorPosition
import csc.vehicle.message.{ PeriodRange, SensorEvent }

/**
 * Interface of the eventBuilder. This is used by the different sensor endpoints
 * to create the event object. This way an endpoint can change the creation of an event depending
 * on the configuration and the order of triggers for the different sensors
 */
trait EventBuilder {
  /**
   * Create the event
   * @param now the time of the new event
   * @return the new created sensorEvent
   */
  def createEvent(now: Long): SensorEvent

}

/**
 * This class implements the event, which has a period from the last received event until the current event.
 *
 * @param src the triggerSource of the created events
 */
class LastEventBuilder(src: String) extends EventBuilder {
  var lastReceivedEvent: Long = System.currentTimeMillis()

  def createEvent(now: Long): SensorEvent = {
    val sensorEvent = new SensorEvent(now, new PeriodRange(lastReceivedEvent, now), src)
    lastReceivedEvent = now
    sensorEvent
  }
}

/**
 * This class implements the event, which has a shifted period from the last received event until the current event.
 *
 * @param src the triggerSource of the created events
 */
class ShiftedLastEventBuilder(src: String, shift: Int) extends EventBuilder {
  var lastReceivedEvent: Long = System.currentTimeMillis()

  def createEvent(now: Long): SensorEvent = {
    val shiftedTime = now + shift
    val sensorEvent = new SensorEvent(now, new PeriodRange(lastReceivedEvent, shiftedTime), src)
    lastReceivedEvent = shiftedTime
    sensorEvent
  }
}

/**
 * This class implements the EventBuilder, which create an event without a period (start and end are equal),
 *
 * @param src the triggerSource of the created events
 */
class TriggerEventBuilder(src: String) extends EventBuilder {

  def createEvent(now: Long): SensorEvent = {
    new SensorEvent(now, new PeriodRange(now, now), src)
  }
}
//TODO 20120229 RR->RB: old factory pattern, there are better ways (do later)
object EventBuilder {
  private var shiftTime = 100

  def setTimeShift(shift: Int) {
    shiftTime = shift
  }

  def getInstance(position: SensorPosition, device: SensorDevice): EventBuilder = {
    import csc.base.SensorPosition._
    position match {
      case Trigger     ⇒ new TriggerEventBuilder(device.toString)
      case Last        ⇒ new LastEventBuilder(device.toString)
      case LastShifted ⇒ new ShiftedLastEventBuilder(device.toString, shiftTime)
    }
  }

}

