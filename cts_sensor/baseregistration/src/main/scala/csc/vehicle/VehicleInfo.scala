package csc.vehicle

import csc.akka.logging.DirectLogging
import imagefunctions.Point

/**
 * Created by carlos on 14/01/16.
 */
case class VehicleInfo(plate: String, vehicle: String, country: String, confidence: Int,
                       countryConfidence: Int, activator: Option[String] = None,
                       platePosition: Option[Point] = None) {
  val MINIMUM_PLATE_SIZE = 8
  val MIN_CONFIDENCE = 0
  val MAX_CONFIDENCE = 100

  require(plate.size == MINIMUM_PLATE_SIZE, throw new IllegalArgumentException("Illegal plate, required plate size to be " + MINIMUM_PLATE_SIZE))
  require(confidence >= MIN_CONFIDENCE && confidence <= MAX_CONFIDENCE, throw new IllegalArgumentException("Illegal confidence, should be between 0 and 100"))
  require(countryConfidence >= MIN_CONFIDENCE && countryConfidence <= MAX_CONFIDENCE, throw new IllegalArgumentException("Illegal country confidence, should be between 0 and 100"))
}

/**
 * Criteria for choosing the best plate:
 * -if one or more exist and some is active, return the first active
 * -if one or more exist, and none is active, return the first
 * -if none exists, return none
 * An 'active' plate means it has an activator, and filenameWithoutExtension.endsWith(activator)
 */
object VehicleInfo extends DirectLogging {

  type PlateExtractor = String ⇒ List[VehicleInfo]

  val extractors: Map[String, PlateExtractor] = Map(
    "1" -> applyMultiPlate,
    "2" -> applyMultiPlateWithPosition)

  /**
   * @param path
   * @return All the license plates found in the given file path
   */
  def allPlates(path: String): List[VehicleInfo] = {
    filenameWithoutExtension(path) match {
      case ""                           ⇒ Nil
      case s if (s.contains("noplate")) ⇒ Nil
      case VersionExtractor(v, c) ⇒ extractors.get(v) match {
        case None ⇒
          log.warning("VehicleInfo parser version {} not found", v)
          Nil
        case Some(extr) ⇒ extr(c)
      }
      case other ⇒ applyLegacy(other)
    }
  }

  /**
   * @param path
   * @return the best plate found in the given file path
   */
  def apply(path: String): Option[VehicleInfo] = allPlates(path) match {
    case Nil         ⇒ None
    case List(plate) ⇒ Some(plate) //only one plate: use it
    case many ⇒ {
      val filename = VehicleInfo.filenameWithoutExtension(path)
      val active = many find { p ⇒ p.activator.isDefined && filename.endsWith(p.activator.get) }
      active match {
        case None ⇒ Some(many(0)) //no plate active: use the first
        case some ⇒ some
      }
    }
  }

  def applyLegacy(str: String): List[VehicleInfo] = {
    val parts = str.split("-")
    if (parts.length < 5) {
      Nil
    } else {
      //legacy ImageRecognitionStubInput.apply
      val rawDataArray = parts.take(5)
      List(VehicleInfo(rawDataArray(0).drop(14), rawDataArray(1), rawDataArray(2), rawDataArray(3).toInt, rawDataArray(4).toInt))
    }
  }

  def applyMultiPlate(source: String): List[VehicleInfo] = {
    val plates = source.split("\\|").toList
    plates map { elem ⇒ applySinglePlate(elem) } flatten
  }

  def applyMultiPlateWithPosition(source: String): List[VehicleInfo] = {
    val plates = source.split("\\|").toList
    plates map { elem ⇒ applySinglePlatePositioned(elem) } flatten
  }

  /**
   * plate version 1 fields:
   * 0 - plate number
   * 1 - classification
   * 2 - plate country
   * 3 - plate confidence
   * 4 - country confidence
   * 5 - activator (optional)
   * @param source
   * @return
   */
  def applySinglePlate(source: String): Option[VehicleInfo] = {
    val parts = source.split(",")
    parts.length match {
      case x if (x < 5) ⇒ None
      case y ⇒ {
        val activator = if (y > 5) Some(parts(5)) else None
        Some(VehicleInfo(parts(0), parts(1), parts(2), parts(3).toInt, parts(4).toInt, activator))
      }
    }
  }

  /**
   * version 2 fields:
   * 0 to 4 - same as version 1
   * 5 - plate position x
   * 6 - plate position y
   * 7 - activator (optional)
   * @param source
   * @return
   */
  def applySinglePlatePositioned(source: String): Option[VehicleInfo] = {
    val parts = source.split(",")
    parts.length match {
      case x if (x < 7) ⇒ None
      case y ⇒ {
        val activator = if (y > 7) Some(parts(7)) else None
        val position = new Point(parts(5).toInt, parts(6).toInt)
        Some(VehicleInfo(parts(0), parts(1), parts(2), parts(3).toInt, parts(4).toInt, activator, Some(position)))
      }
    }
  }

  def filenameWithoutExtension(source: String): String = {
    val filename = source.substring(source.lastIndexOf("/") + 1)
    filename.lastIndexOf(".") match {
      case -1 ⇒ filename
      case x  ⇒ filename.substring(0, x)
    }
  }

}

object VersionExtractor {

  val pattern = """(.*)#XP(.+)#(.+)#(.*)""".r

  def unapply(str: String): Option[(String, String)] = {
    val it = pattern.findAllIn(str).matchData
    if (it.hasNext) {
      val m = it.next()
      Some((m.group(2), m.group(3)))
    } else None
  }
}
