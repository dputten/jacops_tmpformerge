package csc.common

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
/**
 * The exception that is thrown when required properties are not found in the config file
 */
class MissingAttributeConfigException(attributes: List[String]) extends RuntimeException {
  override def toString = "Missing attribute(s) " + attributes

  def parameters = attributes
}