package base
/*
 * Copyright © CSC
 * Date: Jul 19, 2010
 * Time: 2:31:51 PM
 */

import _root_.java.util.concurrent.locks.ReentrantLock
import csc.vehicle.message.Priority

/**
 * This class limits the number of parallel processing of  NotTimeCritical processes.
 * TimeCritical processes are not blocked in any way, but are counted as process running.
 * NotTimeCritical processes are only started when the number of processes are lower than the maximum allowed.
 * Warning changing the behavior in this class, can have serious effects on the correct working of this class.
 * Always contact someone with experience in multithreading (two knows more than one)
 */
class FavorCriticalProcess(maxNrNTCTasks: Int) {
  private val internalLock = new ReentrantLock
  private val lockCondition = internalLock.newCondition
  private var nrProcessed = 0

  /**
   * guard the processing of the body
   * @param prio timeCritical indication if the process should be running TimeCritical
   * @param body the process which is guarded
   */
  def processGuardedResource(prio: Priority.Value, body: ⇒ Unit) {
    val timeCritical: Boolean = Priority.CRITICAL == prio
    startFavorCriticalProcess(timeCritical)
    try {
      //do the processing
      body
    } finally {
      //were done so lower the number of processed
      endFavorCriticalProcess
    }
  }

  /**
   * Get a functional Lock for the process.
   * When TimeCritical it always is possible to lock without blocking
   * NotTimeCritical can be blocked until the nrProcessed is lower than the maximum number of processes.
   */
  def startFavorCriticalProcess(timeCritical: Boolean) {
    //get lock to update test and update nrProcessed
    internalLock.lock
    try {
      if (!timeCritical && nrProcessed >= maxNrNTCTasks) {
        //we are not TimeCritical and we are already the maximum of NTCTasks
        //So wait for the nrProcessed is lower than maxNrNTCTasks
        //The await will unlock the internalLock and when it returns the lock is locked again
        lockCondition.await
      }
      //at this point we update the number of processed
      nrProcessed += 1
    } finally {
      internalLock.unlock
    }
  }

  /**
   * Unlock the functional lock.
   * When the number of processes drops below the maximum number of processes, a signal is given
   * that a NTC process can start
   */
  def endFavorCriticalProcess() {
    //get lock to update nrProcessed
    internalLock.lock
    try {
      //update the number of processed
      nrProcessed -= 1
      //check if we have to signal a awaiting process
      if (nrProcessed < maxNrNTCTasks) {
        lockCondition.signal
      }
    } finally {
      internalLock.unlock
    }
  }
}

object SectionGuard {
  def process(guard: Option[FavorCriticalProcess], prio: Priority.Value, body: ⇒ Unit) {
    if (guard.isDefined) {
      guard.get.processGuardedResource(prio, body)
    } else {
      body
    }
  }
}