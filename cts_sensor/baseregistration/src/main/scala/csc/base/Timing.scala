package base
//copyright CSC 2010

import akka.event.LoggingAdapter

/**
 * Trait for calculating the time needed to execute a function
 */
trait Timing {

  /**
   * calculates the interval of function execution
   * @param body function to time
   * @return time in ms
   */
  def calcInterval(body: ⇒ Unit): Long = {
    val start = System.currentTimeMillis()
    body
    val end = System.currentTimeMillis()
    val time = end - start
    return time
  }

  /**
   * logs the start and end function execution
   *
   * @param startPoint Log point at start of the function
   * @param endPoint Log point at end of the function
   * @param description Log description of the function
   * @param eventId the eventId currently processed
   * @param body function to time
   * @return void
   */
  def performanceLogInterval(log: LoggingAdapter, startPoint: Int, endPoint: Int, description: String, eventId: String, body: ⇒ Unit): Unit = {
    if (startPoint >= 0) {
      log.info("Perf eventId=[%s] point %d: %s Start".format(eventId, startPoint, description))
    } else {
      log.info("Perf eventId=[%s]: %s Start".format(eventId, description))
    }
    val time = calcInterval(body)
    if (endPoint >= 0) {
      log.info("Perf eventId=[%s] point %d: %s processing took %s ms.".format(eventId, endPoint, description, time))
    } else {
      log.info("Perf eventId=[%s]: %s processing took %s ms.".format(eventId, description, time))
    }
  }
  def performanceLogEvent(log: LoggingAdapter, point: Int, description: String, eventId: String) {
    log.info("Perf eventId=[%s] point %d: %s".format(eventId, point, description))
  }

  def performanceLogInstructionInterval(log: LoggingAdapter, description: String, body: ⇒ Unit): Unit = {
    log.info("Perf instruction: %s Start".format(description))
    val time = calcInterval(body)
    log.info("Perf instruction: %s processing took %s ms.".format(description, time))
  }

}
