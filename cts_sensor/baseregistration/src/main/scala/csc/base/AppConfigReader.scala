package csc.base

import java.io.File

import com.typesafe.config.{ Config, ConfigFactory }

object AppConfigReader {

  def getAppConfig(customName: Option[String] = None): Config = {

    Option(System.getenv("AKKA_HOME")) match {
      case None ⇒ throw new IllegalArgumentException("AKKA_HOME not set in env")
      case Some(home) ⇒
        val folder = new File(home, "config")
        val ref = ConfigFactory.parseFile(new File(folder, "reference.conf"))
        val main = ConfigFactory.parseFile(new File(folder, customName.getOrElse("application.conf")))
        main.withFallback(ref)
    }
  }

}