package csc.base

case class ActorPaths(recognizeDelegate: String, classifyDelegate: Option[String])

case class RecognizerHeartbeat(time: Long)

