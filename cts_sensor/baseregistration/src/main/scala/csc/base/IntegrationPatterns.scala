/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package base

import akka.actor._

/**
 * This base class implements the IntegrationPattern RecipientList.
 */
abstract class RecipientList(val recipients: Set[ActorRef]) extends RecipientListLike {
  require(recipients != null, "Missing recipients")
}

trait RecipientListLike extends Actor with ActorLogging {

  def recipients: Set[ActorRef]

  /**
   * Send the message to all recipients
   */
  def sendToRecipients(msg: Any) {
    log.debug("Sending {} to all recipients", msg)
    for (actorRef ← recipients) {
      actorRef.!(msg)
    }
  }

  def handleRoundtrip(name: String, msg: RoundtripMessage): Unit = sendToRecipients(msg.add(name))

}

case class RoundtripMessage(visited: List[String]) {
  def add(location: String) = this.copy(visited = visited :+ location)
}

/**
 * Implementation of RecipientList that filter out messages according to a given predicate,
 * if given predicate holds true for the received message, the message will be forwarded to the recipients, otherwise not.
 *
 */
//class FilterActor[T](recipients: Set[ActorRef], accept: T ⇒ Boolean)
//  extends RecipientList(recipients) with ActorLogging {
//
//  override def receive: Receive = {
//    case msg: RoundtripMessage ⇒ handleRoundtrip("RouterActor", msg) //roundrip is never diverted
//    case msg ⇒ try {
//      handle(msg.asInstanceOf[T])
//    } catch {
//      case e: ClassCastException ⇒ log.warning("Ignoring unexpected message {}", msg)
//    }
//  }
//
//  def handle(msg: T): Unit = if (accept(msg)) sendToRecipients(msg) else log.debug("Filtered out {}", msg)
//
//}