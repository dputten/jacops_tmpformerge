package base

import scala.util.Random
import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.TimeUnit

/**
 * Algorithm for truncated exponential backoff .
 * http://en.wikipedia.org/wiki/Exponential_backoff
 *
 * Truncated interpreted as staying on the ceiling.
 * @param slotTime the time to wait associated with a slot
 * @param ceiling the ceiling of the slots (number of slots)
 * @param stayAtCeiling true=truncated/stays on ceiling, false resets after ceiling
 * @param unit TimeUnit
 */
class ExponentialBackoff(slotTime: Long, ceiling: Int, stayAtCeiling: Boolean = false, unit: TimeUnit = TimeUnit.MILLISECONDS) {
  val rand = new Random()
  val slot = new AtomicInteger(1);

  /**
   * Resets the backoff
   */
  def reset = {
    slot.set(1)
  }

  /**
   * gets the next wait time in milliseconds
   * returns the next wait value or None if the ceiling has been returned before this call (of stayAtCeiling is false).
   * if stayAtCeiling is true and at ceiling, the nextWait will be at the ceiling
   * (reset to start again)
   */
  def nextWait: Option[Long] = {
    def time = Some(unit.toMillis(times * slotTime))
    def times = {
      val exp = rand.nextInt(slot.incrementAndGet())
      math.round(math.pow(2, exp) - 1)
    }
    if (slot.get() > ceiling) {
      if (stayAtCeiling) {
        slot.set(ceiling)
        time
      } else {
        None
      }
    } else {
      time
    }
  }

  /**
   * Sleeps on the current thread for the current slot and return the time slept as an Option,
   * or will reset the backoff when ceiling has been passed, and not sleep
   * and return None
   * @return Some(time) or None if not slept
   */
  def sleepOnCurrentThread = {
    nextWait.map { time ⇒
      Thread.sleep(time)
      time
    }.orElse {
      if (!stayAtCeiling) reset
      None
    }
  }
}