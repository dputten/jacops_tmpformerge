package csc.base

object SensorDevice extends Enumeration {
  type SensorDevice = Value
  val Camera, Radar, RadarLength, PIR = Value
}

object SensorPosition extends Enumeration {
  type SensorPosition = Value
  val Trigger, Last, LastShifted = Value
}
