package imagefunctions;

import imagefunctions.bayertif.BayerTifFunctions;
import imagefunctions.bayertif.ImageMetaData;

import java.util.Date;

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
public class BayerTiffFunctionsTestImpl implements BayerTifFunctions {
    private boolean simulateErrors;
    private Date imageDate = new Date();
    private String imageDescription = "";

    public BayerTiffFunctionsTestImpl(boolean simulateErrors) {
        this.simulateErrors = simulateErrors;
    }

    public void setImageDate(Date date) {
        this.imageDate = date;
    }

    public void setImageDescription(String desc) {
        this.imageDescription = desc;
    }

    public ImageMetaData bayerTiffToRgb(String inputFile, String outputFile) throws ImageFunctionsException {
        if(simulateErrors) {
            throw new ImageFunctionsException(1,"Simulated error");
        }
        ImageMetaData result = new ImageMetaData();
        result.setImageDateTime(imageDate);
        result.setBitsPerSample((short) 24); //Bayer color image
        result.setImageDescription(imageDescription);
        return result;
    }


    public ImageMetaData getTifMetaData(String inputFile) throws ImageFunctionsException {
        return bayerTiffToRgb(inputFile,"Dummy");
    }

}
