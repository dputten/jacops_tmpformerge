package imagefunctions;

import imagefunctions.jpg.JpgFunctions;

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
public class JpgFunctionsTestImpl implements JpgFunctions {
    private boolean simulateErrors;

    public JpgFunctionsTestImpl(boolean simulateErrors) {
        this.simulateErrors = simulateErrors;
    }

    @Override
    public void bayerToJpg(String inputFile, int width, int height, int quality, String outputFile) throws ImageFunctionsException {
        if(simulateErrors) {
            throw new ImageFunctionsException(1,"Simulated error");
        }
    }

    public void rgbToJpg(String inputFile, int width, int height, int quality, String outputFile) throws ImageFunctionsException {
        if(simulateErrors) {
            throw new ImageFunctionsException(1,"Simulated error");
        }
    }

    public void grayToJpg(String inputFile, int width, int height, int quality, String outputFile) throws ImageFunctionsException {
        if(simulateErrors) {
            throw new ImageFunctionsException(1,"Simulated error");
        }
    }

    public void jpgToJpg(String inputFile, int width, int height, int quality, String outputFile) throws ImageFunctionsException {
        if(simulateErrors) {
            throw new ImageFunctionsException(1,"Simulated error");
        }
    }
}
