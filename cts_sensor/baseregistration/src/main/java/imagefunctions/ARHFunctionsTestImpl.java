package imagefunctions;

import imagefunctions.ImageFunctionsException;
import imagefunctions.arh.ArhFunctions;
import imagefunctions.bayertif.BayerTifFunctions;
import imagefunctions.bayertif.ImageMetaData;
import imagefunctions.jpg.JpgFunctions;

import java.util.Date;

/**
 * TODO 06022012 RR->RB: move to imagefunctions with classifier at some point :)
 * This class implements the ImageFunctions Interface and is used for testing
 */
public class ARHFunctionsTestImpl implements ArhFunctions {
    private boolean simulateErrors;

    public ARHFunctionsTestImpl(boolean simulateErrors) {
        this.simulateErrors = simulateErrors;
    }

    @Override
    public LicensePlate rgbToArh(String inputFile, String snippetFileName, boolean lastLicencePlate, String modulename, String groupname) throws ImageFunctionsException {
        return rgbToArh(inputFile);
    }

    @Override
    public LicensePlate bayerToArh(String inputFile, String snippetFileName, boolean lastLicencePlate, String modulename, String groupname) throws ImageFunctionsException {
        return rgbToArh(inputFile);
    }

    @Override
    public LicensePlate jpgToArh(String inputFile, String snippetFileName, boolean lastLicencePlate, String modulename, String groupname) throws ImageFunctionsException {
        return rgbToArh(inputFile);
    }

    public LicensePlate rgbToArh(String inputFile) throws ImageFunctionsException {
        if(simulateErrors) {
            throw new ImageFunctionsException(1,"Simulated error");
        }
        LicensePlate result = new LicensePlate();
        result.setRecognizer(LicensePlate.ARH_RECOGNIZER);
        result.setPlateXOffset(0);
        result.setPlateYOffset(0);
        result.setPlateStatus((short)0);
        result.setPlateNumber("95-HDK-1");
        result.setPlateCountryCode(LicensePlate.PLATE_NETHERLANDS);
        result.setPlateConfidence(80);
        result.setLicenseSnippetFileName("");
        return result;
    }


     public LicensePlate jpgToArh(String inputFile) throws ImageFunctionsException {
         return rgbToArh(inputFile);
    }

    @Override
    public String convertARHPlateAuthorityToCountryCode(int i) {
        return "NL";
    }

    public LicensePlate bayerToArh(String inputFile) throws ImageFunctionsException {
        return rgbToArh(inputFile);
    }
}


