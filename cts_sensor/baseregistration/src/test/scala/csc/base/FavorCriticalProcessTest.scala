package csc.base

/*
 * Copyright © CSC
 * Date: Jul 19, 2010
 * Time: 1:25:12 PM
 */

import java.util.concurrent.{ CountDownLatch, TimeUnit }

import _root_.org.junit.Test
import _root_.org.scalatest.junit.JUnitSuite
import base.FavorCriticalProcess
import csc.akka.logging.DirectLogging
import csc.vehicle.message.Priority
import org.scalatest.MustMatchers

class FavorCriticalProcessTest extends JUnitSuite with DirectLogging with MustMatchers {
  /**
   * test 2 threads with NTC and a maximum of 1
   * One thread should be waiting for the other to finish
   */
  @Test
  def test2NTCMax1 {
    val stepControl1 = new SyncSteps
    val stepControl2 = new SyncSteps

    val ntcLock = new FavorCriticalProcess(1)

    val thread1 = new LockThread(stepControl1, ntcLock, Priority.NONCRITICAL)
    val thread2 = new LockThread(stepControl2, ntcLock, Priority.NONCRITICAL)
    thread1.start
    stepControl1.waitSyncPoint(Step.STARTING)
    thread2.start
    stepControl2.waitSyncPoint(Step.STARTING)
    log.info("Start test")
    thread1.step must be(Step.STARTING) //started
    thread2.step must be(Step.STARTING) //started
    log.info("Proces first")
    stepControl1.signalSyncPoint(Step.WAITING)
    stepControl1.waitSyncPoint(Step.PROCESSING)
    thread1.step must be(Step.PROCESSING) //Processing
    thread2.step must be(Step.STARTING) //started
    log.info("Proces second")
    stepControl2.signalSyncPoint(Step.WAITING)
    thread1.step must be(Step.PROCESSING) //Processing
    thread2.step must be(Step.WAITING) //waiting
    log.info("Proces first finished")
    stepControl1.signalSyncPoint(Step.PROCESS_DONE)
    stepControl2.waitSyncPoint(Step.PROCESSING)
    thread1.step must be(Step.DONE) //Done
    thread2.step must be(Step.PROCESSING) //processing
    log.info("Proces second finished")
    stepControl2.signalSyncPoint(Step.PROCESS_DONE)
    thread1.step must be(Step.DONE) //Done
    thread2.step must be(Step.DONE) //Done
    log.info("DONE")
  }
  /**
   * test 2 threads with NTC and a maximum of 2
   * Both threads can process at the same time
   */
  @Test
  def test2NTCMax2 {
    val stepControl1 = new SyncSteps
    val stepControl2 = new SyncSteps

    val ntcLock = new FavorCriticalProcess(2)

    val thread1 = new LockThread(stepControl1, ntcLock, Priority.NONCRITICAL)
    val thread2 = new LockThread(stepControl2, ntcLock, Priority.NONCRITICAL)
    thread1.start
    stepControl1.waitSyncPoint(Step.STARTING)
    thread2.start
    stepControl2.waitSyncPoint(Step.STARTING)
    log.info("Start test")
    thread1.step must be(Step.STARTING) //started
    thread2.step must be(Step.STARTING) //started
    log.info("Proces first")
    stepControl1.signalSyncPoint(Step.WAITING)
    stepControl1.waitSyncPoint(Step.PROCESSING)
    thread1.step must be(Step.PROCESSING) //Processing
    thread2.step must be(Step.STARTING) //started
    log.info("Proces second")
    stepControl2.signalSyncPoint(Step.WAITING)
    stepControl2.waitSyncPoint(Step.PROCESSING)
    thread1.step must be(Step.PROCESSING) //Processing
    thread2.step must be(Step.PROCESSING) //Processing
    log.info("Proces first finished")
    stepControl1.signalSyncPoint(Step.PROCESS_DONE)
    thread1.step must be(Step.DONE) //Done
    thread2.step must be(Step.PROCESSING) //processing
    log.info("Proces second finished")
    stepControl2.signalSyncPoint(Step.PROCESS_DONE)
    thread1.step must be(Step.DONE) //Done
    thread2.step must be(Step.DONE) //Done
    log.info("DONE")
  }
  /**
   * test 2 threads with TC and a maximum of 1
   * Both threads can start proces at the same time
   */
  @Test
  def test2TCMax1 {
    val stepControl1 = new SyncSteps
    val stepControl2 = new SyncSteps

    val ntcLock = new FavorCriticalProcess(1)

    val thread1 = new LockThread(stepControl1, ntcLock, Priority.CRITICAL)
    val thread2 = new LockThread(stepControl2, ntcLock, Priority.CRITICAL)
    thread1.start
    stepControl1.waitSyncPoint(Step.STARTING)
    thread2.start
    stepControl2.waitSyncPoint(Step.STARTING)
    log.info("Start test")
    thread1.step must be(Step.STARTING) //started
    thread2.step must be(Step.STARTING) //started
    log.info("Proces first")
    stepControl1.signalSyncPoint(Step.WAITING)
    stepControl1.waitSyncPoint(Step.PROCESSING)
    thread1.step must be(Step.PROCESSING) //Processing
    thread2.step must be(Step.STARTING) //started
    log.info("Proces second")
    stepControl2.signalSyncPoint(Step.WAITING)
    stepControl2.waitSyncPoint(Step.PROCESSING)
    thread1.step must be(Step.PROCESSING) //Processing
    thread2.step must be(Step.PROCESSING) //Processing
    log.info("Proces first finished")
    stepControl1.signalSyncPoint(Step.PROCESS_DONE)
    thread1.step must be(Step.DONE) //Done
    thread2.step must be(Step.PROCESSING) //processing
    log.info("Proces second finished")
    stepControl2.signalSyncPoint(Step.PROCESS_DONE)
    thread1.step must be(Step.DONE) //Done
    thread2.step must be(Step.DONE) //Done
    log.info("DONE")
  }

  /**
   * Start first TC and then NTC
   * The NTC should have to wait until the first proces has finished
   */
  @Test
  def testTC_NTCMax1 {
    val stepControl1 = new SyncSteps
    val stepControl2 = new SyncSteps

    val ntcLock = new FavorCriticalProcess(1)

    val thread1 = new LockThread(stepControl1, ntcLock, Priority.CRITICAL)
    val thread2 = new LockThread(stepControl2, ntcLock, Priority.NONCRITICAL)
    thread1.start
    stepControl1.waitSyncPoint(Step.STARTING)
    thread2.start
    stepControl2.waitSyncPoint(Step.STARTING)
    log.info("Start test")
    thread1.step must be(Step.STARTING) //started
    thread2.step must be(Step.STARTING) //started
    log.info("Proces first")
    stepControl1.signalSyncPoint(Step.WAITING)
    stepControl1.waitSyncPoint(Step.PROCESSING)
    thread1.step must be(Step.PROCESSING) //Processing
    thread2.step must be(Step.STARTING) //started
    log.info("Proces second")
    stepControl2.signalSyncPoint(Step.WAITING)
    thread1.step must be(Step.PROCESSING) //Processing
    thread2.step must be(Step.WAITING) //waiting
    log.info("Proces first finished")
    stepControl1.signalSyncPoint(Step.PROCESS_DONE)
    stepControl2.waitSyncPoint(Step.PROCESSING)
    thread1.step must be(Step.DONE) //Done
    thread2.step must be(Step.PROCESSING) //processing
    log.info("Proces second finished")
    stepControl2.signalSyncPoint(Step.PROCESS_DONE)
    thread1.step must be(Step.DONE) //Done
    thread2.step must be(Step.DONE) //Done
    log.info("DONE")
  }

  /**
   * Start first NTC and then TC
   * Bothe threads can process at the same time
   */
  @Test
  def testNTC_TCMax1 {
    val stepControl1 = new SyncSteps
    val stepControl2 = new SyncSteps

    val ntcLock = new FavorCriticalProcess(1)

    val thread1 = new LockThread(stepControl1, ntcLock, Priority.NONCRITICAL)
    val thread2 = new LockThread(stepControl2, ntcLock, Priority.CRITICAL)
    thread1.start
    stepControl1.waitSyncPoint(Step.STARTING)
    thread2.start
    stepControl2.waitSyncPoint(Step.STARTING)
    log.info("Start test")
    thread1.step must be(Step.STARTING) //started
    thread2.step must be(Step.STARTING) //started
    log.info("Proces first")
    stepControl1.signalSyncPoint(Step.WAITING)
    stepControl1.waitSyncPoint(Step.PROCESSING)
    thread1.step must be(Step.PROCESSING) //Processing
    thread2.step must be(Step.STARTING) //started
    log.info("Proces second")
    stepControl2.signalSyncPoint(Step.WAITING)
    stepControl2.waitSyncPoint(Step.PROCESSING)
    thread1.step must be(Step.PROCESSING) //Processing
    thread2.step must be(Step.PROCESSING) //Processing
    log.info("Proces first finished")
    stepControl1.signalSyncPoint(Step.PROCESS_DONE)
    thread1.step must be(Step.DONE) //Done
    thread2.step must be(Step.PROCESSING) //processing
    log.info("Proces second finished")
    stepControl2.signalSyncPoint(Step.PROCESS_DONE)
    thread1.step must be(Step.DONE) //Done
    thread2.step must be(Step.DONE) //Done
    log.info("DONE")
  }
  /**
   * Start first both NTC and then TC
   * Finish first NTC and then the TC
   * One NTC can process (first) and the other should wait and the TC can process also.
   */
  @Test
  def test2NTC_TCMax1 {
    val stepControl1 = new SyncSteps
    val stepControl2 = new SyncSteps
    val stepControl3 = new SyncSteps

    val ntcLock = new FavorCriticalProcess(1)

    val thread1 = new LockThread(stepControl1, ntcLock, Priority.NONCRITICAL)
    val thread2 = new LockThread(stepControl2, ntcLock, Priority.NONCRITICAL)
    val thread3 = new LockThread(stepControl3, ntcLock, Priority.CRITICAL)
    thread1.start
    stepControl1.waitSyncPoint(Step.STARTING)
    thread2.start
    stepControl2.waitSyncPoint(Step.STARTING)
    thread3.start
    stepControl3.waitSyncPoint(Step.STARTING)
    //start test
    thread1.step must be(Step.STARTING) //started
    thread2.step must be(Step.STARTING) //started
    thread3.step must be(Step.STARTING) //started
    // Proces first NTC
    stepControl1.signalSyncPoint(Step.WAITING)
    stepControl1.waitSyncPoint(Step.PROCESSING)
    thread1.step must be(Step.PROCESSING) //Processing
    thread2.step must be(Step.STARTING) //started
    thread3.step must be(Step.STARTING) //started
    //Proces second NTC
    stepControl2.signalSyncPoint(Step.WAITING)
    thread1.step must be(Step.PROCESSING) //Processing
    thread2.step must be(Step.WAITING) //waiting
    thread3.step must be(Step.STARTING) //started
    //Proces TC
    stepControl3.signalSyncPoint(Step.WAITING)
    stepControl3.waitSyncPoint(Step.PROCESSING)
    thread1.step must be(Step.PROCESSING) //Processing
    thread2.step must be(Step.WAITING) //waiting
    thread3.step must be(Step.PROCESSING) //Processing
    //finish NTC (second should still wait for the TC to finish
    stepControl1.signalSyncPoint(Step.PROCESS_DONE)
    thread1.step must be(Step.DONE) //finished
    thread2.step must be(Step.WAITING) //waiting
    thread3.step must be(Step.PROCESSING) //Processing
    //finish TC (second NTC shoul start processing
    stepControl3.signalSyncPoint(Step.PROCESS_DONE)
    stepControl2.waitSyncPoint(Step.PROCESSING)
    thread1.step must be(Step.DONE) //finished
    thread2.step must be(Step.PROCESSING) //Processing
    thread3.step must be(Step.DONE) //finished
    //finish second NTC
    stepControl2.signalSyncPoint(Step.PROCESS_DONE)
    thread1.step must be(Step.DONE) //finished
    thread2.step must be(Step.DONE) //finished
    thread3.step must be(Step.DONE) //finished
    //Done
  }

}

object Step extends Enumeration {
  type Step = Value
  val INIT = Value(0)
  val STARTING = Value(1)
  val WAITING = Value(2)
  val PROCESSING = Value(3)
  val PROCESS_DONE = Value(4)
  val DONE = Value(5)
}

class SyncSteps() {
  val ar = new Array[CountDownLatch](Step.maxId)
  for (s ← Step.values) {
    ar(s.id) = new CountDownLatch(1)
  }

  def waitSyncPoint(step: Step.Value) {
    ar(step.id).await
  }
  def waitSyncPoint(step: Step.Value, secTimeout: Long) {
    ar(step.id).await(secTimeout, TimeUnit.SECONDS)
  }
  def signalSyncPoint(step: Step.Value) {
    ar(step.id).countDown
    //TODO sleep is necessary to run the other thread
    Thread.sleep(100)
  }

}

/**
 * Test tread used to control the duration and starts of the different steps
 */
class LockThread(sync: SyncSteps, ntcLock: FavorCriticalProcess, timeCritical: Priority.Value) extends Thread {
  var step = Step.INIT
  override def run = {
    step = Step.STARTING
    sync.signalSyncPoint(Step.STARTING)
    sync.waitSyncPoint(Step.WAITING)
    step = Step.WAITING
    ntcLock.processGuardedResource(timeCritical, {
      step = Step.PROCESSING
      sync.signalSyncPoint(Step.PROCESSING)
      sync.waitSyncPoint(Step.PROCESS_DONE)
    })
    step = Step.DONE
  }
}
