package csc.vehicle

import org.scalatest.WordSpec
import org.scalatest._

/**
 * Created by carlos on 15/01/16.
 */
class VehicleInfoTest extends WordSpec with MustMatchers {

  "VehicleInfo" must {
    "parse from empty string" in {
      val result = VehicleInfo("")
      result must be(None)
    }

    "parse from string containing noplate" in {
      val result = VehicleInfo("/path/wewwenoplatererere.jpg")
      result must be(None)
    }

    "parse from a string in legacy format" in {
      val result = VehicleInfo("/path/aaaaaaaaaaaaaa00_00_00-TYPE-NL-90-80-.jpg")
      result.size must be(1)
      result must be(Some(VehicleInfo("00_00_00", "TYPE", "NL", 90, 80)))
    }

    "parse from version 1 string (single plate)" in {
      val result = VehicleInfo("/path/prefixxpto#XP1#AA_AA_AA,Car,NL,95,90#.jpg")
      result.size must be(1)
      result must be(Some(VehicleInfo("AA_AA_AA", "Car", "NL", 95, 90)))
    }

    "parse from version 1 string (2 plates and none active)" in {
      val result = VehicleInfo("/path/4234234#XP1#AA_AA_AA,Car,NL,95,90|BB_BB_BB,Truck,PL,85,80#comments.jpg")
      result must be(Some(VehicleInfo("AA_AA_AA", "Car", "NL", 95, 90)))
      //result(1) must be(VehicleInfo("BB_BB_BB","Truck","PL",85,80))
    }

    "parse from version 1 string (2 plates and 2nd active)" in {
      val result = VehicleInfo("/path/4234234#XP1#AA_AA_AA,Car,NL,95,90,_L|BB_BB_BB,Truck,PL,85,80,_R#comments_R.jpg")
      //result must be(VehicleInfo("AA_AA_AA","Car","NL",95,90))
      result must be(Some(VehicleInfo("BB_BB_BB", "Truck", "PL", 85, 80, Some("_R"))))
    }

  }
}
