package csc.vehicle.message

import imagefunctions.{ ImageFunctionsFactory, LicenseFormat }
import net.liftweb.json.{ DefaultFormats, Serialization }
import org.scalatest.WordSpec
import org.scalatest._

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

class JsonConversionTest extends WordSpec with MustMatchers {

  implicit val formats = DefaultFormats

  "test" must {
    "preserve umlauts" in {

      val mylane = new Lane(
        laneId = "id",
        name = "name",
        gantry = "gantry",
        system = "system",
        sensorGPS_longitude = 5,
        sensorGPS_latitude = 52)

      ImageFunctionsFactory.setLicenceFormat(LicenseFormat.UMLAUTS)
      val lic = Some(new ValueWithConfidence[String]("Ü2\u00DC", 40))

      val certData = CertificationData(
        Map("NMICertificate" -> "TP123", "applChecksum" -> "dasdas"),
        Map("serialNr" -> "SN1234"))

      val veh = new VehicleMetadata(
        recordVersion = Version.withCertificationData,
        lane = mylane,
        eventId = "id123",
        eventTimestamp = 111111L,
        eventTimestampStr = Some("asffsdf"),
        length = None,
        category = None,
        speed = None,
        images = Seq(),
        certificationData = Some(certData))

      val json = Serialization.write(veh)
      val mybytes = json.getBytes()
      val metaObj = Serialization.read[VehicleMetadata](new String(mybytes))
      metaObj must be(veh)
    }
  }

}