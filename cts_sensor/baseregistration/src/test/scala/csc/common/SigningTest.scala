/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.common

import org.junit.Test
import org.scalatest.MustMatchers
import org.scalatest.junit.JUnitSuite

/**
 * Test the signing object
 */
class SigningTest extends JUnitSuite with MustMatchers {
  /**
   * Test the SHA256 expected is done by using sha256sum command
   */
  @Test
  def testSha2 {
    val fileName = getClass.getClassLoader.getResource("alfa.tif").getPath
    Signing.digest(fileName, "SHA-256") must be("d8227405571a273d75ac872ec367f14e8143bf26d0ce626ac152d0ea93297d7c")
  }
  /**
   * Test the SHA expected is done by using shasum command
   */
  @Test
  def testMd5 {
    val fileName = getClass.getClassLoader.getResource("alfa.tif").getPath
    Signing.digest(fileName) must be("b8bd3e8cfdc0bb4f94be6fe14ba112df")
  }
}