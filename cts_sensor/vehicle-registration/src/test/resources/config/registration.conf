vehicle-registration {
remotefix.maxMessageLength=500
remotefix.sendUri="tcp://localhost:12550"

    location {
        systemId = "A4"
        gantryIds = ["E1","X1","E2","X2"]
    }

    #JMX configuration parameters
    jmx {
        service=off                                        #Should the JMX service be activated Options: on, off
        host="0.0.0.0"                                     #Host listen address
        accessFile="config/jmxremote.access"               #The file where the permissions are defined
        passwordFile="config/jmxremote.password"           #The file where the user and password are defined
        jmxPort=9998                                       #The JMX port used to connect to get JMX information
        rmiPort=9999                                       #The RMI port used to connect to with JMX tooling
    }
    amqp {
        service = on
        amqp-uri = "amqp://ctes:ctes@localhost:5672/"
        actor-name = "vehicle-registration-actor"
        reconnect-delay = 5000ms
    }
    certificate-check {
      service = off
      certificateComponentName="vehicleregistration"
      use-amqp = "on"
      registration-receiver-endpoint = "gantry-out-exchange"
      registration-receiver-route-key = "A4.E1.VehicleRegistrationCertificate"
    }
    laneConfig {
        configUpdateFrequency = 60000
        zookeeper {
            service=off
            servers="localhost"
            sessionTimeout=1000
            connectTimeout=1000
            baseSleep=500
            maxRetries=100
            labelPath=""
        }
        file {
            service=on
            fileName="/app/laneconfig.json"
        }
    }

    certificate {
        nmi="XXX-XXXX"
    }
    joining {
        maxBufferLen=1000
        shiftPeriod=0
        bufferTimeout=1200
    }
    relaySensorData {
        uri="mina:tcp://localhost:12400?textline=true&sync=false"
    }
    readImage {
        testImagePattern="DataID=00FF"
    }
    sign {
        service="off"
    }
    mask {                     #Apply mask to overview image
        service="off"          #turn the mask functionality on or off
        cropEnabled=true       #crop the image after the mask is added
        outputDir="jpg"        #directory where the new image is stored
        quality=80             #the quality of the new image
    }
    decorate {
        quality=80
    }
    exif {
        service="off"
        outputDir="/data/images/jpg"
        writeJsonToComment=true
        targets {
            OVERVIEW {
                targetVersion=DECORATED
            }
        }
    }
    guards {
        #Default empty
    }
    doorDetector {
        #example expected message "<172>Jan 01 01:58:39 192.168.127.253 INFO:DI 1 transition (On -> Off)"
        expression="<\\d*>(\\S* \\d{1,2} \\d{1,2}:\\d{1,2}:\\d{1,2}) (\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b) INFO:DI 1 transition \\((On|Off) -> (On|Off)\\)"
        uri="mina:udp://0.0.0.0:12500?sync=false"
        doorOpenSignal="On" #[On|Off]
        description="default config"
        host="unknown"
        port=-1
    }

    recognizer {
#        recognizeActor="akka://RecognizeProcessor@127.0.0.1:25552/user/recognize"
          recognizeActor="/user/StartManager/RemoteFix"
        maxRetries=10
        timeout=120000000

        watch {
            service="on"
            serviceCmd="/usr/sbin/service"
            serviceName="recognize-processor"
            frequency=60000
            ping {
                service="on"
                remoteActor="${vehicle-registration.recognizer.recognizeActor}"
                timeout=50000
                failureLimit=5
            }
        }
    }
    jpg {
        outputDir="/data/images/jpg"
        conversions {
            OVERVIEW {
                enabled=false                                            #Is the jpeg conversion enabled for the overview image.
                width = 0                                               #The width of the created jpeg overview image
                height = 0                                              #The height of the created jpeg overview image
                quality = 60                                           #The quality of the created jpeg overview image. This influences the size of the file
            }
            OVERVIEWMASKED {
                enabled=false                                           #Is the jpeg conversion enabled for the overview image.
                width = 0                                               #The width of the created jpeg overview image
                height = 0                                              #The height of the created jpeg overview image
                quality = 100                                           #The quality of the created jpeg overview image. This influences the size of the file
            }
            OVERVIEWREDLIGHT {
                enabled=false                                           #Is the jpeg conversion enabled for the overview image (redlight).
                width = 0                                               #The width of the created jpeg overview image
                height = 0                                              #The height of the created jpeg overview image
                quality = 100                                           #The quality of the created jpeg overview image. This influences the size of the file
            }
            OVERVIEWSPEED {
                enabled=false                                           #Is the jpeg conversion enabled for the overview image (speed).
                width = 0                                               #The width of the created jpeg overview image
                height = 0                                              #The height of the created jpeg overview image
                quality = 100                                           #The quality of the created jpeg overview image. This influences the size of the file
            }
            LICENSE {
                enabled=false                                            #Is the jpeg conversion enabled for the license image. (Currently ignored. Always assumed to be true.)
                width = 0                                               #The width of the created jpeg license image. 0 means use width from original file
                height = 0                                              #The height of the created jpeg license image. 0 means use height from original file
                quality = 100                                           #The quality of the created jpeg license image. This influences the size of the file
            }
            REDLIGHT {
                enabled=false                                           #Is the jpeg conversion enabled for the redlight image. (Currently not used)
                width = 0                                               #The width of the created jpeg redlight image. 0 means use width from original file
                height = 0                                              #The height of the created jpeg redlight image. 0 means use height from original file
                quality = 100                                           #The quality of the created jpeg redlight image. This influences the size of the file
            }
            MEASUREMETHOD2SPEED {
                enabled=false                                           #Is the jpeg conversion enabled for the measureMethod2 image. (Currently not used)
                width = 0                                               #The width of the created jpeg measureMethod2 image. 0 means use width from original file
                height = 0                                              #The height of the created jpeg measureMethod2 image. 0 means use height from original file
                quality = 100                                           #The quality of the created jpeg measureMethod2 image. This influences the size of the file
            }
        }
    }
    completion {
        hbase {
            service="off"
            servers="localhost"
            tableName="vehicle"
        }
        amqp {
            service = "on"
            image-store-endpoint = "gantry-out-exchange"
            image-store-route-key = "A4.E1.ImagePutRequest"
            registration-receiver-endpoint = "gantry-out-exchange"
            registration-receiver-route-key = "A4.E1.VehicleMetaData"
            heartbeat-rate = 10s
            delay-alpha = 0.875
            delay-margin = 60000
        }
        file {
            service="off"
            outputDir="/data/images/outputStore"
        }
    }

  multiLaneCameras = [
    {
      uri = "/data/images/raw/CAM1",
      rightLaneId = "lane1",
      leftLaneId = "lane2"
    }
  ]

}

curator_implementation="xml"
curator_implementation_xml_file_location="/app/ZKexportPRD.xml"



