rootDir="${ROOTDIR}"
vehicle-registration {
    #JMX configuration parameters
    jmx {
     service=off                                        #Should the JMX service be activated Options: on, off
    }

    joining {
        maxBufferLen=22
    }
    relaySensorData {
        uri="mina:tcp://localhost:12403?textline=true&sync=false"
    }
    guards {
    }
    recognizer.recognizeActor = "/user/recognize"
  jpg {
    outputDir="${rootDir}/out/irose-startup-test/jpgDir"
    conversions {
            OVERVIEW {
                enabled=false                                           #Is the jpeg conversion enabled for the overview image.
                width = 0                                               #The width of the created jpeg overview image
                height = 0                                              #The height of the created jpeg overview image
                quality = 100                                           #The quality of the created jpeg overview image. This influences the size of the file
            }
            OVERVIEWMASKED {
                enabled=false                                           #Is the jpeg conversion enabled for the overview image.
                width = 0                                               #The width of the created jpeg overview image
                height = 0                                              #The height of the created jpeg overview image
                quality = 100                                           #The quality of the created jpeg overview image. This influences the size of the file
            }
            OVERVIEWREDLIGHT {
                enabled=false                                           #Is the jpeg conversion enabled for the overview image (redlight).
                width = 0                                               #The width of the created jpeg overview image
                height = 0                                              #The height of the created jpeg overview image
                quality = 100                                           #The quality of the created jpeg overview image. This influences the size of the file
            }
            OVERVIEWSPEED {
                enabled=false                                           #Is the jpeg conversion enabled for the overview image (speed).
                width = 0                                               #The width of the created jpeg overview image
                height = 0                                              #The height of the created jpeg overview image
                quality = 100                                           #The quality of the created jpeg overview image. This influences the size of the file
            }
            LICENSE {
                enabled=true                                            #Is the jpeg conversion enabled for the license image. (Currently ignored. Always assumed to be true.)
                width = 0                                               #The width of the created jpeg license image. 0 means use width from original file
                height = 0                                              #The height of the created jpeg license image. 0 means use height from original file
                quality = 100                                           #The quality of the created jpeg license image. This influences the size of the file
            }
            REDLIGHT {
                enabled=false                                           #Is the jpeg conversion enabled for the redlight image. (Currently not used)
                width = 0                                               #The width of the created jpeg redlight image. 0 means use width from original file
                height = 0                                              #The height of the created jpeg redlight image. 0 means use height from original file
                quality = 100                                           #The quality of the created jpeg redlight image. This influences the size of the file
            }
            MEASUREMETHOD2SPEED {
                enabled=false                                           #Is the jpeg conversion enabled for the measureMethod2 image. (Currently not used)
                width = 0                                               #The width of the created jpeg measureMethod2 image. 0 means use width from original file
                height = 0                                              #The height of the created jpeg measureMethod2 image. 0 means use height from original file
                quality = 100                                           #The quality of the created jpeg measureMethod2 image. This influences the size of the file
            }
    }
  }
  decorate {
     service="on"
     outputDir="${rootDir}/out/irose-startup-test/jpgDir"
     quality=80
     licenseSize=400                          #Size of image in decoration (= width for landscape, height for portrait)
     licenseQuality=80                        #Jpeg quality of image in decoration
     replaceLicenseWithOriginal=true          #Should license in the image be replaced by the version from original image
     showLicenseFrame=true                    #Show a frame around the license in the image
     licenseFrameThickness=6                  #The thickness of license frame
     licenseFrameColorRGB {                   #Color of the license frame
        R=30
        G=225
        B=30
     }
     showLogo=false                           #Show CSC logo in decoration
     fontName="Courier"                       #Font name
     fontColorRGB {                           #Font color
         R=50
         G=50
         B=50
     }

     imageCorrections {
             correction_1{
                 from="00:00:00"
                 to="23:59:59"
                 autoLevel {
                   blackStart=1
                   blackMargin=0.5
                   whiteStart=254
                   whiteMargin=2.0
                 }
             }
     }
     decorations {
            OVERVIEW {
                enabled=false                    #Is the decoration of the overview image enabled
                decorationType=OVERVIEW_1        #Type of decoration. [OVERVIEW_1, SPEED_1, SPEED_2, REDLIGHT_1, REDLIGHT_2]
                imageCorrections=[]
            }
            REDLIGHT {
                enabled=true                     #Is the decoration of the overview image enabled
                decorationType=REDLIGHT_1        #Type of decoration. [OVERVIEW_1, SPEED_1, SPEED_2, REDLIGHT_1, REDLIGHT_2]
                imageCorrections=[correction_1]
            }
            OVERVIEWREDLIGHT {
                enabled=true                     #Is the decoration of the overview image enabled
                decorationType=REDLIGHT_2        #Type of decoration. [OVERVIEW_1, SPEED_1, SPEED_2, REDLIGHT_1, REDLIGHT_2]
                imageCorrections=[correction_1]
            }
            OVERVIEWSPEED {
                enabled=true                     #Is the decoration of the overview image enabled
                decorationType=SPEED_1           #Type of decoration. [OVERVIEW_1, SPEED_1, SPEED_2, REDLIGHT_1, REDLIGHT_2]
                imageCorrections=[correction_1]
            }
            MEASUREMETHOD2SPEED {
                enabled=true                     #Is the decoration of the overview image enabled
                decorationType=SPEED_2           #Type of decoration. [OVERVIEW_1, SPEED_1, SPEED_2, REDLIGHT_1, REDLIGHT_2]
                imageCorrections=[correction_1]
            }
     }
  }
  exif {
     service="on"
     outputDir="${rootDir}/out/irose-startup-test/jpgDir"
     writeJsonToComment=true
     targets {
        REDLIGHT {
           targetVersion=DECORATED
        }
        OVERVIEWREDLIGHT {
           targetVersion=DECORATED
        }
        OVERVIEWSPEED {
           targetVersion=DECORATED
        }
        MEASUREMETHOD2SPEED {
           targetVersion=DECORATED
        }
     }
  }
  mask {                                       #Apply mask to overview image
    service="on"
    cropEnabled=true
    outputDir="${rootDir}/out/irose-startup-test/jpgDir"
    quality=80
  }
  completion {
    hbase {
        service=off
    }
    file {
        service=on
        outputDir="${rootDir}/out/irose-startup-test/outputStoreDir"
    }
  }
}