package imagefunctions;
/*
 * Copyright © CSC
 * Date: Jun 8, 2010
 * Time: 12:18:11 PM
 */

import imagefunctions.arh.ArhFunctions;
import org.junit.Assert;
import junit.framework.TestCase;
import testframe.Resources;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * TODO 06022012 RR->RB: Move to imagefunctions?
 * Test the rgbToArh call to the ImageFunction implementation.
 */
public class TifToArhTest extends TestCase {
    /**
     * Test the happy flow of the rgbToArh call.
     */
    public void testOKSequentially() {
        try {
            ImageFunctionsFactory.setDefaultFunctions();
        } catch(InstantiationException ex) {
         Assert.fail(ex.getMessage());
        }

        // test finding licenseplate a couple of times sequentially
        for (int i=0;i<10;i++)
        {
            File input = Resources.getResourceFile("alfaRGB.tif");
            Assert.assertTrue("Input file doesn't exists: " + input.getAbsolutePath(), input.exists());
            ArhFunctions clib = ImageFunctionsFactory.getARHFunctions();
            LicensePlate license = clib.rgbToArh(input.getAbsolutePath());
            Assert.assertNotNull("return value is null", license);
            Assert.assertEquals("license", "18LKRK", license.getPlateNumber());
            Assert.assertTrue("plateConfidence needs to be higher than zero", 0 <= license.getPlateConfidence());
            Assert.assertEquals("country should be Netherlands", LicensePlate.PLATE_NETHERLANDS, license.getPlateCountryCode());
            File output = new File(license.getLicenseSnippetFileName());
            Assert.assertEquals("filename", "alfaRGB.tif_plate_arh.tif", output.getName());
            Assert.assertTrue("Output file doesn't exists: " + output.getAbsolutePath(), output.exists());
            output.delete();
        }

    }
    // TODO: add longrunning recognize test, 2/3 images per second with randomization

    /**
     * Test the several different images, plates and countries of the rgbToArh call.
     */
    public void testOKFunctional()
    {

        try {
            ImageFunctionsFactory.setDefaultFunctions();
        } catch(InstantiationException ex) {
         Assert.fail(ex.getMessage());
        }
        ArhFunctions clib = ImageFunctionsFactory.getARHFunctions();

        File folder = new File(Resources.getResourceDirPath(),"functional");
        File[] listOfFiles = folder.listFiles();
        // loop over all subfolders in the functional directory and process them
        for (int i = 0; i < listOfFiles.length; i++)
        {
          if (listOfFiles[i].isDirectory())
          {
            File subfolder = new File(listOfFiles[i].getAbsolutePath());
            processFolder(subfolder,clib);
          }
        }
    }

    private void processFolder(File folder,ArhFunctions clib)
    {
        String line;
        String first = "";  // first line
        String second = "";               // second line
        ArrayList rows = new ArrayList();

        File[] listOfFiles = folder.listFiles();
        // loop over all tif files
        for (int i = 0; i < listOfFiles.length; i++) {
          if (listOfFiles[i].isFile() && listOfFiles[i].getName().endsWith(".tif"))
          {
            // read txt file in order to get license text and country to be detected
            try
            {
                BufferedReader in = new BufferedReader(new FileReader(listOfFiles[i].getAbsolutePath().replace(".tif",".txt")));
                if (!in.ready())
                    throw new IOException();
                if ((line = in.readLine()) != null) first = line;
                if ((line = in.readLine()) != null) second = line;
                in.close();
            }
            catch (IOException e)
            {
                System.out.println(e);
            }

              File input = new File(listOfFiles[i].getAbsolutePath());
              Assert.assertTrue("Input file doesn't exists: " + input.getAbsolutePath(), input.exists());

              LicensePlate license = clib.rgbToArh(listOfFiles[i].getAbsolutePath());

              Assert.assertNotNull("return value is null", license);
              Assert.assertEquals("license", first, license.getPlateNumber());
              Assert.assertTrue("plateConfidence needs to be higher than zero", 0 <= license.getPlateConfidence());
              Assert.assertEquals("country should be Netherlands", LicensePlate.PLATE_NETHERLANDS, license.getPlateCountryCode());

              File output = new File(license.getLicenseSnippetFileName());
              Assert.assertEquals("filename", listOfFiles[i].getName() + "_plate_arh.tif", output.getName());
              Assert.assertTrue("Output file doesn't exists: " + output.getAbsolutePath(), output.exists());
              output.delete();

          }
        }

    }

    /**
     * Test the happy flow of the rgbToArh call.
     */
    public void testOKParallel()
    {
        final int N=5;  // number of threads to start and wait for
        final CyclicBarrier barrier = new CyclicBarrier(N+1);

        class Worker implements Runnable {
          String filename;
          Worker(String filename) { this.filename = filename; } 
          public void run() {
            try {
                File input = new File(this.filename);
                Assert.assertTrue("Input file doesn't exists: " + input.getAbsolutePath(), input.exists());
                ImageFunctionsFactory.setDefaultFunctions();
                ArhFunctions clib = ImageFunctionsFactory.getARHFunctions();
                LicensePlate license = clib.rgbToArh(input.getAbsolutePath());
                Assert.assertNotNull("return value is null", license);
                if (this.filename.endsWith("alfaRGB2.tif") )
                {
                    Assert.assertEquals("license", "36PVLX", license.getPlateNumber());
                }
                else
                {
                  Assert.assertEquals("license", "18LKRK", license.getPlateNumber());
                }
                Assert.assertTrue("plateConfidence needs to be higher than zero", 0 <= license.getPlateConfidence());
                Assert.assertEquals("country should be Netherlands", LicensePlate.PLATE_NETHERLANDS, license.getPlateCountryCode());
                File output = new File(license.getLicenseSnippetFileName());
                Assert.assertTrue("Output file doesn't exists: " + output.getAbsolutePath(), output.exists());
                output.delete();
            } catch(InstantiationException ex) {
             Assert.fail(ex.getMessage());
            } finally {

                try {
                  barrier.await();
                } catch (InterruptedException ex) {
                  return;
                } catch (BrokenBarrierException ex) {
                  return;
                }
              }
          }
        }
        File dir = Resources.getResourceDirPath();
        String[] filenames = {dir + "/alfaRGB1.tif",
                dir + "/alfaRGB2.tif",
                dir + "/alfaRGB3.tif",
                dir + "/alfaRGB4.tif",
                dir + "/alfaRGB5.tif",
                dir + "/alfaRGB6.tif",
                dir + "/alfaRGB7.tif",
                dir + "/alfaRGB8.tif",
                dir + "/alfaRGB9.tif",
                dir + "/alfaRGB10.tif"};
        // test finding licenseplate a couple of times in parallel
        boolean first = true;
        for (int i = 0; i < N; ++i)
        {
          new Thread(new Worker(filenames[i])).start();
          // only wait a while after starting the first thread (to see the initialization speed up)
          if (first)
          {
            first = false;
            try{
              Thread.sleep(10000);
            }
            catch (Exception e){
            }
          }
        }
        try {
          barrier.await(1, TimeUnit.MINUTES);
        } catch (TimeoutException ex) {
          return;
        } catch (InterruptedException ex) {
          return;
        } catch (BrokenBarrierException ex) {
          return;
        }
    }

    /**
     * a test with an image with no license plate
     */
    public void testNoPlateInImage() {

        File input = Resources.getResourceFile("blank.tif");
        Assert.assertTrue("Input file doesn't exists: " + input.getAbsolutePath(), input.exists());
        try {
            ImageFunctionsFactory.setDefaultFunctions();
        } catch(InstantiationException ex) {
         Assert.fail(ex.getMessage());
        }
        ArhFunctions clib = ImageFunctionsFactory.getARHFunctions();

        LicensePlate license = clib.rgbToArh(input.getAbsolutePath());
        Assert.assertNotNull("return value is null", license);
        Assert.assertEquals("plate status", LicensePlate.PLATESTATUS_NO_PLATE_FOUND, license.getPlateStatus());
        Assert.assertEquals("license", "", license.getPlateNumber());
        Assert.assertEquals("confidence", 0, license.getPlateConfidence());
        Assert.assertEquals("authority (thus country) should be 0/unknown", LicensePlate.PLATE_UNKNOWN, license.getPlateCountryCode());
        Assert.assertEquals("snippet filename should not be set", "", license.getLicenseSnippetFileName());

        File plate = new File("src/test/resources/blank.tif_plate_arh.tif");
        Assert.assertTrue("Plate file does exist: " + plate.getAbsolutePath(), !plate.exists());

    }

    /**
     * a test with an non existing image
     */
    public void testNoImage() {
        Exception exceptionThrown = null;
        File input = new File("src/test/resources/xxxx.tif");
        Assert.assertTrue("Input file exists: " + input.getAbsolutePath(), !input.exists());
        try{
            ImageFunctionsFactory.setDefaultFunctions();
            ArhFunctions clib = ImageFunctionsFactory.getARHFunctions();
            LicensePlate license = clib.rgbToArh(input.getAbsolutePath());
        } catch (ImageFunctionsException e){
            exceptionThrown = e;
        } catch(InstantiationException ex) {
         Assert.fail(ex.getMessage());
        }
        Assert.assertEquals("exception", "Status: -2 Problem Loading Image", exceptionThrown.getMessage());
    }
}