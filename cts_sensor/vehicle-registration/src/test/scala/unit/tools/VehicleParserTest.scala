/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package unit.tools

import org.scalatest.WordSpec
import org.scalatest._
import csc.vehicle.tools.vehicle._
import org.apache.commons.io.FileUtils
import testframe.Resources

class VehicleParserTest extends WordSpec with MustMatchers {

  "vehicle parser" must {

    "read comment line" in {
      var parserOutput = VehicleParser.parseScript("#100 KS_VERS_PROT=10 KS_LOCATION=0123456\t KS_VERS_HW=3244AD1")
      parserOutput must have size (1)
      val comment = parserOutput.head
      comment match {
        case script: ScriptLine ⇒ script.command must be(SimConstants.COMMENT)
        case _                  ⇒ fail("Unexpected type")
      }
    }
    "read configuration line" in {
      var parserOutput = VehicleParser.parseScript("CONFIG-START\nLANE RELAY-DIR=test\nCONFIG-END")
      parserOutput must have size (1)
      val comment = parserOutput.head
      comment match {
        case config: ConfigLine ⇒ config.options must be(Map("RELAY-DIR" -> "test"))
        case obj: AnyRef        ⇒ fail("Unexpected type " + obj.getClass.getName)
      }
    }
    "read script line" in {
      var parserOutput = VehicleParser.parseScript("SCRIPT-START\n1000 LANE-REGISTRATION IMAGE=test\nSCRIPT-END\n")
      parserOutput must have size (1)
      val comment = parserOutput.head
      comment match {
        case script: ScriptLine ⇒ {
          script.command must be("LANE-REGISTRATION")
          script.options must be(Map("IMAGE" -> "test"))
        }
        case _ ⇒ fail("Unexpected type")
      }
    }
    "read multiplelines" in {
      var parserOutput = VehicleParser.parseScript("#test script\nCONFIG-START\n#first config\nLANE LANE-ID=100 RELAY-DIR=A2_100 HOST=localhost RADAR-PORT=11000 RADAR-ANGLE=34.5 RADAR-HEIGHT=5.30 FTP-PORT=2121 FTP-USER=ctes FTP-PWD=kosterij RADAR-DELAY=0\nCONFIG-END\nSCRIPT-START\n1000 LANE-REGISTRATION LANE-ID=100 SPEED=80 LENGTH=4.20 IMAGE=~/data/image.tif\nSCRIPT-END\n#Script done\n")
      parserOutput must have size (4)
      var record = parserOutput(0)
      record match {
        case script: ScriptLine ⇒ script.command must be(SimConstants.COMMENT)
        case _                  ⇒ fail("Unexpected type")
      }
      record = parserOutput(1)
      record match {
        case config: ConfigLine ⇒ {
          config.config must be(SimConstants.CONFIG_LANE)
        }
        case obj: AnyRef ⇒ fail("Unexpected type " + obj.getClass.getName)
      }
      record = parserOutput(2)
      record match {
        case script: ScriptLine ⇒ {
          script.command must be("LANE-REGISTRATION")
        }
        case _ ⇒ fail("Unexpected type")
      }
      record = parserOutput(3)
      record match {
        case script: ScriptLine ⇒ script.command must be(SimConstants.COMMENT)
        case _                  ⇒ fail("Unexpected type")
      }
    }
    "read commands" in {
      var parserOutput = VehicleParser.parseScript("CONFIG-START\nLANE LANE-ID=100 RELAY-DIR=A2_100 HOST=localhost RADAR-PORT=11000 RADAR-ANGLE=34.5 RADAR-HEIGHT=5.30 FTP-PORT=2121 FTP-USER=ctes FTP-PWD=kosterij RADAR-DELAY=0\nCONFIG-END\nSCRIPT-START\n1000 LANE-REGISTRATION LANE-ID=100 SPEED=80 LENGTH=4.20 IMAGE=~/data/image.tif\n1000 WAIT\n1000 WAIT INPUT\nSCRIPT-END\n")
      parserOutput must have size (4)
      var record = parserOutput(0)
      record match {
        case config: ConfigLine ⇒ {
          config.config must be(SimConstants.CONFIG_LANE)
        }
        case obj: AnyRef ⇒ fail("Unexpected type " + obj.getClass.getName)
      }
      record = parserOutput(1)
      record match {
        case script: ScriptLine ⇒ {
          script.command must be("LANE-REGISTRATION")
        }
        case _ ⇒ fail("Unexpected type")
      }
      record = parserOutput(2)
      record match {
        case script: ScriptLine ⇒ {
          script.command must be(SimConstants.WAIT)
          script.options must have size (0)
        }
        case _ ⇒ fail("Unexpected type")
      }
      record = parserOutput(3)
      record match {
        case script: ScriptLine ⇒ {
          script.command must be(SimConstants.WAIT)
          script.options must have size (1)
        }
        case _ ⇒ fail("Unexpected type")
      }
    }
    "read all posibbilities" in {
      val scriptFile = Resources.getResourceFile("tools/script.txt")
      val script = FileUtils.readFileToString(scriptFile)
      val parserOutput = VehicleParser.parseScript(script)
      var record = parserOutput(0)
      record match {
        case script: ScriptLine ⇒ script.command must be(SimConstants.COMMENT)
        case obj: AnyRef        ⇒ fail("Unexpected type " + obj.getClass.getName)
      }
      record = parserOutput(1)
      record match {
        case config: ConfigLine ⇒ config.config must be(SimConstants.CONFIG_LANE)
        case obj: AnyRef        ⇒ fail("Unexpected type " + obj.getClass.getName)
      }
      record = parserOutput(2)
      record match {
        case config: ConfigLine ⇒ config.config must be(SimConstants.CONFIG_LANE)
        case obj: AnyRef        ⇒ fail("Unexpected type " + obj.getClass.getName)
      }
      record = parserOutput(3)
      record match {
        case config: ConfigLine ⇒ config.config must be(SimConstants.CONFIG_CORRIDOR)
        case obj: AnyRef        ⇒ fail("Unexpected type " + obj.getClass.getName)
      }
      record = parserOutput(4)
      record match {
        case script: ScriptLine ⇒ script.command must be(SimConstants.LANE_REGISTRATION)
        case obj: AnyRef        ⇒ fail("Unexpected type " + obj.getClass.getName)
      }
      record = parserOutput(5)
      record match {
        case script: ScriptLine ⇒ script.command must be(SimConstants.LANE_REGISTRATION)
        case obj: AnyRef        ⇒ fail("Unexpected type " + obj.getClass.getName)
      }
      record = parserOutput(6)
      record match {
        case script: ScriptLine ⇒ script.command must be(SimConstants.REPEAT_START)
        case obj: AnyRef        ⇒ fail("Unexpected type " + obj.getClass.getName)
      }
      record = parserOutput(7)
      record match {
        case script: ScriptLine ⇒ script.command must be(SimConstants.CORRIDOR_REGISTRATION)
        case obj: AnyRef        ⇒ fail("Unexpected type " + obj.getClass.getName)
      }
      record = parserOutput(8)
      record match {
        case script: ScriptLine ⇒ script.command must be(SimConstants.REPEAT_END)
        case obj: AnyRef        ⇒ fail("Unexpected type " + obj.getClass.getName)
      }
      record = parserOutput(9)
      record match {
        case script: ScriptLine ⇒ script.command must be(SimConstants.MULTI_CORRIDOR_REGISTRATION)
        case obj: AnyRef        ⇒ fail("Unexpected type " + obj.getClass.getName)
      }
      record = parserOutput(10)
      record match {
        case script: ScriptLine ⇒ script.command must be(SimConstants.WAIT)
        case obj: AnyRef        ⇒ fail("Unexpected type " + obj.getClass.getName)
      }
    }
  }
}