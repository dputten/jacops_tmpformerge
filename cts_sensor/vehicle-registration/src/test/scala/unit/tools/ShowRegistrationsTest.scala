/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package unit.tools

import java.io.{ StringReader, BufferedReader }

import csc.vehicle.tools.parser._
import csc.vehicle.tools.parser.ShowRegistrations.RegistrationHandler
import org.scalatest.WordSpec
import org.scalatest._

class ShowRegistrationsTest extends WordSpec with MustMatchers {

  val v1Line =
    """INFO] [2016-03-30 00:05:04,248] [VehicleRegistration-akka.actor.default-dispatcher-18619] c.v.r.StoreVehicleRegistrationAmqp:
      |Queued VehicleMetadata(Lane(E1R1,E1R1,E1,A4,1.0,1.0,None),E1R1-2263234546230743,1459169099347,Some(28-03-2016 12:44:59.347 UTC),
      |None,None,None,Some(ValueWithConfidence(Car,100)),None,None,List(VehicleImage(1459169099347,0,/data/images/raw/E1R1/131.20160328.124459.347_R.jpg,
      |Overview,,None,None,None), VehicleImage(1459169099347,0,/data/images/masked_131.20160328.124459.347_R.jpg,OverviewMasked,,None,None,None)),
      |XXX-XXXX,a3b5681896515f94c65efff2552a4953,,None,None,Some(1),Some(LicensePlateData(Some(ValueWithConfidence(7SGZ78,24)),
      |Some(LicensePosition(1824,1498,1982,1501,1822,1529,1981,1532)),Some(OverviewMasked),Some(7_SGZ_78),None,Some(ARH),
      |Some(ValueWithConfidence(NL,67)))),None,None,None,None)""".stripMargin.replaceAll("\n", "")

  val v2Line1 =
    """[INFO] [2016-04-09 08:26:44,778] [VehicleRegistration-akka.actor.default-dispatcher-7684] c.v.r.StoreVehicleRegistrationAmqp: Queued VehicleMetadata(Version(2),Lane(X1R1,X1R1,X1,A4,1.0,1.0,None),X1R1-58188897350794,1460190400581,Some(09-04-2016 08:26:40.581 UTC),None,Some(ValueWithConfidence(Car,100)),None,List(VehicleImage(1460190400581,0,/data/images/jpg/converted-1460190401582.car_center1_R.jpg,Overview,,None,None,None), VehicleImage(1460190400581,0,/data/images/masked_1460190401582.car_center1_R.jpg,OverviewMasked,,None,None,None)),XXX-XXXX,a02c7f9b69470d28df8a3ec13df68257,,Some(LicensePlateData(Some(ValueWithConfidence(5TFD13,90)),Some(LicensePosition(1130,1530,1250,1530,1130,1555,1250,1555)),Some(OverviewMasked),None,None,Some(ICR),Some(ValueWithConfidence(NL,90)))),None,None,None,None,None,RegistrationStatus(0))""".stripMargin

  val v2Line2 =
    """[INFO] [2016-04-09 08:26:44,779] [VehicleRegistration-akka.actor.default-dispatcher-7691] c.v.r.StoreVehicleRegistrationAmqp: Queued VehicleMetadata(Version(2),Lane(E1R1,E1R1,E1,A4,1.0,1.0,None),E1R1-58186173316833,1460190398661,Some(09-04-2016 08:26:38.661 UTC),None,Some(ValueWithConfidence(Car,100)),None,List(VehicleImage(1460190398661,0,/data/images/jpg/converted-1460190398768.car_center1_R.jpg,Overview,,None,None,None), VehicleImage(1460190398661,0,/data/images/masked_1460190398768.car_center1_R.jpg,OverviewMasked,,None,None,None)),XXX-XXXX,a02c7f9b69470d28df8a3ec13df68257,,Some(LicensePlateData(Some(ValueWithConfidence(5TFD13,90)),Some(LicensePosition(1130,1530,1250,1530,1130,1555,1250,1555)),Some(OverviewMasked),None,None,Some(ICR),Some(ValueWithConfidence(NL,90)))),None,None,None,None,None,RegistrationStatus(0))""".stripMargin

  val v1LineExpected = List("20160328.144459.347", "E1R1", "131.20160328.124459.347_R.jpg", "7SGZ78", 24, 1822, 1498, 159, 31, "Car", 100, "")
  val v2Line1Expected = List("20160409.102640.581", "X1R1", "converted-1460190401582.car_center1_R.jpg", "5TFD13", 90, 1130, 1530, 120, 25, "Car", 100, "")
  val v2Line2Expected = List("20160409.102638.661", "E1R1", "converted-1460190398768.car_center1_R.jpg", "5TFD13", 90, 1130, 1530, 120, 25, "Car", 100, "")

  "ShowRegistrations" must {

    "process a v1 VehicleMetadata" in {
      val rows = getRegistrationCsv(v1Line)
      rows.size must be(1)
      rows(0) must be(v1LineExpected)
    }

    "process a v2 VehicleMetadata" in {
      val rows = getRegistrationCsv(v2Line1)
      rows.size must be(1)
      rows(0) must be(v2Line1Expected)
    }

    "process multiple lines, of different VehicleMetadata versions" in {
      val text = v2Line1 + "\n" + v1Line + "\n" + v2Line2
      val rows = getRegistrationCsv(text)
      rows.size must be(3)
      rows(0) must be(v2Line1Expected)
      rows(1) must be(v1LineExpected)
      rows(2) must be(v2Line2Expected)
    }

  }

  def getRegistrationCsv(text: String): List[List[Any]] = {
    var registrations: List[Registration] = Nil

    val handler: RegistrationHandler = { reg ⇒ registrations = registrations :+ reg }

    val reader = new BufferedReader(new StringReader(text))
    try {
      ShowRegistrations.process(reader, handler)
    } finally {
      reader.close()
    }

    registrations map ShowRegistrations.toCsvElements
  }
}