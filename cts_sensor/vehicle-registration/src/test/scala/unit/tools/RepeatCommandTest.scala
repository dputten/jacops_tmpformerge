/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package unit.tools

import org.scalatest.WordSpec
import org.scalatest._
import collection.mutable.ListBuffer
import csc.vehicle.tools.vehicle._

class RepeatCommandTest extends WordSpec with MustMatchers {
  "createRepeatCommandLine" must {
    "create a correct structure" in {
      //create script list
      val scriptList = new ListBuffer[ScriptLine]
      scriptList += new ScriptLine(delay = 1000, command = SimConstants.LANE_REGISTRATION, options = Map())
      scriptList += new ScriptLine(delay = 2000, command = SimConstants.CORRIDOR_REGISTRATION, options = Map())
      scriptList += new ScriptLine(delay = 3000, command = SimConstants.REPEAT_START, options = Map(SimConstants.REPEAT_NUMBER -> "3"))
      scriptList += new ScriptLine(delay = 4000, command = SimConstants.LANE_REGISTRATION, options = Map())
      scriptList += new ScriptLine(delay = 5000, command = SimConstants.CORRIDOR_REGISTRATION, options = Map())
      scriptList += new ScriptLine(delay = 6000, command = SimConstants.REPEAT_START, options = Map(SimConstants.REPEAT_NUMBER -> "6"))
      scriptList += new ScriptLine(delay = 7000, command = SimConstants.LANE_REGISTRATION, options = Map())
      scriptList += new ScriptLine(delay = 8000, command = SimConstants.REPEAT_END, options = Map())
      scriptList += new ScriptLine(delay = 9000, command = SimConstants.CORRIDOR_REGISTRATION, options = Map())
      scriptList += new ScriptLine(delay = 10000, command = SimConstants.REPEAT_END, options = Map())
      scriptList += new ScriptLine(delay = 11000, command = SimConstants.CORRIDOR_REGISTRATION, options = Map())
      val (lastRead, command) = VehicleSimulator.createRepeatCommandLine(scriptList.toList, 0)
      lastRead must be(scriptList.size)
      command.scriptLine.command must be(SimConstants.SCRIPT_START)
      command.scriptLine.delay must be(0)
      command.nrExec must be(1)
      command.startLine must be(0)
      command.commands must have size (4)
      command.commands(0).scriptLine must be(scriptList(0))
      command.commands(0).startLine must be(1)
      command.commands(1).scriptLine must be(scriptList(1))
      command.commands(1).startLine must be(2)
      command.commands(3).scriptLine must be(scriptList(10))
      command.commands(3).startLine must be(11)
      val cmd = command.commands(2)
      cmd.scriptLine must be(scriptList(2))
      cmd.nrExec must be(3)
      cmd.startLine must be(3)
      cmd.commands must have size (4)
      cmd.commands(0).scriptLine must be(scriptList(3))
      cmd.commands(0).startLine must be(4)
      cmd.commands(1).scriptLine must be(scriptList(4))
      cmd.commands(1).startLine must be(5)
      cmd.commands(3).scriptLine must be(scriptList(8))
      cmd.commands(3).startLine must be(9)

      val cmd2 = cmd.commands(2)
      cmd2.scriptLine must be(scriptList(5))
      cmd2.nrExec must be(6)
      cmd2.startLine must be(6)
      cmd2.commands must have size (1)
      cmd2.commands(0).scriptLine must be(scriptList(6))
      cmd2.commands(0).startLine must be(7)
    }
  }

}