package unit.base

import java.util.{ Calendar, Date }
import java.io.{ FileWriter, File }
import java.text.SimpleDateFormat
import csc.akka.logging.DirectLogging
import csc.vehicle.message.CalendarTimeUnitType

//helper class used to created tmp files with the given size and modified date
class FileCreatorHelper(date: Date) extends DirectLogging {
  val files = scala.collection.mutable.Set[File]()

  def cleanup() {
    files.foreach { file ⇒
      log.info("deleting " + file.getAbsolutePath)
      log.info("file is " + (if (file.delete) " deleted" else "not deleted"))
    }
  }

  def createFile(filename: String, path: String, changes: Int, timeUnit: CalendarTimeUnitType.Value, fileSizeInMB: Long) {
    val dir = createDirectoryStructure(path)
    if (dir != null) {
      val newFile = new File(dir, filename)

      createFile(newFile)
      createContent(newFile, fileSizeInMB)
      changeLastModified(newFile, changes, timeUnit)
      files += newFile
    }
  }

  def createFile(newFile: File) {
    if (newFile.exists) {
      log.info("new file already exists " + newFile.getAbsolutePath)
      val isDeleted = newFile.delete
      if (!isDeleted) {
        log.info("Failed to delete existing file " + newFile.getAbsolutePath)
      }
    } else {
      log.info("new file does not exist yet " + newFile.getAbsolutePath)
      val isCreated = newFile.createNewFile
      if (!isCreated) {
        log.info("new file can not be created " + newFile.getAbsolutePath)
      } else {
        log.info("new file is created " + newFile.getAbsolutePath)
      }
    }
  }

  def createDirectoryStructure(path: String): File = {
    val dir = new File(path)
    var directoryExists = dir.exists

    if (!directoryExists) {
      log.error("path does not exist " + path)
      directoryExists = dir.mkdirs
      if (!directoryExists) {
        log.error("Failed to create directory " + path)
      }
    } else {
      log.info("path already exists " + path)
    }

    return if (directoryExists) dir else null
  }

  def createContent(file: File, size: Long) {
    val output = new FileWriter(file)
    val bytes: Long = size * 1024 * 1024
    var i = 0

    while (i < bytes) {
      output.write(0);
      i += 1
    }

    output.close
  }

  def changeLastModified(file: File, changes: Int, timeUnit: CalendarTimeUnitType.Value) {
    var dateCalendar = Calendar.getInstance()
    dateCalendar.setTime(date)
    dateCalendar.add(timeUnit.id, changes)

    val dateIsSet = file.setLastModified(dateCalendar.getTime.getTime)

    if (dateIsSet) {
      log.info("setLastModified is modified")
      log.info((new SimpleDateFormat("yyyyMMdd")).format(dateCalendar.getTime))
    } else {
      log.info("setLastModified is not modified")
    }
  }
}