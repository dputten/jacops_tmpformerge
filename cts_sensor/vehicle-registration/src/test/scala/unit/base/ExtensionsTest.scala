/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package unit.base

import org.scalatest.junit.JUnitSuite
import java.io.File
import java.util.Date
import org.junit.{ After, Test }
import csc.vehicle.message.CalendarTimeUnitType
import csc.akka.logging.DirectLogging
import testframe.Resources

/**
 * test all extension functionality
 */
class ExtensionsTest extends JUnitSuite with DirectLogging {
  import csc.common.Extensions.files
  val fileHelper = new FileCreatorHelper(new Date);
  val home = Resources.getResourceDirPath().getAbsolutePath + "/tmp/"

  @After
  def teardown() {
    fileHelper.cleanup
  }

  /**
   * test the retrieval of a file's extension
   */
  @Test
  def testGetExtension {
    fileHelper.createFile("file1.jpg", home, -5, CalendarTimeUnitType.DAYS, 1)
    fileHelper.createFile("file1.jpg.gif", home, -5, CalendarTimeUnitType.DAYS, 1)
    fileHelper.createFile("file1", home, -5, CalendarTimeUnitType.DAYS, 1)

    assert((new File(home + "/file1.jpg")).getExtension.get == "jpg", (new File(home + "/file1.jpg")).getExtension)
    assert((new File(home + "/file1.jpg.gif")).getExtension.get == "gif")
    assert((new File(home + "/file1")).getExtension == None)
  }

  /**
   * test unpacking a tar file
   */
  @Test
  def testUnpacking {
    val input_path = Resources.getResourceFile("unit/base/files/20081008_121127_00001_0000004.tar").getAbsolutePath
    val output_path = input_path + ".unpacked"

    var input_file = new File(input_path)
    var output_file = new File(output_path)

    if (output_file.exists) {
      var to_remove = output_file.listFiles
      for (file ← to_remove) {
        file.delete
      }
      output_file.delete
    }

    assert(input_file.exists, "test file does not exists: " + input_file.getAbsolutePath)
    assert(!output_file.exists, "output file exists")

    input_file.unpack(pathToStore = output_path)

    assert(output_file.exists, "output directory does not exist")
    var retrieved = output_file.list

    assert(retrieved.length == 3, "no files in output dir")
    assert(retrieved.contains("20081008_121127_00001_0000004.xml"), "missing 20081008_121127_00001_0000004.xml")
    assert(retrieved.contains("20081008_121127_00001_0000004_1.jpg"), "missing 20081008_121127_00001_0000004_1.jpg")
    assert(retrieved.contains("20081008_121127_00001_0000004_2.jpg"), "missing 20081008_121127_00001_0000004_2.jpg")
  }

}
