package unit

import java.io.File

/**
 * Created by carlos on 13.11.15.
 */
object TestResource {

  /**
   * @param path path starting from the test/resources
   * @return resource file
   */
  def getFile(path: String): File = new File("vehicle-registration/src/test/resources", path)

}
