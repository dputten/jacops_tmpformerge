/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package unit

import csc.gantry.config.{ TypeCertificate, MeasurementMethodType, MeasurementMethodInstallationType }

object CertificateTestUtil {
  def createCertificate(cert: String): Seq[MeasurementMethodInstallationType] = {
    Seq(new MeasurementMethodInstallationType(0L,
      new MeasurementMethodType(typeDesignation = "",
        category = "A",
        unitSpeed = "km/h",
        unitRedLight = "s",
        unitLength = "m",
        restrictiveConditions = "",
        displayRange = "20-250 km/h",
        temperatureRange = "temperatuurbereik -10 to 50 graden Celsius",
        permissibleError = "toelatbare fout 3%",
        typeCertificate = new TypeCertificate(cert, 0, List()))))
  }

}