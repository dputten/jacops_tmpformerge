/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

import akka.actor.{ Actor, ActorSystem, Props }
import akka.testkit.{ TestKit, TestProbe }
import csc.akka.logging.DirectLogging
import csc.base.SensorDevice.SensorDevice
import csc.gantry.config.{ CameraConfig, _ }
import csc.vehicle.message.Lane
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import org.scalatest._

class DynamicConfigTest extends TestKit(ActorSystem("DynamicConfigTest")) with WordSpecLike with MustMatchers with DirectLogging with BeforeAndAfterAll {
  val lane = new Lane(laneId = "A2-1-lane1",
    name = "lane1",
    gantry = "1",
    system = "A2",
    sensorGPS_longitude = 52,
    sensorGPS_latitude = 4.2)

  override def afterAll() {
    try {
      system.shutdown
    } catch {
      case e: Exception ⇒ log.error("COULD NOT SHUTDOWN ACTOR REGISTRY".format(e))
    }
  }

  "config" must {
    "test match" in {
      val laneCfg = new HonacSensorConfigImpl(lane = lane, camera = null, imageMask = None, recognizeOptions = Map())
      def test(lane: LaneConfig) {
        lane match {
          case cfg: HonacSensorConfig ⇒ println(">>>>>Honac " + cfg)
        }
      }

      val down: LaneConfig = laneCfg
      down match {
        case cfg: HonacSensorConfig ⇒ println("Honac " + cfg)
      }
      test(laneCfg)
    }
    "be static" in {
      val laneCfg = new HonacSensorConfigImpl(lane = lane, camera = null, imageMask = None, recognizeOptions = Map())
      val actor = system.actorOf(Props(new ConfigActor(laneCfg)))
      val probe = TestProbe()
      probe.send(actor, "get")
      probe.expectMsg(lane.name)
    }
    "be dynamic" in {
      val laneCfg = new LaneMock(lane)
      val actor = system.actorOf(Props(new ConfigActor(laneCfg)))
      val probe = TestProbe()
      probe.send(actor, "get")
      probe.expectMsg(lane.name)
      val lane2 = lane.copy(name = "lanes12")
      laneCfg.setLane(lane2)
      probe.send(actor, "get")
      probe.expectMsg(lane2.name)
    }
  }
  "config2" must {
    "be static" in {
      var laneCfg = new HonacSensorConfigImpl(lane = lane, camera = null, imageMask = None, recognizeOptions = Map())
      val actor = system.actorOf(Props(new ConfigActor2(laneCfg.lane.name)))
      val probe = TestProbe()
      probe.send(actor, "get")
      probe.expectMsg(lane.name)

      val lane2 = lane.copy(name = "lanes12")
      laneCfg = new HonacSensorConfigImpl(lane = lane2, camera = null, imageMask = None, recognizeOptions = Map())
      probe.send(actor, "get")
      probe.expectMsg(lane2.name)
    }
    "be dynamic" in {
      val laneCfg = new LaneMock(lane)
      val actor = system.actorOf(Props(new ConfigActor2(laneCfg.lane.name)))
      val probe = TestProbe()
      probe.send(actor, "get")
      probe.expectMsg(lane.name)
      val lane2 = lane.copy(name = "lanes12")
      laneCfg.setLane(lane2)
      probe.send(actor, "get")
      probe.expectMsg(lane2.name)
    }
  }

}

class ConfigActor(laneCfg: LaneConfig) extends Actor {
  def receive = {
    case "get" ⇒ sender ! laneCfg.lane.name
  }
}

class ConfigActor2(name: ⇒ String) extends Actor {
  def receive = {
    case "get" ⇒ sender ! name
  }
}

class LaneMock(l: Lane) extends LaneConfig {
  var laneMock = l

  def setLane(l: Lane) { laneMock = l }

  /**
   * @return The lane information where this configuration is used
   */
  def lane: Lane = laneMock
  /**
   * @return  The relaySensorData Configuration
   */
  def camera: CameraConfig = null
  /**
   * @return  The image mask configuration
   */
  def imageMask: Option[ImageMask] = None

  /**
   * @return The recognition options
   */
  def recognizeOptions: Map[String, String] = Map()

  override def sensorList: List[SensorDevice] = Nil
}