package csc.base

import base._
import csc.akka.logging.DirectLogging
import org.junit.Test
import org.scalatest.junit.JUnitSuite

class SensorTest extends JUnitSuite with DirectLogging {
  @Test
  def testCopying {
    var sensor = new Sensor(sensorId = 10,
      sensorName = "Naaam",
      sensorDirection = SensorDirection.Inbound,
      sensorGPS_longitude = 100.1f,
      sensorGPS_latitude = 200.2f,
      remoteHostName = "HostName",
      remotePort = 80)
    var cloned = sensor.copy(sensorGPS_longitude = 500.4f, sensorGPS_latitude = 600.2f)

    assert(sensor.sensorId == cloned.sensorId, "sensorId failed")
    assert(sensor.sensorName == cloned.sensorName, "sensorName failed")
    assert(cloned.sensorGPS_longitude == 500.4f, "sensorGPS_longitude failed")
    assert(cloned.sensorGPS_latitude == 600.2f, "sensorGPS_latitude failed")
    assert(sensor.remoteHostName == cloned.remoteHostName, "remoteHostName failed")
    assert(sensor.remotePort == cloned.remotePort, "remotePort failed")
    assert(sensor.sensorDirection == cloned.sensorDirection, "sensorDirection failed")
  }
}