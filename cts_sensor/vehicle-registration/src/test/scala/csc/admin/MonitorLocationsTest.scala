package csc.admin

//copyright CSC 2010 - 2011

import java.util.Date

import admin.{ BeanAttributeFormat, LocationBean, MonitorLocations }
import csc.akka.logging.DirectLogging
import csc.vehicle.message.{ DigitalSwitchDetectedMessage, MonitorStatusMessage }
import org.junit.Test
import org.scalatest.MustMatchers
import org.scalatest.junit.JUnitSuite
import testbase.TestBeanRegistry

/**
 * Test the MonitorLocations class
 */
class MonitorLocationsTest extends JUnitSuite with DirectLogging with MustMatchers {
  /**
   * Test updating with a OpenDoor Message from ImageProcessor
   */
  @Test
  def testProcessingOpenDoor {
    var beanRegistrator = new TestBeanRegistry
    val monitor = new MonitorLocations(registry = beanRegistrator)
    val date1 = new Date()
    val dateFromDetector = "datum 1 van 22"
    val message_22_1 = new DigitalSwitchDetectedMessage(dateFromDetector = dateFromDetector,
      ipFromDetector = "192.168.22.22",
      date = date1,
      imageServerHost = "192.168.22.22",
      imageServerPort = 2000,
      originalMessage = "Test bericht 1 van 22",
      doorIsOpen = true,
      description = "Lokatie naam 1 van 22")
    monitor.processMessage(message_22_1)
    var allbeans = beanRegistrator.getRegisteredBeans() map { bean ⇒ bean.asInstanceOf[LocationBean] }
    allbeans.size must be(1)
    var bean = allbeans.head
    bean.getDoorModifiedDate must be(dateFromDetector)
    bean.getDoorStatus must be("open")

    val date2 = new Date(date1.getTime + 60000)
    val message_22_2 = new DigitalSwitchDetectedMessage(dateFromDetector = "datum 2 van 22",
      ipFromDetector = "192.168.22.22",
      date = date2,
      imageServerHost = "192.168.22.22",
      imageServerPort = 2000,
      originalMessage = "Test bericht 1 van 22",
      doorIsOpen = false,
      description = "Lokatie naam 1 van 22")
    monitor.processMessage(message_22_2)
    allbeans = beanRegistrator.getRegisteredBeans() map { bean ⇒ bean.asInstanceOf[LocationBean] }
    allbeans.size must be(1)
    bean = allbeans.head
    bean.getDoorModifiedDate must be("datum 2 van 22")
    bean.getDoorStatus must be("closed")
  }

  /**
   * Test updating with a MonitorStatus Message from ImageProcessor
   */
  @Test
  def testProcessingIPMonitor {
    var beanRegistrator = new TestBeanRegistry
    val monitor = new MonitorLocations(registry = beanRegistrator)
    val date1 = new Date()
    val date2 = new Date(date1.getTime + 60000)
    val host = "10.248.2.10"
    val port = 4000
    val msg = new MonitorStatusMessage(host, port, date2, "version", date1, List())

    monitor.processMessage(msg)
    var allbeans = beanRegistrator.getRegisteredBeans() map { bean ⇒ bean.asInstanceOf[LocationBean] }
    allbeans.size must be(1)
    var bean = allbeans.head
    bean.getApplicationStartTime must be(BeanAttributeFormat.formatTime(date1))
    bean.getApplicationVersion must be("version")
    bean.getLastUpdateTime must be(BeanAttributeFormat.formatTime(date2))

  }
}