/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.admin

import java.text.SimpleDateFormat
import java.util.Date

import admin.{ MonitorSensors, SensorBean }
import csc.akka.logging.DirectLogging
import csc.vehicle.message._
import org.junit.Test
import org.scalatest.MustMatchers
import org.scalatest.junit.JUnitSuite
import testbase.TestBeanRegistry

/**
 * Test the MonitorSensor class and the SensorBean class
 */
class MonitorSensorsTest extends JUnitSuite with DirectLogging with MustMatchers {
  val timeFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS")
  /**
   * Testing a received monitor message from a image processor.
   * Simulating messages from multiple IPs
   */
  @Test
  def testProcessingIPMessage {
    var beanRegistrator = new TestBeanRegistry
    val monitor = new MonitorSensors(registry = beanRegistrator, totalVehicleTimeWarn = 2500, totalJpegTimeWarn = 3500)
    val sensor1 = new Lane("1", "lane1", "gantry1", "route", 0, 0)
    val date1 = new Date()
    val date2 = new Date(date1.getTime + 60000)
    val msgSensor1 = new MonitorSensorMessage(sensor = sensor1, lastCameraMsg = Some(date1), lastRadarMsg = Some(date2), radarMsg = None)

    monitor.processIPMonitorMessage(date1, List(msgSensor1))
    var allbeans = beanRegistrator.getRegisteredBeans() map { bean ⇒ bean.asInstanceOf[SensorBean] }
    allbeans.size must be(1)
    var firstBean = allbeans.head
    firstBean.getSensor must startWith(sensor1.laneId.toString)
    firstBean.getLastMonitorImage must be(timeFormat.format(date1))
    firstBean.getLastMonitorRadar must be(timeFormat.format(date2))
    firstBean.getLastMonitorRadarMsg must be("")

    val sensor2 = new Lane("2", "lane2", "gantry1", "route", 0, 0)
    val msgSensor2 = new MonitorSensorMessage(sensor = sensor2, lastCameraMsg = Some(date1), lastRadarMsg = Some(date1), radarMsg = Some("info"))

    monitor.processIPMonitorMessage(date1, List(msgSensor2))
    allbeans = beanRegistrator.getRegisteredBeans() map { bean ⇒ bean.asInstanceOf[SensorBean] }
    allbeans.size must be(2)

    for (bean ← allbeans) {
      bean.getSensor match {
        case "1lane1" ⇒ {
          bean.getLastMonitorImage must be(timeFormat.format(date1))
          bean.getLastMonitorRadar must be(timeFormat.format(date2))
          bean.getLastMonitorRadarMsg must be("")
        }
        case "2lane2" ⇒ {
          bean.getLastMonitorImage must be(timeFormat.format(date1))
          bean.getLastMonitorRadar must be(timeFormat.format(date1))
          bean.getLastMonitorRadarMsg must be("info")
        }
        case _ ⇒ fail("unexpected sensor " + bean.getSensor)
      }
    }
  }

  /**
   * Testing a received monitor message from a image processor.
   * Simulating messages from one IP with multiple sensors
   */
  @Test
  def testProcessingIPMessageMultiple {
    var beanRegistrator = new TestBeanRegistry
    val monitor = new MonitorSensors(registry = beanRegistrator, totalVehicleTimeWarn = 2500, totalJpegTimeWarn = 3500)
    val sensor1 = new Lane("1", "lane1", "gantry1", "route", 0, 0)
    val date1 = new Date()
    val date2 = new Date(date1.getTime + 60000)
    val msgSensor1 = new MonitorSensorMessage(sensor = sensor1, lastCameraMsg = Some(date1), lastRadarMsg = Some(date2), radarMsg = None)
    val sensor2 = new Lane("2", "lane2", "gantry1", "route", 0, 0)
    val msgSensor2 = new MonitorSensorMessage(sensor = sensor2, lastCameraMsg = Some(date1), lastRadarMsg = Some(date1), radarMsg = Some("info"))

    monitor.processIPMonitorMessage(date1, List(msgSensor1, msgSensor2))
    val allbeans: List[SensorBean] = beanRegistrator.getRegisteredBeans() map { bean ⇒ bean.asInstanceOf[SensorBean] }
    allbeans.size must be(2)
    for (bean ← allbeans) {
      bean.getSensor match {
        case "1lane1" ⇒ {
          bean.getLastMonitorImage must be(timeFormat.format(date1))
          bean.getLastMonitorImageSeq must be(1)
          bean.getLastMonitorRadar must be(timeFormat.format(date2))
          bean.getLastMonitorRadarSeq must be(1)
          bean.getLastMonitorRadarMsg must be("")
        }
        case "2lane2" ⇒ {
          bean.getLastMonitorImage must be(timeFormat.format(date1))
          bean.getLastMonitorImageSeq must be(1)
          bean.getLastMonitorRadar must be(timeFormat.format(date1))
          bean.getLastMonitorRadarSeq must be(1)
          bean.getLastMonitorRadarMsg must be("info")
        }
        case _ ⇒ fail("unexpected sensor " + bean.getSensor)
      }
    }
  }

  /**
   * Testing the processing of a VehicleMessage
   */
  @Test
  def testProcessingVehicleMsg {
    var beanRegistrator = new TestBeanRegistry
    val monitor = new MonitorSensors(registry = beanRegistrator, totalVehicleTimeWarn = 2500, totalJpegTimeWarn = 3500)
    val sensor1 = new Lane("1", "lane1", "gantry1", "route", 0, 0)
    val date1 = System.currentTimeMillis()
    val date2 = date1 + 60000

    val now = System.currentTimeMillis()
    val vd = new VehicleData(
      length = Some(new ValueWithConfidence[Float](123.4567F, 100)),
      speed = Some(new ValueWithConfidence[Float](100.2F, 10)))

    val msg = new VehicleRegistrationMessage(
      lane = sensor1,
      eventId = Some("12"),
      eventTimestamp = Some(date1),
      event = new SensorEvent(now, new PeriodRange(now, now), "test"),
      priority = Priority.NONCRITICAL,
      licenseData = RegistrationLicenseData(license = Some(new ValueWithConfidence[String]("90XVSB", 90))),
      vehicleData = vd)

    monitor.processVehicleMessage(date1, msg)
    monitor.processVehicleMessage(date2, msg)

    val allbeans: List[SensorBean] = beanRegistrator.getRegisteredBeans() map { bean ⇒ bean.asInstanceOf[SensorBean] }
    allbeans.size must be(1)
    val bean = allbeans.head
    bean.getSensor must be("1lane1")
    bean.getProcessTimeXMLLast must be(60000L)
    bean.getProcessTimeXMLNr must be(1L)
    bean.getProcessTimeXMLNrLate must be(1L)
    bean.getProcessTimeXMLNrPerc must be(50)
    bean.getProcessTimeXMLSum must be(60000L)
    bean.getProcessTimeXMLAvg must be(30000L)

    bean.getRecognizedConfidenceLast must be(90)
    bean.getRecognizedNr must be(2)
    bean.getRecognizedNrFailed must be(0)
    bean.getRecognizedNrPerc must be(100)
    bean.getRecognizedConfidenceSum must be(2 * 90)
    bean.getRecognizedConfidenceAvg must be(90)

  }
}