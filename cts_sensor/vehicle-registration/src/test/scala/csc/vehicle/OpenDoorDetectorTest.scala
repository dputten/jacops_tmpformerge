package csc.vehicle

//copyright CSC 2010 - 2011

import java.net._
import java.util.concurrent.{ CyclicBarrier, TimeUnit }

import akka.actor.{ Actor, ActorRef, ActorSystem, Props }
import akka.testkit.TestKit
import csc.akka.CamelClient
import csc.akka.logging.DirectLogging
import csc.vehicle.message.DigitalSwitchDetectedMessage
import image.OpenDoorDetector
import org.scalatest._
import scala.concurrent.duration._

/**
 * main class for testing OpenDoorDetector
 */
class OpenDoorDetectorTest extends TestKit(ActorSystem("OpenDoorDetectorTest"))
  with WordSpecLike with BeforeAndAfterAll with DirectLogging with CamelClient {

  var openDoorDetector: OpenDoorDetector = null
  val uri = "mina:udp://localhost:12400?sync=false"
  val hostName = "localhost"
  val hostPort = 12400
  val expressionDi1 = "<\\d*>(\\S* \\d{1,2} \\d{1,2}:\\d{1,2}:\\d{1,2}) (\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b) INFO:DI 1 transition \\((On|Off) -> (On|Off)\\)"
  val expressionDi2 = "<\\d*>(\\S* \\d{1,2} \\d{1,2}:\\d{1,2}:\\d{1,2}) (\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b) INFO:DI 2 transition \\((On|Off) -> (On|Off)\\)"

  override def afterAll() {
    system.shutdown
  }

  "OpenDoorDetector" must {

    "Send number of valid messages using digitalInputChannel=1 and doorOpenSignal=On" in {

      val barrier = new CyclicBarrier(2)

      lazy val receiver = new ReceiverActor(barrier = Some(barrier))
      lazy val receiverRef = system.actorOf(Props(receiver))
      val host = "10.0.4.20"
      val port = 2000
      val detector = system.actorOf(Props(new OpenDoorDetector(expression = expressionDi1, uri = uri, host = host, port = port, doorOpenSignal = "On", description = "location name", nextActors = Set[ActorRef](receiverRef))))
      awaitActivation(detector, 10 seconds)

      val messageOff = "<172>Jan 01 01:58:39 192.168.127.253 INFO:DI 1 transition (On -> Off)"
      sendNotification(messageOff, hostName, hostPort)
      barrier.await(10L, TimeUnit.SECONDS)

      var expectedDate = "Jan 01 01:58:39"
      var expectedIpFromDetector = "192.168.127.253"
      var expectedDescription = "location name"

      assert(receiver.msgReceived, "receivingActor did not receive a message")
      assert(receiver.numberReceivedMsg == 1, "receivingActor received more that one reply")
      assert(receiver.receivedMessage.doorIsOpen == false, "Door should be closed")
      assert(receiver.receivedMessage.originalMessage == messageOff, "received [%s], expected [%s]".format(receiver.receivedMessage.originalMessage, messageOff))
      assert(receiver.receivedMessage.dateFromDetector == expectedDate, "received [%s], expected [%s]".format(receiver.receivedMessage.dateFromDetector, expectedDate))
      assert(receiver.receivedMessage.ipFromDetector == expectedIpFromDetector, "received [%s], expected [%s]".format(receiver.receivedMessage.ipFromDetector, expectedIpFromDetector))
      assert(receiver.receivedMessage.imageServerHost == host, "received [%s], expected [%s]".format(receiver.receivedMessage.imageServerHost, host))
      assert(receiver.receivedMessage.imageServerPort == port, "received [%d], expected [%d]".format(receiver.receivedMessage.imageServerPort, port))
      assert(receiver.receivedMessage.description == expectedDescription, "received [%s], expected [%s]".format(receiver.receivedMessage.description, expectedDescription))

    }

    "Send number of valid messages using digitalInputChannel=2 and doorOpenSignal=Off" in {
      val barrier = new CyclicBarrier(2)

      lazy val receiver = new ReceiverActor(barrier = Some(barrier))
      lazy val receiverRef = system.actorOf(Props(receiver))
      val host = "10.0.4.20"
      val port = 2000

      val detector = system.actorOf(Props(new OpenDoorDetector(expression = expressionDi2, uri = uri, host = host, port = port, doorOpenSignal = "Off", description = "location name", nextActors = Set[ActorRef](receiverRef))))
      awaitActivation(detector, 10 seconds)

      val messageOff = "<172>Jan 01 01:58:39 192.168.127.253 INFO:DI 2 transition (On -> Off)"
      sendNotification(messageOff, hostName, hostPort)
      barrier.await(10L, TimeUnit.SECONDS)

      var expectedDate = "Jan 01 01:58:39"
      var expectedIpFromDetector = "192.168.127.253"
      var expectedInetAddress = InetAddress.getLocalHost
      var expectedDescription = "location name"

      assert(receiver.msgReceived, "receivingActor did not receive a message")
      assert(receiver.numberReceivedMsg == 1, "receivingActor received more that one reply")
      assert(receiver.receivedMessage.doorIsOpen == true, "Door should be closed")
      assert(receiver.receivedMessage.originalMessage == messageOff, "received [%s], expected [%s]".format(receiver.receivedMessage.originalMessage, messageOff))
      assert(receiver.receivedMessage.dateFromDetector == expectedDate, "received [%s], expected [%s]".format(receiver.receivedMessage.dateFromDetector, expectedDate))
      assert(receiver.receivedMessage.ipFromDetector == expectedIpFromDetector, "received [%s], expected [%s]".format(receiver.receivedMessage.ipFromDetector, expectedIpFromDetector))
      assert(receiver.receivedMessage.imageServerHost == host, "received [%s], expected [%s]".format(receiver.receivedMessage.imageServerHost, host))
      assert(receiver.receivedMessage.imageServerPort == port, "received [%d], expected [%d]".format(receiver.receivedMessage.imageServerPort, port))
      assert(receiver.receivedMessage.description == expectedDescription, "received [%s], expected [%s]".format(receiver.receivedMessage.description, expectedDescription))

    }

    "Sends an invalid message" in {
      lazy val receiver = new ReceiverActor(barrier = None)
      lazy val receiverRef = system.actorOf(Props(receiver))
      val host = "10.0.4.20"
      val port = 2000

      val detector = system.actorOf(Props(new OpenDoorDetector(expression = expressionDi1, uri = uri, host = host, port = port, doorOpenSignal = "On", description = "", nextActors = Set[ActorRef](receiverRef))))
      awaitActivation(detector, 10 seconds)

      val messageDI2 = "<172>Jan 01 01:58:39 192.168.127.253 INFO:DI 2 transition (On -> Off)"
      sendNotification(messageDI2, hostName, hostPort)

      assert(!receiver.msgReceived, "receivingActor did not receive a message")
    }

    "Send number of valid messages using digitalInputChannel=1 and doorOpenSignal=On (ticket AB-15)" in {
      val barrier = new CyclicBarrier(2)

      lazy val receiver = new ReceiverActor(barrier = Some(barrier))
      lazy val receiverRef = system.actorOf(Props(receiver))
      val host = "10.0.4.20"
      val port = 2000
      val detector = system.actorOf(Props(new OpenDoorDetector(expression = expressionDi1, uri = uri, host = host, port = port, doorOpenSignal = "On", description = "location name", nextActors = Set[ActorRef](receiverRef))))
      awaitActivation(detector, 10 seconds)

      val messageOff = "<172>Jan 01 01:58:39 192.168.127.253 INFO:DI 1 transition (On -> Off)"
      val nrMsg = 30
      for (i ← 0 until nrMsg) {
        log.info("Send Message %d".format(i))
        barrier.reset()
        sendNotification(messageOff, hostName, hostPort)
        barrier.await(10L, TimeUnit.SECONDS)
      }
      assert(receiver.numberReceivedMsg == nrMsg, "not all messages are received %d expected=%d".format(receiver.numberReceivedMsg, nrMsg))
    }
  }

  def sendNotification(msg: String, hostName: String, hostPort: Int): Unit = {

    var clientSocket: DatagramSocket = null
    try {
      clientSocket = new DatagramSocket()
      //val sendData = (msg+"\n").getBytes
      val sendData = msg.getBytes
      val inet = new InetSocketAddress(hostName, hostPort)
      val sendPacket = new DatagramPacket(sendData, sendData.length, inet)
      log.info("Send message length = %d msg=<%s>".format(sendData.length, new String(sendData)))
      clientSocket.send(sendPacket)
      log.info("Done Sending message")
      //Thread.sleep(3000)
    } catch {
      case ex: Exception ⇒ {
        log.error("ConnectToHost failed ", ex)
      }
    } finally {
      clientSocket.close();
    }
    log.info("Resume")
  }
}

class ReceiverActor(barrier: Option[CyclicBarrier]) extends Actor {
  var msgReceived = false
  var numberReceivedMsg = 0
  var receivedMessage: DigitalSwitchDetectedMessage = null

  def reset() {
    msgReceived = false
    numberReceivedMsg = 0
    receivedMessage = null
  }

  def receive = {
    case msg: DigitalSwitchDetectedMessage ⇒ {
      msgReceived = true
      numberReceivedMsg += 1
      receivedMessage = msg
      barrier.get.await()
    }
  }
}

