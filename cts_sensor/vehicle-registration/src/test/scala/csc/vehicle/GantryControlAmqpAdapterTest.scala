package csc.vehicle

import akka.actor.{ Actor, ActorRef, ActorSystem, Props }
import akka.testkit.{ ImplicitSender, TestKit }
import base.Mode
import csc.akka.logging.DirectLogging
import csc.amqp.AmqpSerializers.JsonDeserializer
import csc.amqp.JsonSerializers
import csc.amqp.test.AmqpBridgeActor._
import csc.amqp.test.{ Acked, AmqpBridgeActor, Outcome }
import csc.util.test.UniqueGenerator
import csc.vehicle.message._
import csc.vehicle.registration.GantryControlAmqpAdapter
import net.liftweb.json.{ DefaultFormats, Formats }
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }

/**
 * Created by carlos on 23/06/16.
 */
class GantryControlAmqpAdapterTest
  extends TestKit(ActorSystem("GantryControlAmqpAdapterTest"))
  with ImplicitSender
  with WordSpecLike
  with MustMatchers
  with DirectLogging
  with BeforeAndAfterEach
  with BeforeAndAfterAll {

  import GantryControlAmqpAdapterTest._

  var actor: ActorRef = null
  var client: ActorRef = null

  "GantryControlAmqpAdapter" must {

    "Handle properly a GetModeRequest and answer back" in {
      val request = createGetModeRequest
      val outcome = sendMessage(request)
      outcome must be(Acked)
      val response = expectMsgType[GetModeResponse]
      response.correlationId must be(request.correlationId)
      response.gantry must be(request.gantry)
      response.mode must be(initialMode.toString)
    }

    "Handle properly a valid SetModeRequest and answer back" in {
      val request = createSetModeRequest(Mode.Normal.toString)
      val outcome = sendMessage(request)
      outcome must be(Acked)
      val response = expectMsgType[SetModeResponse]
      response.correlationId must be(request.correlationId)
      response.gantry must be(request.gantry)
      response.mode must be(request.mode)
      response.error must be(None)
    }

    "Handle properly an invalid SetModeRequest and answer back" in {
      val request = createSetModeRequest("foobar")
      val outcome = sendMessage(request)
      outcome must be(Acked)
      val response = expectMsgType[SetModeResponse]
      response.correlationId must be(request.correlationId)
      response.gantry must be(request.gantry)
      response.mode must be(initialMode.toString)
      response.error.isDefined must be(true)
    }

  }

  def sendMessage(msg: AnyRef): Outcome = {
    client ! msg
    expectMsgType[Outcome]
  }

  private def getActor = actor

  override protected def beforeEach(): Unit = {
    val delegate = system.actorOf(Props(new DummyDelegate))
    client = system.actorOf(Props(new AmqpBridgeActor(getActor, DefaultFormats, matcherLogic, jsonDeserializers, true)))
    actor = system.actorOf(Props(new GantryControlAmqpAdapter(client, delegate, "endpoint")))
  }

  override protected def afterAll(): Unit = {
    system.shutdown()
  }
}

class DummyDelegate extends Actor {

  var mode = GantryControlAmqpAdapterTest.initialMode

  override def receive: Receive = {
    case msg: GetModeRequest ⇒
      sender ! GetModeResponse(msg.correlationId, msg.gantry, mode.toString, System.currentTimeMillis())
    case msg: SetModeRequest ⇒
      val error: Option[String] = Mode.forName(msg.mode) match {
        case None ⇒ Some("Invalid mode: " + msg.mode)
        case Some(m) ⇒
          mode = m
          None
      }
      sender ! SetModeResponse(msg.correlationId, msg.gantry, mode.toString, System.currentTimeMillis(), error)
  }
}

object GantryControlAmqpAdapterTest extends JsonSerializers with UniqueGenerator {

  val initialMode = Mode.Test
  val gantry = "E1"

  override implicit val formats: Formats = GantryControlAmqpAdapter.formats

  lazy val matcherLogic: MatcherLogic = msg ⇒ msg match {
    case m: GantryControlMessage ⇒ Some(m.correlationId, false, true)
    case _                       ⇒ None
  }

  lazy val jsonDeserializers: Map[String, JsonDeserializer] = Map(
    "GetModeResponse" -> fromJson[GetModeResponse],
    "SetModeResponse" -> fromJson[SetModeResponse])

  def createSetModeRequest(mode: String) =
    SetModeRequest(nextUnique, gantry, mode, System.currentTimeMillis(), "test")

  def createGetModeRequest = GetModeRequest(nextUnique, gantry, System.currentTimeMillis())

}