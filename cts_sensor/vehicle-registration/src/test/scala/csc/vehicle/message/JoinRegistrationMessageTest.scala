/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.message

import org.junit.Test
import org.scalatest.MustMatchers
import org.scalatest.junit.JUnitSuite
import java.util.Date

/**
 * Test the JoinRegistrationMessages Object
 */
class JoinRegistrationMessageTest extends JUnitSuite with MustMatchers {
  /**
   * Test joinRegistration using None als argument
   */
  @Test
  def testJoinEmpties {
    val list = List(new RegistrationImage("test.tif", ImageFormat.TIF_BAYER, VehicleImageType.Overview, ImageVersion.Original, 1234))
    val event = new SensorEvent(20L, new PeriodRange(10L, 40L), "src")
    val msg = new VehicleRegistrationMessage(new Lane("100", "lane1", "gantry1", "route", 0, 0),
      event = event, files = list)
    JoinRegistrationMessages.joinRegistration(None, None, "dest") must be(None)
    val expectEvent = event.copy(triggerSource = "dest")
    val expectMsg = Some(msg.copy(event = expectEvent))
    JoinRegistrationMessages.joinRegistration(Some(msg), None, "dest") must be(expectMsg)
    JoinRegistrationMessages.joinRegistration(None, Some(msg), "dest") must be(expectMsg)
  }
  /**
   * Test joinRegistration using a complete the  message
   */
  @Test
  def testJoinAllFields {
    val list = List(new RegistrationImage("test.tif", ImageFormat.TIF_BAYER, VehicleImageType.Overview, ImageVersion.Original, 1234))
    val event = new SensorEvent(20L, new PeriodRange(10L, 40L), "src")
    val msg = new VehicleRegistrationMessage(
      lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
      eventId = Some("ID1"),
      eventTimestamp = Some(30L),
      event = event,
      priority = Priority.NONCRITICAL,
      licenseData = RegistrationLicenseData(license = Some(new ValueWithConfidence[String]("12abc3", 90)),
        country = Some(new ValueWithConfidence[String]("NL", 90))),
      vehicleData = VehicleData(length = Some(new ValueWithConfidence[Float](5.6F, 90)),
        speed = Some(new ValueWithConfidence[Float](98F, 80)),
        category = Some(ValueWithConfidence[VehicleCategory.Value](VehicleCategory.Pers, 30))),
      files = list)
    val event2 = new SensorEvent(25L, new PeriodRange(5L, 45L), "src2")
    val msg2 = new VehicleRegistrationMessage(
      lane = new Lane("200", "lane2", "gantry1", "route", 0, 0),
      eventId = Some("ID2"),
      eventTimestamp = Some(40L),
      event = event2,
      priority = Priority.CRITICAL,
      licenseData = RegistrationLicenseData(license = Some(new ValueWithConfidence[String]("12ab30", 80)),
        country = Some(new ValueWithConfidence[String]("DE", 70))),
      vehicleData = VehicleData(length = Some(new ValueWithConfidence[Float](4.6F, 80)),
        speed = Some(new ValueWithConfidence[Float](88F, 88)),
        category = Some(ValueWithConfidence[VehicleCategory.Value](VehicleCategory.Van, 30))),
      files = list)
    val expectEvent = event.copy(triggerSource = "dest")
    val expectMsg = Some(msg.copy(event = expectEvent))
    JoinRegistrationMessages.joinRegistration(Some(msg), Some(msg2), "dest") must be(expectMsg)
  }
  /**
   * Test joinRegistration using a minimal first the  message and full second message
   */
  @Test
  def testJoinUpdateAllFields {
    //TODO PCL-45 test CertificationData merge
    val now = System.currentTimeMillis()
    val msg = new VehicleRegistrationMessage(lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
      event = new SensorEvent(25L, new PeriodRange(5L, 50L), "test"))
    val list = List(new RegistrationImage("test.tif", ImageFormat.TIF_BAYER, VehicleImageType.Overview, ImageVersion.Original, 1234))
    val event2 = new SensorEvent(25L, new PeriodRange(5L, 45L), "src2")
    val msg2 = new VehicleRegistrationMessage(
      lane = new Lane("200", "lane2", "gantry1", "route", 0, 0),
      eventId = Some("ID2"),
      eventTimestamp = Some(40L),
      event = event2,
      priority = Priority.CRITICAL,
      licenseData = RegistrationLicenseData(license = Some(new ValueWithConfidence[String]("12ab30", 80)),
        country = Some(new ValueWithConfidence[String]("DE", 70))),
      vehicleData = VehicleData(length = Some(new ValueWithConfidence[Float](4.6F, 80)),
        speed = Some(new ValueWithConfidence[Float](88F, 88)),
        category = Some(ValueWithConfidence[VehicleCategory.Value](VehicleCategory.Van, 30))),
      files = list)
    val expectEvent = event2.copy(triggerSource = "dest")
    val expectMsg = Some(msg2.copy(lane = msg.lane, event = expectEvent, priority = Priority.NONCRITICAL))
    JoinRegistrationMessages.joinRegistration(Some(msg), Some(msg2), "dest") must be(expectMsg)
  }

}