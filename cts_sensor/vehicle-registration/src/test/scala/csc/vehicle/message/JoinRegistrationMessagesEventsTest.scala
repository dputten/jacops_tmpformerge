/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.message

import java.util.Date

import org.junit.Test
import org.scalatest.MustMatchers
import org.scalatest.junit.JUnitSuite

/**
 * Test the JoinRegistrationMessages Object
 */
class JoinRegistrationMessagesEventsTest extends JUnitSuite with MustMatchers {
  /**
   * test the joining of period in the joinSensorEvents
   */
  @Test
  def testJoinPeriod {
    val event1 = new SensorEvent(20L, new PeriodRange(10L, 40L), "src")
    val event2 = new SensorEvent(30L, new PeriodRange(20L, 50L), "src2")
    val expect = new SensorEvent(20L, new PeriodRange(20L, 40L), "new")
    JoinRegistrationMessages.joinSensorEvents(event1, event2, "new") must be(expect)
  }
  /**
   * test the joining of eventTime in the joinSensorEvents
   */
  @Test
  def testJoinEventTime {
    val event1 = new SensorEvent(10L, new PeriodRange(10L, 40L), "src")
    val event2 = new SensorEvent(30L, new PeriodRange(20L, 50L), "src2")
    val expect1 = new SensorEvent(30L, new PeriodRange(20L, 40L), "new")
    JoinRegistrationMessages.joinSensorEvents(event1, event2, "new") must be(expect1)

    val event3 = new SensorEvent(10L, new PeriodRange(10L, 40L), "src")
    val event4 = new SensorEvent(45L, new PeriodRange(20L, 50L), "src2")
    val expect2 = new SensorEvent(20L, new PeriodRange(20L, 40L), "new")
    JoinRegistrationMessages.joinSensorEvents(event3, event4, "new") must be(expect2)
  }

}