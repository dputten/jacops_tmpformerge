/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.message

import java.util.Date

import org.junit.Test
import org.scalatest.MustMatchers
import org.scalatest.junit.JUnitSuite

/**
 * Test the JoinRegistrationMessages Object
 */
class JoinRegistrationMessagePrioListTest extends JUnitSuite with MustMatchers {
  //TODO PCL-45 test CertificationData merge
  val prioList = List[String]("src", "src1", "src2", "src3")
  val now = System.currentTimeMillis()
  /**
   * Test join with empty list
   */
  @Test
  def testJoinEmpty {
    an[IllegalArgumentException] must be thrownBy (JoinRegistrationMessages.join(prioList, List(), "dest"))
  }
  /**
   * Test join with oly one message
   */
  @Test
  def testJoinOne {
    val msg = new VehicleRegistrationMessage(
      lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
      eventId = Some("ID1"),
      eventTimestamp = Some(30L),
      event = new SensorEvent(now, new PeriodRange(now, now), "test"),
      priority = Priority.NONCRITICAL)

    val expectEvent = msg.event.copy(triggerSource = "dest")
    val expectMsg = msg.copy(event = expectEvent)
    JoinRegistrationMessages.join(prioList, List(msg), "dest") must be(expectMsg)
  }
  /**
   * Test join using a complete the  message, with both events
   */
  @Test
  def testJoinAllFields {
    val list = List(new RegistrationImage("test.tif", ImageFormat.TIF_BAYER, VehicleImageType.Overview, ImageVersion.Original, 1234))
    val event = new SensorEvent(20L, new PeriodRange(10L, 40L), "src")

    val msg = new VehicleRegistrationMessage(
      lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
      eventId = Some("ID1"),
      eventTimestamp = Some(30L),
      event = event,
      priority = Priority.NONCRITICAL,
      licenseData = RegistrationLicenseData(license = Some(new ValueWithConfidence[String]("12abc3", 90)),
        country = Some(new ValueWithConfidence[String]("NL", 90))),
      vehicleData = VehicleData(length = Some(new ValueWithConfidence[Float](5.6F, 90)),
        speed = Some(new ValueWithConfidence[Float](98F, 80)),
        category = Some(ValueWithConfidence[VehicleCategory.Value](VehicleCategory.Pers, 30))),
      files = list)
    val event2 = new SensorEvent(25L, new PeriodRange(5L, 45L), "src2")
    val msg2 = new VehicleRegistrationMessage(
      lane = new Lane("200", "lane2", "gantry1", "route", 0, 0),
      eventId = Some("ID2"),
      eventTimestamp = Some(40L),
      event = event2,
      priority = Priority.CRITICAL,
      licenseData = RegistrationLicenseData(license = Some(new ValueWithConfidence[String]("12ab30", 80)),
        country = Some(new ValueWithConfidence[String]("DE", 70))),
      vehicleData = VehicleData(length = Some(new ValueWithConfidence[Float](4.6F, 80)),
        speed = Some(new ValueWithConfidence[Float](88F, 88)),
        category = Some(ValueWithConfidence[VehicleCategory.Value](VehicleCategory.Van, 30))),
      files = list)
    val expectEvent = event.copy(triggerSource = "dest")
    val expectMsg = msg.copy(event = expectEvent)
    JoinRegistrationMessages.join(prioList, List(msg, msg2), "dest") must be(expectMsg)
    //order shouldn't matter
    JoinRegistrationMessages.join(prioList, List(msg2, msg), "dest") must be(expectMsg)
  }
  /**
   * Test join using a message without event and one with event
   */
  @Test
  def testJoinUpdateAllFields {
    val msg = new VehicleRegistrationMessage(lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
      event = new SensorEvent(25L, new PeriodRange(5L, 45L), "test"))
    val list = List(new RegistrationImage("test.tif", ImageFormat.TIF_BAYER, VehicleImageType.Overview, ImageVersion.Original, 1234))
    val event2 = new SensorEvent(25L, new PeriodRange(5L, 45L), "src2")
    val msg2 = new VehicleRegistrationMessage(
      lane = new Lane("200", "lane2", "gantry1", "route", 0, 0),
      eventId = Some("ID2"),
      eventTimestamp = Some(40L),
      event = event2,
      priority = Priority.CRITICAL,
      licenseData = RegistrationLicenseData(license = Some(new ValueWithConfidence[String]("12ab30", 80)),
        country = Some(new ValueWithConfidence[String]("DE", 70))),
      vehicleData = VehicleData(length = Some(new ValueWithConfidence[Float](4.6F, 80)),
        speed = Some(new ValueWithConfidence[Float](88F, 88)),
        category = Some(ValueWithConfidence[VehicleCategory.Value](VehicleCategory.Van, 30))),
      files = list)
    val expectEvent = event2.copy(triggerSource = "dest")
    val expectMsg = msg2.copy(event = expectEvent, priority = Priority.CRITICAL)
    JoinRegistrationMessages.join(prioList, List(msg, msg2), "dest") must be(expectMsg)
    //order shouldn't matter
    JoinRegistrationMessages.join(prioList, List(msg2, msg), "dest") must be(expectMsg)
  }
  /**
   * Test join three messages
   */
  @Test
  def testJoinUpdateMultipleMsg {
    val event = new SensorEvent(25L, new PeriodRange(10L, 45L), "src")
    val msg = new VehicleRegistrationMessage(lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
      event = event)
    val list = List(new RegistrationImage("test.tif", ImageFormat.TIF_BAYER, VehicleImageType.Overview, ImageVersion.Original, 1234))
    val event2 = new SensorEvent(25L, new PeriodRange(5L, 45L), "src2")
    val msg2 = new VehicleRegistrationMessage(
      lane = new Lane("200", "lane2", "gantry1", "route", 0, 0),
      eventId = Some("ID2"),
      eventTimestamp = Some(40L),
      event = event2,
      priority = Priority.CRITICAL,
      licenseData = RegistrationLicenseData(license = Some(new ValueWithConfidence[String]("12ab34", 70))),
      files = list)
    val event3 = new SensorEvent(10L, new PeriodRange(10L, 40L), "src3")
    val msg3 = new VehicleRegistrationMessage(
      lane = new Lane("300", "lane3", "gantry1", "route", 0, 0),
      eventId = Some("ID3"),
      eventTimestamp = Some(50L),
      event = event3,
      priority = Priority.CRITICAL,
      licenseData = RegistrationLicenseData(license = Some(new ValueWithConfidence[String]("12ab30", 80)),
        country = Some(new ValueWithConfidence[String]("DE", 70))),
      vehicleData = VehicleData(length = Some(new ValueWithConfidence[Float](4.6F, 80)),
        speed = Some(new ValueWithConfidence[Float](88F, 88)),
        category = Some(ValueWithConfidence[VehicleCategory.Value](VehicleCategory.Van, 30))),
      files = list)
    val expectEvent = new SensorEvent(25L, new PeriodRange(10L, 40L), "dest")
    val expectMsg = new VehicleRegistrationMessage(
      lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
      eventId = Some("ID2"),
      eventTimestamp = Some(40L),
      event = expectEvent,
      priority = Priority.NONCRITICAL,
      licenseData = RegistrationLicenseData(license = Some(new ValueWithConfidence[String]("12ab34", 70)),
        country = Some(new ValueWithConfidence[String]("DE", 70))),
      vehicleData = VehicleData(length = Some(new ValueWithConfidence[Float](4.6F, 80)),
        speed = Some(new ValueWithConfidence[Float](88F, 88)),
        category = Some(ValueWithConfidence[VehicleCategory.Value](VehicleCategory.Van, 30))),
      files = list)
    JoinRegistrationMessages.join(prioList, List(msg, msg2, msg3), "dest") must be(expectMsg)
    //order shouldn't matter
    JoinRegistrationMessages.join(prioList, List(msg2, msg, msg3), "dest") must be(expectMsg)
    JoinRegistrationMessages.join(prioList, List(msg2, msg3, msg), "dest") must be(expectMsg)

    JoinRegistrationMessages.join(prioList, List(msg, msg3, msg2), "dest") must be(expectMsg)
    JoinRegistrationMessages.join(prioList, List(msg3, msg, msg2), "dest") must be(expectMsg)
    JoinRegistrationMessages.join(prioList, List(msg3, msg2, msg), "dest") must be(expectMsg)
  }

}