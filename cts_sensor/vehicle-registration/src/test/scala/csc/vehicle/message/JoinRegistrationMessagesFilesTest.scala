/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.message

import java.util.Date

import csc.util.test.{ ObjectBuilder, UniqueGenerator }
import csc.vehicle.message.VehicleImageType._
import org.junit.Test
import org.scalatest.MustMatchers
import org.scalatest.junit.JUnitSuite

/**
 * Test the JoinRegistrationMessages Object
 */
object JoinRegistrationMessagesFilesTest extends ObjectBuilder with UniqueGenerator {
  def createImage(tp: VehicleImageType): RegistrationImage = create[RegistrationImage].copy(
    uri = nextUnique, imageType = tp, imageVersion = ImageVersion.Original, imageFormat = ImageFormat.JPG_RGB)
}

import csc.vehicle.message.JoinRegistrationMessagesFilesTest._

class JoinRegistrationMessagesFilesTest extends JUnitSuite with MustMatchers {
  val now = new Date()

  /**
   * test joinFileList with empty lists
   */
  @Test
  def testJoinEmpties {
    val list = List(createImage(Overview))
    val msg = create[VehicleRegistrationMessage].copy(files = list)
    val msgEmpty = create[VehicleRegistrationMessage]

    JoinRegistrationMessages.mergeFileList(msgEmpty, msgEmpty) must be((Nil, Nil))
    JoinRegistrationMessages.mergeFileList(msg, msgEmpty) must be((list, Nil))
    JoinRegistrationMessages.mergeFileList(msgEmpty, msg) must be((list, Nil))
  }

  /**
   * test joinFileList with different file lists
   */
  @Test
  def testJoinDifferentFiles {
    val list1 = List(createImage(Overview))
    val list2 = List(createImage(License))
    val msg1 = create[VehicleRegistrationMessage].copy(files = list1)
    val msg2 = create[VehicleRegistrationMessage].copy(files = list2)
    val expect = list1 ++ list2

    val (mergedFiles, mergedReplaced) = JoinRegistrationMessages.mergeFileList(msg1, msg2)
    mergedFiles.toSet must be(expect.toSet)
    mergedReplaced must be(Nil)
  }

  /**
   * test joinFileList with lists including the same typed files
   */
  @Test
  def testJoinSameFiles {
    val file1 = createImage(Overview)
    val list1 = List(file1)
    val list2 = List(file1)
    val msg1 = create[VehicleRegistrationMessage].copy(files = list1)
    val msg2 = create[VehicleRegistrationMessage].copy(files = list2)

    JoinRegistrationMessages.mergeFileList(msg1, msg2) must be((list1, Nil))

    val file2 = createImage(License)
    val file3 = createImage(OverviewMasked)
    val list3 = List(file1, file2)
    val list4 = List(file1, file3)
    val msg3 = create[VehicleRegistrationMessage].copy(files = list3)
    val msg4 = create[VehicleRegistrationMessage].copy(files = list4)

    val expect = List(file1, file2, file3)

    val (mergedFiles, mergedReplaced) = JoinRegistrationMessages.mergeFileList(msg3, msg4)
    mergedFiles.toSet must be(expect.toSet)
    mergedReplaced must be(Nil)

  }

  @Test
  def testExclusions {
    val file1 = createImage(Overview)
    val file2 = createImage(Overview)
    val file3 = createImage(Overview)

    val msg1 = create[VehicleRegistrationMessage].copy(files = List(file1), replacedFiles = List(file2))
    val msg2 = create[VehicleRegistrationMessage].copy(files = List(file2, file3))

    val (mergedFiles, mergedReplaced) = JoinRegistrationMessages.mergeFileList(msg1, msg2)
    mergedFiles.toSet must be(List(file1, file3).toSet)
    mergedReplaced must be(List(file2))
  }

}