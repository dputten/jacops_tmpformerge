package csc.vehicle.registration

import org.scalatest.WordSpec
import org.scalatest._

/**
 * Created by carlos on 21/03/16.
 */
class MovingAverageBufferTest extends WordSpec with MustMatchers {

  "MovingAverageBuffer" must {

    "provide good results for a set of values withing limits" in {
      val buffer = new MovingAverageBuffer(5, 100)
      buffer.tryAdd(10) must be((0, Some(0)))
      buffer.tryAdd(20) must be((1, Some(10)))
      buffer.tryAdd(0) must be((2, Some(15)))
      buffer.tryAdd(50) must be((3, Some(10)))
    }

    "not add value bigger than max allowed" in {
      val buffer = new MovingAverageBuffer(5, 100)
      buffer.tryAdd(101) must be((0, None))
      buffer.tryAdd(-101) must be((0, None)) //PCL-46: absolute value higher than maximum
      buffer.tryAdd(10) must be((0, Some(0)))
      buffer.tryAdd(0) must be((1, Some(10)))
      buffer.tryAdd(1000) must be((2, None))
    }

    "remove the oldest element when adding beyond max size" in {
      val buffer = new MovingAverageBuffer(3, 100)

      buffer.tryAdd(10) must be((0, Some(0))) //+10
      buffer.tryAdd(10) must be((1, Some(10))) //+10, 10 / 1
      buffer.tryAdd(10) must be((2, Some(10))) //+10, 20 / 2
      buffer.tryAdd(40) must be((3, Some(10))) //+40, 30 / 3
      buffer.tryAdd(25) must be((3, Some(20))) //+25, 60 / 3 (oldest removed)
      buffer.tryAdd(0) must be((3, Some(25))) //+00, 75 / 3 (oldest removed)
    }
  }

}
