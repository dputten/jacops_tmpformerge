/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import java.io.File
import java.util.Date

import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import scala.concurrent.duration._
import csc.vehicle.message._
import org.scalatest._

/**
 * Test the StoreVehicleRegistrationFile
 */
class StoreVehicleRegistrationFileTest
  extends TestKit(ActorSystem("StoreVehicleRegistrationFileTest"))
  with WordSpecLike
  with MustMatchers
  with BeforeAndAfterAll {

  override def afterAll() {
    system.shutdown
  }

  "The StoreVehicleRegistrationFile" when {
    val fileName = getClass.getClassLoader.getResource("output_license.jpg").getPath
    val inputFile = new File(fileName)
    val outDirFile = new File(inputFile.getParent, "output")
    if (!outDirFile.exists()) {
      outDirFile.mkdir()
    }

    val probe = TestProbe()
    val convertActorRef = TestActorRef(new StoreVehicleRegistrationFile(
      outputDir = outDirFile.getAbsolutePath,
      nextActors = Set(probe.ref)))
    "receiving a complete message" must {

      "create all files" in {

        val images = for (tp ← StoreImage.all) yield new RegistrationImage(fileName, ImageFormat.JPG_RGB, tp._1, tp._2, 1234)
        val now = System.currentTimeMillis()

        val msg = new VehicleRegistrationMessage(
          lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
          eventId = Some("100"),
          eventTimestamp = Some(100L),
          event = new SensorEvent(now, new PeriodRange(now, now), "test"),
          priority = Priority.NONCRITICAL,
          files = images)

        convertActorRef ! msg
        //test if message are received
        probe.expectMsg(500 millis, msg)

        val outputDir = new File(outDirFile, "19700101_00")
        outputDir.exists() must be(true)

        StoreFileConstants.filenamePostfixes.values.foreach { postfix ⇒
          var file = new File(outputDir, "100" + postfix)
          file.exists() must be(true)
          file.delete()
        }

        outputDir.delete()
      }
    }
    "receiving a message without eventId" must {

      "create 2 files" in {
        val license = new RegistrationImage(fileName, ImageFormat.JPG_RGB, VehicleImageType.License, ImageVersion.Original, 1234)
        val now = System.currentTimeMillis()
        val msg = new VehicleRegistrationMessage(
          lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
          eventId = None,
          eventTimestamp = Some(100L),
          event = new SensorEvent(now, new PeriodRange(now, now), "test"),
          priority = Priority.NONCRITICAL,
          files = List(license))

        convertActorRef ! msg
        //test if message are received
        probe.expectMsg(500 millis, msg)

        val outputDir = new File(outDirFile, "19700101_00")
        outputDir.exists() must be(true)
        var file = new File(outputDir, "Unknown1_license.jpg")
        file.exists() must be(true)
        file.delete()
        file = new File(outputDir, "Unknown1_meta.json")
        file.exists() must be(true)
        file.delete()
        outputDir.delete()
      }
    }
    "receiving a message without images" must {

      "create meta file" in {
        val now = System.currentTimeMillis()
        val msg = new VehicleRegistrationMessage(
          lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
          eventId = Some("200"),
          eventTimestamp = Some(100),
          event = new SensorEvent(now, new PeriodRange(now, now), "test"),
          priority = Priority.NONCRITICAL,
          files = List())

        convertActorRef ! msg
        //test if message are received
        probe.expectMsg(500 millis, msg)

        val outputDir = new File(outDirFile, "19700101_00")
        outputDir.exists() must be(true)
        val file = new File(outputDir, "200_meta.json")
        file.exists() must be(true)
        file.delete()
        outputDir.delete()
      }
    }
  }
}