package csc.vehicle.registration.images

import java.io.ByteArrayInputStream

import csc.akka.logging.DirectLogging
import csc.vehicle.message._
import org.apache.commons.io.FileUtils
import org.scalatest.WordSpec
import org.scalatest._
import unit.TestResource

/**
 * Created by carlos on 13.11.15.
 */
class ReadWriteExifUtilTest extends WordSpec with MustMatchers with DirectLogging {

  "ReadWriteExifUtil" must {
    "ensure no exif tags are lost while reading and writing" in {

      val expectedCount = 32
      val file = TestResource.getFile("csc/vehicle/registration/images/makernotes.jpg")
      val array1 = FileUtils.readFileToByteArray(file)
      val exifOriginal = ReadWriteExifUtil.readExif(new ByteArrayInputStream(array1)).toList
      exifOriginal.size must be(expectedCount) //control check

      val array2 = ReadWriteExifUtil.writeExif(array1, exifOriginal)
      val exifWritten = ReadWriteExifUtil.readExif(new ByteArrayInputStream(array2)).toList

      exifWritten.size must be(expectedCount)

      for (i ← 0 until expectedCount) {
        val t1 = exifOriginal(i)
        val t2 = exifWritten(i)
        t2.tagName must be(t1.tagName)
        t2.tagType must be(t1.tagType)
        t2.directory must be(t1.directory)

        compareValues(t1.value, t2.value)
      }
    }
  }

  def compareValues(expected: ExifValue, actual: ExifValue): Unit = expected match {
    case _: ExifValueString   ⇒ actual must be(expected)
    case _: Rational          ⇒ actual must be(expected)
    case _: ExifValueRational ⇒ actual must be(expected)
    case _                    ⇒ getSeq(actual) must be(getSeq(expected))
  }

  def getSeq(source: ExifValue): Seq[Long] = source match {
    case ExifValueBytes(array)    ⇒ array.map { v ⇒ (v & 0xFF) toLong }
    case ExifValueShorts(array)   ⇒ array.map { v ⇒ (v & 0xFFFF) toLong }
    case ExifValueIntegers(array) ⇒ array.map(_.toLong)
    case ExifValueLongs(array)    ⇒ array.toSeq
  }

}
