package csc.vehicle.registration

//copyright CSC 2010

import java.io.{ File, FileWriter }
import java.text.SimpleDateFormat
import java.util.{ Calendar, Date }

import akka.actor.{ ActorRef, ActorSystem }
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import scala.concurrent.duration._
import base.Mode
import csc.akka.logging.DirectLogging
import csc.akka.state.BackupState
import csc.gantry.config.CameraConfig
import csc.util.test.UniqueGenerator
import csc.vehicle.message.{ RegistrationRelay, VehicleRegistrationMessage, _ }
import org.apache.commons.io.FileUtils
import org.scalatest._
import testframe.Resources
import unit.CertificateTestUtil

/*
 * used for testing actor RelaySensorData
*/
class RelaySensorDataTest()
  extends TestKit(ActorSystem("RelaySensorDataTest"))
  with WordSpecLike
  with DirectLogging
  with MustMatchers
  with BeforeAndAfterAll
  with UniqueGenerator {

  import Mode._

  var fileHelper = new FileCreatorHelper(new Date)
  val resourceDir = Resources.getResourceDirPath().getAbsolutePath
  var inputPath = new File(resourceDir, "out/input").getAbsolutePath
  var lane1Path = new File(inputPath, "lane1").getAbsolutePath
  var lane2Path = new File(inputPath, "lane2").getAbsolutePath
  var camPath = new File(inputPath, "camera").getAbsolutePath
  val appChecksumValue = "2451aaa5799a967ace9fced0d6edf7c132137ca198045759ae50b771a41f8fad"
  val lane1 = new Lane("lane1", "lane1", "gantry1", "route", 0, 0)
  val lane2 = new Lane("lane2", "lane2", "gantry1", "route", 0, 0)
  val certificateValue = "cert XXXX"
  val initialCertData: Option[CertificationData] = Some(CertificationData(Map("ImageTimeExtractor" -> certificateValue), Map.empty))

  override def afterAll() {
    try {
      system.shutdown
    } catch {
      case e: Exception ⇒ log.error("COULD NOT SHUTDOWN ACTOR REGISTRY".format(e))
    }
  }

  def createActor(preProcessor: ImagePreProcessor = ImagePreProcessor.singleLaneCameraPreProcessor,
                  statePersister: Option[ActorRef] = None) = {
    //even if we don use it (Camel/Mina) we still need a valid endpoint, otherwise the actor wont start properly
    val result = TestActorRef(new RelaySensorData(
      //"mina:tcp://localhost:12400?textline=true&sync=false",
      initialCertificationData = initialCertData,
      adminRef = None,
      statePersister = statePersister,
      imagePreProcessor = preProcessor))

    //CamelExtension(system).awaitActivation(result, 20 seconds)

    result
  }

  def addLaneMessage(actor: ActorRef, lane: Lane = lane1, path: String = lane1Path): RelaySensorConfig = {
    new RelaySensorConfig(lane = lane, actor = actor, relaySensor = new CameraConfig(path, cameraHost = "localhost", cameraPort = 1400, maxTriggerRetries = 10, timeDisconnected = 20000),
      initialActivation = true, priority = Priority.NONCRITICAL, NMICertificate = CertificateTestUtil.createCertificate(certificateValue))
  }

  def assertRegistration(probe: TestProbe, file: File, lane: Lane = lane1): VehicleRegistrationMessage = {
    val expectedFile = new RegistrationRelay(file.getPath)
    val recvMsg = probe.expectMsgType[VehicleRegistrationMessage](5 seconds)
    recvMsg.lane must be(lane)
    recvMsg.files must be(List(expectedFile))
    recvMsg.eventId.get must startWith(lane.laneId.toString)

    val expectedCertData = Some(CertificationData(Map("ImageTimeExtractor" -> certificateValue)))
    recvMsg.certificationData must be(expectedCertData)
    recvMsg
  }

  "RelaySensorData" must {

    /**
     * test the basic good path. placing a file. end result must be
     * - File move to output dir
     * - message sent to recipient with path and sensor info
     */
    "perform flow of messages" in {

      val filename = "file1.jpg"
      val probe = TestProbe()
      val relayActorRef = createActor()

      relayActorRef ! addLaneMessage(probe.ref)
      //RelayActor is ready to receive messages

      val file = fileHelper.createFile(filename, lane1Path, -5, CalendarTimeUnitType.DAYS, 1)
      relayActorRef ! file

      assertRegistration(probe, file, lane1)

      system.stop(relayActorRef)
    }

    /**
     * Test the basic good path. Placing a directory. End result must be
     * - message sent to recipient with path and sensor info
     */
    "perform flow of messages for directory" in {

      val dirname = "testdir"
      val probe = TestProbe()
      val relayActorRef = createActor()

      relayActorRef ! addLaneMessage(probe.ref)
      //RelayActor is ready to receive messages

      val file = fileHelper.createDirectory(lane1Path + "/" + dirname, -5, CalendarTimeUnitType.DAYS)
      relayActorRef ! file

      assertRegistration(probe, file, lane1)

      system.stop(relayActorRef)
    }

    /**
     * test setting RelayImag.isActive on and off.
     */
    "perform active on and off" in {
      val filename = "file10.jpg"
      val filename2 = "file12.jpg"
      val filename3 = "file13.jpg"

      val probe = TestProbe()
      val relayActorRef = createActor()

      relayActorRef ! addLaneMessage(probe.ref)
      //RelayActor is ready to receive messages

      val file = fileHelper.createFile(filename, lane1Path, -5, CalendarTimeUnitType.DAYS, 1)
      relayActorRef ! file

      assertRegistration(probe, file, lane1)

      //Turn off lane activation
      relayActorRef ! Map[Long, Priority.Value]()

      val file2 = fileHelper.createFile(filename2, lane1Path, -5, CalendarTimeUnitType.DAYS, 1)
      relayActorRef ! file2

      probe.expectNoMsg(5 seconds)

      val file3 = fileHelper.createFile(filename3, lane1Path, -5, CalendarTimeUnitType.DAYS, 1)

      //Turn on lane activation
      relayActorRef ! Map[String, Priority.Value](lane1.laneId -> Priority.NONCRITICAL)

      relayActorRef ! file3

      assertRegistration(probe, file3, lane1)

      system.stop(relayActorRef)
    }

    "perform one image, two registrations" in {

      fileHelper.createDirectoryStructure(lane1Path)
      fileHelper.createDirectoryStructure(lane2Path)
      fileHelper.createDirectoryStructure(camPath)

      //dummy preprocessor that just creates 2 files, one in each lane folder
      val preProcessor: ImagePreProcessor = new ImagePreProcessor("TestPreProcessor") {
        override def apply(incoming: IncomingFile, state: LaneSensorState): List[RegistrationCandidate] = {
          val time = TimeInfo(incoming.time, None)
          val copy1 = new File(state.findLaneById(lane1.laneId).get.relaySensor.relayURI, incoming.file.getName)
          val copy2 = new File(state.findLaneById(lane2.laneId).get.relaySensor.relayURI, incoming.file.getName)
          FileUtils.copyFile(incoming.file, copy1)
          FileUtils.copyFile(incoming.file, copy2)
          copy1.setLastModified(incoming.file.lastModified())
          copy2.setLastModified(incoming.file.lastModified())
          FileUtils.deleteQuietly(incoming.file)
          List(
            RegistrationCandidate(copy1, time, RegistrationStatus.initial, None),
            RegistrationCandidate(copy2, time, RegistrationStatus.initial, None))
        }
      }

      val filename = "img" + System.currentTimeMillis() + ".jpg"
      val probe = TestProbe()
      val relayActorRef = createActor(preProcessor)

      //both lanes register
      relayActorRef ! addLaneMessage(probe.ref, lane1, lane1Path)
      relayActorRef ! addLaneMessage(probe.ref, lane2, lane2Path)

      //file is cam based
      val file = fileHelper.createFile(filename, camPath, -5, CalendarTimeUnitType.DAYS, 1)
      val expectedFile1 = new File(lane1Path, filename)
      val expectedFile2 = new File(lane2Path, filename)

      relayActorRef ! file

      //checking both registrations
      assertRegistration(probe, expectedFile1, lane1)
      assertRegistration(probe, expectedFile2, lane2)

      //checking file existence
      file.exists() must be(false)
      expectedFile1.exists() must be(true)
      expectedFile2.exists() must be(true)

      system.stop(relayActorRef)
    }

    "perform normal mode" in {
      testMode(Normal)
    }

    "perform test mode" in {
      testMode(Mode.Test)
    }

    "perform state persistence" in {
      val probe = TestProbe()
      val actor = createActor(preProcessor = ImagePreProcessor.singleLaneCameraPreProcessor, Some(probe.ref))

      val messages: List[AnyRef] = List(
        addLaneMessage(probe.ref, lane1, lane1Path),
        RelayRemoveRoute(lane1.system),
        addLaneMessage(probe.ref, lane2, lane2Path),
        Map[String, Priority.Value](lane2.laneId -> Priority.NONCRITICAL))

      for (msg ← messages) {
        actor ! msg
        probe.expectMsg(BackupState(msg))
      }

      system.stop(actor)
    }
  }

  private def testMode(mode: Mode.Value): Unit = {
    val dirname = "testdir"
    val probe = TestProbe()
    val relayActorRef = createActor()

    relayActorRef ! addLaneMessage(probe.ref)
    //RelayActor is ready to receive messages

    setMode(relayActorRef, mode)

    val file = fileHelper.createDirectory(lane1Path + "/" + dirname, -5, CalendarTimeUnitType.DAYS)
    relayActorRef ! file

    val result = assertRegistration(probe, file, lane1)

    val expectedModeFlag: Boolean = mode match {
      case Mode.Normal ⇒ false
      case Mode.Test   ⇒ true
    }

    result.status.get(RegistrationStatusFlag.testMode) must be(expectedModeFlag)

    system.stop(relayActorRef)

  }

  private def setMode(actor: ActorRef, mode: Mode.Value): Unit = {
    val probe = TestProbe()
    val req1 = SetModeRequest(nextUnique, lane1.gantry, mode.toString, System.currentTimeMillis(), "system")
    probe.send(actor, req1)

    val res1 = probe.expectMsgType[SetModeResponse]
    res1.correlationId must be(req1.correlationId)
    res1.gantry must be(req1.gantry)
    res1.mode must be(mode.toString)
    res1.error must be(None)

    val req2 = GetModeRequest(nextUnique, lane1.gantry, System.currentTimeMillis())
    probe.send(actor, req2)

    val res2 = probe.expectMsgType[GetModeResponse]
    res2.correlationId must be(req2.correlationId)
    res2.gantry must be(req1.gantry)
    res2.mode must be(mode.toString)
  }
}

//helper class used to created tmp files with the given size and modified date
class FileCreatorHelper(date: Date) extends DirectLogging {
  val files = scala.collection.mutable.Set[File]()

  def cleanup() {
    files.foreach { file ⇒
      log.info("deleting " + file.getAbsolutePath)
      log.info("file is " + (if (file.delete) " deleted" else "not deleted"))
    }
  }

  def createFile(filename: String, path: String, changes: Int, timeUnit: CalendarTimeUnitType.Value, fileSizeInMB: Long): File = {
    val dir = createDirectoryStructure(path)
    if (dir != null) {
      val newFile = new File(dir, filename)

      createFile(newFile)
      createContent(newFile, fileSizeInMB)
      changeLastModified(newFile, changes, timeUnit)
      files += newFile
      newFile
    } else throw new IllegalStateException()
  }

  def createFile(newFile: File) {
    if (newFile.exists) {
      log.info("new file already exists " + newFile.getAbsolutePath)
      val isDeleted = newFile.delete
      if (!isDeleted) {
        log.info("Failed to delete existing file " + newFile.getAbsolutePath)
      }
    } else {
      log.info("new file does not exist yet " + newFile.getAbsolutePath)
      val isCreated = newFile.createNewFile
      if (!isCreated) {
        log.info("new file can not be created " + newFile.getAbsolutePath)
      } else {
        log.info("new file is created " + newFile.getAbsolutePath)
      }
    }
  }
  def createDirectory(path: String, changes: Int, timeUnit: CalendarTimeUnitType.Value): File = {
    val dir = createDirectoryStructure(path)
    if (dir != null) {
      changeLastModified(dir, changes, timeUnit)
    }
    dir
  }

  def createDirectoryStructure(path: String): File = {
    val dir = new File(path)
    var directoryExists = dir.exists

    if (!directoryExists) {
      log.error("path does not exist " + path)
      directoryExists = dir.mkdirs
      if (!directoryExists) {
        log.error("Failed to create directory " + path)
      }
    } else {
      log.info("path already exists " + path)
    }

    return if (directoryExists) dir else null
  }

  def createContent(file: File, size: Long) {
    val output = new FileWriter(file)
    val bytes: Long = size * 1024 * 1024
    var i = 0

    while (i < bytes) {
      output.write(0);
      i += 1
    }

    output.close
  }

  def changeLastModified(file: File, changes: Int, timeUnit: CalendarTimeUnitType.Value) {
    val dateCalendar = Calendar.getInstance()
    dateCalendar.setTime(date)
    dateCalendar.add(timeUnit.id, changes)

    val dateIsSet = file.setLastModified(dateCalendar.getTime.getTime)

    if (dateIsSet) {
      log.info("setLastModified is modified")
      log.info((new SimpleDateFormat("yyyyMMdd")).format(dateCalendar.getTime))
    } else {
      log.info("setLastModified is not modified")
    }
  }
}
