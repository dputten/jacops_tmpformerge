/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import java.io.File
import java.util.Date

import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import scala.concurrent.duration._
import csc.akka.logging.DirectLogging
import csc.vehicle.config.{ ExifConfig, ExifTargetConfig }
import csc.vehicle.message.{ PeriodRange, RegistrationImage, SensorEvent, VehicleRegistrationMessage, _ }
import csc.vehicle.registration.images.ReadWriteExifUtil
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }
import testframe.Resources
import unit.base.FileTestUtils

/**
 * Test the AddMetadataToImage actor
 */
class AddMetadataToImageTest extends TestKit(ActorSystem("ExtractExifTest")) with WordSpecLike with MustMatchers with DirectLogging
  with BeforeAndAfterEach with BeforeAndAfterAll {
  import FileTestUtils._

  // resource directories
  val resourceBaseDir = new File(Resources.getResourceDirPath().getAbsolutePath)
  val extractExifResourceDir = new File(resourceBaseDir, "csc/vehicle/registration/addMetadata")
  val extractExifInputResourceDir = new File(extractExifResourceDir, "input")
  val extractExifOutputResourceDir = new File(extractExifResourceDir, "output")
  // test directories
  val testBaseDir = new File(resourceBaseDir, "out/addMetadata")
  if (!testBaseDir.exists()) {
    testBaseDir.mkdirs()
  } else {
    clearDirectory(testBaseDir)
  }
  val inputDir = new File(testBaseDir, "input")
  inputDir.mkdirs()
  val outputDir = new File(testBaseDir, "output")
  outputDir.mkdirs()

  override def afterAll() {
    try {
      system.shutdown()
    } catch {
      case e: Exception ⇒ log.error("COULD NOT SHUTDOWN ACTOR REGISTRY".format(e))
    }
    clearDirectory(testBaseDir)
    testBaseDir.delete()
  }

  override def afterEach() {
    clearDirectory(inputDir)
    clearDirectory(outputDir)
  }

  case class TestCase(input: String, output: String, info: String)

  def getCompleteMessageJson =
    """{"laneId":"1","direction":"Outgoing","speed":34.0,"length":1.399999976158142,"loop1UpTime":0,"loop1DownTime":308,"loop2UpTime":266,"loop2DownTime":573,"serialNr":"DummyValue",
      |"images":[{"url":"/data/images/raw/3215-RD/20130901144218175/RedLight.jpg","imageType":"RedLight","time":1378046537840,"offset":0,"timeYellow":0,"timeRed":0,
      |"sha":"c47f38317e24651f2c32cefdc13ba80d"},{"url":"/data/images/raw/3215-RD/20130901144218175/Overview.jpg","imageType":"Overview","time":1378046538175,"offset":0,
      |"timeYellow":0,"timeRed":0,"sha":"5d5ad481f72549c765184395f3aa6198"},{"url":"/data/images/raw/3215-RD/20130901144218175/MeasureMethod2.jpg","imageType":"MeasureMethod2",
      |"time":1378046538595,"offset":0,"timeYellow":0,"timeRed":0,"sha":"4876467e7aefb4295df02ea667786d38"}]}"""

  "An AddMetadataToImage actor, configured to add only Exif data the decorated Overview image" when {
    val probe = TestProbe()

    val config = ExifConfig(
      dirExifImage = outputDir.getAbsolutePath,
      targetConfigs = Set(ExifTargetConfig(targetImageType = VehicleImageType.Overview, targetImageVersion = ImageVersion.Decorated)),
      writeJsonToComment = false)

    val addMetadataToImageActor = TestActorRef(new AddMetadataToImage(
      targetImageType = VehicleImageType.Overview,
      config = config,
      nextActors = Set(probe.ref)))

    "receiving a message with a RegistrationImage(Overview, Decorated) and a RegistrationExifFile(Overview)" must {
      "pass on an updated message and create a copy of the image that contains the Exif data" in {
        info("RegistrationImage(Overview, Decorated) is updated with uri of new image with Exif data")
        info("old uri is added as a ObsoleteRegistrationFile(Overview)")
        // copy images
        val inputDecoratedOverviewImage = copyFile("image.jpg", extractExifInputResourceDir, inputDir)
        // prepare message
        val inputDecoratedOverview = createDecoratedOverviewRegistrationImage(inputDecoratedOverviewImage.getAbsolutePath)
        val msg = createVehicleRegistrationMessage(files = List(inputDecoratedOverview), json = Some("""{"isJson":true}"""))
        // send message
        addMetadataToImageActor ! msg
        // create expected message
        val outputDecoratedOverviewImageFile = new File(outputDir, "image_withMetadata.jpg")
        val outputDecoratedOverviewImage = createDecoratedOverviewRegistrationImage(outputDecoratedOverviewImageFile.getAbsolutePath)
        val obsoleteRegistrationFile = createObsoleteRegistrationFile(inputDecoratedOverviewImage.getAbsolutePath)
        val expectedMsg = msg.copy(files = List(outputDecoratedOverviewImage, obsoleteRegistrationFile))
        // test if message is received
        probe.expectMsg(500 millis, expectedMsg)
        // test if result is correct
        inputDecoratedOverviewImage.exists() must be(true)
        outputDecoratedOverviewImageFile.exists() must be(true)
      }
    }

    val testCases = Seq(
      TestCase(input = "image.jpg", output = "image_A1.jpg", info = "NO APP0, APP1, COM segments"),
      TestCase(input = "image_a0.jpg", output = "image_a0A1.jpg", info = "APP0 segment"),
      TestCase(input = "image_a1.jpg", output = "image_A1.jpg", info = "APP1 segment"),
      TestCase(input = "image_a0a1.jpg", output = "image_a0A1.jpg", info = "APP0, APP1 segments"),
      TestCase(input = "image_a0c.jpg", output = "image_a0A1c.jpg", info = "APP0, COM segments"),
      TestCase(input = "image_a1c.jpg", output = "image_A1c.jpg", info = "APP1, COM segments"),
      TestCase(input = "image_a0a1c.jpg", output = "image_a0A1c.jpg", info = "APP0, APP1, COM segments"))

    testCases.foreach { testCase ⇒
      "receiving a message with a RegistrationImage(%s)".format(testCase.info) must {
        "create a correct output image" in {
          // copy images
          val inputDecoratedOverviewImage = copyFile(testCase.input, extractExifInputResourceDir, inputDir)
          val inputExifFile = copyFile("app1.exf", extractExifInputResourceDir, inputDir)
          // prepare message
          val inputDecoratedOverview = createDecoratedOverviewRegistrationImage(inputDecoratedOverviewImage.getAbsolutePath)
          val msg = createVehicleRegistrationMessage(files = List(inputDecoratedOverview), json = Some("""{"isJson":true}"""))
          // send message
          addMetadataToImageActor ! msg
          // create expected message
          val outputDecoratedOverviewImageFile = new File(outputDir, "%s_withMetadata.jpg".format(testCase.input.dropRight(4))) // remove ".jpg"
          val outputDecoratedOverviewImage = createDecoratedOverviewRegistrationImage(outputDecoratedOverviewImageFile.getAbsolutePath)
          val obsoleteRegistrationFile = createObsoleteRegistrationFile(inputDecoratedOverviewImage.getAbsolutePath)
          val expectedMsg = msg.copy(files = List(outputDecoratedOverviewImage, obsoleteRegistrationFile))
          // test if message is received
          probe.expectMsg(500 millis, expectedMsg)
          // test if result is correct
          inputDecoratedOverviewImage.exists() must be(true)
          outputDecoratedOverviewImageFile.exists() must be(true)
          inputExifFile.exists() must be(true)

          val refOutputDecoratedOverviewImageFile = new File(extractExifOutputResourceDir, testCase.output)
          exifOfFilesAreEqual(outputDecoratedOverviewImageFile, refOutputDecoratedOverviewImageFile)
        }
      }

    }

    "receiving a message [RegistrationImage(Overview, Decorated), RegistrationExifFile(Overview)], but image does not exist" must {
      "pass on unchanged message" in {
        // non existing overview image
        val inputDecoratedOverviewImage = new File(inputDir, "image.jpg")
        val inputExifFile = copyFile("app1.exf", extractExifInputResourceDir, inputDir)

        // prepare message
        val inputDecoratedOverview = createDecoratedOverviewRegistrationImage(inputDecoratedOverviewImage.getAbsolutePath)
        val msg = createVehicleRegistrationMessage(files = List(inputDecoratedOverview), json = Some("""{"isJson":true}"""))
        // send message
        addMetadataToImageActor ! msg
        // create expected message
        probe.expectMsg(500 millis, msg)

        directoryIsEmpty(outputDir) must be(true)
      }
    }

    "receiving a message [RegistrationImage(Overview, Decorated), RegistrationExifFile(Overview)], but exif file does not exist" must {
      "pass on unchanged message" in {
        // non existing overview image
        val inputDecoratedOverviewImage = copyFile("image.jpg", extractExifInputResourceDir, inputDir)
        val inputExifFile = new File(inputDir, "app1.exf")

        // prepare message
        val inputDecoratedOverview = createDecoratedOverviewRegistrationImage(inputDecoratedOverviewImage.getAbsolutePath)
        val msg = createVehicleRegistrationMessage(files = List(inputDecoratedOverview.copy(metaInfo = None)), json = Some("""{"isJson":true}"""))
        // send message
        addMetadataToImageActor ! msg
        // create expected message
        probe.expectMsg(500 millis, msg)

        directoryIsEmpty(outputDir) must be(true)
      }
    }

    "receiving a message [RegistrationImage(Overview, Decorated), RegistrationExifFile(Overview)], but segments from image cannot be read" must {
      "pass on an updated message and create a copy of the image that contains the Exif data" in {
        // copy images
        val inputDecoratedOverviewImage = copyFile("cmonster.gif", extractExifInputResourceDir, inputDir)
        val inputExifFile = copyFile("app1.exf", extractExifInputResourceDir, inputDir)
        // prepare message
        val inputDecoratedOverview = createDecoratedOverviewRegistrationImage(inputDecoratedOverviewImage.getAbsolutePath)
        val msg = createVehicleRegistrationMessage(files = List(inputDecoratedOverview), json = Some("""{"isJson":true}"""))
        // send message
        addMetadataToImageActor ! msg
        // create expected message
        probe.expectMsg(500 millis, msg)

        directoryIsEmpty(outputDir) must be(true)
      }
    }
  }

  "An AddMetadataToImage actor, configured to add Exif data AND json message to the decorated Overview image" when {
    val probe = TestProbe()

    val config = ExifConfig(
      dirExifImage = outputDir.getAbsolutePath,
      targetConfigs = Set(ExifTargetConfig(targetImageType = VehicleImageType.Overview, targetImageVersion = ImageVersion.Decorated)),
      writeJsonToComment = true)

    val addMetadataToImageActor = TestActorRef(new AddMetadataToImage(
      targetImageType = VehicleImageType.Overview,
      config = config,
      nextActors = Set(probe.ref)))

    "receiving a message with a RegistrationImage(Overview, Decorated) and a RegistrationExifFile(Overview)" must {
      "pass on an updated message and create a copy of the image that contains the Exif data and json message" in {
        info("RegistrationImage(Overview, Decorated) is updated with uri of new image with Exif data")
        info("old uri is added as a ObsoleteRegistrationFile(Overview)")
        // copy images
        val inputDecoratedOverviewImage = copyFile("image.jpg", extractExifInputResourceDir, inputDir)
        // prepare message
        val inputDecoratedOverview = createDecoratedOverviewRegistrationImage(inputDecoratedOverviewImage.getAbsolutePath)
        val msg = createVehicleRegistrationMessage(files = List(inputDecoratedOverview), json = Some(getCompleteMessageJson))
        // send message
        addMetadataToImageActor ! msg
        // create expected message
        val outputDecoratedOverviewImageFile = new File(outputDir, "image_withMetadata.jpg")
        val outputDecoratedOverviewImage = createDecoratedOverviewRegistrationImage(outputDecoratedOverviewImageFile.getAbsolutePath)
        val obsoleteRegistrationFile = createObsoleteRegistrationFile(inputDecoratedOverviewImage.getAbsolutePath)
        val expectedMsg = msg.copy(files = List(outputDecoratedOverviewImage, obsoleteRegistrationFile))
        info("the COM segment contains a pretty print version of the json message")
        // test if message is received
        probe.expectMsg(500 millis, expectedMsg)
        // test if result is correct
        inputDecoratedOverviewImage.exists() must be(true)
        outputDecoratedOverviewImageFile.exists() must be(true)

        val refOutputDecoratedOverviewImageFile = new File(extractExifOutputResourceDir, "image_withExifAndPrettyJsonComment.jpg")
        exifOfFilesAreEqual(outputDecoratedOverviewImageFile, refOutputDecoratedOverviewImageFile)
      }
    }

    val testCases = Seq(
      TestCase(input = "image.jpg", output = "image_A1C_newCom.jpg", info = "NO APP0, APP1, COM segments"),
      TestCase(input = "image_a0.jpg", output = "image_a0A1C_newCom.jpg", info = "APP0 segment"),
      TestCase(input = "image_a1.jpg", output = "image_A1C_newCom.jpg", info = "APP1 segment"),
      TestCase(input = "image_a0a1.jpg", output = "image_a0A1C_newCom.jpg", info = "APP0, APP1 segments"),
      TestCase(input = "image_a0c.jpg", output = "image_a0A1C_existingCom.jpg", info = "APP0, COM segments"),
      TestCase(input = "image_a1c.jpg", output = "image_A1C_existingCom.jpg", info = "APP1, COM segments"),
      TestCase(input = "image_a0a1c.jpg", output = "image_a0A1C_existingCom.jpg", info = "APP0, APP1, COM segments"))

    testCases.foreach { testCase ⇒
      "receiving a message with a RegistrationImage(%s)".format(testCase.info) must {
        "create a correct output image" in {
          // copy images
          val inputDecoratedOverviewImage = copyFile(testCase.input, extractExifInputResourceDir, inputDir)
          val inputExifFile = copyFile("app1.exf", extractExifInputResourceDir, inputDir)
          // prepare message
          val inputDecoratedOverview = createDecoratedOverviewRegistrationImage(inputDecoratedOverviewImage.getAbsolutePath)
          val msg = createVehicleRegistrationMessage(files = List(inputDecoratedOverview), json = Some("""{"isJson":true}"""))
          // send message
          addMetadataToImageActor ! msg
          // create expected message
          val outputDecoratedOverviewImageFile = new File(outputDir, "%s_withMetadata.jpg".format(testCase.input.dropRight(4))) // remove ".jpg"
          val outputDecoratedOverviewImage = createDecoratedOverviewRegistrationImage(outputDecoratedOverviewImageFile.getAbsolutePath)
          val obsoleteRegistrationFile = createObsoleteRegistrationFile(inputDecoratedOverviewImage.getAbsolutePath)
          val expectedMsg = msg.copy(files = List(outputDecoratedOverviewImage, obsoleteRegistrationFile))
          // test if message is received
          probe.expectMsg(500 millis, expectedMsg)
          // test if result is correct
          inputDecoratedOverviewImage.exists() must be(true)
          outputDecoratedOverviewImageFile.exists() must be(true)
          inputExifFile.exists() must be(true)

          val refOutputDecoratedOverviewImageFile = new File(extractExifOutputResourceDir, testCase.output)
          exifOfFilesAreEqual(outputDecoratedOverviewImageFile, refOutputDecoratedOverviewImageFile)
        }
      }

    }

    "receiving a message [RegistrationImage(Overview, Decorated), RegistrationExifFile(Overview)], but image does not exist" must {
      "pass on unchanged message" in {
        // non existing overview image
        val inputDecoratedOverviewImage = new File(inputDir, "image.jpg")
        val inputExifFile = copyFile("app1.exf", extractExifInputResourceDir, inputDir)

        // prepare message
        val inputDecoratedOverview = createDecoratedOverviewRegistrationImage(inputDecoratedOverviewImage.getAbsolutePath)
        val msg = createVehicleRegistrationMessage(files = List(inputDecoratedOverview.copy(metaInfo = None)), json = Some("""{"isJson":true}"""))
        // send message
        addMetadataToImageActor ! msg
        // create expected message
        probe.expectMsg(500 millis, msg)

        directoryIsEmpty(outputDir) must be(true)
      }
    }

    "receiving a message [RegistrationImage(Overview, Decorated), RegistrationExifFile(Overview)], but exif file does not exist" must {
      "pass on unchanged message" in {
        // non existing overview image
        val inputDecoratedOverviewImage = copyFile("image.jpg", extractExifInputResourceDir, inputDir)
        val inputExifFile = new File(inputDir, "app1.exf")

        // prepare message
        val inputDecoratedOverview = createDecoratedOverviewRegistrationImage(inputDecoratedOverviewImage.getAbsolutePath)
        val msg = createVehicleRegistrationMessage(files = List(inputDecoratedOverview.copy(metaInfo = None)), json = Some("""{"isJson":true}"""))
        // send message
        addMetadataToImageActor ! msg
        // create expected message
        probe.expectMsg(500 millis, msg)

        directoryIsEmpty(outputDir) must be(true)
      }
    }

    "receiving a message [RegistrationImage(Overview, Decorated), RegistrationExifFile(Overview)], but segments from image cannot be read" must {
      "pass on an updated message and create a copy of the image that contains the Exif data" in {
        // copy images
        val inputDecoratedOverviewImage = copyFile("cmonster.gif", extractExifInputResourceDir, inputDir)
        // prepare message
        val inputDecoratedOverview = createDecoratedOverviewRegistrationImage(inputDecoratedOverviewImage.getAbsolutePath)
        val msg = createVehicleRegistrationMessage(files = List(inputDecoratedOverview), json = Some("""{"isJson":true}"""))
        // send message
        addMetadataToImageActor ! msg
        // create expected message
        probe.expectMsg(500 millis, msg)

        val files = outputDir.list()
        if (files != null) {
          files.foreach(f ⇒ println("file: " + f))
        }
        directoryIsEmpty(outputDir) must be(true)
      }
    }
  }

  def createVehicleRegistrationMessage(files: List[RegistrationFile], json: Option[String] = None): VehicleRegistrationMessage = {
    val now = System.currentTimeMillis()
    new VehicleRegistrationMessage(
      lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
      eventId = Some("A2-44.5MH-Lane1"),
      eventTimestamp = Some(100L),
      event = new SensorEvent(now, new PeriodRange(now, now), "test"),
      priority = Priority.NONCRITICAL,
      files = files,
      jsonMessage = json)
  }

  def createDecoratedOverviewRegistrationImage(path: String): RegistrationImage = {
    new RegistrationImage(
      uri = path,
      imageFormat = ImageFormat.JPG_RGB,
      imageType = VehicleImageType.Overview,
      imageVersion = ImageVersion.Decorated,
      timestamp = 1234,
      timeYellow = Some(12342L),
      timeRed = Some(333L),
      metaInfo = Some(createExifTagInfo()))
  }

  def createObsoleteRegistrationFile(path: String): ObsoleteRegistrationFile = {
    new ObsoleteRegistrationFile(
      uri = path)
  }

  def createExifTagInfo(): Seq[ExifTagInfo] = {
    Seq(
      ExifTagInfo(ExifTagInfo.directoryExif_SubIFD, "Exif Version", 36864, ExifValueBytes(Array[Byte](48, 50, 50, 48))),
      ExifTagInfo(ExifTagInfo.directoryExif_SubIFD, "Date/Time Original", 36867, ExifValueString("2014:08:06 08:37:38")),
      ExifTagInfo(ExifTagInfo.directoryExif_SubIFD, "Sub-Sec Time", 37520, ExifValueString("935")),
      ExifTagInfo(ExifTagInfo.directoryExif_SubIFD, "Sub-Sec Time Original", 37521, ExifValueString("935")),
      ExifTagInfo(ExifTagInfo.directoryExif_SubIFD, "Unique Image ID", 42016, ExifValueString("MAC=00 0C DF 85 00 4C IP=192.168.2.20 UserInfo=3238-new-camera-uit-3255")),
      ExifTagInfo(ExifTagInfo.directoryExif_SubIFD, "Body Serial Number", 42033, ExifValueString("SN00000077")),
      ExifTagInfo(ExifTagInfo.directoryExif_IFD0, "Software", 305, ExifValueString("APP=140402A0; FPGA=140716A0; FPGAINI=140402A0; OS=130621A0")),
      ExifTagInfo(ExifTagInfo.directoryExif_IFD0, "Date/Time", 306, ExifValueString("2014:08:06 08:37:38")),
      ExifTagInfo(ExifTagInfo.directoryExif_IFD0, "Copyright", 33432, ExifValueString("Copyright (C) JAI Inc.")))
  }

  def exifOfFilesAreEqual(found: File, expected: File) {
    var foundExif = ReadWriteExifUtil.readExif(found)
    var expectedFileExif = ReadWriteExifUtil.readExif(expected)

    expectedFileExif = expectedFileExif.filter(tag ⇒ tag.directory == ExifTagInfo.directoryJpeg ||
      tag.directory == ExifTagInfo.directoryJpegComment ||
      tag.directory == ExifTagInfo.directoryJfif)

    val addedExif = expectedFileExif ++ createExifTagInfo
    //Exif Version is changed from byte to shorts
    val correctedAddedExif = addedExif.map(tag ⇒ tag.value match {
      case ExifValueBytes(bytes) ⇒ {
        tag.copy(value = ExifValueShorts(bytes.map(_.toShort)))
      }
      case ExifValueLongs(longs) ⇒ {
        tag.copy(value = ExifValueIntegers(longs.map(_.toInt)))
      }
      case _ ⇒ tag
    })

    sortExifTagInfo(foundExif) must be(sortExifTagInfo(correctedAddedExif))
  }

  def sortExifTagInfo(list: Seq[ExifTagInfo]): Seq[ExifTagInfo] = {
    list.sortBy(_.toString)
  }
}
