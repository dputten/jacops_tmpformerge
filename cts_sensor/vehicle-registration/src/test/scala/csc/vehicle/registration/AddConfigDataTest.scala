/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import java.util.Date

import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import scala.concurrent.duration._
import csc.akka.logging.DirectLogging
import csc.gantry.config.{ SerialNumber, SystemConfig }
import csc.vehicle.message.{ PeriodRange, SensorEvent, VehicleRegistrationMessage, _ }
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import org.scalatest._

class AddConfigDataTest extends TestKit(ActorSystem("AddConfigDataTest")) with WordSpecLike with MustMatchers with BeforeAndAfterAll with DirectLogging {
  override def afterAll() {
    try {
      system.shutdown
    } catch {
      case e: Exception ⇒ log.error("COULD NOT SHUTDOWN ACTOR REGISTRY".format(e))
    }
  }

  "AddConfigData" must {
    "Update serial when empty and config" in {
      val serial = "SN123"
      val nmi = "PD123"
      val creationTime = System.currentTimeMillis()
      val mock = MockSystemConfig(serial, nmi)
      val probe = TestProbe()
      val actor = TestActorRef(new AddConfigData(mock, Set(probe.ref)))
      val msg = new VehicleRegistrationMessage(eventId = Some("12345"),
        event = new SensorEvent(creationTime, new PeriodRange(creationTime, creationTime), "relay"),
        priority = Priority.NONCRITICAL, lane = Lane(laneId = "",
          name = "R1",
          gantry = "E1",
          system = "A2",
          sensorGPS_longitude = 52.4,
          sensorGPS_latitude = 5.4), files = List())
      actor ! msg
      val recv = probe.expectMsgType[VehicleRegistrationMessage](1.second)
      recv.serialNr must be(Some(serial))
    }
    "Not update serial when empty and config is empty" in {
      val serial = ""
      val nmi = "PD123"
      val creationTime = System.currentTimeMillis()
      val mock = MockSystemConfig(serial, nmi)
      val probe = TestProbe()
      val actor = TestActorRef(new AddConfigData(mock, Set(probe.ref)))
      val msg = new VehicleRegistrationMessage(eventId = Some("12345"),
        event = new SensorEvent(creationTime, new PeriodRange(creationTime, creationTime), "relay"),
        priority = Priority.NONCRITICAL, lane = Lane(laneId = "",
          name = "R1",
          gantry = "E1",
          system = "A2",
          sensorGPS_longitude = 52.4,
          sensorGPS_latitude = 5.4), files = List())
      actor ! msg
      val recv = probe.expectMsgType[VehicleRegistrationMessage](1.second)
      recv.serialNr must be(None)

    }
    "Not update serial when is filled and config" in {
      val serial = "SN123"
      val nmi = "PD123"
      val creationTime = System.currentTimeMillis()
      val mock = MockSystemConfig(serial, nmi)
      val probe = TestProbe()
      val actor = TestActorRef(new AddConfigData(mock, Set(probe.ref)))
      val msg = new VehicleRegistrationMessage(eventId = Some("12345"),
        event = new SensorEvent(creationTime, new PeriodRange(creationTime, creationTime), "relay"),
        priority = Priority.NONCRITICAL, lane = Lane(laneId = "",
          name = "R1",
          gantry = "E1",
          system = "A2",
          sensorGPS_longitude = 52.4,
          sensorGPS_latitude = 5.4), files = List(),
        certificationData = Some(CertificationData(Map.empty, Map("serialNr" -> "123456"))))
      actor ! msg
      val recv = probe.expectMsgType[VehicleRegistrationMessage](1.second)
      recv.serialNr must be(msg.serialNr)
    }
    "Update NMICert" in {
      val serial = "SN123"
      val nmi = "PD123"
      val creationTime = System.currentTimeMillis()
      val mock = MockSystemConfig(serial, nmi)
      val probe = TestProbe()
      val actor = TestActorRef(new AddConfigData(mock, Set(probe.ref)))
      val msg = new VehicleRegistrationMessage(eventId = Some("12345"),
        certificationData = Some(CertificationData(Map("NMICertificate" -> "NMI"), Map.empty)),
        event = new SensorEvent(creationTime, new PeriodRange(creationTime, creationTime), "relay"),
        priority = Priority.NONCRITICAL, lane = Lane(laneId = "",
          name = "R1",
          gantry = "E1",
          system = "A2",
          sensorGPS_longitude = 52.4,
          sensorGPS_latitude = 5.4), files = List())
      actor ! msg
      val recv = probe.expectMsgType[VehicleRegistrationMessage](1.second)
      recv.certificationData.get.componentChecksumMap("NMICertificate") must be(nmi)

    }
  }

}

case class MockSystemConfig(serial: String, nmi: String) extends SystemConfig {
  def id = null

  def name = null

  def description = null

  def maxSpeed = 0

  def compressionFactor = 0

  def nrDaysKeepTrafficData = 0

  def nrDaysKeepViolations = 0

  def region = null

  def roadNumber = null

  def roadPart = null

  def typeCertificates = Seq()

  def serialNumber = SerialNumber(serial)

  def activeChecksums = null

  override def typeCertificate(time: Long): String = nmi

}