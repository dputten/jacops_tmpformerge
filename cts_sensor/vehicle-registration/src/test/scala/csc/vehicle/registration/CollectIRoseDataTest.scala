/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import java.io.File
import java.text.SimpleDateFormat
import java.util.{ Date, TimeZone }

import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import scala.concurrent.duration._
import csc.akka.logging.DirectLogging
import csc.vehicle.message.{ PeriodRange, SensorEvent, VehicleRegistrationMessage, _ }
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }
import testframe.Resources

/**
 * Test the CollectIRoseData
 */
class CollectIRoseDataTest extends TestKit(ActorSystem("CollectIRoseDataTest")) with WordSpecLike with MustMatchers with DirectLogging
  with BeforeAndAfterEach with BeforeAndAfterAll {
  import IRoseDataHelper._
  val datePattern = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss.SSS")
  datePattern.setTimeZone(TimeZone.getTimeZone("UTC"))

  val resourceDir = Resources.getResourceDirPath().getAbsolutePath
  val inputDir = new File(resourceDir, "out/irose-input")
  if (!inputDir.exists()) {
    inputDir.mkdirs()
  } else {
    clearDirectory(inputDir)
  }

  override def afterAll() {
    system.shutdown()

    inputDir.delete()
  }

  override def afterEach() {
    clearDirectory(inputDir)
  }

  def createTime(str: String): Option[Long] = {
    Some(datePattern.parse(str).getTime)
  }

  def completeJsonWith3Images(withChecksums: Boolean = false): Unit = {
    createFile(inputDir, defaultRedLightJpg)
    createFile(inputDir, defaultOverviewJpg)
    createFile(inputDir, defaultMeasureMethod2Jpg)
    createCompleteAndCorrectJsonFile(inputDir, withChecksums)
  }

  def completeJsonWith3RealImages(withChecksums: Boolean = false): Unit = {
    createCompleteAndCorrectJsonFileWithRealJpg(inputDir, withChecksums)
  }

  //  Exif info vanuit de foto's
  //  foto1==redlight       -> 2014:03:03 12:19:39 810
  //  foto2==alle overviews -> 2014:03:03 12:19:40 930 -> 1120
  //  foto3==MeasureMethod2 -> 2014:03:03 12:19:41 590 -> 660

  "The CollectIRoseData With Real Image" when {
    val probe = TestProbe()
    val collectIRoseDataActorRef = TestActorRef(new CollectIRoseData(nextActors = Set(probe.ref)))

    Seq((false, "without checksums"), (true, "with correct checksums")).foreach { testData ⇒
      val (checksumEnabled, text) = testData
      "receiving a message with a RelayFile referencing a directory containing a complete and correct json file and the 3 corresponding images (%s checksums)".format(text) must {
        "send a message to its nextActors, containing the collected IRose data from the directory given" in {
          // initialize the directory with the data expected
          completeJsonWith3RealImages(checksumEnabled)
          // create message
          val now = System.currentTimeMillis()
          val fileWithDirectory = new RegistrationRelay(inputDir.getAbsolutePath)
          val msg = new VehicleRegistrationMessage(
            lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
            eventId = Some("100"),
            eventTimestamp = Some(100L),
            event = new SensorEvent(now, new PeriodRange(now, now), "test"),
            priority = Priority.NONCRITICAL,
            files = List(fileWithDirectory))
          // construct expected message
          val eventTimeStamp = Some(12340011L)
          val speed = Some(new ValueWithConfidence[Float](123.6F, 95))
          val length = Some(new ValueWithConfidence[Float](4.5F, 95))

          val redLightUrl = getClass.getClassLoader.getResource("RedLight.jpg").getPath
          val overviewUrl = getClass.getClassLoader.getResource("Overview.jpg").getPath
          val measureMethod2Url = getClass.getClassLoader.getResource("MeasureMethod2.jpg").getPath

          val expectedRedLightImage = new RegistrationImage(
            uri = redLightUrl,
            imageFormat = ImageFormat.JPG_RGB,
            imageType = VehicleImageType.RedLight,
            imageVersion = ImageVersion.Original,
            timestamp = 12340000,
            timeYellow = Some(1111),
            timeRed = Some(11),
            imageTimeStamp = createTime("2014:03:03 12:19:39.810"))

          val expectedOverviewImage = new RegistrationImage(
            uri = overviewUrl,
            imageFormat = ImageFormat.JPG_RGB,
            imageType = VehicleImageType.Overview,
            imageVersion = ImageVersion.Original,
            timestamp = 12340011,
            timeYellow = Some(2222),
            timeRed = Some(22),
            imageTimeStamp = createTime("2014:03:03 12:19:40.930"))

          val expectedOverviewRedLightImage = new RegistrationImage(
            uri = overviewUrl,
            imageFormat = ImageFormat.JPG_RGB,
            imageType = VehicleImageType.OverviewRedLight,
            imageVersion = ImageVersion.Original,
            timestamp = 12340011,
            timeYellow = Some(2222),
            timeRed = Some(22),
            imageTimeStamp = createTime("2014:03:03 12:19:40.930"))

          val expectedOverviewSpeedImage = new RegistrationImage(
            uri = overviewUrl,
            imageFormat = ImageFormat.JPG_RGB,
            imageType = VehicleImageType.OverviewSpeed,
            imageVersion = ImageVersion.Original,
            timestamp = 12340011,
            timeYellow = Some(2222),
            timeRed = Some(22),
            imageTimeStamp = createTime("2014:03:03 12:19:40.930"))

          val expectedMeasureMethod2SpeedImage = new RegistrationImage(
            uri = measureMethod2Url,
            imageFormat = ImageFormat.JPG_RGB,
            imageType = VehicleImageType.MeasureMethod2Speed,
            imageVersion = ImageVersion.Original,
            timestamp = 12340022,
            timeYellow = Some(3333),
            timeRed = Some(33),
            imageTimeStamp = createTime("2014:03:03 12:19:41.590"))
          val expectedJsonFile = new RegistrationJsonFile(
            uri = new File(inputDir, defaultIRoseJson).getAbsolutePath)
          val directory = Some(new RelayDirectory(inputDir.getAbsolutePath))
          val expectedMsg = new VehicleRegistrationMessage(
            lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
            eventId = Some("100"),
            eventTimestamp = eventTimeStamp,
            event = new SensorEvent(now, new PeriodRange(now, now), "test"),
            priority = Priority.NONCRITICAL,
            licenseData = RegistrationLicenseData(),
            vehicleData = VehicleData(direction = Some(Direction.Outgoing),
              speed = speed,
              length = length),
            files = List(expectedRedLightImage, expectedOverviewImage, expectedMeasureMethod2SpeedImage, expectedOverviewRedLightImage, expectedOverviewSpeedImage, expectedJsonFile),
            directory = directory,
            certificationData = Some(CertificationData(Map.empty, Map("serialNr" -> "SN1234"))),
            jsonMessage = Some(getCompleteMessageJson),
            interval1Ms = Some(1120),
            interval2Ms = Some(660))
          // send message to CollectIRoseData
          collectIRoseDataActorRef ! msg
          // test if expected message is received
          val recvMsg = probe.expectMsgType[VehicleRegistrationMessage](500 millis)
          //messages should be equal exept the recieved message contains a lot al exif info
          val cleanFiles = recvMsg.files.map {
            case imagefile: RegistrationImage ⇒ imagefile.copy(metaInfo = None)
            case other                        ⇒ other
          }
          recvMsg.copy(files = cleanFiles) must be(expectedMsg)
        }
      }
    }
  }

  "The CollectIRoseData with fake jpg" when {
    val probe = TestProbe()
    val collectIRoseDataActorRef = TestActorRef(new CollectIRoseData(nextActors = Set(probe.ref)))

    Seq((false, "without checksums"), (true, "with correct checksums")).foreach { testData ⇒
      val (checksumEnabled, text) = testData
      "receiving a message with a RelayFile referencing a directory containing a complete and correct json file and the 3 corresponding images (%s checksums)".format(text) must {
        "send a message to its nextActors, containing the collected IRose data from the directory given" in {
          // initialize the directory with the data expected
          //completeJsonWith3RealImages(checksumEnabled)
          completeJsonWith3Images(checksumEnabled)
          // create message
          val now = System.currentTimeMillis()
          val fileWithDirectory = new RegistrationRelay(inputDir.getAbsolutePath)
          val msg = new VehicleRegistrationMessage(
            lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
            eventId = Some("100"),
            eventTimestamp = Some(100L),
            event = new SensorEvent(now, new PeriodRange(now, now), "test"),
            priority = Priority.NONCRITICAL,
            files = List(fileWithDirectory))
          // construct expected message
          val eventTimeStamp = Some(12340011L)
          val speed = Some(new ValueWithConfidence[Float](123.6F, 95))
          val length = Some(new ValueWithConfidence[Float](4.5F, 95))

          val expectedRedLightImage = new RegistrationImage(
            uri = new File(inputDir, defaultRedLightJpg).getAbsolutePath,
            imageFormat = ImageFormat.JPG_RGB,
            imageType = VehicleImageType.RedLight,
            imageVersion = ImageVersion.Original,
            timestamp = 12340000,
            timeYellow = Some(1111),
            timeRed = Some(11), imageTimeStamp = None)

          val expectedOverviewImage = new RegistrationImage(
            uri = new File(inputDir, defaultOverviewJpg).getAbsolutePath,
            imageFormat = ImageFormat.JPG_RGB,
            imageType = VehicleImageType.Overview,
            imageVersion = ImageVersion.Original,
            timestamp = 12340011,
            timeYellow = Some(2222),
            timeRed = Some(22), imageTimeStamp = None)

          val expectedOverviewRedLightImage = new RegistrationImage(
            uri = new File(inputDir, defaultOverviewJpg).getAbsolutePath,
            imageFormat = ImageFormat.JPG_RGB,
            imageType = VehicleImageType.OverviewRedLight,
            imageVersion = ImageVersion.Original,
            timestamp = 12340011,
            timeYellow = Some(2222),
            timeRed = Some(22), imageTimeStamp = None)

          val expectedOverviewSpeedImage = new RegistrationImage(
            uri = new File(inputDir, defaultOverviewJpg).getAbsolutePath,
            imageFormat = ImageFormat.JPG_RGB,
            imageType = VehicleImageType.OverviewSpeed,
            imageVersion = ImageVersion.Original,
            timestamp = 12340011,
            timeYellow = Some(2222),
            timeRed = Some(22), imageTimeStamp = None)

          val expectedMeasureMethod2SpeedImage = new RegistrationImage(
            uri = new File(inputDir, defaultMeasureMethod2Jpg).getAbsolutePath,
            imageFormat = ImageFormat.JPG_RGB,
            imageType = VehicleImageType.MeasureMethod2Speed,
            imageVersion = ImageVersion.Original,
            timestamp = 12340022,
            timeYellow = Some(3333),
            timeRed = Some(33), imageTimeStamp = None)
          val expectedJsonFile = new RegistrationJsonFile(
            uri = new File(inputDir, defaultIRoseJson).getAbsolutePath)
          val directory = Some(new RelayDirectory(inputDir.getAbsolutePath))
          val expectedMsg = new VehicleRegistrationMessage(
            lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
            eventId = Some("100"),
            eventTimestamp = eventTimeStamp,
            event = new SensorEvent(now, new PeriodRange(now, now), "test"),
            priority = Priority.NONCRITICAL,
            licenseData = RegistrationLicenseData(),
            vehicleData = VehicleData(direction = Some(Direction.Outgoing),
              speed = speed,
              length = length),
            files = List(expectedRedLightImage, expectedOverviewImage, expectedMeasureMethod2SpeedImage, expectedOverviewRedLightImage, expectedOverviewSpeedImage, expectedJsonFile),
            directory = directory,
            certificationData = Some(CertificationData(Map.empty, Map("serialNr" -> "SN1234"))),
            jsonMessage = Some(getCompleteMessageJson),
            interval1Ms = None,
            interval2Ms = None)
          // send message to CollectIRoseData
          collectIRoseDataActorRef ! msg
          // test if expected message is received
          val recvMsg = probe.expectMsgType[VehicleRegistrationMessage](500 millis)
          //messages should be equal exept the recieved message contains a lot al exif info
          val cleanFiles = recvMsg.files.map {
            case imagefile: RegistrationImage ⇒ imagefile.copy(metaInfo = None)
            case other                        ⇒ other
          }
          recvMsg.copy(files = cleanFiles) must be(expectedMsg)
        }
      }
    }

    "receiving a message with a RelayFile referencing a directory containing a complete and correct json file and the 3 corresponding images (with wrong checksums)" must {
      "do nothing" in {
        // initialize the directory with the data expected
        createFile(inputDir, defaultRedLightJpg)
        createFile(inputDir, defaultOverviewJpg)
        createFile(inputDir, defaultMeasureMethod2Jpg)
        createJsonFileWithWrongChecksum(inputDir)

        // create message
        val now = System.currentTimeMillis()
        val fileWithDirectory = new RegistrationRelay(inputDir.getAbsolutePath)
        val msg = new VehicleRegistrationMessage(
          lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
          eventId = Some("100"),
          eventTimestamp = Some(100L),
          event = new SensorEvent(now, new PeriodRange(now, now), "test"),
          priority = Priority.NONCRITICAL,
          licenseData = RegistrationLicenseData(),
          files = List(fileWithDirectory))

        // send message to CollectIRoseData
        collectIRoseDataActorRef ! msg
        // test if expected message is received
        probe.expectNoMsg(500 millis)
      }
    }
  }

}
