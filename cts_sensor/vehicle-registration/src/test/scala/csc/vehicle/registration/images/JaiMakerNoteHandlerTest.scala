package csc.vehicle.registration.images

import java.io.ByteArrayInputStream

import csc.akka.logging.DirectLogging
import csc.vehicle.message._
import org.apache.commons.io.FileUtils
import org.scalatest.WordSpec
import org.scalatest._
import unit.TestResource

/**
 * Created by carlos on 12.11.15.
 */
class JaiMakerNoteHandlerTest extends WordSpec with MustMatchers with DirectLogging {

  "JaiMakerNoteHandler" must {
    "read properly all the Jai MakerNotes content, for L" in {
      val makerNotes = getMakerNotes("csc/vehicle/registration/images/jai/nolicense.jpg")

      var _idx: Int = 0

      def idx: Int = {
        val value = _idx
        _idx = _idx + 1
        value
      }

      makerNotes.values.size must be(22)
      makerNotes.values(idx) must be(shortField(0xE000, 34816))
      makerNotes.values(idx) must be(shortField(0xE002, 7))
      makerNotes.values(idx) must be(shortField(0xE003, 0))
      makerNotes.values(idx) must be(shortField(0xE004, 188))
      makerNotes.values(idx) must be(shortField(0xE005, 1000))
      makerNotes.values(idx) must be(byteField(0xE008, 2))
      makerNotes.values(idx) must be(byteField(0xE009, 1))
      makerNotes.values(idx) must be(byteField(0xE00A, 7))
      makerNotes.values(idx) must be(longField(0xE00C, 16))
      makerNotes.values(idx) must be(longField(0xE010, 0))
      makerNotes.values(idx) must be(shortField(0xE00F, 17))
      makerNotes.values(idx) must be(shortField(0xE016, 0))
      makerNotes.values(idx) must be(shortField(0xE017, 0))
      makerNotes.values(idx) must be(byteField(0xE018, 0))
      makerNotes.values(idx) must be(shortField(0xE019, 855))
      makerNotes.values(idx) must be(shortField(0xE01C, 80))
      makerNotes.values(idx) must be(shortField(0xE01D, 100))
      makerNotes.values(idx) must be(byteField(0xE030, 2))
      makerNotes.values(idx) must be(byteField(0xE031, 2))
      makerNotes.values(idx) must be(shortField(0xE032, 16))
      makerNotes.values(idx) must be(byteField(0xE033, 2))

      val last = makerNotes.values(idx)
      last.tagType must be(0xE034)
      //val array = last.value.asInstanceOf[TagValueByteArray].value
      //array.length must be(60) //TODO csilva

      val opt = makerNotes.getTriggerSource
      opt.isDefined must be(true)
      val ts = opt.get
      ts.isLeft must be(true)
      ts.isRight must be(false)
    }

    "read the trigger source, for R" in {
      val makerNotes = getMakerNotes("unit/image/rightTrigger.jpg")
      val opt = makerNotes.getTriggerSource
      opt.isDefined must be(true)
      val ts = opt.get
      ts.isLeft must be(false)
      ts.isRight must be(true)
    }

    "read the trigger source, for L+R" in {
      val makerNotes = getMakerNotes("unit/image/bothTriggers.jpg")
      val opt = makerNotes.getTriggerSource
      opt.isDefined must be(true)
      val ts = opt.get
      ts.isLeft must be(true)
      ts.isRight must be(true)
    }

    def getMakerNotes(path: String): JaiMakerNotes = {
      val file = TestResource.getFile(path)
      val array1 = FileUtils.readFileToByteArray(file)
      val exif = ReadWriteExifUtil.readExif(new ByteArrayInputStream(array1)).toList
      //exif.foreach(println)
      JaiMakerNoteHandler.getJaiMakerNotes(exif)
    }

    "be able to keep all Jai MakerNotes after exif rewrite" in {
      val file = TestResource.getFile("csc/vehicle/registration/images/jai/nolicense.jpg")
      val array1 = FileUtils.readFileToByteArray(file)
      val exif = ReadWriteExifUtil.readExif(new ByteArrayInputStream(array1)).toList

      //exif.foreach(println)
      val makerNotes = JaiMakerNoteHandler.getJaiMakerNotes(exif)

      val array2 = ReadWriteExifUtil.writeExif(array1, exif)
      val exif2 = ReadWriteExifUtil.readExif(new ByteArrayInputStream(array2)).toList
      val makerNotes2 = JaiMakerNoteHandler.getJaiMakerNotes(exif2)

      compareValues(makerNotes, makerNotes2)
    }
  }

  def shortField(tagId: Int, value: Int) = MakerNoteInfo(tagId, TagValueShort(value))
  def byteField(tagId: Int, value: Int) = MakerNoteInfo(tagId, TagValueByte(value))
  def longField(tagId: Int, value: Int) = MakerNoteInfo(tagId, TagValueLong(value))

  "TriggerSource" must {
    "have a left source" in {
      TriggerSource(16).isLeft must be(true)
    }
    "have a right source" in {
      TriggerSource(32).isRight must be(true)
    }
    "have a left + right source" in {
      TriggerSource(16 + 32).isRight must be(true)
    }
  }

  def compareValues(expected: JaiMakerNotes, actual: JaiMakerNotes): Unit = {

    actual.getTriggerSource must be(expected.getTriggerSource)

    val expectedCount = expected.values.size
    actual.values.size must be(expectedCount)

    for (i ← 0 until expectedCount) {
      val t1 = expected.values(i)
      val t2 = actual.values(i)
      t2.tagType must be(t1.tagType)
      compareValues(t1.value, t2.value)
    }
  }

  def compareValues(expected: TagValue[_], actual: TagValue[_]): Unit = (expected, actual) match {
    case (TagValueByteArray(array1), TagValueByteArray(array2)) ⇒ getSeq(array2) must be(getSeq(array1))
    case _ ⇒ actual must be(expected)
  }

  def getSeq(array: Array[Byte]): Seq[Int] = array.map { v ⇒ (v & 0xFF) }

}
