/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import csc.akka.logging.DirectLogging
import csc.gantry.config.RadarConfig
import org.junit.Test
import org.scalatest.MustMatchers
import org.scalatest.junit.JUnitSuite

/**
 * Test the Radar Calculations.
 */
class RadarCalculationsTest extends JUnitSuite with DirectLogging with MustMatchers {

  /**
   * Test the length calculations.
   */
  @Test
  def testLength {
    val config = new RadarConfig("mina:tcp://localhost:11001?textline=true", 45, 5, 1)
    val calc = new RadarCalculations(config)
    calc.factor must be > (0D)
    calc.beamDistance must be > (0D)
    val length = calc.calculateLength(367)
    math.abs(length.value - 4) must be < (0.1F) //Length must be around 4 m
    val length2 = calc.calculateLength(800)
    math.abs(length2.value - 11) must be < (0.1F) //Length must be around 11 m
    val length3 = calc.calculateLength(1000)
    math.abs(length3.value - 14) must be < (0.2F) //Length must be around 14 m

  }
  /**
   * Test the length calculations with reflection 50%.
   */
  @Test
  def testLengthReflection {
    val config = new RadarConfig("mina:tcp://localhost:11002?textline=true", 45, 5, 0.5)
    val calc = new RadarCalculations(config)
    calc.factor must be > (0D)
    calc.beamDistance must be > (0D)

    val length = calc.calculateLength(367)
    math.abs(length.value - 10) must be < (0.2F) //Length must be around 10 m
    val length2 = calc.calculateLength(1000)
    math.abs(length2.value - 30) must be < (0.2F) //Length must be around 28 m

  }

  /**
   * Test the speed confidence
   */
  @Test
  def testSpeedConfidence {
    val config = new RadarConfig("mina:tcp://localhost:11002?textline=true", 45, 5, 0.5)
    val calc = new RadarCalculations(config)

    calc.calculateSpeed(0).confidence must be(0)
    calc.calculateSpeed(1).confidence must be(0)
    calc.calculateSpeed(3).confidence must be(0)
    calc.calculateSpeed(4).confidence must be(25)
    calc.calculateSpeed(53).confidence must be(94)
    calc.calculateSpeed(100).confidence must be(97)
    calc.calculateSpeed(101).confidence must be(97)
    calc.calculateSpeed(130).confidence must be(97)
  }
  /**
   * Test the speed and confidence for incorrect values
   */
  @Test
  def testSpeedError {
    val config = new RadarConfig("mina:tcp://localhost:11002?textline=true", 45, 5, 0.5)
    val calc = new RadarCalculations(config)

    calc.calculateSpeed(-130).confidence must be(0)
    calc.calculateSpeed(-130).value must be(0)
  }

}