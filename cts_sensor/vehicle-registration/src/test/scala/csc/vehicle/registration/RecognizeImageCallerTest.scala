package csc.vehicle.registration

//copyright CSC 2010

import java.util.Date

import akka.actor.{ Actor, ActorSystem, Props }
import akka.testkit.{ TestKit, TestProbe }

import scala.concurrent.duration._
import csc.akka.logging.DirectLogging
import csc.gantry.config.{ CameraConfig, HonacSensorConfigImpl }
import csc.image.recognize.{ RecognizeImageRequest, RecognizeImageResponse, Recognizer, TranslatePlate }
import csc.vehicle.config.RecognizeConfig
import csc.vehicle.message._
import csc.vehicle.registration.recognizer.RecognizeImageCaller
import imagefunctions.{ ImageFunctionsException, LicensePlate, Point }
import org.scalatest._

/**
 * Test the RecognizeImage class
 */

class RecognizeImageCallerTest
  extends TestKit(ActorSystem("RecognizeImageCallerTest"))
  with WordSpecLike
  with DirectLogging
  with MustMatchers
  with BeforeAndAfterAll {

  val cfg = new RecognizeConfig(maxRetries = 3, recognizeTimeout = 1.minute)
  val delegatePath = "/user/recognize"

  val laneConfig = new HonacSensorConfigImpl(
    lane = Lane(laneId = "3215-1-RD",
      name = "RD",
      gantry = "1",
      system = "3215",
      sensorGPS_longitude = 52.4,
      sensorGPS_latitude = 4.52),
    camera = CameraConfig(relayURI = "", cameraHost = "", cameraPort = 1400, maxTriggerRetries = 5, timeDisconnected = 1000),
    imageMask = None,
    recognizeOptions = Map())

  val triggerTime = System.currentTimeMillis()

  override def afterAll() {
    try {
      system.shutdown()
    } catch {
      case e: Exception ⇒ log.error("COULD NOT SHUTDOWN ACTOR REGISTRY".format(e))
    }
  }

  "RecognizeImageCaller" must {
    "perform the good scenario with a mock" in {
      /*
       * This test uses the ImageFunctionsTestImpl which is a mock of the ImageFunctionsTestImpl ImageFunctions implementation.
       * The mock is setup to behave correctly (a good scenario)
       */
      val probe = TestProbe()
      val mock = system.actorOf(Props(new TestRecognizer()), "recognize")
      val recognizeImageRef = system.actorOf(Props(new RecognizeImageCaller(
        delegatePath,
        recognizeCfg = cfg,
        laneConfigProvider = Right(laneConfig),
        candidateTypes = List(VehicleImageType.Overview, VehicleImageType.License),
        recipients = Set(probe.ref))))

      //create message
      val image = new RegistrationImage("TestFile.tiff", ImageFormat.TIF_BAYER, VehicleImageType.Overview, ImageVersion.Original, 1234)
      val msg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventId = Some("1"),
        eventTimestamp = None,
        event = new SensorEvent(triggerTime, new PeriodRange(triggerTime, triggerTime), "test"),
        priority = Priority.NONCRITICAL,
        files = List(image))

      recognizeImageRef ! msg
      val imageLicense = new RegistrationImage("snippet.tif", ImageFormat.TIF_RGB, VehicleImageType.License, ImageVersion.Original, 1234)
      val expectedMsg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventId = Some("1"),
        eventTimestamp = None,
        event = new SensorEvent(triggerTime, new PeriodRange(triggerTime, triggerTime), "test"),
        priority = Priority.NONCRITICAL,
        licenseData = RegistrationLicenseData(
          license = Some(new ValueWithConfidence[String]("12abcd", 90)),
          licensePosition = Some(LicensePosition(TLx = 100, TLy = 100, TRx = 400, TRy = 100, BLx = 100, BLy = 400, BRx = 400, BRy = 400)),
          licenseSourceImage = Some(VehicleImageType.Overview),
          country = Some(new ValueWithConfidence[String]("NL", 90)),
          recognizedBy = Some(Recognizer.ICR_PLATEFINDER)),
        files = List(image, imageLicense))
      //test if message are received
      probe.expectMsg(500.millis, expectedMsg)
      system.stop(mock)
    }

    "perform the failed scenario by letting the CLibary mock throw an exception" in {
      val probe = TestProbe()
      val mock = system.actorOf(Props(new TestRecognizer()), "recognize")
      mock ! UpdateTestRecognizer(simulateErrors = true)
      val recognizeImageRef = system.actorOf(Props(new RecognizeImageCaller(
        delegatePath,
        recognizeCfg = cfg,
        laneConfigProvider = Right(laneConfig),
        candidateTypes = List(VehicleImageType.Overview, VehicleImageType.License),
        recipients = Set(probe.ref))))

      //create message
      val image = new RegistrationImage("TestFile.tiff", ImageFormat.TIF_BAYER, VehicleImageType.Overview, ImageVersion.Original, 1234)
      val msg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventId = Some("1"),
        eventTimestamp = None,
        event = new SensorEvent(triggerTime, new PeriodRange(triggerTime, triggerTime), "test"),
        priority = Priority.NONCRITICAL,
        files = List(image))

      recognizeImageRef ! msg
      val imageLicense = new RegistrationImage("snippet.tif", ImageFormat.TIF_RGB, VehicleImageType.License, ImageVersion.Original, 1234)
      val expectedMsg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventId = Some("1"),
        eventTimestamp = None,
        event = new SensorEvent(triggerTime, new PeriodRange(triggerTime, triggerTime), "test"),
        priority = Priority.NONCRITICAL,
        licenseData = RegistrationLicenseData(
          license = Some(new ValueWithConfidence[String]("12abcd", 90)),
          licensePosition = Some(LicensePosition(TLx = 100, TLy = 100, TRx = 400, TRy = 100, BLx = 100, BLy = 400, BRx = 400, BRy = 400)),
          licenseSourceImage = Some(VehicleImageType.Overview),
          country = Some(new ValueWithConfidence[String]("NL", 90))),
        files = List(image, imageLicense))
      //test if message are received
      probe.expectMsg(500.millis, msg)
      system.stop(mock)
    }

    "perform the failed scenario returning empty" in {
      val probe = TestProbe()
      val mock = system.actorOf(Props(new TestRecognizer()), "recognize")
      mock ! UpdateTestRecognizer(simulateEmpty = true)
      val recognizeImageRef = system.actorOf(Props(new RecognizeImageCaller(
        delegatePath,
        recognizeCfg = cfg,
        laneConfigProvider = Right(laneConfig),
        candidateTypes = List(VehicleImageType.Overview, VehicleImageType.License),
        recipients = Set(probe.ref))))
      //create message
      val image = new RegistrationImage("TestFile.tiff", ImageFormat.TIF_BAYER, VehicleImageType.Overview, ImageVersion.Original, 1234)
      val msg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventId = Some("1"),
        eventTimestamp = None,
        event = new SensorEvent(triggerTime, new PeriodRange(triggerTime, triggerTime), "test"),
        priority = Priority.NONCRITICAL,
        files = List(image))

      recognizeImageRef ! msg
      //test if message are received
      val expect = msg.copy(licenseData = RegistrationLicenseData(
        license = Some(new ValueWithConfidence[String]("", 0)),
        licenseSourceImage = Some(VehicleImageType.Overview),
        recognizedBy = Some(Recognizer.ICR_PLATEFINDER)))
      probe.expectMsg(500.millis, expect)
      system.stop(mock)
    }

    "perform the failed scenario by returning plate not found" in {
      val probe = TestProbe()
      val mock = system.actorOf(Props(new TestRecognizer()), "recognize")
      mock ! UpdateTestRecognizer(simulateEmpty = true, simulatePlateNotFound = true)

      val recognizeImageRef = system.actorOf(Props(new RecognizeImageCaller(
        delegatePath,
        recognizeCfg = cfg,
        laneConfigProvider = Right(laneConfig),
        candidateTypes = List(VehicleImageType.Overview, VehicleImageType.License),
        recipients = Set(probe.ref))))
      //create message
      val image = new RegistrationImage("TestFile.tiff", ImageFormat.TIF_BAYER, VehicleImageType.Overview, ImageVersion.Original, 1234)
      val msg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventId = Some("1"),
        eventTimestamp = None,
        event = new SensorEvent(triggerTime, new PeriodRange(triggerTime, triggerTime), "test"),
        priority = Priority.NONCRITICAL,
        licenseData = RegistrationLicenseData(licenseSourceImage = Some(VehicleImageType.Overview)),
        files = List(image))

      recognizeImageRef ! msg
      //test if message are received

      val expect = msg.copy(licenseData = RegistrationLicenseData(licenseSourceImage = Some(VehicleImageType.Overview), recognizedBy = Some(Recognizer.ICR_PLATEFINDER)))

      probe.expectMsg(500.millis, expect)
      system.stop(mock)
    }

    "perform the failed scenario when there is no image to recognize" in {
      /*
        * Testing a failed scenario where nog image can be found.
        * Configured so recognize can only use Overview, but only License provided.
        */
      val probe = TestProbe()
      val mock = system.actorOf(Props(new TestRecognizer()), "recognize")
      //mock ! UpdateTestRecognizer(simulateEmpty = true)

      val recognizeImageRef = system.actorOf(Props(new RecognizeImageCaller(
        delegatePath,
        recognizeCfg = cfg,
        laneConfigProvider = Right(laneConfig),
        candidateTypes = List(VehicleImageType.Overview),
        recipients = Set(probe.ref))))
      //create message
      val image = new RegistrationImage("TestFile.tiff", ImageFormat.TIF_BAYER, VehicleImageType.License, ImageVersion.Original, 1234)
      val msg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventId = Some("1"),
        eventTimestamp = None,
        event = new SensorEvent(triggerTime, new PeriodRange(triggerTime, triggerTime), "test"),
        priority = Priority.NONCRITICAL,
        files = List(image))

      recognizeImageRef ! msg
      //test if message are received
      probe.expectMsg(500.millis, msg)
      system.stop(mock)
    }
  }
}

case class UpdateTestRecognizer(simulateErrors: Boolean = false, simulateEmpty: Boolean = false, simulatePlateNotFound: Boolean = false)
/**
 *  The Test implementation of the Recognizer.
 */
class TestRecognizer extends Actor {
  var simulateErrors: Boolean = false
  var simulateEmpty: Boolean = false
  var simulatePlateNotFound: Boolean = false

  def receive = {
    case req: RecognizeImageRequest ⇒ {
      if (simulateErrors) {
        sender ! RecognizeImageResponse(req.msgId, req.imageFile, None, System.currentTimeMillis())
      } else {
        sender ! RecognizeImageResponse(req.msgId, req.imageFile,
          TranslatePlate.createLicensePlateResult(Some(recognizeImage())), System.currentTimeMillis())
      }
    }
    case UpdateTestRecognizer(err, empty, plate) ⇒ {
      simulateErrors = err
      simulateEmpty = empty
      simulatePlateNotFound = plate
    }
  }

  /**
   * Do the actual recognizion of the licenseplate  .
   */
  def recognizeImage(): LicensePlate = {
    if (simulateErrors) {
      throw new ImageFunctionsException(1, "Simulated error")
    }
    val plate = new LicensePlate
    if (simulateEmpty) {
      plate.setLicenseSnippetFileName("")
      plate.setPlateNumber("")
      plate.setRecognizer(LicensePlate.ICR_RECOGNIZER)
      plate.setPlateCountryCode(LicensePlate.PLATE_UNKNOWN)
      plate.setPlateConfidence(0)
      plate.setReturnStatus(0)
      plate.setPlateStatus(getPlateStatus)
      plate.setPlateCountryConfidence(0)
    } else {
      plate.setLicenseSnippetFileName("snippet.tif")
      plate.setPlateNumber("12abcd")
      plate.setRecognizer(LicensePlate.ICR_RECOGNIZER)
      plate.setPlateCountryCode(LicensePlate.PLATE_NETHERLANDS)
      plate.setPlateConfidence(90)
      plate.setReturnStatus(0)
      plate.setPlateStatus(getPlateStatus)
      plate.setPlateCountryConfidence(90)
      plate.setPlateTL(new Point(100, 100))
      plate.setPlateTR(new Point(400, 100))
      plate.setPlateBL(new Point(100, 400))
      plate.setPlateBR(new Point(400, 400))
    }
    plate
  }

  def getPlateStatus: Short = {
    if (simulatePlateNotFound) {
      LicensePlate.PLATESTATUS_NO_PLATE_FOUND
    } else {
      LicensePlate.PLATESTATUS_PLATE_FOUND
    }
  }
}

