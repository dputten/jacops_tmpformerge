package csc.vehicle.registration.images

import java.io.File

import com.drew.imaging.ImageProcessingException
import csc.vehicle.message.ExifTagInfo
import org.scalatest.WordSpec
import org.scalatest._
import testframe.Resources

class ExtractExifMetaDataTest extends WordSpec with MustMatchers {
  "TestReadExifMetaData" must {
    "read correct values" in {
      val input: File = Resources.getResourceFile("02020140225130112.534-76f28f76accc5e442f4820b004c928d0.jpg")

      // MetaData with dateTimeOriginal and subSecTimeOriginal
      val info = ReadWriteExifUtil.readExif(input)
      val time = ReadWriteExifUtil.dateTimeOriginalWithSubSecTimeOriginalUtc(info)
      time.get must be(1393333272534L)
    }
    "throw ImageProcessingException when parsing wrong file" in {
      val input: File = Resources.getResourceFile("output.xml")
      an[ImageProcessingException] must be thrownBy { ReadWriteExifUtil.readExif(input) }
    }
    "return 0 when empty jpg" in {
      val input: File = Resources.getResourceFile("output_license.jpg")

      val info = ReadWriteExifUtil.readExif(input)
      info.size must be(13)
      val removedJpegHeaders = info.filter(tag ⇒ tag.directory != ExifTagInfo.directoryJpeg &&
        tag.directory != ExifTagInfo.directoryJpegComment &&
        tag.directory != ExifTagInfo.directoryJfif)
      removedJpegHeaders.size must be(0)

    }
  }
}
