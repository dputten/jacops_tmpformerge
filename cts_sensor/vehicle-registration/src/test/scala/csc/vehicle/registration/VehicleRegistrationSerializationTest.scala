/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import csc.vehicle.message.{ ValueWithConfidence, VehicleImage, VehicleMetadata, _ }
import org.scalatest.WordSpec
import org.scalatest._

import scala.collection.mutable.ListBuffer

/**
 * Test the VehicleRegistrationSerialization
 */
class VehicleRegistrationSerializationTest extends WordSpec with MustMatchers {

  def createCompleteVehicleMetadata: VehicleMetadata = {

    val images: ListBuffer[VehicleImage] = ListBuffer()

    images += new VehicleImage(
      timestamp = 12340010,
      offset = 0,
      uri = "/path/to/overview.jpg",
      imageType = VehicleImageType.Overview,
      checksum = "overview sha",
      timeYellow = Some(123),
      timeRed = Some(789))

    images += new VehicleImage(
      timestamp = 12340010,
      offset = 0,
      uri = "/path/to/overviewMasked.jpg",
      imageType = VehicleImageType.OverviewMasked,
      checksum = "overviewMasked sha",
      timeYellow = Some(123),
      timeRed = Some(789))

    images += new VehicleImage(
      timestamp = 12340010,
      offset = 0,
      uri = "/path/to/overviewRedLight.jpg",
      imageType = VehicleImageType.OverviewRedLight,
      checksum = "overviewRedLight sha",
      timeYellow = Some(123),
      timeRed = Some(789))

    images += new VehicleImage(
      timestamp = 12340010,
      offset = 0,
      uri = "/path/to/overviewSpeed.jpg",
      imageType = VehicleImageType.OverviewSpeed,
      checksum = "overviewSpeed sha",
      timeYellow = Some(123),
      timeRed = Some(789))

    images += new VehicleImage(
      timestamp = 12340010,
      offset = 0,
      uri = "/path/to/redLight.jpg",
      imageType = VehicleImageType.RedLight,
      checksum = "redLight sha")

    images += new VehicleImage(
      timestamp = 12340010,
      offset = 0,
      uri = "/path/to/measureMethod2Speed.jpg",
      imageType = VehicleImageType.MeasureMethod2Speed,
      checksum = "measureMethod2Speed sha",
      timeRed = Some(789))

    val lane = new Lane(
      laneId = "the lane",
      name = "lane name",
      gantry = "gantry",
      system = "system",
      sensorGPS_longitude = 23.44,
      sensorGPS_latitude = 165.33)

    val lpd = LicensePlateData(
      license = Some(new ValueWithConfidence[String]("license")),
      rawLicense = Some("licence"),
      country = Some(new ValueWithConfidence[String]("country", 80)))

    val certData = CertificationData(
      Map("nmiCertificate" -> "nmiCertificate",
        "appChecksum" -> "appChecksum"),
      Map("serialNr" -> "SN1234"))

    new VehicleMetadata(
      recordVersion = Version.withCertificationData,
      lane = lane,
      eventId = "anId",
      eventTimestamp = 12340000,
      eventTimestampStr = Some("an event timestamp string"),
      length = Some(new ValueWithConfidence_Float(4.45F)),
      category = Some(new ValueWithConfidence[String]("category")),
      speed = Some(new ValueWithConfidence_Float(120.00F, 45)),
      images = images.toList,
      certificationData = Some(certData),
      licensePlateData = Some(lpd))
  }

  "VehicleRegistrationSerialization" must {
    "serialize and deserialize an VehicleMetadata object" in {
      val vehicleMetadata = createCompleteVehicleMetadata
      val json = VehicleRegistrationSerialization.write(vehicleMetadata)
      val vehicleMetadataClone = VehicleRegistrationSerialization.read(json)
      vehicleMetadataClone must be(vehicleMetadata)
    }
  }

}
