package csc.vehicle.registration

import java.io.File

import base.FavorCriticalProcess
import csc.akka.logging.DirectLogging
import csc.gantry.config.{ CameraConfig, MultiLaneCamera }
import csc.timeregistration.registration.{ ImageMetaDataReader, ImageInfo }
import csc.util.test.ObjectBuilder
import csc.vehicle.config.DualClockConfig
import csc.vehicle.message._
import org.apache.commons.io.FileUtils
import org.scalatest._
import org.scalatest.{ BeforeAndAfterEach, WordSpec }
import testframe.Resources
import unit.TestResource
import unit.base.FileTestUtils

/**
 * Created by carlos on 17.11.15.
 */
class MultiLaneCameraPreProcessorTest extends WordSpec with MustMatchers with BeforeAndAfterEach {

  import MultiLaneCameraPreProcessorTest._
  import RegistrationStatusFlag._

  "MultiLaneCameraPreProcessor" must {

    "create both files, for 2 configured lanes, both managed by camera, with L+R trigger source" in {

      val preProcessor = new MultiLaneCameraPreProcessor(List(bothLanesCamera)) //both cameras managed
      val state = createLaneSensorState(true, true) //both cameras configured
      val sourceFile = sourceImageFile(bothTriggers) //left source image
      val lastModified = sourceFile.lastModified()

      val results: List[RegistrationCandidate] = preProcessor.apply(IncomingFile(sourceFile.getAbsolutePath, System.currentTimeMillis()), state)
      results.size must be(2) //2 result files
      assertImageInfo(results(0), sourceFile, leftLaneFolder, lastModified, ImageTriggerData(TriggerArea.LEFT, false))
      assertImageInfo(results(1), sourceFile, rightLaneFolder, lastModified, ImageTriggerData(TriggerArea.RIGHT, false))
    }

    "create the correct file, for 2 configured lanes, both managed by camera, with one trigger source" in {

      val preProcessor = new MultiLaneCameraPreProcessor(List(bothLanesCamera)) //both cameras managed
      val state = createLaneSensorState(true, true) //both cameras configured
      val sourceFile = sourceImageFile(leftTrigger) //left source image
      val lastModified = sourceFile.lastModified()

      val results: List[RegistrationCandidate] = preProcessor.apply(IncomingFile(sourceFile.getAbsolutePath, System.currentTimeMillis()), state)

      sourceFile.exists() must be(false) //source file removed

      results.size must be(1) //1 result file
      assertImageInfo(results(0), sourceFile, leftLaneFolder, lastModified, ImageTriggerData(TriggerArea.LEFT, true))
    }

    "do nothing, for 2 configured lanes, none managed by camera, with some trigger source" in {

      val preProcessor = new MultiLaneCameraPreProcessor(List(noLanesCamera)) //no cameras managed
      val state = createLaneSensorState(true, true) //both cameras configured
      val sourceFile = sourceImageFile(leftTrigger) //left source image

      val resultFiles = preProcessor.apply(IncomingFile(sourceFile.getAbsolutePath, System.currentTimeMillis()), state)

      sourceFile.exists() must be(true) //source file kept
      resultFiles.size must be(0) //no result files
    }

    "do nothing, for no configured lanes, 2 managed by camera, with some trigger source" in {

      val preProcessor = new MultiLaneCameraPreProcessor(List(bothLanesCamera)) //both cameras managed
      val state = createLaneSensorState(false, false) //no cameras configured
      val sourceFile = sourceImageFile(leftTrigger) //left source image

      val resultFiles = preProcessor.apply(IncomingFile(sourceFile.getAbsolutePath, System.currentTimeMillis()), state)

      sourceFile.exists() must be(true) //source file kept
      resultFiles.size must be(0) //no result files
    }

    "do nothing, for 1 configured lane, managed by camera, with mismatching trigger source" in {

      val preProcessor = new MultiLaneCameraPreProcessor(List(rightLaneCamera)) //only right is managed
      val state = createLaneSensorState(false, true) //only right configured
      val sourceFile = sourceImageFile(leftTrigger) //left source image

      val resultFiles = preProcessor.apply(IncomingFile(sourceFile.getAbsolutePath, System.currentTimeMillis()), state)

      sourceFile.exists() must be(true) //source file kept
      resultFiles.size must be(0) //no result files
    }

    "do nothing, for 2 configured lanes, both managed by camera, with no trigger source" in {

      val preProcessor = new MultiLaneCameraPreProcessor(List(bothLanesCamera)) //both cameras managed
      val state = createLaneSensorState(true, true) //both cameras configured
      val sourceFile = sourceImageFile(noTrigger) //left source image

      val resultFiles = preProcessor.apply(IncomingFile(sourceFile.getAbsolutePath, System.currentTimeMillis()), state)

      sourceFile.exists() must be(true) //source file kept
      resultFiles.size must be(0) //no result files
    }

    "Return the correct RegistrationStatus for tolerable dual clock discrepancy" in {
      //testing the biggest possible time diff that doesn't cause flag. reception time is immediate
      val files = execute(dualClockConfig.timeDiffThreshold, 0, List(0L))
      files.size must be(1)
      files(0).initialStatus must be(RegistrationStatus.initial) //no flags
    }

    "Return the correct RegistrationStatus for relevant dual clock discrepancy" in {
      //testing the smallest possible time diff that will cause the flag. reception time is immediate
      val files = execute(dualClockConfig.timeDiffThreshold + 1, 0, List(0L))
      files.size must be(1)
      files(0).initialStatus must be(RegistrationStatus.initial.set(clockDifference))
    }

    "Return the correct RegistrationStatus for critical dual clock discrepancy" in {
      //testing the smallest possible time diff that will cause the critical flag. reception time is immediate
      val files = execute(dualClockConfig.criticalTimeDiffThreshold + 1, 0, List())
      files.size must be(1)
      files(0).initialStatus must be(RegistrationStatus.initial.set(clockDifference).set(criticalClockDifference)) //both flags
    }

    "Return the correct RegistrationStatus for incoming direction" in {
      val files = execute(true)
      files.size must be(1)
      files(0).initialStatus must be(RegistrationStatus.initial.set(incomingDirection))
    }

    "Return the correct RegistrationStatus for outgoing direction" in {
      val files = execute(false)
      files.size must be(1)
      files(0).initialStatus must be(RegistrationStatus.initial)
    }

    "Skip the registration of an image considered too old" in {
      val files = execute(0, dualClockConfig.maxPhotoAge + 1, List()) //just enough to be too old
      files must be(Nil)
    }
  }

  /**
   * Time-grained execution of MultiLaneCameraPreProcessor.apply, to test specific timing conditions.
   * There are 3 meaningful times computed in this execution:
   * -baseTime: a recent past moment, to be set as the lastModified on the file. This has the millis truncated (to simulate the FS precision flaw)
   * -exif time: baseTime - exifTimeDelta
   * -reception time: baseTime + receptionTimeDelta
   *
   * @param exifTimeDelta millis to subtract to the base time, being then set on the image EXIF
   * @param receptionTimeDelta millis to add to the base time, being then set as the reception time
   * @return
   */
  def execute(exifTimeDelta: Long, receptionTimeDelta: Long, previousBufferValues: List[Long]): List[RegistrationCandidate] = {

    val time = System.currentTimeMillis() - receptionTimeDelta - 30
    val exifTime = time - exifTimeDelta
    val mdReader = metadataReader(exifTime)
    val preProcessor = new MultiLaneCameraPreProcessor(
      cameras = List(bothLanesCamera),
      dualClockConfig = Some(dualClockConfig),
      metadataReader = Some(mdReader),
      metadataReaderGuard = Some(new FavorCriticalProcess(2)))
    val state = createLaneSensorState(true, true) //both cameras configured
    val sourceFile = sourceImageFile(leftTrigger) //left source image
    val incoming = IncomingFile(sourceFile.getAbsolutePath, time)

    previousBufferValues.foreach {
      preProcessor.avgBuffersMap.get("camera").get.tryAdd
    }

    preProcessor.apply(incoming, state)
  }

  def execute(incomingDirection: Boolean): List[RegistrationCandidate] = {
    val time = System.currentTimeMillis()
    val exifTime = time
    val mdReader = metadataReader(exifTime)
    val preProcessor = new MultiLaneCameraPreProcessor(
      cameras = List(bothLanesCamera),
      dualClockConfig = Some(dualClockConfig),
      metadataReader = Some(mdReader),
      metadataReaderGuard = Some(new FavorCriticalProcess(2)))
    val state = createLaneSensorState(true, true, incomingDirection) //both cameras configured and incoming direction
    val sourceFile = sourceImageFile(leftTrigger) //left source image
    val incoming = IncomingFile(sourceFile.getAbsolutePath, time)

    preProcessor.apply(incoming, state)
  }

  def executeTest(): List[RegistrationCandidate] = {
    val time = System.currentTimeMillis()
    val exifTime = time
    val mdReader = metadataReader(exifTime)
    val preProcessor = new MultiLaneCameraPreProcessor(
      cameras = List(bothLanesCamera),
      dualClockConfig = Some(dualClockConfig),
      metadataReader = Some(mdReader),
      metadataReaderGuard = Some(new FavorCriticalProcess(2)))
    val state = createLaneSensorState(true, true) //both cameras configured
    val sourceFile = sourceImageFile(leftTrigger) //left source image
    val incoming = IncomingFile(sourceFile.getAbsolutePath, time)

    preProcessor.apply(incoming, state)
  }

  override protected def beforeEach(): Unit = {
    FileUtils.deleteDirectory(baseFolder)
    camFolder.mkdirs()
    leftLaneFolder.mkdirs()
    rightLaneFolder.mkdirs()
  }

  def assertImageInfo(imageInfo: RegistrationCandidate,
                      sourceFile: File,
                      expectedFolder: File,
                      lastModified: Long,
                      expectedTriggerData: ImageTriggerData): Unit = imageInfo match {

    case RegistrationCandidate(file, _, _, trigger) ⇒ {
      file.exists() must be(true) //file should exist
      file.getParentFile.getAbsolutePath must be(expectedFolder.getAbsolutePath) //in the correct folder

      val (filename, extension) = FileTestUtils.filenameAndExtension(sourceFile.getName)
      val expectedFilename = filename + TriggerArea.filanemaSuffix(expectedTriggerData.area) + extension

      file.getName must be(expectedFilename) //with correct name
      file.lastModified() must be(lastModified) //and correct lastModified

      trigger match {
        case Some(`expectedTriggerData`) ⇒ //correct trigger data
        case other                       ⇒ fail("Incorrect trigger result: " + other)
      }
    }
  }

}

object MultiLaneCameraPreProcessorTest extends ObjectBuilder with DirectLogging {

  val leftTrigger = TestResource.getFile("unit/image/leftTrigger.jpg")
  val bothTriggers = TestResource.getFile("unit/image/bothTriggers.jpg")
  val noTrigger = TestResource.getFile("unit/image/noTrigger.jpg")

  val leftLane = "laneL"
  val rightLane = "laneR"
  val resourceDir = Resources.getResourceDirPath()
  var baseFolder = new File(resourceDir, "out/input")
  val camFolder = new File(baseFolder, "camera")
  val leftLaneFolder = new File(baseFolder, leftLane)
  val rightLaneFolder = new File(baseFolder, rightLane)

  val cameraConfig = create[CameraConfig].copy(relayURI = camFolder.getPath)
  val bothLanesCamera = create[MultiLaneCamera].copy(config = cameraConfig, leftLaneId = Some(leftLane), rightLaneId = Some(rightLane)) //all
  val rightLaneCamera = bothLanesCamera.copy(leftLaneId = None)
  val noLanesCamera = rightLaneCamera.copy(rightLaneId = None)

  val dualClockConfig = DualClockConfig()

  def sourceImageFile(file: File): File = {
    val target = new File(camFolder, file.getName)
    FileUtils.copyFile(file, target)
    //target.setLastModified(time)
    target
  }

  def createLaneSensorState(withLeft: Boolean, withRight: Boolean, incomingDirection: Boolean = false): LaneSensorState = {
    val result = new MockLaneSensorState
    if (withLeft) result.addLane(leftLane, leftLaneFolder.getAbsolutePath, incomingDirection)
    if (withRight) result.addLane(rightLane, rightLaneFolder.getAbsolutePath, incomingDirection)
    result
  }

  def metadataReader(imageTime: Long): ImageMetaDataReader = new ImageMetaDataReader(log) {
    override def apply(path: String): Option[ImageInfo] =
      Some(ImageInfo(ImageFormat.JPG_RGB, Some(imageTime), None))

    override def getImageInfo(imageBytes: Array[Byte]): Option[ImageInfo] =
      Some(ImageInfo(ImageFormat.JPG_RGB, Some(imageTime), None))
  }

}

class MockLaneSensorState extends LaneSensorState {
  def addLane(laneId: String, path: String, incomingDirection: Boolean): Unit = {
    val config = RelaySensorConfig(
      Lane(laneId, laneId, "gantry1", "route", 0, 0, None, Some(incomingDirection)),
      null,
      CameraConfig(path, "localhost", 1234, 1, 0),
      true,
      Priority.CRITICAL,
      Nil)
    knownLanes += path -> config
  }
}
