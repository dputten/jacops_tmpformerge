/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import java.io.File

import csc.akka.logging.DirectLogging
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }
import testframe.Resources

/**
 * Test the IRoseDataCollector
 */
class IRoseDataCollectorTest extends WordSpec with MustMatchers with DirectLogging with BeforeAndAfterEach with BeforeAndAfterAll {
  import IRoseDataHelper._

  val resourceDir = Resources.getResourceDirPath().getAbsolutePath
  val inputDir = new File(resourceDir, "out/irose-input")
  if (!inputDir.exists()) {
    inputDir.mkdirs()
  } else {
    clearDirectory(inputDir)
  }

  override def afterAll() {
    inputDir.delete()
  }

  override def afterEach() {
    clearDirectory(inputDir)
  }

  def completeJsonWith3Images(withChecksums: Boolean = false) {
    createFile(inputDir, defaultRedLightJpg)
    createFile(inputDir, defaultOverviewJpg)
    createFile(inputDir, defaultMeasureMethod2Jpg)
    createCompleteAndCorrectJsonFile(inputDir, withChecksums)
  }

  "An IRoseDataCollector" when {
    "receiving a directory containing a complete and correct json file and the 3 corresponding images (without checksums)" must {

      "return Right(IRoseRegistration)" in {
        completeJsonWith3Images()

        IRoseDataCollector.collect(inputDir) match {
          case Right((jsonFile, registration)) ⇒
            jsonFile.getAbsolutePath must be(new File(inputDir, "iRose.json").getAbsolutePath)
            registration.laneId must be("test lane")
            registration.direction must be(IRoseDirection.Outgoing)
            registration.speed must be(123.6F)
            registration.length must be(4.5F)
            registration.loop1UpTime must be(12340000)
            registration.loop1DownTime must be(12340010)
            registration.loop2UpTime must be(12340020)
            registration.loop2DownTime must be(12340030)
            registration.images.length must be(3)
          case Left(error) ⇒
            fail("IRoseDataCollector.collect should have returned a Right")
        }
      }

      "produce a redLight IRoseImage as part of the IRoseRegistration" in {
        completeJsonWith3Images()

        IRoseDataCollector.collect(inputDir) match {
          case Right((jsonFile, registration)) ⇒
            val imageO = getIRoseImage(registration, IRoseImageType.RedLight)
            imageO.isDefined must be(true)
            val image = imageO.get
            image.url must be(inputDir.getAbsolutePath + "/" + defaultRedLightJpg)
            image.imageType must be(IRoseImageType.RedLight)
            image.time must be(12340000)
            image.timeYellow must be(1111)
            image.timeRed must be(11)
            image.sha must be(None)
          case Left(error) ⇒
            fail("IRoseDataCollector.collect should have returned a Right")
        }
      }

      "produce an overview IRoseImage as part of the IRoseRegistration" in {
        completeJsonWith3Images()

        IRoseDataCollector.collect(inputDir) match {
          case Right((jsonFile, registration)) ⇒
            val imageO = getIRoseImage(registration, IRoseImageType.Overview)
            imageO.isDefined must be(true)
            val image = imageO.get
            image.url must be(inputDir.getAbsolutePath + "/" + defaultOverviewJpg)
            image.imageType must be(IRoseImageType.Overview)
            image.time must be(12340011)
            image.timeYellow must be(2222)
            image.timeRed must be(22)
            image.sha must be(None)
          case Left(error) ⇒
            fail("IRoseDataCollector.collect should have returned a Right")
        }
      }

      "produce a measureMethod2 IRoseImage as part of the IRoseRegistration" in {
        completeJsonWith3Images()

        IRoseDataCollector.collect(inputDir) match {
          case Right((jsonFile, registration)) ⇒
            val imageO = getIRoseImage(registration, IRoseImageType.MeasureMethod2)
            imageO.isDefined must be(true)
            val image = imageO.get
            image.url must be(inputDir.getAbsolutePath + "/" + defaultMeasureMethod2Jpg)
            image.imageType must be(IRoseImageType.MeasureMethod2)
            image.time must be(12340022)
            image.timeYellow must be(3333)
            image.timeRed must be(33)
            image.sha must be(None)
          case Left(error) ⇒
            fail("IRoseDataCollector.collect should have returned a Right")
        }
      }
    }
    "receiving a directory containing a complete and correct json file and the 3 corresponding images (with checksums)" must {

      "return Right(IRoseRegistration)" in {
        completeJsonWith3Images(true)

        IRoseDataCollector.collect(inputDir) match {
          case Right((jsonFile, registration)) ⇒
            jsonFile.getAbsolutePath must be(new File(inputDir, "iRose.json").getAbsolutePath)
            registration.laneId must be("test lane")
            registration.direction must be(IRoseDirection.Outgoing)
            registration.speed must be(123.6F)
            registration.length must be(4.5F)
            registration.loop1UpTime must be(12340000)
            registration.loop1DownTime must be(12340010)
            registration.loop2UpTime must be(12340020)
            registration.loop2DownTime must be(12340030)
            registration.images.length must be(3)
          case Left(error) ⇒
            fail("IRoseDataCollector.collect should have returned a Right")
        }
      }

      "produce a redLight IRoseImage as part of the IRoseRegistration" in {
        completeJsonWith3Images(true)

        IRoseDataCollector.collect(inputDir) match {
          case Right((jsonFile, registration)) ⇒
            val imageO = getIRoseImage(registration, IRoseImageType.RedLight)
            imageO.isDefined must be(true)
            val image = imageO.get
            image.url must be(inputDir.getAbsolutePath + "/" + defaultRedLightJpg)
            image.imageType must be(IRoseImageType.RedLight)
            image.time must be(12340000)
            image.timeYellow must be(1111)
            image.timeRed must be(11)
            image.sha must be(Some("d41d8cd98f00b204e9800998ecf8427e"))
          case Left(error) ⇒
            fail("IRoseDataCollector.collect should have returned a Right")
        }
      }

      "produce an overview IRoseImage as part of the IRoseRegistration" in {
        completeJsonWith3Images(true)

        IRoseDataCollector.collect(inputDir) match {
          case Right((jsonFile, registration)) ⇒
            val imageO = getIRoseImage(registration, IRoseImageType.Overview)
            imageO.isDefined must be(true)
            val image = imageO.get
            image.url must be(inputDir.getAbsolutePath + "/" + defaultOverviewJpg)
            image.imageType must be(IRoseImageType.Overview)
            image.time must be(12340011)
            image.timeYellow must be(2222)
            image.timeRed must be(22)
            image.sha must be(Some("d41d8cd98f00b204e9800998ecf8427e"))
          case Left(error) ⇒
            fail("IRoseDataCollector.collect should have returned a Right")
        }
      }

      "produce a measureMethod2 IRoseImage as part of the IRoseRegistration" in {
        completeJsonWith3Images(true)

        IRoseDataCollector.collect(inputDir) match {
          case Right((jsonFile, registration)) ⇒
            val imageO = getIRoseImage(registration, IRoseImageType.MeasureMethod2)
            imageO.isDefined must be(true)
            val image = imageO.get
            image.url must be(inputDir.getAbsolutePath + "/" + defaultMeasureMethod2Jpg)
            image.imageType must be(IRoseImageType.MeasureMethod2)
            image.time must be(12340022)
            image.timeYellow must be(3333)
            image.timeRed must be(33)
            image.sha must be(Some("d41d8cd98f00b204e9800998ecf8427e"))
          case Left(error) ⇒
            fail("IRoseDataCollector.collect should have returned a Right")
        }
      }
    }

    "receiving a directory containing a json file with wrong checksum" must {
      "return Left(ErrorMessage)" in {
        createFile(inputDir, defaultRedLightJpg)
        createFile(inputDir, defaultOverviewJpg)
        createFile(inputDir, defaultMeasureMethod2Jpg)
        createJsonFileWithWrongChecksum(inputDir)
        val jsonFileWithWrongChecksum = new File(inputDir, defaultRedLightJpg)

        IRoseDataCollector.collect(inputDir) match {
          case Right((jsonFile, registration)) ⇒
            fail("IRoseDataCollector.collect should have returned a Left")
          case Left(error) ⇒
            error must be("IRoseDataCollector: Retrieved IRoseRegistration not correct: Wrong checksum for image file [%s]".format(jsonFileWithWrongChecksum.getAbsolutePath))
        }
      }
    }

    "receiving a directory not containing a json file" must {
      "return Left(ErrorMessage)" in {
        createFile(inputDir, defaultRedLightJpg)
        createFile(inputDir, defaultOverviewJpg)
        createFile(inputDir, defaultMeasureMethod2Jpg)

        IRoseDataCollector.collect(inputDir) match {
          case Right((jsonFile, registration)) ⇒
            fail("IRoseDataCollector.collect should have returned a Left")
          case Left(error) ⇒
            error must be("IRoseDataCollector: Missing json file in directory [%s]".format(inputDir.getAbsolutePath))
        }
      }
    }

    "receiving a directory containing 2 json files" must {
      "return Left(ErrorMessage)" in {
        createFile(inputDir, "first.json")
        createFile(inputDir, "second.json")

        IRoseDataCollector.collect(inputDir) match {
          case Right((jsonFile, registration)) ⇒
            fail("IRoseDataCollector.collect should have returned a Left")
          case Left(error) ⇒
            error must be("IRoseDataCollector: More than one json file in directory [%s]".format(inputDir.getAbsolutePath))
        }
      }
    }

    "receiving a directory containing a incorrect json file" must {
      "return Left(ErrorMessage)" in {
        createIncorrectJsonFile(inputDir)
        val incorrectJsonFile = new File(inputDir, "incorrect.json")

        IRoseDataCollector.collect(inputDir) match {
          case Right((jsonFile, registration)) ⇒
            fail("IRoseDataCollector.collect should have returned a Left")
          case Left(error) ⇒
            error must be("IRoseDataCollector: Could not deserialize IRoseRegistration from file [%s]".format(incorrectJsonFile.getAbsolutePath))
        }
      }
    }

    "receiving a directory containing a complete and correct json file and missing one of the corresponding images" must {
      "return Left(ErrorMessage)" in {
        createCompleteAndCorrectJsonFile(inputDir)
        createFile(inputDir, defaultRedLightJpg)
        createFile(inputDir, defaultOverviewJpg)
        // No MeasureMethod2 image!

        IRoseDataCollector.collect(inputDir) match {
          case Right((jsonFile, registration)) ⇒
            fail("IRoseDataCollector.collect should have returned a Left")
          case Left(error) ⇒
            error must be("IRoseDataCollector: Retrieved IRoseRegistration not correct: Missing image file [%s/%s]".format(inputDir.getAbsolutePath, defaultMeasureMethod2Jpg))
        }
      }
    }

    "receiving a directory containing a complete and correct json file, referencing an nonexisting image" must {
      "return Left(ErrorMessage)" in {
        createCompleteAndCorrectJsonFile(inputDir)
        createFile(inputDir, defaultRedLightJpg)
        createFile(inputDir, "oops" + defaultOverviewJpg)
        createFile(inputDir, defaultMeasureMethod2Jpg)

        IRoseDataCollector.collect(inputDir) match {
          case Right((jsonFile, registration)) ⇒
            fail("IRoseDataCollector.collect should have returned a Left")
          case Left(error) ⇒
            error must be("IRoseDataCollector: Retrieved IRoseRegistration not correct: Missing image file [%s/%s]".format(inputDir.getAbsolutePath, defaultOverviewJpg))
        }
      }
    }
  }
}
