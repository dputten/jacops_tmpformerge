/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import java.io.{ BufferedReader, InputStreamReader, PrintWriter }
import java.net.Socket
import java.util.Date

import akka.actor.{ ActorRef, ActorSystem, Props }
import akka.camel.CamelExtension
import akka.testkit.{ TestKit, TestProbe }
import akka.util.Timeout
import csc.akka.CamelClient
import scala.concurrent.Await
import scala.concurrent.duration._
import csc.akka.logging.DirectLogging
import csc.base.{ SensorDevice, SensorPosition }
import csc.gantry.config.PIRConfig
import csc.vehicle.message.{ Lane, VehicleCategory, VehicleRegistrationMessage }
import csc.vehicle.pir.VehiclePIRRegistration
import csc.vehicle.registration.events.EventBuilder
import net.liftweb.json.{ DefaultFormats, Serialization }
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import unit.CertificateTestUtil

/**
 * Test the ReceivePir
 */
class ReceivePirTest()
  extends TestKit(ActorSystem("ReceivePirTest"))
  with WordSpecLike
  with DirectLogging
  with MustMatchers
  with CamelClient
  with BeforeAndAfterAll {

  //TODO PCL-45: verify CertificationData contents

  override def afterAll() {
    try {
      system.shutdown
    } catch {
      case e: Exception ⇒ log.error("COULD NOT SHUTDOWN ACTOR REGISTRY".format(e))
    }
  }
  "When receiving a message" must {
    "result in a VehicleRegistrationMessage" in {
      val config = new PIRConfig(
        pirHost = "PirHost",
        pirPort = 1000,
        pirAddress = 1,
        refreshPeriod = 100,
        vrHost = "localhost",
        vrPort = 9111)
      val lane = new Lane("100", "lane1", "gantry1", "route", 0, 0)
      val probe = TestProbe()
      val receivePirActorRef = system.actorOf(Props(new ReceivePir(lane = lane, pirCfg = config,
        NMICertificate = CertificateTestUtil.createCertificate("cert XXXX"),
        eventBuilder = EventBuilder.getInstance(SensorPosition.Trigger, SensorDevice.PIR),
        setEventTime = true,
        nextActors = Set(probe.ref))))

      awaitActivation(receivePirActorRef, 10 seconds)

      val pirMsg = new VehiclePIRRegistration(
        lane = lane,
        eventTime = new Date().getTime,
        speed = Some(120.4F),
        length = Some(4.4F),
        category = Some(1),
        confidence = Some(60))
      val pirStrMsg = Serialization.write(pirMsg)(DefaultFormats)
      val radar = new Socket(config.vrHost, config.vrPort);
      val out = new PrintWriter(radar.getOutputStream(), true);
      val in = new BufferedReader(new InputStreamReader(radar.getInputStream()));
      try {
        out.println(pirStrMsg)

        var vehicleMsg = probe.expectMsgType[VehicleRegistrationMessage](10 seconds)
        vehicleMsg.lane must be(lane)
        vehicleMsg.eventTimestamp.get must be(pirMsg.eventTime)
        vehicleMsg.event.triggerSource must be("PIR")
        vehicleMsg.vehicleData.speed.isDefined must be(true)
        vehicleMsg.vehicleData.speed.get.value must be(pirMsg.speed.get)
        vehicleMsg.vehicleData.speed.get.confidence must be(95)
        vehicleMsg.vehicleData.length.isDefined must be(true)
        vehicleMsg.vehicleData.length.get.value must be(pirMsg.length.get)
        vehicleMsg.vehicleData.length.get.confidence must be(95)
        vehicleMsg.vehicleData.category.isDefined must be(true)
        vehicleMsg.vehicleData.category.get.value must be(VehicleCategory.Pers)
        vehicleMsg.vehicleData.category.get.confidence must be(60)

      } finally {
        try {
          out.close()
        } finally {
          radar.close()
        }
      }

      system.stop(receivePirActorRef)
    }
  }

}