/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import org.scalatest.WordSpec
import org.scalatest._

import scala.collection.mutable.ListBuffer

/**
 * Test the IRoseRegistrationSerialization
 */
class IRoseRegistrationSerializationTest extends WordSpec with MustMatchers {

  def createCompleteIRoseRegistration: IRoseRegistration = {
    val images: ListBuffer[IRoseImage] = ListBuffer()
    images += new IRoseImage(url = "/test/redLight.jpg", imageType = IRoseImageType.RedLight, time = 12340000, offset = 0, timeYellow = 1111, timeRed = 11)
    images += new IRoseImage(url = "/test/overview.jpg", imageType = IRoseImageType.Overview, time = 12340011, offset = 0, timeYellow = 2222, timeRed = 22)
    images += new IRoseImage(url = "/test/measureMethod2.jpg", imageType = IRoseImageType.MeasureMethod2, time = 12340022, offset = 0, timeYellow = 2222, timeRed = 33)

    new IRoseRegistration(
      laneId = "test lane",
      direction = IRoseDirection.Incoming,
      speed = 123.6F,
      length = 4.5F,
      loop1UpTime = 12340000, loop1DownTime = 12340010,
      loop2UpTime = 12340020, loop2DownTime = 12340030,
      serialNr = "SN1234",
      images = images,
      jsonMessage = """{"isJson":true}""")
  }

  "IRoseRegistrationSerialization" must {
    "serialize and deserialize an IRoseRegistration object" in {
      val iRoseRegistration = createCompleteIRoseRegistration
      val json = IRoseRegistrationSerialization.serializeToJSON(iRoseRegistration)
      val iRoseRegistrationClone = IRoseRegistrationSerialization.deserializeFromJSON(json)
      iRoseRegistrationClone must be(iRoseRegistration)
    }
  }
}
