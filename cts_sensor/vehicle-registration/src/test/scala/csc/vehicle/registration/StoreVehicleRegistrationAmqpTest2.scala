/*
 * Copyright (C) 2015. CSC <http://www.csc.com>
 */

package csc.vehicle.registration

import java.util.Date

import akka.actor.{ ActorRef, ActorSystem }
import akka.testkit.{ ImplicitSender, TestActorRef, TestKit }
import akka.util.Timeout
import scala.concurrent.duration._
import csc.akka.logging.DirectLogging
import csc.amqp.AmqpSerializers
import csc.image.store.JsonSerializers
import csc.json.lift.EnumerationSerializer
import csc.vehicle.config.{ AmqpCompletionConfig, Configuration }
import csc.vehicle.message._
import org.scalatest._
import org.scalatest.{ BeforeAndAfter, BeforeAndAfterAll, WordSpec }

/**
 * Tests StoreVehicleRegistrationAmqp without amqp
 *
 * @author carlos
 */
class StoreVehicleRegistrationAmqpTest2 extends TestKit(ActorSystem("amqp"))
  with ImplicitSender
  with WordSpecLike
  with MustMatchers
  with BeforeAndAfterAll
  with BeforeAndAfter
  with AmqpSerializers
  with DirectLogging {

  import StoreVehicleRegistrationAmqpTest2._

  override val ApplicationId = "test"
  override implicit val formats = JsonSerializers.formats + new EnumerationSerializer(VehicleImageType)
  implicit val timeout = Timeout(1000 milliseconds)

  var amqp: ActorRef = _
  val senderSelf = self

  override protected def beforeAll(): Unit = {
    amqp = TestActorRef(new StoreVehicleRegistrationNoAmqp(senderSelf))
  }

  override protected def afterAll(): Unit = {
    system.shutdown()
  }

  "StoreVehicleRegistrationAmqp" ignore {
    "make sure sent keepalive time is never lower than the previous one" in {

      val count = 20
      amqp ! StoreVehicleRegistrationAmqp.KeepaliveTick

      for (i ← 1 to count) {
        sleep
        amqp ! createRegistrationMessage(System.currentTimeMillis() - delay)
        sleep
        amqp ! StoreVehicleRegistrationAmqp.KeepaliveTick
      }

      val msg = expectMsgType[VehicleRegistrationKeepalive]
      var current = msg.processedTime
      //println(current)

      for (i ← 1 to count) {
        expectMsgType[VehicleMetadata]
        val keepalive = expectMsgType[VehicleRegistrationKeepalive]
        (keepalive.processedTime >= current) must be(true)
        current = keepalive.processedTime
        //println(keepalive)
      }
    }
  }

  //this combination of delays creates diverse situations where the processed time is either after of before the previous
  def sleep = Thread.sleep(150) //small delay between sending messages
  def delay: Long = (scala.math.random * 10000).toLong //introduces a random delay of 0 to 10 seconds in each VehicleRegistrationMessage

}

object StoreVehicleRegistrationAmqpTest2 {

  val lane = Lane("A2-1-lane1", "lane1", gantry = "1", system = "A2", 52, 4.2, None, None)

  lazy val configFile = getClass.getClassLoader.getResource("csc/vehicle/config/testAmqp.conf").getPath
  lazy val config = Configuration.createFromFile(configFile)
  lazy val cfg = config.globalConfig

  def amqpConfig: AmqpCompletionConfig = cfg.registrationCompletion(0).asInstanceOf[AmqpCompletionConfig]

  def createRegistrationMessage(time: Long): VehicleRegistrationMessage = {

    new VehicleRegistrationMessage(
      lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
      eventId = Some("100"),
      eventTimestamp = Some(time),
      event = new SensorEvent(time, new PeriodRange(time, time), "test"),
      priority = Priority.NONCRITICAL,
      files = Nil)
  }

}

import csc.vehicle.registration.StoreVehicleRegistrationAmqpTest2._

class StoreVehicleRegistrationNoAmqp(receiver: ActorRef)
  extends StoreVehicleRegistrationAmqp(null, amqpConfig, Some(lane), Set.empty) {

  override def preStart(): Unit = {
    //override to skip all amqp connection setup and automated ticks
  }

  override def sendToRegistrationReceiver[T <: AnyRef: Manifest](msg: T): Unit = {
    receiver ! msg
  }

}
