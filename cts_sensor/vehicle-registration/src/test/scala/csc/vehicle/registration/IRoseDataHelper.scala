/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import java.io.{ File, FileInputStream, FileOutputStream, PrintWriter }
import java.net.URLDecoder

import csc.akka.logging.DirectLogging
import csc.common.Signing

import scala.collection.mutable.ListBuffer

/**
 * Helper object for IRoseData tests
 */
object IRoseDataHelper extends DirectLogging {
  private val emptyString = ""
  val defaultIRoseJson = "iRose.json"
  val defaultRedLightJpg = "redLight.jpg"
  val defaultOverviewJpg = "overview.jpg"
  val defaultMeasureMethod2Jpg = "measureMethod2.jpg"

  def getIRoseImage(registration: IRoseRegistration, imageType: IRoseImageType.Value): Option[IRoseImage] = {
    registration.images.find(_.imageType == imageType)
  }

  def clearDirectory(dir: File) {
    dir.listFiles.foreach { f ⇒ deleteFile(f) }
  }

  def deleteDirectory(dir: File) = {
    deleteFile(dir)
  }

  def deleteFile(file: File): Unit = {
    if (file.isDirectory)
      file.listFiles.foreach { f ⇒ deleteFile(f) }
    file.delete
  }

  def createCompleteAndCorrectJsonFile(
    dir: File,
    withChecksums: Boolean = false,
    iRoseJson: String = defaultIRoseJson,
    redLightJpg: String = defaultRedLightJpg,
    overviewJpg: String = defaultOverviewJpg,
    measureMethod2Jpg: String = defaultMeasureMethod2Jpg): Unit = {
    val registration = createIRoseRegistration(
      redLightUrl = completePath(dir, redLightJpg),
      overviewUrl = completePath(dir, overviewJpg),
      measureMethod2Url = completePath(dir, measureMethod2Jpg),
      withChecksums = withChecksums,
      correctChecksums = true)

    writeAsJson(dir, iRoseJson, registration)
  }

  def createCompleteAndCorrectJsonFileWithRealJpg(
    dir: File,
    withChecksums: Boolean = false,
    iRoseJson: String = defaultIRoseJson,
    redLightJpg: String = "02020140225130112.534-76f28f76accc5e442f4820b004c928d0.jpg",
    overviewJpg: String = "02020140225130112.534-76f28f76accc5e442f4820b004c928d0.jpg",
    measureMethod2Jpg: String = "02020140225130112.534-76f28f76accc5e442f4820b004c928d0.jpg"): Unit = {
    val registration = createIRoseRegistration(
      redLightUrl = getClass.getClassLoader.getResource("RedLight.jpg").getPath,
      overviewUrl = getClass.getClassLoader.getResource("Overview.jpg").getPath,
      measureMethod2Url = getClass.getClassLoader.getResource("MeasureMethod2.jpg").getPath,
      //      redLightUrl = getClass.getClassLoader.getResource("02020140225130112.534-76f28f76accc5e442f4820b004c928d0.jpg").getPath,
      //      overviewUrl = getClass.getClassLoader.getResource("02020140225130112.534-76f28f76accc5e442f4820b004c928d0.jpg").getPath,
      //      measureMethod2Url = getClass.getClassLoader.getResource("02020140225130112.534-76f28f76accc5e442f4820b004c928d0.jpg").getPath,
      withChecksums = withChecksums,
      correctChecksums = true)

    writeAsJson(dir, iRoseJson, registration)
  }

  def createIncorrectJsonFile(dir: File) {
    val jsonFile = createFile(dir, "incorrect.json")
    val writer = new PrintWriter(jsonFile)
    writer.write("wrong:{@*&43")
    writer.close()
  }

  def createJsonFileWithWrongChecksum(dir: File) {
    val registration = createIRoseRegistration(
      redLightUrl = completePath(dir, defaultRedLightJpg),
      overviewUrl = completePath(dir, defaultOverviewJpg),
      measureMethod2Url = completePath(dir, defaultMeasureMethod2Jpg),
      withChecksums = true,
      correctChecksums = false)

    writeAsJson(dir, defaultIRoseJson, registration)
  }

  def writeAsJson(dir: File, iRoseJson: String, registration: IRoseRegistration) {
    val jsonFile = createFile(dir, iRoseJson)
    val writer = new PrintWriter(jsonFile)
    writer.write(IRoseRegistrationSerialization.serializeToJSON(registration))
    writer.close()
  }

  def createFile(dir: File, filename: String): File = {
    val newFile = new File(dir, filename)
    if (newFile.exists) {
      log.info("New file already exists " + newFile.getAbsolutePath)
      val isDeleted = newFile.delete
      if (!isDeleted) {
        log.info("Failed to delete existing file " + newFile.getAbsolutePath)
      }
    } else {
      log.info("New file does not exist yet " + newFile.getAbsolutePath)
      val isCreated = newFile.createNewFile
      if (!isCreated) {
        log.info("New file can not be created " + newFile.getAbsolutePath)
      } else {
        log.info("New file is created " + newFile.getAbsolutePath)
      }
    }
    newFile
  }

  def copyFile(fromDir: File, fromFilename: String, toDir: File, toFilename: String): File = {
    val source = new File(fromDir, fromFilename)
    val destination = new File(toDir, toFilename)
    new FileOutputStream(destination).getChannel.transferFrom(new FileInputStream(source).getChannel(), 0, Long.MaxValue)
    destination
  }

  def createIRoseRegistration(redLightUrl: String = emptyString,
                              overviewUrl: String = emptyString,
                              measureMethod2Url: String = emptyString,
                              withChecksums: Boolean = false,
                              correctChecksums: Boolean): IRoseRegistration = {
    def calcSha(url: String): Option[String] = {
      if (withChecksums) {
        val decodedPath = URLDecoder.decode(url, "UTF-8")
        val sha = Signing.digest(decodedPath, "MD5")
        correctChecksums match {
          case true ⇒ Some(sha)
          case _    ⇒ Some("inva%slid".format(sha))
        }
      } else {
        None
      }
    }

    val images: ListBuffer[IRoseImage] = ListBuffer()
    if (redLightUrl != emptyString) {
      images += new IRoseImage(url = redLightUrl, imageType = IRoseImageType.RedLight, time = 12340000, offset = 0, timeYellow = 1111, timeRed = 11, sha = calcSha(redLightUrl))
    }
    if (overviewUrl != emptyString) {
      images += new IRoseImage(url = overviewUrl, imageType = IRoseImageType.Overview, time = 12340011, offset = 0, timeYellow = 2222, timeRed = 22, sha = calcSha(overviewUrl))
    }
    if (measureMethod2Url != emptyString) {
      images += new IRoseImage(url = measureMethod2Url, imageType = IRoseImageType.MeasureMethod2, time = 12340022, offset = 0, timeYellow = 3333, timeRed = 33, sha = calcSha(measureMethod2Url))
    }

    new IRoseRegistration(laneId = "test lane",
      direction = IRoseDirection.Outgoing,
      speed = 123.6F,
      length = 4.5F,
      loop1UpTime = 12340000, loop1DownTime = 12340010,
      loop2UpTime = 12340020, loop2DownTime = 12340030,
      serialNr = "SN1234",
      images = images,
      jsonMessage = getCompleteMessageJson)
  }

  def getCompleteMessageJson =
    """{"laneId":"1","direction":"Outgoing","speed":34.0,"length":1.399999976158142,"loop1UpTime":0,"loop1DownTime":308,"loop2UpTime":266,"loop2DownTime":573,"serialNr":"DummyValue",
      "images":[{"url":"/data/images/raw/3215-RD/20130901144218175/RedLight.jpg","imageType":"RedLight","time":1378046537840,"offset":0,"timeYellow":0,"timeRed":0,
      "sha":"c47f38317e24651f2c32cefdc13ba80d"},{"url":"/data/images/raw/3215-RD/20130901144218175/Overview.jpg","imageType":"Overview","time":1378046538175,"offset":0,
      "timeYellow":0,"timeRed":0,"sha":"5d5ad481f72549c765184395f3aa6198"},{"url":"/data/images/raw/3215-RD/20130901144218175/MeasureMethod2.jpg","imageType":"MeasureMethod2",
      "time":1378046538595,"offset":0,"timeYellow":0,"timeRed":0,"sha":"4876467e7aefb4295df02ea667786d38"}]}"""

  private def completePath(dir: File, filename: String) = {
    new File(dir, filename).getAbsolutePath
  }

}
