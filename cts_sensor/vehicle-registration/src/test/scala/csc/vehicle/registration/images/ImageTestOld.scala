/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration.images

import java.io.File

import csc.akka.logging.DirectLogging
import org.junit.Test
import org.scalatest.junit.JUnitSuite

class ImageTestOld extends JUnitSuite with DirectLogging {
  //@Test
  def testImageDegradation() {
    val fileName = getClass.getClassLoader.getResource("output.jpg").getPath
    var inputFile = new File(fileName)

    for (i ← 0 until 100) {
      val outputFile = new File(inputFile.getParent, "modified%d.jpg".format(i))
      val image = new Image(inputFile.getAbsolutePath)
      image.writeImage(outputFile.getAbsolutePath, 25)
      //ImageIO.write(image, "jpg", outputFile);
      inputFile = outputFile
      inputFile.deleteOnExit()
    }
    val outputFile = new File(inputFile.getParent, "modified_degr.jpg")
    val image = new Image(inputFile.getAbsolutePath)
    image.writeImage(outputFile.getAbsolutePath, 25)
  }

  @Test
  def testAdd2Lines() {
    val fileName = getClass.getClassLoader.getResource("output.jpg").getPath
    val inputFile = new File(fileName)
    var outputFile = new File(inputFile.getParent, "col2.jpg")
    val image = new Image(inputFile.getAbsolutePath)

    var footer = image.createImageWithFooter(2, List("col1", "col2", "col3", "col4"))
    footer.writeImage(outputFile.getAbsolutePath, 75)
    outputFile = new File(inputFile.getParent, "col4.jpg")
    footer = image.createImageWithFooter(4, List("col1", "col2", "col3", "col4"))
    footer.writeImage(outputFile.getAbsolutePath, 75)
    outputFile = new File(inputFile.getParent, "col4_long.jpg")
    footer = image.createImageWithFooter(4, List("col1 1234567890 1234567890 1234567890 1234567890", "col2", "col3", "col4"))
    footer.writeImage(outputFile.getAbsolutePath, 75)

    outputFile = new File(inputFile.getParent, "col4_1.jpg")
    footer = image.createImageWithFooter(4, List("col1"))
    footer.writeImage(outputFile.getAbsolutePath, 75)

  }
}