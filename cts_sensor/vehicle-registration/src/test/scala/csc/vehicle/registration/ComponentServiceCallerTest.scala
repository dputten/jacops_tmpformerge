package csc.vehicle.registration

import java.util.Date

import akka.actor.{ ActorRef, ActorSystem, Props }
import akka.testkit.{ TestKit, TestProbe }
import csc.akka.logging.DirectLogging
import csc.amqp.adapter.CorrelatedMessage
import csc.util.test.{ ObjectBuilder, UniqueGenerator }
import csc.util.test.actor.ActorBehaviour
import csc.util.test.actor.ActorBehaviour.ActorBehaviour
import csc.vehicle.message.{ ValueWithConfidence, ValueWithConfidence_Float, VehicleCategory, VehicleRegistrationMessage }
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, MustMatchers, WordSpecLike }

import scala.concurrent.duration.Duration

/**
 * Created by carlos on 28/11/16.
 */
class ComponentServiceCallerTest extends TestKit(ActorSystem("ComponentServiceCallerTest"))
  with WordSpecLike
  with MustMatchers
  with DirectLogging
  with BeforeAndAfterEach
  with BeforeAndAfterAll {

  import ComponentServiceCallerTest._

  var serviceProbe: TestProbe = null
  var probe: TestProbe = null

  "ComponentServiceCaller" must {

    "handle the request/response happy case" in {

      val actor = system.actorOf(Props(new TestComponentServiceCaller(serviceProbe.ref, recipients)))
      val registration = createRegistration
      registration.vehicleData.category must be(None) //just to establish the baseline

      actor ! registration

      val result = probe.expectMsgType[VehicleRegistrationMessage]
      result.vehicleData.category must be(category)
    }
  }

  def recipients: Set[ActorRef] = Set(probe.ref)

  override protected def beforeEach(): Unit = {
    super.beforeEach()
    serviceProbe = TestProbe()
    serviceProbe.setAutoPilot(ActorBehaviour.toAutoPilot(actorBehaviour))
    probe = TestProbe()
  }

  override protected def afterAll(): Unit = {
    system.shutdown()
    super.afterAll()
  }

}

case class SvcRequest(msgId: String, time: Long) extends CorrelatedMessage
case class SvcResponse(msgId: String, time: Long,
                       category: Option[ValueWithConfidence[VehicleCategory.Value]]) extends CorrelatedMessage

class TestComponentServiceCaller(serviceActor: ActorRef, val recipients: Set[ActorRef])
  extends ComponentServiceCaller {

  override type Request = SvcRequest
  override type Response = SvcResponse

  override def timeout: Duration = Duration("5 seconds")

  override def ownReceive: Receive = {
    case response: Response ⇒ handleResponse(response)
  }

  override def sendToTarget(request: SvcRequest): Unit = serviceActor ! request

  override def requestFor(registration: VehicleRegistrationMessage): Either[String, SvcRequest] = {
    Right(SvcRequest(registration.id, System.currentTimeMillis()))
  }

  override def enrichRegistration(registration: VehicleRegistrationMessage, response: SvcResponse): VehicleRegistrationMessage = {
    val vd = registration.vehicleData
    registration.copy(vehicleData = vd.copy(category = response.category))
  }

}

object ComponentServiceCallerTest extends ObjectBuilder with UniqueGenerator {

  val category = Some(ValueWithConfidence(VehicleCategory.Large_Truck))

  val actorBehaviour: ActorBehaviour = {
    case req: SvcRequest ⇒ Some(SvcResponse(req.msgId, System.currentTimeMillis(), category))
  }

  def createRegistration: VehicleRegistrationMessage =
    create[VehicleRegistrationMessage].copy(eventId = Some(nextUnique), eventTimestamp = Some(System.currentTimeMillis()))

}