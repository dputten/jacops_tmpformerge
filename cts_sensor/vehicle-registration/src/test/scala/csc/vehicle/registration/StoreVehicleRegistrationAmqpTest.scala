/*
 * Copyright (C) 2015. CSC <http://www.csc.com>
 */

package csc.vehicle.registration

import java.io.File
import java.util.Date

import akka.actor.{ ActorRef, ActorSystem }
import scala.concurrent.Await
import akka.pattern.{ ask, gracefulStop }
import akka.testkit.{ ImplicitSender, TestActorRef, TestKit }
import akka.util.Timeout
import scala.concurrent.duration._
import com.github.sstone.amqp.Amqp._
import com.github.sstone.amqp.Amqp
import csc.akka.logging.DirectLogging
import csc.amqp.{ MQConnection, AmqpSerializers }
import csc.image.store.JsonSerializers
import csc.image.store.protocol.ImageContentPutRequest
import csc.json.lift.EnumerationSerializer
import csc.vehicle.config.{ AmqpCompletionConfig, Configuration }
import csc.vehicle.message._
import org.apache.commons.codec.digest.DigestUtils
import org.apache.commons.io.FileUtils
import org.scalatest._

/**
 * @author csomogyi
 */
class StoreVehicleRegistrationAmqpTest extends TestKit(ActorSystem("amqp")) with ImplicitSender
  with WordSpecLike with MustMatchers with BeforeAndAfterAll with BeforeAndAfter with AmqpSerializers
  with DirectLogging with Assertions {

  override val ApplicationId = "test"
  override implicit val formats = JsonSerializers.formats + new EnumerationSerializer(VehicleImageType)
  implicit val timeout = Timeout(1900 milliseconds)

  val testEventId = "100"
  val lane = Lane("A2-1-lane1", "lane1", gantry = "1", system = "A2", 52, 4.2, None, None)

  var amqpConfig: AmqpCompletionConfig = _
  var amqpConnection: MQConnection = _
  var adminChannel: ActorRef = _
  var amqp: ActorRef = _

  override protected def beforeAll(): Unit = {
    val configFile = getClass.getClassLoader.getResource("csc/vehicle/config/testAmqp.conf").getPath
    val config = Configuration.createFromFile(configFile)
    val cfg = config.globalConfig

    if (cfg.registrationCompletion.isEmpty) fail("Missing configuration")

    cfg.registrationCompletion.foreach(compConfig ⇒ {
      compConfig match {
        case _amqpConfig: AmqpCompletionConfig ⇒ {
          amqpConfig = _amqpConfig
          val amqpCfg = cfg.amqpConfig
          amqpConnection = MQConnection.create(amqpCfg.actorName, amqpCfg.amqpUri, amqpCfg.reconnectDelay)
          adminChannel = amqpConnection.producer
          recreateQueue(amqpConfig.imageStoreRouteKey)
          recreateQueue(amqpConfig.registrationReceiverRouteKey)
          amqp = TestActorRef(new StoreVehicleRegistrationAmqp(adminChannel, amqpConfig, Some(lane), Set()))
        }
        case _: Any ⇒ fail("Bad configuration")
      }
    })
  }

  override protected def afterAll(): Unit = {
    deleteQueue(amqpConfig.imageStoreRouteKey)
    deleteQueue(amqpConfig.registrationReceiverRouteKey)
    system.shutdown()
  }

  def deleteQueue(queue: String): Unit = {
    Await.result(adminChannel.ask(DeleteQueue(queue)), 2 seconds).asInstanceOf[Ok]
  }

  def createQueue(queue: String): Unit = {
    Await.result(adminChannel.ask(DeclareQueue(QueueParameters(queue, false, false, true, false))), 2 seconds).asInstanceOf[Ok]
  }

  def recreateQueue(queue: String): Unit = {
    deleteQueue(queue)
    createQueue(queue)
  }

  def cleanupQueue(queue: String): Unit = {
    Await.result(adminChannel.ask(PurgeQueue(queue)), 2 seconds).asInstanceOf[Ok]
  }

  def createConsumer(queue: String): ActorRef = {
    amqpConnection.createSimpleConsumer(queue, testActor, None, true)
  }

  def stopConsumer(consumer: ActorRef): Unit = {
    val stopped = gracefulStop(consumer, 5 seconds)
    Await.result(stopped, 6 seconds)
  }

  "StoreVehicleRegistrationAmqp" ignore {
    "send 1 metadata and 6 images" in {
      // clean up queues
      cleanupQueue(amqpConfig.imageStoreRouteKey)
      cleanupQueue(amqpConfig.registrationReceiverRouteKey)

      // create sample VehicleRegistrationMessage and send it to actor
      val fileName = getClass.getClassLoader.getResource("output_decorate.jpg").getPath

      //map of (ImageType,ImageVersion) -> RegistrationImage
      val imageMap = StoreImage.all.map(tp ⇒ tp -> new RegistrationImage(fileName, ImageFormat.JPG_RGB, tp._1, tp._2, 1234)).toMap
      val images = imageMap.values.toList

      val now = System.currentTimeMillis()

      val nmiCertValue = "cert XXXX"
      val appChecksumValue = "2451aaa5799a967ace9fced0d6edf7c132137ca198045759ae50b771a41f8fad"
      val certData = Some(CertificationData(Map("nmiCertificate" -> nmiCertValue, "appChecksum" -> appChecksumValue)))

      val msg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        certificationData = certData,
        eventId = Some(testEventId),
        eventTimestamp = Some(100L),
        event = new SensorEvent(now, new PeriodRange(now, now), "test"),
        priority = Priority.NONCRITICAL,
        files = images)

      amqp ! msg

      val rrConsumer = createConsumer(amqpConfig.registrationReceiverRouteKey)
      // wait for the VehicleMetadata message
      val delivery = receiveOne(5 seconds).asInstanceOf[Delivery]
      // for the sake of simplicity I assume the envelope is correct
      val response = VehicleRegistrationSerialization.read(new String(delivery.body, "UTF-8"))
      assert(response.lane === msg.lane)
      assert(response.certificationData == certData)
      assert(response.eventId === msg.eventId.get)
      assert(response.eventTimestamp === msg.eventTimestamp.get)

      assert(response.images.size == images.size) //no image missing

      response.images.foreach { vi ⇒
        val tp = (vi.imageType, vi.imageVersion.get)
        assertImage(Some(vi), imageMap.get(tp)) //image matches by type/version
      }

      stopConsumer(rrConsumer)

      val isConsumer = createConsumer(amqpConfig.imageStoreRouteKey)

      for (i ← Range(0, images.size)) {
        val imageDelivery = receiveOne(5 seconds).asInstanceOf[Delivery]
        val imageResponse = toMessage[ImageContentPutRequest](imageDelivery).right.get
        //order is not relevant: cannot compare image contents
      }

      stopConsumer(isConsumer)
    }
    "send keepalive" in {
      // clean up queues
      cleanupQueue(amqpConfig.imageStoreRouteKey)
      cleanupQueue(amqpConfig.registrationReceiverRouteKey)

      amqp ! StoreVehicleRegistrationAmqp.KeepaliveTick

      val rrConsumer = createConsumer(amqpConfig.registrationReceiverRouteKey)
      // wait for the VehicleMetadata message
      val delivery = receiveOne(5 seconds).asInstanceOf[Delivery]
      // for the sake of simplicity I assume the envelope is correct
      val response = VehicleRegistrationSerialization.readKeepalive(new String(delivery.body, "UTF-8"))
      assert(response.lane === lane)

      assert(response.processedTime / 60000 === System.currentTimeMillis() / 60000) // must have happened in the last minute

      stopConsumer(rrConsumer)
    }
  }

  def assertImage(veh: Option[VehicleImage], reg: Option[RegistrationImage]): Unit = {
    veh match {
      case None ⇒ {
        reg match {
          case None        ⇒
          case Some(right) ⇒ fail("Image " + right.imageType + " is missing in response")
        }
      }
      case Some(left) ⇒ {
        reg match {
          case None ⇒ fail("Image " + left.imageType + " was not originally sent")
          case Some(right) ⇒ {
            assert(left.uri === right.uri)
            assert(left.timestamp === right.timestamp)
            assert(left.offset === right.offset)
            assert(left.imageType === right.imageType)
            // checksum is not used
            assert(left.timeYellow === right.timeYellow)
            assert(left.timeRed === right.timeRed)
          }
        }
      }
    }
  }

  def assertImage(left: ImageContentPutRequest, right: RegistrationImage): Unit = {
    // we know all test images are JPG_RGB
    val imageId = testEventId + "@" + right.imageType + "@JPG_RGB"
    val imageBytes = FileUtils.readFileToByteArray(new File(right.uri))
    val checksum = DigestUtils.md5Hex(imageBytes)
    val reference = ImageContentPutRequest(imageId, right.timestamp, imageBytes, checksum)
    assert(left === reference)
  }
}
