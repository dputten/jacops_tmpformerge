/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import java.io.{ BufferedReader, InputStreamReader, PrintWriter }
import java.net.Socket

import akka.actor.{ ActorRef, ActorSystem, Props }
import akka.camel.CamelExtension
import akka.testkit.{ TestKit, TestProbe }
import akka.util.Timeout
import csc.akka.CamelClient
import scala.concurrent.Await
import scala.concurrent.duration._
import csc.akka.logging.DirectLogging
import csc.base.{ SensorDevice, SensorPosition }
import csc.gantry.config.RadarConfig
import csc.vehicle.message.{ Lane, VehicleRegistrationMessage }
import csc.vehicle.registration.events.EventBuilder
import org.scalatest._
import unit.CertificateTestUtil

/**
 * Test the ReceiveRadar
 */
class ReceiveRadarTest()
  extends TestKit(ActorSystem("ReceiveRadarTest"))
  with WordSpecLike
  with MustMatchers
  with BeforeAndAfterAll
  with CamelClient
  with DirectLogging {

  //TODO PCL-45: verify CertificationData contents

  override def afterAll() {
    try {
      system.shutdown
    } catch {
      case e: Exception ⇒ log.error("COULD NOT SHUTDOWN ACTOR REGISTRY".format(e))
    }
  }

  "ReceiveRadarTest" must {
    "do the good scenario" in {
      val config = new RadarConfig("mina:tcp://localhost:11500?textline=true", 45, 5, 1)
      val lane = new Lane("100", "lane1", "gantry1", "route", 0, 0)
      val probe = TestProbe()
      val receiveRadarActorRef = system.actorOf(Props(new ReceiveRadar(lane, config,
        NMICertificate = CertificateTestUtil.createCertificate("cert XXXX"),
        eventBuilder = EventBuilder.getInstance(SensorPosition.Last, SensorDevice.Radar),
        setEventTime = true,
        nextActors = Set(probe.ref))))
      awaitActivation(receiveRadarActorRef, 10.seconds)

      val radar = new Socket("localhost", 11500);
      val out = new PrintWriter(radar.getOutputStream(), true);
      val in = new BufferedReader(new InputStreamReader(
        radar.getInputStream()))
      try {
        out.println("12;800;")
        var result = in.readLine
        assert(result == "OK", "radar message isn't as expected")

        var vehicleMsg = probe.expectMsgType[VehicleRegistrationMessage](10.seconds)
        vehicleMsg.lane must be(lane)
        vehicleMsg.event.triggerSource must be("Radar")
        vehicleMsg.vehicleData.speed.isDefined must be(true)
        math.abs(vehicleMsg.vehicleData.speed.get.value - 12) must be < 0.001F
        vehicleMsg.vehicleData.speed.get.confidence must be(75)
        vehicleMsg.vehicleData.length.isDefined must be(true)
        math.abs(vehicleMsg.vehicleData.length.get.value - 11) must be < 0.25F
        vehicleMsg.vehicleData.length.get.confidence must be(95)

        out.println("-120;800;")
        result = in.readLine
        assert(result == "OK", "radar message isn't as expected")

        vehicleMsg = probe.expectMsgType[VehicleRegistrationMessage](10.seconds)
        vehicleMsg.lane must be(lane)
        vehicleMsg.event.triggerSource must be("Radar")
        vehicleMsg.vehicleData.speed.isDefined must be(true)
        math.abs(vehicleMsg.vehicleData.speed.get.value - 120) must be < 0.001F
        vehicleMsg.vehicleData.speed.get.confidence must be(97)
        vehicleMsg.vehicleData.length.isDefined must be(true)
        math.abs(vehicleMsg.vehicleData.length.get.value - 11) must be < 0.25F
        vehicleMsg.vehicleData.length.get.confidence must be(95)

      } finally {
        try {
          out.close()
        } finally {
          radar.close()
        }
      }

      system.stop(receiveRadarActorRef)
    }

    "do the good scenario with gap" in {
      val config = new RadarConfig("mina:tcp://localhost:11501?textline=true", 45, 5, 1)
      val lane = new Lane("100", "lane1", "gantry1", "route", 0, 0)
      val probe = TestProbe()
      val receiveRadarActorRef = system.actorOf(Props(new ReceiveRadar(lane, config,
        NMICertificate = CertificateTestUtil.createCertificate("cert XXXX"),
        eventBuilder = EventBuilder.getInstance(SensorPosition.Last, SensorDevice.Radar),
        setEventTime = true,
        nextActors = Set(probe.ref))))
      awaitActivation(receiveRadarActorRef, 10.seconds)

      val radar = new Socket("localhost", 11501)
      val out = new PrintWriter(radar.getOutputStream(), true)
      val in = new BufferedReader(new InputStreamReader(
        radar.getInputStream()))
      try {
        out.println("12;800;0.98;")
        var result = in.readLine
        assert(result == "OK", "radar message isn't as expected")

        var vehicleMsg = probe.expectMsgType[VehicleRegistrationMessage](10.seconds)
        vehicleMsg.lane must be(lane)
        vehicleMsg.event.triggerSource must be("Radar")
        vehicleMsg.vehicleData.speed.isDefined must be(true)
        math.abs(vehicleMsg.vehicleData.speed.get.value - 12) must be < 0.001F
        vehicleMsg.vehicleData.speed.get.confidence must be(75)
        vehicleMsg.vehicleData.length.isDefined must be(true)
        math.abs(vehicleMsg.vehicleData.length.get.value - 11) must be < 0.25F
        vehicleMsg.vehicleData.length.get.confidence must be(95)

        out.println("-117;800;0.98;")
        result = in.readLine
        assert(result == "OK", "radar message isn't as expected")

        vehicleMsg = probe.expectMsgType[VehicleRegistrationMessage](10.seconds)
        vehicleMsg.lane must be(lane)
        vehicleMsg.event.triggerSource must be("Radar")
        vehicleMsg.vehicleData.speed.isDefined must be(true)
        math.abs(vehicleMsg.vehicleData.speed.get.value - 117) must be < 0.001F
        vehicleMsg.vehicleData.speed.get.confidence must be(97)
        vehicleMsg.vehicleData.length.isDefined must be(true)
        math.abs(vehicleMsg.vehicleData.length.get.value - 11) must be < 0.25F
        vehicleMsg.vehicleData.length.get.confidence must be(95)

      } finally {
        try {
          out.close()
        } finally {
          radar.close()
        }
      }

      system.stop(receiveRadarActorRef)
    }
  }

}