/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import java.util.Date

import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import scala.concurrent.duration._
import csc.akka.logging.DirectLogging
import csc.vehicle.message._
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, WordSpec }

/**
 * Test the JoinByEventId Actor
 */
class JoinByEventIdTest extends TestKit(ActorSystem("JoinByEventIdTest")) with WordSpecLike with MustMatchers with DirectLogging with BeforeAndAfterAll {

  override def afterAll() {
    try {
      system.shutdown
    } catch {
      case e: Exception ⇒ log.error("COULD NOT SHUTDOWN ACTOR REGISTRY".format(e))
    }
  }
  val prioList = List[String]("src1", "src2")

  "The JoinActor" must {
    "join two messages" in {
      val probe = TestProbe()
      val convertActorRef = TestActorRef(new JoinByEventId(prioList, "dest", 10, Set(probe.ref)))

      val list = List(new RegistrationImage("test.tif", ImageFormat.TIF_BAYER, VehicleImageType.Overview, ImageVersion.Original, 1234))
      val event = new SensorEvent(20L, new PeriodRange(10L, 40L), "src1")
      val msg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventId = Some("ID1"),
        eventTimestamp = Some(30L),
        event = event,
        priority = Priority.NONCRITICAL,
        licenseData = RegistrationLicenseData(license = Some(new ValueWithConfidence[String]("12abc3", 90)),
          country = Some(new ValueWithConfidence[String]("NL", 90))),
        vehicleData = VehicleData(length = Some(new ValueWithConfidence[Float](5.6F, 90)),
          direction = Some(Direction.Incoming),
          speed = Some(new ValueWithConfidence[Float](98F, 80)),
          category = Some(ValueWithConfidence[VehicleCategory.Value](VehicleCategory.Pers, 30))),
        files = list,
        directory = Some(new RelayDirectory("/uri/to/directory")))
      val event2 = new SensorEvent(25L, new PeriodRange(5L, 45L), "src2")
      val msg2 = new VehicleRegistrationMessage(
        lane = new Lane("200", "lane2", "gantry1", "route", 0, 0),
        eventId = Some("ID1"),
        eventTimestamp = Some(40L),
        event = event2,
        priority = Priority.CRITICAL,
        licenseData = RegistrationLicenseData(license = Some(new ValueWithConfidence[String]("12ab30", 80)),
          country = Some(new ValueWithConfidence[String]("DE", 70))),
        vehicleData = VehicleData(length = Some(new ValueWithConfidence[Float](4.6F, 80)),
          direction = Some(Direction.Outgoing),
          speed = Some(new ValueWithConfidence[Float](88F, 88)),
          category = Some(ValueWithConfidence[VehicleCategory.Value](VehicleCategory.Van, 30))),
        files = list,
        directory = Some(new RelayDirectory("/uri/to/another/directory")))
      val expectEvent = event.copy(triggerSource = "dest")
      val expectMsg = msg.copy(event = expectEvent)

      convertActorRef ! msg
      probe.expectNoMsg()
      convertActorRef ! msg2
      probe.expectMsg(500 milli, expectMsg)
    }
    "cleanup messages after joining" in {
      val probe = TestProbe()
      val convertActorRef = TestActorRef(new JoinByEventId(prioList, "dest", 10, Set(probe.ref)))

      val list = List(new RegistrationImage("test.tif", ImageFormat.TIF_BAYER, VehicleImageType.Overview, ImageVersion.Original, 1234))
      val event = new SensorEvent(20L, new PeriodRange(10L, 40L), "src1")
      val msg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventId = Some("ID2"),
        eventTimestamp = Some(30L),
        event = event,
        priority = Priority.NONCRITICAL,
        licenseData = RegistrationLicenseData(license = Some(new ValueWithConfidence[String]("12abc3", 90)),
          country = Some(new ValueWithConfidence[String]("NL", 90))),
        vehicleData = VehicleData(length = Some(new ValueWithConfidence[Float](5.6F, 90)),
          direction = Some(Direction.Outgoing),
          speed = Some(new ValueWithConfidence[Float](98F, 80)),
          category = Some(ValueWithConfidence[VehicleCategory.Value](VehicleCategory.Pers, 30))),
        files = list,
        directory = Some(new RelayDirectory("/uri/to/directory")))
      val event2 = new SensorEvent(25L, new PeriodRange(5L, 45L), "src2")
      val msg2 = new VehicleRegistrationMessage(
        lane = new Lane("200", "lane2", "gantry1", "route", 0, 0),
        eventId = Some("ID2"),
        eventTimestamp = Some(40L),
        event = event2,
        priority = Priority.CRITICAL,
        licenseData = RegistrationLicenseData(license = Some(new ValueWithConfidence[String]("12ab30", 80)),
          country = Some(new ValueWithConfidence[String]("DE", 70))),
        vehicleData = VehicleData(length = Some(new ValueWithConfidence[Float](4.6F, 80)),
          direction = Some(Direction.Outgoing),
          speed = Some(new ValueWithConfidence[Float](88F, 88)),
          category = Some(ValueWithConfidence[VehicleCategory.Value](VehicleCategory.Van, 30))),
        files = list,
        directory = Some(new RelayDirectory("/uri/to/another/directory")))
      val expectEvent = event.copy(triggerSource = "dest")
      val expectMsg = msg.copy(event = expectEvent)

      convertActorRef ! msg
      probe.expectNoMsg()
      convertActorRef ! msg2
      probe.expectMsg(500 milli, expectMsg)
      convertActorRef ! msg
      probe.expectNoMsg()
    }
    "process message when exceeding buffersize" in {
      val bufferSize = 10
      val probe = TestProbe()
      val convertActorRef = TestActorRef(new JoinByEventId(prioList, "dest", bufferSize, Set(probe.ref)))

      val list = List(new RegistrationImage("test.tif", ImageFormat.TIF_BAYER, VehicleImageType.Overview, ImageVersion.Original, 1234))
      val event = new SensorEvent(20L, new PeriodRange(10L, 40L), "src1")
      val msg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventId = Some("ID1"),
        eventTimestamp = Some(30L),
        event = event,
        priority = Priority.NONCRITICAL,
        licenseData = RegistrationLicenseData(license = Some(new ValueWithConfidence[String]("12abc3", 90)),
          country = Some(new ValueWithConfidence[String]("NL", 90))),
        vehicleData = VehicleData(length = Some(new ValueWithConfidence[Float](5.6F, 90)),
          direction = Some(Direction.Outgoing),
          speed = Some(new ValueWithConfidence[Float](98F, 80)),
          category = Some(ValueWithConfidence[VehicleCategory.Value](VehicleCategory.Pers, 30))),
        files = list,
        directory = Some(new RelayDirectory("/uri/to/directory")))

      for (index ← 0 until bufferSize) {
        val msgTmp = msg.copy(eventId = Some("ID%d".format(index)))
        convertActorRef ! msgTmp
        probe.expectNoMsg()
      }
      val msgTmp = msg.copy(eventId = Some("ID%d".format(bufferSize)))

      val expectEvent = event.copy(triggerSource = "dest")
      val expectMsg = msg.copy(eventId = Some("ID%d".format(0)), event = expectEvent)

      convertActorRef ! msgTmp
      probe.expectMsg(500 milli, expectMsg)
    }
    "Forward message it has no EventId" in {
      val bufferSize = 10
      val probe = TestProbe()
      val convertActorRef = TestActorRef(new JoinByEventId(prioList, "dest", bufferSize, Set(probe.ref)))

      val list = List(new RegistrationImage("test.tif", ImageFormat.TIF_BAYER, VehicleImageType.Overview, ImageVersion.Original, 1234))
      val event = new SensorEvent(20L, new PeriodRange(10L, 40L), "src1")
      val msg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventTimestamp = Some(30L),
        event = event,
        priority = Priority.NONCRITICAL,
        licenseData = RegistrationLicenseData(license = Some(new ValueWithConfidence[String]("12abc3", 90)),
          country = Some(new ValueWithConfidence[String]("NL", 90))),
        vehicleData = VehicleData(length = Some(new ValueWithConfidence[Float](5.6F, 90)),
          direction = Some(Direction.Outgoing),
          speed = Some(new ValueWithConfidence[Float](98F, 80)),
          category = Some(ValueWithConfidence[VehicleCategory.Value](VehicleCategory.Pers, 30))),
        files = list,
        directory = Some(new RelayDirectory("/uri/to/directory")))

      convertActorRef ! msg
      probe.expectMsg(msg)
    }
  }
}