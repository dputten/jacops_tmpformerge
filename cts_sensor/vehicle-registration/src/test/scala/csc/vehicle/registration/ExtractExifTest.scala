/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import java.io.File
import java.util.Date

import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import scala.concurrent.duration._
import csc.akka.logging.DirectLogging
import csc.vehicle.config.{ ExifConfig, ExifTargetConfig }
import csc.vehicle.message.{ PeriodRange, RegistrationImage, SensorEvent, VehicleRegistrationMessage, _ }
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }
import testframe.Resources
import unit.base.FileTestUtils

/**
 * Test the ExtractExif actor
 */
class ExtractExifTest extends TestKit(ActorSystem("ExtractExifTest")) with WordSpecLike with MustMatchers with DirectLogging
  with BeforeAndAfterEach with BeforeAndAfterAll {
  import FileTestUtils._

  // resource directories
  val resourceBaseDir = new File(Resources.getResourceDirPath().getAbsolutePath)
  val extractExifResourceDir = new File(resourceBaseDir, "csc/vehicle/registration/extractExif")
  val extractExifInputResourceDir = new File(extractExifResourceDir, "input")
  val extractExifOutputResourceDir = new File(extractExifResourceDir, "output")
  // test directories
  val testBaseDir = new File(resourceBaseDir, "out/extractExif")
  if (!testBaseDir.exists()) {
    testBaseDir.mkdirs()
  } else {
    clearDirectory(testBaseDir)
  }
  val inputDir = new File(testBaseDir, "input")
  inputDir.mkdirs()
  val outputDir = new File(testBaseDir, "output")
  outputDir.mkdirs()

  override def afterAll() {
    try {
      system.shutdown()
    } catch {
      case e: Exception ⇒ log.error("COULD NOT SHUTDOWN ACTOR REGISTRY".format(e))
    }
    clearDirectory(testBaseDir)
    testBaseDir.delete()
  }

  override def afterEach() {
    clearDirectory(inputDir)
    clearDirectory(outputDir)
  }

  "A ExifConfig actor, configured to extract the Overview image" when {
    val probe = TestProbe()

    val config = ExifConfig(
      dirExifImage = outputDir.getAbsolutePath,
      targetConfigs = Set(ExifTargetConfig(targetImageType = VehicleImageType.Overview, targetImageVersion = ImageVersion.Decorated)),
      writeJsonToComment = true)

    val extractExifActorRef = TestActorRef(new ExtractExif(
      imageType = VehicleImageType.Overview,
      config = config,
      nextActors = Set(probe.ref)))

    "receiving a message with an Overview RegistrationImage (jpeg, Original)" must {
      "pass on message, containing new RegistrationExifFile (Overview), and create the corresponding file" in {
        // copy images
        val inputOverviewImage = copyFile("imageWithExif.jpg", extractExifInputResourceDir, inputDir)
        // prepare message
        val overviewImage = new RegistrationImage(
          uri = inputOverviewImage.getAbsolutePath,
          imageFormat = ImageFormat.JPG_RGB,
          imageType = VehicleImageType.Overview,
          imageVersion = ImageVersion.Original,
          timestamp = 1234,
          timeYellow = Some(12342L),
          timeRed = Some(333L))
        val now = System.currentTimeMillis()
        val msg = new VehicleRegistrationMessage(
          lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
          eventId = Some("A2-44.5MH-Lane1"),
          eventTimestamp = Some(100L),
          event = new SensorEvent(now, new PeriodRange(now, now), "test"),
          priority = Priority.NONCRITICAL,
          files = List(overviewImage))
        // send message
        extractExifActorRef ! msg
        // create expected message
        val expectedMsg = msg.copy(files = List(overviewImage))
        // test if message is received
        val recvMsg = probe.expectMsgType[VehicleRegistrationMessage](500 millis)
        //messages should be equal except the received message contains a lot al exif info
        val cleanFiles = recvMsg.files.map {
          case imagefile: RegistrationImage ⇒ {
            imagefile.metaInfo.isDefined must be(true)
            imagefile.metaInfo.get.size must be(33)
            imagefile.copy(metaInfo = None)
          }
          case other ⇒ other
        }
        recvMsg.copy(files = cleanFiles) must be(expectedMsg)
      }
    }

    "receiving a message with an Overview RegistrationImage (jpeg, Original), but image does not exist" must {
      "pass unchanged message on" in {
        // non existing overview image
        val inputOverviewImage = new File(inputDir, "imageWithExif.jpg")

        // prepare message
        val overviewImage = new RegistrationImage(
          uri = inputOverviewImage.getAbsolutePath,
          imageFormat = ImageFormat.JPG_RGB,
          imageType = VehicleImageType.Overview,
          imageVersion = ImageVersion.Original,
          timestamp = 1234,
          timeYellow = Some(12342L),
          timeRed = Some(333L))
        val now = System.currentTimeMillis()
        val msg = new VehicleRegistrationMessage(
          lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
          eventId = Some("A2-44.5MH-Lane1"),
          eventTimestamp = Some(100L),
          event = new SensorEvent(now, new PeriodRange(now, now), "test"),
          priority = Priority.NONCRITICAL,
          files = List(overviewImage))
        // send message
        extractExifActorRef ! msg
        // create expected message
        // test if message is received
        probe.expectMsg(500 millis, msg)

        directoryIsEmpty(outputDir) must be(true)
      }
    }

    "receiving a message with an Overview RegistrationImage (jpeg, Original), but image does not contain any Exif data" must {
      "pass unchanged message on" in {
        // copy images
        val inputOverviewImage = copyFile("imageWithoutExif.jpg", extractExifInputResourceDir, inputDir)

        // prepare message
        val overviewImage = new RegistrationImage(
          uri = inputOverviewImage.getAbsolutePath,
          imageFormat = ImageFormat.JPG_RGB,
          imageType = VehicleImageType.Overview,
          imageVersion = ImageVersion.Original,
          timestamp = 1234,
          timeYellow = Some(12342L),
          timeRed = Some(333L))
        val now = System.currentTimeMillis()
        val msg = new VehicleRegistrationMessage(
          lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
          eventId = Some("A2-44.5MH-Lane1"),
          eventTimestamp = Some(100L),
          event = new SensorEvent(now, new PeriodRange(now, now), "test"),
          priority = Priority.NONCRITICAL,
          files = List(overviewImage))
        // send message
        extractExifActorRef ! msg
        // create expected message
        // test if message is received
        val recvMsg = probe.expectMsgType[VehicleRegistrationMessage](500 millis)
        //messages should be equal exept the recieved message contains a lot al exif info
        val cleanFiles = recvMsg.files.map {
          case imagefile: RegistrationImage ⇒ imagefile.copy(metaInfo = None)
          case other                        ⇒ other
        }
        recvMsg.copy(files = cleanFiles) must be(msg)

        directoryIsEmpty(outputDir) must be(true)
      }
    }

    "receiving a message with an Overview RegistrationImage (jpeg, Original), but message has no eventId" must {
      "pass unchanged message on" in {
        // copy images
        val inputOverviewImage = copyFile("imageWithExif.jpg", extractExifInputResourceDir, inputDir)

        // prepare message
        val overviewImage = new RegistrationImage(
          uri = inputOverviewImage.getAbsolutePath,
          imageFormat = ImageFormat.JPG_RGB,
          imageType = VehicleImageType.Overview,
          imageVersion = ImageVersion.Original,
          timestamp = 1234,
          timeYellow = Some(12342L),
          timeRed = Some(333L))
        val now = System.currentTimeMillis()
        val msg = new VehicleRegistrationMessage(
          lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
          eventId = None,
          eventTimestamp = Some(100L),
          event = new SensorEvent(now, new PeriodRange(now, now), "test"),
          priority = Priority.NONCRITICAL,
          files = List(overviewImage))
        // send message
        extractExifActorRef ! msg
        // create expected message
        // test if message is received
        val recvMsg = probe.expectMsgType[VehicleRegistrationMessage](500 millis)
        //messages should be equal exept the recieved message contains a lot al exif info
        val cleanFiles = recvMsg.files.map {
          case imagefile: RegistrationImage ⇒ imagefile.copy(metaInfo = None)
          case other                        ⇒ other
        }
        recvMsg.copy(files = cleanFiles) must be(msg)

        directoryIsEmpty(outputDir) must be(true)
      }
    }

  }

}
