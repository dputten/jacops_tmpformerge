/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import java.util.Date

import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import csc.akka.logging.DirectLogging
import csc.vehicle.message._
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, WordSpec }

/**
 * Test the SignRegistrationFiles Actor
 */
class SignRegistrationFilesTest extends TestKit(ActorSystem("SignRegistrationFilesTest")) with WordSpecLike with MustMatchers with DirectLogging with BeforeAndAfterAll {

  override def afterAll() {
    try {
      system.shutdown
    } catch {
      case e: Exception ⇒ log.error("COULD NOT SHUTDOWN ACTOR REGISTRY".format(e))
    }
  }

  "The SignActor" must {
    "add SHA to the registration file" in {
      val probe = TestProbe()
      val testActorRef = TestActorRef(new SignRegistrationFiles(Set(probe.ref)))

      val fileName = getClass.getClassLoader.getResource("alfa.tif").getPath
      val list = List(new RegistrationImage(fileName, ImageFormat.TIF_BAYER, VehicleImageType.Overview, ImageVersion.Original, 1234))
      val event = new SensorEvent(20000L, new PeriodRange(10000L, 40000L), "src1")
      val msg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventId = Some("ID1"),
        event = event,
        priority = Priority.NONCRITICAL,
        licenseData = RegistrationLicenseData(license = Some(new ValueWithConfidence[String]("12abc3", 90))),
        files = list)
      testActorRef ! msg
      val expectedFile = list.head.copy(sha = Some("b8bd3e8cfdc0bb4f94be6fe14ba112df"))
      val expectedMsg = msg.copy(files = List(expectedFile))
      probe.expectMsg(expectedMsg)
    }
  }
}