package csc.vehicle.registration

import java.io.File
import java.util.Date

import akka.actor.{ ActorRef, ActorSystem }
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import csc.akka.logging.DirectLogging
import csc.gantry.config.ImageMask
import csc.util.test.{ FuzzyImageMatcher, ObjectBuilder }
import csc.vehicle.config.{ DetailedTriggerMaskConfig, TriggerMaskConfig, SimpleTriggerMaskConfig }
import csc.vehicle.message._
import org.scalatest._
import testframe.Resources
import unit.base.FileTestUtils._

/**
 * Created by carlos on 18/01/16.
 */
class TriggerMaskImageTest extends TestKit(ActorSystem("TriggerMaskImageTest"))
  with WordSpecLike with MustMatchers with DirectLogging with BeforeAndAfterEach with BeforeAndAfterAll {

  import TriggerMaskImageTest._

  override def afterAll() {
    system.shutdown
  }

  override def beforeAll(): Unit = {
    testBaseDir.mkdirs()
    clearDirectory(testBaseDir)
    inputDir.mkdir()
    outputDir.mkdir()
  }

  val height = 100
  val width = 100

  "SimpleTriggerMaskConfig" must {
    val maskConfig = new SimpleTriggerMaskConfig(50, 65, "jpg")

    "compute the correct image mask for L trigger" in {
      val imageMask = maskConfig.imageMask(height, width, ImageTriggerData(TriggerArea.LEFT, true))
      assertMask(imageMask, List((0, 50), (65, 50), (65, 100), (0, 100)))
    }
    "compute the correct image mask for R trigger" in {
      val imageMask = maskConfig.imageMask(height, width, ImageTriggerData(TriggerArea.RIGHT, false))
      assertMask(imageMask, List((35, 50), (100, 50), (100, 100), (35, 100)))
    }
  }

  "DetailedTriggerMaskConfig" must {

    val maskConfig = defaultMaskConfig

    "compute the correct image mask for L trigger" in {
      val imageMask = maskConfig.imageMask(height, width, ImageTriggerData(TriggerArea.LEFT, true))
      assertMask(imageMask, List((0, 50), (65, 50), (65, 100), (0, 100)))
    }
    "compute the correct image mask for R trigger" in {
      val imageMask = maskConfig.imageMask(height, width, ImageTriggerData(TriggerArea.RIGHT, false))
      assertMask(imageMask, List((35, 50), (100, 50), (100, 100), (35, 100)))
    }

  }

  "TriggerMaskImage" must {

    val probe = TestProbe()

    "forward unaltered registration when message has no trigger data" in {

      val actorRef = createActor(probe, defaultMaskConfig)
      val registration = createRegistration(None)

      actorRef ! registration

      val result = probe.expectMsgType[VehicleRegistrationMessage]
      result must be(registration)
    }

    "forward registration with masked image when message has L trigger data" in {
      testMaskImage("ref_l_trigger_mask.jpg", true, defaultMaskConfig)
    }

    "forward registration with masked image when message has R trigger data" in {
      testMaskImage("ref_r_trigger_mask.jpg", false, defaultMaskConfig)
    }

    "forward registration with exotic masked image when message has L trigger data" in {
      testMaskImage("ref_l_trigger_mask_exotic.jpg", true, exoticMaskConfig)
    }

    "forward registration with exotic masked image when message has R trigger data" in {
      testMaskImage("ref_r_trigger_mask_exotic.jpg", false, exoticMaskConfig)
    }

    def testMaskImage(refImage: String, triggerLeft: Boolean, cfg: TriggerMaskConfig): Unit = {

      val side = if (triggerLeft) TriggerArea.LEFT else TriggerArea.RIGHT

      val registration = createRegistration(Some(ImageTriggerData(side, true)))
      val actorRef = createActor(probe, cfg)
      actorRef ! registration

      val result = probe.expectMsgType[VehicleRegistrationMessage]
      assertMaskedImage(registration, result, refImage)

    }

    def assertMaskedImage(base: VehicleRegistrationMessage, result: VehicleRegistrationMessage, refImageFilename: String): Unit = {
      val maskedImage = result.getImageFile(ImageFormat.JPG_RGB, VehicleImageType.OverviewMasked, ImageVersion.Original)
      val appliedMask = result.appliedMask
      (maskedImage, appliedMask) match {
        case (Some(img), Some(mask)) ⇒ {
          val actualFile = new File(img.uri)
          actualFile.exists() must be(true) //file was really created
          val refFile = new File(imageResourceDir, refImageFilename)
          FuzzyImageMatcher.check(actualFile, refFile, 10)
        }
        case (None, _) ⇒ fail("no mask image found")
        case (_, None) ⇒ fail("no applied mask found")
      }

      val tmp = result.copy(files = base.files, appliedMask = None)
      tmp must be(base) //making sure nothing else was changed
    }

    def createActor(probe: TestProbe, cfg: TriggerMaskConfig): ActorRef =
      TestActorRef(new TriggerMaskImage(cfg, Set(probe.ref)))

  }

}

object TriggerMaskImageTest extends ObjectBuilder with MustMatchers {

  val resourceDir = new File(Resources.getResourceDirPath().getAbsolutePath)
  val imageResourceDir = new File(resourceDir, "csc/vehicle/registration/mask-image")
  val testBaseDir = new File(resourceDir, "out/mask-image")
  val inputDir = new File(testBaseDir, "input")
  val outputDir = new File(testBaseDir, "output")

  lazy val inputImage = new File(imageResourceDir, "mask_source.jpg")
  lazy val overviewImage = create[RegistrationImage].copy(
    uri = inputImage.getPath,
    imageFormat = ImageFormat.JPG_RGB,
    imageType = VehicleImageType.Overview,
    imageVersion = ImageVersion.Original,
    timestamp = System.currentTimeMillis())

  lazy val defaultMaskConfig = DetailedTriggerMaskConfig(
    List(Point(0, 50), Point(65, 50), Point(65, 100), Point(0, 100)),
    List(Point(35, 50), Point(100, 50), Point(100, 100), Point(35, 100)), outputDir.getPath, 80)

  lazy val exoticMaskConfig = DetailedTriggerMaskConfig(
    List(Point(0, 0), Point(100, 0), Point(0, 100)),
    List(Point(50, 50), Point(70, 50), Point(80, 70), Point(60, 80), Point(45, 65)),
    defaultMaskConfig.dirMaskImage, defaultMaskConfig.maskQuality)

  def createRegistration(triggerData: Option[ImageTriggerData]): VehicleRegistrationMessage = {

    val time = System.currentTimeMillis()

    create[VehicleRegistrationMessage].copy(
      eventId = Some(time.toString),
      eventTimestamp = Some(time),
      files = List(overviewImage),
      imageTriggerData = triggerData)
  }

  def assertMask(mask: ImageMask, expected: List[(Int, Int)]): Unit = {
    expected.size must be(mask.vertices.size)
    val actual = mask.vertices.map { v ⇒ (v.x, v.y) }
    actual must be(expected)
  }

}
