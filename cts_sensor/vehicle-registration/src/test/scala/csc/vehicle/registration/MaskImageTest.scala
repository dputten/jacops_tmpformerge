/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import java.io.File
import java.util.Date

import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import scala.concurrent.duration._
import csc.akka.logging.DirectLogging
import csc.gantry.config.{ ImageMaskVertex, _ }
import csc.vehicle.message.{ PeriodRange, RegistrationImage, SensorEvent, VehicleRegistrationMessage, _ }
import org.apache.commons.io.FileUtils
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }
import testframe.Resources
import unit.base.FileTestUtils

/**
 * Test the MaskImage class
 */
class MaskImageTest extends TestKit(ActorSystem("MaskImageTest")) with WordSpecLike with MustMatchers with DirectLogging
  with BeforeAndAfterEach with BeforeAndAfterAll {
  import FileTestUtils._

  val resourceDir = new File(Resources.getResourceDirPath().getAbsolutePath)
  val imageResourceDir = new File(resourceDir, "csc/vehicle/registration/mask-image")
  val testBaseDir = new File(resourceDir, "out/mask-image")
  if (!testBaseDir.exists()) {
    testBaseDir.mkdirs()
  } else {
    clearDirectory(testBaseDir)
  }
  val inputDir = new File(testBaseDir, "input")
  inputDir.mkdir()
  val outputDir = new File(testBaseDir, "output")
  outputDir.mkdir()

  override def afterAll() {
    try {
      system.shutdown
    } catch {
      case e: Exception ⇒ log.error("COULD NOT SHUTDOWN ACTOR REGISTRY".format(e))
    }
    clearDirectory(testBaseDir)
    testBaseDir.delete()
  }

  override def afterEach {
    clearDirectory(inputDir)
    clearDirectory(outputDir)
  }

  val vertices = List(
    new Point(200, 600),
    new Point(300, 250),
    new Point(700, 150),
    new Point(1000, 400),
    new Point(750, 600),
    new Point(900, 800),
    new Point(500, 900))

  val mask = new ImageMaskImpl(
    vertices = vertices.map { point ⇒ new ImageMaskVertex(point.x, point.y) })

  "The MaskImage actor, configured to mask without cropping" when {
    val probe = TestProbe()

    val maskActorRef = TestActorRef(new MaskImage(
      mask = mask,
      cropEnabled = false,
      jpgQuality = 100,
      dirMaskedImage = outputDir.getAbsolutePath,
      nextActors = Set(probe.ref)))

    "receiving a message with a Overview RegistrationImage (jpeg, Original)" must {
      "create a new RegistrationImage (OverviewMasked, jpeg, Original, same size) and the corresponding file" in {
        // copy images
        val inputOverviewImage = copyFile("mask_source.jpg", imageResourceDir, inputDir)
        // prepare message
        val overviewImage = new RegistrationImage(
          uri = inputOverviewImage.getAbsolutePath,
          imageFormat = ImageFormat.JPG_RGB,
          imageType = VehicleImageType.Overview,
          imageVersion = ImageVersion.Original,
          timestamp = 1234,
          timeYellow = Some(12342L),
          timeRed = Some(333L))
        val now = System.currentTimeMillis()
        val msg = new VehicleRegistrationMessage(
          lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
          eventId = Some("A2-44.5MH-Lane1"),
          eventTimestamp = Some(100L),
          event = new SensorEvent(now, new PeriodRange(now, now), "test"),
          priority = Priority.NONCRITICAL,
          files = List(overviewImage))
        // send message
        maskActorRef ! msg
        // create expected message
        val outputMaskedOverviewImage = new File(outputDir, "masked_mask_source.jpg")
        val overviewMaskedImage = new RegistrationImage(
          uri = outputMaskedOverviewImage.getAbsolutePath,
          imageFormat = ImageFormat.JPG_RGB,
          imageType = VehicleImageType.OverviewMasked,
          imageVersion = ImageVersion.Original,
          timestamp = 1234,
          timeYellow = Some(12342L),
          timeRed = Some(333L))
        val expectedMsg = msg.copy(
          appliedMask = Some(vertices),
          appliedMaskCropped = false,
          files = List(overviewImage, overviewMaskedImage))
        // test if message is received
        probe.expectMsg(500 millis, expectedMsg)
        // test if result is correct
        outputMaskedOverviewImage.exists() must be(true)

        val refImageFile = new File(imageResourceDir, "ref_image_mask_without_crop.jpg")
        imagesMustBeTheSame(outputMaskedOverviewImage, refImageFile)
      }
    }
  }

  "The MaskImage actor, configured to mask with cropping" when {
    val probe = TestProbe()

    val maskActorRef = TestActorRef(new MaskImage(
      mask = mask,
      cropEnabled = true,
      jpgQuality = 100,
      dirMaskedImage = outputDir.getAbsolutePath,
      nextActors = Set(probe.ref)))

    "receiving a message with a Overview RegistrationImage (jpeg, Original)" must {
      "create a new RegistrationImage (OverviewMasked, jpeg, Original, smaller size) and the corresponding file" in {
        // copy images
        val inputOverviewImage = copyFile("mask_source.jpg", imageResourceDir, inputDir)
        // prepare message
        val overviewImage = new RegistrationImage(
          uri = inputOverviewImage.getAbsolutePath,
          imageFormat = ImageFormat.JPG_RGB,
          imageType = VehicleImageType.Overview,
          imageVersion = ImageVersion.Original,
          timestamp = 1234,
          timeYellow = Some(12342L),
          timeRed = Some(333L))
        val now = System.currentTimeMillis()
        val msg = new VehicleRegistrationMessage(
          lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
          eventId = Some("A2-44.5MH-Lane1"),
          eventTimestamp = Some(100),
          event = new SensorEvent(now, new PeriodRange(now, now), "test"),
          priority = Priority.NONCRITICAL,
          files = List(overviewImage))
        // send message
        maskActorRef ! msg
        // create expected message
        val outputMaskedOverviewImage = new File(outputDir, "masked_mask_source.jpg")
        val overviewMaskedImage = new RegistrationImage(
          uri = outputMaskedOverviewImage.getAbsolutePath,
          imageFormat = ImageFormat.JPG_RGB,
          imageType = VehicleImageType.OverviewMasked,
          imageVersion = ImageVersion.Original,
          timestamp = 1234,
          timeYellow = Some(12342L),
          timeRed = Some(333L))
        val expectedMsg = msg.copy(
          appliedMask = Some(vertices),
          appliedMaskCropped = true,
          files = List(overviewImage, overviewMaskedImage))
        // test if message is received
        probe.expectMsg(500 millis, expectedMsg)
        // test if result is correct
        outputMaskedOverviewImage.exists() must be(true)

        val refImageFile = new File(imageResourceDir, "ref_image_mask_with_crop.jpg")
        imagesMustBeTheSame(outputMaskedOverviewImage, refImageFile)
      }
    }
  }

  def imagesMustBeTheSame(testImageFile: File, refImageFile: File) = {
    val testFile = FileUtils.readFileToByteArray(testImageFile)
    val refFile = FileUtils.readFileToByteArray(refImageFile)

    val equal = testFile.deep == refFile.deep
    equal must be(true) //making sure we don't flood the output with the image bytes
  }

}
