/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import csc.amqp.adapter.CorrelatedMessage
import csc.util.test.ObjectBuilder
import csc.vehicle.message.VehicleRegistrationMessage
import org.scalatest.{ WordSpec, _ }

import scala.concurrent.duration._
class RegistrationBufferTest extends WordSpec with MustMatchers {

  import RegistrationBufferTest._

  "The buffer" must {

    "keeptrack of waiting messages" in {
      val startTime = System.currentTimeMillis() - 1.hour.toMillis
      val buffer = new RegistrationBuffer[DummyCorrelated](1.minute)
      val id = "id1"
      val reg = createReg(id)
      val msg = DummyCorrelated(reg.id, startTime)
      buffer.addRegistration(reg, msg)
      buffer.isRegistrationProcessed(id) must be(false)
      buffer.registrationProcessed(id)
      buffer.isRegistrationProcessed(id) must be(true)
    }
    "keeptrack of timeout messages" in {
      val startTime = System.currentTimeMillis() - 1.hour.toMillis
      val buffer = new RegistrationBuffer[DummyCorrelated](1.minute)
      val reg = createReg("id1")
      val msg = DummyCorrelated(reg.id, startTime)
      buffer.addRegistration(reg, msg)
      buffer.getTimeouts(startTime) must be(Map())
      buffer.getTimeouts(startTime + 1.minute.toMillis - 1) must be(Map())
      buffer.getTimeouts(startTime + 1.minute.toMillis + 1) must be(Map(reg.id -> reg))
      buffer.registrationProcessed(reg.id)
      buffer.getTimeouts(startTime + 5.minute.toMillis + 5) must be(Map())
    }
  }

  case class DummyCorrelated(msgId: String, time: Long) extends CorrelatedMessage

}

object RegistrationBufferTest extends ObjectBuilder {

  def createReg(id: String): VehicleRegistrationMessage =
    create[VehicleRegistrationMessage].copy(eventId = Some(id))

}