/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import java.io.File
import java.util.Date

import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import scala.concurrent.duration._
import csc.akka.logging.DirectLogging
import csc.vehicle.config.JpegConvertParameters
import csc.vehicle.message._
import imagefunctions.{ ImageFunctionsFactory, JpgFunctionsTestImpl }
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import org.scalatest._

/**
 * Test the ConvertJPEG class
 */
class ConvertJPEGTest() extends TestKit(ActorSystem("ConvertJPEGTest")) with WordSpecLike with DirectLogging with MustMatchers with BeforeAndAfterAll {
  val dirJpg = ""

  override def afterAll() {
    try {
      system.shutdown
    } catch {
      case e: Exception ⇒ log.error("COULD NOT SHUTDOWN ACTOR REGISTRY".format(e))
    }
  }

  "ConvertJPEG" must {
    "perform the good scenario with mock overview" in {
      /**
       *
       * This test uses the ClibraryTestImpl which is a mock of the ClibraryTestImpl ImageFunctions implementation.
       * The mock is setup to behave correctly (a good scenario)
       */
      val jpg = new JpegConvertParameters(imageType = VehicleImageType.Overview, jpg_width = 1, jpg_height = 1, jpg_quality = 1, conversionEnabled = true)
      val imagefunctionsImpl = new JpgFunctionsTestImpl(false)
      ImageFunctionsFactory.setJpgFunctions(imagefunctionsImpl)
      val fileName = "TestFile.tiff"
      val file = new File(fileName)
      if (!file.exists()) {
        file.deleteOnExit()
        file.createNewFile() must be(true)
      }

      val probe = TestProbe()
      val convertActorRef = TestActorRef(new ConvertJPEG(jpg, None, ImageVersion.Original, Set(probe.ref), dirJpg))

      val image = new RegistrationImage(fileName, ImageFormat.TIF_BAYER, VehicleImageType.Overview, ImageVersion.Original, 1234)
      val now = System.currentTimeMillis()
      val event = new SensorEvent(now, new PeriodRange(now, now), "test")
      val msg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventId = Some("100"),
        eventTimestamp = None,
        event = event,
        priority = Priority.NONCRITICAL,
        files = List(image))

      convertActorRef ! msg

      val newImage = new RegistrationImage("/converted-TestFile.jpg", ImageFormat.JPG_RGB, VehicleImageType.Overview, ImageVersion.Original, 1234)
      val expectedMsg = msg.copy(files = List(newImage), replacedFiles = List(image))

      probe.expectMsg(200.millis, expectedMsg)
    }

    "perform the good scenario with mock license" in {
      /**
       *
       * This test uses the ClibraryTestImpl which is a mock of the ClibraryTestImpl ImageFunctions implementation.
       * The mock is setup to behave correctly (a good scenario)
       */
      val jpg = new JpegConvertParameters(imageType = VehicleImageType.License, jpg_width = 1, jpg_height = 1, jpg_quality = 1, conversionEnabled = true)
      val imagefunctionsImpl = new JpgFunctionsTestImpl(false)
      ImageFunctionsFactory.setJpgFunctions(imagefunctionsImpl)
      val fileName = "TestFile.tiff"
      val file = new File(fileName)
      if (!file.exists()) {
        file.deleteOnExit()
        file.createNewFile() must be(true)
      }
      val probe = TestProbe()
      //VehicleImageType.License,
      val convertActorRef = TestActorRef(new ConvertJPEG(jpg, None, ImageVersion.Original, Set(probe.ref), dirJpg))

      val image = new RegistrationImage(fileName, ImageFormat.TIF_BAYER, VehicleImageType.License, ImageVersion.Original, 1234)
      val now = System.currentTimeMillis()
      val msg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventId = Some("100"),
        eventTimestamp = None,
        event = new SensorEvent(now, new PeriodRange(now, now), "test"),
        priority = Priority.NONCRITICAL,
        files = List(image))

      convertActorRef ! msg

      val imageLicense = new RegistrationImage("/converted-TestFile.jpg", ImageFormat.JPG_RGB, VehicleImageType.License, ImageVersion.Original, 1234)
      val expectedMsg = msg.copy(files = List(imageLicense), replacedFiles = List(image))

      probe.expectMsg(200 millis, expectedMsg)
    }

    "perform without an image" in {
      /*
      * Testing a failed scenario by having a wrong image.
      * Using a mock for ImageFunctionsImpl imagefunctions
      */
      val jpg = new JpegConvertParameters(imageType = VehicleImageType.License, jpg_width = 1, jpg_height = 1, jpg_quality = 1, conversionEnabled = true)
      val imagefunctionsImpl = new JpgFunctionsTestImpl(false)
      ImageFunctionsFactory.setJpgFunctions(imagefunctionsImpl)

      val probe = TestProbe()
      val convertActorRef = TestActorRef(new ConvertJPEG(jpg, None, ImageVersion.Original, Set(probe.ref), dirJpg))

      val image = new RegistrationImage("TestFile.tiff", ImageFormat.TIF_BAYER, VehicleImageType.Overview, ImageVersion.Original, 1234)
      val now = System.currentTimeMillis()
      val msg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventId = Some("100"),
        eventTimestamp = None,
        event = new SensorEvent(now, new PeriodRange(now, now), "test"),
        priority = Priority.NONCRITICAL,
        files = List(image))

      convertActorRef ! msg

      probe.expectMsg(200.millis, msg) //same message as result
    }
  }
}
