/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import java.io.File
import java.util.Date

import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import scala.concurrent.duration._
import csc.akka.logging.DirectLogging
import csc.vehicle.message.{ PeriodRange, RegistrationImage, SensorEvent, VehicleRegistrationMessage, _ }
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }
import testframe.Resources

/**
 * Test the CleanupVehicleRegistration
 */
class CleanupVehicleRegistrationTest extends TestKit(ActorSystem("CleanupVehicleRegistrationTest")) with WordSpecLike with MustMatchers with DirectLogging
  with BeforeAndAfterEach with BeforeAndAfterAll {
  val testSubdirectory = "tempTestDirectory"

  val resourceDir = Resources.getResourceDirPath().getAbsolutePath
  val inputDir = new File(resourceDir, "out/cleanup-test")
  if (!inputDir.exists()) {
    inputDir.mkdirs()
  } else {
    clearDirectory(inputDir)
  }

  override def afterAll() {
    system.shutdown

    inputDir.delete()
  }

  override def afterEach {
    clearDirectory(inputDir)
  }

  "The CleanupVehicleRegistration" when {
    val probe = TestProbe()
    val cleanupActorRef = TestActorRef(new CleanupVehicleRegistration(nextActors = Set(probe.ref)))

    "receiving a message with some files" must {
      "delete all these files" in {
        // create 3 files
        val file1 = createFile(inputDir, "testFile1.txt")
        val file2 = createFile(inputDir, "testFile2.tar")
        val file3 = createFile(inputDir, "testFile3.jpg")
        // prepare VehicleRegistrationMessage containing these files
        val registrationFile1 = new RegistrationRelay(file1.getAbsolutePath)
        val registrationFile2 = new RegistrationArchive(file2.getAbsolutePath)
        val registrationFile3 = new RegistrationImage(file3.getAbsolutePath, ImageFormat.JPG_RGB, VehicleImageType.Overview, ImageVersion.Decorated, 1234)
        val now = System.currentTimeMillis()
        val msg = new VehicleRegistrationMessage(
          lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
          eventId = Some("100"),
          eventTimestamp = Some(100L),
          event = new SensorEvent(now, new PeriodRange(now, now), "test"),
          priority = Priority.NONCRITICAL,
          files = List(registrationFile1, registrationFile2, registrationFile3))
        // send message to CleanupVehicleRegistration
        cleanupActorRef ! msg
        // test if message are received
        probe.expectMsg(500 millis, msg)
        // check result
        inputDir.listFiles().isEmpty must be(true)
      }
    }

    "receiving a message with a directory and some files in this directory" must {
      "delete all these files and the directory" in {
        // create a subdirectory with 3 files
        val inputSubdirectory = new File(inputDir, testSubdirectory)
        inputSubdirectory.mkdir()
        val file1 = createFile(inputSubdirectory, "testFile1.txt")
        val file2 = createFile(inputSubdirectory, "testFile2.tar")
        val file3 = createFile(inputSubdirectory, "testFile3.jpg")
        // prepare VehicleRegistrationMessage containing this directory and these files
        val registrationFile1 = new RegistrationRelay(file1.getAbsolutePath)
        val registrationFile2 = new RegistrationArchive(file2.getAbsolutePath)
        val registrationFile3 = new RegistrationImage(file3.getAbsolutePath, ImageFormat.JPG_RGB, VehicleImageType.Overview, ImageVersion.Decorated, 1234)
        val registrationDir = new RelayDirectory(inputSubdirectory.getAbsolutePath)
        val now = System.currentTimeMillis()
        val msg = new VehicleRegistrationMessage(
          lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
          eventId = Some("100"),
          eventTimestamp = Some(100L),
          event = new SensorEvent(now, new PeriodRange(now, now), "test"),
          priority = Priority.NONCRITICAL,
          files = List(registrationFile1, registrationFile2, registrationFile3),
          directory = Some(registrationDir))
        // send message to CleanupVehicleRegistration
        cleanupActorRef ! msg
        // test if message are received
        probe.expectMsg(500 millis, msg)
        // check result
        inputDir.listFiles().isEmpty must be(true)
      }
    }

    "receiving a message with a directory and some files in and outside this directory" must {
      "delete all these files and the directory" in {
        // create a subdirectory with 3 files
        val inputSubdirectory = new File(inputDir, testSubdirectory)
        inputSubdirectory.mkdir()
        val file1 = createFile(inputDir, "testFile1.txt")
        val file2 = createFile(inputSubdirectory, "testFile2.tar")
        val file3 = createFile(inputSubdirectory, "testFile3.jpg")
        // prepare VehicleRegistrationMessage containing this directory and these files
        val registrationFile1 = new RegistrationRelay(file1.getAbsolutePath)
        val registrationFile2 = new RegistrationArchive(file2.getAbsolutePath)
        val registrationFile3 = new RegistrationImage(file3.getAbsolutePath, ImageFormat.JPG_RGB, VehicleImageType.Overview, ImageVersion.Decorated, 1234)
        val registrationDir = new RelayDirectory(inputSubdirectory.getAbsolutePath)
        val now = System.currentTimeMillis()
        val msg = new VehicleRegistrationMessage(
          lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
          eventId = Some("100"),
          eventTimestamp = Some(100L),
          event = new SensorEvent(now, new PeriodRange(now, now), "test"),
          priority = Priority.NONCRITICAL,
          files = List(registrationFile1, registrationFile2, registrationFile3),
          directory = Some(registrationDir))
        // send message to CleanupVehicleRegistration
        cleanupActorRef ! msg
        // test if message are received
        probe.expectMsg(500 millis, msg)
        // check result
        inputDir.listFiles().isEmpty must be(true)
      }
    }

    "receiving a message with a non-empty directory" must {
      "not delete this directory" in {
        val inputSubdirectory = new File(inputDir, testSubdirectory)
        inputSubdirectory.mkdir()
        createFile(inputSubdirectory, "testFile1.txt")
        // prepare VehicleRegistrationMessage containing this directory (but not the file)
        val registrationDir = new RelayDirectory(inputSubdirectory.getAbsolutePath)
        val now = System.currentTimeMillis()
        val msg = new VehicleRegistrationMessage(
          lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
          eventId = Some("100"),
          eventTimestamp = Some(100L),
          event = new SensorEvent(now, new PeriodRange(now, now), "test"),
          priority = Priority.NONCRITICAL,
          // NO files
          directory = Some(registrationDir))
        // send message to CleanupVehicleRegistration
        cleanupActorRef ! msg
        // test if message are received
        probe.expectMsg(500 millis, msg)
        // check result
        inputDir.listFiles().isEmpty must be(false)

      }
    }

    "receiving a message with a directory and some files, but a file in directory is not part of message" must {
      "delete all files in message and not delete the directory and the omitted file" in {
        // create a subdirectory with 3 files
        val inputSubdirectory = new File(inputDir, testSubdirectory)
        inputSubdirectory.mkdir()
        val file1 = createFile(inputSubdirectory, "testFile1.txt")
        val file2 = createFile(inputSubdirectory, "testFile2.tar")
        val file3 = createFile(inputSubdirectory, "testFile3.jpg")
        val forgotten_file4 = createFile(inputSubdirectory, "testFile4_not_in_message.jpg")
        // prepare VehicleRegistrationMessage containing this directory and only the first 3 files
        val registrationFile1 = new RegistrationRelay(file1.getAbsolutePath)
        val registrationFile2 = new RegistrationArchive(file2.getAbsolutePath)
        val registrationFile3 = new RegistrationImage(file3.getAbsolutePath, ImageFormat.JPG_RGB, VehicleImageType.Overview, ImageVersion.Decorated, 1234)
        val registrationDir = new RelayDirectory(inputSubdirectory.getAbsolutePath)
        val now = System.currentTimeMillis()
        val msg = new VehicleRegistrationMessage(
          lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
          eventId = Some("100"),
          eventTimestamp = Some(100L),
          event = new SensorEvent(now, new PeriodRange(now, now), "test"),
          priority = Priority.NONCRITICAL,
          files = List(registrationFile1, registrationFile2, registrationFile3),
          directory = Some(registrationDir))
        // send message to CleanupVehicleRegistration
        cleanupActorRef ! msg
        // test if message are received
        probe.expectMsg(500 millis, msg)
        // check result
        inputDir.listFiles().isEmpty must be(false)
        inputSubdirectory.listFiles().isEmpty must be(false)
        inputSubdirectory.listFiles().length must be(1)
        forgotten_file4.exists() must be(true)
      }
    }

  }

  def createFile(dir: File, filename: String): File = {
    val newFile = new File(dir, filename)
    if (newFile.exists) {
      log.info("New file already exists " + newFile.getAbsolutePath)
      val isDeleted = newFile.delete
      if (!isDeleted) {
        log.info("Failed to delete existing file " + newFile.getAbsolutePath)
      }
    } else {
      log.info("New file does not exist yet " + newFile.getAbsolutePath)
      val isCreated = newFile.createNewFile
      if (!isCreated) {
        log.info("New file can not be created " + newFile.getAbsolutePath)
      } else {
        log.info("New file is created " + newFile.getAbsolutePath)
      }
    }
    newFile
  }

  def clearDirectory(dir: File) {
    def deleteFile(file: File) {
      if (file.isDirectory) {
        val subfiles = file.listFiles
        if (subfiles != null)
          subfiles.foreach { f ⇒ deleteFile(f) }
      }
      file.delete
    }

    for {
      file ← dir.listFiles()
    } deleteFile(file)
  }

  def createDirectory(path: String) {
    new File(path).mkdir()
  }
}
