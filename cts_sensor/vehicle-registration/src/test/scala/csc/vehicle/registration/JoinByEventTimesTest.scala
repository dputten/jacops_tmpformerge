/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import java.util.Date

import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import scala.concurrent.duration._
import csc.akka.logging.DirectLogging
import csc.vehicle.message._
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, WordSpec }

/**
 * Test the JoinByEventId Actor
 */
class JoinByEventTimesTest extends TestKit(ActorSystem("JoinByEventTimesTest")) with WordSpecLike with MustMatchers with DirectLogging with BeforeAndAfterAll {

  override def afterAll() {
    try {
      system.shutdown
    } catch {
      case e: Exception ⇒ log.error("COULD NOT SHUTDOWN ACTOR REGISTRY".format(e))
    }
  }
  val prioList = List[String]("src1", "src2")

  "The JoinActor" must {
    "join two messages" in {
      val probe = TestProbe()
      val testActorRef = TestActorRef(new JoinByEventTimes(prioList, "dest", 10, false, "LaneId", 1.minute, Set(probe.ref)))

      val list = List(new RegistrationImage("test.tif", ImageFormat.TIF_BAYER, VehicleImageType.Overview, ImageVersion.Original, 1234))
      val event = new SensorEvent(20000L, new PeriodRange(10000L, 40000L), "src1")
      val msg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventId = Some("ID1"),
        event = event,
        priority = Priority.NONCRITICAL,
        licenseData = RegistrationLicenseData(license = Some(new ValueWithConfidence[String]("12abc3", 90))),
        files = list)
      val event2 = new SensorEvent(25000L, new PeriodRange(5000L, 45000L), "src2")
      val msg2 = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventTimestamp = Some(40L),
        event = event2,
        vehicleData = VehicleData(length = Some(new ValueWithConfidence[Float](4.6F, 80)),
          speed = Some(new ValueWithConfidence[Float](88F, 88))),
        licenseData = RegistrationLicenseData(country = Some(new ValueWithConfidence[String]("DE", 70))))

      val expectEvent = new SensorEvent(20000L, new PeriodRange(10000L, 40000L), "dest")
      val expectMsg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventId = Some("ID1"),
        eventTimestamp = Some(40L),
        event = expectEvent,
        vehicleData = VehicleData(length = Some(new ValueWithConfidence[Float](4.6F, 80)),
          speed = Some(new ValueWithConfidence[Float](88F, 88))),
        licenseData = RegistrationLicenseData(license = Some(new ValueWithConfidence[String]("12abc3", 90)),
          country = Some(new ValueWithConfidence[String]("DE", 70))),
        files = list)

      testActorRef ! msg
      probe.expectNoMsg()
      testActorRef ! msg2
      probe.expectMsg(500 milli, expectMsg)

    }
    "process two messages out of order" in {
      val probe = TestProbe()
      val testActorRef = TestActorRef(new JoinByEventTimes(prioList, "dest", 10, false, "LaneId", 1.minute, Set(probe.ref)))

      val list = List(new RegistrationImage("test.tif", ImageFormat.TIF_BAYER, VehicleImageType.Overview, ImageVersion.Original, 1234))
      val event = new SensorEvent(20L, new PeriodRange(10L, 40L), "src1")
      val msg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventId = Some("ID1"),
        event = event,
        priority = Priority.NONCRITICAL,
        licenseData = RegistrationLicenseData(license = Some(new ValueWithConfidence[String]("12abc3", 90))),
        files = list)
      val event2 = new SensorEvent(25L, new PeriodRange(5L, 45L), "src2")
      val msg2 = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventTimestamp = Some(40L),
        event = event2,
        vehicleData = VehicleData(length = Some(new ValueWithConfidence[Float](4.6F, 80)),
          speed = Some(new ValueWithConfidence[Float](88F, 88))),
        licenseData = RegistrationLicenseData(country = Some(new ValueWithConfidence[String]("DE", 70))))

      val list22 = List(new RegistrationImage("test.tif", ImageFormat.TIF_BAYER, VehicleImageType.Overview, ImageVersion.Original, 1234))
      val event21 = new SensorEvent(60L, new PeriodRange(50L, 80L), "src1")
      val msg21 = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventId = Some("ID1"),
        event = event21,
        priority = Priority.NONCRITICAL,
        licenseData = RegistrationLicenseData(license = Some(new ValueWithConfidence[String]("12abc3", 90))),
        files = list22)
      val event22 = new SensorEvent(65L, new PeriodRange(45L, 85L), "src2")
      val msg22 = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventTimestamp = Some(80L),
        event = event22,
        vehicleData = VehicleData(length = Some(new ValueWithConfidence[Float](4.6F, 80)),
          speed = Some(new ValueWithConfidence[Float](88F, 88))),
        licenseData = RegistrationLicenseData(country = Some(new ValueWithConfidence[String]("DE", 70))))

      val expectEvent = new SensorEvent(20L, new PeriodRange(10L, 40L), "dest")
      val expectMsg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventId = Some("ID1"),
        eventTimestamp = Some(40L),
        event = expectEvent,
        vehicleData = VehicleData(length = Some(new ValueWithConfidence[Float](4.6F, 80)),
          speed = Some(new ValueWithConfidence[Float](88F, 88))),
        licenseData = RegistrationLicenseData(
          license = Some(new ValueWithConfidence[String]("12abc3", 90)),
          country = Some(new ValueWithConfidence[String]("DE", 70))),
        files = list)

      val expectEvent2 = new SensorEvent(60L, new PeriodRange(50L, 80L), "dest")
      val expectMsg2 = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventId = Some("ID1"),
        eventTimestamp = Some(80L),
        event = expectEvent2,
        vehicleData = VehicleData(length = Some(new ValueWithConfidence[Float](4.6F, 80)),
          speed = Some(new ValueWithConfidence[Float](88F, 88))),
        licenseData = RegistrationLicenseData(
          license = Some(new ValueWithConfidence[String]("12abc3", 90)),
          country = Some(new ValueWithConfidence[String]("DE", 70))),
        files = list)

      testActorRef ! msg22
      testActorRef ! msg2
      probe.expectNoMsg()
      testActorRef ! msg21
      probe.expectMsg(500 milli, expectMsg2)
      testActorRef ! msg
      probe.expectMsg(500 milli, expectMsg)
    }
    "cleanup message after joining" in {
      val probe = TestProbe()
      val testActorRef = TestActorRef(new JoinByEventTimes(prioList, "dest", 10, false, "LaneId", 1.minute, Set(probe.ref)))

      val list = List(new RegistrationImage("test.tif", ImageFormat.TIF_BAYER, VehicleImageType.Overview, ImageVersion.Original, 1234))
      val event = new SensorEvent(20L, new PeriodRange(10L, 40L), "src1")
      val msg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventId = Some("ID1"),
        event = event,
        priority = Priority.NONCRITICAL,
        licenseData = RegistrationLicenseData(license = Some(new ValueWithConfidence[String]("12abc3", 90))),
        files = list)
      val event2 = new SensorEvent(25L, new PeriodRange(5L, 45L), "src2")
      val msg2 = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventTimestamp = Some(40L),
        event = event2,
        vehicleData = VehicleData(length = Some(new ValueWithConfidence[Float](4.6F, 80)),
          speed = Some(new ValueWithConfidence[Float](88F, 88))),
        licenseData = RegistrationLicenseData(country = Some(new ValueWithConfidence[String]("DE", 70))))

      val expectEvent = new SensorEvent(20L, new PeriodRange(10L, 40L), "dest")
      val expectMsg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventId = Some("ID1"),
        eventTimestamp = Some(40L),
        event = expectEvent,
        vehicleData = VehicleData(length = Some(new ValueWithConfidence[Float](4.6F, 80)),
          speed = Some(new ValueWithConfidence[Float](88F, 88))),
        licenseData = RegistrationLicenseData(license = Some(new ValueWithConfidence[String]("12abc3", 90)),
          country = Some(new ValueWithConfidence[String]("DE", 70))),
        files = list)

      testActorRef ! msg
      probe.expectNoMsg()
      testActorRef ! msg2
      probe.expectMsg(500 milli, expectMsg)
      testActorRef ! msg
      probe.expectNoMsg()
    }
    "process message in cleanup with EventId" in {
      val bufferSize = 10
      val probe = TestProbe()
      val testActorRef = TestActorRef(new JoinByEventTimes(prioList, "dest", bufferSize, false, "LaneId", 1.minute, Set(probe.ref)))

      val list = List(new RegistrationImage("test.tif", ImageFormat.TIF_BAYER, VehicleImageType.Overview, ImageVersion.Original, 1234))
      val event = new SensorEvent(20L, new PeriodRange(10L, 40L), "src1")
      val msg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventId = Some("ID1"),
        eventTimestamp = Some(30L),
        event = event,
        priority = Priority.NONCRITICAL,
        licenseData = RegistrationLicenseData(license = Some(new ValueWithConfidence[String]("12abc3", 90)),
          country = Some(new ValueWithConfidence[String]("NL", 90))),
        vehicleData = VehicleData(length = Some(new ValueWithConfidence[Float](5.6F, 90)),
          speed = Some(new ValueWithConfidence[Float](98F, 80)),
          category = Some(ValueWithConfidence[VehicleCategory.Value](VehicleCategory.Pers, 30))),
        files = list)

      val range = 50
      for (index ← 0 until bufferSize) {
        val date = index * range
        val eventTmp = new SensorEvent(date, new PeriodRange(date, (index + 1) * range), "src1")
        val msgTmp = msg.copy(event = eventTmp)
        testActorRef ! msgTmp
        probe.expectNoMsg()
      }
      val eventTmp = new SensorEvent(bufferSize * range, new PeriodRange(bufferSize * range, (bufferSize + 1) * range), "src1")
      val msgTmp = msg.copy(event = eventTmp)

      val date = 0L
      val expectEvent = new SensorEvent(date, new PeriodRange(date, range), "dest")
      val expectMsg = msg.copy(event = expectEvent)

      testActorRef ! msgTmp
      probe.expectMsg(500 milli, expectMsg)
    }
    "process message in cleanup without EventId" in {
      val bufferSize = 10
      val probe = TestProbe()
      val testActorRef = TestActorRef(new JoinByEventTimes(prioList, "dest", bufferSize, false, "LaneId", 1.minute, Set(probe.ref)))

      val list = List(new RegistrationImage("test.tif", ImageFormat.TIF_BAYER, VehicleImageType.Overview, ImageVersion.Original, 1234))
      val event = new SensorEvent(20L, new PeriodRange(10L, 40L), "src1")
      val msg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventTimestamp = Some(30L),
        event = event,
        priority = Priority.NONCRITICAL,
        licenseData = RegistrationLicenseData(license = Some(new ValueWithConfidence[String]("12abc3", 90)),
          country = Some(new ValueWithConfidence[String]("NL", 90))),
        vehicleData = VehicleData(length = Some(new ValueWithConfidence[Float](5.6F, 90)),
          speed = Some(new ValueWithConfidence[Float](98F, 80)),
          category = Some(ValueWithConfidence[VehicleCategory.Value](VehicleCategory.Pers, 30))),
        files = list)
      val range = 50
      for (index ← 0 until bufferSize) {
        val date = index * range
        val eventTmp = new SensorEvent(date, new PeriodRange(date, (index + 1) * range), "src1")
        val msgTmp = msg.copy(event = eventTmp)
        testActorRef ! msgTmp
        probe.expectNoMsg()
      }
      val eventTmp = new SensorEvent(bufferSize * range, new PeriodRange(bufferSize * range, (bufferSize + 1) * range), "src1")
      val msgTmp = msg.copy(event = eventTmp)

      testActorRef ! msgTmp
      probe.expectNoMsg()
    }
    "process message without EventTime" in {
      val bufferSize = 2
      val probe = TestProbe()
      val testActorRef = TestActorRef(new JoinByEventTimes(prioList, "dest", bufferSize, true, "LaneId", 1.minute, Set(probe.ref)))

      val list = List(new RegistrationImage("test.tif", ImageFormat.TIF_BAYER, VehicleImageType.Overview, ImageVersion.Original, 1234))
      val event = new SensorEvent(20L, new PeriodRange(10L, 40L), "src1")
      val msg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        event = event,
        eventId = Some("ID1"),
        priority = Priority.NONCRITICAL,
        licenseData = RegistrationLicenseData(license = Some(new ValueWithConfidence[String]("12abc3", 90)),
          country = Some(new ValueWithConfidence[String]("NL", 90))),
        vehicleData = VehicleData(length = Some(new ValueWithConfidence[Float](5.6F, 90)),
          speed = Some(new ValueWithConfidence[Float](98F, 80)),
          category = Some(ValueWithConfidence[VehicleCategory.Value](VehicleCategory.Pers, 30))),
        files = list)

      val range = 50
      for (index ← 0 until bufferSize) {
        val date = index * range
        val eventTmp = new SensorEvent(date, new PeriodRange(date, (index + 1) * range), "src1")
        val msgTmp = msg.copy(event = eventTmp)
        testActorRef ! msgTmp
        probe.expectNoMsg()
      }
      val eventTmp = new SensorEvent(bufferSize * range, new PeriodRange(bufferSize * range, (bufferSize + 1) * range), "src1")
      val msgTmp = msg.copy(event = eventTmp)

      testActorRef ! msgTmp
      val receivedFile = probe.expectMsgType[VehicleRegistrationMessage]
      receivedFile.eventTimestamp must be(Some(bufferSize * range))
    }
  }
}