package csc.vehicle.registration

import java.util.concurrent.TimeUnit

import akka.actor.{ Actor, ActorRef, ActorSystem, Props }
import akka.testkit.{ TestKit, TestProbe }

import scala.concurrent.duration.FiniteDuration
import csc.akka.logging.DirectLogging
import csc.amqp.adapter.DroppedMessage
import csc.image.recognize.{ RecognizeImageRequest, RecognizeImageResponse }
import csc.util.test.{ ObjectBuilder, UniqueGenerator }
import csc.vehicle.registration.recognizer.RecognizerMQAdapter
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach }
import org.scalatest._

import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by carlos on 01/09/16.
 */
class RecognizerMQAdapterTest
  extends TestKit(ActorSystem("RecognizerMQAdapterTest"))
  with WordSpecLike
  with DirectLogging
  with MustMatchers
  with BeforeAndAfterAll
  with BeforeAndAfterEach
  with ObjectBuilder
  with UniqueGenerator {

  var trash: TestProbe = null
  val oneMinute = FiniteDuration(1, TimeUnit.MINUTES)
  val oneSecond = FiniteDuration(1, TimeUnit.SECONDS)
  val twoSecondMillis = 2000

  "RecognizerMQAdapter" must {

    "handle, delegate and reply properly RecognizeImage  and Ping requests and responses" in {

      val actor = createActor(oneMinute, None)

      val req1 = createImageRequest
      val req3 = createImageRequest

      val probe1 = TestProbe()
      val probe2 = TestProbe()

      probe1.send(actor, req1)
      probe2.send(actor, req3)

      val res1 = probe1.expectMsgType[RecognizeImageResponse]
      val res3 = probe2.expectMsgType[RecognizeImageResponse]

      //verifies that matching responses are sent to the correct clients
      res1.msgId must be(req1.msgId)
      res3.msgId must be(req3.msgId)

      trash.expectNoMsg()
    }

    "performs message cleanup" in {

      val actor = createActor(oneSecond, Some(twoSecondMillis))

      val req = createImageRequest
      val probe = TestProbe()

      probe.send(actor, req)

      val dropped = trash.expectMsgType[DroppedMessage]

      dropped.msgId must be(req.msgId)
      dropped.clientPath must be(probe.ref.path.toString)

      probe.expectNoMsg()
    }

    "discards unmatched RecognizeImageResponses" in {

      val actor = createActor(oneMinute, None)
      val res = create[RecognizeImageResponse].copy(msgId = nextUnique)

      actor ! res

      val dropped = trash.expectMsgType[RecognizeImageResponse]
      dropped must be(res)
    }
  }

  def createActor(cleanup: FiniteDuration, imageDelay: Option[Long]): ActorRef = {
    val producer = system.actorOf(Props(new DummyRecognizer(imageDelay)))
    system.actorOf(Props(new RecognizerMQAdapter(producer, cleanup, Some(trash.ref))))
  }

  override protected def beforeEach(): Unit = {
    trash = TestProbe()
  }

  override protected def afterAll(): Unit = {
    system.shutdown()
  }

  class DummyRecognizer(imageResponseDelay: Option[Long]) extends Actor {

    override def receive: Receive = {
      case msg: RecognizeImageRequest ⇒
        val delay = FiniteDuration(imageResponseDelay.getOrElse(0L), TimeUnit.MILLISECONDS)
        val resp = RecognizeImageResponse(msg.msgId, "/tmp/nofile", None)
        system.scheduler.scheduleOnce(delay, sender, resp)
    }
  }

  def createImageRequest: RecognizeImageRequest = create[RecognizeImageRequest].copy(msgId = nextUnique)

}
