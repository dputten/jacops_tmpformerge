package csc.vehicle

//copyright CSC 2010

import java.util.Date

import csc.akka.logging.DirectLogging
import csc.vehicle.message.{ Lane, MonitorCameraMessage, MonitorRadarMessage }
import image.SensorMonitor
import org.junit.Test
import org.scalatest.MustMatchers
import org.scalatest.junit.JUnitSuite

/**
 * Test the SensorMonitorTest
 */
class SensorMonitorTest extends JUnitSuite with DirectLogging with MustMatchers {
  /**
   * Test the updating of one sensor with both messages (radar and camera)
   */
  @Test
  def testUpdatingStatusForSensor {
    val host = "10.248.2.10"
    val port = 4000
    val monitor = new SensorMonitor(host, port)
    val sensor1 = new Lane("1", "lane1", "gantry1", "route", 0, 0)
    val date1 = new Date
    val msg1 = new MonitorCameraMessage(sensor1, date1)
    monitor.updateCamera(msg1)
    var sensorStatus = monitor.getSensorStatus()
    sensorStatus.size must be(1)
    var item = sensorStatus.head
    item.sensor must be(sensor1)
    item.lastCameraMsg must be(Some(date1))
    item.lastRadarMsg must be(None)
    item.radarMsg must be(None)
    val radarMsg = Some("Test msg")
    val msg2 = new MonitorRadarMessage(sensor1, date1, radarMsg)
    monitor.updateRadar(msg2)
    sensorStatus = monitor.getSensorStatus()
    sensorStatus.size must be(1)
    item = sensorStatus.head
    item.sensor must be(sensor1)
    item.lastCameraMsg must be(Some(date1))
    item.lastRadarMsg must be(Some(date1))
    item.radarMsg must be(radarMsg)
  }
  /**
   * Test the updating of one sensor with both messages (radar and camera)
   */
  @Test
  def testUpdatingStatusForSensorRadarFirst {
    val host = "10.248.2.10"
    val port = 4000
    val monitor = new SensorMonitor(host, port)
    val sensor1 = new Lane("1", "lane1", "gantry1", "route", 0, 0)
    val date1 = new Date
    val radarMsg = Some("Test msg")
    val msg2 = new MonitorRadarMessage(sensor1, date1, radarMsg)
    monitor.updateRadar(msg2)
    var sensorStatus = monitor.getSensorStatus()
    sensorStatus.size must be(1)
    var item = sensorStatus.head
    item.sensor must be(sensor1)
    item.lastCameraMsg must be(None)
    item.lastRadarMsg must be(Some(date1))
    item.radarMsg must be(radarMsg)
    val msg1 = new MonitorCameraMessage(sensor1, date1)
    monitor.updateCamera(msg1)
    sensorStatus = monitor.getSensorStatus()
    sensorStatus.size must be(1)
    item = sensorStatus.head
    item.sensor must be(sensor1)
    item.lastCameraMsg must be(Some(date1))
    item.lastRadarMsg must be(Some(date1))
    item.radarMsg must be(radarMsg)
  }
  /**
   * Test the updating of multiple sensors with both messages (radar and camera)
   */
  @Test
  def testUpdatingStatusForMultipleSensors {
    val host = "10.248.2.10"
    val port = 4000
    val monitor = new SensorMonitor(host, port)
    val sensor1 = new Lane("1", "lane1", "gantry1", "route", 0, 0)
    val sensor2 = new Lane("2", "lane2", "gantry1", "route", 0, 0)
    val date1 = new Date
    val date2 = new Date(date1.getTime + 60000) //minute later
    val msg1 = new MonitorCameraMessage(sensor1, date1)
    val msg2 = new MonitorCameraMessage(sensor2, date2)
    monitor.updateCamera(msg1)
    monitor.updateCamera(msg2)
    var sensorStatus = monitor.getSensorStatus()
    sensorStatus.size must be(2)
    for (item ← sensorStatus) {
      item.sensor.laneId match {
        case "1" ⇒ {
          item.lastCameraMsg must be(Some(date1))
          item.lastRadarMsg must be(None)
          item.radarMsg must be(None)
        }
        case "2" ⇒ {
          item.lastCameraMsg must be(Some(date2))
          item.lastRadarMsg must be(None)
          item.radarMsg must be(None)
        }
        case _ ⇒ fail("Unexpected sensor " + item.sensor.laneId)
      }
    }
    val radarMsg = Some("Test msg")
    val msg3 = new MonitorRadarMessage(sensor1, date2, radarMsg)
    monitor.updateRadar(msg3)
    sensorStatus = monitor.getSensorStatus()
    sensorStatus.size must be(2)
    for (item ← sensorStatus) {
      item.sensor.laneId match {
        case "1" ⇒ {
          item.lastCameraMsg must be(Some(date1))
          item.lastRadarMsg must be(Some(date2))
          item.radarMsg must be(radarMsg)
        }
        case "2" ⇒ {
          item.lastCameraMsg must be(Some(date2))
          item.lastRadarMsg must be(None)
          item.radarMsg must be(None)
        }
        case _ ⇒ fail("Unexpected sensor " + item.sensor.laneId)
      }
    }
  }
  /**
   * Test the getting update message
   */
  @Test
  def testCreatingMsg {
    val host = "10.248.2.10"
    val port = 4000
    val monitor = new SensorMonitor(host, port)
    val date1 = new Date

    var updateMsg = monitor.getUpdateMessage(date1)
    updateMsg.hostName must be(host)
    updateMsg.hostPort must be(port)
    updateMsg.updateTime must be(date1)
    updateMsg.version must be("Unknown")
    math.abs(updateMsg.startTime.getTime - date1.getTime) < 200 must be(true)
    updateMsg.sensorMonitor.size must be(0)

    val sensor1 = new Lane("1", "lane1", "gantry1", "route", 0, 0)
    val date2 = new Date(date1.getTime + 60000)
    val msg1 = new MonitorCameraMessage(sensor1, date1)
    monitor.updateCamera(msg1)
    val radarMsg = Some("Test msg")
    val msg3 = new MonitorRadarMessage(sensor1, date2, radarMsg)
    monitor.updateRadar(msg3)

    updateMsg = monitor.getUpdateMessage(date2)
    updateMsg.hostName must be(host)
    updateMsg.hostPort must be(port)
    updateMsg.updateTime must be(date2)
    updateMsg.version must be("Unknown")
    math.abs(updateMsg.startTime.getTime - date1.getTime) < 200 must be(true)
    updateMsg.sensorMonitor.size must be(1)
    var item = updateMsg.sensorMonitor.head
    item.sensor must be(sensor1)
    item.lastCameraMsg must be(Some(date1))
    item.lastRadarMsg must be(Some(date2))
    item.radarMsg must be(radarMsg)
  }

}