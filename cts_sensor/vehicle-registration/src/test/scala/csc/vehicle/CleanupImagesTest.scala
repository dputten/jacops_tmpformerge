package csc.vehicle

//copyright CSC 2010

import java.io._
import java.text.SimpleDateFormat
import java.util.{ Calendar, Date }

import akka.actor.{ ActorSystem, Props }
import akka.testkit.TestKit
import csc.akka.logging.DirectLogging
import csc.vehicle.message.{ CalendarTimeUnitType, CleanupImagePathMessage, CleanupImagesMessage }
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }
import org.scalatest._

/**
 * Test the CleanupImagesTest class
 */
class CleanupImagesTest extends TestKit(ActorSystem("CleanupImagesTest")) with WordSpecLike with MustMatchers
  with DirectLogging with BeforeAndAfterAll with BeforeAndAfterEach {
  val fileHelper = new FileCreatorHelper(new Date);
  val home = "out/"

  override def afterAll() {
    try {
      system.shutdown
    } catch {
      case e: Exception ⇒ log.error("COULD NOT SHUTDOWN ACTOR REGISTRY".format(e))
    }
  }

  override def afterEach() {
    fileHelper.cleanup()
  }

  "CleanupImagesTest" must {

    "remove files from subdirectories" in {
      val path = home + "images/jpg"
      val subpath = home + "images/jpg/sub"
      val subpath2 = home + "images/jpg/sub/sub2"

      fileHelper.createFile("file1.jpg", path, -5, CalendarTimeUnitType.DAYS, 1)
      fileHelper.createFile("file2.jpg", subpath, -1, CalendarTimeUnitType.DAYS, 1)
      fileHelper.createFile("file3.jpg", subpath2, -5, CalendarTimeUnitType.DAYS, 1)

      //check that files are created
      assert(new File(path + "/file1.jpg").exists, "file file1.jpg is not created")
      assert(new File(subpath + "/file2.jpg").exists, "file file2.jpg is not created")
      assert(new File(subpath2 + "/file3.jpg").exists, "file file3.jpg is not created")

      val imagecleaner = system.actorOf(Props(new CleanupImages()))
      val msg = new CleanupImagesMessage(Set[CleanupImagePathMessage](new CleanupImagePathMessage(path = path,
        extensions = Set[String]("jpg"),
        olderThen = 3,
        timeUnit = CalendarTimeUnitType.DAYS,
        recursive = true)))

      imagecleaner ! msg

      checkForFile(file = new File(path + "/file1.jpg"), mustExists = false, assertMessage = "file file1.jpg is not deleted")
      checkForFile(file = new File(subpath + "/file2.jpg"), mustExists = true, assertMessage = "file file2.jpg is deleted from subdir")
      checkForFile(file = new File(subpath2 + "/file3.jpg"), mustExists = false, assertMessage = "file file3.jpg is not deleted from subdi")
    }

    "remove files older than X days" in {
      val path = home + "images/jpg"

      fileHelper.createFile("file1.jpg", path, -5, CalendarTimeUnitType.DAYS, 1)
      fileHelper.createFile("file2.jpg", path, -1, CalendarTimeUnitType.DAYS, 1)
      fileHelper.createFile("file3.gif", path, -5, CalendarTimeUnitType.DAYS, 1)

      val imagecleaner = system.actorOf(Props(new CleanupImages()))
      val msg = new CleanupImagesMessage(Set[CleanupImagePathMessage](new CleanupImagePathMessage(path = path,
        extensions = Set[String]("jpg"),
        olderThen = 3,
        timeUnit = CalendarTimeUnitType.DAYS,
        recursive = true)))

      imagecleaner ! msg

      checkForFile(file = new File(path + "/file1.jpg"), mustExists = false, assertMessage = "file file1.jpg is not deleted")
      checkForFile(file = new File(path + "/file2.jpg"), mustExists = true, assertMessage = "file file2.jpg is deleted")
      checkForFile(file = new File(path + "/file3.gif"), mustExists = true, assertMessage = "file file3.gif is deleted")
    }

    "remove files older than X minutes" in {
      val path = home + "images/jpg"

      fileHelper.createFile("file11.jpg", path, -5, CalendarTimeUnitType.MINUTE, 1)
      fileHelper.createFile("file21.jpg", path, -1, CalendarTimeUnitType.MINUTE, 1)
      fileHelper.createFile("file31.gif", path, -5, CalendarTimeUnitType.MINUTE, 1)

      val imagecleaner = system.actorOf(Props(new CleanupImages()))
      val msg = new CleanupImagesMessage(Set[CleanupImagePathMessage](new CleanupImagePathMessage(path = path,
        extensions = Set[String]("jpg", "gif"),
        olderThen = 3,
        timeUnit = CalendarTimeUnitType.MINUTE,
        recursive = true)))

      imagecleaner ! msg

      checkForFile(file = new File(path + "/file11.jpg"), mustExists = false, assertMessage = "file file11.jpg is not deleted")
      checkForFile(file = new File(path + "/file21.jpg"), mustExists = true, assertMessage = "file file21.jpg is deleted")
      checkForFile(file = new File(path + "/file31.gif"), mustExists = false, assertMessage = "file file31.gif is deleted")
    }

    "remove files older than X hours" in {
      val path = home + "images/jpg"

      fileHelper.createFile("file12.jpg", path, -5, CalendarTimeUnitType.HOUR, 1)
      fileHelper.createFile("file22.jpg", path, -1, CalendarTimeUnitType.HOUR, 1)
      fileHelper.createFile("file32.gif", path, -5, CalendarTimeUnitType.HOUR, 1)

      val imagecleaner = system.actorOf(Props(new CleanupImages()))
      val msg = new CleanupImagesMessage(Set[CleanupImagePathMessage](new CleanupImagePathMessage(path = path,
        extensions = Set[String]("jpg", "gif"),
        olderThen = 3,
        timeUnit = CalendarTimeUnitType.HOUR,
        recursive = true)))

      imagecleaner ! msg

      checkForFile(file = new File(path + "/file12.jpg"), mustExists = false, assertMessage = "file file12.jpg is not deleted")
      checkForFile(file = new File(path + "/file22.jpg"), mustExists = true, assertMessage = "file file22.jpg is deleted")
      checkForFile(file = new File(path + "/file32.gif"), mustExists = false, assertMessage = "file file32.gif is deleted")
    }

    "remove files for extension" ignore {
      val path = home + "images/jpg"
      val filename = ".gif.tiff.giff.jpg.jpg.gif"

      fileHelper.createFile(filename, path, -5, CalendarTimeUnitType.HOUR, 1)

      val imagecleaner = system.actorOf(Props(new CleanupImages()))
      val msg = new CleanupImagesMessage(Set[CleanupImagePathMessage](new CleanupImagePathMessage(path = path,
        extensions = Set[String]("jpg"),
        olderThen = 3,
        timeUnit = CalendarTimeUnitType.HOUR)))

      imagecleaner ! msg

      checkForFile(file = new File(path + filename), mustExists = true, assertMessage = "file " + filename + " is deleted")
    }

    "remove files from different paths" in {
      val path1 = home + "images/jpg"
      val path2 = home + "images/gif"

      val filename10 = "filename10.jpg"
      val filename11 = "filename11.jpg"
      val filename12 = "filename12.gif"

      val filename20 = "filename20.jpg"
      val filename21 = "filename21.jpg"
      val filename22 = "filename22.gif"

      fileHelper.createFile(filename10, path1, -5, CalendarTimeUnitType.HOUR, 1)
      fileHelper.createFile(filename11, path1, -5, CalendarTimeUnitType.HOUR, 1)
      fileHelper.createFile(filename12, path1, -5, CalendarTimeUnitType.HOUR, 1)

      fileHelper.createFile(filename20, path2, -5, CalendarTimeUnitType.HOUR, 1)
      fileHelper.createFile(filename21, path2, -5, CalendarTimeUnitType.HOUR, 1)
      fileHelper.createFile(filename22, path2, -5, CalendarTimeUnitType.HOUR, 1)

      val imagecleaner = system.actorOf(Props(new CleanupImages()))
      val msg = new CleanupImagesMessage(Set[CleanupImagePathMessage](
        new CleanupImagePathMessage(path = path1,
          extensions = Set[String]("jpg"),
          olderThen = 3,
          timeUnit = CalendarTimeUnitType.HOUR),
        new CleanupImagePathMessage(path = path2,
          extensions = Set[String]("gif"),
          olderThen = 3,
          timeUnit = CalendarTimeUnitType.HOUR)))

      imagecleaner ! msg

      checkForFile(file = new File(path1 + "/" + filename10), mustExists = false, assertMessage = "file " + filename10 + " is not deleted")
      checkForFile(file = new File(path1 + "/" + filename11), mustExists = false, assertMessage = "file " + filename11 + " is not deleted")
      checkForFile(file = new File(path1 + "/" + filename12), mustExists = true, assertMessage = "file " + filename12 + " is deleted")

      checkForFile(file = new File(path2 + "/" + filename20), mustExists = true, assertMessage = "file " + filename20 + " is deleted")
      checkForFile(file = new File(path2 + "/" + filename21), mustExists = true, assertMessage = "file " + filename21 + " is deleted")
      checkForFile(file = new File(path2 + "/" + filename22), mustExists = false, assertMessage = "file " + filename22 + " is not deleted")
    }

    "remove non existing file" in {
      val path1 = home + "images/jpg"
      val path2 = home + "images/gif"

      val filename10 = "filename10.jpg"
      val filename11 = "filename11.jpg"
      val filename12 = "filename12.jpg"

      val filename20 = "filename20.gif"
      val filename21 = "filename21.gif"
      val filename22 = "filename22.gif"

      //fileHelper.createFile(filename10,path1,-5,CalendarTimeUnitType.HOUR,1)
      fileHelper.createFile(filename11, path1, -5, CalendarTimeUnitType.HOUR, 1)
      fileHelper.createFile(filename12, path1, -5, CalendarTimeUnitType.HOUR, 1)

      //fileHelper.createFile(filename20,path2,-5,CalendarTimeUnitType.HOUR,1)
      fileHelper.createFile(filename21, path2, -5, CalendarTimeUnitType.HOUR, 1)
      fileHelper.createFile(filename22, path2, -5, CalendarTimeUnitType.HOUR, 1)

      val imagecleaner = system.actorOf(Props(new CleanupImages()))
      val msg = new CleanupImagesMessage(Set[CleanupImagePathMessage](
        new CleanupImagePathMessage(path = path1,
          extensions = Set[String]("jpg"),
          olderThen = 3,
          timeUnit = CalendarTimeUnitType.HOUR),
        new CleanupImagePathMessage(path = path2,
          extensions = Set[String]("gif"),
          olderThen = 3,
          timeUnit = CalendarTimeUnitType.HOUR)))

      imagecleaner ! msg

      checkForFile(file = new File(path1 + "/" + filename11), mustExists = false, assertMessage = "file " + filename11 + " is not deleted")
      checkForFile(file = new File(path1 + "/" + filename12), mustExists = false, assertMessage = "file " + filename12 + " is deleted")

      checkForFile(file = new File(path2 + "/" + filename21), mustExists = false, assertMessage = "file " + filename21 + " is not deleted")
      checkForFile(file = new File(path2 + "/" + filename22), mustExists = false, assertMessage = "file " + filename22 + " is not deleted")
    }

    "remove files with any extension" ignore {
      val path1 = home + "images/jpg"
      val filename10 = "filename10.jpg"
      val filename11 = "filename11.jpg"
      val filename12 = "filename12.gif"

      fileHelper.createFile(filename10, path1, -5, CalendarTimeUnitType.HOUR, 1)
      fileHelper.createFile(filename11, path1, -1, CalendarTimeUnitType.HOUR, 1)
      fileHelper.createFile(filename12, path1, -5, CalendarTimeUnitType.HOUR, 1)

      val imagecleaner = system.actorOf(Props(new CleanupImages()))
      val msg = new CleanupImagesMessage(Set[CleanupImagePathMessage](
        new CleanupImagePathMessage(path = path1,
          extensions = Set[String]("*"),
          olderThen = 3,
          timeUnit = CalendarTimeUnitType.HOUR)))

      imagecleaner ! msg

      checkForFile(file = new File(path1 + "/" + filename10), mustExists = false, assertMessage = "file " + filename10 + " is not deleted")
      checkForFile(file = new File(path1 + "/" + filename11), mustExists = true, assertMessage = "file " + filename11 + " is deleted")
      checkForFile(file = new File(path1 + "/" + filename12), mustExists = false, assertMessage = "file " + filename12 + " is deleted")
    }

    "delete file with no security rights" ignore {

      val file = "/bin/bash"
      val security = System.getSecurityManager();
      if (security == null) {
        fail("Security manager is null, can not proceed with test")
      }

      try {
        security.checkDelete(file)
        fail("could not find file with the right security restrictions")
      } catch {
        case se: SecurityException ⇒ {
          //this is what we want
        }
        case e: Exception ⇒ fail(e)
      }

      val imagecleaner = system.actorOf(Props(new CleanupImages()))
      val msg = new CleanupImagesMessage(Set[CleanupImagePathMessage](
        new CleanupImagePathMessage(path = "/bin",
          extensions = Set[String]("*"),
          olderThen = 3,
          timeUnit = CalendarTimeUnitType.MINUTE)))

      imagecleaner ! msg

      checkForFile(file = new File(file), mustExists = true, assertMessage = "file " + file + " is deleted")
    }

    "remove directories" in {
      val path = home + "images/jpg"
      val subpath = home + "images/jpg/sub"
      val subpath2 = home + "images/jpg/sub/sub2"

      fileHelper.createFile("file1.jpg", path, -5, CalendarTimeUnitType.DAYS, 1)
      fileHelper.createFile("file2.jpg", subpath, -5, CalendarTimeUnitType.DAYS, 1)
      fileHelper.createFile("file3.jpg", subpath2, -5, CalendarTimeUnitType.DAYS, 1)
      fileHelper.changeLastModified(new File(path), -5, CalendarTimeUnitType.DAYS)
      fileHelper.changeLastModified(new File(subpath), -5, CalendarTimeUnitType.DAYS)
      fileHelper.changeLastModified(new File(subpath2), -5, CalendarTimeUnitType.DAYS)

      //check that files are created
      assert((new File(path + "/file1.jpg")).exists, "file file1.jpg is not created")
      assert((new File(subpath + "/file2.jpg")).exists, "file file2.jpg is not created")
      assert((new File(subpath2 + "/file3.jpg")).exists, "file file3.jpg is not created")

      val imagecleaner = system.actorOf(Props(new CleanupImages()))
      val msg = new CleanupImagesMessage(Set[CleanupImagePathMessage](new CleanupImagePathMessage(path = path,
        extensions = Set[String]("sub"),
        olderThen = 3,
        timeUnit = CalendarTimeUnitType.DAYS,
        recursive = true)))

      imagecleaner ! msg

      checkForFile(file = new File(path + "/file1.jpg"), mustExists = true, assertMessage = "file file1.jpg is deleted")
      checkForFile(file = new File(subpath + "/file2.jpg"), mustExists = false, assertMessage = "file file2.jpg is not deleted from subdir")
      checkForFile(file = new File(subpath2 + "/file3.jpg"), mustExists = false, assertMessage = "file file3.jpg is not deleted from subdi")
      assert(!(new File(subpath).exists()), "directory should be deleted")
    }
  }

  /**
   * helper def. check if file exists or not. def will check if file existence matches with what is expected,
   * if not it will try max 10 times, total of 1 sec
   */
  def checkForFile(file: File, mustExists: Boolean, assertMessage: String) {
    var maxTimes = 10
    var counter = 0

    do {
      counter += 1
      Thread.sleep(100)
      if (file.exists == mustExists) {
        return
      }
    } while (counter <= maxTimes)

    assert(file.exists == mustExists, assertMessage)

  }

  //helper class used to created tmp files with the given size and modified date
  class FileCreatorHelper(date: Date) {
    val files = scala.collection.mutable.Set[File]()

    def cleanup() {
      files.foreach { file ⇒
        log.info("deleting " + file.getAbsolutePath)
        log.info("file is " + (if (file.delete) " deleted" else "not deleted"))
      }
    }

    def createFile(filename: String, path: String, changes: Int, timeUnit: CalendarTimeUnitType.Value, fileSizeInMB: Long) {
      val dir = createDirectoryStructure(path)
      if (dir != null) {
        val newFile = new File(dir, filename)

        createFile(newFile)
        createContent(newFile, fileSizeInMB)
        changeLastModified(newFile, changes, timeUnit)
        files += newFile
      }
    }

    def createFile(newFile: File) {
      if (newFile.exists) {
        log.info("new file already exists " + newFile.getAbsolutePath)
        val isDeleted = newFile.delete
        if (!isDeleted) {
          log.info("Failed to delete existing file " + newFile.getAbsolutePath)
        }
      } else {
        log.info("new file does not exist yet " + newFile.getAbsolutePath)
        val isCreated = newFile.createNewFile
        if (!isCreated) {
          log.info("new file can not be created " + newFile.getAbsolutePath)
        } else {
          log.info("new file is created " + newFile.getAbsolutePath)
        }
      }
    }

    def createDirectoryStructure(path: String): File = {
      val dir = new File(path)
      var directoryExists = dir.exists

      if (!directoryExists) {
        log.error("path does not exist " + path)
        directoryExists = dir.mkdirs
        if (!directoryExists) {
          log.error("Failed to create directory " + path)
        }
      } else {
        log.info("path already exists " + path)
      }

      return if (directoryExists) dir else null
    }

    def createContent(file: File, size: Long) {
      val output = new FileWriter(file)
      val bytes: Long = size * 1024 * 1024
      var i = 0

      while (i < bytes) {
        output.write(0);
        i += 1
      }

      output.close
    }

    def changeLastModified(file: File, changes: Int, timeUnit: CalendarTimeUnitType.Value) {
      var dateCalendar = Calendar.getInstance()
      dateCalendar.setTime(date)
      dateCalendar.add(timeUnit.id, changes)

      val dateIsSet = file.setLastModified(dateCalendar.getTime.getTime)

      if (dateIsSet) {
        log.info("setLastModified is modified")
        log.info((new SimpleDateFormat("yyyyMMdd")).format(dateCalendar.getTime))
      } else {
        log.info("setLastModified is not modified")
      }
    }
  }
}

