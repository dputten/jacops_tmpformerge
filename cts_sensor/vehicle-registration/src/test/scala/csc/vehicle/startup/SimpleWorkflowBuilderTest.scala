package csc.vehicle.startup

import akka.actor._
import akka.testkit.{ TestKit, TestProbe }
import base.{ RecipientList, RoundtripMessage }
import csc.akka.logging.DirectLogging
import csc.base.ActorPaths
import csc.gantry.config._
import csc.timeregistration.registration.ReadTiffImageMetaData
import csc.util.test._
import csc.util.test.actor.{ ActorLifecycleTracking, LifecycleStats }
import csc.vehicle.config._
import csc.vehicle.message.{ ImageVersion, Lane, VehicleImageType, VehicleRegistrationMessage }
import csc.vehicle.registration._
import csc.vehicle.registration.classifier.ClassifyVehicleCaller
import csc.vehicle.registration.recognizer.RecognizeImageCaller
import csc.vehicle.startup.WorkflowBuilder.{ ActorBuilder, ActorOf }
import image.FilterDoubleTriggerEvents
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }

/**
 * Created by carlos on 18.11.15.
 */
class SimpleWorkflowBuilderTest extends TestKit(ActorSystem("WorkflowBuilderTest", ActorLifecycleTracking.akkaConfig))
  with WordSpecLike
  with MustMatchers
  with DirectLogging
  with BeforeAndAfterEach
  with BeforeAndAfterAll
  with ActorLifecycleTracking {

  import WorkflowBuilderTest._

  lazy val actorOf: ActorOf = StartLaneActor.createActorOfFunction(system)

  override protected def beforeEach(): Unit = {
    clearLifecycleStats()
  }

  override protected def beforeAll(): Unit = {
    // TODO: enable when not ignoring tests anymore
    //WorkflowBuilderTest.using(system) //adding system to the cache, so it can be picked up instantiating objects that need it
    //initializeLifecycleTracker(Set(LifecycleType.Creation))
  }

  override protected def afterAll(): Unit = {
    super.afterAll()
    log.info("shutting down actor system")
    system.shutdown()
    system.awaitTermination()
    log.info("actor system shut down")
  }

  "WorkflowBuilder" ignore {

    "create the proper workflow for PIR + RADAR + CAMERA" in {
      runAndVerify(WorkflowTestCase(systemConfig, pirRadarCameraLaneConfig, true, 2, true, true))
    }

    "create the proper workflow for PIR + CAMERA" in {
      runAndVerify(WorkflowTestCase(systemConfig, pirCameraLaneConfig, true, 2, true, true))
    }

    "create the proper workflow for RADAR + CAMERA" in {
      runAndVerify(WorkflowTestCase(systemConfig, radarCameraLaneConfig, true, 2, true, true))
    }

    "create the proper workflow for double RADAR + CAMERA" in {
      runAndVerify(WorkflowTestCase(systemConfig, doubleRadarCameraLaneConfig, true, 2, true, true))
    }

    "create the proper workflow for JacopsPOC (Camera only with OpenCV)" in {
      runAndVerify(WorkflowTestCase(systemConfig, cameraLaneConfig, false, 0, false, false, true))
    }

    "create the proper workflow for an iRose config, with everything disabled where possible" in {
      runAndVerify(WorkflowTestCase(systemConfig, iRoseLaneConfig, false, 0, false, false))
    }
  }

  "WorkflowBuilder.createRegistrationCompletedBuilder" ignore {

    //cannot test these without stubbing further...
    //    "create the actor for hbase" in {
    //      runAndVerifyCompletion(create[HBaseCompletionConfig], classOf[StoreVehicleRegistrationHBase])
    //    }
    //
    //    "create the actor for amqp" in {
    //      runAndVerifyCompletion(create[AmqpCompletionConfig], classOf[StoreVehicleRegistrationAmqp])
    //    }

    "create the actor for file system" in {
      runAndVerifyCompletion(create[FileSystemCompletionConfig], classOf[StoreVehicleRegistrationFile])
    }

  }

  def runAndVerifyCompletion(config: CompletionConfig, expectedActorClass: Class[_]): Unit = {
    val producer: ActorRef = system.deadLetters //no need to inspect it
    val global: VehicleRegistrationConfiguration = create[VehicleRegistrationConfig].copy(registrationCompletion = List(config))
    val builder = WorkflowBuilder.createRegistrationCompletedBuilder(global, Some(pirRadarCameraLaneConfig), producer)

    builder.apply(actorOf, Set.empty)
    waitForActors

    val stats = getLifecycleStats()
    stats.count(expectedActorClass) must be(1)
  }

  def waitForActors: Unit = Thread.sleep(3000) //TODO csilva: improve this (and wait only what is needed). dirty way of waiting: some actors take longer to start

  def runAndVerify(testCase: WorkflowTestCase): LifecycleStats = {

    //println(FileLaneConfiguration.toJson(testCase.laneConfig))
    val probe = new TestProbe(system)
    val completionActorBuilder = dummyCompletionBuilder(Set(probe.ref))
    val dtm: Option[ActorRef] = testCase.globalConfig.doubleRegistrationFilterConfig match {
      case None ⇒ None
      case _    ⇒ Some(probe.ref)
    }
    val builder = new TestWorkflowBuilder(testCase, completionActorBuilder, actorOf, dtm, Some(probe.ref))
    val first = builder.build()
    waitForActors

    val stats = getLifecycleStats()
    //stats.events.foreach(println)

    //first ! RoundtripMessage(Nil)

    //val roundtripResult = probe.expectMsgClass(Duration("1 second"), classOf[RoundtripMessage])
    //println(roundtripResult.visited)

    //always there
    stats.count(classOf[CleanupVehicleRegistration]) must be(1)
    stats.count(classOf[RecognizeImageCaller]) must be(1)
    stats.count(classOf[DummyRegistrationCompleter]) must be(1)

    //flags
    stats.count(classOf[SignRegistrationFiles]) must be(if (testCase.sign) 1 else 0)
    stats.count(classOf[MaskImage]) must be(if (testCase.maskImage) 1 else 0)
    stats.count(classOf[TriggerMaskImage]) must be(if (testCase.globalConfig.imageConfig.triggerMaskConfig.isDefined) 1 else 0)

    //conditional
    stats.count(classOf[FilterDoubleTriggerEvents]) must be(if (testCase.globalConfig.doubleRegistrationFilterConfig.isDefined) 1 else 0)

    //multiple
    stats.count(classOf[AddMetadataToImage]) must be(testCase.exifTypeCount)
    stats.count(classOf[ExtractExif]) must be(testCase.exifTypeCount)

    var expectedConvertJpeg = 0

    testCase.globalConfig.jpegStep.overview map { c ⇒
      if (c.conversionEnabled) expectedConvertJpeg = expectedConvertJpeg + 1
    }
    testCase.globalConfig.jpegStep.license map { c ⇒
      if (c.conversionEnabled) expectedConvertJpeg = expectedConvertJpeg + 1
    }

    if (testCase.inputImageConversionEnabled) {
      stats.count(classOf[JoinByEventId]) must be(1)
      //      expectedConvertJpeg = expectedConvertJpeg + 1
    }

    stats.count(classOf[ConvertJPEG]) must be(expectedConvertJpeg)

    verifySensorActors(testCase.laneConfig, stats)

    stats
  }

  def verifySensorActors(laneConfig: LaneConfig, stats: LifecycleStats): Unit = {
    val sourceCount = laneConfig.sensorList.size
    if (sourceCount == 1) stats.count(classOf[ChangeTriggerSource]) must be(1)
    if (sourceCount > 0) stats.count(classOf[JoinByEventTimes]) must be(sourceCount - 1)

    var cameraMetadata = true

    laneConfig match {
      case cfg: IRoseSensorConfig ⇒ {
        stats.count(classOf[CollectIRoseData]) must be(1)
        cameraMetadata = false
      }
      case cfg: PirRadarCameraSensorConfig ⇒ {
        stats.count(classOf[ReceiveRadar]) must be(1)
        stats.count(classOf[ReceivePir]) must be(1)
      }
      case cfg: PirCameraSensorConfig         ⇒ stats.count(classOf[ReceivePir]) must be(1)
      case cfg: RadarCameraSensorConfig       ⇒ stats.count(classOf[ReceiveRadar]) must be(1)
      case cfg: DoubleRadarCameraSensorConfig ⇒ stats.count(classOf[ReceiveRadar]) must be(2)
      case cfg: CameraOnlyConfig              ⇒ stats.count(classOf[ClassifyVehicleCaller]) must be(1)
    }

    stats.count(classOf[ReadTiffImageMetaData]) must be(if (cameraMetadata) 1 else 0)
    stats.count(classOf[AddConfigData]) must be(if (cameraMetadata) 1 else 0)
  }
}

object WorkflowBuilderTest extends ObjectCache with UniqueGenerator {

  var currentPort = 13000

  def nextPort = {
    val value = currentPort + 1
    currentPort = value
    value
  }

  lazy val systemConfig: SystemConfig = create[SystemConfigImpl]
  lazy val cameraConfig: CameraConfig = create[CameraConfig]

  def radarConfig: RadarConfig = create[RadarConfig].copy(radarURI = "mina:tcp://localhost:%d?textline=true".format(nextPort))
  def pirConfig: PIRConfig = create[PIRConfig].copy(vrHost = "localhost", vrPort = nextPort)

  lazy val cameraLaneConfig: LaneConfig = create[CameraOnlyConfigImpl].copy(lane = createLane, camera = cameraConfig)
  lazy val pirRadarCameraLaneConfig: LaneConfig = create[PirRadarCameraSensorConfigImpl].copy(lane = createLane, camera = cameraConfig, radar = radarConfig, pir = pirConfig)
  lazy val pirCameraLaneConfig: LaneConfig = create[PirCameraSensorConfigImpl].copy(lane = createLane, camera = cameraConfig, pir = pirConfig)
  lazy val radarCameraLaneConfig: LaneConfig = create[RadarCameraSensorConfigImpl].copy(lane = createLane, camera = cameraConfig, radar = radarConfig)
  lazy val doubleRadarCameraLaneConfig: LaneConfig = create[DoubleRadarCameraSensorConfigImpl].copy(lane = createLane, camera = cameraConfig, radar = radarConfig, radarLength = radarConfig)
  lazy val iRoseLaneConfig: LaneConfig = create[IRoseSensorConfigImpl].copy(lane = createLane, camera = cameraConfig)

  lazy val leftMultilaneCamera: MultiLaneCamera = create[MultiLaneCamera].copy(leftLaneId = Some("left"))

  def createLane: Lane = create[Lane].copy(laneId = nextUnique)

  def dummyCompletionBuilder(nextActors: Set[ActorRef]): ActorBuilder = new ActorBuilder {
    override def apply(actorOf: ActorOf, next: Set[ActorRef]): ActorRef = actorOf(Props(new DummyRegistrationCompleter(nextActors)), None)
  }

}

case class WorkflowTestCase(systemConfig: SystemConfig,
                            laneConfig: LaneConfig,
                            sign: Boolean,
                            exifTypeCount: Int,
                            inputImageConversionEnabled: Boolean,
                            maskImage: Boolean,
                            multilaneCamera: Boolean = false) extends ObjectBuilder {

  val applChecksum = "dummy"
  lazy val actorPaths: ActorPaths = create[ActorPaths]

  lazy val globalConfig: VehicleRegistrationConfiguration = {
    var result = create[VehicleRegistrationConfig]

    var imageConfig = result.imageConfig
    val jpgConvertParams = create[JpegConvertParameters].copy(imageType = VehicleImageType.Overview, conversionEnabled = inputImageConversionEnabled)
    val jpegConfig = create[JpegConfig].copy(jpgConfig = Set(jpgConvertParams))
    imageConfig = imageConfig.copy(jpegStep = jpegConfig)

    if (sign) result = result.copy(certConfig = CertConfig(Some(SignConfig()), None))

    if (maskImage) imageConfig = imageConfig.copy(mask = Some(create[MaskConfig]))

    if (exifTypeCount > 0) {
      val targetConfigs = VehicleImageType.values.toList.take(exifTypeCount) map { t ⇒ ExifTargetConfig(t, ImageVersion.Original) }
      val exifConfig = create[ExifConfig].copy(targetConfigs = targetConfigs.toSet)
      imageConfig = imageConfig.copy(exif = Some(exifConfig))
    }

    if (multilaneCamera) {
      val mlcConfig = List(WorkflowBuilderTest.leftMultilaneCamera)
      imageConfig = imageConfig.copy(multiLaneCameras = mlcConfig)
    }

    result = result.copy(imageConfig = imageConfig)

    result
  }
}

class TestWorkflowBuilder(testCase: WorkflowTestCase,
                          registrationCompletedBuilder: ActorBuilder,
                          actorOf: ActorOf,
                          doubleTriggerMonitor: Option[ActorRef] = None,
                          lastStep: Option[ActorRef] = None)
  extends SimpleWorkflowBuilder(
    testCase.actorPaths,
    testCase.globalConfig,
    testCase.systemConfig,
    testCase.laneConfig,
    lastStep,
    registrationCompletedBuilder,
    doubleTriggerMonitor,
    actorOf)

class DummyRegistrationCompleter(nextActors: Set[ActorRef]) extends RecipientList(nextActors) {
  override def receive: Receive = {
    case msg: RoundtripMessage           ⇒ handleRoundtrip("RegistrationCompleter", msg)
    case msg: VehicleRegistrationMessage ⇒ sendToRecipients(msg)
  }
}
