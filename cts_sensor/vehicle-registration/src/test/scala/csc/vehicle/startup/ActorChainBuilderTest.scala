package csc.vehicle.startup

import akka.actor.{ ActorRef, ActorSystem, Props }
import akka.testkit.{ TestKit, TestProbe }
import base.{ RecipientList, RoundtripMessage }
import csc.akka.logging.DirectLogging
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, MustMatchers, WordSpecLike }

/**
 * Created by carlos on 30/11/16.
 */
class ActorChainBuilderTest
  extends TestKit(ActorSystem("ActorChainTest"))
  with WordSpecLike
  with MustMatchers
  with DirectLogging
  with BeforeAndAfterEach
  with BeforeAndAfterAll {

  var chain: ActorChainBuilder = null
  var probe: TestProbe = null

  "Actor Chain" must {
    "create a reliable actor chain" in {

      addActor("path")
      addActor("reliable")
      addActor("some")
      addActor("is")
      val f = addActor("this")

      val first = chain.first.toList(0)
      first must be(f)
      //chain.plugEndingTo(probe.ref) //making sure we plug the ending to the probe
      first ! RoundtripMessage(Nil)

      val result = probe.expectMsgType[RoundtripMessage]
      //println(result)
      result.visited.mkString(" ") must be("this is some reliable path")

    }
  }

  override protected def beforeEach(): Unit = {
    super.beforeEach()
    probe = TestProbe()
    chain = new ActorChainBuilder(Some(probe.ref))
  }

  def addActor(text: String): ActorRef = {
    val next = chain.nextActors //first thing is to fix the next actors
    val actor = system.actorOf(Props(new DummyActor(next)), text) //then we build the new actor
    chain.add(actor) //then we add it to the chain (so 'nextActors') will be different
    actor
  }

  override protected def afterAll(): Unit = {
    system.shutdown()
  }

  class DummyActor(recipients: Set[ActorRef]) extends RecipientList(recipients) {

    val str = recipients.map(_.path)
    println(s"${self.path.name} -> " + str)
    override def receive: Receive = {
      case msg: RoundtripMessage ⇒
        //println(msg)
        handleRoundtrip(self.path.name, msg)
    }
  }

  /*
  val actorOf: ActorOf = { (p, n) ⇒
    n match {
      case None       ⇒ system.actorOf(p)
      case Some(name) ⇒ system.actorOf(p, name)
    }
  }

  "ActorChain2" must {

    "create a reliable actor chain" in {

      val probe = TestProbe()
      val chain = new ActorChain2(actorOf)
      val f = chain.addActor(system.actorOf(Props(new DummyActor(chain.nextRecipients)), "this"))
      chain.addActor(system.actorOf(Props(new DummyActor(chain.nextRecipients)), "is"))
      chain.addActor(system.actorOf(Props(new DummyActor(chain.nextRecipients)), "some")) //cannot use 'a' (too short for name, will clash)
      chain.addActor(system.actorOf(Props(new DummyActor(chain.nextRecipients)), "reliable"))
      chain.addActor(system.actorOf(Props(new DummyActor(chain.nextRecipients)), "path"))

      val first = chain.resolve
      first must be(f)
      chain.plugEndingTo(probe.ref) //making sure we plug the ending to the probe
      first ! RoundtripMessage(Nil)

      val result = probe.expectMsgType[RoundtripMessage]
      //println(result)
      result.visited.mkString(" ") must be("this is some reliable path")
    }
  }

*/
}

