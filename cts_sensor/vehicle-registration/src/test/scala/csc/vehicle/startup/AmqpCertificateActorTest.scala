/*
 * Copyright (C) 2015. CSC <http://www.csc.com>
 */

package csc.vehicle.startup

import akka.actor.{ ActorRef, ActorSystem }
import scala.concurrent.Await
import akka.pattern.{ ask, gracefulStop }
import akka.testkit.{ ImplicitSender, TestActorRef, TestKit }
import akka.util.Timeout
import scala.concurrent.duration._
import com.github.sstone.amqp.Amqp._
import csc.akka.logging.DirectLogging
import csc.amqp.{ MQConnection, AmqpSerializers }
import csc.gantry.config.{ ActiveCertificate, ComponentCertificate, HonacSensorConfigImpl }
import csc.image.store.JsonSerializers
import csc.json.lift.EnumerationSerializer
import csc.vehicle.config.{ AmqpCertificateCheckConfig, Configuration }
import csc.vehicle.message._
import csc.vehicle.registration.VehicleRegistrationSerialization
import org.scalatest._

/**
 * @author csomogyi
 */
class AmqpCertificateActorTest extends TestKit(ActorSystem("amqp")) with ImplicitSender
  with WordSpecLike with MustMatchers with BeforeAndAfterAll with BeforeAndAfter with AmqpSerializers
  with DirectLogging {

  override val ApplicationId = "test"
  override implicit val formats = JsonSerializers.formats + new EnumerationSerializer(VehicleImageType)
  implicit val timeout = Timeout(1900 milliseconds)

  val lane1 = Lane("A2-1-lane1", "lane1", gantry = "1", system = "A2", 52, 4.2, None, None)
  val laneConfig1 = new HonacSensorConfigImpl(lane = lane1, null, None, Map())
  val lane2 = Lane("A2-3-lane1", "lane1", gantry = "3", system = "A2", 52, 4.2, None, None)
  val laneConfig2 = new HonacSensorConfigImpl(lane = lane2, null, None, Map())

  var amqpCheckConfig: AmqpCertificateCheckConfig = _
  var amqpConnection: MQConnection = _
  var adminChannel: ActorRef = _
  var actor: ActorRef = _

  override protected def beforeAll(): Unit = {
    val configFile = getClass.getClassLoader.getResource("csc/vehicle/config/testAmqp.conf").getPath
    val config = Configuration.createFromFile(configFile)
    val cfg = config.globalConfig

    if (cfg.amqpCertificateCheckConfig.isEmpty) fail("Missing certificate check configuration")

    val amqpConfig = cfg.amqpConfig
    amqpConnection = MQConnection.create(amqpConfig.actorName, amqpConfig.amqpUri, amqpConfig.reconnectDelay)
    adminChannel = amqpConnection.producer

    amqpCheckConfig = cfg.amqpCertificateCheckConfig.get
    recreateQueue(amqpCheckConfig.registrationReceiverRouteKey)
    actor = TestActorRef(new AmqpCertificateActor(adminChannel, amqpCheckConfig))
  }

  override protected def afterAll(): Unit = {
    deleteQueue(amqpCheckConfig.registrationReceiverRouteKey)
    system.shutdown()
  }

  def deleteQueue(queue: String): Unit = {
    Await.result(adminChannel.ask(DeleteQueue(queue)), 2 seconds).asInstanceOf[Ok]
  }

  def createQueue(queue: String): Unit = {
    Await.result(adminChannel.ask(DeclareQueue(QueueParameters(queue, false, false, true, false))), 2 seconds).asInstanceOf[Ok]
  }

  def recreateQueue(queue: String): Unit = {
    deleteQueue(queue)
    createQueue(queue)
  }

  def cleanupQueue(queue: String): Unit = {
    Await.result(adminChannel.ask(PurgeQueue(queue)), 2 seconds).asInstanceOf[Ok]
  }

  def createConsumer(queue: String): ActorRef = {
    amqpConnection.createSimpleConsumer(queue, testActor, None, true)
  }

  def stopConsumer(consumer: ActorRef): Unit = {
    val stopped = gracefulStop(consumer, 5 seconds)
    Await.result(stopped, 6 seconds)
  }

  "AmqpCertificateActor" ignore {
    "send 2 VehicleRegistrationCertificate messages" in {
      // clean up queues
      cleanupQueue(amqpCheckConfig.registrationReceiverRouteKey)

      val checksum = "la-la-la-la-la-tjooooe"
      val certificate = ComponentCertificate(amqpCheckConfig.certificateComponentName, checksum)
      val activeCertificate = ActiveCertificate(System.currentTimeMillis(), certificate)

      actor ! AmqpCertificateActor.SendCerts(List(laneConfig1, laneConfig2), activeCertificate)

      val rrConsumer = createConsumer(amqpCheckConfig.registrationReceiverRouteKey)

      val delivery = receiveOne(5 seconds).asInstanceOf[Delivery]
      val response = VehicleRegistrationSerialization.readCertificate(new String(delivery.body, "UTF-8"))
      assert(delivery.properties.getType == "VehicleRegistrationCertificate")
      assert(response.systemId == "A2")
      assert(response.gantryId == "1")
      assert(response.activeCertificate == activeCertificate)

      val delivery2 = receiveOne(5 seconds).asInstanceOf[Delivery]
      val response2 = VehicleRegistrationSerialization.readCertificate(new String(delivery2.body, "UTF-8"))
      assert(delivery2.properties.getType == "VehicleRegistrationCertificate")
      assert(response2.systemId == "A2")
      assert(response2.gantryId == "3")
      assert(response2.activeCertificate == activeCertificate)

      stopConsumer(rrConsumer)
    }
  }
}
