/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle

import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import csc.akka.logging.DirectLogging
import csc.vehicle.message.{ Priority, SensorActivationMessage }
import image.InstructSensors
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import org.scalatest._

class InstructSensorsTest extends TestKit(ActorSystem("InstructSensorsTest")) with WordSpecLike with DirectLogging with MustMatchers with BeforeAndAfterAll {

  override def afterAll() {
    try {
      system.shutdown
    } catch {
      case e: Exception ⇒ log.error("COULD NOT SHUTDOWN ACTOR REGISTRY".format(e))
    }
  }

  "InstructSensors" must {
    "perform the update of a new version" in {
      val probe = TestProbe()
      val sensorInstructionsRef = TestActorRef(new InstructSensors(0L, probe.ref))
      val activeMap = Map[Long, Priority.Value](100L -> Priority.NONCRITICAL, 200L -> Priority.NONCRITICAL, 300L -> Priority.NONCRITICAL)
      val msg = new SensorActivationMessage(2L, activeMap)
      sensorInstructionsRef ! msg
      probe.expectMsg(activeMap)
    }

    "perform the update of a same version" in {
      val probe = TestProbe()
      val sensorInstructionsRef = TestActorRef(new InstructSensors(2L, probe.ref))
      val activeMap = Map[Long, Priority.Value](100L -> Priority.NONCRITICAL, 200L -> Priority.NONCRITICAL, 300L -> Priority.NONCRITICAL)
      val msg = new SensorActivationMessage(2L, activeMap)
      sensorInstructionsRef ! msg
      //shouldn't result in any actions
      probe.expectNoMsg()
    }

    "perform the update of an old version" in {
      val probe = TestProbe()
      val sensorInstructionsRef = TestActorRef(new InstructSensors(2L, probe.ref))

      val msg = new SensorActivationMessage(0L, Map[Long, Priority.Value](100L -> Priority.NONCRITICAL, 200L -> Priority.NONCRITICAL, 300L -> Priority.NONCRITICAL))
      sensorInstructionsRef ! msg
      //shouldn't result in any actions
      probe.expectNoMsg()
    }
  }
}
