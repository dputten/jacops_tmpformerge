/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle

import java.io.File

import csc.akka.logging.DirectLogging
import image.HonacManager
import org.junit.Test
import org.scalatest.junit.JUnitSuite
import testframe.Resources

class HonacManagerTest extends JUnitSuite with DirectLogging {

  @Test
  def testConstructor {
    try {
      val honac = new HonacManager(pathToFile = null)
      fail("expected: ArgumentNullException")
    } catch {
      case ae: IllegalArgumentException ⇒ assert(ae.getMessage == "requirement failed: Missing pathToFile", "wrong parameter received: " + ae.getMessage)
    }

    try {
      val honac = new HonacManager(pathToFile = "")
      fail("expected: ArgumentEmptyException")
    } catch {
      case ae: IllegalArgumentException ⇒ assert(ae.getMessage == "requirement failed: Empty pathToFile", "wrong parameter received: " + ae.getMessage)
    }

    try {
      val honac = new HonacManager(pathToFile = "something")
      fail("expected: Exception")
    } catch {
      case e: Exception ⇒ assert(e.getMessage == "Given path is not a file [something]", "wrong parameter received: " + e.getMessage)
    }

    try {
      val honac = new HonacManager(pathToFile = "/bin/ls")
    } catch {
      case e: Exception ⇒ assert(e.getMessage == "Given file must have extension [tar], received [None]", "wrong parameter received: " + e.getMessage)
    }
  }

  @Test
  def testUnpacking {
    val path = Resources.getResourceFile("csc/vehicle/tar_files/20081008_121127_00001_0000004.tar").getAbsolutePath
    val pathExpected = new File(path + ".unpacked")
    val honac = new HonacManager(pathToFile = path)
    honac.process

    var imageGreyScalePathExpected = (new File(pathExpected, "20081008_121127_00001_0000004_1.jpg")).getAbsolutePath
    var imageColorPathExpected = (new File(pathExpected, "20081008_121127_00001_0000004_2.jpg")).getAbsolutePath

    //assert(honac.eventTimestamp. == "2008-10-08","received path => " + honac.date)
    assert(honac.date == "2008-10-08", "received path => " + honac.date)
    assert(honac.time == "12:11:27", "received path => " + honac.time)
    assert(honac.longitude == 0.000000f, "received path => " + honac.longitude)
    assert(honac.latitude == 0.000000f, "received path => " + honac.latitude)
    assert(honac.imageGreyScale == imageGreyScalePathExpected, "received path => " + honac.imageGreyScale)
    assert(honac.imageColor == imageColorPathExpected, "received path => " + honac.imageColor)
    assert(honac.xml == "20081008_121127_00001_0000004.xml", "received path => " + honac.xml)
    assert(honac.basePath == (new File(path)).getAbsolutePath + ".unpacked", "received path => " + honac.basePath)
  }
}