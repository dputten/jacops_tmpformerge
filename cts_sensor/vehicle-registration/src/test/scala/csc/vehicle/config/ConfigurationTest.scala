package csc.vehicle.config

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

import java.util.concurrent.TimeUnit

import org.scalatest.MustMatchers

import scala.concurrent.duration.Duration
import scala.concurrent.duration._
import base.Mode
import csc.akka.logging.DirectLogging
import csc.amqp.ServicesConfiguration
import csc.vehicle.message.{ ImageVersion, VehicleImageType }
import csc.watchdog.{ ServiceConfig, WatchdogConfig }
import org.junit.Test
import org.scalatest.junit.JUnitSuite

class ConfigurationTest extends JUnitSuite with DirectLogging with MustMatchers {

  /*
  * test using no configuration file
  */
  @Test
  def testingFallbackConfig {

    val config = new Configuration(None)
    val cfg = config.globalConfig
    cfg.guards must be(Map())
    cfg.joinEventConfig.maxBufferLen must be(300)

    cfg.jpegStep.jpgOutputDir must be("jpg")
    cfg.jpegStep.jpgConfig must have size (7)
    val configuredVehicleImageTypes = cfg.jpegStep.jpgConfig collect { case params: JpegConvertParameters ⇒ params.imageType }
    configuredVehicleImageTypes must be(VehicleImageType.values.toSet)
    val overviewJpgParam = cfg.jpegStep.jpgConfig.find(_.imageType == VehicleImageType.Overview).get
    overviewJpgParam.conversionEnabled must be(true)

    cfg.openDoor.expression must be("""<\d*>(\S* \d{1,2} \d{1,2}:\d{1,2}:\d{1,2}) (\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b) INFO:DI 1 transition \((On|Off) -> (On|Off)\)""")
    //cfg.recognizeConfig.recognizeActorPath must be("akka://RecognizeProcessor@127.0.0.1:25552/user/recognize")
    val complList = cfg.registrationCompletion
    complList must have size (1)
    complList.head must be(new FileSystemCompletionConfig(("outputStore")))
    config.jmxService must be(None)

    cfg.imageConfig.triggerMaskConfig must be(Some(SimpleTriggerMaskConfig(50, 65, "/data/images/", 80)))
    cfg.doubleRegistrationFilterConfig must be(Some(DoubleRegistrationFilterConfig(30000, 100)))
    cfg.dualClockConfig must be(Some(DualClockConfig(100, 100, 1000, 60000)))

    cfg.gantryControlConfig must be(None)
    cfg.initialMode must be(Mode.Normal)

  }

  /*
  * using complete configuration file
  */
  @Test
  def testingFullConfig {
    val configFile = getClass.getClassLoader.getResource("csc/vehicle/config/testVR.conf").getPath
    val config = Configuration.createFromFile(configFile)

    val cfg = config.globalConfig
    cfg.guards must have size (1)
    cfg.guards.keySet.head must be("ARH")
    cfg.joinEventConfig.maxBufferLen must be(22)

    cfg.jpegStep.jpgOutputDir must be("jpgDir")
    cfg.jpegStep.jpgConfig must have size (7)
    val configuredVehicleImageTypes = cfg.jpegStep.jpgConfig collect { case params: JpegConvertParameters ⇒ params.imageType }
    configuredVehicleImageTypes must be(VehicleImageType.values.toSet)
    val overviewJpgParam = cfg.jpegStep.jpgConfig.find(_.imageType == VehicleImageType.Overview).get
    overviewJpgParam.conversionEnabled must be(true)

    cfg.openDoor.expression must be("""Empty""")
    //cfg.recognizeConfig.recognizeActorPath must be("akka://RecognizeProcessor@localhost:2552/user/recognize")
    val complList = cfg.registrationCompletion
    complList must have size (1)
    complList.head must be(new HBaseCompletionConfig("Host123:2181", "vehicleTable"))
    config.jmxService.isDefined must be(true)
    config.jmxService.get.jmxAccessFile must be("config/myremote.access")

    // TEMPORARY
    val iRoseDataConfig = cfg.iRose.get
    iRoseDataConfig must have size (2)
    println("config = %s".format(iRoseDataConfig))
    iRoseDataConfig.get("lane3214-1-RD") match {
      case Some(systemData) ⇒
        systemData.interval must be(4002)
        systemData.loopDistance must be(1502)
      case None ⇒
        fail("Key lane3214-1-RD should exist")
    }
    iRoseDataConfig.get("lane3214-1-RA") match {
      case Some(systemData) ⇒
        systemData.interval must be(4000)
        systemData.loopDistance must be(1500)
      case None ⇒
        fail("Key lane3214-1-RA should exist")
    }
    cfg.initialMode must be(Mode.Normal)
  }

  /*
  * using partial configuration file
  */
  @Test
  def testingPartialConfig {
    val configFile = getClass.getClassLoader.getResource("csc/vehicle/config/testPartVR.conf").getPath
    val config = Configuration.createFromFile(configFile)
    val cfg = config.globalConfig
    cfg.joinEventConfig.maxBufferLen must be(300)
    //cfg.recognizeConfig.recognizeActorPath must be("akka://RecognizeProcessor@localhost:2552/user/recognize")
  }

  /**
   * Test for replacing values, $(key) is replaced by the value of key.
   * $(section.subsection.key) is replaced by the value of section.subsection.key.
   * replacement is done recursively, so if a key refers to another $(...) it will replace that as well.
   * You need to use the replace function in Configuration, it only works for now with strings.
   */
  @Test
  def testDeepReplace {
    val configFile = getClass.getClassLoader.getResource("csc/vehicle/config/testReplace.conf").getPath
    val config = Configuration.createFromFile(configFile)
    val cfg = config.globalConfig
    //test string
    //cfg.recognizeConfig.recognizeActorPath must be("akka://RecognizeProcessor@localhost:2552/user/recognize")
    //test integer
    cfg.joinEventConfig.maxBufferLen must be(20)
  }

  /**
   * Test with a config file that does use the enabled key for jpg conversions.
   */
  @Test
  def testJpgConversionConfigWithEnabled {
    val configFile = getClass.getClassLoader.getResource("csc/vehicle/config/testJpgConversionsWithEnabled.conf").getPath
    val config = Configuration.createFromFile(configFile)
    val cfg = config.globalConfig

    cfg.jpegStep.jpgOutputDir must be("with")
    val overviewJpgParam = cfg.jpegStep.jpgConfig.find(_.imageType == VehicleImageType.Overview).get
    overviewJpgParam.conversionEnabled must be(false)
    overviewJpgParam.jpg_width must be(11)
    val licenseJpgParam = cfg.jpegStep.jpgConfig.find(_.imageType == VehicleImageType.License).get
    licenseJpgParam.conversionEnabled must be(true)
    licenseJpgParam.jpg_width must be(21)
  }

  /**
   * Test with a config file that does NOT use the enabled key for jpg conversions. (pre eg100)
   * Enabled should default to true.
   */
  @Test
  def testJpgConversionConfigWithoutEnabled {
    val configFile = getClass.getClassLoader.getResource("csc/vehicle/config/testJpgConversionsWithoutEnabled.conf").getPath
    val config = Configuration.createFromFile(configFile)
    val cfg = config.globalConfig

    cfg.jpegStep.jpgOutputDir must be("without")
    val overviewJpgParam = cfg.jpegStep.jpgConfig.find(_.imageType == VehicleImageType.Overview).get
    overviewJpgParam.conversionEnabled must be(true)
    overviewJpgParam.jpg_width must be(11)
    val licenseJpgParam = cfg.jpegStep.jpgConfig.find(_.imageType == VehicleImageType.License).get
    licenseJpgParam.conversionEnabled must be(true)
    licenseJpgParam.jpg_width must be(21)
  }

  /**
   * Test with a config file that does have an exif entry. (eg100 onwards)
   */
  @Test
  def testConfigWithExif(): Unit = {
    val configFile = getClass.getClassLoader.getResource("csc/vehicle/config/testConfigWithExif.conf").getPath
    val config = Configuration.createFromFile(configFile)
    val cfg = config.globalConfig

    cfg.exif match {
      case Some(exifConfig) ⇒
        exifConfig.dirExifImage must be("exifDir")
        exifConfig.writeJsonToComment must be(true)
        exifConfig.targetConfigs must have size (4)
        // Overview
        val expectedConfig = Set(
          new ExifTargetConfig(targetImageType = VehicleImageType.RedLight, targetImageVersion = ImageVersion.Decorated),
          new ExifTargetConfig(targetImageType = VehicleImageType.OverviewRedLight, targetImageVersion = ImageVersion.Decorated),
          new ExifTargetConfig(targetImageType = VehicleImageType.OverviewSpeed, targetImageVersion = ImageVersion.Decorated),
          new ExifTargetConfig(targetImageType = VehicleImageType.MeasureMethod2Speed, targetImageVersion = ImageVersion.Decorated))
        exifConfig.targetConfigs must be(expectedConfig)

        exifConfig.getTargetImageTypes.toSet must be(Set(VehicleImageType.RedLight, VehicleImageType.OverviewRedLight, VehicleImageType.OverviewSpeed, VehicleImageType.MeasureMethod2Speed))
        exifConfig.getTargetConfig(VehicleImageType.RedLight) must be(Some(new ExifTargetConfig(targetImageType = VehicleImageType.RedLight, targetImageVersion = ImageVersion.Decorated)))
        exifConfig.getTargetConfig(VehicleImageType.License) must be(None)
      case None ⇒ fail("Config should contain exif section")
    }
  }

  /**
   * Test with a config file that does NOT have an exif entry. (pre eg100)
   */
  @Test
  def testConfigWithoutExif(): Unit = {
    val configFile = getClass.getClassLoader.getResource("csc/vehicle/config/testConfigWithoutExif.conf").getPath
    val config = Configuration.createFromFile(configFile)
    val cfg = config.globalConfig

    cfg.exif must be(None)
  }

  /**
   * Test with a config file that has AMQP completion
   */
  @Test
  def testConfigWithAmqp(): Unit = {
    val configFile = getClass.getClassLoader.getResource("csc/vehicle/config/testAmqp.conf").getPath
    val config = Configuration.createFromFile(configFile)
    val cfg = config.globalConfig

    val amqpConfig = cfg.amqpConfig
    amqpConfig.actorName must be("vehicle-registration-actor")
    amqpConfig.amqpUri must be("amqp://guest:guest@localhost:5672")
    amqpConfig.reconnectDelay must be(5000 millis)

    val amqpCertCheck = cfg.amqpCertificateCheckConfig
    if (amqpCertCheck.isDefined) {
      val config = amqpCertCheck.get
      config.certificateComponentName must be("vehicleregistration")
      config.registrationReceiverEndpoint must be("")
      config.registrationReceiverRouteKey must be("vehicleregistrationq")
    } else fail("AMQP certificate check config is undefined")

    cfg.registrationCompletion.foreach(compConfig ⇒ {
      compConfig match {
        case amqp: AmqpCompletionConfig ⇒ {
          amqp.imageStoreEndpoint must be("")
          amqp.imageStoreRouteKey must be("imagestoreq")
          amqp.registrationReceiverEndpoint must be("")
          amqp.registrationReceiverRouteKey must be("vehicleregistrationq")
          amqp.heartbeatRate must be(0.second)
          amqp.delayAlpha must be(0.875)
          amqp.delayMargin must be(0)
        }
        case _: Any ⇒ fail("Only AMQP completion config should exist")
      }
    })
  }

  @Test
  def testJacopsConfig: Unit = {
    val configFile = getClass.getClassLoader.getResource("csc/vehicle/config/jacopsConfig.conf").getPath
    val config = Configuration.createFromFile(configFile)
    val cfg = config.globalConfig

    cfg.gantryControlConfig must be(Some(GantryControlConfig(ServicesConfiguration("gantryControl", "gantry-out-exchange", "", 1))))
    cfg.initialMode must be(Mode.Test)

    cfg.recognizerTransport must be(RecognizerQueueConfig("gantry-recognition-out", "gantry-internal", "", 1, FiniteDuration(4, TimeUnit.HOURS)))

    cfg.watchdogConfig match {
      case None ⇒ fail("watchdogConfig should be present and enabled")
      case Some(c) ⇒
        c.services.size must be(3)
        //println(c.services)
        verifyServiceConfig(c, "recognize-processor", "30 seconds", 2)
        verifyServiceConfig(c, "vehicle-classifier", "30 seconds", 2)
        verifyServiceConfig(c, "dummy", "1 second", 3)
    }
  }

  def verifyServiceConfig(config: WatchdogConfig, name: String, timeoutStr: String, tolerance: Int): Unit = {
    val duration = Duration(timeoutStr)
    val fd = FiniteDuration(duration.toMillis, TimeUnit.MILLISECONDS)
    val expected = ServiceConfig(name, s"/etc/init.d/$name", fd, tolerance)
    config.configOf(name) must be(Some(expected))

  }

}
