/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.config

import java.text.SimpleDateFormat
import java.util.Date

import akka.actor.ActorSystem
import akka.agent.Agent
import akka.testkit.TestKit

import scala.concurrent.Await
import scala.concurrent.duration._
import csc.gantry.config._
import csc.vehicle.message.Lane
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, WordSpec }

class DynamicLaneConfigurationTest extends TestKit(ActorSystem("DynamicLaneConfigurationTest"))
  with WordSpecLike with MustMatchers with BeforeAndAfterAll {

  override def afterAll() {
    system.shutdown()
  }

  def await(agent: Agent[_], duration: Duration) = Await.ready(agent.future(), duration)

  "DynamicLaneConfiguration" must {
    "return configuration initial" in {
      val mock = new MockLaneConfiguration()
      val input = createLaneIRose()
      mock.updateConfig(List(input))

      val dynamic = new DynamicLaneConfiguration(mock, system)
      val listLanes = dynamic.getLanesForRoute("A4")
      listLanes.size must be(1)
      val lane = listLanes.head
      lane.lane must be(input.lane)
      lane.camera must be(input.camera)
      lane.imageMask.get.vertices must be(input.imageMask.get.vertices)
    }
    "return same but updated configuration" in {
      val mock = new MockLaneConfiguration()
      val input = createLaneIRose()
      mock.updateConfig(List(input))

      val dynamic = new DynamicLaneConfiguration(mock, system)
      val listLanes = dynamic.getLanesForRoute("A4")
      //update
      dynamic.laneAgent.send(Map(input.lane.laneId -> input))
      await(dynamic.laneAgent, 1.second)

      listLanes.size must be(1)
      val lane = listLanes.head
      lane.lane must be(input.lane)
      lane.camera must be(input.camera)
      lane.imageMask.get.vertices must be(input.imageMask.get.vertices)
    }
    "return updated configuration" in {
      val mock = new MockLaneConfiguration()
      val inputStart = createLaneIRose()
      mock.updateConfig(List(inputStart))

      val dynamic = new DynamicLaneConfiguration(mock, system)
      val listLanes = dynamic.getLanesForRoute("A4")
      //update
      val imageMask2 = new ImageMaskImpl(List(new ImageMaskVertex(0, 0), new ImageMaskVertex(100, 0), new ImageMaskVertex(100, 100), new ImageMaskVertex(0, 100)))
      val input = inputStart.copy(imageMask = Some(imageMask2))
      dynamic.laneAgent.send(Map(input.lane.laneId -> input))
      await(dynamic.laneAgent, 1.second)

      listLanes.size must be(1)
      val lane = listLanes.head
      lane.lane must be(input.lane)
      lane.camera must be(input.camera)
      lane.imageMask.get.vertices must be(input.imageMask.get.vertices)
    }
    "return updated configuration when having an old reference" in {
      val mock = new MockLaneConfiguration()
      val inputStart = createLaneIRose()
      mock.updateConfig(List(inputStart))

      val dynamic = new DynamicLaneConfiguration(mock, system)
      val listLanes = dynamic.getLanesForRoute("A4")
      listLanes.size must be(1)
      val lane = listLanes.head
      val maskRef = lane.imageMask.get

      //update
      val imageMask2 = new ImageMaskImpl(List(new ImageMaskVertex(0, 0), new ImageMaskVertex(100, 0), new ImageMaskVertex(100, 100), new ImageMaskVertex(0, 100)))
      val input = inputStart.copy(imageMask = Some(imageMask2))
      dynamic.laneAgent.send(Map(input.lane.laneId -> input))
      await(dynamic.laneAgent, 1.second)

      //test if the changed values are returned

      maskRef.vertices must be(input.imageMask.get.vertices)
    }
  }

  def createLaneIRose(): IRoseSensorConfigImpl = {
    val lane = new Lane(
      laneId = "A4-44.5HM-Lane3",
      name = "Lane3",
      gantry = "44.5HM",
      system = "A4",
      sensorGPS_latitude = 0,
      sensorGPS_longitude = 0)
    new IRoseSensorConfigImpl(
      lane = lane,
      camera = new CameraConfig("L43", cameraHost = "localhost", cameraPort = 1400, maxTriggerRetries = 10, timeDisconnected = 20000),
      imageMask = Some(new ImageMaskImpl(List(new ImageMaskVertex(10, 10), new ImageMaskVertex(20, 10), new ImageMaskVertex(20, 20), new ImageMaskVertex(10, 20)))),
      recognizeOptions = Map(),
      seqConfig = new LaneSeqConfig(Some(5), Some("PCB")))
  }

  def toDate(time: String): Date = {
    if (time == "24:00:00") (new SimpleDateFormat("HH:mm:ss.SSS")).parse("23:59:59.999")
    else (new SimpleDateFormat("HH:mm:ss")).parse(time)
  }
}

class MockLaneConfiguration extends DefaultLaneConfiguration {
  private var allLanes = List[LaneConfig]()

  override def lanes: List[LaneConfig] = allLanes

  def updateConfig(lanes: List[LaneConfig]) {
    allLanes = lanes
  }

}