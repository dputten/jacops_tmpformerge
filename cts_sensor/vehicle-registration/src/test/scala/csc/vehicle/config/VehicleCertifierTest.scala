/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.config

import java.text.SimpleDateFormat

import csc.akka.logging.DirectLogging
import csc.config.Path
import csc.curator.CuratorTestServer
import csc.curator.utils.{ Curator, CuratorToolsImpl, Versioned }
import csc.gantry.config.{ ComponentCertificate, TypeCertificate, _ }
import org.scalatest.WordSpec
import org.scalatest._

class VehicleCertifierTest extends WordSpec with MustMatchers with CuratorTestServer with DirectLogging {
  var curator: Curator = _
  var verifier: VehicleCertifier = _
  val pathToEvents = Path("/ctes/systems/eg033/events")

  val certInstallTime = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss.SSS").parse("01/01/2012 00:00:00.00").getTime

  override def beforeEach() {
    super.beforeEach()

    // compose a system configuration with the required serializer, in this case the serializer needs implicit formats
    curator = new CuratorToolsImpl(clientScope, log)

    verifier = new VehicleCertifier(curator)
  }

  override def afterEach() {
    super.afterEach()
  }

  "The VehicleCertifier" must {

    "accept no Type Certificates" in {
      verifier.processCertificate(new ActiveCertificate(System.currentTimeMillis(), ComponentCertificate("name1", "123")))
    }

    "accept valid Active Certificate" in {
      if (curator.exists(pathToEvents)) {
        curator.delete(pathToEvents)
      }

      val cert = ComponentCertificate("name1", "123")
      putTypeCertificate("eg033", TypeCertificate("TC12345", 0, List(cert)), certInstallTime)
      curator.exists(Path("/ctes/systems/eg033/activeCertificates")) must be(false)
      val active = new ActiveCertificate(System.currentTimeMillis(), cert)

      verifier.processCertificate(active)

      curator.exists(Path("/ctes/systems/eg033/activeCertificates/name1")) must be(true)
      val stored = curator.getVersioned[ActiveCertificate](Path("/ctes/systems/eg033/activeCertificates/name1"))
      stored.get must be(Versioned(active, 0))

      curator.exists(pathToEvents) must be(false)
    }

    "find and accept valid Active Certificate" in {
      if (curator.exists(pathToEvents)) {
        curator.delete(pathToEvents)
      }
      val cert = ComponentCertificate("name1", "123")
      val second = ComponentCertificate("name2", "22222222222222222222")
      putTypeCertificate("eg033", TypeCertificate("TC12345", 0, List(cert, second)), certInstallTime)
      val active = new ActiveCertificate(System.currentTimeMillis(), cert)
      verifier.processCertificate(active)

      curator.exists(pathToEvents) must be(false)

      curator.exists(Path("/ctes/systems/eg033/activeCertificates/name1")) must be(true)
      curator.getVersioned[ActiveCertificate](Path("/ctes/systems/eg033/activeCertificates/name1")) match {
        case Some(Versioned(c, _)) ⇒ c must be(active)
        case None                  ⇒ fail("Certificate must be stored")
      }
      //the second
      verifier.processCertificate(new ActiveCertificate(System.currentTimeMillis(), second))
      curator.exists(pathToEvents) must be(false)
    }

    "reject invalid Active Certificate" in {
      if (curator.exists(pathToEvents)) {
        curator.delete(pathToEvents)
      }
      val cert = ComponentCertificate("name1", "123")
      putTypeCertificate("eg033", TypeCertificate("TC12345", 0, List(cert)), certInstallTime)
      val active = new ActiveCertificate(System.currentTimeMillis(), ComponentCertificate("name1", "123-NO-MATCH"))
      verifier.processCertificate(active)

      curator.exists(pathToEvents) must be(true)

      curator.exists(Path("/ctes/systems/eg033/activeCertificates/name1")) must be(true)
      curator.getVersioned[ActiveCertificate](Path("/ctes/systems/eg033/activeCertificates/name1")) match {
        case Some(Versioned(c, _)) ⇒ c must be(active)
        case None                  ⇒ fail("Certificate must be stored")
      }
    }

    "store unchecked Active Certificate without valid Type Certificate" in {
      if (curator.exists(pathToEvents)) {
        curator.deleteRecursive(pathToEvents)
      }
      val cert = ComponentCertificate("name1", "123")
      putTypeCertificate("eg033", TypeCertificate("TC12345", 0, List(cert)), certInstallTime)
      val active = new ActiveCertificate(System.currentTimeMillis(), ComponentCertificate("no-name", "777"))
      verifier.processCertificate(active)

      //when component isn't part of type certificate means do not check
      // and only create active certificate
      curator.exists(pathToEvents) must be(false)

      curator.exists(Path("/ctes/systems/eg033/activeCertificates/no-name")) must be(true)
      curator.getVersioned[ActiveCertificate](Path("/ctes/systems/eg033/activeCertificates/no-name")) match {
        case Some(Versioned(c, _)) ⇒ c must be(active)
        case None                  ⇒ fail("Certificate must be stored")
      }
    }

  }

  private def putTypeCertificate(systemId: String, certificate: TypeCertificate, timeFrom: Long) {

    val pathInst = Path(CertificateService.getInstallCertificatePath(systemId)) / timeFrom.toString
    val inst = new MeasurementMethodInstallation(typeId = timeFrom.toString, timeFrom = timeFrom)

    curator.getVersioned[MeasurementMethodInstallation](pathInst) match {
      case Some(Versioned(_, v)) ⇒ {
        curator.set(pathInst, inst, v)
      }
      case None ⇒
        if (!curator.exists(pathInst.parent)) {
          curator.createEmptyPath(pathInst.parent)
        }
        curator.put(pathInst, inst)
    }

    val global = new MeasurementMethodType(typeDesignation = "XX123",
      category = "A",
      unitSpeed = "km/h",
      unitRedLight = "s",
      unitLength = "m",
      restrictiveConditions = "",
      displayRange = "20-250 km/h",
      temperatureRange = "temperatuurbereik -10 to 50 graden Celsius",
      permissibleError = "toelatbare fout 3%",
      typeCertificate = certificate)
    val pathGlobal = Path(CertificateService.getCertificateMeasurementMethodTypePath(inst.typeId))

    curator.getVersioned[MeasurementMethodType](pathGlobal) match {
      case Some(Versioned(_, v)) ⇒ {
        curator.set(pathGlobal, global, v)
      }
      case None ⇒
        if (!curator.exists(pathGlobal.parent)) {
          curator.createEmptyPath(pathGlobal.parent)
        }
        curator.put(pathGlobal, global)
    }
  }

}