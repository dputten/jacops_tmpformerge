package testbase

//copyright CSC 2010

import admin.BeanRegistry
import javax.management.ObjectName
import collection.mutable.HashMap
import java.lang.IllegalStateException

/**
 * Test implementation for JMX registration
 */
class TestBeanRegistry extends BeanRegistry {
  val register = HashMap[ObjectName, AnyRef]()

  def unregisterBean(name: ObjectName) = {
    register.get(name) match {
      case None               ⇒ throw new IllegalStateException("ObjectName " + name + " not registered ")
      case Some(existingBean) ⇒ register.remove(name)
    }
  }

  def registerBean(name: ObjectName, bean: AnyRef) = {
    register.get(name) match {
      case None               ⇒ register.put(name, bean)
      case Some(existingBean) ⇒ throw new IllegalStateException("ObjectName " + name + " already registered")
    }
  }

  def getBean(name: ObjectName): AnyRef = {
    register.get(name) match {
      case None               ⇒ throw new IllegalStateException("ObjectName " + name + " not registered ")
      case Some(bean: AnyRef) ⇒ bean
    }
  }

  def queryBean(query: String): List[AnyRef] = {
    val queriedList = for ((name, bean) ← register if (name.toString.startsWith(query))) yield bean
    queriedList.toList
  }

  def getRegisteredBeans(): List[AnyRef] = {
    register.values.toList
  }

  def clear() = {
    register.clear()
  }
}