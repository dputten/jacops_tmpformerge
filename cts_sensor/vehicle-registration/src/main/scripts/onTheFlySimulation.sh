AKKA_HOME="$(cd "$(cd "$(dirname "$0")"; pwd -P)"/..; pwd)"
AKKA_CLASSPATH="$AKKA_HOME/lib/*:$AKKA_HOME/deploy/*:$AKKA_HOME/config/tools"

java -cp $AKKA_CLASSPATH csc.vehicle.tools.vehicle.OnTheFlyVehicleSimulator "$@"
