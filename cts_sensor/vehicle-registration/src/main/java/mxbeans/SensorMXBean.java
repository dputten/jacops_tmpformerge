package mxbeans;

/**
 * Defines the data presented by the sensor bean when using jmx
 * Is implemented in JAVA, because setters in Scala are
 * implemented using Objects, which aren't supported by JMX
 */
public interface SensorMXBean {
    void resetCounters();
    String getSensor();
    //last received data
    String getLastReceivedEventTime();
    String getLastReceivedXML();
    String getLastReceivedJPG();
    //performance XML
    long getProcessTimeXMLNr();
    long getProcessTimeXMLNrLate();
    float getProcessTimeXMLNrPerc();
    long getProcessTimeXMLLast();
    long getProcessTimeXMLSum();
    long getProcessTimeXMLAvg();
    long getProcessTimeXMLPeriodAvg();
    //performance JPG
    long getProcessTimeJPGNr();
    long getProcessTimeJPGNrLate();
    float getProcessTimeJPGNrPerc();
    long getProcessTimeJPGLast();
    long getProcessTimeJPGSum();
    long getProcessTimeJPGAvg();
    long getProcessTimeJPGPeriodAvg();
    //recognition data
    long getRecognizedNr();
    long getRecognizedNrFailed();
    float getRecognizedNrPerc();
    int getRecognizedConfidenceLast();
    long getRecognizedConfidenceSum();
    int getRecognizedConfidenceAvg();

    //IP monitor
    String getModifiedLastMonitorTime();
    long getModifiedLastMonitorTimeSeq();
    String getLastMonitorRadar();
    long getLastMonitorRadarSeq();
    String getLastMonitorRadarMsg();
    String getLastMonitorImage();
    long getLastMonitorImageSeq();
}
