package mxbeans;

/**
 * TODO think about how to do use this in the future.
 * TODO 06022012 RR->RB: we need to talk about JMX and how to reuse this. Is it maybe Amigo specific? (at least, control and location)
 * Defines the data presented by the location bean when using jmx
 * Is implemented in JAVA, because setters in Scala are
 * implemented using Objects, which aren't supported by JMX
 */
public interface LocationMXBean {
      /**
       * Get the location
       */
      String getLocation();

      /**
       * get the time when this location is last updated
       */
      String getLastUpdateTime();
      /**
       * get the sequenceNumber which is incremented when this location is updated
       */
      long getLastUpdateTimeSeq();

      /**
       * Get the duration of sending the update message to the receiver in msec
       */
      int getSendDuration();

      /**
       * Get the application version running on the location
       */
      String getApplicationVersion();

      /**
       * Get the start time of the application on the location
       */
      String getApplicationStartTime();
      /**
       * Get the sequence number of the start time modifications
       */
      long getApplicationStartTimeSeq();

      /**
       * get the modification time of the change of the door status
       */
      String getDoorModifiedDate();

      /**
       * get The door status
       */
      String getDoorStatus();
}
