package csc.vehicle

import akka.actor.{ ActorRef, ActorLogging, Actor }
import base.Timing
import csc.vehicle.config.DoubleRegistrationFilterConfig
import csc.vehicle.message.VehicleRegistrationMessage
import csc.vehicle.registration.RegistrationCleanup

/**
 *
 * Filter messages with the same license within a defined interval.
 * This actor will centralize the double registration management for a certain scope.
 * Its not itself a RecipientList, but will forward each accepted registration to every associated recipient (passed in RegistrationReady msg).
 * If the registration is a confirmed duplicate, it will send it to cleanup actor.
 *
 * Created by carlos on 16/01/16.
 */
class DoubleTriggerMonitorActor(cfg: DoubleRegistrationFilterConfig)
  extends Actor with ActorLogging with ConfirmedRegistrationBuffer with Timing {

  override def timeInterval: Int = cfg.millisInterval

  override def maxBufferSize: Int = cfg.bufferSize

  override def receive: Receive = {
    case msg: RegistrationReady ⇒ handle(msg)
  }

  def handle(msg: RegistrationReady): Unit = {
    val reg = msg.reg

    performanceLogInterval(log, 41, 42, "Filter double trigger events", reg.id,
      {
        if (shouldBlockRegistration(reg)) {
          log.info("Found double trigger event eventId=%s for license %s. Sending to cleanup".format(reg.id, reg.licenseData.license))
          sendToCleanup(reg)
        } else {
          for (actorRef ← msg.recipients) {
            actorRef ! reg //forwarding message to the indicated recipients
          }
        }
      })
  }

  def sendToCleanup(msg: VehicleRegistrationMessage): Unit = {
    context.system.eventStream.publish(RegistrationCleanup(msg, "duplicate"))
  }
}

case class RegistrationReady(reg: VehicleRegistrationMessage, recipients: Set[ActorRef])
