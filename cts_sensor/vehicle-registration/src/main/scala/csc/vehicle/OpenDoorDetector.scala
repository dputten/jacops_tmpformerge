package image
//copyright CSC 2010 - 2011

import akka.camel.{ Consumer, CamelMessage }
import base.{ RecipientList, Timing }
import java.util.Date
import akka.actor.{ ActorLogging, ActorRef }
import csc.vehicle.message.DigitalSwitchDetectedMessage

/**
 * Detects via messages if a door has been opened
 * @param expression regular expression
 * @param uri Apache Camel uri needed for configuration
 * @param doorOpenSignal what signal is given when a door is open (On or Off)
 * @param description of location
 * @param nextActors ActorRefs that will receive an alert when a door has been opened
 */
class OpenDoorDetector(expression: ⇒ String, uri: ⇒ String, host: ⇒ String, port: ⇒ Int, doorOpenSignal: ⇒ String, description: ⇒ String, nextActors: Set[ActorRef]) extends RecipientList(nextActors) with Consumer with Timing with ActorLogging {
  def endpointUri = uri
  val openDoorExpression = expression.r

  def receive = {
    case msg: CamelMessage ⇒ {
      val txt = msg.bodyAs[String]
      txt match {
        case openDoorExpression(date, ip, prevState, currState) ⇒ {
          log.info("Door Message received date=[%s], ip=%s, prevState=[%s], currState=[%s]".format(date, ip, prevState, currState))
          val msg = new DigitalSwitchDetectedMessage(dateFromDetector = date,
            ipFromDetector = ip,
            date = new Date,
            imageServerHost = host,
            imageServerPort = port,
            originalMessage = txt,
            doorIsOpen = (currState == doorOpenSignal),
            description = description)
          sendToRecipients(msg)
        }
        case _ ⇒ log.info("No match found, message received [%s]".format(txt))
      }
    }
  }
}