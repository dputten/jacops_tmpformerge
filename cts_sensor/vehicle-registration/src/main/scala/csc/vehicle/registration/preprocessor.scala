package csc.vehicle.registration

import java.io.File

import base._
import csc.akka.logging.DirectLogging
import csc.gantry.config.MultiLaneCamera
import csc.timeregistration.registration.{ ImageMetaDataReader, ImageInfo }
import csc.vehicle.config.{ DualClockConfig, VehicleRegistrationConfiguration }
import csc.vehicle.message.TriggerArea
import csc.vehicle.message.TriggerArea._
import csc.vehicle.message._
import csc.vehicle.registration.images.{ JaiMakerNoteHandler, ReadWriteExifUtil, TriggerSource }
import imagefunctions.ImageFunctionsException
import org.apache.commons.io.FileUtils

import scala.collection.immutable.Queue
import scala.collection.mutable.HashMap

case class IncomingFile(path: String, time: Long) {
  lazy val file = new File(path)
}

case object FileHandled

case class RegistrationCandidate(file: File, time: TimeInfo, initialStatus: RegistrationStatus, trigger: Option[ImageTriggerData])

trait LaneSensorState {

  /**
   * List of the sensors using the imageDir as a key.
   */
  val knownLanes = new HashMap[String, RelaySensorConfig]()

  def getFolder(config: RelaySensorConfig): File = new File(config.relaySensor.relayURI)

  /**
   * Find the lane for requested file
   */
  def findLane(imageDir: String): Option[RelaySensorConfig] = {
    knownLanes.get(imageDir)
  }

  def findLaneById(laneId: String): Option[RelaySensorConfig] = knownLanes.values.find(_.lane.laneId == laneId)

}

abstract class ImagePreProcessor(val name: String)
  extends Function2[IncomingFile, LaneSensorState, List[RegistrationCandidate]]

object ImagePreProcessor {

  def apply(config: VehicleRegistrationConfiguration): ImagePreProcessor = config.isMultilaneCameraSystem match {
    case true ⇒ new MultiLaneCameraPreProcessor(config.multiLaneCameras, config.metaImageSectionGuard,
      Some(config.metadataReader), config.dualClockConfig)
    case false ⇒ singleLaneCameraPreProcessor
  }

  val singleLaneCameraPreProcessor: ImagePreProcessor = new ImagePreProcessor("SingleLaneCameraPreProcessor") {
    override def apply(incoming: IncomingFile, state: LaneSensorState) =
      List(RegistrationCandidate(incoming.file, TimeInfo(incoming.time, None), RegistrationStatus.initial, None))
  }

  val clockDiffExceededStatus = RegistrationStatus.initial.set(RegistrationStatusFlag.clockDifference)

  val majorClockDiffStatus = clockDiffExceededStatus.set(RegistrationStatusFlag.criticalClockDifference) //both flags

  val incomingDirectionStatus = RegistrationStatus.initial.set(RegistrationStatusFlag.incomingDirection)

  val testMode = RegistrationStatus.initial.set(RegistrationStatusFlag.testMode)
}

import ImagePreProcessor._

class MultiLaneCameraPreProcessor(cameras: List[MultiLaneCamera],
                                  metadataReaderGuard: Option[FavorCriticalProcess] = None,
                                  metadataReader: Option[ImageMetaDataReader] = None,
                                  dualClockConfig: Option[DualClockConfig] = None)
  extends ImagePreProcessor("MultiLaneCameraPreProcessor") with DirectLogging {

  val cameraMap = cameras map { e ⇒ e.config.relayURI -> e } toMap
  val avgBuffersMap: Map[String, MovingAverageBuffer] = dualClockConfig match {
    case None      ⇒ Map.empty
    case Some(cfg) ⇒ cameras map { e ⇒ e.config.cameraId -> createBuffer(cfg) } toMap
  }

  def clockDiffThreshold: Long = dualClockConfig map (_.timeDiffThreshold) getOrElse (0)

  def criticalDiffThreshold: Long = dualClockConfig map (_.criticalTimeDiffThreshold) getOrElse (0)

  cameras.foreach { c ⇒
    val folder = new File(c.config.relayURI)
    folder.mkdirs() //this will be needed if (for example) we are using the simulator
  }

  private def createBuffer(cfg: DualClockConfig): MovingAverageBuffer =
    new MovingAverageBuffer(cfg.maxSampleBufferSize, cfg.criticalTimeDiffThreshold)

  override def apply(incoming: IncomingFile, state: LaneSensorState): List[RegistrationCandidate] = {
    val folder = incoming.file.getParent
    cameraMap.get(folder) match {
      case None ⇒ Nil
      case Some(config) ⇒ triggerSource(incoming.file) match {
        case None ⇒
          log.warning("No JAI triggerSource found in image {}", incoming.path)
          Nil
        case Some(triggerSource) ⇒ preProcess(incoming, config, triggerSource, state)
      }
    }
  }
  private def getClockStatus(cameraId: String, timeInfo: TimeInfo): RegistrationStatus = {
    log.debug("Getting Clock RegistrationStatus for {}", timeInfo)
    val status: Option[RegistrationStatus] = (timeInfo.diff, avgBuffersMap.get(cameraId)) match {
      case (Some(diff), Some(buf)) ⇒ buf.tryAdd(diff) match {
        case (_, None) ⇒
          log.warning("Critical clock difference {}. Limit is {}", diff, buf.maxValue)
          Some(majorClockDiffStatus) //major difference
        case (0, _) ⇒
          log.info("First clock difference sample for {} is {}", timeInfo, diff)
          None //first sample is always tolerable
        case (_, Some(avg)) ⇒ {
          log.info("Clock difference for {} is {}, average was {}, threshold is {}", timeInfo, diff, avg, clockDiffThreshold)
          math.abs(diff - avg) match {
            case x if (x > clockDiffThreshold) ⇒ Some(clockDiffExceededStatus) //exceeded threshold
            case _                             ⇒ None //tolerable difference
          }
        }
      }
      case (d, b) ⇒
        log.debug("time difference is {} avgBuffer is {}", d, b)
        None //unable to get required components
    }
    status.getOrElse(RegistrationStatus.initial) //falls back to initial status (0)
  }

  private def getIncomingDirectionStatus(config: RelaySensorConfig): RegistrationStatus = {
    if (config.lane.isIncoming) incomingDirectionStatus else RegistrationStatus.initial
  }

  /**
   * @param incoming
   * @return Some(incoming.time), or None if dual clock is enabled and the image was considered too old to be processed
   */
  def getFileSystemTime(incoming: IncomingFile): Option[Long] = {
    dualClockConfig match {
      case None ⇒ Some(incoming.time)
      case Some(cfg) ⇒
        val currentTime = System.currentTimeMillis()
        val diff = currentTime - incoming.time
        diff match {
          case x if (x < cfg.maxPhotoAge) ⇒ Some(incoming.time)
          case x ⇒
            log.warning("Image {} receptionTime {} too instant from the current time {}", incoming.path, incoming.time, currentTime)
            None
        }
    }
  }

  def preProcess(incoming: IncomingFile,
                 camera: MultiLaneCamera,
                 triggerSource: TriggerSource,
                 state: LaneSensorState): List[RegistrationCandidate] = {

    getFileSystemTime(incoming) match {
      case None ⇒
        log.warning("Image {} was considered too old to be processed at {}. Discarding", incoming.path, System.currentTimeMillis())
        Nil
      case Some(systemTime) ⇒ {
        val timeInfo = TimeInfo(systemTime, getExifImageTime(incoming.file))
        val clockStatus = getClockStatus(camera.config.cameraId, timeInfo)

        def fileInfoFor(triggerArea: TriggerArea): Option[RegistrationCandidate] = {
          val relayConfig = relayConfigFor(camera, state, triggerSource, triggerArea)
          relayConfig.map { relayConfig ⇒
            val file = imageFor(relayConfig, state, incoming.file, triggerArea)
            val status = clockStatus.merge(getIncomingDirectionStatus(relayConfig))
            RegistrationCandidate(file, timeInfo, status, Some(ImageTriggerData(triggerArea, triggerSource.isOneSided)))
          }
        }

        val lCandidate: Option[RegistrationCandidate] = fileInfoFor(TriggerArea.LEFT)
        val rCandidate: Option[RegistrationCandidate] = fileInfoFor(TriggerArea.RIGHT)

        val result = Nil ++ lCandidate ++ rCandidate

        if (result.isEmpty) {
          log.warning("No resulting candidates preProcessing {}", incoming.path)
          log.debug("camera = {}, triggerSource = {}, knownLanes = {}", camera, triggerSource, state.knownLanes)
        } else {
          log.debug("preProcessing {} result is {}", incoming.file.getName, result)
          FileUtils.deleteQuietly(incoming.file) //we have some resulting image
        }

        result
      }
    }
  }

  private def relayConfigFor(camera: MultiLaneCamera,
                             state: LaneSensorState,
                             triggerSource: TriggerSource,
                             targetArea: TriggerArea): Option[RelaySensorConfig] = {
    if (triggerSource.contains(targetArea)) {
      for (
        laneId ← camera.laneIdFor(targetArea);
        relayConfig ← state.findLaneById(laneId)
      ) yield relayConfig
    } else {
      None
    }
  }

  private def imageFor(relayConfig: RelaySensorConfig,
                       state: LaneSensorState,
                       sourceFile: File,
                       targetArea: TriggerArea): File = {
    val targetFolder = state.getFolder(relayConfig)
    val filenameParts = getFilenameParts(sourceFile.getName)
    val filename = filenameParts._1 + TriggerArea.filanemaSuffix(targetArea) + filenameParts._2
    val targetFile = new File(targetFolder, filename)
    FileUtils.copyFile(sourceFile, targetFile)
    targetFile.setLastModified(sourceFile.lastModified())
    targetFile
  }

  private def getExifImageTime(file: File): Option[Long] = metadataReader match {
    case None ⇒ None
    case Some(mr) ⇒ {
      val filePath = file.getAbsolutePath
      var info: Option[ImageInfo] = None
      def getInfo: Unit = {
        info = mr.apply(filePath)
      }
      try {
        SectionGuard.process(metadataReaderGuard, Priority.CRITICAL, getInfo)
      } catch {
        case e: ImageFunctionsException ⇒ {
          log.error("getMetaData file=[%s] returned %s".format(filePath, e.getMessage))
        }
      }
      info flatMap (_.time)
    }
  }

  def getFilenameParts(source: String): Tuple2[String, String] = source.lastIndexOf('.') match {
    case -1  ⇒ (source, "")
    case idx ⇒ (source.substring(0, idx), source.substring(idx))
  }

  def triggerSource(file: File): Option[TriggerSource] = try {
    JaiMakerNoteHandler.getJaiMakerNotes(ReadWriteExifUtil.readExif(file)).getTriggerSource
  } catch {
    case ex: Exception ⇒
      log.error(ex, "Error obtaining TriggerSource from image: {}", ex.getMessage)
      None
  }

}

class MovingAverageBuffer(maxSize: Int, val maxValue: Long) {

  private var queue: Queue[Long] = Queue()

  /**
   * @return tuple2 with element count and average value
   */
  private def average: (Int, Long) = queue.size match {
    //should be synchronized if not private
    case 0 ⇒ (0, 0)
    case n ⇒ (n, queue.foldLeft(0.toLong)(_ + _) / n)
  }

  /**
   * Tries to add the given element and returns a tuple(Int,Option[Int]).
   * The first element is the amount of samples in the buffer, the 2nd element is:
   * -Some(previousAverage) if value was valid and added to the buffer
   * -None if the value was bigger than the max allowed, and was not added
   * Note that the averages are not absolute (so they can be negative)
   *
   * @param value
   * @return a tuple (element count, average), where average is None if abs difference too high
   */
  def tryAdd(value: Long): (Int, Option[Long]) = synchronized {
    //synchronized to make sure its atomic
    val (count, avg) = average

    if (math.abs(value) > maxValue) {
      (count, None)
    } else {
      count match {
        case x if (x < maxSize) ⇒ queue = queue.enqueue(value) //add the new element
        case n                  ⇒ queue = queue.dequeue._2.enqueue(value) //remove the oldest element and adds the new one
      }
      (count, Some(avg))
    }
  }
}
