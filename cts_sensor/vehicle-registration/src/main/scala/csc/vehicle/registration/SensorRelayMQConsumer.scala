package csc.vehicle.registration

import akka.pattern.ask
import akka.actor.SupervisorStrategy.Restart
import akka.actor.{ Actor, ActorLogging, ActorRef, OneForOneStrategy }
import akka.util.Timeout
import com.github.sstone.amqp.Amqp.{ Ack, Delivery, Reject }
import csc.amqp.AmqpSerializers
import net.liftweb.json.DefaultFormats

import scala.concurrent._
import scala.concurrent.duration._

/**
 * Created by carlos on 22/11/16.
 */
class SensorRelayMQConsumer(delegate: ActorRef) extends Actor with ActorLogging with AmqpSerializers {

  override implicit val formats = DefaultFormats
  override val ApplicationId = "SensorRelayMQConsumer"

  implicit val timeout = Timeout(30 seconds)

  object CameraImageExtractor extends MessageExtractor[CameraImageMessage]

  override val supervisorStrategy = OneForOneStrategy() {
    case _: Exception ⇒ Restart
  }

  override def receive: Receive = {
    case delivery @ CameraImageExtractor(msg) ⇒
      log.info("Received {}", msg)
      val consumer = sender
      val future = delegate ? msg.toIncomingFile
      try {
        Await.ready(future, timeout.duration) //some back pressure on the consumer
      } catch {
        case ex: TimeoutException ⇒
          log.warning("Timed out while waiting for file handle conformation. Proceeding anyway")
      }
      consumer ! Ack(delivery.envelope.getDeliveryTag)

    case delivery: Delivery ⇒
      val consumer = sender
      log.error("Dropping message with tag {} - cannot determine message type", delivery.envelope.getDeliveryTag)
      log.debug("Dropped message content is {}", new String(delivery.body))
      consumer ! Reject(delivery.envelope.getDeliveryTag, false)
  }

}

case class CameraImageMessage(version: Int, path: String, time: Long, action: String) {
  def toIncomingFile: IncomingFile = IncomingFile(path, time)
}