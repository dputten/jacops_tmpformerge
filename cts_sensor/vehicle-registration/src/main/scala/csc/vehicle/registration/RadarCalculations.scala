/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import csc.akka.logging.DirectLogging
import csc.vehicle.message.{ ValueWithConfidence }
import csc.gantry.config.RadarConfig

/**
 * This class contains the calculations needed to use the data received from the Radar.
 */
class RadarCalculations(config: ⇒ RadarConfig) extends DirectLogging {

  val lengthConfidence = 95

  //see doc for method calculateLength for more details
  val waveLength: Double = 0.0124 //radar waveLength is 12.4 mm
  val beanAngle: Double = 11 //radar nominal bean width
  //factor = WaveLength /[cos(measurementAngle-beamAngle/2) * surfaceReflection]
  def angle1Rad = math.toRadians(config.measurementAngle - beanAngle / 2)
  def factor: Double = waveLength / (math.cos(angle1Rad) * config.surfaceReflection)
  //beanDistance = [cot(measurementAngle-beamAngle/2) - cot(measurementAngle+beamAngle/2)]*h
  def angle2Rad = math.toRadians(config.measurementAngle + beanAngle / 2)
  def beamDistance: Double = (1 / math.tan(angle1Rad) - 1 / math.tan(angle2Rad)) * config.height

  /**
   * Falcon Plus II returns half of the Doppler pulses.
   * So. electricLength = n * WaveLength /cos(Angle)
   * And electricLength  = vehicleLength + beamDistance
   * And beamDistance = [cot(measurementAngle-beamAngle/2) - cot(measurementAngle+beamAngle/2)]*h
   * results into:
   * vehicleLength = n * WaveLength /cos(measurementAngle-beamAngle/2) - [cot(measurementAngle-beamAngle/2) - cot(measurementAngle+beamAngle/2)]*h
   * Surfaces aren't 100% reflective so the Measured Doppler pulses have to be corrected
   * n = measuredDoppler/surfaceReflection
   * pre calculation values:
   *    factor = WaveLength /[cos(measurementAngle-beamAngle/2) * surfaceReflection]
   *    beanDistance = [cot(measurementAngle-beamAngle/2) - cot(measurementAngle+beamAngle/2)]*h
   * @param reflection The reflection value received from the radar
   * @return The Length with the confidence
   */
  def calculateLength(reflection: Int): ValueWithConfidence[Float] = {
    var length = (reflection * factor - beamDistance).floatValue

    var confidence = lengthConfidence
    if (length <= 0) {
      log.warning("radar reflection %d leads to negative calculated length of %f , will reset value to 0.".format(reflection, length))
      confidence = 0
      length = 0
    }
    new ValueWithConfidence[Float](length, confidence)
  }

  /**
   * This function calculateSpeed and confidence using the following formulas.
   * At this moment there are no speed calculations.
   *   C_v=((v-3)/v)×100  {v∈R│0≤v≤100}
   *   C_v=  97  {v∈R │v>100}
   * @param measuredSpeed The speed measured by the Radar
   * @return the corrected speed with its confidence
   */
  def calculateSpeed(measuredSpeed: Int): ValueWithConfidence[Float] = {
    var confidence = 97
    if (measuredSpeed <= 0) {
      return new ValueWithConfidence[Float](0, 0)
    }
    if (measuredSpeed <= 100) {
      var correctedSpeed: Float = measuredSpeed - 3
      if (correctedSpeed <= 0) {
        correctedSpeed = 0F
      }
      confidence = (correctedSpeed / measuredSpeed.toFloat * 100).toInt
    }
    new ValueWithConfidence[Float](measuredSpeed, confidence)
  }

  /**
   * This function calculates the reflection based on the requested length.
   * This method is used in the Simulators
   * @param length the requested length
   * @return the reflection
   */
  def calculateReflection(length: Double): Int = {
    val reflection = (length + beamDistance) / factor
    reflection.toInt
  }

}