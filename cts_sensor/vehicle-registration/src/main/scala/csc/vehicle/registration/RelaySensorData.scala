/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import java.io.File
import java.text.SimpleDateFormat
import java.util.Date

import base.{ RecipientListLike, Mode, Timing }
import csc.akka.state.{ BackupState, ReplayState }
import csc.vehicle.message._
import akka.actor.{ Actor, ActorLogging, ActorRef }
import csc.gantry.config.{ CameraConfig, MeasurementMethodInstallationType }
import akka.event.LoggingReceive

/**
 * Lane Configuration for the RelaySensorData Actor
 */
case class RelaySensorConfig(lane: Lane, actor: ActorRef, relaySensor: CameraConfig, initialActivation: Boolean, priority: Priority.Value, NMICertificate: Seq[MeasurementMethodInstallationType])
case class RelayRemoveRoute(route: String)

/**
 * This class switch determines the source (sensor).
 * This is done by comparing the directory path of the file and the imageDir in the InitConfigSensor.
 * When the sensor is known the check for activation is done by checking if it is part of the active sensors.
 * When the sensor is active, a message is created and send to the actor linked to the found sensor.
 * New Sensors are added by sending a LaneAttributes message
 * Turning sensors on and off can be done by sending a complete list of sensors which are turned on
 * @param initialCertificationData initial certification data
 * @param adminRef actor where the monitor message are sent to //TODO rewrite this to use the eventqueue
 * @param statePersister optional actor the backup state messages to
 * @param imagePreProcessor preprocessor of images. By default, uses a single lane camera setup (no real preprocessing)
 */
class RelaySensorData(val initialCertificationData: Option[CertificationData],
                      val adminRef: Option[ActorRef] = None,
                      val statePersister: Option[ActorRef],
                      val imagePreProcessor: ImagePreProcessor = ImagePreProcessor.singleLaneCameraPreProcessor,
                      val initialMode: Mode.Value = Mode.Normal)
  extends RelaySensorLike

class CommonWorkflowRelaySensor(val initialCertificationData: Option[CertificationData],
                                val adminRef: Option[ActorRef] = None,
                                val statePersister: Option[ActorRef],
                                val imagePreProcessor: ImagePreProcessor = ImagePreProcessor.singleLaneCameraPreProcessor,
                                val initialMode: Mode.Value = Mode.Normal,
                                val recipients: Set[ActorRef])
  extends RelaySensorLike with RecipientListLike {

  override def sendToNext(config: RelaySensorConfig, msg: VehicleRegistrationMessage): Unit = {
    sendToRecipients(msg) //wired to the common workflow recipients
  }
}

trait RelaySensorLike extends Actor with ActorLogging with Timing with LaneSensorState {

  def initialCertificationData: Option[CertificationData]
  def adminRef: Option[ActorRef] //= None
  def statePersister: Option[ActorRef]
  def imagePreProcessor: ImagePreProcessor //= ImagePreProcessor.singleLaneCameraPreProcessor
  def initialMode: Mode.Value //= Mode.Normal

  log.info("ImagePreProcessor is {}", imagePreProcessor.name)

  var activeList = Map[String, Priority.Value]()

  var gantryModeMap: Map[String, Mode.Value] = Map.empty withDefaultValue (initialMode)

  override def postRestart(reason: Throwable): Unit = {
    statePersister foreach (_ ! ReplayState) //request state replay
  }

  def receive = LoggingReceive {
    case msg: IncomingFile ⇒
      receiveFile(msg)
      sender ! FileHandled //informs the sender the to take the next one
    case file: File ⇒
      receiveFile(IncomingFile(file.getAbsolutePath, System.currentTimeMillis())) //testing purposes
    case activation: Map[_, _] ⇒
      setSensorsActive(activation.asInstanceOf[Map[String, Priority.Value]])
      backup(activation)
    case newLane: RelaySensorConfig ⇒
      addLane(newLane)
      backup(newLane)
    case rmRoute: RelayRemoveRoute ⇒
      removeRoute(rmRoute)
      backup(rmRoute)
    case msg: GetModeRequest ⇒
      //just reply
      sender ! GetModeResponse(msg.correlationId, msg.gantry, gantryModeMap(msg.gantry).toString, System.currentTimeMillis())
    case msg: SetModeRequest ⇒
      //change the mode and if successful backs it up
      if (handleSetMode(msg)) backup(InternalSetMode(msg.gantry, gantryModeMap(msg.gantry)))
    case msg: InternalSetMode ⇒
      //change the mode
      gantryModeMap = gantryModeMap.updated(msg.gantry, msg.mode)

  }

  /**
   * Handles SetModeRequest
   * @param msg
   * @return true if state set, false otherwise
   */
  private def handleSetMode(msg: SetModeRequest): Boolean = {
    val client = sender
    var error: Option[String] = None

    val mode: Mode.Value = Mode.forName(msg.mode) match {
      case Some(md) ⇒
        gantryModeMap = gantryModeMap.updated(msg.gantry, md)
        md //new mode
      case None ⇒
        error = Some(s"Unrecognized mode: ${msg.mode}")
        gantryModeMap(msg.gantry) //current
    }

    client ! SetModeResponse(msg.correlationId, msg.gantry, mode.toString, System.currentTimeMillis(), error)
    error.isEmpty
  }

  private def backup(msg: AnyRef): Unit = statePersister foreach (_ ! BackupState(msg))

  private def removeRoute(rmRoute: RelayRemoveRoute): Unit = {
    val values = knownLanes.values.toList
    for (config ← values) {
      if (config.lane.system == rmRoute.route) {
        knownLanes -= config.relaySensor.relayURI
      }
    }
  }

  private def receiveFile(incoming: IncomingFile): Unit = {
    log.info("Received file {}", incoming)
    val files = imagePreProcessor.apply(incoming, this)
    if (files.isEmpty)
      laneNotFound(incoming.file)
    else
      files.foreach(handle)
  }

  private def laneNotFound(file: File): Unit = {
    //couldn't find sensor. Probably configuration problem
    log.warning("No Lane configured for received image %s".format(file.getPath))
    knownLanes foreach {
      case (path, laneAttr) ⇒ {
        log.info("Lane %s inputpath=%s".format(laneAttr.lane.laneId, path))
      }
    }
    //forward to cleanup
    sendToCleanup(file, "LaneNotFound")
  }

  private def handle(candidate: RegistrationCandidate): Unit = {
    log.info("Handling {}", candidate)
    val file = candidate.file
    val fileName = file.getPath
    val imagePath = file.getParent

    findLane(imagePath) match {
      case None ⇒ laneNotFound(file)
      case Some(laneAttr) ⇒ {
        val id = laneAttr.lane.laneId + "-" + System.nanoTime()

        performanceLogInterval(log, 1, 2, "Receive from FTP and forward", id,
          {
            log.info("Received image ready %s".format(fileName))
            if (isSensorActive(laneAttr.lane)) {
              val creationTime = file.lastModified
              val eventTime = candidate.time.imageTime.getOrElse(creationTime)
              val dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS")

              log.info("Perf msgID=%s received image file: %s file creation time %s , event time %s ".format(
                id,
                fileName,
                dateFormat.format(creationTime),
                dateFormat.format(eventTime)))

              val image = new RegistrationRelay(file.getAbsolutePath)
              val status = statusFor(laneAttr.lane, candidate)

              val msgVR = VehicleRegistrationMessage(eventId = Some(id),
                certificationData = getCertificationData(laneAttr, eventTime),
                event = new SensorEvent(eventTime, new PeriodRange(eventTime, eventTime), "relay"),
                priority = getPriority(laneAttr.lane), lane = laneAttr.lane,
                files = List(image), imageTriggerData = candidate.trigger,
                timeInfo = Some(candidate.time),
                status = status)

              sendToNext(laneAttr, msgVR)

            } else sendToCleanup(file, "InactiveSensor")

          })
        sendMonitorStatus(laneAttr.lane, new Date)
      }
    }
  }

  def sendToNext(config: RelaySensorConfig, msg: VehicleRegistrationMessage): Unit = {
    val processChain = config.actor
    processChain ! msg
  }

  private def statusFor(lane: Lane, candidate: RegistrationCandidate): RegistrationStatus = gantryModeMap(lane.gantry) match {
    case Mode.Normal ⇒ candidate.initialStatus
    case Mode.Test   ⇒ candidate.initialStatus.set(RegistrationStatusFlag.testMode)
  }

  private def sendToCleanup(file: File, reason: String) {
    val creationTime = file.lastModified
    val image = new RegistrationRelay(file.getAbsolutePath)
    val msgVR = new VehicleRegistrationMessage(eventId = None,
      event = new SensorEvent(creationTime, new PeriodRange(creationTime, creationTime), "relay"),
      lane = new Lane(laneId = "Unknown",
        name = "Unknown",
        gantry = "Unknown",
        system = "Unknown",
        sensorGPS_longitude = 0,
        sensorGPS_latitude = 0), files = List(image))

    context.system.eventStream.publish(RegistrationCleanup(msgVR, reason))
  }

  private def addLane(attr: RelaySensorConfig) {
    val file = getFolder(attr)
    log.info("Received new lane %s with URi %s".format(attr.lane.laneId, file.getAbsolutePath))
    knownLanes += file.getPath -> attr
    if (attr.initialActivation) {
      setSensorActive(attr.lane.laneId, attr.priority)
      log.info("Lane %s is Active".format(attr.lane.laneId))
    }
  }

  /**
   * Update the complete active Lane List
   */
  private def setSensorsActive(activeSensors: Map[String, Priority.Value]) = {
    activeList = activeSensors
    if (activeSensors.size == 0) {
      log.info("ActiveSensorList is empty")
    }
    activeList foreach {
      case (sensorId, active) ⇒ {
        log.info("Sensor %s is activated %s".format(sensorId, active))
      }
    }
  }

  /**
   * Add a active lane
   */
  private def setSensorActive(id: String, prio: Priority.Value) {
    activeList += id -> prio
  }

  /**
   * is the reauested lane active
   */
  private def isSensorActive(lane: Lane): Boolean = {
    activeList.contains(lane.laneId)
  }

  /**
   * Get the priority op the requested lane
   */
  private def getPriority(lane: Lane): Priority.Value = {
    val active = activeList.get(lane.laneId)
    if (active.isDefined) {
      return active.get
    } else {
      //should not happen
      log.warning("Tried to get priority for not active sensor: returning noncritical")
      return Priority.NONCRITICAL
    }
  }

  private def sendMonitorStatus(sensor: Lane, now: Date) {
    if (adminRef != None) {
      adminRef.get ! new MonitorCameraMessage(sensor, now)
    }
  }

  def getCertificationData(config: RelaySensorConfig, time: Long): Option[CertificationData] = {
    //val nmiCertificate = CertificateService.getNMICertificate(config.NMICertificate, time)
    //val certMap: Map[String, String] = Map("nmiCertificate" -> nmiCertificate, "appChecksum" -> applChecksum)
    //Some(CertificationData(certMap, Map.empty))
    //TODO PCL-42: should we add anything else? will time or lane make a difference in the certificates?
    initialCertificationData
  }

}

//class CommonWorkflowRelaySensorData

case class InternalSetMode(gantry: String, mode: Mode.Value)
