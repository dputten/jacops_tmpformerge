/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import akka.actor.{ ActorLogging, ActorRef }
import akka.event.LoggingReceive
import base.{ RoundtripMessage, Timing, RecipientList }
import csc.gantry.config.ImageMask
import csc.vehicle.config.TriggerMaskConfig
import csc.vehicle.message.VehicleRegistrationMessage
import csc.vehicle.registration.images.Image

/**
 * This actor applies a mask (dynamic position) to the Overview Image.
 */
class TriggerMaskImage(maskConfig: TriggerMaskConfig,
                       nextActors: Set[ActorRef])
  extends RecipientList(nextActors) with Timing with ActorLogging with ImageMasker {

  override def targetFolder: String = maskConfig.dirMaskImage
  override def crop: Boolean = maskConfig.cropEnabled
  override def imageQuality: Int = maskConfig.maskQuality

  def receive = LoggingReceive {
    case msg: RoundtripMessage ⇒ handleRoundtrip("DynamicMaskImage", msg)
    case msg: VehicleRegistrationMessage ⇒ {
      val eventId = msg.eventId.getOrElse("Unknown")
      performanceLogInterval(log, 320, 321, "Image Masking (dynamic)", eventId, {
        val result = maskOverview(msg)
        sendToRecipients(result)
      })
    }
  }

  override def maskFor(msg: VehicleRegistrationMessage, img: Image): Option[ImageMask] = {
    msg.imageTriggerData map { d ⇒ maskConfig.imageMask(img.heigth, img.width, d) }
  }

}
