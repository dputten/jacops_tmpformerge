/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import akka.camel.{ CamelMessage, Consumer }
import base.{ RoundtripMessage, Timing, RecipientList }
import events.EventBuilder
import net.liftweb.json.{ Serialization, DefaultFormats }
import csc.vehicle.pir.VehiclePIRRegistration
import akka.actor.{ ActorLogging, ActorRef }
import csc.gantry.config.{ CertificateService, MeasurementMethodInstallationType, PIRConfig }
import java.util.Date
import scala.concurrent.duration._
import csc.vehicle.message._
import akka.event.LoggingReceive

class ReceivePir(lane: ⇒ Lane,
                 pirCfg: ⇒ PIRConfig,
                 NMICertificate: ⇒ Seq[MeasurementMethodInstallationType],
                 eventBuilder: EventBuilder,
                 setEventTime: Boolean,
                 nextActors: Set[ActorRef]) extends RecipientList(nextActors) with Consumer with Timing with ActorLogging {
  def endpointUri = "mina:tcp://%s:%d?textline=true&sync=false".format(pirCfg.vrHost.trim(), pirCfg.vrPort)
  implicit val formats = DefaultFormats
  val speedConf = 95
  val lengthConf = 95
  val categoryConfDefault = 60
  log.info("Created PIR %s %d".format(pirCfg.vrHost, pirCfg.vrPort))
  override def activationTimeout = 50 seconds
  def receive = LoggingReceive {
    case msg: RoundtripMessage ⇒ handleRoundtrip("ReceivePir", msg)
    case msg: CamelMessage ⇒ {
      val str = msg.bodyAs[String]
      val pirRegistration = Serialization.read[VehiclePIRRegistration](str)
      val speed = pirRegistration.speed match {
        case Some(sp) ⇒ Some(new ValueWithConfidence[Float](sp, if (sp == 0) 0 else speedConf))
        case None     ⇒ None
      }
      val length = pirRegistration.length match {
        case Some(len) ⇒ {
          val l = if (len > 0) len else 0
          val conf = if (len > 0) lengthConf else 0
          Some(new ValueWithConfidence[Float](l, conf))
        }
        case None ⇒ None
      }
      val categoryConf = pirRegistration.confidence.getOrElse(categoryConfDefault)
      val cat = try {
        pirRegistration.category.map(c ⇒ new ValueWithConfidence[VehicleCategory.Value](VehicleCategory(c), categoryConf))
      } catch {
        case ex: Exception ⇒ None
      }

      val eventTime = pirRegistration.eventTime
      val vehicleData = VehicleData(speed = speed, length = length, category = cat)

      val vehicleMsg = new VehicleRegistrationMessage(
        lane = lane,
        certificationData = getCertificationData(eventTime),
        eventTimestamp = if (setEventTime) Some(eventTime) else None,
        event = eventBuilder.createEvent(eventTime),
        vehicleData = vehicleData)
      sendToRecipients(vehicleMsg)
    }
  }

  def getCertificationData(time: Long): Option[CertificationData] = {
    if (NMICertificate.isEmpty) {
      None
    } else {
      //TODO PCL-45: is this necessary?
      val nmiCertificate = CertificateService.getNMICertificate(NMICertificate, time)
      val certMap: Map[String, String] = Map("nmiCertificate" -> nmiCertificate)
      Some(CertificationData(certMap, Map.empty))
    }
  }

  override def postStop() {
    log.info("Receive Pir %s stopped".format(lane.laneId))
  }

}