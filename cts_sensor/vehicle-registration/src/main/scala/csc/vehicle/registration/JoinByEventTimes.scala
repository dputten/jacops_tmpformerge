/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import base.{ RoundtripMessage, Timing, RecipientList }
import java.util.Date
import akka.actor.{ ActorLogging, ActorRef }
import csc.vehicle.message.{ TimeTrigger, JoinRegistrationMessages, VehicleRegistrationMessage, PeriodRange }
import akka.event.LoggingReceive

import scala.concurrent.duration._

/**
 * This class does the matching of two events using the matchPeriod in the message. The two events are defined by the sourcePrioList.
 * The order in sourcePrioList is important, when joining the two events. The attributes of the first triggerSource in sourcePrioList
 * overrules the attributes in de second triggerSource.
 * When receiving a event not defined in the sourcePrio list it is send directly to the next actors
 * When failing to match the events, only the events with a eventId, are completed and send to the next actors.
 * The setTime defines if the eventTimestamp has to be set when this is missing and couldn't be completed.
 * At this moment the class is only able to join two eventTriggers and contains the sourcePrio list only two triggers
 *
 * Assumptions: Events of one source are received in order
 * @param sourcePrio: list of two strings defining the expected triggerSources
 * @param outputSource: the new sourceTrigger of the joined events
 * @param maxBufferLen: the maximum number of unmatched events for each event type
 * @param setTime: should the eventTimestamp be set after detection of a unmatched event without an eventTimestamp
 * @param nextActors: the next actors
 */
class JoinByEventTimes(sourcePrio: List[String],
                       outputSource: String,
                       maxBufferLen: ⇒ Int,
                       setTime: ⇒ Boolean,
                       laneId: String,
                       timeToKeep: ⇒ FiniteDuration,
                       nextActors: Set[ActorRef]) extends RecipientList(nextActors) with Timing with ActorLogging {

  implicit val executor = context.system.dispatcher
  val sourceTrigger1 = sourcePrio.head
  val sourceTrigger2 = sourcePrio.last

  val joinId = "[" + laneId + ":" + outputSource + ":" + sourceTrigger1 + ":" + sourceTrigger2 + "]"

  /**
   * List of all received Events with the same sourcetrigger(sourceList1) which aren't matched yet
   */
  var msgList1: List[VehicleRegistrationMessage] = List()

  /**
   * List of all received Events with the same sourcetrigger(sourceList2) which aren't matched yet
   */
  var msgList2: List[VehicleRegistrationMessage] = List()

  context.system.scheduler.schedule(timeToKeep, timeToKeep, self, new TimeTrigger())

  def receive = LoggingReceive {
    case msg: RoundtripMessage ⇒ handleRoundtrip("JoinByEventTimes", msg)
    case msg: VehicleRegistrationMessage ⇒ {
      val event = msg.event
      log.info("%s Received event trigger %s %s".format(joinId, event.triggerSource, event.matchRange))
      if (event.triggerSource == sourceTrigger1) {
        val msgList = addToFirstList(msg)
        for (outMsg ← msgList) {
          log.info("EventId=[%s] Sending joined %s message %s".format(outMsg.eventId.getOrElse("Unknown"), joinId, outMsg.event.matchRange))
          sendToRecipients(outMsg)
        }
      } else if (event.triggerSource == sourceTrigger2) {
        val msgList = addToSecondList(msg)
        for (outMsg ← msgList) {
          log.info("EventId=[%s] Sending joined %s message %s".format(outMsg.eventId.getOrElse("Unknown"), joinId, outMsg.event.matchRange))
          sendToRecipients(outMsg)
        }
      } else {
        log.info("EventId=[%s] %s Sending unknown message source [%s]".format(msg.eventId.getOrElse("Unknown"), joinId, event.triggerSource))
        sendToRecipients(msg)
      }
      log.debug("%s List %s has size %d and List %s has Size %d".format(joinId, sourceTrigger1, msgList1.size, sourceTrigger2, msgList2.size))
    }
    case trigger: TimeTrigger ⇒ {
      val now = System.currentTimeMillis()
      val keepTime = now - timeToKeep.toMillis

      val splitResult1 = msgList1.partition(_.event.matchRange.end < keepTime)
      msgList1 = splitResult1._2
      processCleanedMessages(splitResult1._1, now).foreach(outMsg ⇒ {
        log.info("EventId=[%s] Sending unmatched %s [%s] message %s".format(outMsg.eventId.getOrElse("Unknown"), outMsg.event.triggerSource, joinId, outMsg.event.matchRange))
        sendToRecipients(outMsg)
      })

      val splitResult2 = msgList2.partition(_.event.matchRange.end < keepTime)
      msgList2 = splitResult2._2
      processCleanedMessages(splitResult2._1, now).foreach(outMsg ⇒ {
        log.info("EventId=[%s] Sending unmatched %s [%s] message %s".format(outMsg.eventId.getOrElse("Unknown"), outMsg.event.triggerSource, joinId, outMsg.event.matchRange))
        sendToRecipients(outMsg)
      })
    }
  }

  /**
   * add a event to the first list.
   * Check if there is already a event present in de second list, which can be matched with this event.
   * If not store the event
   * Cleanup old imageEvents by creating a VehicleRegistrationMessage
   * @param msg: The event.
   * @return List of matched VehicleRegistrationMessage and/or old unmatched Events
   */
  private def addToFirstList(msg: VehicleRegistrationMessage): List[VehicleRegistrationMessage] = {
    var joinedMessages: List[VehicleRegistrationMessage] = List()
    val period = msg.event.matchRange
    //is there a matched image present
    val found = msgList2.filter(msgItem ⇒ { period.matchPeriod(msgItem.event.matchRange) })
    if (!found.isEmpty) {
      val imageMsg = found.last
      //remove from list
      msgList2 = msgList2.filterNot(_ == imageMsg)
      //join messages
      joinedMessages = List(JoinRegistrationMessages.join(sourcePrio, List(msg, imageMsg), outputSource))
    } else {
      msgList1 = msgList1 :+ msg
    }
    //cleanup Images
    val cleanupTime = msg.event.matchRange.start - timeToKeep.toMillis
    val splitResult = msgList2.partition(_.event.matchRange.end < cleanupTime)
    msgList2 = splitResult._2
    joinedMessages = joinedMessages ++ processCleanedMessages(splitResult._1, msg.event.timestamp)

    //check buffer length
    if (msgList1.size > maxBufferLen) {
      log.warning("EventId=[%s] %s join buffer %s overflow. Processing message anyway".format(msgList1(0).eventId.getOrElse("Unknown"), joinId, sourceTrigger1))
      joinedMessages = joinedMessages ++ processCleanedMessages(List(msgList1(0)), msg.event.timestamp)
      msgList1 = msgList1.tail
    }
    joinedMessages
  }

  /**
   * add an event to the second list.
   * Check if there is already an event present in the first list, which can be matched with this event.
   * If not store the event
   * Cleanup old imageEvents by creating a VehicleRegistrationMessage
   * @param msg: The event.
   * @return List of matched VehicleRegistrationMessage and/or old unmatched Events
   */
  private def addToSecondList(msg: VehicleRegistrationMessage): List[VehicleRegistrationMessage] = {
    var joinedMessages: List[VehicleRegistrationMessage] = List()
    val period = msg.event.matchRange
    //is there a matched image present
    val found = msgList1.filter(msg ⇒ { period.matchPeriod(msg.event.matchRange) })
    if (!found.isEmpty) {
      val imageMsg = found.last
      //remove from list
      msgList1 = msgList1.filterNot(_ == imageMsg)
      //join messages
      joinedMessages = List(JoinRegistrationMessages.join(sourcePrio, List(msg, imageMsg), outputSource))
    } else {
      msgList2 = msgList2 :+ msg
    }
    //cleanup Images
    val cleanupTime = msg.event.matchRange.start - timeToKeep.toMillis
    val splitResult = msgList1.partition(_.event.matchRange.end < cleanupTime)
    msgList1 = splitResult._2
    joinedMessages = joinedMessages ++ processCleanedMessages(splitResult._1, msg.event.timestamp)
    //check buffer length
    if (msgList2.size > maxBufferLen) {
      log.warning("EventId=[%s] %s join buffer %s overflow. Processing message anyway".format(msgList2(0).eventId.getOrElse("Unknown"), joinId, sourceTrigger2))
      joinedMessages = joinedMessages ++ processCleanedMessages(List(msgList2(0)), msg.event.timestamp)
      msgList2 = msgList2.tail
    }
    joinedMessages
  }

  /**
   * Process unmatched events
   */
  private def processCleanedMessages(cleanupImages: List[VehicleRegistrationMessage], time: Long): List[VehicleRegistrationMessage] = {
    var joinedMessages = List[VehicleRegistrationMessage]()
    for (imageMsg ← cleanupImages) {
      log.warning("EventId=[%s] %s Couldn't match message %s.".format(imageMsg.eventId.getOrElse("Unknown"), joinId, imageMsg.event.triggerSource))
      imageMsg.eventId.foreach(eventId ⇒ {
        log.info("EventId=[%s] %s Sending Message source [%s] without match.".format(eventId, joinId, imageMsg.event.triggerSource))
        //log failure to match radarEvent
        val event = imageMsg.event.copy(triggerSource = outputSource)
        if (setTime && imageMsg.eventTimestamp.isEmpty) {
          log.warning("EventId=[%s] %s Set eventTime to current time %s.".format(eventId, joinId, time))
          joinedMessages = joinedMessages :+ imageMsg.copy(eventTimestamp = Some(time), event = event)
        } else {
          joinedMessages = joinedMessages :+ imageMsg.copy(event = event)
        }
      })
    }
    joinedMessages
  }

}