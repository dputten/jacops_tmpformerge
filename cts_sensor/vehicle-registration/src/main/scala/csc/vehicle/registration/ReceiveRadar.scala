/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import base._
import events.EventBuilder
import java.util.Date
import akka.actor.{ ActorLogging, ActorRef }
import csc.vehicle.message._
import akka.camel.{ CamelMessage, Consumer }
import csc.gantry.config.{ CertificateService, MeasurementMethodInstallationType, RadarConfig }
import scala.concurrent.duration._
import akka.event.LoggingReceive

/**
 * Receives messages from the radar containing speed;reflection;
 * And send this information to the next actors
 */
class ReceiveRadar(lane: ⇒ Lane, config: ⇒ RadarConfig,
                   NMICertificate: ⇒ Seq[MeasurementMethodInstallationType],
                   eventBuilder: EventBuilder,
                   setEventTime: Boolean,
                   nextActors: Set[ActorRef],
                   adminRef: Option[ActorRef] = None) extends RecipientList(nextActors) with Consumer with Timing with ActorLogging {
  def endpointUri = config.radarURI
  val calculator = new RadarCalculations(config)
  log.info("Constructed: ReceiveRadar %s %s".format(lane.laneId, config.radarURI))
  override def activationTimeout = 50 seconds

  def receive = LoggingReceive {
    case msg: RoundtripMessage ⇒ handleRoundtrip("ReceiveRadar", msg)
    case msg: CamelMessage ⇒ {
      //radar message: 26;655;
      //!EVENT INTER VEHICLE GAP RESET
      val RadarMessageWithGap = """-?(\d*);(\d*);([\d\.]*);""".r
      val RadarMessage = """-?(\d*);(\d*);""".r
      val RadarInfoMessage = """!(.*)""".r
      val now = System.currentTimeMillis()
      val data = msg.bodyAs[String]
      log.info("Received data from Radar(%s): %s".format(lane.laneId, data))
      context.sender ! "OK"
      var radarMessage: Option[String] = None
      data match {
        case RadarMessage(speed, reflection) ⇒ {
          performanceLogEvent(log, 13, "Received radar", "Unknown")
          processRadarMessage(now, speed, reflection)
          performanceLogEvent(log, 14, "Calculate and forward event", "Unknown")
        }
        case RadarMessageWithGap(speed, reflection, gap) ⇒ {
          performanceLogEvent(log, 13, "Received radar", "Unknown")
          processRadarMessage(now, speed, reflection)
          performanceLogEvent(log, 14, "Calculate and forward event", "Unknown")
        }
        case RadarInfoMessage(info) ⇒ {
          //received GAP message
          //This is a self test and it succeeded
          log.info("Received Radar Info message %s".format(info))
          radarMessage = Some(info)
        }
        case _ ⇒ {
          log.info("Received Unknown message: %s".format(data))
          radarMessage = Some(data)
        }
      }
      sendMonitorStatus(new Date(), radarMessage)
    }
    case msg: Any ⇒ {
      log.info("Unknown data Received data from Radar %s".format(msg))
    }
  }

  def sendMonitorStatus(now: Date, radarMessage: Option[String]) {
    //TODO implement correct monitor implementation
    if (adminRef.isDefined) {
      adminRef.get ! new MonitorRadarMessage(lane, now, radarMessage)
    }
  }
  override def postStop() {
    log.info("Received Radar %s stopped".format(lane.laneId))
  }

  def processRadarMessage(now: Long, speed: String, reflection: String) {
    var realSpeed = calculator.calculateSpeed(speed.toInt)
    var length = calculator.calculateLength(reflection.toInt)
    //todo: RB20130206 changed due to radar test
    var category = length.value match {
      case l if (l <= 5.6f) ⇒
        VehicleCategory.Pers
      case l if (l <= 12.2f) ⇒
        VehicleCategory.Truck
      case _ ⇒
        VehicleCategory.Large_Truck
    }
    log.info("Radar(%s) Received speed: %s km/u, reflection: %s length %s".format(lane.laneId, speed, reflection, length.value))
    val event = eventBuilder.createEvent(now)
    val eventTimeStamp = if (setEventTime) Some(now) else None
    //todo: RB20130206 changed due to radar test
    val vehicleData = VehicleData(speed = Some(realSpeed), length = Some(length), category = Some(new ValueWithConfidence[VehicleCategory.Value](category, 95)))
    val vehicleMsg = new VehicleRegistrationMessage(
      lane = lane,
      certificationData = getCertificationData(now),
      eventTimestamp = eventTimeStamp,
      event = event,
      vehicleData = vehicleData)
    sendToRecipients(vehicleMsg)
  }

  def getCertificationData(time: Long): Option[CertificationData] = {
    if (NMICertificate.isEmpty) {
      None
    } else {
      //TODO PCL-45: is this necessary?
      val nmiCertificate = CertificateService.getNMICertificate(NMICertificate, time)
      val certMap: Map[String, String] = Map("nmiCertificate" -> nmiCertificate)
      Some(CertificationData(certMap, Map.empty))
    }
  }

}
