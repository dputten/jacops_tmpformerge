package csc.vehicle.registration.images

import scala.math.pow

/**
 * Useful for sequentially extracting fields from a Array[Byte],
 * keeping track of the pointer to the next element to be read.
 * Assumes big endian data.
 * Created by carlos on 11.11.15.
 */
class ArrayParser(source: Array[Byte]) {

  var idx = 0

  def readInt(length: Int = 4): Int = {
    val values = for (i ← 0 until length) yield readByte
    val partials = for (i ← 0 until length) yield values(i) * pow(256, i).toInt
    partials.sum
  }

  def readByte: Int = {
    val value = source(idx) & 0xFF
    idx = idx + 1
    value
  }

  def readByteArray(length: Int): Array[Byte] = {
    val result = source.slice(idx, idx + length)
    idx = idx + length
    result
  }

}
