package csc.vehicle.registration

import akka.actor.ActorLogging
import akka.event.LoggingReceive
import base.{ RecipientListLike, RoundtripMessage, Timing }
import csc.amqp.adapter.CorrelatedMessage
import csc.base.Wakeup
import csc.vehicle.message.VehicleRegistrationMessage

import scala.concurrent.duration.Duration

/**
 * Created by carlos on 26/11/16.
 */
trait ComponentServiceCaller extends RecipientListLike with Timing with ActorLogging {

  type Request <: CorrelatedMessage
  type Response <: CorrelatedMessage

  val messageBuffer = new RegistrationBuffer[Request](timeout)

  def timeout: Duration

  def stepName: String = getClass.getSimpleName

  def sendToTarget(request: Request)

  def requestFor(registration: VehicleRegistrationMessage): Either[String, Request]

  def enrichRegistration(registration: VehicleRegistrationMessage, response: Response): VehicleRegistrationMessage

  def ownReceive: Receive

  def fallbackReceive: Receive = LoggingReceive {
    case msg: RoundtripMessage           ⇒ handleRoundtrip(stepName, msg)
    case msg: VehicleRegistrationMessage ⇒ handleRegistration(msg)
    case Wakeup                          ⇒ handleWakeup()
  }

  def receive = ownReceive orElse fallbackReceive

  private def handleRegistration(registration: VehicleRegistrationMessage): Unit = requestFor(registration) match {
    case Left(reason) ⇒
      //do nothing forward message
      log.warning(s"eventId=${registration.id} $stepName $reason")
      sendToRecipients(registration)
    case Right(request) ⇒
      sendToTarget(registration, request)
  }

  def handleResponse(response: Response) {
    log.info("Processing response {}", response)
    if (messageBuffer.isRegistrationProcessed(response.msgId)) {
      log.warning("Received a result [{}] while message is already processed. MessageBuffer size is {}", response.msgId, messageBuffer.size)
    } else {
      messageBuffer.getRegistration(response.msgId) match {
        case Some(registration) ⇒ sendToRecipients(enrichRegistration(registration, response))
        case None               ⇒ log.error("Should not happen: Could not found eventId %s".format(response.msgId))
      }
      messageBuffer.registrationProcessed(response.msgId)
    }
  }

  def sendToTarget(registration: VehicleRegistrationMessage, request: Request) {
    val eventId = registration.id
    log.info("Sending request {} to target actor", request)
    sendToTarget(request)
    messageBuffer.addRegistration(registration, request)
    log.debug("Added {} to message buffer. MessageBuffer size is is now {}", eventId, messageBuffer.size)
  }

  private def handleWakeup(): Unit = {
    log.debug("Handling wakeup. MessageBuffer is {}", messageBuffer)
    val now = System.currentTimeMillis()

    //checkTimeouts
    val timeoutMessages = messageBuffer.getTimeouts(now)
    timeoutMessages.foreach {
      case (eventId, registration) ⇒ {
        log.warning("Failed to recognize message [%s] due to timeout".format(eventId))
        sendToRecipients(registration)
        messageBuffer.registrationProcessed(eventId)
      }
    }
  }

}

//case class DroppedMessage(msgId: String, clientPath: String)

class RegistrationBuffer[MT <: CorrelatedMessage](timeout: ⇒ Duration) {

  case class MessageEnvelope(eventId: String, lastSend: Long, registration: VehicleRegistrationMessage, msg: MT)

  var waiting = Map[String, MessageEnvelope]()

  override def toString: String = {
    "RegistrationBuffer[size=" + waiting.size + ", keys=" + waiting.keys + "]"
  }

  def addRegistration(registration: VehicleRegistrationMessage, msg: MT) {
    waiting += registration.id -> MessageEnvelope(registration.id, msg.time, registration, msg)
  }

  def isRegistrationProcessed(eventId: String): Boolean = {
    waiting.get(eventId).isEmpty
  }

  def registrationProcessed(eventId: String) {
    waiting -= eventId
  }

  def getRegistration(eventId: String): Option[VehicleRegistrationMessage] = {
    waiting.get(eventId).map(_.registration)
  }

  def getTimeouts(now: Long): Map[String, VehicleRegistrationMessage] = {
    val triggerTime = now - timeout.toMillis
    val timeouts = waiting.values.filter(env ⇒ env.lastSend < triggerTime)
    timeouts.map(env ⇒ env.eventId -> env.registration).toMap
  }

  def size = waiting.size
}
