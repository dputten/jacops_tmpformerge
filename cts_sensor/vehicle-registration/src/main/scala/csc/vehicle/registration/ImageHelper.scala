package csc.vehicle.registration

import java.awt.Point

import csc.vehicle.message.VehicleRegistrationMessage

/**
 * Created by carlos on 20.07.15.
 */
object ImageHelper {

  def calcTopLeftOfBoundingBox(vertices: Seq[Point]): Point = {
    val x = vertices.map(_.getX.toInt).min
    val y = vertices.map(_.getY.toInt).min
    new Point(x, y)
  }

  def calcImagePointsForRectangle(message: VehicleRegistrationMessage): Option[Seq[Point]] = message.licenseData.licensePosition match {
    // LicensePosition(TLx = 407, TLy = 97, TRx = 481, TRy = 99, BLx = 408, BLy = 113, BRx = 482, BRy = 114) will be
    // LicensePosition(TLx = 407, TLy = 97, TRx = 482, TRy = 97, BLx = 407, BLy = 114, BRx = 482, BRy = 114)

    case Some(licensePosition) ⇒ {

      val xMin = math.min(licensePosition.TLx, licensePosition.BLx)
      val yMin = math.min(licensePosition.TLy, licensePosition.TRy)
      val xMax = math.max(licensePosition.TRx, licensePosition.BRx)
      val yMax = math.max(licensePosition.BLy, licensePosition.BRy)

      val vertices = Seq(
        new Point(xMin, yMin), // Top left
        new Point(xMax, yMin), // Top right
        new Point(xMax, yMax), // Bottom right
        new Point(xMin, yMax)) // Bottom left
      Some(vertices)
    }

    case None ⇒ None
  }

}
