/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import net.liftweb.json.{ Serialization, DefaultFormats }
import java.io.{ FileReader, File }
import csc.json.lift.EnumerationSerializer

/**
 * Serialize IRoseRegistration
 */
object IRoseRegistrationSerialization {
  implicit val formats = DefaultFormats + new EnumerationSerializer(IRoseImageType, IRoseDirection)

  def serializeToJSON(iRoseRegistration: IRoseRegistration): String = {
    Serialization.write(iRoseRegistration)
  }

  def deserializeFromJSON(jsonString: String): IRoseRegistration = {
    Serialization.read[IRoseRegistration](jsonString)
  }

  def deserializeFromJSON(jsonFile: File): IRoseRegistration = {
    Serialization.read[IRoseRegistration](new FileReader(jsonFile))
  }

}
