/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import csc.vehicle.message._
import net.liftweb.json.{ DefaultFormats, Serialization }
import csc.json.lift.EnumerationSerializer

/**
 * Serialize the VehicleRegistration message
 */
object VehicleRegistrationSerialization {
  implicit val formats = DefaultFormats + new EnumerationSerializer(VehicleImageType)
  /**
   * Serialize the VehicleRegistrationMessage into JSON
   * @param msg The VehicleRegistrationMessage
   * @param overviewImage The overview image
   * @param overviewMaskedImage A masked version of the overview image
   * @param overviewRedLightImage The overview image for red light
   * @param overviewSpeedImage The overview image for speed
   * @param licenseImage The license image
   * @param redLightImage The red light image
   * @param measureMethod2SpeedImage The measuredMethod2 image for speed
   * @return JSON String
   */

  /**
   * Write VehicleMetadata to a String
   * @param obj the VehicleMetadata
   * @return JSON String
   */
  def write(obj: VehicleMetadata): String = {
    Serialization.write(obj)
  }

  /**
   * Read VehicleMetadata from a String
   * @param json string
   * @return the VehicleMetadata
   */
  def read(json: String): VehicleMetadata = {
    Serialization.read[VehicleMetadata](json)
  }

  def write(keepalive: VehicleRegistrationKeepalive): String = {
    Serialization.write(keepalive)
  }

  def write(message: VehicleRegistrationCertificate): String = {
    Serialization.write(message)
  }

  def readKeepalive(json: String): VehicleRegistrationKeepalive = {
    Serialization.read[VehicleRegistrationKeepalive](json)
  }

  def readCertificate(json: String): VehicleRegistrationCertificate = {
    Serialization.read[VehicleRegistrationCertificate](json)
  }

}