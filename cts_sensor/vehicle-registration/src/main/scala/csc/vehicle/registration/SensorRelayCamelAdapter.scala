package csc.vehicle.registration

import akka.pattern.ask
import akka.actor.{ Actor, ActorLogging, ActorRef }
import akka.camel.{ CamelMessage, Consumer }
import akka.util.Timeout

import scala.concurrent.{ Await, TimeoutException }
import scala.concurrent.duration._

/**
 *
 * @param uri this is the endpointUri used by the consumer example "mina:tcp://localhost:12400?textline=true"
 * @param delegate actor to receive the image messages
 * Created by carlos on 22/11/16.
 */
class SensorRelayCamelAdapter(uri: String, delegate: ActorRef)
  extends Actor with Consumer with ActorLogging {

  import SensorRelayCamelAdapter._

  override def endpointUri: String = uri

  implicit val timeout = Timeout(30 seconds)

  override def receive: Receive = {
    case msg: CamelMessage ⇒
      val payload = msg.bodyAs[String]

      fileOf(payload) match {
        case Some(file) ⇒
          val future = delegate ? file
          try {
            Await.ready(future, timeout.duration) //some back pressure
          } catch {
            case ex: TimeoutException ⇒
              log.warning("Timed out while waiting for file handle conformation. Proceeding anyway")
          }
        case None ⇒
          log.error("Unable to parse file from '{}'. Ignoring ", payload)
      }

  }

}

object SensorRelayCamelAdapter {

  val separator = ':'

  def fileOf(str: String): Option[IncomingFile] = str.substring(0, 2) match {
    case "X1" ⇒
      val idx1 = str.indexOf(separator) + 1
      val idx2 = str.indexOf(separator, idx1) + 1
      if (idx2 > idx1 && idx1 > 0) {
        Some(IncomingFile(str.substring(idx1, idx2 - 1), str.substring(idx2).toLong))
      } else None
    case _ ⇒ Some(IncomingFile(str, System.currentTimeMillis()))
  }
}
