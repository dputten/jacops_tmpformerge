package csc.vehicle.registration.classifier

import akka.actor.{ ActorContext, ActorRef, Props }
import csc.amqp.AmqpConnection
import csc.amqp.adapter.{ MQAdapter, MQConsumer, MQProducer }
import csc.amqp.adapter.MQProducer._
import csc.vehicle.config.ClassifierConfig
import csc.vehicle.message.{ ClassifierFormats, ClassifyVehicleRequest, ClassifyVehicleResponse }
import csc.watchdog.ActivateServiceWatch

import scala.concurrent.duration.FiniteDuration

class ClassifierMQAdapter(val producer: ActorRef,
                          val cleanupFrequency: FiniteDuration,
                          override val dropTarget: Option[ActorRef] = None) extends MQAdapter {

  override def ownReceive: Receive = {
    case req: ClassifyVehicleRequest  ⇒ handleRequest(req)
    case res: ClassifyVehicleResponse ⇒ handleResponse(res)
  }

}

class ClassifierMQConsumer(val target: ActorRef) extends MQConsumer with ClassifierFormats {

  object ResponseExtractor extends MessageExtractor[ClassifyVehicleResponse]

  override def ownReceive: Receive = {
    case delivery @ ResponseExtractor(msg) ⇒ target ! msg
  }
}

class ClassifierMQProducer(val producer: ActorRef, val producerEndpoint: String, val systemId: String)
  extends MQProducer with ClassifierFormats {
  val routeKeyFunction: RouteKeyFunction = MQProducer.prefixedRoute(systemId)
}

object ClassifierMQAdapter {

  def create(context: ActorContext,
             amqpConnection: AmqpConnection,
             config: ClassifierConfig,
             systemId: String): ActorRef = {

    context.system.eventStream.publish(ActivateServiceWatch("vehicle-classifier")) //exactly here we know we need the real service

    val producer = context.actorOf(Props(new ClassifierMQProducer(amqpConnection.connection.producer, config.services.producerEndpoint, systemId)))
    val actor = context.actorOf(Props(new ClassifierMQAdapter(producer, config.cleanup)))
    val consumer = context.actorOf(Props(new ClassifierMQConsumer(actor)))
    amqpConnection.queue(config.services).wireConsumer(consumer, true)
    actor
  }

}
