/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import base.{ RoundtripMessage, Timing, RecipientList }
import akka.actor.{ ActorLogging, ActorRef }
import csc.vehicle.message.VehicleRegistrationMessage
import java.io.File
import akka.event.{ EventStream, LoggingReceive }

/**
 * Cleanup all the registration files
 * @param nextActors next step probably only used in tests
 */
class CleanupVehicleRegistration(nextActors: Set[ActorRef] = Set()) extends RecipientList(nextActors) with Timing with ActorLogging {

  /**
   * catches the VehicleRegistrationMessage
   */
  def receive = LoggingReceive {
    case msg: RoundtripMessage ⇒ handleRoundtrip("CleanupVehicleRegistration", msg)
    case RegistrationCleanup(msg, reason) ⇒
      log.info("Cleaning up {} at {}", msg.id, reason)
      cleanup(msg)
    case msg: VehicleRegistrationMessage ⇒
      log.info("Cleaning up {} at complete", msg.id)
      cleanup(msg)
  }

  def cleanup(msg: VehicleRegistrationMessage): Unit = {
    val eventId = msg.id
    //TODO 08022012 RR->RB: it is starting to make sense to have one
    //TODO 08022012 RR->RB: class which encapsulates the flow (and the numbers for the begin/end of perf, and step in process
    performanceLogInterval(log, 390, 391, "Cleanup registration", eventId, {
      val prop = System.getProperty("cleanup")
      if (prop == null || prop != "false") {
        val allFiles = msg.files ++ msg.replacedFiles

        for (file ← allFiles) {
          val fileObj = new File(file.uri)
          if (fileObj.exists()) {
            val result = fileObj.delete()
            log.info("eventId=[%s] deleting file %s".format(eventId, fileObj.getAbsolutePath))
            if (!result)
              log.info("Deleting file %s failed.".format(fileObj.getAbsolutePath))
          }
        }
        for (dir ← msg.directory) {
          val dirObj = new File(dir.uri)
          if (dirObj.exists()) {
            val result = dirObj.delete()
            log.info("eventId=[%s] deleting directory %s".format(eventId, dirObj.getAbsolutePath))
            if (!result)
              log.info("Deleting directory %s failed.".format(dirObj.getAbsolutePath))
          }
        }
      }
      sendToRecipients(msg)
    })
  }

}

object CleanupVehicleRegistration {
  def subscribe(ref: ActorRef, eventStream: EventStream): Unit =
    eventStream.subscribe(ref, classOf[RegistrationCleanup]) //subscribed to early cleanups
}

case class RegistrationCleanup(msg: VehicleRegistrationMessage, step: String)
