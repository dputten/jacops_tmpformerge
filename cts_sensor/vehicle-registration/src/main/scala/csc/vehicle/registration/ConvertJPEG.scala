/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import java.awt.Dimension
import java.io.File

import akka.actor.{ ActorLogging, ActorRef }
import akka.event.LoggingReceive
import base._
import csc.vehicle.config.JpegConvertParameters
import csc.vehicle.message._
import imagefunctions.{ ImageFunctionsException, ImageFunctionsFactory }

/**
 * Converts different image types to a Jpeg image
 */
class ConvertJPEG(convertParm: ⇒ JpegConvertParameters, sectionGuard: Option[FavorCriticalProcess], imageVersion: ImageVersion.Value, nextActors: Set[ActorRef], dirJpg: ⇒ String) extends RecipientList(nextActors) with Timing with ActorLogging {

  val imageType = convertParm.imageType
  /**
   * Catches the VehicleRegistrationMessage
   * @return
   */
  def receive = LoggingReceive {
    case msg: RoundtripMessage ⇒ handleRoundtrip("ConvertJPEG", msg)
    case msg: VehicleRegistrationMessage ⇒ {
      val eventId = msg.eventId.getOrElse("Unknown")
      performanceLogInterval(log, 6, 8, "JPEG scaling", eventId, {
        var newFileAbsolutePath = "";
        var newImage: Option[RegistrationImage] = None
        getFirstImageRegistrationFile(msg) match {
          case None ⇒ {
            log.warning("eventId:[%s]: No image [%s,%s] found in VehicleRegistration".format(eventId, imageType.toString, imageVersion.toString))
          }
          case Some(image) ⇒ {
            val file = new File(image.uri)
            log.debug("file " + image.uri + " image.imageFormat" + image.imageFormat)
            val newFileName = getNewFile(file.getName, dirJpg)
            newFileAbsolutePath = newFileName.getAbsolutePath
            if (convertParm != null) {
              try {
                val dim = new Dimension(convertParm.jpg_width, convertParm.jpg_height)
                SectionGuard.process(sectionGuard, msg.priority,
                  ConvertJPEG.convertJPG(image, dim, file, newFileName.getAbsolutePath, convertParm.jpg_quality))
                newImage = Some(image.copy(uri = newFileAbsolutePath,
                  imageFormat = ImageFormat.toJpg(image.imageFormat),
                  imageType = imageType))
                log.info("converted file: %s".format(newFileName.getAbsolutePath))
                log.info("original image size: {}. converted image size: {}", file.length(), newFileName.length())
              } catch {
                case e: ImageFunctionsException ⇒ {
                  //log only problem and forward the original message without the license and image
                  log.error("%s returned %s".format(imageType, e.getMessage))
                  log.info("src file: %s".format(file.getAbsolutePath))
                  log.info("dest file: %s".format(newFileAbsolutePath))
                }
              }
            }
          }
        }
        newImage match {
          case Some(image) ⇒ {
            val outputMsg = msg.replaceImage(image)
            sendToRecipients(outputMsg)
          }
          case None ⇒ {
            //forward message
            sendToRecipients(msg)
          }
        }
      })
    }
  }

  def getFirstImageRegistrationFile(msg: VehicleRegistrationMessage): Option[RegistrationImage] = {
    val imageList = msg.getImageFiles(imageType, imageVersion)
    imageList.headOption match {
      case Some(regFile) ⇒ {
        if (new File(regFile.uri).exists)
          Some(regFile)
        else {
          log.error(String.format("eventId:[%s], eventTimestamp:[%s], property filename is null or empty", msg.eventId.getOrElse("Unknown"), msg.eventTimestamp))
          None
        }
      }
      case None ⇒ None
    }
  }

  def getFilenameNoExtension(filename: String): String = filename.lastIndexOf(".") match {
    case -1 ⇒ filename
    case x  ⇒ filename.substring(0, x)
  }

  def getNewFile(filename: String, dirJpg: String): File = {
    val properFilename = getFilenameNoExtension(filename)
    new File(dirJpg, "converted-" + properFilename + ".jpg")
  }
}

object ConvertJPEG {

  def convertJPG(image: RegistrationImage,
                 outputDimensions: Dimension,
                 file: File,
                 newFilePath: String,
                 jpg_quality: Int) {

    val sensorLib = ImageFunctionsFactory.getJpgFunctions

    image.imageFormat match {
      case ImageFormat.TIF_BAYER ⇒ sensorLib.bayerToJpg(file.getAbsolutePath,
        outputDimensions.width,
        outputDimensions.height,
        jpg_quality,
        newFilePath)
      case ImageFormat.TIF_RGB ⇒ sensorLib.rgbToJpg(file.getAbsolutePath,
        outputDimensions.width,
        outputDimensions.height,
        jpg_quality,
        newFilePath)
      case ImageFormat.TIF_GRAY ⇒ sensorLib.grayToJpg(file.getAbsolutePath,
        outputDimensions.width,
        outputDimensions.height,
        jpg_quality,
        newFilePath)
      case ImageFormat.JPG_RGB ⇒ sensorLib.jpgToJpg(file.getAbsolutePath,
        outputDimensions.width,
        outputDimensions.height,
        jpg_quality,
        newFilePath)
      case ImageFormat.JPG_GRAY ⇒ sensorLib.jpgToJpg(file.getAbsolutePath,
        outputDimensions.width,
        outputDimensions.height,
        jpg_quality,
        newFilePath)
    }
  }
}
