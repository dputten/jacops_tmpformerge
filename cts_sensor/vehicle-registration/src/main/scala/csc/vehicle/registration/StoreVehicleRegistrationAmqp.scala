/*
 * Copyright (C) 2015. CSC <http://www.csc.com>
 */
package csc.vehicle.registration

import java.io.File

import akka.actor.{ Cancellable, ActorLogging, ActorRef }
import akka.event.LoggingReceive
import scala.concurrent.duration._
import base.{ RecipientList, Timing }
import com.github.sstone.amqp.Amqp.{ Request, Error, Publish }
import csc.amqp.AmqpSerializers
import csc.gantry.config.ComponentCertificate
import csc.image.store.JsonSerializers
import csc.image.store.protocol._
import csc.vehicle.config.AmqpCompletionConfig
import csc.vehicle.message._
import org.apache.commons.codec.digest.DigestUtils
import org.apache.commons.io.FileUtils

/**
 * Forwards the VehicleRegistration to RegistrationReceiver using Amqp (RabbitMQ). It has two functions:
 * - process vehicle registration record and send metadata messages to the registration receiver and images to the
 *   image store
 * - calculate processing delay and send the corrected last processed time towards the registration receiver in
 *   keepalive messages periodically
 *
 * Note, that all messages we send are persisted so they cannot get lost.
 *
 * @param nextActors The next step
 */
class StoreVehicleRegistrationAmqp(producer: ActorRef,
                                   config: AmqpCompletionConfig,
                                   lane: Option[Lane],
                                   nextActors: Set[ActorRef]) extends RecipientList(nextActors)
  with Timing with ActorLogging with AmqpSerializers {

  log.info("Starting StoreVehicleRegistrationAmqp actor.....")

  import StoreVehicleRegistrationAmqp._

  implicit val executor = context.system.dispatcher
  override val formats = VehicleRegistrationSerialization.formats + new JsonSerializers.ImagePutSerializer
  override val ApplicationId = "StoreVehicleRegistrationAmqp-" + (lane map (_.laneId) getOrElse "common")

  var keepalive: Option[Cancellable] = None
  var activeCertificate: Option[ComponentCertificate] = None
  var unknownCount = 0
  val delayAlpha: Double = config.delayAlpha
  val delayMargin: Long = config.delayMargin
  var delay: Long = 0L

  val attributeOverview = "O"
  val attributeOverviewMasked = "OM"
  val attributeOverviewRedLight = "OR"
  val attributeOverviewSpeed = "OS"
  val attributeLicense = "L"
  val attributeRedLight = "R"
  val attributeMeasureMethod2Speed = "2S"
  val attributeMeta = "M"
  val attributeMetaLicense = "ML"

  var pendingStorageImageFolder: Option[File] = None

  var lastKeepaliveSent: Option[VehicleRegistrationKeepalive] = None

  override def preStart() {
    if (config.heartbeatRate.toMillis != 0L) {
      keepalive = Some(context.system.scheduler.schedule(0 second, config.heartbeatRate, self, KeepaliveTick))
      log.info("Keepalive scheduled with frequency of {}", config.heartbeatRate)
    } else {
      log.info("Keepalive is disabled (heartbeat rate is 0)")
    }
  }

  override def postStop(): Unit = {
    keepalive.foreach { svc ⇒
      svc.cancel()
      log.info("Keepalive cancelled")
    }
  }

  val storeImageRemote: Function[ImageToStore, Unit] = { image ⇒
    val (imageId, timestamp, imageBytes) = image
    val request = ImageContentPutRequest(imageId, timestamp, imageBytes, DigestUtils.md5Hex(imageBytes))
    toPublishPayload[ImageContentPutRequest](request) match {
      case Right((body, properties)) ⇒ {
        val props = properties.builder().deliveryMode(2 /* persistent */ ).build()
        val publish = Publish(config.imageStoreEndpoint, config.imageStoreRouteKey, body, Some(props))
        producer ! publish
        log.debug("Image {} queued", imageId)
      }
      case Left(error) ⇒ log.error("Failed to serialize ImageContentPutRequest (id: {}) - {}", imageId, error)
    }
  }

  val storeImageLocal: Function[ImageToStore, Unit] = { image ⇒
    val (imageId, timestamp, imageBytes) = image
    val imageFile = copyImageToPending(imageId)
    val imagePath = imageFile.getAbsolutePath

    val request = ImagePutRequest(imageId, timestamp, imagePath, DigestUtils.md5Hex(imageBytes))
    toPublishPayload[ImagePutRequest](request) match {
      case Right((body, properties)) ⇒ {
        val props = properties.builder().deliveryMode(2 /* persistent */ ).build()
        val publish = Publish(config.imageStoreEndpoint, config.imageStoreRouteKey, body, Some(props))
        producer ! publish
        log.debug("Image {} queued", imageId)
      }
      case Left(error) ⇒ log.error("Failed to serialize ImagePutRequest (id: {}) - {}", imageId, error)
    }
  }

  def receive = LoggingReceive {
    case msg: VehicleRegistrationMessage ⇒ {
      val eventId = msg.eventId.getOrElse({
        unknownCount += 1
        "Unknown" + unknownCount
      })
      performanceLogInterval(log, 900, 901, "Store Registration(AMQP)", eventId, {

        val images = StoreImage.getAllImages(msg)

        val tuples = images.map { image ⇒
          val imageBytes = FileUtils.readFileToByteArray(new File(image.uri))
          Tuple3(image.uri, image.timestamp, imageBytes)
        }

        val metadata = msg.createVehicleMeta(images)

        msg.eventTimestamp foreach { time ⇒
          val measuredDelay = System.currentTimeMillis() - time
          log.info("vehicle-registration measured delay for event [{}] is {} millis", msg.eventId.getOrElse("unknown"), measuredDelay)
          delay = (delayAlpha * delay.toDouble + (1.0d - delayAlpha) * measuredDelay.toDouble).toLong
        }

        sendToRegistrationReceiver(metadata)

        //tuples foreach storeImageRemote //remote version
        tuples foreach storeImageLocal //local version

        sendToRecipients(msg)
      })
    }
    //TODO csilva: make keepalive configurable (Jacops doesn't use it)
    //    case KeepaliveTick ⇒ {
    //      val keepalive = computeKeepalive()
    //      //log.info("POC skipping amqp keepalive sending {}", keepalive)
    //      sendToRegistrationReceiver(keepalive)
    //    }
    case Error(request, reason) ⇒ {
      request match {
        case publish: Publish ⇒ {
          val `type` = if (publish.properties.isDefined) publish.properties.get.getType else "<<unknown>>"
          log.error(reason, "Failed to send message type '{}'", `type`)
        }
        case any: Request ⇒ log.error(reason, "Unknown error on AMQP channel")
      }
    }
  }

  def sendToRegistrationReceiver[T <: AnyRef: Manifest](msg: T): Unit = {
    val payload = toPublishPayload[T](msg)

    payload match {
      case Right((body, properties)) ⇒ {
        val persistent = properties.builder().deliveryMode(2 /* persistent */ ).build()
        val publish = Publish(config.registrationReceiverEndpoint, config.registrationReceiverRouteKey,
          body, Some(persistent))

        log.info("sending registration to amqp: {}", publish)

        producer ! publish
        log.info("Queued {}", msg)
      }
      case Left(error) ⇒ log.error("Error serializing {} message - {}", msg.getClass.getSimpleName, error)
    }
  }

  //TODO csilva: make keepalive configurable (Jacops doesn't use it)
  //  def computeKeepalive(): VehicleRegistrationKeepalive = {
  //    val processedTime = System.currentTimeMillis() - delay - delayMargin
  //    val keepalive = VehicleRegistrationKeepalive(lane, processedTime)
  //
  //    val toSend = lastKeepaliveSent match {
  //      case Some(last) ⇒ if (keepalive.processedTime < last.processedTime) {
  //        val diff = keepalive.processedTime - last.processedTime
  //        log.warning("Lane %s: computed keepalive (%s) is lower than previous (%s)".format(lane, keepalive.processedTime, last.processedTime))
  //        log.warning("Difference is %s millisseconds. Current delay is %s".format(diff, delay))
  //        last
  //      } else keepalive
  //      case None ⇒ keepalive
  //    }
  //
  //    lastKeepaliveSent = Some(toSend)
  //    toSend
  //  }

  def copyImageToPending(uri: String): File = {
    val source = new File(uri)

    if (pendingStorageImageFolder.isEmpty) {
      val paths = getPaths(source, Nil).toList
      val folder = new File(new File("/", paths(0)), "pendingStorageImages")
      folder.mkdirs()
      pendingStorageImageFolder = Some(folder)
    }

    val target = new File(pendingStorageImageFolder.get, source.getName)
    FileUtils.copyFile(source, target)
    val time = source.lastModified()
    if (target.lastModified() != time) {
      target.setLastModified(time)
    }
    target
  }
}

object StoreVehicleRegistrationAmqp {
  type ImageToStore = Tuple3[String, Long, Array[Byte]]
  case object KeepaliveTick

  def getPaths(file: File, acc: Seq[String]): Seq[String] = {
    if (file.getParentFile == null) {
      acc
    } else {
      getPaths(file.getParentFile, file.getName +: acc)
    }
  }
}
