package csc.vehicle.registration.images

import csc.vehicle.message.TriggerArea.TriggerArea
import csc.vehicle.message._

/**
 * Created by carlos on 11.11.15.
 */
object JaiMakerNoteHandler extends MakerNoteHandler {

  val vendorIdentifier = "JAI ITS"

  def getJaiMakerNotes(source: Seq[ExifTagInfo]): JaiMakerNotes = new JaiMakerNotes(getMakerNoteTags(source))

  override def parseTags(source: Array[Byte]): Seq[MakerNoteInfo] = {
    val parser = new ArrayParser(source)
    val fieldCount = parser.readInt(2)
    for (i ← 1 to fieldCount) yield nextInfo(parser)
  }

  private def nextInfo(parser: ArrayParser): MakerNoteInfo = {
    val tag = parser.readInt(2)
    val tp = JaiDataType(parser.readInt(2))
    val counter = parser.readInt(4)
    val value = parseValue(parser, tp, counter)
    MakerNoteInfo(tag, value)
  }

  import JaiDataType._

  private def parseValue(parser: ArrayParser, dataType: JaiDataType, counter: Int): TagValue[_] = dataType match {
    case BYTE      ⇒ TagValueByte(parser.readInt(4))
    case SHORT     ⇒ TagValueShort(parser.readInt(4))
    case LONG      ⇒ TagValueLong(parser.readInt(4).toLong)
    case UNDEFINED ⇒ TagValueByteArray(parser.readByteArray(counter))
    case _         ⇒ throw new UnsupportedOperationException("Not supported: " + dataType)
  }

}

case class JaiMakerNotes(values: Seq[MakerNoteInfo]) {
  import JaiMakerTag._
  def get(tagId: Int): Option[TagValue[_]] = values.find(_.tagType == tagId) map (_.value)
  def getTriggerSource: Option[TriggerSource] = get(TriggerSrc.id) map (_.asInstanceOf[TagValueShort].value.toByte) map (TriggerSource(_))
  def toMap: Map[Int, TagValue[_]] = values map (e ⇒ e.tagType -> e.value) toMap
}

object JaiMakerTag extends Enumeration {
  val TriggerSrc = Value(0xE032)
}

case class TriggerSource(value: Int) {
  def isLeft: Boolean = (value & 0x10) != 0
  def isRight: Boolean = (value & 0x20) != 0
  def isOneSided: Boolean = isLeft != isRight

  def contains(area: TriggerArea): Boolean = area match {
    case TriggerArea.LEFT  ⇒ isLeft
    case TriggerArea.RIGHT ⇒ isRight
  }
}

object JaiDataType extends Enumeration {
  type JaiDataType = Value

  val BYTE = Value(1)
  val ASCII = Value(2)
  val SHORT = Value(3)
  val LONG = Value(4)
  val RATIONAL = Value(5)
  val SBYTE = Value(6)
  val UNDEFINED = Value(7)
  val SSHORT = Value(8)
  val SLONG = Value(9)
  val SRATIONAL = Value(10)
  val FLOAT = Value(11)
  val DOUBLE = Value(12)

}
