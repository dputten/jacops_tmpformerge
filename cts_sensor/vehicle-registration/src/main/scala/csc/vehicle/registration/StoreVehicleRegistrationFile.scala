/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import java.io.File
import java.util.Date

import akka.actor.{ ActorLogging, ActorRef }
import akka.event.LoggingReceive
import base.{ RecipientList, Timing }
import csc.common.Extensions._
import csc.vehicle.message.StoreImage._
import csc.vehicle.message.VehicleRegistrationMessage
import org.apache.commons.io.FileUtils

/**
 * This actor writes the received messages to disk grouped by hour
 * @param outputDir: the root directory of the received messages
 * @param nextActors: the Set of next actors
 */
class StoreVehicleRegistrationFile(outputDir: ⇒ String, nextActors: Set[ActorRef]) extends RecipientList(nextActors) with Timing with ActorLogging {
  var unknownCount = 0

  override def preRestart(reason: Throwable, message: Option[Any]) {
    super.preRestart(reason, message)
    log.error(reason, "Restarting while processing %s".format(message))
  }

  /**
   * VehicleRegistrationMessage received
   */
  def receive = LoggingReceive {
    case msg: VehicleRegistrationMessage ⇒ {
      val eventId = msg.eventId.getOrElse({
        unknownCount += 1
        "Unknown" + unknownCount
      })
      performanceLogInterval(log, 902, 903, "Store Registration(File)", eventId, {
        val dir = getDirectory(msg)

        val imagesAndFilenames =
          for (
            tp ← all;
            image ← msg.getJpgImageFile(tp._1, tp._2)
          ) yield (image, eventId + StoreFileConstants.filenamePostfixes(tp))

        val images = imagesAndFilenames.map {
          case (image, filename) ⇒
            val file = new File(image.uri)
            val newFile = new File(dir, filename)
            //copy file
            FileUtils.copyFile(file, newFile)
            log.debug("Created file %s".format(newFile.getAbsolutePath))
            image.copy(uri = newFile.getAbsolutePath)
        }

        val metadataJson = VehicleRegistrationSerialization.write(msg.createVehicleMeta(images))

        val newFile = new File(dir, eventId + "_meta.json")
        FileUtils.writeStringToFile(newFile, metadataJson)
        log.debug("Created file %s".format(newFile.getAbsolutePath))

        sendToRecipients(msg)
      })
    }
  }
  def getDirectory(msg: VehicleRegistrationMessage): String = {
    val time = msg.eventTimestamp.map(t ⇒ new Date(t)).getOrElse(new Date)
    val dir = new File(outputDir, time.toString("yyyyMMdd_HH", "UTC"))
    if (!dir.exists()) {
      dir.mkdirs()
      log.debug("Created output directory %s".format(dir.getAbsolutePath))
    }
    dir.getAbsolutePath
  }

}

object StoreFileConstants {

  val filenamePostfixes = Map(
    overviewOriginal -> "_overview.jpg",
    overviewMaskedOriginal -> "_overviewMasked.jpg",
    licenseOriginal -> "_license.jpg",
    redLightOriginal -> "_redLight.jpg",
    mm2sOriginal -> "_measureMethod2Speed.jpg")
}