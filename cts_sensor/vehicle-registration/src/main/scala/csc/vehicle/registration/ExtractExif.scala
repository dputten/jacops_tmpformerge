/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import base.{ RoundtripMessage, Timing, RecipientList }
import akka.actor.{ ActorRef, ActorLogging }
import akka.event.LoggingReceive
import csc.vehicle.message._
import java.io.File
import csc.vehicle.registration.images._
import csc.vehicle.message.RegistrationExifFile
import csc.vehicle.config.ExifConfig
import csc.vehicle.message.VehicleRegistrationMessage

/**
 * This actor extracts the Exif information from a jpeg file and writes it to a separate file.
 */
class ExtractExif(imageType: VehicleImageType.Value, config: ⇒ ExifConfig, nextActors: Set[ActorRef]) extends RecipientList(nextActors) with Timing with ActorLogging {
  log.info("Created ExtractExif for " + imageType.toString)

  def receive = LoggingReceive {
    case msg: RoundtripMessage ⇒ handleRoundtrip("ExtractExif", msg)
    case msg: VehicleRegistrationMessage ⇒ {
      val eventId = msg.eventId.getOrElse("Unknown")
      performanceLogInterval(log, 370, 371, "Extract Exif", eventId, {
        log.debug("Try to extraxt EXIF for imageType %s".format(imageType.toString))
        val imageList = msg.getImageFiles(imageType, ImageVersion.Original)

        val forwardMsg = if (imageList.isEmpty) {
          log.warning("Exif data could not be extracted: Original image not found")
          msg
        } else {
          val updatedFiles = imageList.map(imageFile ⇒ {
            if (imageFile.metaInfo.isEmpty) {
              try {
                val info = ReadWriteExifUtil.readExif(new File(imageFile.uri))
                imageFile.copy(metaInfo = Some(info))
              } catch {
                case ex: Throwable ⇒ {
                  log.error(ex, "eventId=[%s] Failed to extract exif information from %s".format(eventId, imageType))
                  imageFile
                }
              }
            } else {
              //image already contain exif information
              log.debug("Message contains already Exif data")
              imageFile
            }
          })
          //update files
          val updateUri = updatedFiles.map(_.uri)
          val files = msg.files.filter {
            case cur: RegistrationImage ⇒ !(updateUri.contains(cur.uri) && cur.imageType == imageType && cur.imageVersion == ImageVersion.Original)
            case other                  ⇒ true
          } ++ updatedFiles
          msg.copy(files = files)
        }
        sendToRecipients(forwardMsg)
      })
    }
  }

  override def postStop() {
    log.info("ExtractExif for " + imageType.toString + " stopped")
  }
}
