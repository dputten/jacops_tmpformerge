/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import base.{ RoundtripMessage, Timing, RecipientList }
import akka.actor.{ ActorLogging, ActorRef }
import csc.vehicle.message.VehicleRegistrationMessage
import akka.event.LoggingReceive

/**
 * Changes the trigger source when available to the outputSource
 * @param outputSource: the new triggersource of messages
 * @param nextActors: the next step
 */
class ChangeTriggerSource(outputSource: String, nextActors: Set[ActorRef]) extends RecipientList(nextActors) with Timing with ActorLogging {

  /**
   * Catches the VehicleRegistrationMessage
   */
  def receive = LoggingReceive {
    case msg: RoundtripMessage ⇒ handleRoundtrip("ChangeTriggerSource", msg)
    case msg: VehicleRegistrationMessage ⇒ {
      performanceLogInterval(log, 100, 101, "Change triggerSource", msg.eventId.getOrElse("Unknown"), {
        val event = msg.event
        val newEvent = event.copy(triggerSource = outputSource)
        val newMsg = msg.copy(event = newEvent)
        sendToRecipients(newMsg)
      })
    }
  }
}