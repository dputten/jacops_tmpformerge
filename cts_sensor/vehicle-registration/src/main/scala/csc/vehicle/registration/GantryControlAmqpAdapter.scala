package csc.vehicle.registration

import akka.actor._
import com.github.sstone.amqp.Amqp.ChannelParameters
import com.github.sstone.amqp.Amqp
import csc.akka.logging.DirectLogging
import csc.amqp.{ AmqpConnection, AmqpSerializers, MQConnection, Publisher }
import csc.vehicle.config.GantryControlConfig
import csc.vehicle.message._
import net.liftweb.json.{ DefaultFormats, Formats }

import scala.concurrent.ExecutionContext

/**
 *
 * Responsible for adapting all control messages to AMQP, extracting them from deliveries and publishing the responses.
 *
 * Created by carlos on 22/06/16.
 */
class GantryControlAmqpAdapter(val producer: ActorRef, delegate: ActorRef, endpoint: String)
  extends Actor with ActorLogging with AmqpSerializers with Publisher {

  override val ApplicationId: String = "GantryControlAmqpAdapter"
  override implicit val formats: Formats = GantryControlAmqpAdapter.formats
  override implicit val executor: ExecutionContext = context.system.dispatcher

  object SetModeRequestExtractor extends MessageExtractor[SetModeRequest]
  object GetModeRequestExtractor extends MessageExtractor[GetModeRequest]

  def receive: Receive = {
    case delivery @ SetModeRequestExtractor(msg) ⇒ delegate ! msg
    case delivery @ GetModeRequestExtractor(msg) ⇒ delegate ! msg
    case msg: SetModeResponse                    ⇒ publish(msg)
    case msg: GetModeResponse                    ⇒ publish(msg)
    case other                                   ⇒ log.warning("Don' know how to handle {}. Ignoring", other)
  }

  private def publish(msg: GantryControlMessage): Unit = {
    val payload = toPublishPayload[msg.type](msg)
    val name = msg.getClass.getSimpleName
    val routeKey = "%s.%s".format(msg.gantry, name)
    publishOrError(endpoint, routeKey, payload, name)
  }
}

object GantryControlAmqpAdapter {

  val formats: Formats = DefaultFormats

  def create(context: ActorContext, config: GantryControlConfig, connection: MQConnection, delegate: ActorRef): ActorRef = {
    val actorSystem = context.system
    val services = config.services
    val listener = context.actorOf(Props(new GantryControlAmqpAdapter(connection.producer, delegate, services.producerEndpoint)))

    connection.createSimpleConsumer(config.services.consumerQueue, listener,
      Some(ChannelParameters(services.serversCount)), true)

    listener
  }
}
