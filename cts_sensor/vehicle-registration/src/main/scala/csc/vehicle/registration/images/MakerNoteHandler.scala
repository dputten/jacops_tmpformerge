package csc.vehicle.registration.images

import csc.vehicle.message._

/**
 * Generic handler of MarkerNote EXIF sub-tags.
 * Each vendor should have an implementation for his.
 *
 * Created by carlos on 12.11.15.
 */
trait MakerNoteHandler {

  def vendorIdentifier: String

  /**
   * @param source array containing the makernote tag value, excluding the vendor prefix
   * @return makernote tags
   */
  def parseTags(source: Array[Byte]): Seq[MakerNoteInfo]

  /**
   * @param source
   * @return maker note tags from the given EXIF tags
   */
  def getMakerNoteTags(source: Seq[ExifTagInfo]): Seq[MakerNoteInfo] = getMakerNote(source) match {
    case None ⇒ Nil
    case Some(tag) ⇒ getVendorContent(tag) match {
      case None        ⇒ Nil
      case Some(array) ⇒ parseTags(array)
    }
  }

  def getMakerNote(source: Seq[ExifTagInfo]): Option[ExifTagInfo] = source.find(_.tagName == "Makernote")

  /**
   * @param makerNote
   * @return maker note vendor-specific contents, if matches the vendor identifier
   */
  def getVendorContent(makerNote: ExifTagInfo): Option[Array[Byte]] = getBytes(makerNote.value) match {
    case Some(bytes) ⇒
      val id = vendorIdentifier.getBytes ++ Array(0.toByte)
      if (bytes.length > id.length && bytes.take(id.length).sameElements(id)) Some(bytes.drop(id.length))
      else None
    case None ⇒ None
  }

  def getBytes(source: ExifValue): Option[Array[Byte]] = source match {
    case ExifValueBytes(array)    ⇒ Some(array.toArray)
    case ExifValueShorts(array)   ⇒ Some(array.map { _.toByte } toArray)
    case ExifValueIntegers(array) ⇒ Some(array.map { _.toByte } toArray)
    case _                        ⇒ None
  }

}
