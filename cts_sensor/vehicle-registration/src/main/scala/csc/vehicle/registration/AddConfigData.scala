/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import akka.actor.{ ActorRef }
import akka.event.LoggingReceive
import base.{ RoundtripMessage, RecipientList, Timing }
import csc.vehicle.message.{ CertificationData, VehicleRegistrationMessage }
import csc.gantry.config.SystemConfig

class AddConfigData(systemCfg: SystemConfig, next: Set[ActorRef]) extends RecipientList(next) with Timing {
  log.info("Created AddConfigData")

  def receive = LoggingReceive {
    case msg: RoundtripMessage ⇒ handleRoundtrip("AddConfigData", msg)
    case msg: VehicleRegistrationMessage ⇒ {
      val eventId = msg.eventId.getOrElse("Unknown")
      performanceLogInterval(log, 44, 45, "AddConfigData", eventId, {

        //update serial number
        log.debug("certificationData = {}", msg.certificationData)
        var certData: CertificationData = msg.certificationData getOrElse (CertificationData(Map.empty, Map.empty))

        val serialNum = systemCfg.serialNumber.serialNumber.trim
        if (certData.serialNr.isEmpty && !serialNum.isEmpty) {
          certData = certData.withSerialNumber(serialNum)
        }

        //update nmi certificate
        //due to continue up-time it is possible that is has changed after startup
        val creationTime = msg.eventTimestamp.getOrElse(msg.event.timestamp)
        val certificate = systemCfg.typeCertificate(creationTime)
        //TODO PCL-45: is this needed? Is it different certificate set by ReceivePir and ReceiveRadar?
        certData = certData.copy(certData.componentChecksumMap.updated("NMICertificate", certificate))

        val sendMsg = msg.copy(certificationData = Some(certData))
        log.debug("Updated message {}", sendMsg)
        sendToRecipients(sendMsg)
      })
    }
  }

  override def postStop() {
    log.info("AddConfigData stopped")
  }

}