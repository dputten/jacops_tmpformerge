package csc.vehicle.registration.recognizer

import java.awt.Point

import akka.actor.ActorRef
import csc.gantry.config.LaneConfig
import csc.image.recognize.{ LicensePlateResponse, RecognizeImageRequest, RecognizeImageResponse }
import csc.vehicle.config.RecognizeConfig
import csc.vehicle.message._
import csc.vehicle.registration.{ ComponentServiceCaller, ImageHelper }
import org.apache.commons.lang.StringUtils

import scala.concurrent.duration.Duration

class RecognizeImageCaller(targetActorPath: String,
                           recognizeCfg: ⇒ RecognizeConfig,
                           laneConfigProvider: LaneConfig.LaneConfigProvider,
                           candidateTypes: Seq[VehicleImageType.Value],
                           val recipients: Set[ActorRef])
  extends ComponentServiceCaller {

  override type Request = RecognizeImageRequest
  override type Response = RecognizeImageResponse

  lazy val timeout: Duration = recognizeCfg.recognizeTimeout

  override def ownReceive: Receive = {
    case response: RecognizeImageResponse ⇒ handleResponse(response)
  }

  override def sendToTarget(request: RecognizeImageRequest): Unit = context.actorSelection(targetActorPath) ! request

  override def requestFor(registration: VehicleRegistrationMessage): Either[String, RecognizeImageRequest] = {
    // return the images for the first candidate type that returns a non-empty list.
    findImage(registration) match {
      case None ⇒ Left("Couldn't find image to recognize")
      case Some(image) ⇒
        Right(RecognizeImageRequest(registration.id, registration.priority, image.uri, image.imageFormat,
          laneConfig(registration).recognizeOptions, System.currentTimeMillis()))
    }
  }

  def laneConfig(reg: VehicleRegistrationMessage): LaneConfig = laneConfigProvider match {
    case Right(cfg)     ⇒ cfg
    case Left(function) ⇒ function(reg.lane.laneId)
  }

  private def findImage(msg: VehicleRegistrationMessage): Option[RegistrationImage] = {

    val licenseSourceImage = candidateTypes.find { !msg.getImageFiles(_, ImageVersion.Original).isEmpty }
    val imageListO = for {
      imageType ← licenseSourceImage
    } yield msg.getImageFiles(imageType, ImageVersion.Original)
    imageListO.flatMap(_.headOption)
  }

  override def enrichRegistration(registration: VehicleRegistrationMessage,
                                  response: RecognizeImageResponse): VehicleRegistrationMessage = {
    response.plate match {
      case Some(licenseResult) ⇒ processResult(registration, licenseResult, response)
      case None ⇒ {
        log.info("Recognizer: eventId=[{}], Recognizer could not find license plate in file {}.", registration.id, response.imageFile)
        registration //original message
      }
    }
  }

  def processResult(msg: VehicleRegistrationMessage,
                    licenseResult: LicensePlateResponse,
                    response: RecognizeImageResponse): VehicleRegistrationMessage = {

    val plate = if (licenseResult.plateStatus == LicensePlateResponse.PLATESTATUS_PLATE_FOUND) {
      log.info("Recognizer: eventId=[%s], Recognizer found license plate in file %s, license=[%s][%s] license conf=[%d] country=[%s] country conf=[%d]".format(
        msg.eventId.getOrElse("Unknown"),
        response.imageFile,
        licenseResult.plateNumber, licenseResult.rawPlateNumber, licenseResult.plateConfidence,
        licenseResult.plateCountryCode, licenseResult.plateCountryConfidence))

      Some(new ValueWithConfidence[String](licenseResult.plateNumber, licenseResult.plateConfidence))
    } else {
      log.info("Recognizer: eventId=[%s], Recognizer could not find license plate in file %s. returnStatus=[%d] plateStatus=[%d]".format(msg.eventId.getOrElse("Unknown"),
        response.imageFile, licenseResult.returnStatus, licenseResult.plateStatus))
      None
    }

    val country = if (licenseResult.plateCountryCode.isEmpty || licenseResult.plateCountryCode == LicensePlateResponse.PLATE_UNKNOWN)
      None
    else
      Some(new ValueWithConfidence[String](licenseResult.plateCountryCode, licenseResult.plateCountryConfidence))

    val image = findImage(msg)
    val imageFormat = image.map(_.imageFormat).getOrElse(ImageFormat.JPG_RGB) //default should never be used otherwise the recognizeRequest wouldn't be send
    val time = image.map(_.timestamp).getOrElse(msg.eventTimestamp.getOrElse(msg.event.timestamp)) //default should never be used otherwise the recognizeRequest wouldn't be send

    val licenseImage = if (StringUtils.isNotBlank(licenseResult.licenseSnippetFileName))
      Some(new RegistrationImage(licenseResult.licenseSnippetFileName, ImageFormat.toTif(imageFormat), VehicleImageType.License, ImageVersion.Original, time))
    else None

    val rawPlate = licenseResult.rawPlateNumber
    val rawLicense = if (rawPlate == null || rawPlate.isEmpty) {
      None
    } else {
      Some(rawPlate)
    }

    val sourceImageType = image.map(_.imageType)

    val rld = RegistrationLicenseData(
      license = plate,
      licensePosition = getLicensePosition(licenseResult, msg, sourceImageType),
      licenseSourceImage = sourceImageType,
      rawLicense = rawLicense,
      country = country,
      recognizedBy = Some(licenseResult.recognizer),
      licenseType = licenseResult.plateType)

    log.info("Plate data for event {} is {}", msg.eventId, rld)

    msg.copy(
      licenseData = rld,
      files = msg.files ++ licenseImage)

  }

  private def getLicensePosition(licenseResult: LicensePlateResponse,
                                 msg: VehicleRegistrationMessage,
                                 sourceImageType: Option[VehicleImageType.Value]): Option[LicensePosition] = {

    if (!licenseResult.licenseSnippetFileName.isEmpty) {
      val base = getLicensePositionTopLeft(msg, sourceImageType)
      Some(platePosition(licenseResult, base))
    } else {
      //plate position is not longer dependent on licenseSnippetFileName. This is needed for OpenCV classification
      (licenseResult.plateXOffset, licenseResult.plateYOffset) match {
        case (x, y) if (x > 0 || y > 0) ⇒ Some(platePosition(licenseResult, new Point(0, 0)))
        case _                          ⇒ None
      }
    }
  }

  private def platePosition(licenseResult: LicensePlateResponse, base: Point) = LicensePosition(
    TLx = licenseResult.plateTL.map(_.x + base.x).getOrElse(-1),
    TLy = licenseResult.plateTL.map(_.y + base.y).getOrElse(-1),
    TRx = licenseResult.plateTR.map(_.x + base.x).getOrElse(-1),
    TRy = licenseResult.plateTR.map(_.y + base.y).getOrElse(-1),
    BLx = licenseResult.plateBL.map(_.x + base.x).getOrElse(-1),
    BLy = licenseResult.plateBL.map(_.y + base.y).getOrElse(-1),
    BRx = licenseResult.plateBR.map(_.x + base.x).getOrElse(-1),
    BRy = licenseResult.plateBR.map(_.y + base.y).getOrElse(-1))

  private def getLicensePositionTopLeft(msg: VehicleRegistrationMessage,
                                        sourceImageType: Option[VehicleImageType.Value]): Point = {

    val initial = new Point(0, 0)

    sourceImageType match {
      case VehicleImageType.OverviewMasked if msg.appliedMaskCropped ⇒ msg.appliedMaskAwt match {
        case Some(points) ⇒ ImageHelper.calcTopLeftOfBoundingBox(points) // getBaseCoordinate(points)
        case None         ⇒ initial
      }
      //@todo csilva: what to do when VehicleImageType is License? is that actually possible? check other source
      case _ ⇒ initial //no translation needed
    }
  }
}

