/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import base.{ RoundtripMessage, Timing, RecipientList }
import akka.actor.{ ActorRef, ActorLogging }
import csc.vehicle.message.VehicleRegistrationMessage
import csc.gantry.config.ImageMask
import akka.event.LoggingReceive
import csc.vehicle.registration.images.Image

/**
 * This actor applies a mask (fixed position) to the Overview Image.
 */
class MaskImage(mask: ⇒ ImageMask,
                cropEnabled: ⇒ Boolean,
                jpgQuality: ⇒ Int,
                dirMaskedImage: ⇒ String,
                nextActors: Set[ActorRef])
  extends RecipientList(nextActors) with Timing with ActorLogging with ImageMasker {

  override def targetFolder: String = dirMaskedImage
  override def crop: Boolean = cropEnabled
  override def imageQuality: Int = jpgQuality

  def receive = LoggingReceive {
    case msg: RoundtripMessage ⇒ handleRoundtrip("MaskImage", msg)
    case msg: VehicleRegistrationMessage ⇒ {
      val eventId = msg.eventId.getOrElse("Unknown")
      performanceLogInterval(log, 320, 321, "Image Masking", eventId, {
        val result = maskOverview(msg)
        sendToRecipients(result)
      })
    }
  }

  override def maskFor(msg: VehicleRegistrationMessage, img: Image): Option[ImageMask] = Some(mask)

}
