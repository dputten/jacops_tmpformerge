/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import csc.common.Extensions._
import akka.actor.{ ActorLogging, ActorRef }
import base.{ RoundtripMessage, Timing, RecipientList }
import csc.vehicle.message._
import images.ReadWriteExifUtil
import java.io.{ FileFilter, File }
import csc.vehicle.message.RegistrationImage
import csc.vehicle.message.RegistrationRelay
import csc.vehicle.message.VehicleRegistrationMessage
import java.util.Date
import scala.Exception
import akka.event.LoggingReceive
import csc.common.Signing
import java.net.URLDecoder

/**
 * The type of an iRose image
 */
object IRoseImageType extends Enumeration {
  val Overview, RedLight, MeasureMethod2 = Value
}

/**
 * Represents the direction in an iRose registration
 */
object IRoseDirection extends Enumeration {
  val Incoming = Value(-1, "Incoming")
  val Outgoing = Value(1, "Outgoing")
}

/**
 * An iRose image as stored in an iRose json file
 * @param url The absolute location of the image file
 * @param imageType The image type of the image (RedLight, Overview, MeasureMethod2)
 * @param time The timestamp for the image
 * @param offset The extra wait time which is used to synchronize to the VRI
 * @param timeYellow The yellow time for the image
 * @param timeRed The red time for the image
 * @param sha The optional checksum for the image
 */
case class IRoseImage(url: String, imageType: IRoseImageType.Value, time: Long, offset: Long, timeYellow: Long, timeRed: Long, sha: Option[String] = None)

/**
 * An iRose registration as stored in an iRose json file
 * @param laneId The id of the lane on which the registration took place
 * @param direction The direction of the registered vehicle
 * @param speed The registered vehicle speed
 * @param length The registered vehicle length
 * @param loop1UpTime The time the vehicle entered the first loop
 * @param loop1DownTime The time the vehicle exited the first loop
 * @param loop2UpTime The time the vehicle entered the second loop
 * @param loop2DownTime The time the vehicle exited the second loop
 * @param serialNr The serial number of the measurement Unit
 * @param images The images that are part of the registration
 * @param jsonMessage Json version of the corresponding vehicle message
 */
case class IRoseRegistration(laneId: String,
                             direction: IRoseDirection.Value,
                             speed: Float, length: Float,
                             loop1UpTime: Long, loop1DownTime: Long, loop2UpTime: Long, loop2DownTime: Long,
                             serialNr: String,
                             images: Seq[IRoseImage],
                             jsonMessage: String)

case class ImageTimeStamps(redLightTimestamp: Option[Long],
                           overviewTimestamp: Option[Long],
                           measureMethod2Timestamp: Option[Long])
/**
 * Receives VehicleRegistrationMessages (containing only a reference to a directory containing all data from iRose),
 * collects the referenced iRose data and adds this to the received message.
 * @param nextActors will receive the processed VehicleRegistrationMessages
 */
class CollectIRoseData(nextActors: Set[ActorRef]) extends RecipientList(nextActors) with Timing with ActorLogging {
  /**
   * Accepts VehicleRegistrationMessage's.
   * These should contain a relay file with the directory in which the data of an iRose registration can be found.
   * The iRose data is read and is then added to the VehicleRegistrationMessage which (at the end) will be sent to the nextActors.
   */

  val datePattern = "dd-MM-yyyy HH:mm:ss.SSS"
  val datePatternDatabalk = "dd-MM-yyyy HH:mm:ss z"
  val datePatternExif = "yyyy:MM:dd hh:mm:ss"
  val logoImageResourcePath = "csc_logo_black.jpg"
  val timeZone = "Europe/Amsterdam"

  def receive = LoggingReceive {
    case msg: RoundtripMessage ⇒ handleRoundtrip("CollectIRoseData", msg)
    case msg: VehicleRegistrationMessage ⇒ {
      performanceLogInterval(log, 3, 47, "Process iRose data", msg.eventId.getOrElse("Unknown"),
        {
          msg.getRelayFile() match {
            case None ⇒ log.warning("Registration received without file")
            case Some(file) ⇒ {
              val iRoseDataDirectory = extractPath(file)
              log.info("Processing data in relay directory %s".format(iRoseDataDirectory.getAbsolutePath))
              IRoseDataCollector.collect(iRoseDataDirectory) match {
                case Right((jsonFile, registration)) ⇒

                  // Prepare the new values for the VehicleRegistrationMessage
                  val registrationImages = createRegistrationImages(registration.images)

                  val redLightTimestamp: Option[Long] = getRedLightImage(registrationImages).flatMap(_.imageTimeStamp)
                  val overviewTimestamp: Option[Long] = getOverviewImage(registrationImages).flatMap(_.imageTimeStamp)
                  val measureMethod2Timestamp: Option[Long] = getMeasureMethod2Image(registrationImages).flatMap(_.imageTimeStamp)

                  val filteredFiles = msg.files.filterNot(file ⇒ file.isInstanceOf[RegistrationRelay])
                  val jsonFilePath = new RegistrationJsonFile(jsonFile.getAbsolutePath)

                  val eventTimestamp = getOverviewImage(registrationImages).map(img ⇒ img.timestamp)

                  val direction = Some(translateDirection(registration.direction))
                  val speed = Some(new ValueWithConfidence[Float](registration.speed, 95))
                  val length = Some(new ValueWithConfidence[Float](registration.length, 95))
                  val files = (filteredFiles ++ registrationImages) :+ jsonFilePath
                  val directory = Some(new RelayDirectory(iRoseDataDirectory.getAbsolutePath))
                  val jsonMessage = registration.jsonMessage
                  val interval1 = calcIntervalBetween(redLightTimestamp, overviewTimestamp)
                  val interval2 = calcIntervalBetween(overviewTimestamp, measureMethod2Timestamp)
                  // Merge the new values into (a copy of) the VehicleRegistrationMessage

                  //set iRose serialNr
                  val certData = msg.certificationData.getOrElse(CertificationData()).withSerialNumber(registration.serialNr)

                  val vehicleData = msg.vehicleData.copy(speed = speed, direction = direction, length = length)
                  val output = msg.copy(
                    eventTimestamp = eventTimestamp,
                    vehicleData = vehicleData,
                    files = files,
                    directory = directory,
                    certificationData = Some(certData),
                    jsonMessage = Some(jsonMessage),
                    interval1Ms = interval1,
                    interval2Ms = interval2)
                  log.debug("Outgoing registration message [%s]".format(output))
                  sendToRecipients(output)

                case Left(error) ⇒
                  log.error("Failed to retrieve registration from relay directory %s. Error: %s".format(iRoseDataDirectory.getAbsolutePath, error))
              }
            }
          }
        })
    }
  }

  /**
   * Given the input IRoseImages, create the corresponding RegistrationImage's.
   * A RedLight and Overview IRoseImage result in one RegistrationImage each.
   * A MeasureMethod2 IRoseImage result in two RegistrationImages's! (MeasureMethod2RedLight and MeasureMethod2Speed)
   * @param images A collection of IRoseImage's
   * @return The list of RegistrationImage's for the input images
   */
  def createRegistrationImages(images: Seq[IRoseImage]): List[RegistrationImage] = {
    // First create an RegistrationImage for each IRoseImage in images.
    var registrationImages: List[RegistrationImage] = images.toList.map(createRegistrationImage(_))
    // More RegistrationImages may be needed for the Overview IRoseImage.
    val overviewImage = registrationImages.find(_.imageType == VehicleImageType.Overview)

    //    If "redlight" then corresponding overview version is needed.
    val redLightImage = registrationImages.find(_.imageType == VehicleImageType.RedLight)
    if (overviewImage.isDefined && redLightImage.isDefined) {
      val overviewRedLightImage = overviewImage.get.copy(imageType = VehicleImageType.OverviewRedLight)
      registrationImages = registrationImages :+ overviewRedLightImage
    }
    //    If "speed" then corresponding overview version is needed.
    val measureMethod2SpeedImage = registrationImages.find(_.imageType == VehicleImageType.MeasureMethod2Speed)
    if (overviewImage.isDefined && measureMethod2SpeedImage.isDefined) {
      val overviewSpeedImage = overviewImage.get.copy(imageType = VehicleImageType.OverviewSpeed)
      registrationImages = registrationImages :+ overviewSpeedImage
    }
    registrationImages
  }

  /**
   * Given the input IRoseImage, create the corresponding RegistrationImage
   * @param image An IRoseImage
   * @return A RegistrationImage for the input image
   */
  def createRegistrationImage(image: IRoseImage): RegistrationImage = {
    import IRoseDataCollector._
    val metaInfo = try {
      val info = ReadWriteExifUtil.readExif(new File(image.url))
      Some(info)
    } catch {
      case ex: Throwable ⇒ {
        log.error(ex, "Failed to extract exif information from %s".format(image.url))
        None
      }
    }

    val imageTimeStamp = metaInfo.flatMap(ReadWriteExifUtil.dateTimeOriginalWithSubSecTimeOriginalUtc(_))

    new RegistrationImage(uri = image.url,
      imageFormat = ImageFormat.JPG_RGB,
      imageType = translateToVehicleImageType(image.imageType).get,
      imageVersion = ImageVersion.Original,
      timestamp = image.time,
      offset = image.offset,
      timeYellow = Some(image.timeYellow),
      timeRed = Some(image.timeRed),
      imageTimeStamp = imageTimeStamp,
      metaInfo = metaInfo)
  }

  def getImageTimeStamp(image: IRoseImage, imageTimeStamps: ImageTimeStamps): Option[Long] = {
    image.imageType match {
      case IRoseImageType.RedLight       ⇒ imageTimeStamps.redLightTimestamp
      case IRoseImageType.Overview       ⇒ imageTimeStamps.overviewTimestamp
      case IRoseImageType.MeasureMethod2 ⇒ imageTimeStamps.measureMethod2Timestamp
    }
  }

  /**
   * Extract the directory from a RegistrationRelay file
   * @param file The RegistrationRelay file that should contain a directory
   * @return A File object for the directory contained in the input file
   */
  def extractPath(file: RegistrationRelay): File = {
    val path = file.uri
    check(path != null, "Path in relay file is missing")
    check(path.trim.length != 0, "Path in relay file is empty")

    val directory = new File(path)
    check(directory.isDirectory, "Path in relay file is not a directory [" + path + "]")

    directory
  }

  def getOverviewImage(images: Seq[RegistrationImage]): Option[RegistrationImage] = {
    images.find(_.imageType == VehicleImageType.Overview)
  }

  def getRedLightImage(images: List[RegistrationImage]): Option[RegistrationImage] = {
    images.find(_.imageType == VehicleImageType.RedLight)
  }

  def getMeasureMethod2Image(images: Seq[RegistrationImage]): Option[RegistrationImage] = {
    images.find(_.imageType == VehicleImageType.MeasureMethod2Speed)
  }

  def calcIntervalBetween(timestamp1O: Option[Long], timestamp2O: Option[Long]): Option[Long] = for {
    t1 ← timestamp1O
    t2 ← timestamp2O
  } yield t2 - t1

  def translateDirection(iRoseDirection: IRoseDirection.Value): Direction.Value = {
    iRoseDirection match {
      case IRoseDirection.Incoming ⇒ Direction.Incoming
      case IRoseDirection.Outgoing ⇒ Direction.Outgoing
    }
  }

  /**
   * Check if requirement is met. If not, log message as an error and throw exception.
   * @param requirement The requirement to be met
   * @param message Error message to use in case requirement is not met
   */
  private def check(requirement: Boolean, message: String) = {
    if (!requirement) {
      val msg = "CollectIRoseData: " + message
      log.error(msg)
      throw new Exception(msg)
    }
  }
}

object IRoseDataCollector {

  def collect(directory: File): Either[String, (File, IRoseRegistration)] = {
    findJsonFile(directory) match {
      case Right(jsonFile) ⇒
        getIRoseRegistration(jsonFile) match {
          case Right(registration) ⇒ Right((jsonFile, registration))
          case Left(error)         ⇒ Left("IRoseDataCollector: " + error)
        }
      case Left(error) ⇒ Left("IRoseDataCollector: " + error)
    }
  }

  /**
   * Return the single json file that should be present in the input directory.
   * @param directory The directory containing iRose data
   * @return A Right(File) containing the json file or a Left(errormessage)
   */
  private def findJsonFile(directory: File): Either[String, File] = {
    val jsonFiles = directory.listFiles(new FileFilter {
      def accept(file: File): Boolean = {
        file.getExtension.exists(_ == "json")
      }
    })
    jsonFiles.length match {
      case 0 ⇒ Left("Missing json file in directory [%s]".format(directory.getAbsolutePath))
      case 1 ⇒ Right(jsonFiles.head)
      case _ ⇒ Left("More than one json file in directory [%s]".format(directory.getAbsolutePath))
    }
  }

  /**
   * Return an IRoseRegistration for the contents of the input file. (Deserialize)
   */
  private def getIRoseRegistration(file: File): Either[String, IRoseRegistration] = {
    deserializeIRoseRegistration(file) match {
      case Right(registration) ⇒ checkRegistration(registration)
      case Left(error)         ⇒ Left(error)
    }
  }

  private def deserializeIRoseRegistration(file: File): Either[String, IRoseRegistration] = try {
    Right(IRoseRegistrationSerialization.deserializeFromJSON(file))
  } catch {
    case e: Exception ⇒ Left("Could not deserialize IRoseRegistration from file [%s]".format(file.getAbsolutePath))
  }

  /**
   * Check if IRoseRegistration is valid
   * @return A Right(IRoseRegistration) containing the valid registration or a Left(errormessage)
   */
  private def checkRegistration(registration: IRoseRegistration): Either[String, IRoseRegistration] = try {
    registration.images.foreach(checkImage(_))
    Right(registration)
  } catch {
    case e: Exception ⇒ Left("Retrieved IRoseRegistration not correct: %s".format(e.getMessage))
  }
  /**
   * Check if an IRoseImage exists and has a valid type.
   */
  private def checkImage(image: IRoseImage): Unit = {
    check(fileExists(image.url), "Missing image file [%s]".format(image.url))
    check(imageTypeValid(image.imageType), "Unknown image type [%s] for file [%s]".format(image.imageType, image.url))
    image.sha.foreach(expectedSha ⇒
      check(expectedSha == calculateSha(image.url), "Wrong checksum for image file [%s]".format(image.url)))
  }

  private def calculateSha(url: String): String = {
    val decodedPath = URLDecoder.decode(url, "UTF-8")
    Signing.digest(decodedPath, "MD5")
  }

  private def fileExists(path: String): Boolean = {
    new File(path).exists()
  }

  private def imageTypeValid(imageType: IRoseImageType.Value): Boolean = {
    translateToVehicleImageType(imageType).isDefined
  }

  private def check(requirement: Boolean, message: String): Unit = {
    if (!requirement) {
      throw new Exception(message)
    }
  }

  /**
   * Contains the default translations for the possible image types in an iRose json file to VehicleImageType's.
   */
  private val translations = Map(
    IRoseImageType.RedLight -> VehicleImageType.RedLight,
    IRoseImageType.Overview -> VehicleImageType.Overview,
    IRoseImageType.MeasureMethod2 -> VehicleImageType.MeasureMethod2Speed)

  def translateToVehicleImageType(imageType: IRoseImageType.Value): Option[VehicleImageType.Value] = {
    translations.get(imageType)
  }
}
