package csc.vehicle.registration

import java.awt.Point
import java.io.File

import csc.gantry.config.ImageMask
import csc.vehicle.message.{ RegistrationImage, ImageVersion, VehicleImageType, VehicleRegistrationMessage }
import csc.vehicle.registration.images.Image

/**
 * Trait with image mask operation
 *
 * Created by carlos on 18/01/16.
 */
trait ImageMasker {

  def targetFolder: String
  def crop: Boolean
  def imageQuality: Int
  def maskFor(msg: VehicleRegistrationMessage, img: Image): Option[ImageMask]

  def maskOverview(msg: VehicleRegistrationMessage): VehicleRegistrationMessage = {
    val image = msg.getJpgImageFile(VehicleImageType.Overview, ImageVersion.Original)
    image match {
      case Some(image) ⇒ {
        val overviewImage = new Image(image.uri)
        maskFor(msg, overviewImage) match {
          case None ⇒ msg //no mask: return the same message
          case Some(mask) ⇒ {
            val vertices = createVertices(mask)
            val maskedOverviewImage = overviewImage.applyPolygonMask(vertices, doCrop = crop)
            val newName = createMaskedFilename(msg, image)
            maskedOverviewImage.writeImage(newName, compression = imageQuality)
            // Add masked image to message
            val file = image.copy(uri = newName, imageType = VehicleImageType.OverviewMasked, imageVersion = ImageVersion.Original)
            val newFiles = msg.files :+ file
            msg.copy(
              appliedMask = Some(vertices map fromAwt),
              appliedMaskCropped = crop,
              files = newFiles)
          }
        }
      }
      case None ⇒ msg //no image: return the same message
    }
  }

  def fromAwt(point: Point) = csc.vehicle.message.Point(point.x, point.y)

  def createMaskedFilename(msg: VehicleRegistrationMessage, image: RegistrationImage): String = {
    val sepIdx = image.uri.lastIndexOf("/")
    val name = "masked_" + image.uri.substring(sepIdx + 1)
    //TODO csilva: do we need the eventId in the name or _masked in the end?
    //if so, we lose the original name (and the activator suffix) necessary for the recognition stub
    //    msg.eventId match {
    //      case Some(id) ⇒ id + "_masked.jpg"
    //      case None     ⇒ image.uri + "_masked.jpg"
    //    }
    val newFile = new File(targetFolder, name)
    newFile.getAbsolutePath
  }

  private def createVertices(mask: ImageMask): Seq[Point] = {
    mask.vertices map { vertex ⇒ new Point(vertex.x, vertex.y) }
  }

}
