package csc.vehicle.registration.images

import java.io.File
import java.awt.image.BufferedImage
import javax.imageio.ImageIO

/**
 * used to make image correction from the command shell
 * example to use. Start path is deploy folder of vehicle registration
 * sudo java -cp vehicle-registration-2.0.0.9-SNAPSHOT.jar:../lib/scala-library.jar  csc.vehicle.registration.images.ImagesCommand /app/registration/deploy/ORG2/ production 2 1 0.5 254 0.25
 */
object ImagesCommand extends App {
  val folder = args(0)
  val command = args(1)

  command match {
    case "gamma" ⇒
      val gamma = args(2).toDouble
      foreachFile(folder) {
        case (file, image) ⇒
          val result = ImageCorrections.applyGammaCorrection(image, gamma)
          val fileName = "%s_gamma_%s".format(file.getCanonicalPath, gamma)
          (result, fileName)
      }
    case "color" ⇒
      val red = args(2).toInt
      val green = args(3).toInt
      val blue = args(4).toInt
      foreachFile(folder) {
        case (file, image) ⇒
          val result = ImageCorrections.applyRGBCorrection(image, red, green, blue)
          val fileName = "%s_red_%s_green_%s_blue_%s".format(file.getCanonicalPath, red, green, blue)
          (result, fileName)
      }
    case "autolevel-manual" ⇒
      val blackChannel = args(2).toInt
      val whiteChannel = args(3).toInt
      foreachFile(folder) {
        case (file, image) ⇒
          val result = ImageCorrections.applyAutoLevel(image, blackChannel, whiteChannel)
          val fileName = "%s_autolevel_black_%s_white_%s".format(file.getCanonicalPath, blackChannel, whiteChannel)
          (result, fileName)
      }
    case "autolevel-dynamic" ⇒
      val blackChannel = args(2).toInt
      val blackMargin = args(3).toDouble
      val whiteChannel = args(4).toInt
      val whiteMargin = args(5).toDouble
      foreachFile(folder) {
        case (file, image) ⇒
          val (result, black, white) = ImageCorrections.applyDynamicAutoLevel(image, blackChannel, blackMargin, whiteChannel, whiteMargin)
          val fileName = "%s_dynamic_blackdynamic_%s_whitedynamic_%s_black_%s_blackmargin_%s_white_%s_whitemargin_%s".format(file.getCanonicalPath, black, white, blackChannel, blackMargin, whiteChannel, whiteMargin)
          (result, fileName)
      }
    case "production" ⇒
      val gamma = args(2).toDouble
      val blackChannel = args(3).toInt
      val blackMargin = args(4).toDouble
      val whiteChannel = args(5).toInt
      val whiteMargin = args(6).toDouble
      foreachFile(folder) {
        case (file, image) ⇒
          val (resultAutolevel, black, white) = ImageCorrections.applyDynamicAutoLevel(image, blackChannel, blackMargin, whiteChannel, whiteMargin)
          val result = ImageCorrections.applyGammaCorrection(resultAutolevel, gamma)
          val fileName = "%s_dynamic_blackdynamic_%s_whitedynamic_%s_gamma_%s_black_%s_blackmargin_%s_white_%s_whitemargin_%s".format(file.getCanonicalPath, black, white, gamma, blackChannel, blackMargin, whiteChannel, whiteMargin)
          (result, fileName)
      }
  }

  private def foreachFile(folderPath: String)(f: (File, BufferedImage) ⇒ (BufferedImage, String)) {
    val folder = new File(folderPath)
    folder.listFiles.foreach {
      case file ⇒
        val (image, fileName) = f(file, ImageIO.read(file))
        writeFile(image, fileName)
    }
  }
  private def writeFile(image: BufferedImage, fileName: String) {
    ImageIO.write(image, "jpg", new File(fileName + ".jpg"))
  }
}