/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import base._
import akka.actor.{ ActorLogging, ActorRef }
import collection.mutable.{ ListBuffer, HashMap }
import csc.vehicle.message.{ JoinRegistrationMessages, VehicleRegistrationMessage }
import akka.event.LoggingReceive

/**
 * This class joins multiple VehicleRegistrationMessage into one VehicleRegistrationMessage, based on the eventId
 * When the message doesn't have an Id it is send directly to the next actors.
 * The sourcePrio defines the expected messages which has to be joined. The order is important because it defines
 * which message overrules the others when joining the attributes. The lower triggerSources have a higher priority
 * When the maxBufferLength exceeds the oldest already received messages are joined and send to the next actors
 * @param sourcePrio: list of expected triggerSources
 * @param outputSource: the new triggersource of the new joined message
 * @param maxBufferLen: maximum length of the buffer
 * @param nextActors: The next actors.
 */
class JoinByEventId(sourcePrio: List[String], outputSource: String, maxBufferLen: ⇒ Int, nextActors: Set[ActorRef]) extends RecipientList(nextActors) with Timing with ActorLogging {
  val nrMessages = sourcePrio.size
  val receivedVehicleMessages = new HashMap[String, ListBuffer[VehicleRegistrationMessage]]
  val orderReceivedEventIds = new ListBuffer[String]

  def receive = LoggingReceive {
    case msg: RoundtripMessage ⇒ handleRoundtrip("JoinByEventId", msg)
    case msg: VehicleRegistrationMessage ⇒ {
      msg.eventId match {
        case None ⇒ sendToRecipients(msg)
        case Some(eventId) ⇒ {
          performanceLogEvent(log, 38, "Join VehicleMessages", eventId)

          val collection = receivedVehicleMessages.get(eventId).getOrElse({
            val newList = new ListBuffer[VehicleRegistrationMessage]()
            receivedVehicleMessages += eventId -> newList
            orderReceivedEventIds += eventId
            newList
          })
          collection += msg

          //check if all messages are received
          if (collection.size >= nrMessages) {
            val outputMsg = JoinRegistrationMessages.join(sourcePrio, collection.toList, outputSource)
            sendToRecipients(outputMsg)
            performanceLogEvent(log, 40, "Send VehicleMessage", outputMsg.eventId.getOrElse("Unknown"))
            //cleanup
            receivedVehicleMessages -= eventId
            orderReceivedEventIds -= eventId
          }
          //cleanup old message
          if (orderReceivedEventIds.size > maxBufferLen) {
            log.warning("EventId=[%s] join buffer overflow. Sending incomplete message".format(orderReceivedEventIds(0)))
            //TODO: what should we do with these message Send them anyway or drop
            //send received messages
            val cleanEventId = orderReceivedEventIds(0)
            receivedVehicleMessages.get(cleanEventId) match {
              case None ⇒ {
                //shouldn't happen sync error
                orderReceivedEventIds -= cleanEventId
              }
              case Some(recvList) ⇒ {
                val outputMsg = JoinRegistrationMessages.join(sourcePrio, recvList.toList, outputSource)
                sendToRecipients(outputMsg)
                performanceLogEvent(log, 40, "Send VehicleMessage", cleanEventId)
                //cleanup
                receivedVehicleMessages -= cleanEventId
                orderReceivedEventIds -= cleanEventId
              }
            }
          }
        }
      }
    }
  }
}