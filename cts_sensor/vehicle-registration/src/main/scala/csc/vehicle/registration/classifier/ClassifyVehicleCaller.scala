package csc.vehicle.registration.classifier

import akka.actor.{ ActorRef, Props }
import csc.vehicle.config.ClassifierConfig
import csc.vehicle.message._
import csc.vehicle.registration.ComponentServiceCaller

import scala.concurrent.duration.Duration

class ClassifyVehicleCaller(targetActorPath: String,
                            val maxRetries: Int,
                            val timeout: Duration,
                            val recipients: Set[ActorRef]) extends ComponentServiceCaller {

  override type Request = ClassifyVehicleRequest
  override type Response = ClassifyVehicleResponse

  def now = System.currentTimeMillis()

  override def ownReceive: Receive = {
    case response: Response ⇒ handleResponse(response)
  }

  override def sendToTarget(request: ClassifyVehicleRequest): Unit = context.actorSelection(targetActorPath) ! request

  override def requestFor(registration: VehicleRegistrationMessage): Either[String, ClassifyVehicleRequest] = {
    val image = registration.getImageFile(ImageFormat.JPG_RGB, VehicleImageType.Overview, ImageVersion.Original)
    val position = registration.licenseData.licensePosition

    (image, position) match {
      case (Some(img), Some(pos)) ⇒ Right(ClassifyVehicleRequest(registration.id, img.uri, Point(pos.TLx, pos.TLy), now))
      case _ ⇒
        Left(s"Cannot classify, as some arg is missing. image: ${image.map(_.uri)} , plate position: $position")
    }
  }

  override def enrichRegistration(registration: VehicleRegistrationMessage,
                                  response: ClassifyVehicleResponse): VehicleRegistrationMessage = {

    log.info("Classification for {} is {}", registration.id, response.category)
    val vehicleData = registration.vehicleData.copy(category = response.category)
    registration.copy(vehicleData = vehicleData)
  }

}

object ClassifyVehicleCaller {

  def props(actorPath: String, config: ClassifierConfig, nextActors: Set[ActorRef]): Props =
    Props(new ClassifyVehicleCaller(actorPath, 0, config.timeout, nextActors))

}
