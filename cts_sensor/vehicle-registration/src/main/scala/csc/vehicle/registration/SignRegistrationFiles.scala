/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import base.{ RoundtripMessage, Timing, RecipientList }
import akka.actor.{ ActorLogging, ActorRef }
import csc.common.Signing
import collection.mutable.ListBuffer
import csc.vehicle.message._
import akka.event.LoggingReceive

class SignRegistrationFiles(nextActors: Set[ActorRef]) extends RecipientList(nextActors) with Timing with ActorLogging {

  /**
   * VehicleRegistrationMessage received
   */
  def receive = LoggingReceive {
    case msg: RoundtripMessage ⇒ handleRoundtrip("SignRegistrationFiles", msg)
    case msg: VehicleRegistrationMessage ⇒ {
      val eventId = msg.eventId.getOrElse("Unknown")
      performanceLogInterval(log, 300, 301, "File Signing", eventId, {

        val orgFiles = msg.files
        var updatedFiles = new ListBuffer[RegistrationFile]()
        for (file ← orgFiles) {
          val updated = file match {
            case relay: RegistrationRelay     ⇒ relay.copy(sha = Some(Signing.digest(relay.uri, "MD5")))
            case image: RegistrationImage     ⇒ image.copy(sha = Some(Signing.digest(image.uri, "MD5")))
            case archive: RegistrationArchive ⇒ archive.copy(sha = Some(Signing.digest(archive.uri, "MD5")))
            case f: RegistrationFile          ⇒ f
          }
          updatedFiles += updated
        }
        val updatedMsg = msg.copy(files = updatedFiles.toList)
        sendToRecipients(updatedMsg)
      })
    }
  }
}