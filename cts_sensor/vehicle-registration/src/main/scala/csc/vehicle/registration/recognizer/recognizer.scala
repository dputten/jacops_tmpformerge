package csc.vehicle.registration.recognizer

import akka.actor.{ ActorContext, ActorRef, Props }
import csc.amqp.AmqpConnection
import csc.amqp.adapter.MQProducer._
import csc.amqp.adapter.{ CorrelatedMessage, MQAdapter, MQConsumer, MQProducer }
import csc.base.RecognizerHeartbeat
import csc.image.recognize._
import csc.vehicle.config.RecognizerQueueConfig
import csc.watchdog.ActivateServiceWatch

import scala.concurrent.duration.FiniteDuration

/**
 * Created by carlos on 27/11/16.
 */
class RecognizerMQAdapter(val producer: ActorRef,
                          val cleanupFrequency: FiniteDuration,
                          override val dropTarget: Option[ActorRef] = None)
  extends MQAdapter with RecognizerFormats {

  override def ownReceive: Receive = {
    case req: RecognizeImageRequest  ⇒ handleRequest(req)
    case res: RecognizeImageResponse ⇒ handleResponse(res)
  }

  override def handleResponse(res: CorrelatedMessage): Unit = {
    if (res.isInstanceOf[RecognizeImageResponse]) {
      context.system.eventStream.publish(RecognizerHeartbeat(res.time))
    }
    super.handleResponse(res)
  }
}

class RecognizerMQProducer(val producer: ActorRef, val producerEndpoint: String, val systemId: String)
  extends MQProducer with RecognizerFormats {

  val routeKeyFunction: RouteKeyFunction = MQProducer.prefixedRoute(systemId)
}

class RecognizerMQConsumer(val target: ActorRef) extends MQConsumer with RecognizerFormats {

  object ResponseExtractor extends MessageExtractor[RecognizeImageResponse]

  override def ownReceive: Receive = {
    case delivery @ ResponseExtractor(msg) ⇒ target ! msg
  }
}

object RecognizerMQAdapter {

  def create(context: ActorContext,
             amqpConnection: AmqpConnection,
             config: RecognizerQueueConfig,
             systemId: String): ActorRef = {

    context.system.eventStream.publish(ActivateServiceWatch("recognize-processor")) //exactly here we know we need the real service

    val producer = context.actorOf(Props(new RecognizerMQProducer(amqpConnection.connection.producer, config.producerEndpoint, systemId)))
    val actor = context.actorOf(Props(new RecognizerMQAdapter(producer, config.cleanup, None)))
    val consumer = context.actorOf(Props(new RecognizerMQConsumer(actor)))
    amqpConnection.queue(config).wireConsumer(consumer, true)
    actor
  }

}