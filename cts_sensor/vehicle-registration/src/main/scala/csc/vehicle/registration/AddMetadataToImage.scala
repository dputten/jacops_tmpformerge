/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.vehicle.registration

import base.{ RoundtripMessage, Timing, RecipientList }
import csc.vehicle.message.{ VehicleImageType, ObsoleteRegistrationFile, RegistrationImage, VehicleRegistrationMessage }
import akka.actor.{ ActorLogging, ActorRef }
import akka.event.LoggingReceive
import csc.vehicle.registration.images._
import java.io.File
import csc.vehicle.config.ExifConfig
import scala.util.matching.Regex
import net.liftweb.json.{ JsonAST, Printer, JsonParser }

/**
 * This actor adds Exif data and/or json message to metadata of a jpeg file and writes it to a new file.
 */
class AddMetadataToImage(targetImageType: VehicleImageType.Value, config: ⇒ ExifConfig, nextActors: Set[ActorRef]) extends RecipientList(nextActors) with Timing with ActorLogging {

  def outputDir = new File(config.dirExifImage)

  def receive = LoggingReceive {
    case msg: RoundtripMessage ⇒ handleRoundtrip("AddMetadataToImage", msg)
    case msg: VehicleRegistrationMessage ⇒ {
      val eventId = msg.eventId.getOrElse("Unknown")
      performanceLogInterval(log, 372, 373, "Add metadata to image", eventId, {
        val oldAndNewFiles = try {
          for (
            targetImageTypeConfig ← config.getTargetConfig(targetImageType);
            targetImageFile ← msg.getJpgImageFile(targetImageType, targetImageTypeConfig.targetImageVersion);
            exifInfo ← targetImageFile.metaInfo;
            filePath ← createNewTargetFilename(targetImageFile)
          ) yield {
            val addExif = if (config.writeJsonToComment && msg.jsonMessage.isDefined) {
              exifInfo :+ ReadWriteExifUtil.createCommentTag(prettyJson(msg.jsonMessage.get))
            } else {
              exifInfo
            }

            ReadWriteExifUtil.writeExif(new File(targetImageFile.uri), addExif, new File(filePath))
            (targetImageFile, filePath)
          }
        } catch {
          case ex: Throwable ⇒ {
            log.error("EventId=%s: Failed to create image with metadata".format(eventId), ex)
            None
          }
        }
        oldAndNewFiles match {
          case Some((originalTargetImageFile, newTargetImageFilePath)) ⇒
            // Remove current targetImageFile in message by one with new uri; old uri is added as ObsoleteRegistrationFile
            val indexOfOriginalTargetImageFile = msg.files.indexOf(originalTargetImageFile)
            val obsoleteFile = new ObsoleteRegistrationFile(uri = originalTargetImageFile.uri)
            val newTargetImageFile = originalTargetImageFile.copy(uri = newTargetImageFilePath)
            val newFiles = msg.files.updated(indexOfOriginalTargetImageFile, newTargetImageFile) :+ obsoleteFile
            val newMsg = msg.copy(files = newFiles)
            sendToRecipients(newMsg)
          case _ ⇒
            log.warning("Metadata could not be added")
            sendToRecipients(msg)
        }
      })
    }
  }

  private def createNewTargetFilename(targetImage: RegistrationImage): Option[String] = try {
    val ImagiFilename = new Regex("""(.*)[.](.*)""")

    val targetFile = new File(targetImage.uri)
    targetFile.getName match {
      case ImagiFilename(name, extension) ⇒
        val newFilename = "%s_withMetadata.%s".format(name, extension)
        Some(new File(outputDir, newFilename).getAbsolutePath)
      case _ ⇒
        log.error("Error determining new filename. [Cannot separate extension from filename]")
        None
    }
  } catch {
    case e: Exception ⇒
      log.error("Error determining new filename. [%s]".format(e.getMessage))
      None
  }

  private def prettyJson(json: String): String = try {
    val ast = JsonParser.parse(json)
    Printer.pretty(JsonAST.render(ast))
  } catch {
    case e: Exception ⇒
      log.warning("Error whilst making a pretty print version of a json text. [%s]".format(e.getMessage))
      json
  }

}
