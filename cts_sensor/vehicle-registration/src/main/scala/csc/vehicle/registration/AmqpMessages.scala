/*
 * Copyright (C) 2015. CSC <http://www.csc.com>
 */

package csc.vehicle.registration

import java.util.Date

import com.rabbitmq.client.AMQP
import csc.gantry.config.{ ActiveCertificate, ComponentCertificate }
import csc.timeregistration.config.SoftwareChecksum
import csc.vehicle.message.Lane

/**
 * Message class for vehicle registration keepalives. For each lane we maintain the last processed time and a
 * delay we calculate from the difference the of registration creation time and the time when it arrives in the
 * registration completion component ([[StoreVehicleRegistrationAmqp]]). We produce an exponential moving average
 * of the delays (see Wikipedia) and using this delay and a constant the last processed time is corrected. This time
 * is sent periodically using the keepalive message. Later this keepalive value can be used to correct the end time of
 * the processing (matching) window.
 *
 * @author csomogyi
 */
case class VehicleRegistrationKeepalive(lane: Lane, processedTime: Long)

/**
 * Message used to the send component certificate of the vehicle registration application. Sent using during the startup
 * of the
 * @param systemId the system id of the current vehicle registration
 * @param gantryId the gantry id of the current vehicle registration
 * @param activeCertificate the certificate created using [[SoftwareChecksum.createHashFromCombinedRuntime()]]
 */
case class VehicleRegistrationCertificate(systemId: String, gantryId: String, activeCertificate: ActiveCertificate)
