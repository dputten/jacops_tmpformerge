package csc.vehicle.instruct.alertwatch

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

import akka.actor.ActorRef
import java.util.concurrent.atomic.AtomicReference
import csc.akka.logging.DirectLogging
import csc.config.Path
import scala.collection.JavaConversions._
import org.apache.curator.framework.CuratorFramework
import org.apache.curator.framework.recipes.cache.{ PathChildrenCacheEvent, PathChildrenCache, PathChildrenCacheListener }

case class AlertAdded(path: Path)
case class AlertRemoved(path: Path)
case class AlertUpdated(path: Path)

/**
 * This class watch the zookeeper trigger Node. When created it sends the trigger to the SelftestTrigger
 * @param client the zookeeper curator
 * @param parentPath  The parent path of the system nodes
 * @param watchActor the watch actor
 */
class WatchAlert(client: CuratorFramework, parentPath: String, watchActor: ActorRef) extends PathChildrenCacheListener with DirectLogging {

  private val cache = new AtomicReference[Option[PathChildrenCache]](None)
  //Path can't end with a / because otherwise the calls start and get children will hang
  private val path = if (parentPath.charAt(parentPath.size - 1) == '/') {
    parentPath.substring(0, parentPath.size - 1)
  } else {
    parentPath
  }

  /**
   * Starts the watch for changes on the systems
   */
  def start() {
    if (cache.compareAndSet(None, Some(new PathChildrenCache(client, path, false)))) {
      cache.get.foreach(pathCache ⇒ {
        pathCache.start()
        //add listener
        pathCache.getListenable().addListener(this)
      })
    }
  }

  /**
   * Stops the watch for changes on the systems
   */
  def stop() {
    cache.getAndSet(None).foreach(pathCache ⇒ {
      pathCache.getListenable().removeListener(this)
      pathCache.close()
    })
  }

  /**
   * One of the children has been changed
   * @param client the CuratorFramework
   * @param event the indication what has changed
   */
  def childEvent(client: CuratorFramework, event: PathChildrenCacheEvent) {
    if (event.getType == PathChildrenCacheEvent.Type.CHILD_ADDED) {
      val path = Path(event.getData.getPath)
      watchActor ! new AlertAdded(path)
    }
    if (event.getType == PathChildrenCacheEvent.Type.CHILD_REMOVED) {
      val path = Path(event.getData.getPath)
      watchActor ! new AlertRemoved(path)
    }
    if (event.getType == PathChildrenCacheEvent.Type.CHILD_UPDATED) {
      val path = Path(event.getData.getPath)
      watchActor ! new AlertUpdated(path)
    }
  }
}