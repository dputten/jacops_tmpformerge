/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package image

import csc.common.Extensions._
import base._
import scala.xml._
import java.io.{ FileFilter, File }
import java.util.Date
import java.text.SimpleDateFormat
import akka.actor.{ ActorLogging, ActorRef }
import csc.vehicle.message._
import csc.akka.logging.DirectLogging
import akka.event.LoggingReceive

/**
 * Main actor entry point for processing each received SensorDataReadyMessage message
 * @param maxDelayInMillisec max number of milliseconds that a received message from honac may be delayed
 * @param recognizers are the license plate recognizers
 */
class JoinHonacData(maxDelayInMillisec: ⇒ Int, recognizers: Set[ActorRef]) extends RecipientList(recognizers) with Timing with ActorLogging {

  def receive = LoggingReceive {
    case msg: VehicleRegistrationMessage ⇒ {
      performanceLogInterval(log, 3, 47, "Process Honac data", msg.eventId.getOrElse("UnKnown"),
        {
          msg.getRelayFile() match {
            case None ⇒ log.warning("Registration received without file")
            case Some(file) ⇒ {
              val honacManager = new HonacManager(pathToFile = file.uri)
              honacManager.process

              val currentDateTime = new Date
              val diffInMilliseconds = (currentDateTime.getTime - honacManager.eventTimestamp.getTime) / 10000

              if (diffInMilliseconds > maxDelayInMillisec) {
                log.warning("eventId=[%s] has a delay of [%s] milliseconds. Honac eventTimestamp=[%s], current date=[%s]".format(msg.eventId, diffInMilliseconds, honacManager.eventTimestamp, currentDateTime))
              }
              log.info("Perf eventId=[%s] point 0: EventTimestamp %s".format(msg.eventId, new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS") format honacManager.eventTimestamp))
              val newLane = msg.lane.copy(sensorGPS_longitude = honacManager.longitude, sensorGPS_latitude = honacManager.latitude)
              val fileColor = new RegistrationImage(honacManager.imageColor, ImageFormat.JPG_RGB, VehicleImageType.Overview, ImageVersion.Original, honacManager.eventTimestamp.getTime)
              val fileGray = new RegistrationImage(honacManager.imageGreyScale, ImageFormat.JPG_GRAY, VehicleImageType.Overview, ImageVersion.Original, honacManager.eventTimestamp.getTime)
              val event = new SensorEvent(honacManager.eventTimestamp.getTime, new PeriodRange(honacManager.eventTimestamp.getTimezoneOffset, honacManager.eventTimestamp.getTime), "Honac")
              val filteredFiles = msg.files.filterNot(file ⇒ file.isInstanceOf[RegistrationRelay])
              val updatedFile = new RegistrationArchive(uri = file.uri, sha = file.sha)
              val output = msg.copy(lane = newLane, event = event, files = filteredFiles :+ updatedFile :+ fileColor :+ fileGray)
              sendToRecipients(output)
            }
          }
        })
    }
  }
}

/**
 * Main class that handles all Honac specific functionality
 */
class HonacManager(pathToFile: String) extends DirectLogging {
  val expected_extension = "tar"
  val datetime_format = "yyyy-MM-dd HH:mm:ss"

  var basePath = ""
  var imageGreyScale = ""
  var imageColor = ""
  var xml = ""
  var date = ""
  var time = ""
  var latitude = 0f
  var longitude = 0f

  require(pathToFile != null, "Missing pathToFile")
  require(pathToFile.trim.length != 0, "Empty pathToFile")

  val file = new File(pathToFile)

  if (!file.isFile) {
    throw new Exception("Given path is not a file [" + pathToFile + "]")
  }

  if (!file.getExtension.isDefined || file.getExtension.get != expected_extension) {
    throw new Exception("Given file must have extension [" + expected_extension + "], received [" + file.getExtension + "]")
  }

  /**
   * Main entry point for for processing a received tar file. It start with unpacking the tar file and procesing the included xml file
   */
  def process {
    val folderToStore = file.getAbsolutePath + ".unpacked"
    val unpacked = file.unpack(pathToStore = folderToStore)
    if (!unpacked) {
      log.error("Upacking failed for file [%s]".format(file.getAbsolutePath))
    } else {
      parseXml(path = folderToStore)
    }
  }

  /**
   * eventTimestamp is a Date combined of the DATA and TIME xml element
   */
  def eventTimestamp: Date = {
    (date + " " + time).toDate(datetime_format)
  }

  /**
   * used for parsing the xml file that is included within the tar file
   * @param path is the location where the xml file is located
   */
  private def parseXml(path: String) {
    val folder = new File(path)
    var xmlFile = folder.listFiles(new FileFilter {
      def accept(file: File): Boolean = {
        file.getExtension.get == "xml"
      }
    })
    if (xmlFile.length > 1) {
      log.error("path [%s] does not contain a xml file".format(path))
    } else {
      val xmlData = XML.loadFile(xmlFile.head)
      basePath = path
      xml = xmlFile.head.getName
      date = (xmlData \ "DATE").text
      time = (xmlData \ "TIME").text

      try {
        latitude = (xmlData \\ "GPS_INFO" \\ "LATITUDE").text.toFloat
      } catch {
        case e: Exception ⇒ log.warning("failed retrieving latitude, setting latitude to " + latitude)
      }

      try {
        longitude = (xmlData \\ "GPS_INFO" \\ "LONGITUDE").text.toFloat
      } catch {
        case e: Exception ⇒ log.warning("failed retrieving longitude, setting longitude to " + longitude)
      }

      for (entry ← xmlData \\ "IMAGE" \\ "IMAGE_FILENAME") {
        val type_value = entry.attribute("type").get.text
        val text = entry.text
        if (type_value == "BW") {
          imageGreyScale = folder.getAbsolutePath + "/" + text
        } else if (type_value == "COLOR") {
          imageColor = folder.getAbsolutePath + "/" + text
        }
      }
    }
  }
}