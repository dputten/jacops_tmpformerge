/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package image

import java.lang.Long
import collection.mutable.HashMap
import java.util.Date
import csc.vehicle.message.{ MonitorStatusMessage, MonitorCameraMessage, MonitorSensorMessage, MonitorRadarMessage }
import csc.vehicle.config.ApplicationInfo

/**
 * Monitor the sensors. Join the different monitor data of one sensor into one message.
 */
class SensorMonitor(host: String, port: Int) {
  val sensors = HashMap[String, MonitorSensorMessage]()
  val applicationVersion = new ApplicationInfo
  /**
   * Received an update of the Radar
   */
  def updateRadar(msg: MonitorRadarMessage) {
    sensors.get(msg.sensor.laneId) match {
      case None ⇒ {
        sensors.put(msg.sensor.laneId, new MonitorSensorMessage(msg.sensor, lastCameraMsg = None, lastRadarMsg = Some(msg.lastRadarMsg), radarMsg = msg.message))
      }
      case Some(oldStatus) ⇒ {
        sensors.put(msg.sensor.laneId, new MonitorSensorMessage(msg.sensor, lastCameraMsg = oldStatus.lastCameraMsg, lastRadarMsg = Some(msg.lastRadarMsg), radarMsg = msg.message))
      }
    }
  }
  /**
   * Received an update of the Camera
   */
  def updateCamera(msg: MonitorCameraMessage) {
    sensors.get(msg.sensor.laneId) match {
      case None ⇒ {
        sensors.put(msg.sensor.laneId, new MonitorSensorMessage(msg.sensor, lastCameraMsg = Some(msg.lastCameraMsg), lastRadarMsg = None))
      }
      case Some(oldStatus) ⇒ {
        sensors.put(msg.sensor.laneId, new MonitorSensorMessage(msg.sensor, lastCameraMsg = Some(msg.lastCameraMsg), lastRadarMsg = oldStatus.lastRadarMsg, radarMsg = oldStatus.radarMsg))
      }
    }
  }

  /**
   * Get the monitor Status list of known sensors
   */
  def getSensorStatus(): List[MonitorSensorMessage] = {
    sensors.values.toList
  }

  /**
   * Create the update message with the current monitor status
   */
  def getUpdateMessage(now: Date): MonitorStatusMessage = {
    new MonitorStatusMessage(host, port, now, applicationVersion.getVersion(), applicationVersion.getStartTime(), getSensorStatus())
  }
}