package image

//copyright CSC 2010
import base._
import java.util.Date
import akka.actor.{ ActorLogging, ActorRef, Actor }
import csc.vehicle.message.{ AdminSendMonitor, MonitorRadarMessage, MonitorCameraMessage }

/**
 * This class handle admin requests.
 */
class ProcessAdmin(imageHost: ⇒ String, imagePort: ⇒ Int, systemMonitor: Option[ActorRef] = None) extends Actor with ActorLogging {
  val monitor = new SensorMonitor(host = imageHost, port = imagePort)
  def receive = {
    /**
     * received a update camera monitor message
     */
    case msg: MonitorCameraMessage ⇒ {
      monitor.updateCamera(msg)
    }
    /**
     * received a update radar monitor message
     */
    case msg: MonitorRadarMessage ⇒ {
      monitor.updateRadar(msg)
    }
    /**
     * received a request to send a update monitor message
     */
    case msg: AdminSendMonitor ⇒ {
      systemMonitor foreach (actor ⇒ {
        actor ! monitor.getUpdateMessage(new Date())
        log.debug("Sent MonitorUpdateStatus Message")
      })
    }
  }
}