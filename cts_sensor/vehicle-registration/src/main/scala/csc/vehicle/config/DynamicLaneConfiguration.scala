/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.config

import akka.actor.ActorSystem
import akka.agent.Agent
import akka.event.Logging
import csc.gantry.config.{ CameraConfig, CorridorsConfig, ImageMaskVertex, SystemConfig, _ }
import csc.vehicle.message.Lane

/**
 * Implementation of LaneConfiguration returning the dynamic configuration wrappers.
 *
 * @param laneConfig The laneConfig used to get original configuration
 * @param system An actor system
 */
class DynamicLaneConfiguration(laneConfig: LaneConfiguration, system: ActorSystem) extends LaneConfiguration {
  implicit val executor = system.dispatcher
  val laneAgent = Agent(Map[String, LaneConfig]())
  val systemAgent = Agent(Map[String, SystemConfig]())
  val corridorAgent = Agent(Map[String, CorridorsConfig]())
  val log = Logging.getLogger(system, getClass)

  /**
   * Read the configuration and Update it to be used by the configuration wrappers
   */
  def update() = {
    val lanes = laneConfig.getLanes()
    val map = lanes.map(cfg ⇒ cfg.lane.laneId -> cfg).toMap
    laneAgent.send(map)

    val systems = lanes.map(_.lane.system).distinct
    val systemConfig = systems.flatMap(sys ⇒ laneConfig.getSystemConfig(sys))
    val systemConfigMap = systemConfig.map(cfg ⇒ cfg.id -> cfg).toMap
    systemAgent.send(systemConfigMap)

    val corridorConfigMap = systems.flatMap(sys ⇒ {
      laneConfig.getCorridorsConfig(sys).map(cfg ⇒ sys -> cfg)
    }).toMap
    corridorAgent.send(corridorConfigMap)

    log.info("Updating configuration")
  }

  def getAllSystemIds(): Seq[String] = {
    laneConfig.getAllSystemIds()
  }

  /**
   * Get all lane configuration items of the specified gantry
   * @param systemId the system of the gantry
   * @param gantryId the requested gantry
   * @return List of LaneConfig
   */
  def getLanesForGantry(systemId: String, gantryId: String): List[LaneConfig] = {
    val lanes = laneConfig.getLanesForGantry(systemId, gantryId)
    lanes.map(cfg ⇒ DynamicLaneConfig(laneAgent, cfg))
  }
  /**
   * Get all lane configuration items of the specified route
   * @param systemId the requested route
   * @return List of LaneConfig
   */
  def getLanesForRoute(systemId: String): List[LaneConfig] = {
    val lanes = laneConfig.getLanesForRoute(systemId)
    lanes.map(cfg ⇒ DynamicLaneConfig(laneAgent, cfg))
  }

  /**
   * Get the configuration of all known lanes for all routes
   * @return List of all LaneConfig
   */
  def getLanes(): List[LaneConfig] = {
    val lanes = laneConfig.getLanes()
    lanes.map(cfg ⇒ DynamicLaneConfig(laneAgent, cfg))
  }

  /**
   * Get system configuration of the specified system
   * @return
   */
  def getSystemConfig(system: String): Option[SystemConfig] = {
    laneConfig.getSystemConfig(system) match {
      case Some(cfg) ⇒ Some(new DynamicSystemConfig(systemAgent, cfg))
      case None      ⇒ None
    }
  }

  /**
   * Get corridor configuration of the specified system
   * @return
   */
  def getCorridorsConfig(system: String): Option[CorridorsConfig] = corridorAgent().get(system)

  def getMultiLaneCameras: List[MultiLaneCamera] = laneConfig.getMultiLaneCameras

}

/**
 * The Dynamic wrapper of the LaneConfig class.
 * @param agent The agent where to find the current configuration
 */
abstract class DynamicLaneConfig(agent: Agent[Map[String, LaneConfig]]) extends LaneConfig {
  type Config <: LaneConfig
  /**
   * The last known correct value of this lane.
   * It is possible that this instance is removed in the new configuration.
   * When this happens this instance is probably not been used any more or only at the moment of removal
   * To prevent the system to get crazy, we always returns the last known value
   */
  var lastVersion: Config

  def lane: Lane = {
    updateLastVersion()
    lastVersion.lane
  }
  def camera: CameraConfig = {
    updateLastVersion()
    lastVersion.camera
  }
  def imageMask: Option[ImageMask] = {
    updateLastVersion()
    lastVersion.imageMask.map(mask ⇒ new DynamicImageMask(agent, lastVersion))
  }
  def recognizeOptions: Map[String, String] = {
    updateLastVersion()
    lastVersion.recognizeOptions
  }

  /**
   * Update the lastVersion when we can find the new value
   */
  protected def updateLastVersion() {
    val newVersion = agent().get(lastVersion.lane.laneId)
    lastVersion = newVersion.map { _.asInstanceOf[Config] }.getOrElse(lastVersion)
  }
}

/**
 * The Dynamic wrapper of the ImageMask class.
 * @param agent The agent where to find the current configuration
 * @param start The initial value of the ImageMask wrapper
 */
class DynamicImageMask(agent: Agent[Map[String, LaneConfig]], start: LaneConfig) extends ImageMask {
  /**
   * The last known correct value of this lane.
   * It is possible that this instance is removed in the new configuration.
   * When this happens this instance is probably not been used any more or only at the moment of removal
   * To prevent the system to get crazy, we always returns the last known value
   */
  var lastVersion = start.imageMask.get

  def vertices: List[ImageMaskVertex] = {
    updateLastVersion()
    lastVersion.vertices
  }

  /**
   * Update the lastVersion when we can find the new value
   */
  private def updateLastVersion() {
    val newVersion = agent().get(start.lane.laneId)
    newVersion match {
      case Some(cfg) if (cfg.imageMask.isDefined) ⇒ lastVersion = cfg.imageMask.get
      case _                                      ⇒ //don't update
    }
  }
}

object DynamicLaneConfig {
  def apply(agent: Agent[Map[String, LaneConfig]], laneConfig: LaneConfig) = {
    laneConfig match {
      case pirConfig: PirRadarCameraSensorConfig ⇒ {
        new DynamicPirRadarCameraSensorConfig(agent, pirConfig)
      }
      case pirConfig: PirCameraSensorConfig ⇒ {
        new DynamicPirCameraSensorConfig(agent, pirConfig)
      }
      case radarCameraConfig: DoubleRadarCameraSensorConfig ⇒ {
        new DynamicDoubleRadarCameraSensorConfig(agent, radarCameraConfig)
      }
      case radarCameraConfig: RadarCameraSensorConfig ⇒ {
        new DynamicRadarCameraSensorConfig(agent, radarCameraConfig)
      }
      case honacConfig: HonacSensorConfig ⇒ {
        new DynamicHonacSensorConfig(agent, honacConfig)
      }
      case iRoseConfig: IRoseSensorConfig ⇒ {
        new DynamicIRoseSensorConfig(agent, iRoseConfig)
      }
      case cfg: CameraOnlyConfig ⇒ {
        new DynamicCameraOnlyConfig(agent, cfg)
      }
    }
  }
}

class DynamicPirRadarCameraSensorConfig(agent: Agent[Map[String, LaneConfig]], start: PirRadarCameraSensorConfig) extends DynamicLaneConfig(agent) with PirRadarCameraSensorConfig {
  type Config = PirRadarCameraSensorConfig
  var lastVersion = start

  def radar = {
    updateLastVersion()
    lastVersion.radar
  }
  def pir = {
    updateLastVersion()
    lastVersion.pir
  }
}
class DynamicPirCameraSensorConfig(agent: Agent[Map[String, LaneConfig]], start: PirCameraSensorConfig) extends DynamicLaneConfig(agent) with PirCameraSensorConfig {
  type Config = PirCameraSensorConfig
  var lastVersion = start

  def pir = {
    updateLastVersion()
    lastVersion.pir
  }
}

class DynamicDoubleRadarCameraSensorConfig(agent: Agent[Map[String, LaneConfig]], start: DoubleRadarCameraSensorConfig) extends DynamicLaneConfig(agent) with DoubleRadarCameraSensorConfig {
  type Config = DoubleRadarCameraSensorConfig
  var lastVersion = start

  def radar = {
    updateLastVersion()
    lastVersion.radar
  }

  def radarLength = {
    updateLastVersion()
    lastVersion.radarLength
  }
}

class DynamicRadarCameraSensorConfig(agent: Agent[Map[String, LaneConfig]], start: RadarCameraSensorConfig) extends DynamicLaneConfig(agent) with RadarCameraSensorConfig {
  type Config = RadarCameraSensorConfig
  var lastVersion = start

  def radar = {
    updateLastVersion()
    lastVersion.radar
  }
}

class DynamicHonacSensorConfig(agent: Agent[Map[String, LaneConfig]], start: HonacSensorConfig) extends DynamicLaneConfig(agent) with HonacSensorConfig {
  type Config = HonacSensorConfig
  var lastVersion = start
}

class DynamicIRoseSensorConfig(agent: Agent[Map[String, LaneConfig]], start: IRoseSensorConfig) extends DynamicLaneConfig(agent) with IRoseSensorConfig {
  type Config = IRoseSensorConfig
  var lastVersion = start

  def seqConfig = {
    updateLastVersion()
    lastVersion.seqConfig
  }
}

class DynamicCameraOnlyConfig(agent: Agent[Map[String, LaneConfig]], start: CameraOnlyConfig) extends DynamicLaneConfig(agent) with CameraOnlyConfig {
  type Config = CameraOnlyConfig
  var lastVersion = start

}

class DynamicSystemConfig(agent: Agent[Map[String, SystemConfig]], start: SystemConfig) extends SystemConfig {
  var lastVersion = start

  def id: String = lastVersion.id

  def name: String = {
    updateLastVersion()
    lastVersion.name
  }
  def description: String = {
    updateLastVersion()
    lastVersion.description
  }
  def maxSpeed: Int = {
    updateLastVersion()
    lastVersion.maxSpeed
  }
  def compressionFactor: Int = {
    updateLastVersion()
    lastVersion.compressionFactor
  }
  def nrDaysKeepTrafficData: Int = {
    updateLastVersion()
    lastVersion.nrDaysKeepTrafficData
  }
  def nrDaysKeepViolations: Int = {
    updateLastVersion()
    lastVersion.nrDaysKeepViolations
  }
  def region: String = {
    updateLastVersion()
    lastVersion.region
  }
  def roadNumber: String = {
    updateLastVersion()
    lastVersion.roadNumber
  }
  def roadPart: String = {
    updateLastVersion()
    lastVersion.roadPart
  }
  def typeCertificates: Seq[MeasurementMethodInstallationType] = {
    updateLastVersion()
    lastVersion.typeCertificates
  }
  def serialNumber: SerialNumber = {
    updateLastVersion()
    lastVersion.serialNumber
  }
  def activeChecksums: Seq[ActiveCertificate] = {
    updateLastVersion()
    lastVersion.activeChecksums
  }

  /**
   * Update the lastVersion when we can find the new value
   */
  protected def updateLastVersion() {
    val newVersion = agent().get(lastVersion.id)
    lastVersion = newVersion.map { case cfg: SystemConfig ⇒ cfg }.getOrElse(lastVersion)
  }

}
