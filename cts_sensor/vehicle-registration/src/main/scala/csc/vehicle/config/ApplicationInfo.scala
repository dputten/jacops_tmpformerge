package csc.vehicle.config

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

import java.util.Date
import java.net.URL
import csc.akka.logging.DirectLogging

/**
 * finding and storing the version and the starttime of the application
 * The start time is the time that the Actors are started and not when the application is completely started.
 */
class ApplicationInfo extends DirectLogging {
  val startTime = new Date
  log.info("Starting %s".format(getDetailedVersion()))

  private def findManifest(): Option[java.util.jar.Manifest] = {
    val className = getClass.getSimpleName();
    val classFileName = className + ".class";
    val resource = getClass.getResource(classFileName)
    val pathToThisClass = if (resource != null) resource.toString else ""

    val mark = pathToThisClass.indexOf("!");
    if (mark > 0) {
      var pathToManifest = pathToThisClass.substring(0, mark + 1);
      pathToManifest += "/META-INF/MANIFEST.MF";
      val stream = new URL(pathToManifest).openStream()
      try {
        return Some(new java.util.jar.Manifest(stream))
      } finally {
        stream.close()
      }
    } else {
      log.warning("Not running from Jar file. Manifest not present")
      None
    }
  }

  /**
   * Gets the current version
   */
  def getVersion(): String = {
    val manifest = findManifest
    if (manifest == None) {
      "Unknown"
    } else {
      manifest.get.getMainAttributes().getValue("Implementation-Version")
    }
  }

  /**
   * Gets the version with git information
   */
  def getDetailedVersion(): String = {
    val manifest = findManifest
    if (manifest == None) {
      "Unknown"
    } else {
      "%s %s".format(manifest.get.getMainAttributes().getValue("Implementation-Version"),
        manifest.get.getMainAttributes().getValue("GIT-Version"))
    }
  }

  /**
   * Show the start time of the application.
   */
  def getStartTime(): Date = startTime
}

