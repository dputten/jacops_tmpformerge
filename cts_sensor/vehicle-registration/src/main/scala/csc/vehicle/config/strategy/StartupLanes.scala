/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.config.strategy

trait StartupLanes {
  def startSystem(systemId: String)

  def stopSystem(systemId: String)

  def startAllLanes()
}