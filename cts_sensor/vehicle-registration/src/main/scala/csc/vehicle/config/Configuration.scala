/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.config

import base.{ FavorCriticalProcess, Mode }
import com.typesafe.config.{ Config, ConfigFactory, ConfigList }
import csc.akka.config.ConfigUtil
import csc.amqp.ServicesConfiguration
import csc.image.ImageTimeExtractor
import csc.vehicle.message.{ ImageVersion, Point, VehicleImageType }

import collection.JavaConverters._
import collection.mutable.ListBuffer
import csc.akka.logging.DirectLogging

import scala.concurrent.duration._
import csc.akka.config.ConfigUtil._
import csc.gantry.config._
import java.io.File

import csc.timeregistration.config.SoftwareChecksum
import csc.gantry.config.ComponentCertificate
import csc.curator.utils.{ Curator, CuratorToolsImpl }
import org.apache.curator.retry.RetryUntilElapsed
import org.apache.curator.framework.CuratorFrameworkFactory
import net.liftweb.json.DefaultFormats
import csc.json.lift.EnumerationSerializer
import csc.curator.utils.xml.{ CuratorToolsImplXml, XmlReader }
import csc.watchdog.{ ServiceConfig, WatchdogConfig }

import scala.collection.JavaConversions._

/**
 * Reading the configuration file and create the VehicleRegistration Configuration classes
 * @param configuration, The application configuration file
 */
class Configuration(configuration: Option[Config]) extends DirectLogging {
  private val rootConfig = "vehicle-registration"
  private val fallback = ConfigFactory.parseResources(this.getClass, "/defaultVR.conf")
  private val config = configuration match {
    case None      ⇒ fallback
    case Some(cfg) ⇒ cfg.withFallback(fallback)
  }
  var curator: Option[Curator] = None

  lazy val laneConfiguration = createDynamicConfig(config)
  lazy val globalConfig = createGlobalConfig(config, laneConfiguration.getMultiLaneCameras)
  lazy val jmxService = createJmxConfig(config)

  val systemId = config.getReplacedString(rootConfig + ".location.systemId")
  val gantryIds = config.getStringList(rootConfig + ".location.gantryIds")

  def getZookeeperCurator(zkServerQuorum: String): Curator = {
    curator.getOrElse {
      val retryPolicy = new RetryUntilElapsed(3.days.toMillis.toInt, 5000)
      val client = CuratorFrameworkFactory.newClient(zkServerQuorum, retryPolicy)
      client.start()
      val formats = DefaultFormats + new EnumerationSerializer(VehicleImageType)
      val impl = new CuratorToolsImpl(Some(client), log, formats)
      curator = Some(impl)
      impl
    }
  }

  def getXmlCurator(fileLocation: String): Curator = {
    curator.getOrElse {
      val formats = DefaultFormats + new EnumerationSerializer(VehicleImageType)
      val impl = new CuratorToolsImplXml(new XmlReader(fileLocation, log), log, formats)
      curator = Some(impl)
      impl
    }
  }

  /**
   * Create the LaneConfiguration implementation
   * @param config
   * @return The Laneconfiguration implementation which has to be used
   */
  def createDynamicConfig(config: Config): LaneConfiguration = {
    val service = config.getReplacedString(rootConfig + ".laneConfig.zookeeper.service")

    if (service == "on") {
      log.info("Using zookeeper for LaneConfiguration")
      var curator: Curator = null
      var servers = ""
      // Quickfix to prevent two conf files being needed, since DEV/TEST/PROD use registration.conf
      // TODO This should never be hardcoded
      val appConfig: Config = ConfigFactory.load("registration.conf")

      appConfig.getString("curator_implementation") match {
        case "xml" ⇒
          curator = getXmlCurator(appConfig.getString("curator_implementation_xml_file_location"))
        case "zookeeper" ⇒
          servers = config.getReplacedString(rootConfig + ".laneConfig.zookeeper.servers")
          curator = getZookeeperCurator(servers)
      }

      val zookeeperPath = "/ctes/systems/"
      config.getReplacedString(rootConfig + ".certificate-check.service") match {
        case "on" ⇒ {
          // when certificate-check.use-amqp is on the code goes through the amqpCertificateActor. Not handled here.
          if (config.getReplacedString(rootConfig + ".certificate-check.use-amqp") != "on") {
            SoftwareChecksum.createHashFromRuntimeClass(classOf[ImageTimeExtractor]) match {
              case Some(softwareChecksum) ⇒
                val compCertificate = ComponentCertificate(softwareChecksum.fileName, softwareChecksum.checksum)
                val vehicleCertifier = new VehicleCertifier(curator)

                // when using the curator xml implementation this will fail.
                // In de call stack there will be a delete, update or set and these are not implemented.
                try {
                  vehicleCertifier.processCertificate(ActiveCertificate(System.currentTimeMillis(), compCertificate))
                } catch {
                  case e: Exception ⇒
                    config.getString("curator_implementation") match {
                      case "xml" ⇒
                        log.error("laneConfig.zookeeper.service is used with xml curator: %s".format(e))
                      case "zookeeper" ⇒
                        log.error("laneConfig.zookeeper.service is used with zookeeper curator: %s".format(e))
                    }
                }
              case None ⇒
                //very strange couldn't find checksum
                log.warning("Couldn't find checksum for ViolationQualifier")
            }
          }
        }
        case anything ⇒ log.info("Certificate is not checked (service='%s').".format(anything))
      }
      val labelPath = {
        val path = config.getReplacedString(rootConfig + ".laneConfig.zookeeper.labelPath", "")
        if (path.isEmpty) {
          None
        } else {
          Some(path)
        }
      }
      new ZookeeperLaneConfiguration(curator, zookeeperPath, labelPath)

    } else {
      val fileService = config.getReplacedString(rootConfig + ".laneConfig.file.service")
      if (fileService == "on") {
        val file = config.getReplacedString(rootConfig + ".laneConfig.file.fileName")
        log.info("Using jsonFile %s for LaneConfiguration".format(file))

        val cameraFile = ConfigUtil.getReplacedString(config, rootConfig + ".laneConfig.file.cameraFilename")
        log.info("Using camera jsonFile %s for LaneConfiguration".format(cameraFile))

        val cert = config.getReplacedString(rootConfig + ".certificate.nmi")
        val jpgQuality = config.getReplacedInteger(rootConfig + ".decorate.quality")
        var compress = 100 - jpgQuality
        if (compress > 100) {
          compress = 100
        } else if (compress < 0) {
          compress = 0
        }

        val sysCfg = new SystemConfigImpl(id = "all",
          name = "",
          description = "",
          serialNumber = SerialNumber(""),
          typeCertificates = Seq(new MeasurementMethodInstallationType(0L,
            new MeasurementMethodType(typeDesignation = "",
              category = "A",
              unitSpeed = "km/h",
              unitRedLight = "s",
              unitLength = "m",
              restrictiveConditions = "",
              displayRange = "20-250 km/h",
              temperatureRange = "temperatuurbereik -10 to 50 graden Celsius",
              permissibleError = "toelatbare fout 3%",
              typeCertificate = new TypeCertificate(cert, 0, List())))),
          maxSpeed = 130,
          compressionFactor = compress,
          nrDaysKeepTrafficData = 0,
          nrDaysKeepViolations = 0,
          region = "",
          roadNumber = "",
          roadPart = "",
          activeChecksums = Seq())

        new FileLaneConfiguration(file, Some(sysCfg), cameraFile)
      } else {
        log.info("Using Empty LaneConfiguration")
        new EmptyLaneConfiguration()
      }
    }
  }

  /**
   * Create the JMX configuration
   * @param config
   * @return JMXConfig when JMX is configuterd other wise None
   */
  def createJmxConfig(config: Config): Option[JMXConfig] = {
    val service = config.getReplacedString(rootConfig + ".jmx.service")
    if (service == "on") {
      val host = config.getReplacedString(rootConfig + ".jmx.host")
      val pwdFile = config.getReplacedString(rootConfig + ".jmx.passwordFile")
      val accFile = config.getReplacedString(rootConfig + ".jmx.accessFile")
      val rmi = config.getReplacedInteger(rootConfig + ".jmx.rmiPort")
      val jmx = config.getReplacedInteger(rootConfig + ".jmx.jmxPort")
      Some(new JMXConfig(host = host, jmxPasswordFile = pwdFile, jmxAccessFile = accFile, rmiPort = rmi, jmxPort = jmx))
    } else {
      None
    }
  }

  /**
   * Create the AMQP configuration
   * @param config
   * @return AmqpConfig when AMQP is configuterd other wise None
   */
  def createAmqpConfig(config: Config): AmqpConfig = {
    val service = config.getReplacedString(rootConfig + ".amqp.service")
    if (service == "on") {
      val amqpUri = config.getReplacedString(rootConfig + ".amqp.amqp-uri")
      val actorName = config.getReplacedString(rootConfig + ".amqp.actor-name")
      val reconnectDelay = config.getReplacedMilliseconds(rootConfig + ".amqp.reconnect-delay").millis
      AmqpConfig(amqpUri = amqpUri, actorName = actorName, reconnectDelay = reconnectDelay)
    } else {
      throw new IllegalStateException("AMQP config is mandatory")
    }
  }

  def createAmqpCertificateCheckConfig(config: Config): Option[AmqpCertificateCheckConfig] = {
    val service = config.getReplacedString(rootConfig + ".certificate-check.service")
    val useAmqp = config.getReplacedString(rootConfig + ".certificate-check.use-amqp")
    if (service == "on" && useAmqp == "on") {
      val registrationReceiverEndpoint = config.getReplacedString(rootConfig + ".certificate-check.registration-receiver-endpoint")
      val registrationReceiverRouteKey = config.getReplacedString(rootConfig + ".certificate-check.registration-receiver-route-key")
      val certificateComponentName = config.getReplacedString(rootConfig + ".certificate-check.certificateComponentName")

      Some(AmqpCertificateCheckConfig(registrationReceiverEndpoint, registrationReceiverRouteKey, certificateComponentName))
    } else {
      None
    }
  }

  /**
   * Create the global configuration structure
   * @param config
   * @return VehicleRegistrationConfig the vehicle configuration
   */
  private def createGlobalConfig(config: Config, multiLaneCameras: List[MultiLaneCamera]): VehicleRegistrationConfiguration = {
    val maxBufferLen = config.getReplacedInteger(rootConfig + ".joining.maxBufferLen")
    val timeout = config.getReplacedInteger(rootConfig + ".joining.bufferTimeout")
    val shift = config.getReplacedInteger(rootConfig + ".joining.shiftPeriod")
    val jpgConfig = createJPGConfig(config)
    val recognizeConfig = createRecognizeConfig(config)
    val guards = createGuards(config)
    val registrationCompletion = createCompletionConfig(config)
    val openDoor = createOpenDoor(config)

    val relayAdapterConfig: RelayAdapterConfig = config.configOpt(rootConfig + ".relaySensorAmqp") match {
      case None      ⇒ CamelRelayAdapterConfig(config.getReplacedString(rootConfig + ".relaySensorData.uri"))
      case Some(cfg) ⇒ AmqpRelayAdapterConfig(new ServicesConfiguration(cfg))
    }

    val signConfig = createSignConfig(config)
    val maskConfig = createMaskConfig(config)
    val exifConfig = createExifConfig(config)
    val testImagePattern = config.getReplacedString(rootConfig + ".readImage.testImagePattern")
    val iRoseDataConfig = createIRoseDataConfig(config) //TEMPORARY
    val configFreq = config.getReplacedInteger(rootConfig + ".laneConfig.configUpdateFrequency", 60000)

    val recognizerTransportCfg = createRecognizeTransportConfig
    val classifierConfig = createClassifierConfig

    val amqp = createAmqpConfig(config)
    val amqpCertCheck = createAmqpCertificateCheckConfig(config)
    val cleanupCfg = getCleanupConfig(config)
    val joinConfig = JoinEventConfig(maxBufferLen, timeout.seconds, shift)
    val certConfig = CertConfig(signConfig, amqpCertCheck)
    val triggerMaskConfig = getTriggerMaskConfig(config)
    val doubleRegistrationFilterConfig = getDoubleRegistrationFilterConfig(config)
    val dualClockConfig = getDualClockConfig(config)
    val gantryControl = getGantryControl(config)

    val imageConfig = ImageConfig(testImagePattern, jpgConfig, recognizeConfig,
      maskConfig, exifConfig, multiLaneCameras, triggerMaskConfig)

    new VehicleRegistrationConfig(
      imageConfig = imageConfig,
      joinEventConfig = joinConfig,
      guards = guards,
      registrationCompletion = registrationCompletion,
      openDoor = openDoor,
      relayAdapterConfig = relayAdapterConfig,
      certConfig = certConfig,
      iRose = iRoseDataConfig,
      configUpdateFrequency = configFreq.millis,
      watchdogConfig = createWatchdogConfig,
      recognizerTransport = recognizerTransportCfg,
      classifierConfig = classifierConfig,
      amqpConfig = amqp,
      cleanupConfig = cleanupCfg,
      doubleRegistrationFilterConfig = doubleRegistrationFilterConfig,
      dualClockConfig = dualClockConfig,
      gantryControlConfig = gantryControl,
      workflowConfig = createWorkflowConfig)
  }

  def createWorkflowConfig: WorkflowConfig = config.configOpt(rootConfig + ".workflowConfig") match {
    case None ⇒ GenericWorkflowConfig
    case Some(cfg) ⇒ cfg.getStringOpt("impl") match {
      case Some("mq") ⇒ MQWorkflowConfig(
        cfg.getString("producerEndpoint"),
        cfg.getString("preparedQueue"),
        cfg.getString("recognizedQueue"),
        cfg.getString("classifiedQueue"),
        cfg.getInt("poolSize"))
      case _ ⇒ GenericWorkflowConfig
    }
  }

  def createWatchdogConfig: Option[WatchdogConfig] = config.configOpt(rootConfig + ".watchdog") match {
    case Some(cfg) ⇒ if (cfg.getBoolean("enabled")) {
      val serviceCommandPattern = cfg.getString("serviceCommandPattern")
      val hbTimeout = cfg.getFiniteDuration("heartbeatTimeout")
      val hbTolerance = cfg.getInt("heartbeatTolerance")

      val obj = cfg.getObject("services")
      val svcs = obj.toConfig
      val services: List[ServiceConfig] = obj.keySet().asScala.toList map { name: String ⇒
        val o = svcs.getObject(name)
        val svcCfg = o.toConfig

        val svcCmd = serviceCommandPattern.replace("_", name)

        ServiceConfig(name, svcCmd,
          svcCfg.optFiniteDuration("heartbeatTimeout").getOrElse(hbTimeout),
          svcCfg.getIntOpt("heartbeatTolerance").getOrElse(hbTolerance))
      }

      val amqp = new ServicesConfiguration(cfg.getConfig("amqp"))
      Some(WatchdogConfig(services, amqp))

    } else None

    case None ⇒ None
  }

  def createRecognizeTransportConfig: RecognizerTransportConfig =
    new RecognizerQueueConfig(config.getConfig(rootConfig + ".recognizerDelegate")) //new recognizer delegate, using AMQP

  def createClassifierConfig: Option[ClassifierConfig] =
    config.configOpt(rootConfig + ".classifier") map { c ⇒ new ClassifierConfig(c) }

  def getTriggerMaskConfig(config: Config): Option[TriggerMaskConfig] = {
    config.configOpt(rootConfig + ".triggerMaskConfig") map { c ⇒
      val dir = c.getString("dirMaskImage")
      val quality = c.getInt("maskQuality")

      c.configOpt("triggers") match {
        case None ⇒ SimpleTriggerMaskConfig(c.getInt("heightPct"), c.getInt("widthPct"), dir, quality)
        case Some(cfg) ⇒ DetailedTriggerMaskConfig(
          createTriggerMaskPoints(cfg.getList("LEFT")),
          createTriggerMaskPoints(cfg.getList("RIGHT")),
          dir,
          quality)
      }
    }
  }

  def createTriggerMaskPoints(cfg: ConfigList): List[Point] = cfg.unwrapped().toList map { list ⇒
    val l = list.asInstanceOf[java.util.List[Int]].toList
    Point(l.get(0), l.get(1))
  }

  def getDoubleRegistrationFilterConfig(config: Config): Option[DoubleRegistrationFilterConfig] = {
    ConfigUtil(config).getConfig(rootConfig + ".doubleRegistrationFilterConfig") map { c ⇒
      DoubleRegistrationFilterConfig(c.getInt("millisInterval"), c.getInt("bufferSize"))
    }
  }

  def getDualClockConfig(config: Config): Option[DualClockConfig] = {
    ConfigUtil(config).getConfig(rootConfig + ".dualClockConfig") map { c ⇒
      DualClockConfig(
        c.getInt("maxSampleBufferSize"),
        c.getLong("timeDiffThreshold"),
        c.getLong("criticalTimeDiffThreshold"),
        c.getLong("maxPhotoAge"))
    }
  }

  def getGantryControl(config: Config): Option[GantryControlConfig] = {
    ConfigUtil(config).getConfig(rootConfig + ".gantryControl") match {
      case Some(c) ⇒
        val cfg = ConfigUtil(c)
        cfg.getBoolean(cfg.getReplacedString("enabled"), false) match {
          case false ⇒ None
          case true ⇒
            val services = new ServicesConfiguration(c.getConfig("services"))
            Some(GantryControlConfig(services))
        }
      case None ⇒ None
    }
  }

  def getCleanupConfig(config: Config): Option[CleanupConfig] = {
    ConfigUtil(config).getConfig(rootConfig + ".cleanup") map { c ⇒
      CleanupConfig(c.getBoolean("dropWithoutPlate"))
    }
  }

  //TEMPORARY method
  private def createIRoseDataConfig(config: Config): Option[Map[String, IRoseSystemData]] = {
    if (config.hasPath(rootConfig + ".iRose")) {
      val iRoseDataConfig = config.getConfig(rootConfig + ".iRose")
      val laneIdList = iRoseDataConfig.getKeySet()
      var iRoseSystemData: Map[String, IRoseSystemData] = Map()
      for (laneId ← laneIdList) {
        val interval = iRoseDataConfig.getReplacedInteger(laneId + ".interval")
        val loopDistance = iRoseDataConfig.getReplacedInteger(laneId + ".loopDistance")
        iRoseSystemData += laneId -> new IRoseSystemData(interval = interval, loopDistance = loopDistance)
      }
      Some(iRoseSystemData)
    } else {
      None
    }
  }

  /**
   * Create the known priority guards
   * @param config
   * @return Map[String, FavorCriticalProcess]: key is the ID of the Guard and the value is the guard itself
   */
  private def createGuards(config: Config): Map[String, FavorCriticalProcess] = {
    val guardListConfig = config.getConfig(rootConfig + ".guards")
    val keyList = guardListConfig.getKeySet()
    var guards: Map[String, FavorCriticalProcess] = Map()
    for (key ← keyList) {
      val nr = guardListConfig.getReplacedInteger(key)
      guards += key -> new FavorCriticalProcess(nr)
    }
    guards
  }

  /**
   * Create the Opendoor configuration
   * @param config
   * @return OpenDoorConfig: the requested configuration
   */
  private def createOpenDoor(config: Config): OpenDoorConfig = {
    val expression = config.getReplacedString(rootConfig + ".doorDetector.expression")
    val doorDetectorUri = config.getReplacedString(rootConfig + ".doorDetector.uri")
    val host = config.getReplacedString(rootConfig + ".doorDetector.host")
    val port = config.getReplacedInteger(rootConfig + ".doorDetector.port")
    val doorOpenSignal = config.getReplacedString(rootConfig + ".doorDetector.doorOpenSignal")
    val description = config.getReplacedString(rootConfig + ".doorDetector.description")

    new OpenDoorConfig(
      expression = expression,
      doorDetectorUri = doorDetectorUri,
      host = host,
      port = port,
      doorOpenSignal = doorOpenSignal,
      description = description)
  }

  private def findExecutable(executable: String): String = {
    val file = new File(executable)
    if (file.exists()) {
      return file.getAbsolutePath()
    }
    val home = System.getenv("akka.home")
    if (home != null) {
      val homeDir = new File(home)
      val pathFile = new File(homeDir, executable)
      if (pathFile.exists())
        return pathFile.getAbsolutePath()
      val binDir = new File(homeDir, "bin")
      val binFile = new File(binDir, executable)
      if (binFile.exists())
        return binFile.getAbsolutePath()
    }
    log.error("Couldn't fine executable %s".format(executable))
    executable
  }

  /**
   * Create JPG configuration
   * @param config
   * @return JpegConfig the requested configuration
   */
  private def createJPGConfig(config: Config): JpegConfig = {
    val jpgConfig = config.getConfig(rootConfig + ".jpg.conversions")
    val keyList = jpgConfig.getKeySet()
    var params: Set[JpegConvertParameters] = Set()
    for (key ← keyList) {
      getVehicleImageTypeForConfigKey(key) match {
        case Some(imageType) ⇒
          val width = jpgConfig.getReplacedInteger(key + ".width")
          val height = jpgConfig.getReplacedInteger(key + ".height")
          val quality = jpgConfig.getReplacedInteger(key + ".quality")
          val enabled = jpgConfig.getReplacedString(key = key + ".enabled", default = "true") == "true"
          params += new JpegConvertParameters(imageType, jpg_width = width, jpg_height = height, jpg_quality = quality, conversionEnabled = enabled)
        case None ⇒ // do nothing
      }

    }
    val jpgOutputDir = config.getReplacedString(rootConfig + ".jpg.outputDir")
    new JpegConfig(jpgConfig = params, jpgOutputDir = jpgOutputDir)
  }

  /**
   * Return the key used in config file for each VehicleImageType
   * @param imageType
   */
  private def getConfigKeyForVehicleImageType(imageType: VehicleImageType.Value): String = {
    imageType match {
      case VehicleImageType.Overview            ⇒ "OVERVIEW"
      case VehicleImageType.OverviewMasked      ⇒ "OVERVIEWMASKED"
      case VehicleImageType.OverviewRedLight    ⇒ "OVERVIEWREDLIGHT"
      case VehicleImageType.OverviewSpeed       ⇒ "OVERVIEWSPEED"
      case VehicleImageType.License             ⇒ "LICENSE"
      case VehicleImageType.RedLight            ⇒ "REDLIGHT"
      case VehicleImageType.MeasureMethod2Speed ⇒ "MEASUREMETHOD2SPEED"
    }
  }

  private def getVehicleImageTypeForConfigKey(key: String): Option[VehicleImageType.Value] = {
    VehicleImageType.values.find(value ⇒ getConfigKeyForVehicleImageType(value) == key)
  }

  /**
   * Create completion configuration
   * @param config
   * @return CompletionConfig list of completion Configuration
   */
  private def createCompletionConfig(config: Config): List[CompletionConfig] = {
    val list = new ListBuffer[CompletionConfig]
    val hbase = config.getReplacedString(rootConfig + ".completion.hbase.service")
    hbase match {
      case "on" ⇒ {
        list += new HBaseCompletionConfig(
          config.getReplacedString(rootConfig + ".completion.hbase.servers"),
          config.getReplacedString(rootConfig + ".completion.hbase.tableName"))
      }
      case _ ⇒ log.info("HBase storage is turned off")
    }
    val fileBase = config.getReplacedString(rootConfig + ".completion.file.service")
    fileBase match {
      case "on" ⇒ {
        val dir = config.getReplacedString(rootConfig + ".completion.file.outputDir")
        list += new FileSystemCompletionConfig(dir)
      }
      case _ ⇒ log.info("File storage is turned off")
    }
    val amqpBase = config.getReplacedString(rootConfig + ".completion.amqp.service")
    amqpBase match {
      case "on" ⇒ {
        val isEndpoint = config.getReplacedString(rootConfig + ".completion.amqp.image-store-endpoint")
        val isRouteKey = config.getReplacedString(rootConfig + ".completion.amqp.image-store-route-key")
        val rrEndpoint = config.getReplacedString(rootConfig + ".completion.amqp.registration-receiver-endpoint")
        val rrRouteKey = config.getReplacedString(rootConfig + ".completion.amqp.registration-receiver-route-key")
        val heartbeatRateMillis = config.getReplacedMilliseconds(rootConfig + ".completion.amqp.heartbeat-rate")
        // truncating milliseconds
        val heartbeatRateSeconds = (heartbeatRateMillis / 1000).seconds
        val delayAlpha = config.getReplacedDouble(rootConfig + ".completion.amqp.delay-alpha", 0.875)
        val delayMargin = config.getReplacedLong(rootConfig + ".completion.amqp.delay-margin", 60000) // 1 minute
        list += new AmqpCompletionConfig(isEndpoint, isRouteKey, rrEndpoint, rrRouteKey, heartbeatRateSeconds,
          delayAlpha, delayMargin)
      }
      case _ ⇒ log.info("AMQP completion is turned off")
    }
    list.toList
  }

  /**
   * Create the signing configuration
   * @param config
   * @return SignConfig when configured otherwise None
   */
  private def createSignConfig(config: Config): Option[SignConfig] = {
    val service = config.getReplacedString(rootConfig + ".sign.service")
    service match {
      case "on" ⇒ Some(new SignConfig)
      case _    ⇒ None
    }
  }

  def getChildConfig(config: Config, path: String): Option[Config] = try {
    Some(config.getConfig(path))
  } catch {
    case e: Exception ⇒
      None
  }
  /**
   * Create Exif configuration
   * @param config
   * @return ExifConfig when configured otherwise None
   */
  private def createExifConfig(config: Config): Option[ExifConfig] = {
    val service = config.getReplacedString(rootConfig + ".exif.service")
    service match {
      case "on" ⇒ {
        val dir = config.getReplacedString(rootConfig + ".exif.outputDir")
        val writeJsonToComment = config.getReplacedString(rootConfig + ".exif.writeJsonToComment", "true") == "true"

        val exifConfig = config.getConfig(rootConfig + ".exif.targets")
        val keyList = exifConfig.getKeySet()
        var targetExifConfigs: Set[ExifTargetConfig] = Set()
        for (key ← keyList) {
          getVehicleImageTypeForConfigKey(key) match {
            case Some(imageType) ⇒
              val targetVersionString = exifConfig.getReplacedString(key + ".targetVersion")
              ImageVersion.getImageVersionForString(targetVersionString) match {
                case Some(targetVersion) ⇒
                  targetExifConfigs += new ExifTargetConfig(targetImageType = imageType, targetImageVersion = targetVersion)
                case None ⇒
                  log.warning("targetVersion [%s] isn't a valid ImageVersion".format(targetVersionString))
              }
            case None ⇒ // do nothing
          }
        }

        Some(new ExifConfig(
          dirExifImage = dir,
          targetConfigs = targetExifConfigs,
          writeJsonToComment = writeJsonToComment))
      }
      case _ ⇒ None
    }
  }

  /**
   * Create mask configuration
   * @param config
   * @return MaskConfig when configured otherwise None
   */
  private def createMaskConfig(config: Config): Option[MaskConfig] = {
    val service = config.getReplacedString(rootConfig + ".mask.service", "off")
    service match {
      case "on" ⇒ {
        val cropEnabled = config.getReplacedString(rootConfig + ".mask.cropEnabled", "false") == "true"
        val dir = config.getReplacedString(rootConfig + ".mask.outputDir")
        val quality = config.getReplacedInteger(rootConfig + ".mask.quality", 80)

        Some(new MaskConfig(cropEnabled = cropEnabled, dirMaskImage = dir, maskQuality = quality))
      }
      case _ ⇒ None
    }
  }

  /**
   * create the Recognize configuration
   * @param config
   * @return RecognizeConfig the requested configuration
   */
  private def createRecognizeConfig(config: Config): RecognizeConfig = {
    val maxRetries = config.getReplacedInteger(rootConfig + ".recognizer.maxRetries")
    val timeout = config.getReplacedInteger(rootConfig + ".recognizer.timeout")
    new RecognizeConfig(maxRetries, timeout.millis)
  }

}

/**
 * Create configuration class
 */
object Configuration extends DirectLogging {
  /*
  *create configuration.
  * It first tries to check if there is a systemproperty set
  * if not then the classpath is checked for a file "registration.conf"
  * if not check the AKKA config dir
  * @return Configuration class
  */
  def create(): Configuration = {
    var config: Option[Config] = None
    val registrationConfigFile = "registration.conf"
    val configFile = System.getProperty(registrationConfigFile, "")
    if (configFile != "") {
      config = Some(ConfigFactory.parseFile(new File(configFile)))
      log.info("Config loaded from -D%s=%s".format(registrationConfigFile, configFile))
    } else {
      val resource = getClass.getClassLoader.getResource(registrationConfigFile)
      if (resource != null) {
        config = Some(ConfigFactory.parseFile(new File(resource.getPath)))
        log.info("Config loaded from resource %s".format(resource.getPath))
      } else {
        Option(System.getenv("AKKA_HOME")) match {
          case Some(akkaHome) ⇒ {
            val configFile = akkaHome + "/config/" + registrationConfigFile
            config = Some(ConfigFactory.parseFile(new File(configFile)))
            log.info("AKKA_HOME is defined as [%s], registration config loaded from [%s].".format(akkaHome, configFile))
          }
          case None ⇒ {
            log.warning("Use default configuration! Can't load 'registration.conf'." +
              "\nOne of the three ways of locating the 'registration.conf' file needs to be defined:" +
              "\n\t1. Define the '-registration.conf=...' system property option." +
              "\n\t2. Put the 'registration.conf' file on the classpath." +
              "\n\t3. Define 'AKKA_HOME' environment variable pointing to the root of the Akka distribution.")
          }
        }
      }
    }
    new Configuration(config)
  }

  /**
   * create configuration based on the given configuration file
   * @param fileName
   * @return Configuration class
   */
  def createFromFile(fileName: String): Configuration = {
    val config = Some(ConfigFactory.parseFile(new File(fileName)))
    new Configuration(config)
  }

}