/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.vehicle.config

import base.{ FavorCriticalProcess, Mode }
import com.typesafe.config.Config
import csc.amqp.{ ConnectionConfig, ServicesConfig, ServicesConfiguration }
import csc.gantry.config._
import csc.timeregistration.registration.ImageMetaDataReader
import csc.vehicle.message._
import csc.akka.config.ConfigUtil._
import csc.watchdog.WatchdogConfig

import scala.concurrent.duration._

/**
 * Base type used to be able to support multiple recognizers
 */
case class RecognizeConfig(maxRetries: Int, recognizeTimeout: FiniteDuration)

/**
 * Base type used to be able to support multiple implementations of the last completion Step
 * Current implementations stores the vehicle registration
 */
trait CompletionConfig

/**
 * Class used to indicate the actification state of the Lane
 * @param active true sensor is turned on false sensor is off
 * @param priority the used priority of this Lane (not used yet)
 */
case class LaneActivation(active: Boolean, priority: Priority.Value)

/**
 * Jpeg Configuration for the different image types
 * @param imageType Configuration is valid vor this imageType
 * @param jpg_width The new width of the jpeg 0 means keep the current width
 * @param jpg_height The new width of the jpeg 0 means keep the current height
 * @param jpg_quality The JPEG quality between 0 and 100
 * @param conversionEnabled Whether the conversion (to JPEG) for this image type is enabled
 */
case class JpegConvertParameters(imageType: VehicleImageType.Value, jpg_width: Int, jpg_height: Int, jpg_quality: Int, conversionEnabled: Boolean)

/**
 * Global Jpg conversion configuration
 * @param jpgConfig List of all known Jpeg Conversion configuration
 * @param jpgOutputDir The directory of the converted images
 */
case class JpegConfig(jpgConfig: Set[JpegConvertParameters], jpgOutputDir: String) {
  def overview: Option[JpegConvertParameters] = jpgConfig.find(_.imageType == VehicleImageType.Overview)
  def license: Option[JpegConvertParameters] = jpgConfig.find(_.imageType == VehicleImageType.License)
}

//TODO 06022012 RR->RB: just a note, we also need TemperatureSwitchConfig
/**
 * Open Door Configuration
 * @param expression used to detect opendoor messages
 * @param doorDetectorUri the URI used to catch the messages
 * @param host  The host where the messages comes from
 * @param port  The port where the messages comes from
 * @param doorOpenSignal which signal indicates that the door is open
 * @param description Description used in the signal
 */
case class OpenDoorConfig(expression: String, doorDetectorUri: String, host: String, port: Int, doorOpenSignal: String, description: String)

/**
 * HBase implementation of the completion step.
 * Store the registrations into HBase
 */
case class HBaseCompletionConfig(quorum: String, tableName: String) extends CompletionConfig

/**
 * AMQP/RabbitMQ implementation of the completion step. Sending registrations (vehicle metadata) with AMQP
 *
 * @param imageStoreEndpoint Image Store exchange / endpoint name
 * @param imageStoreRouteKey Image Store route key
 * @param registrationReceiverEndpoint Registration Receiver exchange / endpoint name
 * @param registrationReceiverRouteKey Registration Receiver route key
 * @param heartbeatRate the frequency of heartbeats with a resolution of seconds (can be 0 then heartbeat is disabled)
 * @param delayAlpha alpha coefficient used to compute a weighed average of the current and the previous delay
 * @param delayMargin constant offset subtracted from the keepalive time
 */
case class AmqpCompletionConfig(imageStoreEndpoint: String,
                                imageStoreRouteKey: String,
                                registrationReceiverEndpoint: String,
                                registrationReceiverRouteKey: String,
                                heartbeatRate: FiniteDuration,
                                delayAlpha: Double,
                                delayMargin: Long) extends CompletionConfig

/**
 * FileSystem implementation of the completion step.
 * Store the registrations into the fileSystem
 * @param outputDir the directory where to store the registration
 */
case class FileSystemCompletionConfig(outputDir: String) extends CompletionConfig

/**
 * Exif configuration for a target image
 * @param targetImageType the type of the target image
 * @param targetImageVersion the version of the target image
 */
case class ExifTargetConfig(targetImageType: VehicleImageType.Value, targetImageVersion: ImageVersion.Value)

/**
 * The configuration of the Exif step
 * @param dirExifImage The directory where the images (with Exif data) are stored
 * @param targetConfigs The configurations of the individual target images
 * @param writeJsonToComment Should the json vehicle message be written to the (Jpeg) comment field
 */
case class ExifConfig(
  dirExifImage: String,
  targetConfigs: Set[ExifTargetConfig],
  writeJsonToComment: Boolean = true) {
  def getTargetImageTypes: Seq[VehicleImageType.Value] = targetConfigs.foldLeft(Seq[VehicleImageType.Value]())((result, targetConfig) ⇒ result :+ targetConfig.targetImageType)
  def getTargetConfig(imageType: VehicleImageType.Value): Option[ExifTargetConfig] = targetConfigs.find(targetConfig ⇒ targetConfig.targetImageType == imageType)
}

trait MaskConfiguration {
  def cropEnabled: Boolean
  def dirMaskImage: String
  def maskQuality: Int
}
/**
 * The configuration of the mask step
 * @param cropEnabled Cropping of masked image is enabled
 * @param dirMaskImage The directory where to store the masked image
 * @param maskQuality Jpeg quality of masked image
 */
case class MaskConfig(cropEnabled: Boolean, dirMaskImage: String, maskQuality: Int = 80) extends MaskConfiguration

/**
 * Configuration for determining the mask to be applied to an image, based on the source trigger.
 */
trait TriggerMaskConfig extends MaskConfiguration {

  override def cropEnabled: Boolean = false //we want to mask, not crop

  /**
   *
   * @param height height of image
   * @param width width of image
   * @param triggerData
   * @return mask to be applied
   */
  def imageMask(height: Int, width: Int, triggerData: ImageTriggerData): ImageMask

}

/**
 * TriggerMaskConfig determining a quadrant mask area.
 * @param heightPct height (in pixels) of the quadrant area
 * @param widthPct widht (in pixels) of the quadrant area
 */
case class SimpleTriggerMaskConfig(heightPct: Int, widthPct: Int, dirMaskImage: String, maskQuality: Int = 80) extends TriggerMaskConfig {
  require(heightPct > 0 && heightPct <= 100, "Height out of bounds (should be > 0 and <= 100)")
  require(widthPct > 0 && widthPct <= 100, "Width out of bounds (should be > 0 and <= 100)")

  def imageMask(height: Int, width: Int, triggerData: ImageTriggerData): ImageMask = {
    val minY = height * (100 - heightPct) / 100
    val focusWidth = width * widthPct / 100

    val (minX, maxX) = triggerData match {
      case ImageTriggerData(TriggerArea.LEFT, _)  ⇒ (0, focusWidth)
      case ImageTriggerData(TriggerArea.RIGHT, _) ⇒ (width - focusWidth, width)
    }

    ImageMaskImpl(List(
      ImageMaskVertex(minX, minY),
      ImageMaskVertex(maxX, minY),
      ImageMaskVertex(maxX, height),
      ImageMaskVertex(minX, height)))
  }
}

case class DetailedTriggerMaskConfig(leftPctPoints: List[Point], rightPctPoints: List[Point], dirMaskImage: String, maskQuality: Int = 80) extends TriggerMaskConfig {

  private val pctPointsMap: Map[TriggerArea.Value, List[Point]] = {
    val result = Map(
      TriggerArea.LEFT -> leftPctPoints.distinct,
      TriggerArea.RIGHT -> rightPctPoints.distinct)

    //validate points
    result foreach { e ⇒
      val what = e._1
      val points = e._2
      require(points.size >= 3, "%s needs at least 3 distinct point to define an area in %s".format(what, points))
      points foreach { p ⇒
        require(p.x >= 0 && p.x <= 100, "x coordinate not a percentage in %s".format(p))
        require(p.y >= 0 && p.y <= 100, "y coordinate not a percentage in %s".format(p))
      }
    }
    result
  }

  def imageMask(height: Int, width: Int, triggerData: ImageTriggerData): ImageMask = {
    val pctPoints = pctPointsMap(triggerData.area)
    val scaledPoints: List[ImageMaskVertex] = pctPoints map { p ⇒ ImageMaskVertex(width * p.x / 100, height * p.y / 100) }
    ImageMaskImpl(scaledPoints)
  }

}

/**
 * The configuration needed for the Sign Step
 */
case class SignConfig()

/**
 * TEMPORARY: Some configuration representing iRose data
 */
case class IRoseSystemData(interval: Int, loopDistance: Int)

case class JoinEventConfig(maxBufferLen: Int, bufferTimeout: Duration, shiftJoinPeriod: Int)

case class CertConfig(sign: Option[SignConfig], amqpCertificateCheckConfig: Option[AmqpCertificateCheckConfig]) {
  def isCertificationEnabled: Boolean = sign.isDefined //PCL-45: binding certification functionality to signing (temporary)
}

case class GantryControlConfig(services: ServicesConfiguration)

/**
 * Configuration for image, recognition and camera related items
 * @param testImagePattern
 * @param jpegStep
 * @param recognizeConfig
 * @param mask
 * @param exif
 * @param multiLaneCameras
 * @param triggerMaskConfig
 */
case class ImageConfig(testImagePattern: String,
                       jpegStep: JpegConfig,
                       recognizeConfig: RecognizeConfig,
                       mask: Option[MaskConfig],
                       exif: Option[ExifConfig],
                       multiLaneCameras: List[MultiLaneCamera],
                       triggerMaskConfig: Option[TriggerMaskConfig]) {

  def isMultilaneCameraSystem = !multiLaneCameras.isEmpty

  lazy val metadataReader: ImageMetaDataReader =
    if (sourceImageJpeg) ImageMetaDataReader.jpeg else ImageMetaDataReader.tiff

  //for now, anchoring the source image type to the camera system type (multilane or singlelane)
  def sourceImageJpeg = isMultilaneCameraSystem

}

trait RelayAdapterConfig
case class CamelRelayAdapterConfig(uri: String) extends RelayAdapterConfig
case class AmqpRelayAdapterConfig(services: ServicesConfiguration) extends RelayAdapterConfig

trait WorkflowConfig
case class MQWorkflowConfig(producerEndpoint: String,
                            preparedQueue: String,
                            recognizedQueue: String,
                            classifiedQueue: String,
                            poolSize: Int) extends WorkflowConfig

case object GenericWorkflowConfig extends WorkflowConfig

trait VehicleRegistrationConfiguration {
  def imageConfig: ImageConfig
  def joinEventConfig: JoinEventConfig
  def guards: Map[String, FavorCriticalProcess]
  def registrationCompletion: List[CompletionConfig]
  def openDoor: OpenDoorConfig
  def relayAdapterConfig: RelayAdapterConfig
  def certConfig: CertConfig
  def iRose: Option[Map[String, IRoseSystemData]]
  def configUpdateFrequency: FiniteDuration
  def watchdogConfig: Option[WatchdogConfig]
  def recognizerTransport: RecognizerTransportConfig
  def classifierConfig: Option[ClassifierConfig]
  def amqpConfig: AmqpConfig
  def cleanupConfig: Option[CleanupConfig]
  def doubleRegistrationFilterConfig: Option[DoubleRegistrationFilterConfig]
  def dualClockConfig: Option[DualClockConfig]
  def gantryControlConfig: Option[GantryControlConfig]
  def workflowConfig: WorkflowConfig

  //initial mode is test if gantryControl is enabled
  lazy val initialMode: Mode.Value = gantryControlConfig map { _ ⇒ Mode.Test } getOrElse Mode.Normal

  lazy val metaImageSectionGuard: Option[FavorCriticalProcess] = guards.get("metaImage")

  //shortcuts
  def jpegStep = imageConfig.jpegStep
  def recognizeConfig = imageConfig.recognizeConfig
  def exif = imageConfig.exif
  def sign = certConfig.sign
  def amqpCertificateCheckConfig = certConfig.amqpCertificateCheckConfig
  def multiLaneCameras = imageConfig.multiLaneCameras
  def isMultilaneCameraSystem = imageConfig.isMultilaneCameraSystem

  def laneIdSet: Set[String] = imageConfig.multiLaneCameras.map(mlc ⇒ Nil ++ mlc.leftLaneId ++ mlc.rightLaneId).flatten.toSet

  def metadataReader = imageConfig.metadataReader
  def maskConfiguration: Option[MaskConfiguration] = imageConfig.triggerMaskConfig.orElse(imageConfig.mask)

  //TODO: csilva: have this properly configured, not derived from another jacops aspect
  def customClassifier: Boolean = imageConfig.triggerMaskConfig.isDefined

}

case class VehicleRegistrationConfig(
  imageConfig: ImageConfig,
  joinEventConfig: JoinEventConfig,
  guards: Map[String, FavorCriticalProcess],
  registrationCompletion: List[CompletionConfig],
  openDoor: OpenDoorConfig,
  relayAdapterConfig: RelayAdapterConfig,
  certConfig: CertConfig,
  iRose: Option[Map[String, IRoseSystemData]],
  configUpdateFrequency: FiniteDuration,
  watchdogConfig: Option[WatchdogConfig],
  recognizerTransport: RecognizerTransportConfig,
  classifierConfig: Option[ClassifierConfig],
  amqpConfig: AmqpConfig,
  cleanupConfig: Option[CleanupConfig],
  doubleRegistrationFilterConfig: Option[DoubleRegistrationFilterConfig],
  dualClockConfig: Option[DualClockConfig],
  gantryControlConfig: Option[GantryControlConfig],
  workflowConfig: WorkflowConfig) extends VehicleRegistrationConfiguration

/**
 * Class related to dual clock settings
 * @param maxSampleBufferSize maximum amount of time diff readings kept in buffer
 * @param timeDiffThreshold threshold to flag the registration as
 * @param criticalTimeDiffThreshold
 * @param maxPhotoAge maximum millissecond difference between the image lastModified and the system time to consider the registration
 */
case class DualClockConfig(maxSampleBufferSize: Int = 100,
                           timeDiffThreshold: Long = 100,
                           criticalTimeDiffThreshold: Long = 1000,
                           maxPhotoAge: Long = 60000)

case class CleanupConfig(dropWithoutPlate: Boolean)

/**
 * The configuration of the JMX interface
 * @param host  Host where to bind to
 * @param jmxPasswordFile The used password file
 * @param jmxAccessFile The used access file
 * @param rmiPort rmi port where to bind to
 * @param jmxPort JMX port where to bind to
 */
case class JMXConfig(host: String, jmxPasswordFile: String, jmxAccessFile: String, rmiPort: Int, jmxPort: Int)

/**
 * Global configuration for AMQP connections
 * @param amqpUri AMQP uri
 * @param actorName Akka actor name for the connection actor
 * @param reconnectDelay reconnect delay used by the connection actor
 */
case class AmqpConfig(amqpUri: String, actorName: String, reconnectDelay: FiniteDuration) extends ConnectionConfig

/**
 * Configuration of the AMQP certification checker component. If defined it is configured to send the certification check
 * to the AMQP endpoint/exchange of the Regisration Receiver
 * @param registrationReceiverEndpoint the endpoint / exchange name used to access the Regisration Receiver
 * @param registrationReceiverRouteKey the route key used to reach the Registration Receiver
 * @param certificateComponentName the component name of the certificate in Curator
 */
case class AmqpCertificateCheckConfig(registrationReceiverEndpoint: String, registrationReceiverRouteKey: String,
                                      certificateComponentName: String)

case class DoubleRegistrationFilterConfig(millisInterval: Int, bufferSize: Int)

case class ClassifierConfig(services: ServicesConfig,
                            cleanup: FiniteDuration,
                            timeout: FiniteDuration) {

  def this(config: Config) = this(
    new ServicesConfiguration(config.getConfig("services")),
    config.getFiniteDuration("cleanupTime"),
    config.getFiniteDuration("timeout"))

}

trait RecognizerTransportConfig

case class RecognizerQueueConfig(consumerQueue: String,
                                 producerEndpoint: String,
                                 producerRouteKey: String,
                                 serversCount: Int,
                                 cleanup: FiniteDuration) extends ServicesConfig with RecognizerTransportConfig {

  def this(config: Config) = this(
    config.getString("consumer-queue"),
    config.getString("producer-endpoint"),
    "",
    config.getInt("servers-count"),
    config.getInt("cleanupTime").millis)

}
