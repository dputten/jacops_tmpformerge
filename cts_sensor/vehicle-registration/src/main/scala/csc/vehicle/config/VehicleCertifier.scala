/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.config

import csc.akka.logging.DirectLogging
import csc.gantry.config._
import csc.config.Path
import csc.systemevents.SystemEvent
import csc.curator.utils.{ Versioned, Curator }
import org.apache.zookeeper.KeeperException.BadVersionException

/**
 * Manage certificates in ZooKeeper
 * @param curator ZooKeeper connection
 */
class VehicleCertifier(curator: Curator) extends DirectLogging {

  val keyPrefix = Path("/ctes/systems")

  val componentName = "VehicleRegistration"

  /**
   * Find the active certificate in the type certificate from ZooKeeper by its name. Check the checksum and store the active certificate
   * for day report generation (under ~/[systemID]/activeCertificates/[certificate name])
   * @param certificate the active certificate to be checked and stored
   */
  def processCertificate(certificate: ActiveCertificate) {
    if (curator.exists(keyPrefix)) {
      val routeList = curator.getChildren(keyPrefix)
      routeList.foreach(routePath ⇒ processSystemCertificate(routePath, certificate))
    } else {
      log.error("No systems found. under %s".format(keyPrefix))
    }
  }

  /**
   * Handle the BadVersion exception while updating the JarCertificate
   * @param curator the curator
   * @param systemId the systemId
   * @param certificate the certificate found
   */
  private def handleBadVersionException(curator: Curator, systemId: String, certificate: ActiveCertificate) {
    val activeCert = CertificateService.getActiveJarCertificate(curator, systemId, certificate.componentCertificate.name)
    //compare certificate
    activeCert match {
      case Some(active) ⇒ {
        if (certificate.componentCertificate.checksum != active.componentCertificate.checksum) {
          log.info("Received handleBadVersionException and checksums are different [%s,%s]".format(systemId, certificate.componentCertificate.name))
          val path = keyPrefix / systemId
          sendAlarm(path, systemId, "Different checksums used by multiple VehicleRegistration processes for %s".format(certificate.componentCertificate.name))
        } else {
          log.info("Received handleBadVersionException but checksums are equale ignore exception [%s,%s]".format(systemId, certificate.componentCertificate.name))
        }
      }
      case None ⇒ {
        //very strange to get a version exception when it doesn't exists
        log.error("Received handleBadVersionException while ActiveJarCertificate doesn't exists [%s,%s]".format(systemId, certificate.componentCertificate.name))
      }
    }
  }

  /**
   * Check and store the provided Active Certificate  for one system
   * @param path  ZooKeeper path to the system
   * @param certificate  the runtime Active Certificate
   * @return true when found a match in TypeCertificate
   *
   */
  private def processSystemCertificate(path: Path, certificate: ActiveCertificate): Boolean = {
    val systemId = path.nodes.last.name
    val compCertificate = certificate.componentCertificate
    //store the active certificate for day report
    CertificateService.removeOldJarVersionsCertificates(curator, systemId, certificate.componentCertificate.name)
    try {
      CertificateService.putJarCertificate(curator, systemId, certificate)
    } catch {
      case ex: BadVersionException ⇒ handleBadVersionException(curator, systemId, certificate)
    }
    //compare with provided
    val measureType = CertificateService.getMeasurementMethodType(curator, systemId, certificate.time)

    measureType match {
      case Some(cert) ⇒ {
        //find the certificate with the same name and check its checksum
        val found: Boolean = cert.typeCertificate.components.find(provided ⇒ compCertificate.name == provided.name) match {
          case None ⇒ {
            //alarm - no component certificate found inside TypeCertificate
            val text = "%s: Geen voertuig component gevonden in type certificaat %s".format(systemId, cert)
            log.warning(text)
            true
          }
          case Some(c) ⇒ {
            if (c.checksum != compCertificate.checksum) {
              //certificates do not match
              //req 85: Een trajectcontrolesysteem moet de waarde van de softwarezegels elke dag na
              //het verstrijken van de meetdag bepalen voordat de data in de betreffende map
              //wordt gezet.
              val error = "%s: Actieve certificaat is niet gelijk aan verwachte certificaat (%s != %s)".format(
                systemId, c.checksum, compCertificate.checksum)
              log.error(error)
              sendAlarm(path, systemId, error)
              false
            } else {
              log.debug("%s: Active Certificate (%s) has been verified.".format(systemId, certificate))
              true
            }
          }
        }
        found
      }
      case None ⇒ {
        val text = "%s: Type certificaat niet gevonden.".format(systemId)
        log.error(text)
        sendAlarm(path, systemId, text)
        false
      }
    }
  }

  private def sendAlarm(systemPath: Path, systemId: String, text: String) {
    val alarmEvent = SystemEvent(componentName, System.currentTimeMillis(), systemId, "Alarm", "system",
      reason = text)
    sendEvent(systemPath, alarmEvent)
  }

  private def sendEvent(systemPath: Path, systemEvent: SystemEvent) {
    val pathToEvents = systemPath / "events"
    if (!curator.exists(pathToEvents)) {
      curator.createEmptyPath(pathToEvents)
    }
    curator.appendEvent(pathToEvents / "qn-", systemEvent)
  }
}
