/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.message

import math._
import java.util.Date

/**
 * Support Joining multiple VehicleRegistrationMessage
 */
object JoinRegistrationMessages {
  /**
   * Join a list of VehicleRegistrationMessage with a priority defined in sourcePrio
   * The messages with a triggersource defined in de start op de PrioList overrules attributes of messages with a lower
   * triggersource
   * @param sourcePrio Ordered list with expected triggersources. The first triggersource has the highest priority
   * @param msgList The list containing the messages
   * @param newTriggerSource the triggersource of the joined message
   * @return the joined VehicleRegistrationMessage
   */
  def join(sourcePrio: List[String], msgList: List[VehicleRegistrationMessage], newTriggerSource: String): VehicleRegistrationMessage = {
    require(msgList != null, "Missing messageList")
    require(msgList.size > 0, "Empty messageList")

    if (msgList.size == 1) {
      val reg = msgList.head
      val event = reg.event.copy(triggerSource = newTriggerSource)
      return reg.copy(event = event)
    }

    //there are message to join
    //getHighest prio
    //sort collection based on the sourcePriority
    def sortBasedOnPriority(msg1: VehicleRegistrationMessage, msg2: VehicleRegistrationMessage): Boolean = {
      //is msg1 smaller (higher priority)
      val ev1 = msg1.event
      val ev2 = msg2.event
      //both message has a event so compare sources
      var index1 = sourcePrio.indexOf(ev1.triggerSource)
      if (index1 < 0) {
        index1 = sourcePrio.size + 1
      }
      var index2 = sourcePrio.indexOf(ev2.triggerSource)
      if (index2 < 0) {
        index2 = sourcePrio.size + 1
      }
      index1 < index2
    }

    val sortedList = msgList.sortWith(sortBasedOnPriority)
    join(sortedList, newTriggerSource)
  }

  /**
   * Join a list of VehicleRegistrationMessage with a priority defined in sourcePrio
   * The messages first in the list overrules attributes of messages with a higher index
   * @param msgList: The list containing the messages
   * @param newTriggerSource: the triggersource of the joined message
   * @return the joined VehicleRegistrationMessage
   */
  def join(msgList: List[VehicleRegistrationMessage], newTriggerSource: String): VehicleRegistrationMessage = {
    //require(msgList!=null)
    if (msgList == null || msgList.size == 0) {
      throw new IllegalArgumentException("There are no messages to join")
    }
    if (msgList.size == 1) {
      return joinRegistration(msgList.headOption, None, newTriggerSource).get
    }

    //there are message to join
    var joinMsg: Option[VehicleRegistrationMessage] = None
    for (msg ← msgList) {
      joinMsg = joinRegistration(joinMsg, Some(msg), newTriggerSource)
    }
    //there has to be a joined message
    joinMsg.get
  }

  def mergeFileList(msg1: VehicleRegistrationMessage, msg2: VehicleRegistrationMessage): (List[RegistrationFile], List[RegistrationFile]) = {
    val replacedSet = (msg1.replacedFiles ++ msg2.replacedFiles).toSet //replaced is the union of all replaced images (removing dups)
    val fileSet = (msg1.files ++ msg2.files).toSet.filterNot(replacedSet) //files is the union of all files, unless replaced (removing dups)
    (fileSet.toList, replacedSet.toList)
  }

  /**
   * Joins two VehicleRegistrationMessage into one VehicleRegistrationMessage. The first message overrules
   * the attributeds of the second message
   * @param reg1: the first registration
   * @param reg2: the second registration
   * @param newTriggerSource: the new trigger source of the joined messages
   * @return the joined VehicleRegistrationMessage
   */
  def joinRegistration(reg1: Option[VehicleRegistrationMessage], reg2: Option[VehicleRegistrationMessage], newTriggerSource: String): Option[VehicleRegistrationMessage] = {
    if (reg1 == None && reg2 == None) {
      return None
    }
    if (reg1 == None || reg2 == None) {
      //just return the registration with updated trigger source
      val reg = reg1.orElse(reg2)
      val event = reg.get.event.copy(triggerSource = newTriggerSource)
      return Some(reg.get.copy(event = event))
    } else {
      //join registrations
      val firstRegistration = reg1.get
      val secondRegistration = reg2.get

      val vehicleData1 = firstRegistration.vehicleData
      val vehicleData2 = secondRegistration.vehicleData

      val vehicleData = firstRegistration.vehicleData.merge(secondRegistration.vehicleData)

      val (mergedFiles, mergedReplacedFiles) = mergeFileList(firstRegistration, secondRegistration)

      val status = firstRegistration.status.merge(secondRegistration.status)

      val certificationData = CertificationData.merge(firstRegistration.certificationData, secondRegistration.certificationData)

      Some(new VehicleRegistrationMessage(
        lane = firstRegistration.lane,
        certificationData = certificationData,
        eventId = firstRegistration.eventId.orElse(secondRegistration.eventId),
        eventTimestamp = firstRegistration.eventTimestamp.orElse(secondRegistration.eventTimestamp),
        event = joinSensorEvents(firstRegistration.event, secondRegistration.event, newTriggerSource),
        priority = firstRegistration.priority,
        licenseData = RegistrationLicenseData(
          license = firstRegistration.licenseData.license.orElse(secondRegistration.licenseData.license),
          rawLicense = firstRegistration.licenseData.rawLicense.orElse(secondRegistration.licenseData.rawLicense),
          country = firstRegistration.licenseData.country.orElse(secondRegistration.licenseData.country),
          licensePosition = firstRegistration.licenseData.licensePosition.orElse(secondRegistration.licenseData.licensePosition),
          licenseSourceImage = firstRegistration.licenseData.licenseSourceImage.orElse(secondRegistration.licenseData.licenseSourceImage),
          licenseType = firstRegistration.licenseData.licenseType.orElse(secondRegistration.licenseData.licenseType),
          recognizedBy = firstRegistration.licenseData.recognizedBy.orElse(secondRegistration.licenseData.recognizedBy)),
        vehicleData = vehicleData,
        files = mergedFiles,
        replacedFiles = mergedReplacedFiles,
        directory = firstRegistration.directory.orElse(secondRegistration.directory),
        appliedMask = firstRegistration.appliedMask.orElse(secondRegistration.appliedMask),
        appliedMaskCropped = firstRegistration.appliedMaskCropped && secondRegistration.appliedMaskCropped,
        jsonMessage = firstRegistration.jsonMessage.orElse(secondRegistration.jsonMessage),
        interval1Ms = firstRegistration.interval1Ms.orElse(secondRegistration.interval1Ms),
        interval2Ms = firstRegistration.interval2Ms.orElse(secondRegistration.interval2Ms),
        status = status))
    }
  }

  /**
   * Join two SensorEvents into one
   * The period will be the time which both events have in common
   * The event is the time of the first (when doesn't exists the time of the second is used) when it is within the new range
   * Otherwise the start time is used
   * @param firstEvent: the first sensorEvent
   * @param secondEvent: the second sensorEvent
   * @param newSource: the new triggersource of the new joined sensorEvent
   * @return the new sensor event
   */
  def joinSensorEvents(firstEvent: SensorEvent, secondEvent: SensorEvent, newSource: String): SensorEvent = {
    //join both events
    //create new range
    val start = max(firstEvent.matchRange.start, secondEvent.matchRange.start)
    val end = min(firstEvent.matchRange.end, secondEvent.matchRange.end)
    //getNew eventTime
    //TODO 08022012 RR->RB: would be nice to have a Range class, with methods like contains, etc
    val eventTime = if (firstEvent.timestamp >= start && firstEvent.timestamp <= end) {
      firstEvent.timestamp
    } else if (secondEvent.timestamp >= start && secondEvent.timestamp <= end) {
      secondEvent.timestamp
    } else {
      start
    }
    new SensorEvent(eventTime, new PeriodRange(start, end), newSource)
  }
}