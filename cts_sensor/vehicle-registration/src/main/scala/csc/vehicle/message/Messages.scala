/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.message

import base.Plan
import java.util.{ Date, Calendar }

//instructions

//sensor configuratie
case class SensorInstructionsMessage(plans: Set[Plan])

case class SensorActivationMessage(versionId: Long, activeSensors: Map[Long, Priority.Value]) {
}

//used with cleanupimages to indicate the time difference
object CalendarTimeUnitType extends Enumeration {
  type CalendarTimeUnitType = Value
  val YEAR = Value(Calendar.YEAR)
  val MONTH = Value(Calendar.MONTH)
  val DAYS = Value(Calendar.DATE)
  val HOUR = Value(Calendar.HOUR)
  val MINUTE = Value(Calendar.MINUTE)
  val SECOND = Value(Calendar.SECOND)
}

//message that is sent to cleanup old images
case class CleanupImagesMessage(directories: Set[CleanupImagePathMessage]) {
  require(directories != null, "Missing directories")
  require(!directories.isEmpty, "Empty directories")
}

//message that is sent to cleanup old images
case class CleanupImagePathMessage(path: String, extensions: Set[String], olderThen: Int, timeUnit: CalendarTimeUnitType.Value, recursive: Boolean = true) {
  require(path != null, "Missing path")
  require(!path.isEmpty, "Empty path")

  require(extensions != null, "Missing extensions")
  require(!extensions.isEmpty, "Empty extensions")
}

//surfaceReflection range 0-1, 1 = 100% reflection

//sent from DigitalSwitchDetector to control server
//TODO: probably remove when monitoring/Alarm Events are implemented
case class DigitalSwitchDetectedMessage(dateFromDetector: String,
                                        ipFromDetector: String,
                                        date: Date,
                                        imageServerHost: String,
                                        imageServerPort: Int,
                                        originalMessage: String,
                                        doorIsOpen: Boolean,
                                        description: String) {
  override def toString() = "dateFromDetector:[%s], ipFromDetector:[%s], date:[%s], imageServerAddress:[%s:%d], originalMessage:[%s], doorIsOpen:[%s], description:[%s]".format(
    dateFromDetector,
    ipFromDetector,
    date.toString(),
    imageServerHost,
    imageServerPort,
    originalMessage,
    doorIsOpen.toString(),
    description)

}

/**
 * Message used to update the time of the last received image.
 * Used between RelayImageData and AdminProcessor
 */
case class MonitorCameraMessage(sensor: Lane, lastCameraMsg: Date)

/**
 * Message used to update the time of the last received radar message.
 * Used between ReceiveRadar and AdminProcessor
 */
case class MonitorRadarMessage(sensor: Lane, lastRadarMsg: Date, message: Option[String] = None)

/**
 * Trigger message used to schedule sending the MonitorStatusMessage to the control server
 */
case class AdminSendMonitor()

/**
 * Trigger message used to schedule an action
 */
case class TimeTrigger()

/**
 * TODO: remove when monitoring Events are implemented
 * Message used to send monitor data to the control server
 */
case class MonitorStatusMessage(hostName: String, hostPort: Int, updateTime: Date, version: String, startTime: Date, sensorMonitor: List[MonitorSensorMessage]) {
}

/**
 * TODO: remove when monitoring Events are implemented
 * message used in MonitorStatusMessage containing monitor data per sensor
 */
case class MonitorSensorMessage(sensor: Lane, lastCameraMsg: Option[Date], lastRadarMsg: Option[Date], radarMsg: Option[String] = None) {
}

trait GantryControlMessage {
  def correlationId: String
  def gantry: String
  def time: Long
}

case class SetModeRequest(correlationId: String, gantry: String, mode: String, time: Long, user: String) extends GantryControlMessage
case class SetModeResponse(correlationId: String, gantry: String, mode: String, time: Long, error: Option[String]) extends GantryControlMessage

case class GetModeRequest(correlationId: String, gantry: String, time: Long) extends GantryControlMessage
case class GetModeResponse(correlationId: String, gantry: String, mode: String, time: Long) extends GantryControlMessage

