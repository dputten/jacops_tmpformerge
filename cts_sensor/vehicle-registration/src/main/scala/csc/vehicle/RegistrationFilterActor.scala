package csc.vehicle

import akka.actor.ActorRef
import base.{ RoundtripMessage, RecipientListLike }
import csc.vehicle.message.VehicleRegistrationMessage
import csc.vehicle.registration.RegistrationCleanup

/**
 * Created by carlos on 06/12/16.
 */
class RegistrationFilterActor(val recipients: Set[ActorRef],
                              reject: VehicleRegistrationMessage ⇒ Option[String]) extends RecipientListLike {

  override def receive: Receive = {
    case msg: RoundtripMessage ⇒ handleRoundtrip("RegistrationFilterActor", msg) //roundrip is never diverted
    case msg: VehicleRegistrationMessage ⇒ reject(msg) match {
      case None         ⇒ sendToRecipients(msg)
      case Some(reason) ⇒ sendToCleanup(RegistrationCleanup(msg, reason))
    }
  }

  def sendToCleanup(msg: RegistrationCleanup) = {
    log.debug("Filtered out {}", msg.msg)
    context.system.eventStream.publish(msg)
  }

}
