package csc.vehicle

import akka.actor.ActorRef
import base.RecipientList
import csc.vehicle.config.DoubleRegistrationFilterConfig
import csc.vehicle.message.VehicleRegistrationMessage
import csc.vehicle.registration.RegistrationCleanup

/**
 * Created by carlos on 07/12/16.
 */
class MultiGantryDoubleTriggerFilter(recipients: Set[ActorRef], cfg: DoubleRegistrationFilterConfig)
  extends RecipientList(recipients) {

  private var bufferMap: Map[String, ConfirmedRegistrationBuffer] = Map.empty

  override def receive: Receive = {
    case msg: VehicleRegistrationMessage ⇒ handle(msg)
  }

  def handle(msg: VehicleRegistrationMessage): Unit = {
    if (shouldBlockRegistration(msg)) {
      log.info("Found double trigger event eventId=%s for license %s. Sending to cleanup".format(msg.id, msg.licenseData.license))
      sendToCleanup(msg)
    } else sendToRecipients(msg)
  }

  private def shouldBlockRegistration(msg: VehicleRegistrationMessage): Boolean = bufferForGantry(msg.lane.gantry).shouldBlockRegistration(msg)

  private def bufferForGantry(gantry: String): ConfirmedRegistrationBuffer = bufferMap.get(gantry) match {
    case None ⇒
      val buffer = new ConfirmedRegistrationBufferImpl(cfg)
      bufferMap = bufferMap.updated(gantry, buffer)
      buffer
    case Some(buffer) ⇒ buffer
  }

  def sendToCleanup(msg: VehicleRegistrationMessage): Unit = {
    context.system.eventStream.publish(RegistrationCleanup(msg, "duplicate"))
  }
}

class ConfirmedRegistrationBufferImpl(cfg: DoubleRegistrationFilterConfig) extends ConfirmedRegistrationBuffer {

  override def timeInterval: Int = cfg.millisInterval
  override def maxBufferSize: Int = cfg.bufferSize

}