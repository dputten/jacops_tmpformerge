package csc.vehicle.tools.parser

import java.util.StringTokenizer

/**
 *
 * Tool to parse useful information from logs
 * Created by carlos on 11/04/16.
 */
object LogParser {

  /**
   * Attempts to parse a composite data strcuture from the given string
   * @param str source string
   * @return Some composite structure, or None if unable to parse
   */
  def parseComposite(str: String): Option[Composite] = try {
    tokenize(str) match {
      case name :: "(" :: tail if tail.last == ")" ⇒
        val node = complete(Composite(name.toString, Nil), tail.dropRight(1))._1
        Some(normalize(node).asInstanceOf[Composite])
      case other ⇒
        Console.err.println("Unable to parse composite from " + str)
        None
    }
  } catch {
    case ex: Exception ⇒
      Console.err.println("Error parsing composite: " + ex.toString)
      None
  }

  /**
   * Converts common composites to their Scala equivalent objects, with recursion
   * @param obj
   * @return fully normalized object
   */
  def normalize(obj: Any): Any = obj match {
    case "None"                  ⇒ None
    case Composite("Some", list) ⇒ Some(normalize(list(0)))
    case Composite("List", list) ⇒ removeLeadingSpace(list) map normalize
    case Composite(name, list)   ⇒ Composite(name, list map normalize)
    case other: String           ⇒ other
    case other                   ⇒ throw new UnsupportedOperationException(other.getClass.getName)
  }

  def removeLeadingSpace(list: List[Any]): List[Any] = {

    def remSpace(str: String): String = str(0) match {
      case ' ' ⇒ str.substring(1)
      case _   ⇒ str
    }

    def removeSpace(l: List[Any]): List[Any] = l match {
      case Nil                             ⇒ Nil
      case Composite(name, params) :: tail ⇒ Composite(remSpace(name), params) :: removeSpace(tail)
      case head :: tail                    ⇒ remSpace(head.toString) :: removeSpace(tail)
    }

    list.head :: removeSpace(list.tail)
  }

  def complete(struct: Composite, list: List[Any]): (Composite, List[Any]) = list match {
    case Nil         ⇒ (struct, Nil)
    case "," :: tail ⇒ complete(struct, tail)
    case ")" :: tail ⇒ (struct, tail)
    case name :: "(" :: tail ⇒ complete(Composite(name.toString, Nil), tail) match {
      case (child, rest) ⇒ complete(struct.add(child), rest)
    }
    case head :: tail ⇒ complete(struct.add(head), tail)
    case other        ⇒ throw new UnsupportedOperationException("Not expecting " + list.mkString(" "))
  }

  def tokenize(str: String): List[Any] = {
    val tokenizer = new StringTokenizer(str, "(),", true)
    val list = toList(tokenizer)
    val opening = list.count(_ == "(")
    val closing = list.count(_ == ")")
    if (opening != closing) throw new IllegalArgumentException("Unbalanced parenthesis in " + str)
    list
  }

  def toList(tokenizer: StringTokenizer): List[Any] = {
    var result: List[Any] = Nil
    var prev: Option[String] = None
    while (tokenizer.hasMoreTokens) {
      val token = tokenizer.nextToken()
      val addEmpty: Boolean = (prev, token) match {
        case (Some("("), ",") ⇒ true
        case (Some(","), ",") ⇒ true
        case (Some(","), ")") ⇒ true
        case _                ⇒ false
      }
      if (addEmpty) result = result :+ "" //stuffing empty string
      result = result :+ token
      prev = Some(token)
    }
    result
  }

}

/**
 * Represents some non-atomic (not String or primitive) data structure.
 * Typical examples: Some, List, TupleX, case class
 * @param name
 * @param parts
 */
case class Composite(name: String, parts: List[Any]) {
  override def toString: String = name + "(" + parts.mkString(",") + ")"
  def add(child: Any): Composite = this.copy(name, parts :+ child)
  def compositeListAt(idx: Int): List[Composite] = parts(idx).asInstanceOf[List[Any]] map { e ⇒ e.asInstanceOf[Composite] }
  def compositeOptAt(idx: Int): Option[Composite] = parts(idx).asInstanceOf[Option[Composite]]
  def compositeAt(idx: Int): Composite = parts(idx).asInstanceOf[Composite]
  def stringAt(idx: Int): String = parts(idx).asInstanceOf[String]
  def intAt(idx: Int): Int = stringAt(idx).toInt
  def intListAt(idx: Int): List[Int] = parts(idx).asInstanceOf[List[Any]] map { e ⇒ e.toString.toInt }
  def longAt(idx: Int): Long = stringAt(idx).toLong
}

