/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.tools.vehicle

import csc.akka.logging.DirectLogging
import org.apache.commons.io.FileUtils
import akka.actor.{ ActorSystem, Props, ActorRef }
import java.util.concurrent.TimeUnit
import scala.concurrent.duration.Duration
import java.util.Date
import java.io.{ IOException, InputStreamReader, BufferedReader, File }
import csc.gantry.config._
import collection.mutable.ListBuffer
import csc.gantry.config.CameraConfig
import csc.gantry.config.RadarConfig
import csc.vehicle.message.Lane
import com.typesafe.config.ConfigFactory

/**
 * This is the object which starts the simulator.
 * It is responseble
 * to read the commandlines
 * to read the scriptfile
 * start the Simulator (sender)
 * and execute the commands
 *
 */
object VehicleSimulator extends DirectLogging {
  type OptionMap = Map[Symbol, String]
  var reader = new BufferedReader(new InputStreamReader(System.in))
  val cfg = ConfigFactory.load("simulator")
  val system = ActorSystem("SIMULATOR", cfg)

  /**
   * Parse the command arguments
   */
  def nextOption(map: OptionMap, list: List[String]): OptionMap = {
    list match {
      case Nil ⇒ map
      case "--script" :: value :: tail ⇒
        nextOption(map ++ Map('script -> value), tail)
      case "--help" :: tail ⇒
        nextOption(map ++ Map('help -> ""), tail)
      case "--log" :: value :: tail ⇒
        nextOption(map ++ Map('log -> value), tail)
      case option :: tail ⇒
        println("Unknown option " + option)
        sys.exit(1)
    }
  }

  /**
   * Prints the usage string to standard out
   */
  def printUsage() {
    println("Simulator Usage --script <scriptFile>")
    println("\t--script <scriptFile>: the script file which contains the instructions")
    println("\t--help: show the usage and script usage")
  }

  /**
   * Prints the script usage string to standard out
   */
  def printScriptUsage() {
    println("Script Usage: ")
    println("\t# comment")
    println("\tCONFIG CONFIG-START ...configurations... CONFIG-END")
    println("Config Usage: command [argument[=value] ...]")
    println("\tLANE LANE-ID=<ref-id> RELAY-DIR=<dir> HOST=<VR host> RADAR-PORT=<port> RADAR-ANGLE=<angle> RADAR-HEIGHT=<height> FTP-PORT=<port> FTP-USER=<user> FTP-PWD=<pwd> RADAR-DELAY=<delay>")
    println("\t\tLANE-ID=<ref-id> The referention id of this lane")
    println("\t\tRELAY-DIR=<ftp directory> The ftp directory used by relaySensordata to receive image")
    println("\t\tHOST=<VR host> The host where the Vehicle Registration is running")
    println("\t\tRADAR-PORT=<port> The listen port number of the radar")
    println("\t\tRADAR-ANGLE=<angle> The simulated radar angle")
    println("\t\tRADAR-HEIGHT=<height> The simulated radar height")
    println("\t\tFTP-PORT=<port> The port number of the FTP server")
    println("\t\tFTP-USER=<user> The user to logon the FTP server")
    println("\t\tFTP-PWD=<pwd> The password of the ftp user")
    println("\t\tRADAR-DELAY=<delay> The delay between the FTP and the radar triggers")

    println("\tSCRIPT SCRIPT-START ...scripts... SCRIPT-END")
    println("Script Usage: delay command [argument[=value] ...]")
    println("\tLANE-REGISTRATION LANE-ID=<ref-id> SPEED=<speed> LENGTH=<length> IMAGE=<image>")
    println("\t\tLANE-ID=<ref-id> The referention id of this lane")
    println("\t\tSPEED=<speed> The speed of the vehicle")
    println("\t\tLENGTH=<length> The length of the vehicle in meters (optional)")
    println("\t\tIMAGE=<image> The path to the image")
    println("\tWAIT NOP | INPUT")
    println("\t\tWAIT NOP: Just wait and do nothing")
    println("\t\tWAIT INPUT: Wait until user gives an enter")
  }

  def executeEmbedded(scriptFile: File): Unit = {
    try {
      val lines = readScriptFile(scriptFile.getAbsolutePath)
      val config = configureSimulator(lines._1)
      val scriptList = lines._2
      //create repeat structure
      val (nrRead, command) = createRepeatCommandLine(scriptList, 0)
      if (nrRead != scriptList.size) {
        throw new RuntimeException("Parsing script file failed: Mismatch between REPEAT-START and REPEAD-END")
      }
      //process script
      executeCommand(config, command, System.currentTimeMillis)
    } finally {
      System.out.println("Stop Simulator")
      reader.close
      system.shutdown()
      System.out.println("Simulator Stoped")
    }
  }

  /**
   * The main tread.
   * Parse arguments and check them for correctness
   */
  def main(args: Array[String]): Unit = {
    val options = nextOption(Map(), args.toList)
    println(options)
    options.get('help) foreach {
      value ⇒
        {
          printUsage
          printScriptUsage
          sys.exit(0)
        }
    }

    val scriptOpt = options.get('script)
    if (!scriptOpt.isDefined) {
      printUsage
      sys.exit(1)
    }
    val scriptFile = new File(scriptOpt.get)

    var execLog = options.get('log).getOrElse(scriptFile.getName + ".execlog")

    //readScriptFile
    try {
      val lines = readScriptFile(scriptFile.getAbsolutePath)
      val config = configureSimulator(lines._1)
      val scriptList = lines._2
      //create repeat structure
      val (nrRead, command) = createRepeatCommandLine(scriptList, 0)
      if (nrRead != scriptList.size) {
        println("Parsing script file failed: Mismatch between REPEAT-START and REPEAD-END")
        sys.exit(2)
      }
      //process script
      executeCommand(config, command, System.currentTimeMillis)
    } catch {
      case e: ParserException ⇒ {
        //log error on line index
        println("Parsing script file failed: " + e.getMessage)
        e.printStackTrace
        sys.exit(2)
      }
      case e: IllegalArgumentException ⇒ {
        //log error on line index
        println("Configuration failed: " + e.getMessage)
        e.printStackTrace
        sys.exit(3)
      }
    }
    //stop system
    System.out.println("Stop Simulator")
    reader.close
    system.shutdown()
    System.out.println("Simulator Stoped")
    sys.exit(0)
  }

  def configureSimulator(configList: List[ConfigLine]): Map[String, ActorRef] = {
    var refMap = Map[String, ActorRef]()
    for (cfg ← configList) {
      val options = cfg.options
      cfg.config match {
        case SimConstants.CONFIG_LANE     ⇒ refMap = refMap ++ configureLane(options)
        case SimConstants.CONFIG_CORRIDOR ⇒ refMap = refMap ++ configureCorridor(options, refMap)
        case SimConstants.CONFIG_CAMERA   ⇒ refMap = refMap ++ configureCamera(options)
      }
    }
    refMap
  }

  def configureCamera(options: Map[String, String]): Map[String, ActorRef] = {
    val id = getOption(SimConstants.CONFIG_CAMERA, options, SimConstants.ID)
    val ftp = getFtpConfig(SimConstants.CONFIG_CAMERA, options)
    val simulate = new SimulateSimpleCamera(id, ftp)
    Map((SimConstants.CONFIG_LANE + id) -> system.actorOf(Props(new SimulateRegistrationActor(simulate))))
  }

  def configureCorridor(options: Map[String, String], refMap: Map[String, ActorRef]): Map[String, ActorRef] = {
    val id = getOption(SimConstants.CONFIG_CORRIDOR, options, SimConstants.CORRIDOR_ID)
    val start = getOption(SimConstants.CONFIG_CORRIDOR, options, SimConstants.START_LANE_ID)
    val end = getOption(SimConstants.CONFIG_CORRIDOR, options, SimConstants.END_LANE_ID)
    val length = getOption(SimConstants.CONFIG_CORRIDOR, options, SimConstants.CORRIDOR_LENGTH)
    val startActorRef = refMap.get(SimConstants.CONFIG_LANE + start) match {
      case Some(actorRef) ⇒ actorRef
      case None           ⇒ throw new IllegalArgumentException("Can't find lane with id %s".format(start))
    }
    val endActorRef = refMap.get(SimConstants.CONFIG_LANE + end) match {
      case Some(actorRef) ⇒ actorRef
      case None           ⇒ throw new IllegalArgumentException("Can't find lane with id %s".format(end))
    }
    Map((SimConstants.CONFIG_CORRIDOR + id) -> system.actorOf(Props(new SimulateCorridorRegistration(startActorRef, endActorRef, length.toFloat))))
  }

  def getFtpConfig(section: String, options: Map[String, String]): FtpConfig = {
    val host = getOption(section, options, SimConstants.HOST)
    val port = getOption(section, options, SimConstants.FTP_PORT)
    val user = getOption(section, options, SimConstants.FTP_USER)
    val pwd = getOption(section, options, SimConstants.FTP_PWD)
    FtpConfig(host, port.toInt, user, pwd)
  }

  def configureLane(options: Map[String, String]): Map[String, ActorRef] = {
    val id = getOption(SimConstants.CONFIG_LANE, options, SimConstants.LANE_ID)
    val dir = getOption(SimConstants.CONFIG_LANE, options, SimConstants.RELAY_DIR)
    val ftp = getFtpConfig(SimConstants.CONFIG_LANE, options)
    val radarOpt = options.get(SimConstants.RADAR_PORT)
    val angle = getOption(SimConstants.CONFIG_LANE, options, SimConstants.RADAR_ANGLE)
    val height = getOption(SimConstants.CONFIG_LANE, options, SimConstants.RADAR_HEIGHT)
    val delay = getOption(SimConstants.CONFIG_LANE, options, SimConstants.RADAR_DELAY)
    val pirOption = options.get(SimConstants.PIR_PORT)
    val lane = new Lane(id, "lane1", "gantry1", "route", 0, 0)
    val relaySensor = new CameraConfig(relayURI = dir, cameraHost = "localhost", cameraPort = 1400, maxTriggerRetries = 10, timeDisconnected = 20000)
    val radarCfg = new RadarConfig(radarURI = "Dummy",
      measurementAngle = angle.toDouble, //Angle of the radar (degrees) between ground and the radar
      height = height.toDouble)
    val rcConfig = new RadarCameraSensorConfigImpl(lane = lane, camera = relaySensor, imageMask = None, recognizeOptions = Map(), radar = radarCfg)

    val simulate = pirOption match {
      case Some(pir) ⇒ {
        val pirDelta = options.get(SimConstants.PIR_TIME_DELTA).getOrElse("0")

        radarOpt match {
          case Some(radar) ⇒ new SimulatePirRadarCamera(config = rcConfig,
            ftpConfig = ftp,
            radarPort = radar.toInt,
            radarDelay = Duration(delay.toLong, TimeUnit.MILLISECONDS),
            pirPort = pir.toInt,
            pirTimeDelta = pirDelta.toLong)
          case None ⇒ new SimulatePirCamera(config = rcConfig,
            ftpConfig = ftp,
            pirPort = pir.toInt,
            pirTimeDelta = pirDelta.toLong)
        }
      }
      case None ⇒ {
        radarOpt match {
          case Some(radar) ⇒ new SimulateRadarCamera(config = rcConfig,
            ftpConfig = ftp,
            radarPort = radar.toInt,
            radarDelay = Duration(delay.toLong, TimeUnit.MILLISECONDS))
          case None ⇒ throw new IllegalArgumentException("Need at leased one PIR or RADAR port")
        }
      }
    }
    Map((SimConstants.CONFIG_LANE + id) -> system.actorOf(Props(new SimulateRegistrationActor(simulate))))
  }

  def getOption(command: String, optionList: Map[String, String], option: String): String = {
    val value = optionList.get(option)
    if (value == None) {
      throw new IllegalArgumentException("Missing required argument %s for command %s".format(option, command))
    }
    value.get
  }

  /**
   * Parse the script
   */
  def readScriptFile(fileName: String): (List[ConfigLine], List[ScriptLine]) = {
    val lines = FileUtils.readFileToString(new File(fileName))
    val allList = VehicleParser.parseScript(lines)
    val configList = allList.filter(obj ⇒ obj.isInstanceOf[ConfigLine]).asInstanceOf[List[ConfigLine]]
    val scriptList = allList.filter(obj ⇒ obj.isInstanceOf[ScriptLine]).asInstanceOf[List[ScriptLine]]
    (configList, scriptList)
  }

  /**
   * Execute the script and keep up the delay time independent of the duration of the execution time
   * This is called recursive when it find a repeater block (commands is not empty)
   * @param config all the references to the registration Actors
   * @param command the current repeat command
   * @param curTime the startTime
   * @return the time of the last script line execution
   */
  def executeCommand(config: Map[String, ActorRef], command: RepeatCommandLine, curTime: Long): Long = {
    //To prevent the simulator to drift from the script file delays the wait time is calculated
    // using a accumulated time. This way the execution time is ignored
    var accTime = curTime

    for (nr ← 0 until command.nrExec) {
      for (commandLine ← command.commands) {
        if (commandLine.commands.isEmpty) {
          //just one command so execute
          accTime += commandLine.scriptLine.delay
          val delay = accTime - System.currentTimeMillis
          if (delay > 0) {
            Thread.sleep(delay)
          }
          try {
            accTime = excecuteLine(config, commandLine.scriptLine, new Date(accTime))
          } catch {
            case ex: IllegalArgumentException ⇒ {
              val msg = "Error in script. Command %s scriptLine: %d error %s".format(commandLine.scriptLine.command, commandLine.startLine, ex.getMessage)
              System.out.println(msg)
              log.error(msg)
              sys.exit(-1)
            }
          }
        } else {
          //list fo commands so a repeat block
          accTime = executeCommand(config, commandLine, accTime)
        }
      }
    }
    accTime
  }

  /**
   * Execute one script line
   * It returns the last execution time. For normal execution this is the same as now, but
   * in some actions like waiting for input of a repeating command like MULTI_CORRIDOR_REGISTRATION,
   * this is adjusted to the last execution time
   *
   * @param config all the references to the registration Actors
   * @param line the script line which has to be executed
   * @param now current time
   * @return time of the last execution time.
   */
  def excecuteLine(config: Map[String, ActorRef], line: ScriptLine, now: Date): Long = {
    var executeTime = now.getTime //normal actions return the now time, but repeated commands or wait commands will alter this time
    val options = line.options
    line.command match {
      case SimConstants.COMMENT ⇒ {
        //do nothing
      }
      case SimConstants.LANE_REGISTRATION ⇒ {
        log.info("Simulate: laneRegistration")
        val id = getOption(SimConstants.LANE_REGISTRATION, options, SimConstants.LANE_ID)
        val lane = config.get(SimConstants.CONFIG_LANE + id)
        if (lane == None) {
          throw new IllegalArgumentException("Missing Lane %s isn't defined in configuration".format(id))
        }
        val speed = getOption(SimConstants.LANE_REGISTRATION, options, SimConstants.SPEED)
        val length: Option[Double] = options.get(SimConstants.LENGTH).map(_.toDouble)
        val image = getOption(SimConstants.LANE_REGISTRATION, options, SimConstants.IMAGE)
        val cat = options.get(SimConstants.CATEGORY)
        val delay: Option[Long] = options.get(SimConstants.DELAY).map(_.toLong)
        log.info("Using image: " + image)
        lane.get ! new SimulateVehicleRegistration(image = image, speed = speed.toDouble, length = length, category = cat, time = now, delay = delay)
      }
      case SimConstants.CORRIDOR_REGISTRATION ⇒ {
        log.info("Simulate: CorridorRegistration")
        val id = getOption(SimConstants.CORRIDOR_REGISTRATION, options, SimConstants.CORRIDOR_ID)
        config.get(SimConstants.CONFIG_CORRIDOR + id) match {
          case None ⇒ throw new IllegalArgumentException("Missing Corridor %s isn't defined in configuration".format(id))
          case Some(corridor) ⇒ {
            val speed = getOption(SimConstants.CORRIDOR_REGISTRATION, options, SimConstants.SPEED)
            val length: Option[Double] = options.get(SimConstants.LENGTH).map(_.toDouble)
            val image = getOption(SimConstants.CORRIDOR_REGISTRATION, options, SimConstants.IMAGE)
            val cat = options.get(SimConstants.CATEGORY)
            corridor ! new SimulateVehicleRegistration(image = image, speed = speed.toDouble, length = length, category = cat, time = now)
          }
        }
      }
      case SimConstants.MULTI_CORRIDOR_REGISTRATION ⇒ {
        log.info("Simulate: Multy CorridorRegistration")
        val id = getOption(SimConstants.CORRIDOR_REGISTRATION, options, SimConstants.CORRIDOR_ID)
        config.get(SimConstants.CONFIG_CORRIDOR + id) match {
          case None ⇒ throw new IllegalArgumentException("Missing Corridor %s isn't defined in configuration".format(id))
          case Some(corridor) ⇒ {
            val speed = getOption(SimConstants.CORRIDOR_REGISTRATION, options, SimConstants.SPEED)
            val length: Option[Double] = options.get(SimConstants.LENGTH).map(_.toDouble)
            val cat = options.get(SimConstants.CATEGORY)
            val imageDir = new File(getOption(SimConstants.CORRIDOR_REGISTRATION, options, SimConstants.IMAGE_DIR))
            if (!imageDir.exists()) {
              throw new IllegalArgumentException("Image directory %s doesn't exists".format(imageDir.getAbsolutePath))
            }
            if (!imageDir.isDirectory()) {
              throw new IllegalArgumentException("Image directory %s isn't a directory".format(imageDir.getAbsolutePath))
            }
            for (image ← imageDir.listFiles()) {
              if (image.getName.endsWith(".tif")) {
                corridor ! new SimulateVehicleRegistration(image = image.getAbsolutePath, speed = speed.toDouble,
                  length = length, category = cat, time = new Date(executeTime))
                executeTime += line.delay
                val delay = executeTime - System.currentTimeMillis
                if (delay > 0) {
                  Thread.sleep(delay)
                }
              }
            }
          }
        }
      }
      case SimConstants.WAIT ⇒ {
        if (options.size == 0) {
          //NOP
        }
        if (options.size > 1) {
          throw new IllegalArgumentException("Only one argument possible for WAIT command")
        }
        options.get(SimConstants.INPUT) foreach { _ ⇒
          {
            log.info("Simulate: wait for input")
            //  prompt the user to enter their name
            System.out.print("Wait for command input: ");
            //  open up standard input
            var inputLine: String = null;
            //  readLine() method
            try {
              inputLine = reader.readLine();
            } catch {
              case ex: IOException ⇒ System.out.println("IO error while waiting for input. resuming scriptprocessing");
            }
            System.out.println("Resuming scriptprocessing");
            log.info("Simulate: resume control after wait for input")
            executeTime = System.currentTimeMillis
          }
        }
      }
    }
    executeTime
  }

  /**
   * Create a repeat structure
   * @param scriptLines list of all scriptLines
   * @param startPos start position in the scriptLines list
   * @return Tuple of (next row to read, new created commandLine)
   */
  def createRepeatCommandLine(scriptLines: List[ScriptLine], startPos: Int): Tuple2[Int, RepeatCommandLine] = {
    val list = new ListBuffer[RepeatCommandLine]
    var ptr = if (scriptLines(startPos).command == SimConstants.REPEAT_START) {
      startPos + 1
    } else {
      startPos
    }
    var done = false
    while (!done && ptr < scriptLines.size) {
      val line = scriptLines(ptr)
      line.command match {
        case SimConstants.REPEAT_END ⇒ {
          done = true
          ptr += 1
        }
        case SimConstants.REPEAT_START ⇒ {
          val (pos, command) = createRepeatCommandLine(scriptLines, ptr)
          ptr = pos
          list += command
        }
        case _ ⇒ {
          list += new RepeatCommandLine(1, ptr + 1, line, Seq())
          ptr += 1
        }
      }
    }
    val command = if (scriptLines(startPos).command == SimConstants.REPEAT_START) {
      val scriptLine = scriptLines(startPos)
      val nrRepeat = getOption(SimConstants.REPEAT_START, scriptLine.options, SimConstants.REPEAT_NUMBER).toInt
      new RepeatCommandLine(nrRepeat, startPos + 1, scriptLine, list.toSeq)
    } else {
      new RepeatCommandLine(1, startPos, new ScriptLine(0, SimConstants.SCRIPT_START, Map()), list.toSeq)
    }
    (ptr, command)
  }
}

case class RepeatCommandLine(nrExec: Int, startLine: Int, scriptLine: ScriptLine, commands: Seq[RepeatCommandLine])