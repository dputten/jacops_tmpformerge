package csc.vehicle.tools.vehicle

import java.io._
import java.text.DecimalFormat

import com.typesafe.config.{ ConfigFactory, Config }
import csc.akka.config.ConfigUtil._
import csc.vehicle.tools.parser.{ Registration, CsvWriter, ShowRegistrations }
import csc.vehicle.tools.parser.ShowRegistrations.RegistrationHandler

/**
 * Created by carlos on 20/09/16.
 */
object OnTheFlyVehicleSimulator {

  import Options._

  def main(args: Array[String]) {
    parse(Options(), args.toList) match {
      case Left(error) ⇒
        println(error)
        sys.exit(1)
      case Right(options) ⇒ options.help match {
        case true ⇒
          printUsage
          sys.exit(0)
        case false ⇒
          val config = getConfig(options.conf)
          val sim = new OnTheFlyVehicleSimulator(config, options)

          val error: Option[String] = try {
            sim.run()
          } catch {
            case ex: Exception ⇒ Some(ex.toString)
          }

          error foreach (println(_))

          sys.exit(error.size) //empty = no errors = code 0
      }
    }
  }

  private def getConfig(path: Option[String]): SimulatorConfig = {
    val fallback = ConfigFactory.load("fast-simulator-defaults.conf")
    val cfg = path match {
      case None        ⇒ fallback
      case Some(value) ⇒ ConfigFactory.load(value).withFallback(fallback)
    }
    new SimulatorConfig(cfg.getConfig("simulator"))
  }

  case class Options(help: Boolean = false,
                     skipEntry: Boolean = false,
                     skipExit: Boolean = false,
                     conf: Option[String] = None,
                     sharedImageFolder: Option[String] = None,
                     entryImageFolder: Option[String] = None,
                     exitImageFolder: Option[String] = None,
                     generateOnly: Boolean = false,
                     noTimestampSort: Boolean = false,
                     resultCsv: Option[String] = None) {

    lazy val runEntry = !skipEntry
    lazy val runExit = !skipExit
    lazy val entryFolder: Option[String] = sharedImageFolder orElse entryImageFolder
    lazy val exitFolder: Option[String] = sharedImageFolder orElse exitImageFolder
    val sharedImages: Boolean = sharedImageFolder.isDefined && runEntry && runExit

  }

  object Options {

    val eitherEntryOrExit = "Cannot skip both entry and exit"
    val entryFolderRequired = "Must define either sharedImageFolder or entryImageFolder"
    val exitFolderRequired = "Must define either sharedImageFolder or exitImageFolder"

    def printUsage: Unit = {
      println("Simulator Usage [--help] [--conf <path>] [--generateOnly] [--skipEntry] [--skipExit] [--sharedImageFolder <path>] [--entryImageFolder <path>] [--exitImageFolder <path>] [--resultCsv <file_path>]")
      println("\t--help: show this information")
      println("\t--conf <path>: path to a .conf file (to override the defaults)")
      println("\t--generateOnly: only generates the script (no run). By default it runs it")
      println("\t--skipEntry: no entry registrations. By default both entry and exit registrations are generated")
      println("\t--skipExit: no exit registrations. By default both entry and exit registrations are generated")
      println("\t--sharedImageFolder <path>: path to folder with .jpg images to be used for both entry and exit registrations. This path has precedence over the other image path options")
      println("\t--entryImageFolder <path>: path to folder with .jpg images to be used for entry registrations")
      println("\t--exitImageFolder <path>: path to folder with .jpg images to be used for exit registrations")
      println("\t--resultCsv <file_path>: if running, path to csv file with the results. Default is /tmp/<script_filename>.csv")
      println("\t" + eitherEntryOrExit)
      println("\tnot skipping entry: " + entryFolderRequired)
      println("\tnot skipping exit: " + exitFolderRequired)
    }

    def parse(options: Options, list: List[String]): Either[String, Options] = {
      list match {
        case Nil ⇒
          if (options.skipEntry && options.skipExit) return Left(eitherEntryOrExit)
          if (options.runEntry && options.entryFolder.isEmpty) return Left(entryFolderRequired)
          if (options.runExit && options.exitFolder.isEmpty) return Left(exitFolderRequired)
          Right(options)

        case "--help" :: tail                       ⇒ Right(options.copy(help = true)) //immediate return (no recursion)
        case "--conf" :: value :: tail              ⇒ parse(options.copy(conf = Some(value)), tail)
        case "--skipEntry" :: tail                  ⇒ parse(options.copy(skipEntry = true), tail)
        case "--skipExit" :: tail                   ⇒ parse(options.copy(skipExit = true), tail)
        case "--generateOnly" :: tail               ⇒ parse(options.copy(generateOnly = true), tail)
        case "--noTimestampSort" :: tail            ⇒ parse(options.copy(noTimestampSort = true), tail)
        case "--sharedImageFolder" :: value :: tail ⇒ parse(options.copy(sharedImageFolder = Some(value)), tail)
        case "--entryImageFolder" :: value :: tail  ⇒ parse(options.copy(entryImageFolder = Some(value)), tail)
        case "--exitImageFolder" :: value :: tail   ⇒ parse(options.copy(exitImageFolder = Some(value)), tail)
        case "--resultCsv" :: value :: tail         ⇒ parse(options.copy(resultCsv = Some(value)), tail)
        case option :: tail                         ⇒ Left("Unknown option " + option)
      }
    }

  }

  case class Image(file: File, camId: String, delta: Int)

  object JpgFilter extends FileFilter {
    override def accept(file: File): Boolean = file.isFile && file.getName.endsWith(".jpg")
  }

  case class SimulatorConfig(cfg: Config) {
    lazy val ftpPort = cfg.getInt("ftp.port")
    lazy val ftpUser = cfg.getString("ftp.user")
    lazy val ftpPwd = cfg.getString("ftp.pwd")
    lazy val entryId = cfg.getString("corridor.entry")
    lazy val exitId = cfg.getString("corridor.exit")
    lazy val corridorType = cfg.getString("corridor.type")
    lazy val corridorLength = cfg.getInt("corridor.length")
    lazy val registrationDelay = delayAsString(cfg.getInt("script.registrationDelay"))
    lazy val inputDelay = cfg.getIntOpt("script.waitInputDelay") map (delayAsString(_))

    def delayAsString(value: Int) = new DecimalFormat("0000").format(value)
  }

  class Script(cfg: SimulatorConfig, writer: PrintWriter) {

    //println(cfg)

    private def laneConfig(id: String): String =
      """%s ID=%s HOST=localhost FTP-PORT=%s FTP-USER=%s FTP-PWD=%s""".format(cfg.corridorType, id, cfg.ftpPort, cfg.ftpUser, cfg.ftpPwd)

    private def corridorConfig: String =
      """CORRIDOR CORRIDOR-ID=corridor START-LANE-ID=%s END-LANE-ID=%s CORRIDOR-LENGTH=%s""".format(cfg.entryId, cfg.exitId, cfg.corridorLength)

    private def registration(laneId: String, image: File): String =
      "%s LANE-REGISTRATION LANE-ID=%s SPEED=100 CATEGORY=CAR IMAGE=%s".format(cfg.registrationDelay, laneId, image.getAbsolutePath)

    def writeConfig(): Unit = {
      writer.println("CONFIG-START")
      writer.println(laneConfig(cfg.entryId))
      writer.println(laneConfig(cfg.exitId))
      writer.println(corridorConfig)
      writer.println("CONFIG-END")
    }

    def writeStart(): Unit = {
      writer.println()
      writer.println("SCRIPT-START")
    }

    def writeRegistrations(images: List[Image]): Unit = {
      images foreach { i ⇒
        writer.println(registration(i.camId, i.file))
      }
    }

    def writeEnd(): Unit = {
      cfg.inputDelay foreach { value ⇒ writer.println("%s WAIT INPUT".format(value)) }
      writer.println("SCRIPT-END")
    }

  }

}

import OnTheFlyVehicleSimulator._

class OnTheFlyVehicleSimulator(cfg: SimulatorConfig, options: Options) extends CsvWriter {

  val scriptExtension = ".txt"
  val filenamePattern = """(\d+)\.(\d+)\.(\d+)\.(\d+)\.jpg""".r
  val resultHeaders = List("filename", "lane", "plate", "confidence")

  val file = File.createTempFile("script-", scriptExtension)
  //val file = new File("/tmp/script.txt")

  def run(): Option[String] = generateScript() match {
    case Left(exception) ⇒
      Some("Error generating script: " + exception.toString)
    case Right(file) ⇒
      options.generateOnly match {
        case true ⇒ None
        case false ⇒
          println("Running simulator on script " + file.getAbsolutePath)
          VehicleSimulator.executeEmbedded(file) //launches the simulator embedded
          Thread.sleep(5000) //waits for all the registrations to be processed (there is no way to wait for this accurately)

          val resultFilePath = options.resultCsv getOrElse {
            val name = file.getName.substring(0, file.getName.indexOf(scriptExtension))
            "/tmp/%s.csv".format(name)
          }

          println("Writing results to " + resultFilePath)

          val writer = new PrintWriter(new FileWriter(resultFilePath))
          val reader = new BufferedReader(new FileReader("/app/registration/logs/registration.log"))
          val handler: RegistrationHandler = { reg ⇒ writer.println(toCsv(reg)) }

          try {
            writer.println(resultHeaders.mkString(separator))
            ShowRegistrations.process(reader, handler) //writes all registrations to results
            writer.flush()
            println("CSV results written to file")
          } finally {
            writer.close()
            reader.close()
          }

          None
      }
  }

  override def toCsvElements(reg: Registration): List[Any] = {
    val image = reg.imageFilename map (sourceImageFilename(_)) getOrElse "noimage"
    val lane = reg.laneId
    val plate = reg.plateOpt getOrElse "noplate"
    val confidence = reg.plateConfidence
    val classification = reg.classificationOpt getOrElse "noclass"
    List(image, lane, plate, confidence, classification)
  }

  private def sourceImageFilename(name: String) = name.substring(name.indexOf(".") + 1, name.lastIndexOf("_")) + ".jpg"

  private def generateScript(): Either[Exception, File] = {

    if (imageFiles.isEmpty) throw new IllegalStateException("No images found in specified folders")

    val writer = new PrintWriter(new FileWriter(file))
    try {
      val script = new Script(cfg, writer)
      script.writeConfig()
      script.writeStart()
      script.writeRegistrations(imageFiles)
      script.writeEnd()
      writer.flush()
      Right(file)

    } catch {
      case ex: Exception ⇒
        ex.printStackTrace(System.err)
        Left(ex)
    } finally {
      writer.close()
    }
  }

  def getAllFiles(folder: File): List[File] = folder.exists() match {
    case true  ⇒ folder.listFiles(JpgFilter).toList.sortBy(_.getName)
    case false ⇒ Nil
  }

  def getAllImages(files: List[File], camId: String, baseTime: Int): List[Image] = options.noTimestampSort match {
    case false ⇒ files map { f ⇒ Image(f, camId, getTime(f.getName) - baseTime) } //delta is parsed from filename
    case true  ⇒ files.zipWithIndex map { e ⇒ Image(e._1, camId, e._2) } //delta is just the file index
  }

  def getTime(filename: String) = filenamePattern.findFirstMatchIn(filename) match {
    case None ⇒ throw new IllegalArgumentException(filename)
    case Some(m) ⇒
      val time = m.group(3)
      val secs1 = time.substring(4, 6).toInt
      val secs2 = time.substring(2, 4).toInt * 60
      val secs3 = time.substring(0, 2).toInt * 60 * 60
      val mills = m.group(4).toInt
      (secs1 + secs2 + secs3) * 1000 + mills
  }

  lazy val imageFiles: List[Image] = options.sharedImages match {
    case true ⇒ //shared images: sequence is all images as entry, then all images as exit
      val files = getAllFiles(new File(options.sharedImageFolder.get))
      val entry = getAllImages(files, cfg.entryId, 0) sortBy (_.delta)
      entry ++ (entry map { i ⇒ i.copy(camId = cfg.exitId) })
    case false ⇒ (options.runEntry, options.runExit) match {
      case (true, true) ⇒ //both entry and exit, each with a different image set: sequence is mixed
        val entryImages = getAllImages(getAllFiles(new File(options.entryFolder.get)), cfg.entryId, 0)
        val exitImages = getAllImages(getAllFiles(new File(options.exitFolder.get)), cfg.exitId, 0)
        (entryImages ++ exitImages) sortBy (_.delta)
      case (true, false) ⇒ //only entry
        getAllImages(getAllFiles(new File(options.entryFolder.get)), cfg.entryId, 0) sortBy (_.delta)
      case (false, true) ⇒ //only exit
        getAllImages(getAllFiles(new File(options.exitFolder.get)), cfg.exitId, 0) sortBy (_.delta)
      case (false, false) ⇒ throw new IllegalArgumentException(Options.eitherEntryOrExit)
    }
  }
}
