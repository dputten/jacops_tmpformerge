AKKA_HOME="$(cd "$(cd "$(dirname "$0")"; pwd -P)"/..; pwd)"
AKKA_CLASSPATH="$AKKA_HOME/lib/*:$AKKA_HOME/config:$AKKA_HOME/deploy/*:$AKKA_HOME/bin/amigoboras.sensor-test.jar"

java -cp $AKKA_CLASSPATH tools.Simulator "$@"
