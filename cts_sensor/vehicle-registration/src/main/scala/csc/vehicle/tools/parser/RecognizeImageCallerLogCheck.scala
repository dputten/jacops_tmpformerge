package tools.parser

import java.io.File

import scala.io.Source

/**
 * Created by carlos on 15/02/16.
 */
object RecognizeImageCallerLogCheck {

  val prefix = "[INFO] ["
  val eventIdPrefix = "Perf eventId=["

  trait EntryType
  case object Request extends EntryType
  case object Response extends EntryType

  def main(args: Array[String]) {
    //val file = new File(args(0))
    var set: Set[String] = Set.empty

    //val file = new File("/Users/carlos/Documents/csc/tickets/PCL-27/logs/problematic/registration.log.2016-02-09")
    //val file = new File("/Users/carlos/Documents/csc/tickets/PCL-27/logs/problematic/ric.txt")
    val file = new File("/Users/carlos/Documents/csc/tickets/PCL-27/logs/problematic/ric_2.txt")

    for (line ← Source.fromFile(file).getLines()) {
      getEntry(line).foreach { e ⇒
        //println(e)
        e.tp match {
          case Request  ⇒ set = set + e.event
          case Response ⇒ set = set - e.event
        }
        //if (set.size % 10 == 0) {
        if (set.size < 50) {
          //println(set)
          println(e.time + ": " + set.size)
        }
      }
    }

  }

  def getEntry(line: String): Option[Entry] = {
    val tp: Option[EntryType] = if (line.contains("point 7:")) {
      Some(Request)
    } else if (line.contains("point 34:")) {
      Some(Response)
    } else None

    tp map { t ⇒
      val time = line.substring(prefix.length, line.indexOf(']', prefix.length))
      val idx = line.indexOf(eventIdPrefix)
      val start = idx + eventIdPrefix.length
      val event = line.substring(start, line.indexOf(']', start))
      Entry(t, time, event)
    }
  }

  case class Entry(tp: EntryType, time: String, event: String)

}

