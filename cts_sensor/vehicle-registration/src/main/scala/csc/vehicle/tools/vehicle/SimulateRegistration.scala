/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.tools.vehicle

import csc.vehicle.registration.RadarCalculations
import scala.concurrent.duration.Duration
import java.util.Date
import csc.gantry.config.RadarCameraSensorConfig
import csc.akka.logging.DirectLogging
import csc.common.Extensions._
import akka.actor.{ ActorLogging, Actor }

trait SimulateRegistration {
  def sendRegistration(image: String, speed: Double, length: Option[Double], category: Option[String], now: Date, delay: Option[Long])
  def getLaneId(): String
}

case class FtpConfig(host: String, port: Int, username: String, password: String)

/**
 * This class simulates a registration using an Image FTP and a radar message
 * @param config the configuration also used in the application
 * @param ftpConfig FTP configuration
 * @param radarPort the port of the radar listener
 * @param radarDelay  the delay between ftp and radar message
 */
class SimulateRadarCamera(config: RadarCameraSensorConfig,
                          ftpConfig: FtpConfig,
                          radarPort: Int,
                          radarDelay: Duration) extends SimulateRegistration with DirectLogging {
  val radarCalc = new RadarCalculations(config.radar)
  val ftp = new SendFTPRegistration(config.camera.relayURI, ftpConfig)
  val radar = new SendRadarRegistration(host = ftpConfig.host, radarPort = radarPort, laneId = config.lane.laneId)

  def sendRegistration(image: String, speed: Double, len: Option[Double], category: Option[String], now: Date, delay: Option[Long]) {
    val (length, reflection) = len match {
      case None    ⇒ (0.0, 0)
      case Some(l) ⇒ (l, radarCalc.calculateReflection(l))
    }
    log.info("Lane [%s] Sending registration time=%s image=%s speed=%.2f length=%.2f".format(config.lane.laneId, now.toString("HH:mm:ss.SSS z", "UTC"), image, speed, length))
    if (radarDelay.toMillis >= 0) {
      //image first
      ftp.sendRegistration(image, now)
      Thread.sleep(radarDelay.toMillis)
      radar.sendRegistration(speed.toInt, reflection)
    } else {
      val delay = math.abs(radarDelay.toMillis)
      radar.sendRegistration(speed.toInt, reflection)
      Thread.sleep(delay)
      ftp.sendRegistration(image, now)
    }
  }
  def getLaneId(): String = {
    config.lane.laneId
  }
}

class SimulateSimpleCamera(id: String, ftpConfig: FtpConfig) extends SimulateRegistration with DirectLogging {
  val ftp = new SendFTPRegistration(id, ftpConfig)

  def sendRegistration(image: String, speed: Double, len: Option[Double], category: Option[String], now: Date, delay: Option[Long]) {

    delay match {
      case Some(d) if (d > 0) ⇒
        log.info("Camera [%s] Sending timed registration delay=%s image=%s speed=%.2f ".format(id, d, image, speed))
        ftp.sendTimedRegistration(image, d)

      case _ ⇒
        log.info("Camera [%s] Sending registration time=%s image=%s speed=%.2f ".format(id, now.toString("HH:mm:ss.SSS z", "UTC"), image, speed))
        ftp.sendRegistration(image, now)
    }

  }

  def getLaneId(): String = id
}

/**
 * This class simulates a registration using an Image FTP and a radar message
 * @param config the configuration also used in the application
 * @param ftpConfig FTP configuration
 * @param radarPort the port of the radar listener
 * @param radarDelay  the delay between ftp and radar message
 */
class SimulatePirRadarCamera(config: RadarCameraSensorConfig,
                             ftpConfig: FtpConfig,
                             radarPort: Int,
                             radarDelay: Duration,
                             pirPort: Int,
                             pirTimeDelta: Long) extends SimulateRegistration with DirectLogging {
  val radarCalc = new RadarCalculations(config.radar)
  val ftp = new SendFTPRegistration(config.camera.relayURI, ftpConfig)
  val radar = new SendRadarRegistration(host = ftpConfig.host, radarPort = radarPort, laneId = config.lane.laneId)
  val pir = new SendPirRegistration(
    host = ftpConfig.host,
    pirPort = pirPort,
    lane = config.lane,
    pirTimeDelta = pirTimeDelta)

  def sendRegistration(image: String, speed: Double, len: Option[Double], category: Option[String], now: Date, delay: Option[Long]) {
    val (length, reflection) = len match {
      case None    ⇒ (0.0, 0)
      case Some(l) ⇒ (l, radarCalc.calculateReflection(l))
    }
    val cat = category match {
      case Some("CAR")         ⇒ 1
      case Some("TRUCK")       ⇒ 3
      case Some("LARGE_TRUCK") ⇒ 4
      case other               ⇒ 0
    }
    log.info("Lane [%s] Sending registration time=%s image =%s cat=%d speed=%.2f length=%.2f".format(config.lane.laneId, now.toString("HH:mm:ss.SSS z", "UTC"), image, cat, speed, length))
    if (radarDelay.toMillis >= 0) {
      //image first
      try {
        ftp.sendRegistration(image, now)
      } catch {
        case ex: Exception ⇒ log.error("Lane [%s] Failed to send Image %s".format(getLaneId(), ex))
      }
      Thread.sleep(radarDelay.toMillis)
      try {
        radar.sendRegistration(speed.toInt, reflection)
      } catch {
        case ex: Exception ⇒ log.error("Lane [%s] Failed to send Radar %s".format(getLaneId(), ex))
      }
    } else {
      val delay = math.abs(radarDelay.toMillis)
      try {
        radar.sendRegistration(speed.toInt, reflection)
      } catch {
        case ex: Exception ⇒ log.error("Lane [%s] Failed to send Radar %s".format(getLaneId(), ex))
      }
      Thread.sleep(delay)
      try {
        ftp.sendRegistration(image, now)
      } catch {
        case ex: Exception ⇒ log.error("Lane [%s] Failed to send Image %s".format(getLaneId(), ex))
      }
    }
    try {
      if (cat != 0) {
        pir.sendRegistration(now, speed.toFloat, length.toFloat, cat)
      } else {
        log.warning("Lane [%s] Skipping PIR message for time=%s image =%s cat=%d speed=%.2f length=%.2f".format(config.lane.laneId, now.toString("HH:mm:ss.SSS z", "UTC"), image, cat, speed, length))
      }
    } catch {
      case ex: Exception ⇒ log.error("Lane [%s] Failed to send PIR %s".format(getLaneId(), ex))
    }

  }
  def getLaneId(): String = {
    config.lane.laneId
  }
}

/**
 * This class simulates a registration using an Image FTP and a radar message
 * @param config the configuration also used in the application
 * @param ftpConfig FTP configuration
 */
class SimulatePirCamera(config: RadarCameraSensorConfig,
                        ftpConfig: FtpConfig,
                        pirPort: Int,
                        pirTimeDelta: Long) extends SimulateRegistration with DirectLogging {
  val radarCalc = new RadarCalculations(config.radar)
  val ftp = new SendFTPRegistration(config.camera.relayURI, ftpConfig)
  val pir = new SendPirRegistration(
    host = ftpConfig.host,
    pirPort = pirPort,
    lane = config.lane,
    pirTimeDelta = pirTimeDelta)

  def sendRegistration(image: String, speed: Double, len: Option[Double], category: Option[String], now: Date, delay: Option[Long]) {
    val length = len match {
      case None    ⇒ 0.0
      case Some(l) ⇒ l
    }
    val cat = category.map(_.toUpperCase) match {
      case Some("PERS")          ⇒ 1
      case Some("CARTRAILER")    ⇒ 2
      case Some("CAR_TRAILER")   ⇒ 2
      case Some("TRUCK")         ⇒ 3
      case Some("LARGE_TRUCK")   ⇒ 4
      case Some("LARGETRUCK")    ⇒ 4
      case Some("BUS")           ⇒ 5
      case Some("UNKNOWN")       ⇒ 6
      case Some("CAR")           ⇒ 7
      case Some("TRUCKTRAILER")  ⇒ 8
      case Some("TRUCK_TRAILER") ⇒ 8
      case Some("SEMITRAILER")   ⇒ 9
      case Some("SEMI_TRAILER")  ⇒ 9
      case Some("MOTOR")         ⇒ 10
      case Some("VAN")           ⇒ 11
      case other                 ⇒ 0
    }
    log.info("Lane [%s] Sending registration time=%s image =%s cat=%d speed=%.2f length=%.2f".format(config.lane.laneId, now.toString("HH:mm:ss.SSS z", "UTC"), image, cat, speed, length))
    //image first
    try {
      ftp.sendRegistration(image, now)
    } catch {
      case ex: Exception ⇒ log.error("Lane [%s] Failed to send Image %s".format(getLaneId(), ex))
    }
    try {
      if (cat != 0) {
        pir.sendRegistration(now, speed.toFloat, length.toFloat, cat)
      } else {
        log.warning("Lane [%s] Skipping PIR message for time=%s image =%s cat=%d speed=%.2f length=%.2f".format(config.lane.laneId, now.toString("HH:mm:ss.SSS z", "UTC"), image, cat, speed, length))
      }
    } catch {
      case ex: Exception ⇒ log.error("Lane [%s] Failed to send PIR %s".format(getLaneId(), ex))
    }
  }
  def getLaneId(): String = {
    config.lane.laneId
  }
}

case class SimulateVehicleRegistration(image: String, speed: Double, length: Option[Double],
                                       category: Option[String], time: Date, delay: Option[Long] = None)

/**
 * Actor to keep track of the registrations. And gives warnings when registrations are
 * To close and maybe result in problems in Vehicle Registration application
 * @param reg
 */
class SimulateRegistrationActor(reg: SimulateRegistration) extends Actor with ActorLogging {
  var lastMsgTime = new Date()
  def receive = {
    case msg: SimulateVehicleRegistration ⇒ {
      reg.sendRegistration(msg.image, msg.speed, msg.length, msg.category, msg.time, msg.delay)
      if (msg.time.before(lastMsgTime)) {
        log.warning("Lane [%s] sends an old registration lastMsgTime=%s currentMsgTime= %s".format(reg.getLaneId(),
          lastMsgTime.toString("HH:mm:ss.SSS z", "UTC"),
          msg.time.toString("HH:mm:ss.SSS z", "UTC")))
      } else {
        val timeBetween = msg.time.getTime - lastMsgTime.getTime
        if (timeBetween < 200) { //lowered a bit the warning threshold for close messages (was 500). Should be more natural to reproduce real traffic situations
          log.warning("Lane [%s] sends an registration close to last registration time between registrations %d".format(
            reg.getLaneId(),
            timeBetween))
        }
        lastMsgTime = msg.time
      }
    }
  }
}
