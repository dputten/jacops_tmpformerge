package csc.vehicle.tools

import java.io.{ FileFilter, File }

import csc.vehicle.registration.images.{ JaiMakerNoteHandler, ReadWriteExifUtil }

/**
 * Created by carlos on 10.11.15.
 */
object ShowExifTool {

  val filter = new FileFilter {
    override def accept(pathname: File): Boolean = pathname.getName.endsWith(".jpg")
  }

  def main(args: Array[String]): Unit = {
    val folder = new File(args(0))
    val files = folder.listFiles(filter).toList

    for (i ← 0 until 3) {
      val file = files(i)
      println("--" + file.getName)
      val exif = ReadWriteExifUtil.readExif(file)
      exif.foreach(println)
      val makerNotes = JaiMakerNoteHandler.getJaiMakerNotes(exif)
      makerNotes.values.foreach(println)
    }
  }

}
