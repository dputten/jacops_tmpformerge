/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.tools.vehicle

import java.io.File
import java.text.SimpleDateFormat

import org.apache.commons.net.ftp.{ FTP, FTPClient }
import java.util.{ TimerTask, Timer, Date }
import resource._
import csc.akka.logging.DirectLogging

/**
 * Simulate a registration using a FTP upload of a image
 * @param outputDir The directory where the images has to be places
 * @param config FTP configuration
 */
class SendFTPRegistration(outputDir: String, config: FtpConfig) extends DirectLogging {

  /**
   * Update the time in the image and send it to the ftpserver
   * @param fileName The filename of the image
   * @param imageTime The new image time
   */
  def sendRegistration(fileName: String, imageTime: Date): Unit = {
    sendFile(ImageFile(new File(fileName), imageTime), ftpConn())
  }

  import SendFTPRegistration._

  def sendTimedRegistration(fileName: String, delay: Long) {
    val sendTime = System.currentTimeMillis() + 1000 //one second from now, to give us some time to prepare
    val exifTime = new Date(sendTime - delay)
    val file = ImageFile(new File(fileName), exifTime)
    file.newContents //making sure we resolve this now
    val ftp = ftpConn()

    val task = new TimerTask {
      override def run(): Unit = {
        sendFile(file, ftp)
      }
    }

    val executionTime = nextExecutionTime(sendTime)

    timer.schedule(task, executionTime)
  }

  private def ftpConn(): FTPClient = FTPSession.connect(config, outputDir)

  private def sendFile(file: ImageFile, ftp: FTPClient): Unit = {
    try {
      log.info("Sending image using FTP time=%s, EXIF time=%s".format(format(new Date()), format(file.exifTime)))
      FTPSession.put(ftp, file.newFilename, file.newContents)
    } finally {
      ftp.disconnect()
    }
  }

}

object SendFTPRegistration {
  val dfPattern = "HH:mm:ss.SSS z"
  val timer = new Timer(false)
  val latency: Long = 10 //~ typical delay observed between sending and receiving in FTP on the test machine

  def format(date: Date): String = new SimpleDateFormat(dfPattern).format(date)

  def nextExecutionTime(sendTime: Long) = new Date(sendTime - latency)

  def nextSecond(delay: Long) = (System.currentTimeMillis() + delay + 1000) / 1000 * 1000
}

case class ImageFile(sourceFile: File, exifTime: Date) {
  lazy val newFilename: String = System.currentTimeMillis().toString + "." + sourceFile.getName
  lazy val newContents: Array[Byte] = ModifyBayerTiff.replaceImageCreationTime(sourceFile, exifTime)
}

object FTPSession {

  def connect(config: FtpConfig, dir: String): FTPClient =
    connect(config.host, config.port, config.username, config.password, dir)

  def connect(host: String, port: Int, username: String, password: String, dir: String): FTPClient = {
    val ftp = new FTPClient()
    ftp.connect(host, port)
    try {
      ftp.login(username, password)
      ftp.setFileType(FTP.BINARY_FILE_TYPE)
      ftp.changeWorkingDirectory(dir)
      ftp
    } catch {
      case ex: Exception ⇒
        ftp.disconnect()
        throw ex
    }
  }

  def put(ftp: FTPClient, filename: String, contents: ⇒ Array[Byte]): Unit = {
    for (stream ← managed(ftp.storeFileStream(filename))) {
      stream.write(contents)
    }
  }

}