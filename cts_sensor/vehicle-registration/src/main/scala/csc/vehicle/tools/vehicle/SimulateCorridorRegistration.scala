/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.tools.vehicle

import akka.actor.{ ActorLogging, ActorRef, Actor }
import java.util.Date
import scala.concurrent.duration._
import csc.common.Extensions._

case class SimulateVehicleRegistrationEnd(image: String, speed: Double, length: Option[Double], category: Option[String], time: Date)

/**
 * Trigger the two lane registrations depending on the speed and length
 * @param startLane the reference to the first LaneRegistrationActor
 * @param endLane the reference to the last LaneRegistrationActor
 * @param length the length between the two LaneRegistrations
 */
class SimulateCorridorRegistration(startLane: ActorRef, endLane: ActorRef, length: Float) extends Actor with ActorLogging {

  implicit val executor = context.system.dispatcher

  def receive = {
    case msg: SimulateVehicleRegistration ⇒ {
      startLane ! msg
      //calculate delay
      if (msg.speed != 0) {
        val delay = (length / msg.speed * 3600).toLong //delay in milliseconds
        val newTime = new Date(msg.time.getTime + delay)
        //schedule next registration
        val regEndMsg = new SimulateVehicleRegistrationEnd(image = msg.image, speed = msg.speed,
          length = msg.length, category = msg.category, time = newTime)
        context.system.scheduler.scheduleOnce(delay milliseconds, self, regEndMsg)
      } else {
        log.error("Sending registration time=%s image =%s cat=%s speed=%.2f length=%.2f".format(msg.time.toString("HH:mm:ss.SSS z", "UTC"), msg.image, msg.category, msg.speed, msg.length.getOrElse(-1)))
      }
    }
    case msg: SimulateVehicleRegistrationEnd ⇒ {
      endLane ! new SimulateVehicleRegistration(image = msg.image, speed = msg.speed, length = msg.length,
        category = msg.category, time = msg.time)
    }
  }
}