package csc.vehicle.tools

import java.io._
import java.net.Socket
import java.util.Date

import akka.actor.{ Actor, ActorRef, ActorSystem, Props }
import csc.vehicle.tools.vehicle.FTPSession
import csc.vehicle.tools.vehicle.ModifyBayerTiff

import scala.collection.mutable.HashSet

/**
 * This class simulates the camera and the radar messages.
 * @param <inputDir> The directory where the pictures are located which has to be send to the application
 * @param <outputDir> The directory of the ftp site where the pictures has to be stored (see configuration of the sensor)
 * @param <delay Images (msec)> The delay between the sending of the pictures
 * @param <delay Radar> The delay between the sending of the picture and the radar trigger
 * @param <Host> The host of the application
 * @param <radarPort> The port of the radar listener (see configuration of the sensor)
 * @param <ftpPort> The ftp port
 * @param <ftpUser>The user used for the FTP
 * @param <pwd> The password of the ftpUser
 *
 */
object Simulator {
  def main(args: Array[String]): Unit = {
    if (args.size != 9) {
      println("Usage:")
      println("java Simulator <inputDir> <outputDir> <delay Images (msec)> <delay Radar> <Host> <radarPort> <ftpPort> <ftpUser> <pwd>")
      return
    }
    val inputDir = args(0)
    val outputDir = args(1)
    val delayImage = args(2).toInt
    val delayRadar = args(3).toInt
    val host = args(4)
    val radarPort = args(5).toInt
    val ftpPort = args(6).toInt
    val ftpUser = args(7)
    val pwd = args(8)

    println("Config")
    println("\tinputDir " + inputDir)
    println("\toutputDir " + outputDir)
    println("\tdelayImage " + delayImage)
    println("\tdelayRadar " + delayRadar)
    println("\tHost " + host)
    println("\tradarPort " + radarPort)
    println("\tftpPort " + ftpPort)
    println("\tftpuser " + ftpUser)
    val system = ActorSystem("Simulator")
    //use multiple actors when radardelay > image delay
    val nrActors: Int = (delayRadar / delayImage + 1).toInt
    println("number Actors " + nrActors)
    val actorList = new HashSet[ActorRef]()
    for (i ← 0 until nrActors) {
      val actor = system.actorOf(Props(new SimulateRadarCameraActor(delayRadar, outputDir, host, radarPort, host, ftpPort, ftpUser, pwd)))
      actorList += actor
    }
    println("starting")
    val dir = new File(inputDir)
    var list = dir.list();
    var i: Int = 0
    while (i < list.size) {
      println("looping " + i)
      for (actor ← actorList) {
        if (i < list.size) {
          println("LOOP: start")
          val file = new File(inputDir, list(i))
          actor ! file.getAbsolutePath
          println("LOOP: msg Send")
          i += 1
          println("LOOP: start wait " + delayImage)
          Thread.sleep(delayImage) //wait one second
        }
      }
    }
    //Done stop actors
    println("Stopping Actors")
    system.shutdown()
    println("Done")
  }
}

class SimulateRadarCameraActor(deltaTimeRadar: Int, outputDir: String, radarHost: String, radarPort: Int, ftpHost: String, ftpPort: Int, ftpUser: String, pwd: String) extends Actor {
  val radarMsg = "12;800;"

  def receive = {
    case fileName: String ⇒ {

      var radar: Socket = null
      var out: PrintWriter = null
      var in: BufferedReader = null

      if (deltaTimeRadar > 0) {
        try {
          radar = new Socket(radarHost, radarPort);
          out = new PrintWriter(radar.getOutputStream(), true);
          in = new BufferedReader(new InputStreamReader(radar.getInputStream()));
        } catch {
          case e: java.net.ConnectException ⇒ {
            println("Connect problem can't send radar msg:" + e.getMessage)
          }
        }
      }
      val inputFile = new File(fileName)

      def fileContents: Array[Byte] = ModifyBayerTiff.replaceImageCreationTime(inputFile, new Date)

      val ftp = FTPSession.connect(ftpHost, ftpPort, ftpUser, pwd, outputDir)
      try {
        FTPSession.put(ftp, inputFile.getName, fileContents)
      } finally {
        ftp.disconnect()
      }

      println("delay radar:" + deltaTimeRadar)
      Thread.sleep(deltaTimeRadar)
      if (out != null) {
        try {
          out.println(radarMsg)
          println("RadarMsg send")
          val result = in.readLine
          println("Received reply: " + result)
        } finally {
          out.close
          in.close
          radar.close
        }
      }
    }
  }
}

