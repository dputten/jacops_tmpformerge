/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.tools.vehicle

import csc.akka.logging.DirectLogging
import csc.vehicle.message.{ ExifValueString, ExifTagInfo }
import csc.vehicle.registration.images.ReadWriteExifUtil
import ftplet.FileReadyNotifier
import org.apache.commons.io.FileUtils
import java.text.{ MessageFormat, DecimalFormat, SimpleDateFormat }
import java.util.{ TimeZone, Date }
import java.net.Socket
import java.io._
import resource._

/**
 * Simulates a vehicle registration, by sending messages directly to the ActorConsumers
 * using local host
 */
class SendRelayRegistration(radarPort: Int, relayPort: Int, laneDir: String) {
  val host = "localhost"
  val relayNotifier = new FileReadyNotifier(host, relayPort)

  /**
   * Sends the registration
   * @param image the image which has to be send
   * @param speed the speed of the vehicle
   * @param reflection the reflection of the vehicle
   * @param now the time the vehicle passed
   * @param radarDelay the delay time between radar en relay
   */
  def sendRegistration(image: String, speed: Int, reflection: Int, now: Date, radarDelay: Int) {
    val newImage = copyAndModifyImage(image, now)
    relayNotifier.sendNotification(newImage, now.getTime, "put")
    println("sleep: " + radarDelay.toLong)
    //Thread.sleep(radarDelay.toLong)
    println("sleepEnd: " + radarDelay.toLong)
    sendRadar(speed, reflection)
  }

  /**
   * Modify image by changing the time and copy saves it in the correct directory
   * @param image the image
   * @param creationDate the new date
   * @return  the new file
   */
  def copyAndModifyImage(image: String, creationDate: Date): String = {
    val src = new File(image)
    val modifiedImage = ModifyBayerTiff.replaceImageCreationTime(src, creationDate)
    val newFile = new File(laneDir, src.getName)
    FileUtils.writeByteArrayToFile(newFile, modifiedImage)
    newFile.getAbsolutePath
  }

  /**
   * Sends the radar message
   * @param speed the speed of the vehicle
   * @param reflection the reflection of the vehicle
   */
  def sendRadar(speed: Int, reflection: Int) {
    for (
      radar ← managed(new Socket(host, radarPort));
      out ← managed(new PrintWriter(radar.getOutputStream(), true));
      in ← managed(new BufferedReader(new InputStreamReader(radar.getInputStream())))
    ) {
      val radarMsg = "%d;%d;".format(speed, reflection)
      out.println(radarMsg)
      //just wait for the reply
      in.readLine
    }
  }
}

/**
 * Modifies the image with a new creation time
 */
object ModifyBayerTiff extends DirectLogging {
  val dateFormat = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss")
  dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"))
  val startPosDate = 443
  val startPosMsec = 258

  /**
   * Read the image and replace the image creation time in the file.
   * And return the byte Array
   */
  def replaceImageCreationTime(from: java.io.File, imageDate: Date): Array[Byte] = {
    if (from.getName.endsWith(".jpg"))
      replaceJpegCreationTime(from, imageDate)
    else
      replaceTiffCreationTime(from, imageDate)
  }

  def replaceJpegCreationTime(from: java.io.File, imageDate: Date): Array[Byte] = {
    val byteArray = FileUtils.readFileToByteArray(from)
    val exif = ReadWriteExifUtil.readExif(new ByteArrayInputStream(byteArray))
    val modified = replaceDateTimeTag(exif, imageDate)
    ReadWriteExifUtil.writeExif(byteArray, modified)
  }

  def replaceDateTimeTag(source: Seq[ExifTagInfo], imageDate: Date): Seq[ExifTagInfo] = {
    val dateString = dateFormat.format(imageDate)
    val millisString = (imageDate.getTime % 1000).toString
    for (tag ← source) yield {
      if (tag.tagName.startsWith("Date/Time")) {
        tag.copy(value = ExifValueString(dateString))
      } else if (tag.tagName.startsWith("Sub-Sec Time")) {
        tag.copy(value = ExifValueString(millisString))
      } else tag
    }
  }

  def replaceTiffCreationTime(from: java.io.File, imageDate: Date): Array[Byte] = {
    val byteArray = FileUtils.readFileToByteArray(from)
    var correctedPos = startPosDate
    //find position
    //Bytes before date (decimal) 65:48:0
    if (byteArray(startPosDate - 3) != 65 || byteArray(startPosDate - 2) != 48 || byteArray(startPosDate - 1) != 0) {
      //probably gray
      if (byteArray(startPosDate - 4) != 65 || byteArray(startPosDate - 3) != 48 || byteArray(startPosDate - 2) != 0) {
        //error
        throw new IllegalArgumentException("Image isn't a tif from a Jai camera")
      } else {
        correctedPos -= 1
      }
    }
    //replace date
    val newDateStr = dateFormat format imageDate
    val newDate = (dateFormat format imageDate).getBytes
    for (i ← 0 until newDate.size) {
      byteArray(correctedPos + i) = newDate(i)
    }

    //replace msec
    val msec = "%3d".format(imageDate.getTime % 1000).getBytes
    for (i ← 0 until msec.size) {
      byteArray(startPosMsec + i) = msec(i)
    }
    byteArray
  }

}