package csc.vehicle.tools.parser

import java.io.{ BufferedWriter, StringReader, FileReader, BufferedReader }

/**
 * Created by carlos on 11/04/16.
 */
object ShowRegistrations extends CsvWriter {

  type RegistrationHandler = Registration ⇒ Unit

  val csvHeaders: List[String] = List(
    "Time",
    "LaneId",
    "ImageFilename",
    //"ImageId",
    "Plate",
    "PlateConfidence",
    "PlateX",
    "PlateY",
    "PlateWidth",
    "PlateHeight",
    "Class",
    "ClassConfidence",
    "ExternalClass")

  def toCsvElements(reg: Registration): List[Any] = {

    val (plateX, plateY, plateWidth, plateHeight) = reg.lpd match {
      case None    ⇒ (0, 0, 0, 0)
      case Some(l) ⇒ (l.plateBase._1, l.plateBase._2, l.plateWidth, l.plateHeight)
    }

    List(
      reg.time,
      reg.laneId,
      reg.imageFilename.getOrElse(""),
      //reg.imageId.getOrElse(""),
      reg.plate,
      reg.plateConfidence,
      plateX,
      plateY,
      plateWidth,
      plateHeight,
      reg.classification,
      reg.classificationConfidence,
      "")
  }

  def main(args: Array[String]): Unit = {

    val reader: BufferedReader = args.size match {
      case 0                    ⇒ Console.in
      case 2 if args(0) == "-f" ⇒ new BufferedReader(new FileReader(args(1)))
      case 2 if args(0) == "-s" ⇒ new BufferedReader(new StringReader(args(1)))
      case _ ⇒
        Console.err.println("Usage showRegistrations.sh [-f file] [-s string]")
        Console.err.println("If no file or string provided, reads from standard input")
        System.exit(-1)
        null //never reaches here
    }

    try {
      println(csvHeaders.mkString(separator))
      process(reader, printRegistration)
    } finally {
      if (args.size > 0) reader.close() //close reader, it the case
    }
  }

  def printRegistration(reg: Registration): Unit = println(toCsvElements(reg).mkString(separator))

  def process(reader: BufferedReader, handler: RegistrationHandler): Unit = {
    Iterator.continually(reader.readLine).takeWhile(_ != null).foreach { line ⇒
      VehicleMetadataParser.toRegistration(line) foreach { handler(_) }
    }
  }
}

trait CsvWriter {

  val separator = ";"

  def toCsvElements(reg: Registration): List[Any]

  def toCsv(reg: Registration): String = {
    toCsvElements(reg).mkString(separator)
  }

}