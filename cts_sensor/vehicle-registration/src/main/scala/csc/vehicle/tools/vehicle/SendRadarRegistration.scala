/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.tools.vehicle

import java.net.Socket
import java.io.{ InputStreamReader, BufferedReader, PrintWriter }
import resource._
import csc.akka.logging.DirectLogging

/**
 * Simulate a radar registration
 * @param host  The host of the radar listener
 * @param radarPort The port of the radar listener
 */
class SendRadarRegistration(host: String, radarPort: Int, laneId: String) extends DirectLogging {

  /**
   * Send the registration to the radar listener
   * @param speed The simulated speed of the vehicle
   * @param reflection The simulated reflection of the vehicle
   */
  def sendRegistration(speed: Int, reflection: Int) {

    for (
      radar ← managed(new Socket(host, radarPort));
      out ← managed(new PrintWriter(radar.getOutputStream(), true));
      in ← managed(new BufferedReader(new InputStreamReader(radar.getInputStream())))
    ) {
      try {
        val radarMsg = "%d;%d;".format(speed, reflection)
        out.println(radarMsg)
        val result = in.readLine
      } catch {
        case e: java.net.ConnectException ⇒ {
          println("Connect problem can't send radar msg:" + e.getMessage)
        }
      }
    }
    log.info("Lane [%s] Sending radar speed=%d reflection=%d".format(laneId, speed, reflection))
  }

}
