package csc.vehicle.tools.control

import java.io.{ BufferedReader, FileReader, StringReader }
import java.util.UUID
import java.util.concurrent.TimeUnit

import akka.actor.{ ActorRef, ActorSystem }
import base.Mode

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.FiniteDuration
import com.typesafe.config.ConfigFactory
import csc.akka.logging.DirectLogging
import csc.amqp.{ AmqpSerializers, MQConnection, Publisher }
import csc.vehicle.message.{ GantryControlMessage, GetModeRequest, SetModeRequest }
import net.liftweb.json.{ DefaultFormats, Formats }

/**
 * Created by carlos on 22/06/16.
 */
object GantryControlTool extends AmqpSerializers with DirectLogging with Publisher {

  override val ApplicationId: String = "GantryContolTool"
  override implicit val formats: Formats = DefaultFormats

  val cfg = ConfigFactory.load() //.load("akka.conf")
  implicit val actorSystem: ActorSystem = ActorSystem(ApplicationId, cfg)
  override implicit val executor: ExecutionContext = actorSystem.dispatcher

  Runtime.getRuntime.addShutdownHook(new Thread() {
    override def run(): Unit = actorSystem.shutdown()
  })

  lazy val connection: MQConnection =
    MQConnection.create(ApplicationId, "amqp://localhost:5672/", FiniteDuration(1, TimeUnit.SECONDS))

  lazy val producer: ActorRef = connection.producer

  def main(args: Array[String]): Unit = {
    val reader: BufferedReader = args.size match {
      case 0                    ⇒ Console.in
      case 2 if args(0) == "-f" ⇒ new BufferedReader(new FileReader(args(1)))
      case 2 if args(0) == "-s" ⇒ new BufferedReader(new StringReader(args(1)))
      case _ ⇒
        Console.err.println("Usage gantryControl.sh [-f file] [-s string]")
        Console.err.println("If no file or string provided, reads from standard input")
        System.exit(-1)
        null //never reaches here
    }

    try {
      process(reader)
      if (args.size > 0) Thread.sleep(2000) //making sure whatever was sent gets flushed (useful in -f and -s)
      System.exit(0)
    } finally {
      if (args.size > 0) reader.close() //close reader, it the case
    }
  }

  val setExpression = """(.+) set (.+)""".r
  val getExpression = """(.+) get""".r

  def createCorrelationId = UUID.randomUUID().toString

  def toMessage(cmd: String): Option[GantryControlMessage] = cmd match {
    case setExpression(gantry, mode) ⇒
      Mode.forName(mode) match {
        case None ⇒
          println(s"Unrecognized mode $mode")
          None
        case _ ⇒ Some(SetModeRequest(createCorrelationId, gantry, mode, System.currentTimeMillis(), "test"))
      }
    case getExpression(gantry) ⇒ Some(GetModeRequest(createCorrelationId, gantry, System.currentTimeMillis()))
    case other ⇒
      println("Cannot understand command " + cmd)
      None
  }

  def publish(msg: GantryControlMessage): Unit = {
    val payload = toPublishPayload[msg.type](msg)
    val name = msg.getClass.getSimpleName
    val key = "%s.%s".format(msg.gantry, name)
    publishOrError("gantry-in-exchange", key, payload, name)
  }

  def process(reader: BufferedReader): Unit = {
    Iterator.continually(reader.readLine).takeWhile(_ != null).foreach { line ⇒
      toMessage(line) foreach { publish(_) }
    }
  }

}