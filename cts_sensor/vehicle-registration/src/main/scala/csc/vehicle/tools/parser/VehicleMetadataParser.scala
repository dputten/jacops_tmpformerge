package csc.vehicle.tools.parser

import java.text.SimpleDateFormat
import java.util.Date

import LogParser._

/**
 * Knows how to parse VehicleMetadata case classes from strings
 * Created by carlos on 11/04/16.
 */
object VehicleMetadataParser {
  val queuedStr = "Queued "
  val metadataStr = "VehicleMetadata"
  val markerStr = queuedStr + metadataStr

  def toRegistration(line: String): Option[Registration] =
    for (l ← registrationString(line); s ← parseComposite(l)) yield toRegistration(s)

  def toRegistration(src: Composite): Registration = {
    val version = src.parts(0) match {
      case s @ Composite("Version", _) ⇒ s.intAt(0)
      case _                           ⇒ 1
    }
    version match {
      case 1     ⇒ RegistrationV1(src)
      case 2     ⇒ RegistrationV2(src)
      case 3     ⇒ RegistrationV3(src)
      case other ⇒ throw new UnsupportedOperationException("record version " + other)
    }
  }

  def registrationString(str: String): Option[String] = str.indexOf(markerStr) match {
    case -1 ⇒ None
    case idx ⇒ str.indexOf(metadataStr, idx) match {
      case -1    ⇒ None
      case start ⇒ Some(str.substring(start))
    }
  }
}

case class VehicleImage(src: Composite) {
  lazy val imageType = src.stringAt(3)
  lazy val imagePath: String = src.stringAt(2)
}

case class LicensePlateData(src: Composite) {
  private lazy val lp: Option[List[Int]] = src.compositeOptAt(1) match {
    case None      ⇒ None
    case Some(pos) ⇒ Some(pos.parts map { e ⇒ e.toString.toInt })
  }

  private lazy val plateBox: Option[(Int, Int, Int, Int)] = lp map { e ⇒
    val minx = math.min(e(0), e(4))
    val maxx = math.min(e(2), e(6))
    val miny = math.min(e(1), e(3))
    val maxy = math.min(e(5), e(7))
    (minx, maxx, miny, maxy)
  }

  lazy val plateWidth = plateBox map { pb ⇒ pb._2 - pb._1 } getOrElse 0
  lazy val plateHeight = plateBox map { pb ⇒ pb._4 - pb._3 } getOrElse 0
  lazy val plateBase: (Int, Int) = plateBox map { e ⇒ (e._1, e._3) } getOrElse ((0, 0))
  lazy val plate: Option[String] = src.compositeOptAt(0) map (_.stringAt(0))
  lazy val plateConfidence: Option[Int] = src.compositeOptAt(0) map (_.intAt(1))
}

trait Registration {
  val src: Composite
  val df = new SimpleDateFormat("yyyyMMdd.HHmmss.SSS")

  lazy val time: String = {
    val millis = src.longAt(timeIdx)
    df.format(new Date(millis))
  }

  lazy val eventId: String = src.stringAt(eventIdIdx)
  lazy val laneId: String = src.compositeAt(laneIdx).stringAt(0)
  lazy val imagePath: Option[String] = images.find(_.imageType == "Overview") map (_.imagePath)
  lazy val imageId: Option[String] = imagePath map (_.replace('/', '_'))
  lazy val imageFilename: Option[String] = imagePath map (p ⇒ p.substring(p.lastIndexOf('/') + 1))
  lazy val lpd: Option[LicensePlateData] = src.compositeOptAt(lpdIdx) map LicensePlateData
  lazy val plate: String = plateOpt getOrElse ("")
  lazy val plateOpt: Option[String] = lpd flatMap (_.plate)
  lazy val plateConfidence: Int = lpd flatMap (_.plateConfidence) getOrElse (0)
  lazy val classificationOpt: Option[String] = src.compositeOptAt(classificationIdx) map (_.stringAt(0))
  lazy val classification: String = classificationOpt getOrElse ""
  lazy val classificationConfidence: Int = src.compositeOptAt(classificationIdx) map (_.intAt(1)) getOrElse (0)
  lazy val images: List[VehicleImage] = src.compositeListAt(imagesIdx) map VehicleImage

  val laneIdx: Int
  val lpdIdx: Int
  val timeIdx: Int
  val eventIdIdx: Int
  val imagesIdx: Int
  val classificationIdx: Int
}

case class RegistrationV1(src: Composite) extends Registration {
  val laneIdx: Int = 0
  val eventIdIdx: Int = 1
  val timeIdx: Int = 2
  val classificationIdx: Int = 7
  val imagesIdx: Int = 10
  val lpdIdx: Int = 17
}

case class RegistrationV2(src: Composite) extends Registration {
  val laneIdx: Int = 1
  val eventIdIdx: Int = 2
  val timeIdx: Int = 3
  val classificationIdx: Int = 6
  val imagesIdx: Int = 8
  val lpdIdx: Int = 12
}

case class RegistrationV3(src: Composite) extends Registration {
  val laneIdx: Int = 1
  val eventIdIdx: Int = 2
  val timeIdx: Int = 3
  val classificationIdx: Int = 6
  val imagesIdx: Int = 8
  val lpdIdx: Int = 9
}

