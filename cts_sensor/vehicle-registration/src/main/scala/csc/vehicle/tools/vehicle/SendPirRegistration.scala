package csc.vehicle.tools.vehicle

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

import java.net.Socket
import java.io.PrintWriter
import resource._
import csc.vehicle.pir.VehiclePIRRegistration
import java.util.Date
import net.liftweb.json.{ DefaultFormats, Serialization }
import csc.akka.logging.DirectLogging
import csc.common.Extensions._
import csc.vehicle.message.Lane

/**
 * Simulate a pir registration
 * @param host  The host of the pir listener
 * @param pirPort The port of the pir listener
 * @param lane The lane information
 */
class SendPirRegistration(host: String, pirPort: Int, lane: Lane, pirTimeDelta: Long) extends DirectLogging {

  /**
   * Send the registration to the pir listener
   * @param eventTime the time of the pir registration
   * @param speed the detected speed
   * @param length the detected length
   */
  def sendRegistration(eventTime: Date, speed: Float, length: Float, category: Int) {
    val pirRegistration = VehiclePIRRegistration(lane = lane,
      eventTime = eventTime.getTime + pirTimeDelta,
      speed = Some(speed),
      length = Some(length),
      category = Some(category),
      confidence = None)
    val pirMsg = Serialization.write(pirRegistration)(DefaultFormats)

    for (
      radar ← managed(new Socket(host, pirPort));
      out ← managed(new PrintWriter(radar.getOutputStream(), true))
    ) {
      try {
        out.println(pirMsg)
      } catch {
        case e: java.net.ConnectException ⇒ {
          println("Connect problem can't send pir msg:" + e.getMessage)
        }
      }
    }
    log.info("Lane [%s] Sending pir time=%s speed=%.2f length=%.2f".format(lane.laneId, (new Date(pirRegistration.eventTime)).toString("HH:mm:ss.SSS z", "UTC"), speed, length))
  }
}
