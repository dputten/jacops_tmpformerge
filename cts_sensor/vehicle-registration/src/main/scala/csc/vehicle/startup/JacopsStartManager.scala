/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.startup

import akka.actor._
import csc.amqp.AmqpConnection
import csc.base.{ ActorPaths, SendCertificateMsg, Wakeup }
import csc.gantry.config.LaneConfig.LaneConfigProvider
import csc.gantry.config._
import csc.image.ImageTimeExtractor
import csc.timeregistration.config.SoftwareChecksum
import csc.vehicle.config._
import csc.vehicle.message.{ CertificationData, Priority }
import csc.vehicle.registration.{ SensorRelayMQConsumer, GantryControlAmqpAdapter, RelaySensorConfig }
import csc.vehicle.registration.classifier.ClassifierMQAdapter
import csc.vehicle.registration.recognizer.RecognizerMQAdapter

import scala.collection.JavaConversions._

class JacopsStartManager(configuration: Configuration,
                         staticLaneConfig: LaneConfiguration,
                         amqpConnection: AmqpConnection,
                         initialCertificationData: Option[CertificationData],
                         lastStep: Option[ActorRef] = None) extends Actor with ActorLogging {

  implicit val system = context.system
  val config = configuration.globalConfig

  log.info("StartManager config: {}", config)
  log.info("StartManager staticLaneConfig: {}", staticLaneConfig)

  val recognizeTransportActor: ActorRef = config.recognizerTransport match {
    case cfg: RecognizerQueueConfig ⇒ RecognizerMQAdapter.create(context, amqpConnection, cfg, configuration.systemId)
    case other                      ⇒ throw new UnsupportedOperationException("Not supported: " + other)
  }

  val classifyTransportActor: Option[ActorRef] =
    config.classifierConfig map { ClassifierMQAdapter.create(context, amqpConnection, _, configuration.systemId) }

  val actorPaths = ActorPaths(recognizeTransportActor.path.toString, classifyTransportActor map { _.path.toString })

  context.system.eventStream.publish(actorPaths) //other actors will need this

  var lanes: Set[String] = Set.empty

  val systemId: String = staticLaneConfig.getAllSystemIds().toList match {
    case Nil       ⇒ throw new IllegalArgumentException("No system defined")
    case List(sys) ⇒ sys
    case list      ⇒ throw new IllegalArgumentException("Too many systems. For jacops only one is allowed: " + list)
  }

  val systemConfig = staticLaneConfig.getSystemConfig(systemId).get
  val laneConfigMap: Map[String, LaneConfig] = staticLaneConfig.getLanes().map { cfg ⇒ cfg.lane.laneId -> cfg }.toMap

  val laneConfigProvider: LaneConfigProvider = Left({ laneId ⇒ laneConfigMap(laneId) })

  val registrationCompletedBuilder =
    WorkflowBuilder.createRegistrationCompletedBuilder(config, None, amqpConnection.connection.producer)

  val workflowBuilder = new MQWorkflowBuilder(system, systemConfig, config, laneConfigProvider,
    actorPaths, registrationCompletedBuilder, amqpConnection)

  val relaySensor: ActorRef = workflowBuilder.build() //builds the workflow, retuning the relay actor

  config.gantryControlConfig foreach { c ⇒
    //wires AMQP to relaySensor for control messages
    GantryControlAmqpAdapter.create(context, c, amqpConnection.connection, relaySensor)
  }

  log.info("SensorStartUp done")

  def receive = {
    case SendCertificateMsg ⇒
      log.info("Got SendCertificateMsg, now checking and trying to send certificates.")
      checkCertificate(staticLaneConfig.getLanes())
    case Wakeup ⇒ {

      val currentLanes = configuration.gantryIds.toList.flatMap { gantryId ⇒
        staticLaneConfig.getLanesForGantry(configuration.systemId, gantryId)
      }

      if (currentLanes.isEmpty) {
        log.info("No matching lanes found. system = {}, gantries = {} lanes = {}",
          configuration.systemId, configuration.gantryIds, staticLaneConfig.getLanes().map(_.lane))
      }

      //check new or removed lanes
      updateLanes(currentLanes)
    }
  }

  override def postStop() {
    super.postStop()
    log.info("StartManager is stopped")
  }

  private def checkCertificate(lanes: List[LaneConfig]): Unit = {
    config.amqpCertificateCheckConfig foreach { cfg ⇒
      SoftwareChecksum.createHashFromRuntimeClass(classOf[ImageTimeExtractor]) foreach { softwareChecksum ⇒
        val certificate = ComponentCertificate(softwareChecksum.fileName, softwareChecksum.checksum)
        val activeCertificate = ActiveCertificate(System.currentTimeMillis(), certificate)

        val actor = context.actorOf(Props(new AmqpCertificateActor(amqpConnection.connection.producer, cfg)))
        actor ! AmqpCertificateActor.SendCerts(lanes, activeCertificate)
      }
      //if (applChecksum.isEmpty) log.error("Could not calculate Certificate checksum, not sending Certificate(s)")
    }
  }

  private def updateLanes(currentLanes: List[LaneConfig]) {
    log.debug("update lanes {}", currentLanes)
    var tmpLanes: Set[String] = lanes
    currentLanes.foreach(lane ⇒ {
      if (tmpLanes(lane.lane.laneId)) {
        tmpLanes = tmpLanes.filterNot(_ == lane.lane.laneId)
      } else {
        //newSystem
        staticLaneConfig.getSystemConfig(lane.lane.system) match {
          case Some(sysConfig) ⇒ {
            log.info("Starting Lane %s".format(lane.lane.laneId))
            val active = LaneActivation(true, Priority.NONCRITICAL)
            try {
              startLane(lane, active)
            } catch {
              case ex: Exception ⇒
                log.error("Failed to start lane %s: %s".format(lane.lane.laneId, ex.getMessage))
            }
          }
          case None ⇒ log.error("Systemconfiguration is missing for %s. skip Lane %s".format(lane.lane.system, lane.lane.laneId))
        }
      }
    })
    //removed lanes
    tmpLanes.foreach(laneId ⇒ {
      //stop actor after processing it current messages
      //remove from list
      lanes -= laneId
    })
  }

  private def startLane(laneConfig: LaneConfig, active: LaneActivation) {
    val lane = laneConfig.lane

    //add lane to relay
    val relayConfig = new RelaySensorConfig(lane, null, laneConfig.camera, active.active, active.priority, systemConfig.typeCertificates)
    relaySensor ! relayConfig
    lanes += laneConfig.lane.laneId
  }
}

