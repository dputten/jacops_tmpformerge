/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.startup

import csc.image.ImageTimeExtractor
import csc.vehicle.message.CertificationData
import imagefunctions.bayertif.impl.BayerTifFunctionsImpl
import imagefunctions.jpg.impl.JpgFunctionsImpl
import imagefunctions.{ ImageFunctionsFactory, LicenseFormat }
import akka.actor._

import scala.concurrent.duration._
import csc.vehicle.registration._
import events.EventBuilder
import csc.vehicle.config._
import csc.gantry.config._
import csc.timeregistration.config.SoftwareChecksum
import akka.event.Logging
import csc.base.{ SendCertificateMsg, Wakeup }
import csc.amqp.AmqpConnection
import csc.watchdog.WatchdogManagerActor

class StartupVehicleRegistration(configuration: Configuration, staticLaneConfig: LaneConfiguration) {

  implicit val system: ActorSystem = ActorSystem("VehicleRegistration")

  val log = Logging(system, getClass)

  implicit val executor = system.dispatcher

  val config = configuration.globalConfig

  /**
   * Creating actors and start the image server.
   * returns when the VehicleRegistration is up and running
   */
  def start(lastStep: Option[ActorRef] = None) = {
    log.info("Starting vehicleRegistration")

    val echo = system.actorOf(Props(new EchoActor))
    system.eventStream.subscribe(echo, classOf[DeadLetter])

    val amqpConnection = new AmqpConnection(config.amqpConfig)
    amqpConnection.init()

    config.watchdogConfig foreach (WatchdogManagerActor.create(system, _, amqpConnection))

    //start all support actors
    //ImageFunctionsFactory.setDefaultFunctions() //we should not/need not to load all the native libs together
    ImageFunctionsFactory.setJpgFunctions(JpgFunctionsImpl.getInstance()) //ugly but efficient and straight to the point
    if (!config.imageConfig.sourceImageJpeg) ImageFunctionsFactory.setBayerTifFunctions(BayerTifFunctionsImpl.getInstance())
    ImageFunctionsFactory.setLicenceFormat(LicenseFormat.UMLAUTS)

    EventBuilder.setTimeShift(config.joinEventConfig.shiftJoinPeriod)

    val manager: ActorRef = config.workflowConfig match {
      case GenericWorkflowConfig ⇒
        system.actorOf(Props(new StartManager(
          configuration = configuration,
          staticLaneConfig = staticLaneConfig,
          amqpConnection = amqpConnection,
          initialCertificationData = initialCertificationData,
          lastStep = lastStep)), "StartManager")

      case cfg: MQWorkflowConfig ⇒
        system.actorOf(Props(new JacopsStartManager(
          configuration = configuration,
          staticLaneConfig = staticLaneConfig,
          amqpConnection = amqpConnection,
          initialCertificationData = initialCertificationData,
          lastStep = lastStep)), "StartManager")
    }

    //start Configuration update
    //system.scheduler.schedule(1 millis, config.configUpdateFrequency, receiver = manager, message = Wakeup)
    system.scheduler.scheduleOnce(1 millis, manager, Wakeup) //jacops assumption: no dynamic config, only once will do
    system.scheduler.scheduleOnce(500 millis, manager, SendCertificateMsg)

    log.info("Started vehicleRegistration")
  }
  def stop() {
    log.info("Shutdown vehicleRegistration")
    system.shutdown()
    log.info("Awaiting for system termination")
    system.awaitTermination() //PCL-27: we have to do this, otherwise we can end up with messy duplicate actor paths
    log.info("System successfully terminated")
  }

  lazy val initialCertificationData: Option[CertificationData] = StartupVehicleRegistration.certificationData(config)
  //
  //    if (config.certConfig.isCertificationEnabled) {
  //    val applChecksum = SoftwareChecksum.createHashFromRuntime(classOf[ImageTimeExtractor]).getOrElse("No checksum available")
  //    Some(CertificationData(Map("ImageTimeExtractor" -> applChecksum), Map.empty))
  //  } else None
}

class EchoActor extends Actor with ActorLogging {
  override def receive: Receive = {
    case msg: DeadLetter ⇒ log.warning("Received {}", msg)
  }
}

object StartupVehicleRegistration {

  def certificationData(config: VehicleRegistrationConfiguration): Option[CertificationData] = {
    if (config.certConfig.isCertificationEnabled) {
      val applChecksum = SoftwareChecksum.createHashFromRuntime(classOf[ImageTimeExtractor]).getOrElse("No checksum available")
      Some(CertificationData(Map("ImageTimeExtractor" -> applChecksum), Map.empty))
    } else None
  }

}