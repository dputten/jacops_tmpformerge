package csc.vehicle.startup

import akka.actor.ActorRef

/**
 * Created by carlos on 30/11/16.
 */

class ActorChainBuilder(end: Option[ActorRef] = None) {

  private var path: List[ActorRef] = Nil

  def nextActors: Set[ActorRef] = path match {
    case Nil ⇒ end.toSet
    case _   ⇒ Set(path.last)
  }

  def add(actor: ActorRef): Unit = {
    path = path :+ actor
  }

  def first: Set[ActorRef] = path match {
    case Nil ⇒ Set.empty
    case _   ⇒ Set(path.last)
  }

  //  override def clone: ActorChainBuilder = {
  //    val result = new ActorChainBuilder(end)
  //    path foreach(result.add(_))
  //    result
  //  }

}
