/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.startup

import akka.actor._
import csc.akka.state.StatePersisterActor
import csc.amqp.AmqpConnection
import csc.image.ImageTimeExtractor
import csc.timeregistration.config.SoftwareChecksum
import csc.vehicle.DoubleTriggerMonitorActor
import csc.vehicle.config._
import csc.gantry.config._
import csc.vehicle.registration._
import image.ProcessAdmin
import csc.gantry.config.SystemConfig
import csc.vehicle.message.{ CertificationData, Lane, Priority }

import scala.concurrent.Await
import scala.concurrent.duration._
import csc.base.{ ActorPaths, SendCertificateMsg, Wakeup }
import csc.vehicle.registration.classifier.ClassifierMQAdapter
import csc.vehicle.registration.recognizer.RecognizerMQAdapter

import scala.collection.JavaConversions._

class StartManager(configuration: Configuration,
                   staticLaneConfig: LaneConfiguration,
                   amqpConnection: AmqpConnection,
                   initialCertificationData: Option[CertificationData],
                   labelPath: Option[String] = None,
                   lastStep: Option[ActorRef] = None) extends Actor with ActorLogging {

  implicit val system = context.system
  val dynamicConfig = new DynamicLaneConfiguration(staticLaneConfig, context.system)
  val config = configuration.globalConfig

  log.info("StartManager config: {}", config)
  log.info("StartManager staticLaneConfig: {}", staticLaneConfig)

  val imagePreProcessor: ImagePreProcessor = ImagePreProcessor(config)
  val admin = context.actorOf(Props(new ProcessAdmin("imageHost", 9999, None)))
  val statePersister = context.actorOf(Props(new StatePersisterActor()))
  val cleanup = context.actorOf(Props(new CleanupVehicleRegistration(Set())), "Cleanup")
  CleanupVehicleRegistration.subscribe(cleanup, context.system.eventStream)

  val relaySensor = context.actorOf(Props(new RelaySensorData(initialCertificationData,
    Some(admin), Some(statePersister), imagePreProcessor, config.initialMode)), "RelaySensorData")

  createSensorRelayAdapter(config.relayAdapterConfig, relaySensor)

  var doubleTriggerMonitorMap: Map[String, ActorRef] = Map()

  val recognizeTransportActor: ActorRef = config.recognizerTransport match {
    case cfg: RecognizerQueueConfig ⇒ RecognizerMQAdapter.create(context, amqpConnection, cfg, configuration.systemId)
    case other                      ⇒ throw new UnsupportedOperationException("Not supported: " + other)
  }

  val classifyTransportActor: Option[ActorRef] =
    config.classifierConfig map { ClassifierMQAdapter.create(context, amqpConnection, _, configuration.systemId) }

  config.gantryControlConfig foreach { c ⇒
    //wires AMQP to relaySensor for control messages
    GantryControlAmqpAdapter.create(context, c, amqpConnection.connection, relaySensor)
  }

  val actorPaths = ActorPaths(recognizeTransportActor.path.toString, classifyTransportActor map { _.path.toString })

  context.system.eventStream.publish(actorPaths) //other actors will need this

  var systems = Seq[String]()
  var lanes = Map[String, ActorRef]()
  log.info("SensorStartUp done")

  def receive = {
    case SendCertificateMsg ⇒
      log.info("Got SendCertificateMsg, now checking and trying to send certificates.")
      checkCertificate(dynamicConfig.getLanes())
    case Wakeup ⇒ {
      dynamicConfig.update()
      try {
        Await.result(dynamicConfig.laneAgent.future(), 1 second)
      } catch {
        case ex: Exception ⇒ {
          //do nothing and use the old data
          log.info("Timeout while updating configuration. Using older configuration")
        }
      }

      val currentLanes = configuration.gantryIds.toList.flatMap { gantryId ⇒ dynamicConfig.getLanesForGantry(configuration.systemId, gantryId) }
      if (currentLanes.isEmpty) {
        log.info("No matching lanes found. system = {}, gantries = {} lanes = {}",
          configuration.systemId, configuration.gantryIds, dynamicConfig.getLanes().map(_.lane))
      }

      //check new or removed lanes
      updateLanes(currentLanes)
    }
  }

  override def postStop() {
    super.postStop()
    log.info("StartManager is stopped")
  }

  private def createSensorRelayAdapter(config: RelayAdapterConfig, delegate: ActorRef): Unit = config match {
    case c: CamelRelayAdapterConfig ⇒
      //camel/mina adapter
      context.actorOf(Props(new SensorRelayCamelAdapter(c.uri, delegate)))

    case c: AmqpRelayAdapterConfig ⇒
      //amqp adapter
      val consumer = context.actorOf(Props(new SensorRelayMQConsumer(delegate)))
      amqpConnection.queue(c.services).wireConsumer(consumer, false)
  }

  private def checkCertificate(lanes: List[LaneConfig]): Unit = {
    config.amqpCertificateCheckConfig foreach { cfg ⇒
      SoftwareChecksum.createHashFromRuntimeClass(classOf[ImageTimeExtractor]) foreach { softwareChecksum ⇒
        val certificate = ComponentCertificate(softwareChecksum.fileName, softwareChecksum.checksum)
        val activeCertificate = ActiveCertificate(System.currentTimeMillis(), certificate)

        val actor = context.actorOf(Props(new AmqpCertificateActor(amqpConnection.connection.producer, cfg)))
        actor ! AmqpCertificateActor.SendCerts(lanes, activeCertificate)
      }
      //if (applChecksum.isEmpty) log.error("Could not calculate Certificate checksum, not sending Certificate(s)")
    }
  }

  private def updateLanes(currentLanes: List[LaneConfig]) {
    log.debug("update lanes {}", currentLanes)
    var tmpLanes = lanes.keySet
    currentLanes.foreach(lane ⇒ {
      if (tmpLanes.contains(lane.lane.laneId)) {
        tmpLanes = tmpLanes.filterNot(_ == lane.lane.laneId)
      } else {
        //newSystem
        dynamicConfig.getSystemConfig(lane.lane.system) match {
          case Some(sysConfig) ⇒ {
            log.info("Starting Lane %s".format(lane.lane.laneId))
            val active = LaneActivation(true, Priority.NONCRITICAL)
            try {
              val dtm = doubleTriggerMonitor(lane.lane)
              startLane(sysConfig, lane, active, dtm)
            } catch {
              case ex: Exception ⇒
                log.error("Failed to start lane %s: %s".format(lane.lane.laneId, ex.getMessage))
            }
          }
          case None ⇒ log.error("Systemconfiguration is missing for %s. skip Lane %s".format(lane.lane.system, lane.lane.laneId))
        }
      }
    })
    //removed lanes
    tmpLanes.foreach(laneId ⇒ {
      //stop actor after processing it current messages
      lanes.get(laneId).foreach(_ ! PoisonPill)
      //remove from list
      lanes -= laneId
    })
  }

  /**
   * Strategy method that defines the criteria for assigning doubleTriggerMonitors to lanes.
   * The default implementation is per gantry, if filter is enabled by config
   * @param lane
   * @return double trigger monitor actor for the given lane
   */
  private def doubleTriggerMonitor(lane: Lane): Option[ActorRef] = {
    val key = lane.gantry
    config.doubleRegistrationFilterConfig match {
      case None ⇒ None
      case Some(cfg) ⇒ {
        doubleTriggerMonitorMap.get(key) match {
          case None ⇒ {
            val monitor = context.actorOf(Props(new DoubleTriggerMonitorActor(cfg)), "DoubleTriggerMonitorActor-" + key)
            doubleTriggerMonitorMap = doubleTriggerMonitorMap.updated(key, monitor)
            Some(monitor)
          }
          case some ⇒ some
        }
      }
    }
  }

  private def startLane(systemConfig: SystemConfig,
                        laneConfig: LaneConfig,
                        active: LaneActivation,
                        doubleTriggerMonitor: Option[ActorRef]) {
    val lane = laneConfig.lane

    val actor = context.actorOf(StartLaneActor.props(
      actorPaths,
      context,
      amqpConnection.connection.producer,
      config,
      systemConfig,
      dynamicConfig.getCorridorsConfig(lane.system),
      laneConfig,
      doubleTriggerMonitor,
      lastStep), laneConfig.lane.laneId)

    //add lane to relay
    val relayConfig = new RelaySensorConfig(lane, actor, laneConfig.camera, active.active, active.priority, systemConfig.typeCertificates)
    relaySensor ! relayConfig
    lanes += laneConfig.lane.laneId -> actor
  }
}

