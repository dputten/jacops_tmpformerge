/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.startup

import java.rmi.registry.LocateRegistry
import management.ManagementFactory
import collection.mutable.HashMap
import javax.management.remote.{ JMXConnectorServerFactory, JMXServiceURL }
import collection.JavaConversions._

/**
 * Starting JMX on specified port numbers, which has to be used when firewalls are present in the network
 * Because using the system properties, changes the JMX port dynamically
 */
object JMXSupport {
  def startJMX(host: String, passwordFile: String, accessFile: String, rmiPort: Int, JMXPort: Int) {
    // Ensure cryptographically strong random number generator used
    // to choose the object number - see java.rmi.server.ObjID
    //
    System.setProperty("java.rmi.server.randomIDs", "true")

    // Start an RMI registry on given port
    //
    LocateRegistry.createRegistry(rmiPort)

    // Retrieve the PlatformMBeanServer.
    val mbs = ManagementFactory.getPlatformMBeanServer

    // Environment map.
    //
    val env = new HashMap[String, Object]

    // Provide the password file used by the connector server to
    // perform user authentication. The password file is a properties
    // based text file specifying username/password pairs.
    //
    env.put("jmx.remote.x.password.file", passwordFile)

    // Provide the access level file used by the connector server to
    // perform user authorization. The access level file is a properties
    // based text file specifying username/access level pairs where
    // access level is either "readonly" or "readwrite" access to the
    // MBeanServer operations.
    //
    env.put("jmx.remote.x.access.file", accessFile)

    // Create an RMI connector server.
    //
    // As specified in the JMXServiceURL the RMIServer stub will be
    // registered in the RMI registry running in the local host on
    // port 3000 with the name "jmxrmi". This is the same name the
    // out-of-the-box management agent uses to register the RMIServer
    // stub too.
    //
    val url = new JMXServiceURL("service:jmx:rmi://%s:%d/jndi/rmi://%s:%d/jmxrmi".format(host, JMXPort, host, rmiPort))

    val cs = JMXConnectorServerFactory.newJMXConnectorServer(url, env, mbs)
    // Start the RMI connector server.
    //
    //log.info("Start the RMI connector server");
    cs.start()
  }

}