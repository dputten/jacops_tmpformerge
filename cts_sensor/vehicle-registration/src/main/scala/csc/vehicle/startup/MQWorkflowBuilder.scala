package csc.vehicle.startup

import akka.actor._
import akka.routing.RoundRobinRouter
import base.RecipientList
import com.github.sstone.amqp.Amqp.Ack
import csc.akka.state.StatePersisterActor
import csc.amqp.{ ServicesConfiguration, PublisherActor, AmqpConnection }
import csc.amqp.adapter.MQConsumer
import csc.base.{ SensorPosition, ActorPaths }
import csc.gantry.config.LaneConfig.LaneConfigProvider
import csc.gantry.config.SystemConfig
import csc.vehicle.MultiGantryDoubleTriggerFilter
import csc.vehicle.config._
import csc.vehicle.message.{ VehicleImageType, VehicleRegistrationFormats, VehicleRegistrationMessage }
import csc.vehicle.registration.{ CleanupVehicleRegistration, SensorRelayMQConsumer, CommonWorkflowRelaySensor, ImagePreProcessor }
import csc.vehicle.startup.WorkflowBuilder.{ ActorBuilder, ActorOf }
import image.ProcessAdmin

/**
 * Created by carlos on 06/12/16.
 */
class MQWorkflowBuilder(system: ActorSystem,
                        val systemConfig: SystemConfig,
                        val globalConfig: VehicleRegistrationConfiguration,
                        val laneConfigProvider: LaneConfigProvider,
                        val actorPaths: ActorPaths,
                        val registrationCompletedBuilder: ActorBuilder,
                        amqpConnection: AmqpConnection) extends WorkflowBuilder {

  val workflowConfig = globalConfig.workflowConfig.asInstanceOf[MQWorkflowConfig] //jacops assumption

  private val poolSize: Int = {
    val laneIds = globalConfig.laneIdSet
    val result = math.max(1, workflowConfig.poolSize)
    log.info("Pool size: {}, Lane Ids: {}", result, laneIds)
    result
  }

  val coordinator: ActorRef = system.actorOf(Props(new RegistrationMQCoordinator), "RegistrationMQCoordinator")

  /**
   * Builds all the actor chains necessary to the MQ workflow, focused on the Jacops spec
   * @return sensor relay actor ref
   */
  def build(): ActorRef = {
    createFinisherChain()
    createClassifierChain()
    createRecognizerChain()
    createRelayChain()
  }

  private def producerEndpoint: String = workflowConfig.producerEndpoint
  private val queuePrepared = workflowConfig.preparedQueue
  private val queueRecognized = workflowConfig.recognizedQueue
  private val queueClassified = workflowConfig.classifiedQueue

  override def actorOfFunction: ActorOf = { (props: Props, name: Option[String]) ⇒
    name match {
      case None    ⇒ system.actorOf(props)
      case Some(n) ⇒ system.actorOf(props, n)
    }
  }

  override def actorOf(props: Props) = actorOf(props, None)

  private def actorOf(props: Props, name: Option[String]) = actorOfFunction(routed(props), name)

  private def singleActorOf(props: Props, name: Option[String]) = actorOfFunction(props, name) //always a non routed actor

  private def routed(props: Props): Props = poolSize match {
    case 1 ⇒ props //no routing
    case x ⇒ props.withRouter(RoundRobinRouter(nrOfInstances = poolSize))
  }

  private def createFinisherChain(): Unit = {
    val chain = createChain
    chain.addCheckpoint(None, "cleanup") //post cleanup checkpoint

    val cleaner = actorOf(cleanerProps(chain.nextActors), Some("CleanupVehicleRegistration"))
    //subscribed to eventStream at the router level, otherwise all routees get the same messages
    CleanupVehicleRegistration.subscribe(cleaner, system.eventStream)
    chain.addActor(cleaner)

    chain.addCheckpoint(None, "complete") //post complete checkpoint
    chain.addActor(registrationCompletedBuilder(actorOfFunction, chain.nextActors))
    signerProps(chain.nextActors) foreach { props ⇒ chain.addActor(actorOf(props, Some("RegistrationSigner"))) }
    createDoubleTriggerFilter(chain.nextActors) foreach { actor ⇒ chain.addActor(actor) }
    chain.createQueueInActor(queueClassified) //input queue: classified
  }

  private def createClassifierChain(): Unit = {
    val chain = createChain
    chain.addCheckpoint(Some(queueClassified), "classified") //output queue: classified
    chain.addActor(singleActorOf(customClassifierProps(chain.nextActors).get, Some("CustomClassifier")))
    chain.createQueueInActor(queueRecognized) //input queue: recognized
  }

  private def createRecognizerChain(): Unit = {
    val chain = createChain
    chain.addCheckpoint(Some(queueRecognized), "recognized") //output queue: recognized
    noPlateFilterProps(chain.nextActors) foreach { props ⇒ chain.addActor(actorOf(props, Some("NoPlateFilter"))) }
    chain.addActor(actorOf(triggerSourceChangerProps("Joined", chain.nextActors), Some("ChangeTriggerSource")))
    chain.addActor(singleActorOf(recognizeImageCallerProps(chain.nextActors, VehicleImageType.OverviewMasked), Some(recogImageCallerName)))
    //always and only masked
    val maskConfig: TriggerMaskConfig = globalConfig.maskConfiguration.get.asInstanceOf[TriggerMaskConfig] //jacops assumption
    chain.addActor(actorOf(triggerImageMaskerProps(chain.nextActors, maskConfig), Some("TriggerMaskImage")))
    chain.createQueueInActor(queuePrepared) //input queue: prepared
  }

  private def createRelayChain(): ActorRef = {
    val chain = createChain
    chain.addCheckpoint(Some(queuePrepared), "prepared") //output queue: prepared
    chain.addActor(actorOf(configDataAdderProps(chain.nextActors), Some("AddConfigData")))
    chain.addActor(actorOf(imageMetadataReaderProps(chain.nextActors, SensorPosition.Trigger), Some("ReadTiffImageMetaData"))) //jacops assumption

    val relayActor = createSensorRelayActor(chain.nextActors)
    chain.addActor(relayActor)

    //sensor relay adapter reads CameraImageMessage, not VehicleRegistratioMessages, so cannot use 'createQueueInActor'
    createSensorRelayAdapter(relayActor)
    relayActor
  }

  private def createSensorRelayActor(nextActors: Set[ActorRef]): ActorRef = {

    val imagePreProcessor: ImagePreProcessor = ImagePreProcessor(globalConfig)
    val admin = actorOf(Props(new ProcessAdmin("imageHost", 9999, None)))
    val statePersister = actorOf(Props(new StatePersisterActor()))
    val initialCertificationData = StartupVehicleRegistration.certificationData(globalConfig)

    val props = Props(new CommonWorkflowRelaySensor(
      initialCertificationData,
      Some(admin),
      Some(statePersister),
      imagePreProcessor,
      globalConfig.initialMode,
      nextActors))

    singleActorOf(props, Some("CommonWorkflowRelaySensor")) //single actor (non-routed)
  }

  private def createSensorRelayAdapter(delegate: ActorRef): Unit = {

    //config: RelayAdapterConfig,
    globalConfig.relayAdapterConfig match {
      case c: AmqpRelayAdapterConfig ⇒
        //amqp adapter
        val consumer = actorOf(Props(new SensorRelayMQConsumer(delegate)), Some("SensorRelayMQConsumer"))
        amqpConnection.queue(c.services).wireConsumer(consumer, false)
      case other ⇒ throw new IllegalArgumentException("Only AmqpRelayAdapterConfig is supported. Please provide one. Got instead " + other)
    }
  }

  private def createDoubleTriggerFilter(nextActors: Set[ActorRef]): Option[ActorRef] = {
    globalConfig.doubleRegistrationFilterConfig map { c ⇒
      singleActorOf(Props(new MultiGantryDoubleTriggerFilter(nextActors, c)), Some("MultiGantryDoubleTriggerFilter"))
    }
  }

  private def createChain: MQWorkflowChain = new MQWorkflowChain

  class MQWorkflowChain {

    private val chain: ActorChainBuilder = new ActorChainBuilder()

    def nextActors: Set[ActorRef] = chain.nextActors

    def addCheckpoint(routeKey: Option[String], step: String): ActorRef = {
      val recipients = nextActors

      val publisher: Option[ActorRef] = routeKey map { key ⇒
        singleActorOf(Props(new RegistrationMQProducer(amqpConnection.connection.producer, producerEndpoint, key)), Some(s"RegistrationMQProducer-$step"))
      }

      val actor = actorOf(Props(new RegistrationCheckOutActor(coordinator, recipients, step, publisher)), Some(s"RegistrationCheckOut-$step"))
      addActor(actor)
      actor
    }

    def createQueueInActor(queueName: String): ActorRef = {
      val handler: ActorRef = chain.first.headOption.get
      val queueConfig = ServicesConfiguration(queueName, "notused", "notused", poolSize)
      val actor = actorOf(Props(new RegistrationCheckInActor(coordinator, handler, queueName)), Some(s"RegistrationCheckIn-$queueName"))
      val queue = amqpConnection.queue(queueConfig)
      queue.wireConsumer(actor, false) //no auto ack
      actor
    }

    def addActor(actor: ActorRef): Unit = chain.add(actor)
  }
}

class RegistrationMQProducer(val producer: ActorRef, val producerEndpoint: String, routeKey: String)
  extends PublisherActor
  with ActorLogging
  with VehicleRegistrationFormats {

  override def producerRouteKey(msg: AnyRef): String = routeKey

  override val ApplicationId: String = s"RegistrationMQProducer-$routeKey"

}

/**
 * Actor that coordinates MQ Ack messages related to VehicleRegistrationMessage deliveries.
 * Receives RegistrationCheckIn messages, informing that a registration was consumed (with the indication of the consumer)
 * When we receive a RegistrationCheckOut message matching a previously received RegistrationCheckIn, we send an Ack to the consumer.
 * This way we guarantee there is no loss of registrations along the workflow.
 */
class RegistrationMQCoordinator extends Actor with ActorLogging {

  var active: Map[String, RegistrationCheckIn] = Map.empty

  override def receive: Receive = {
    case msg: RegistrationCheckIn  ⇒ handleCheckIn(msg)
    case msg: RegistrationCheckOut ⇒ handleCheckOut(msg)
  }

  def handleCheckIn(msg: RegistrationCheckIn): Unit = {
    active.get(msg.eventId) foreach { previous ⇒
      log.warning("There is already an active registration {}. Will ACK it", previous)
      ack(previous)
    }
    log.info("Checking in {}", msg)
    active = active.updated(msg.eventId, msg)
  }

  def ack(msg: RegistrationCheckIn): Unit = {
    log.info("Acking {}, tag {}", msg.eventId, msg.deliveryTag)
    msg.ackTo ! Ack(msg.deliveryTag)
    active = active - msg.eventId
  }

  def handleCheckOut(msg: RegistrationCheckOut): Unit = {
    log.info("Checking out {}", msg)
    active.get(msg.eventId) foreach (ack(_))
  }

}

class RegistrationCheckInActor(coordinator: ActorRef, val target: ActorRef, queueName: String)
  extends MQConsumer with VehicleRegistrationFormats {

  object RegistrationExtractor extends MessageExtractor[VehicleRegistrationMessage]

  override def ownReceive: Receive = {
    case delivery @ RegistrationExtractor(msg) ⇒ handleRegistration(msg, delivery.envelope.getDeliveryTag)
  }

  def handleRegistration(msg: VehicleRegistrationMessage, deliveryTag: Long): Unit = {
    val client = sender
    coordinator ! RegistrationCheckIn(msg.id, deliveryTag, client, System.currentTimeMillis(), queueName)
    target ! msg
  }
}

class RegistrationCheckOutActor(coordinator: ActorRef, recipients: Set[ActorRef], step: String, publisher: Option[ActorRef])
  extends RecipientList(recipients) {

  override def receive: Receive = {
    case msg: VehicleRegistrationMessage ⇒ handleRegistration(msg)
  }

  def handleRegistration(msg: VehicleRegistrationMessage): Unit = {
    coordinator ! RegistrationCheckOut(msg.id, System.currentTimeMillis(), step) //notifies checkout
    publisher foreach (_ ! msg) //send to queue (if present)
    sendToRecipients(msg) //sends to recipients (if any)
  }
}

case class RegistrationCheckIn(eventId: String, deliveryTag: Long, ackTo: ActorRef, time: Long, step: String)
case class RegistrationCheckOut(eventId: String, time: Long, step: String)