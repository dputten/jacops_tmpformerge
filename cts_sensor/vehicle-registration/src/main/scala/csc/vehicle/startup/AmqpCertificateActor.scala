/*
 * Copyright (C) 2015. CSC <http://www.csc.com>
 */

package csc.vehicle.startup

import akka.actor.{ ActorRef, ActorLogging, Actor }
import com.github.sstone.amqp.Amqp.{ Error, Request, Ok, Publish }
import csc.amqp.AmqpSerializers
import csc.gantry.config.{ LaneConfig, ActiveCertificate }
import csc.vehicle.config.AmqpCertificateCheckConfig
import csc.vehicle.registration.{ VehicleRegistrationSerialization, VehicleRegistrationCertificate }
import csc.vehicle.startup.AmqpCertificateActor.SendCerts

/**
 * It will send a [[VehicleRegistrationCertificate]] for each gantry (which is theoretically multiple but practically only one)
 * and wait for acknowledgements from the underlying AMQP infrastructure. After all acknowledges arrived the actor will
 * automatically stop.
 *
 * @author csomogyi
 */
class AmqpCertificateActor(producer: ActorRef, config: AmqpCertificateCheckConfig) extends Actor with ActorLogging
  with AmqpSerializers {

  override implicit val formats = VehicleRegistrationSerialization.formats
  override val ApplicationId = "VehicleRegistrationCertificateCheck"

  var messagesSent = 0

  /**
   * Collects the gantries from the lanes and sends the given certificate for each gantry. Note, that practically we
   * always have only a single gantry but programatically is not true. Hence the extraction of gantries from the lanes.
   *
   * @param lanes
   * @param activeCertificate
   */
  def sendCertificates(lanes: List[LaneConfig], activeCertificate: ActiveCertificate): Unit = {
    var gantries: Set[(String, String)] = Set()
    lanes foreach { lane ⇒
      val gantry = (lane.lane.system, lane.lane.gantry)
      gantries += gantry
    }

    log.info("Sending certificate for gantries (system,gantry): " + gantries.mkString(","))

    gantries.headOption foreach {
      case (system, gantry) ⇒
        val message = VehicleRegistrationCertificate(system, gantry, activeCertificate)
        val payload = toPublishPayload[VehicleRegistrationCertificate](message)
        payload match {
          case Right((body, properties)) ⇒ {
            val persistent = properties.builder().deliveryMode(2 /* persistent */ ).build()
            val publish = Publish(config.registrationReceiverEndpoint, config.registrationReceiverRouteKey, body, Some(persistent))
            producer ! publish
            messagesSent += 1
            log.info("Certificate message queued for {}-{}", system, gantry)
          }
          case Left(error) ⇒ log.error("Error when serializing certificate message - {}", error)
        }
    }
  }

  /**
   * Checks if we received as many acknowledgements (Ok or Error) as many certificate messages we sent. If so then
   * the actor stops itself
   */
  def checkForStop(): Unit = {
    messagesSent -= 1
    if (messagesSent == 0) {
      log.info("Received feedback from every sent mesage")
      log.info("Time to stop")
      context.stop(self)
    }
  }

  def receive = {
    case SendCerts(lanes, activeCertificate) ⇒ sendCertificates(lanes, activeCertificate)
    case Ok(request, _) ⇒ {
      request match {
        case publish: Publish ⇒ {
          log.debug("Sent request with properties {}", publish.properties)
          checkForStop()
        }
        case _: Request ⇒ log.debug("Unknown AMQP Ok")
      }
    }
    case Error(request, reason) ⇒ {
      request match {
        case publish: Publish ⇒ {
          log.error(reason, "Failed to send request with properties {}", publish.properties)
          checkForStop()
        }
        case _: Request ⇒ log.error(reason, "Unknown AMQP error")
      }
    }
  }
}

object AmqpCertificateActor {
  case class SendCerts(lanes: List[LaneConfig], activeCertificate: ActiveCertificate)
}
