package csc.vehicle.startup

import akka.actor.{ ActorRef, Props }
import csc.akka.logging.DirectLogging
import csc.base.{ SensorDevice, ActorPaths }
import csc.base.SensorPosition.SensorPosition
import csc.gantry.config.LaneConfig.LaneConfigProvider
import csc.gantry.config._
import csc.timeregistration.registration.ReadTiffImageMetaData
import csc.vehicle.RegistrationFilterActor
import csc.vehicle.config._
import csc.vehicle.message.VehicleImageType.VehicleImageType
import csc.vehicle.message.VehicleRegistrationMessage
import csc.vehicle.registration._
import csc.vehicle.registration.classifier.ClassifyVehicleCaller
import csc.vehicle.registration.events.EventBuilder
import csc.vehicle.registration.recognizer.RecognizeImageCaller

/**
 * Created by carlos on 18.11.15.
 */
object WorkflowBuilder extends DirectLogging {

  type ActorOf = (Props, Option[String]) ⇒ ActorRef
  type ActorBuilder = (ActorOf, Set[ActorRef]) ⇒ ActorRef

  def createRegistrationCompletedBuilder(globalConfig: VehicleRegistrationConfiguration,
                                         laneConfig: Option[LaneConfig],
                                         amqpProducer: ActorRef): ActorBuilder = new ActorBuilder {

    override def apply(actorOf: ActorOf, nextSteps: Set[ActorRef]): ActorRef = {

      val amqpConfig: Option[AmqpCompletionConfig] = globalConfig.registrationCompletion.find { e ⇒
        e.isInstanceOf[AmqpCompletionConfig]
      } map (_.asInstanceOf[AmqpCompletionConfig])

      val lane = laneConfig map (_.lane)

      amqpConfig match {
        case None ⇒
          val msg = "Only AmqpCompletionConfig is supported. Please provide one"
          log.error(msg)
          throw new UnsupportedOperationException(msg)
        case Some(cfg) ⇒
          if (globalConfig.registrationCompletion.size > 1) {
            log.warning("Only AmqpCompletionConfig is supported is supported. All others will be ignored")
          }
          actorOf(Props(new StoreVehicleRegistrationAmqp(amqpProducer, cfg, lane, nextSteps)), None)
      }
    }
  }

}

trait WorkflowBuilder extends DirectLogging {

  import csc.vehicle.startup.WorkflowBuilder._

  def globalConfig: VehicleRegistrationConfiguration
  def systemConfig: SystemConfig

  //  def laneConfig: Option[LaneConfig]
  def laneConfigProvider: LaneConfigProvider

  def actorPaths: ActorPaths
  //def lastStep: Option[ActorRef]
  def actorOfFunction: ActorOf
  def registrationCompletedBuilder: ActorBuilder
  //def doubleTriggerMonitor: Option[ActorRef]

  val laneId: String = laneConfigProvider match {
    case Right(cfg) ⇒ cfg.lane.laneId
    case Left(_)    ⇒ "common" //no fixed lane
  }

  val recogImageCallerName: String = s"RecognizeImageCaller-$laneId"

  def cleanerProps(nextActors: Set[ActorRef]): Props =
    Props(new CleanupVehicleRegistration(nextActors))

  def signerProps(nextActors: Set[ActorRef]): Option[Props] = globalConfig.sign match {
    case Some(signConfig) ⇒ Some(Props(new SignRegistrationFiles(nextActors)))
    case None ⇒
      log.info("Signing files is turned off")
      None
  }

  def customClassifierProps(nextSteps: Set[ActorRef]): Option[Props] = globalConfig.customClassifier match {
    case true ⇒ (actorPaths.classifyDelegate, globalConfig.classifierConfig) match {
      case (Some(path), Some(cfg)) ⇒
        Some(ClassifyVehicleCaller.props(path, cfg, nextSteps))
      case (path, cfg) ⇒
        throw new IllegalArgumentException(s"Some expected configuration is missing: path=$path classifierConfig=$cfg")
    }
    case false ⇒ None
  }

  lazy val noPlateRejector: VehicleRegistrationMessage ⇒ Option[String] = { msg ⇒
    msg.hasLicensePlate match {
      case true  ⇒ None
      case false ⇒ Some("noplate")
    }
  }

  def noPlateFilterProps(nextSteps: Set[ActorRef]): Option[Props] = globalConfig.cleanupConfig match {
    case None ⇒ None
    case Some(cfg) ⇒ cfg.dropWithoutPlate match {
      case false ⇒ None
      case true  ⇒ Some(Props(new RegistrationFilterActor(nextSteps, noPlateRejector)))
    }
  }

  def recognizeImageCallerProps(nextActors: Set[ActorRef], candidateTypes: VehicleImageType*): Props =
    Props(new RecognizeImageCaller(targetActorPath = actorPaths.recognizeDelegate,
      recognizeCfg = globalConfig.recognizeConfig, laneConfigProvider = laneConfigProvider,
      candidateTypes = candidateTypes, recipients = nextActors)) //, Some(recogImageCallerName)

  def triggerImageMaskerProps(nextActors: Set[ActorRef], maskConfig: TriggerMaskConfig): Props =
    Props(new TriggerMaskImage(maskConfig, nextActors))

  def triggerSourceChangerProps(targetSource: String, nextSteps: Set[ActorRef]): Props =
    Props(new ChangeTriggerSource(targetSource, nextSteps))

  def configDataAdderProps(nextSteps: Set[ActorRef]): Props = Props(new AddConfigData(systemConfig, nextSteps))

  def imageMetadataReaderProps(nextSteps: Set[ActorRef], position: SensorPosition): Props = {
    val cameraEventBuilder = EventBuilder.getInstance(position, SensorDevice.Camera)

    Props(new ReadTiffImageMetaData(
      sectionGuard = globalConfig.metaImageSectionGuard,
      eventBuilder = cameraEventBuilder,
      setEventTime = true,
      nextActors = nextSteps,
      testImagePattern = globalConfig.imageConfig.testImagePattern,
      metadataReader = globalConfig.metadataReader))
  }

  def actorOf(props: Props) = actorOfFunction.apply(props, None)

  def uniqueActorOf(props: Props) = actorOfFunction.apply(props, None)

}
