package csc.vehicle.startup

import akka.actor.{ ActorRef, Props }
import base.FavorCriticalProcess
import csc.base.{ ActorPaths, SensorPosition }
import csc.base.SensorPosition.SensorPosition
import csc.gantry.config.LaneConfig.LaneConfigProvider
import csc.gantry.config._
import csc.vehicle.config._
import csc.vehicle.message.{ ImageVersion, Lane, VehicleImageType }
import csc.vehicle.registration._
import csc.vehicle.registration.events.EventBuilder
import image.FilterDoubleTriggerEvents

import scala.concurrent.duration._
import csc.vehicle.startup.WorkflowBuilder._

class SimpleWorkflowBuilder(val actorPaths: ActorPaths,
                            val globalConfig: VehicleRegistrationConfiguration,
                            val systemConfig: SystemConfig,
                            laneConfig: LaneConfig,
                            lastStep: Option[ActorRef],
                            val registrationCompletedBuilder: ActorBuilder,
                            doubleTriggerMonitor: Option[ActorRef],
                            val actorOfFunction: ActorOf) extends WorkflowBuilder {

  import csc.base.SensorDevice._

  //val laneConfig: Option[LaneConfig] = Some(laneCfg)

  override def laneConfigProvider: LaneConfigProvider = Right(laneConfig)

  /**
   * Create the actors for each sensor.
   * returns a list of new created actors
   */
  def build(): ActorRef = {

    val lastSteps: Set[ActorRef] = lastStep match {
      case Some(actor) ⇒ Set(actor)
      case None        ⇒ Set()
    }

    val cleanupRegistration = actorOf(cleanerProps(lastSteps))

    //we will always use nextActors, so its easier to keep track while include optional steps
    var nextActors: Set[ActorRef] = Set(registrationCompletedBuilder(actorOfFunction, Set(cleanupRegistration)))

    signerProps(nextActors) foreach { props ⇒ nextActors = Set(actorOf(props)) }

    createDoubleTriggerFilter(nextActors) foreach { actor ⇒ nextActors = Set(actor) }

    globalConfig.exif match {
      case Some(exifConfig) ⇒
        val addMetadataStart = constructAddMetadataSteps(exifConfig, nextActors)
        nextActors = addMetadataStart

      case None ⇒
        log.info("Adding metadata to images is turned off")
    }

    customClassifierProps(nextActors) foreach { props ⇒ nextActors = Set(actorOf(props)) }

    val jpgGuard = globalConfig.guards.get("jpeg")

    // Create inputImagesConversion chain of actors.
    val (inputImagesConversionStart, inputImagesConversionEndJoin) = constructInputImageConversions(jpgGuard, nextActors)

    inputImagesConversionEndJoin match {
      case Some(joinActor) ⇒ nextActors = Set(joinActor)
      case None            ⇒
    }

    globalConfig.jpegStep.license map { c ⇒
      if (c.conversionEnabled) { //Jacops-POC: now we only convert license if enabled
        val jpegLicense = actorOf(Props(
          new ConvertJPEG(
            convertParm = c,
            sectionGuard = jpgGuard,
            imageVersion = ImageVersion.Original,
            nextActors = nextActors,
            dirJpg = globalConfig.jpegStep.jpgOutputDir)))

        nextActors = Set(jpegLicense)
      }
    }

    //filters out, going directly to cleanup, if configured and has no plate
    noPlateFilterProps(nextActors) foreach { props ⇒ nextActors = Set(actorOf(props)) }

    var joinSensorsNextStepMap = constructJoinSensors(
      laneConfig.sensorList,
      "Joined",
      globalConfig.joinEventConfig.maxBufferLen,
      nextActors,
      laneId,
      globalConfig.joinEventConfig.bufferTimeout)

    nextActors = joinSensorsNextStepMap.get(Camera.toString) getOrElse Set.empty

    val recognizeStart = constructRecognizeStep(nextActors)

    // If an inputImagesConversion chain exist, then images from "Camera" have to go to recognizer AND to this conversion chain.
    nextActors = inputImagesConversionStart match {
      case Some(conversionActor) ⇒ Set(recognizeStart, conversionActor)
      case None                  ⇒ Set(recognizeStart)
    }

    globalConfig.exif match {
      case Some(exifConfig) ⇒ nextActors = constructExifSteps(exifConfig, nextActors)
      case None             ⇒ log.info("Extraction of Exif data is turned off")
    }

    joinSensorsNextStepMap += Camera.toString -> nextActors

    constructLaneSensor(joinSensorsNextStepMap)
  }

  private def constructAddMetadataSteps(exifConfig: ⇒ ExifConfig, nextActors: Set[ActorRef]): Set[ActorRef] = {
    var nextSteps = nextActors

    exifConfig.getTargetImageTypes.foreach { targetImageType ⇒
      val par = nextSteps
      val addMetadata = actorOf(Props(new AddMetadataToImage(
        targetImageType = targetImageType,
        config = exifConfig,
        nextActors = par)))
      nextSteps = Set(addMetadata)
    }
    nextSteps
  }

  private def createDoubleTriggerFilter(nextActors: Set[ActorRef]): Option[ActorRef] =
    doubleTriggerMonitor map { m ⇒ actorOf(FilterDoubleTriggerEvents.props(m, nextActors)) }

  /**
   * Construct a chain of actors for converting the input images.
   * The last actor will be a JoinByEventId actor. The rest will be ConvertJPG actors.
   * The join actor will be used to join the created chain with the "license" chain (= recognize->sensor-joins->license-convert-jpg)
   * @return The first and the last actor of this chain.
   */
  private def constructInputImageConversions(jpgGuard: Option[FavorCriticalProcess], nextActors: Set[ActorRef]): (Option[ActorRef], Option[ActorRef]) = {
    // Note: Currently only the conversion of the Overview image is supported.
    //       However, conversion of other input images can easily be added here.
    globalConfig.jpegStep.overview match {
      case Some(c) if c.conversionEnabled ⇒
        val joinImages = actorOf(Props(
          new JoinByEventId(
            sourcePrio = List("Joined", "Camera"),
            outputSource = "Final",
            maxBufferLen = (globalConfig.joinEventConfig.maxBufferLen * 1.5).toInt, //use a bigger buffer than the joining earlier in the flow
            nextActors = nextActors)))
        val jpegOverview = actorOf(Props(
          new ConvertJPEG(
            convertParm = c,
            sectionGuard = jpgGuard,
            imageVersion = ImageVersion.Original,
            nextActors = Set(joinImages),
            dirJpg = globalConfig.jpegStep.jpgOutputDir)))

        (Some(jpegOverview), Some(joinImages))
      case _ ⇒
        log.info("Conversion of input images is turned off")
        (None, None)
    }
  }

  private def constructJoinSensors(sourceList: List[SensorDevice],
                                   outputSource: String,
                                   maxBufferLen: Int,
                                   nextSteps: Set[ActorRef],
                                   laneId: String,
                                   timeToKeep: Duration): Map[String, Set[ActorRef]] = {
    sourceList.size match {
      case 0 ⇒ Map()
      case 1 ⇒ {
        val source = sourceList.head.toString
        val change = actorOf(triggerSourceChangerProps(outputSource, nextSteps))
        Map(source -> Set(change))
      }
      case _ ⇒ {
        //need joining
        var output: Map[String, Set[ActorRef]] = Map()
        var list = sourceList
        var tmpNextSet = nextSteps
        var firstTime = true
        while (list.size > 1) {
          val source = list.last.toString
          val keepTime = ((list.size - 1) * timeToKeep.toMillis).millis
          list = list.init
          val firstSource = if (list.size > 1) outputSource else list.head.toString
          val prioList: List[String] = List(firstSource, source)
          val par = tmpNextSet
          val joinActor = actorOf(Props(
            new JoinByEventTimes(
              sourcePrio = prioList,
              outputSource = outputSource,
              maxBufferLen = maxBufferLen,
              setTime = firstTime,
              laneId = laneId,
              timeToKeep = keepTime,
              nextActors = par)))
          tmpNextSet = Set(joinActor)
          output += source -> tmpNextSet
          firstTime = false
        }
        output += list.head.toString -> tmpNextSet
        output
      }
    }
  }

  private def constructRecognizeStep(nextActors: Set[ActorRef]): ActorRef = {

    val maskConfiguration = globalConfig.maskConfiguration

    maskConfiguration match {
      case None ⇒
        // Only recognize on Overview
        actorOfFunction(recognizeImageCallerProps(nextActors, VehicleImageType.Overview, VehicleImageType.License), Some(recogImageCallerName))

      case Some(cfg) ⇒ {
        // Recognize on masked Overview
        val recognize =
          actorOfFunction(recognizeImageCallerProps(nextActors, VehicleImageType.OverviewMasked, VehicleImageType.Overview, VehicleImageType.License),
            Some(recogImageCallerName))

        cfg match {
          case maskConfig: MaskConfig ⇒ {
            val mask = laneConfig.imageMask match {
              case Some(imageMask) ⇒ imageMask
              case None ⇒
                log.warning("No image mask defined in config, even though mask is enabled. Use default (= useless) one!") //TODO-RV: Betere oplossing?
                new ImageMaskImpl(List(new ImageMaskVertex(0, 0), new ImageMaskVertex(100, 0), new ImageMaskVertex(100, 100), new ImageMaskVertex(0, 100)))
            }
            actorOf(Props(
              new MaskImage(
                mask = mask,
                cropEnabled = maskConfig.cropEnabled,
                jpgQuality = maskConfig.maskQuality,
                dirMaskedImage = maskConfig.dirMaskImage,
                nextActors = Set(recognize))))
          }
          case maskConfig: TriggerMaskConfig ⇒ actorOf(triggerImageMaskerProps(Set(recognize), maskConfig))
        }
      }
    }
  }

  private def constructExifSteps(exifConfig: ⇒ ExifConfig, nextActors: Set[ActorRef]): Set[ActorRef] = {
    var nextSteps = nextActors

    exifConfig.getTargetImageTypes.foreach { targetImageType ⇒
      val par = nextSteps
      val exifExtractor = actorOf(Props(new ExtractExif(
        imageType = targetImageType,
        config = exifConfig,
        nextActors = par)))
      nextSteps = Set(exifExtractor)
    }
    nextSteps
  }

  private def constructLaneSensor(nextStepMap: Map[String, Set[ActorRef]]): ActorRef = {
    laneConfig match {
      case camConfig: CameraOnlyConfig                      ⇒ createCameraOnly(camConfig, nextStepMap)
      case pirConfig: PirRadarCameraSensorConfig            ⇒ createPirRadarCamera(pirConfig, nextStepMap)
      case pirConfig: PirCameraSensorConfig                 ⇒ createPirCamera(pirConfig, nextStepMap)
      case radarCameraConfig: DoubleRadarCameraSensorConfig ⇒ createDoubleRadarCamera(radarCameraConfig, nextStepMap)
      case radarCameraConfig: RadarCameraSensorConfig       ⇒ createRadarCamera(radarCameraConfig, nextStepMap)
      //honac case is not matching at all, as HonacConfig is not a LaneConfig
      //case honacConfig: HonacConfig                         ⇒ createHonac(honacConfig, nextStepMap)
      case iRoseConfig: IRoseSensorConfig                   ⇒ createIRose(nextStepMap)
    }
  }

  //Honac is not really working (see above)
  //  private def createHonac(honacConfig: HonacConfig, nextStepMap: Map[String, Set[ActorRef]]): ActorRef = {
  //    // TODO: RV - Waarom HonacConfig ipv HonacSensorConfig ?
  //    val nextStep = nextStepMap.get("Camara").getOrElse(Set[ActorRef]()) //TODO: RV - Waarom Camare ipv Camera ?
  //    val honac = actorOf(Props(
  //        new JoinHonacData(
  //          maxDelayInMillisec = honacConfig.maxDelayInMillisec,
  //          recognizers = nextStep)))
  //    honac
  //  }

  private def createCameraOnly(config: CameraOnlyConfig, nextStepMap: Map[String, Set[ActorRef]]): ActorRef = {
    log.info("Creating Camera only Lane sensor %s".format(laneId))
    createCamera(SensorPosition.Trigger, nextStepMap)
  }

  private def createPirRadarCamera(pirConfig: PirRadarCameraSensorConfig, nextStepMap: Map[String, Set[ActorRef]]): ActorRef = {
    log.info("Creating PirRadarCamera Lane sensor %s".format(laneId))
    val metadata = createCamera(SensorPosition.LastShifted, nextStepMap)
    createRadar(Radar, SensorPosition.Last, pirConfig.lane, pirConfig.radar, nextStepMap)
    createPir(pirConfig.lane, pirConfig.pir, nextStepMap)
    metadata
  }

  private def createPirCamera(pirConfig: PirCameraSensorConfig, nextStepMap: Map[String, Set[ActorRef]]): ActorRef = {
    log.info("Creating PirCamera Lane sensor %s".format(laneId))
    val metadata = createCamera(SensorPosition.LastShifted, nextStepMap)
    createPir(pirConfig.lane, pirConfig.pir, nextStepMap)
    metadata
  }

  private def createDoubleRadarCamera(radarCameraConfig: DoubleRadarCameraSensorConfig, nextStepMap: Map[String, Set[ActorRef]]): ActorRef = {
    val metadata = createCamera(SensorPosition.Last, nextStepMap)
    createRadar(Radar, SensorPosition.Last, radarCameraConfig.lane, radarCameraConfig.radar, nextStepMap)
    createRadar(RadarLength, SensorPosition.Trigger, radarCameraConfig.lane, radarCameraConfig.radarLength, nextStepMap)
    metadata
  }

  private def createRadarCamera(radarCameraConfig: RadarCameraSensorConfig, nextStepMap: Map[String, Set[ActorRef]]): ActorRef = {
    val metadata = createCamera(SensorPosition.Trigger, nextStepMap)
    createRadar(Radar, SensorPosition.Last, radarCameraConfig.lane, radarCameraConfig.radar, nextStepMap)
    metadata
  }

  private def createIRose(nextStepMap: Map[String, Set[ActorRef]]): ActorRef = {
    val nextStep = nextStepMap.get(Camera.toString).getOrElse(Set[ActorRef]())
    actorOf(Props(new CollectIRoseData(nextActors = nextStep)))
  }

  private def createCamera(position: SensorPosition, nextStepMap: Map[String, Set[ActorRef]]): ActorRef = {
    val metadataNextStep = nextStepMap.get(Camera.toString).getOrElse(Set[ActorRef]())
    val addconfig = actorOf(configDataAdderProps(metadataNextStep))
    actorOf(imageMetadataReaderProps(Set(addconfig), position))
  }

  lazy val typeCertificates: Seq[MeasurementMethodInstallationType] = {
    if (globalConfig.certConfig.isCertificationEnabled) {
      systemConfig.typeCertificates
    } else Nil
  }

  private def createRadar(device: SensorDevice, position: SensorPosition, lane: Lane, config: RadarConfig, nextStepMap: Map[String, Set[ActorRef]]): ActorRef = {
    val radarNextStep = nextStepMap.get(device.toString).getOrElse(Set[ActorRef]())
    val radarEventBuilder = EventBuilder.getInstance(position, device)
    actorOf(Props(
      new ReceiveRadar(
        lane = lane,
        NMICertificate = typeCertificates,
        config = config,
        eventBuilder = radarEventBuilder,
        setEventTime = false,
        nextActors = radarNextStep)))
  }

  private def createPir(lane: Lane, pir: PIRConfig, nextStepMap: Map[String, Set[ActorRef]]): ActorRef = {
    val pirNextStep = nextStepMap.get(PIR.toString).getOrElse(Set[ActorRef]())
    val pirEventBuilder = EventBuilder.getInstance(SensorPosition.Trigger, PIR)
    actorOf(Props(
      new ReceivePir(
        lane = lane,
        NMICertificate = typeCertificates,
        pirCfg = pir,
        eventBuilder = pirEventBuilder,
        setEventTime = false,
        nextActors = pirNextStep)))
  }

}
