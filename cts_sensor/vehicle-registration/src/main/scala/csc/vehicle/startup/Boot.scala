/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.startup

import csc.akka.logging.DirectLogging
import akka.kernel.Bootable
import csc.vehicle.config.Configuration

/**
 * Boot class Starts VehicleRegistration
 */
private[startup] class Boot extends Bootable with DirectLogging {
  var startupOpt: Option[StartupVehicleRegistration] = None

  def startup = {
    log.info("Starting VehicleRegistration")

    //Runtime.getRuntime().addShutdownHook(new ShutdownHook);
    val config = Configuration.create() //find configuration and process the file

    //start JMX
    config.jmxService match {
      case Some(jmxConfig) ⇒ {
        JMXSupport.startJMX(
          host = jmxConfig.host,
          passwordFile = jmxConfig.jmxPasswordFile,
          accessFile = jmxConfig.jmxAccessFile,
          rmiPort = jmxConfig.rmiPort,
          JMXPort = jmxConfig.jmxPort)
      }
      case None ⇒ log.debug("JMX isn't turned off")
    }
    val start = new StartupVehicleRegistration(config, config.laneConfiguration)
    start.start()
    startupOpt = Some(start)
    log.info("VehicleRegistration is started")
  }

  def shutdown = {
    startupOpt.foreach(_.stop())
    startupOpt = None
  }
}
