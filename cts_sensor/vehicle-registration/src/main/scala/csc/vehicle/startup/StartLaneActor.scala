/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.startup

import akka.actor._
import csc.akka.logging.DirectLogging
import csc.base.ActorPaths
import csc.gantry.config.{ CorridorsConfig, SystemConfig, _ }
import csc.vehicle.config._
import csc.vehicle.startup.WorkflowBuilder.ActorOf

class StartLaneActor(workflowBuilder: SimpleWorkflowBuilder)
  extends Actor with ActorLogging {

  var firstStep: Option[ActorRef] = None

  override def preStart() {
    startLane()
    log.info("Started Lane %s".format(laneId))
  }

  override def postStop() {
    log.info("Stopped Lane %s".format(laneId))
  }

  def laneId = workflowBuilder.laneId

  def receive = {
    case msg ⇒ firstStep.foreach(_ ! msg)
  }

  private def startLane() {
    val actor = workflowBuilder.build()
    firstStep = Some(actor)
  }

}

object StartLaneActor extends DirectLogging {

  def createActorOfFunction(system: ActorRefFactory): ActorOf = { (p, n) ⇒
    n match {
      case None       ⇒ system.actorOf(p)
      case Some(name) ⇒ system.actorOf(p, name)
    }
  }

  def props(actorPaths: ActorPaths,
            system: ActorRefFactory,
            amqpProducer: ActorRef,
            globalConfig: VehicleRegistrationConfiguration,
            systemConfig: SystemConfig,
            corridorsConfig: ⇒ Option[CorridorsConfig],
            laneConfig: LaneConfig,
            doubleTriggerMonitor: Option[ActorRef] = None,
            lastStep: Option[ActorRef] = None): Props = {

    val registrationCompletedBuilder =
      WorkflowBuilder.createRegistrationCompletedBuilder(globalConfig, Some(laneConfig), amqpProducer)

    val actorOf: ActorOf = createActorOfFunction(system)

    val workflowBuilder = new SimpleWorkflowBuilder(
      actorPaths,
      globalConfig,
      systemConfig,
      laneConfig,
      lastStep,
      registrationCompletedBuilder,
      doubleTriggerMonitor,
      actorOf)

    Props(new StartLaneActor(workflowBuilder))
  }

}
