package csc.vehicle

import csc.vehicle.message.VehicleRegistrationMessage

import scala.collection.mutable.LinkedHashMap

/**
 * Created by carlos on 16/01/16.
 */
trait ConfirmedRegistrationBuffer {

  def timeInterval: Int
  def maxBufferSize: Int

  private var buffer = new LinkedHashMap[String, Long]()

  /**
   * Returns true if this registration was considered a duplicate, false otherwise.
   * If false, the message is added to this buffer.
   * Only complete licenses are checked.
   * @param msg the new message
   */
  def shouldBlockRegistration(msg: VehicleRegistrationMessage): Boolean = {
    //keep track only the complete licenses
    if (msg.licenseData.license != None && msg.licenseData.license.get.value.length > 0 && msg.licenseData.license.get.value.indexOf("?") < 0) {
      val license = msg.licenseData.license.get.value

      val (block: Boolean, newTime: Long) = buffer.get(license) match {
        case Some(lastTime) ⇒
          val delta = math.abs(msg.time - lastTime)
          val shouldBlock = delta <= timeInterval
          (shouldBlock, math.max(msg.time, lastTime))
        case _ ⇒ (false, msg.time)
      }

      if (!block) {
        //normal case: update buffer
        buffer.update(license, newTime)
        //keep track of only the latest messages
        if (buffer.size > maxBufferSize) {
          buffer = buffer.drop(buffer.size - maxBufferSize)
        }
      }
      block
    } else false
  }

}
