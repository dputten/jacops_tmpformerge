package image
/*
 * Copyright © CSC
 * Date: Sep 1, 2010
 * Time: 9:49:27 AM
 */

import base.{ RoundtripMessage, RecipientList, Timing }
import csc.vehicle.RegistrationReady
import akka.actor.{ Props, ActorLogging, ActorRef }
import csc.vehicle.message.VehicleRegistrationMessage

/**
 * Delegates the registrations to an actor that confirms or filters out registrations if considered duplicate.
 */
class FilterDoubleTriggerEvents(doubleTriggerManager: ActorRef, nextActors: Set[ActorRef]) extends RecipientList(nextActors) with ActorLogging {

  def receive = {
    case msg: RoundtripMessage ⇒ handleRoundtrip("FilterDoubleTriggerEvents", msg)
    case msg: VehicleRegistrationMessage ⇒ {
      doubleTriggerManager ! RegistrationReady(msg, nextActors)
    }
  }
}

object FilterDoubleTriggerEvents {
  def props(doubleTriggerManager: ActorRef, nextActors: Set[ActorRef]): Props =
    Props(new FilterDoubleTriggerEvents(doubleTriggerManager, nextActors))
}