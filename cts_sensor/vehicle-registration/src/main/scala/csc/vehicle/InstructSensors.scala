package image
//copyright CSC 2010

import _root_.base._
import csc.vehicle.message.SensorActivationMessage
import akka.actor.{ ActorRef, ActorLogging, Actor }

/**
 * This class keeps track of the sensors instruction messages for each sensor.
 */
class InstructSensors(versionId: Long, relayActor: ActorRef) extends Actor with Timing with ActorLogging {
  var lastVersionId = versionId

  def receive = {
    /**
     * receive a new instruction and call updateSensor for all sensors sensors
     */
    case msg: SensorActivationMessage ⇒ {
      performanceLogInterval(log, 36, 37, "Receive instruction", versionId.toString, {
        if (msg.versionId > lastVersionId) {
          //new sensor state received
          relayActor ! msg.activeSensors
        } else {
          log.info("Received an old SensorActivationMessage with version=%d, current version=%d".format(msg.versionId, lastVersionId))
        }
      })
    }
  }
}
