package admin

import javax.management.ObjectName
import collection.mutable.HashMap
import java.util.Date
import mxbeans.LocationMXBean
import csc.akka.logging.DirectLogging
import csc.vehicle.message.{ MonitorStatusMessage, DigitalSwitchDetectedMessage }

/**
 * This class manages the creation and keeping track of the location beans.
 * This class is expected to be used from an Actor and is therefor not thread-safe
 * @param registry: the registry which is used to register the beans
 */
class MonitorLocations(registry: BeanRegistry) extends DirectLogging {
  /**
   * map of aal the known location beans. Key is the hostname:portnumber
   */
  val locationBeans = new HashMap[String, LocationBean]()
  /**
   * Unregister all the beans from the registry.
   */
  def unregisterLocations() {
    for (bean ← locationBeans.values) {
      registry.unregisterBean(bean.objectName)
    }
  }

  /**
   * Process an OpenDoorDetectedMessage, by finding the correct location bean and update the state.
   * @param msg: The received OpenDoorDetectedMessage
   */
  def processMessage(msg: DigitalSwitchDetectedMessage) {
    val bean = getLocationBean(msg.imageServerHost, msg.imageServerPort)
    bean.setNewDoorState(msg.dateFromDetector, msg.doorIsOpen)
  }

  /**
   * Process an MonitorStatusMessage, by finding the correct location bean and update the state.
   * @param msg: The received MonitorStatusMessage
   */
  def processMessage(msg: MonitorStatusMessage) {
    val bean = getLocationBean(msg.hostName, msg.hostPort)
    bean.setUpdateLocation(msg.updateTime, msg.version, msg.startTime, new Date)
  }

  /**
   * Get the location bean, for the specified location.
   * When the bean doesn't exists a new bean is created and registered in the registry.
   * @param host: The host name
   * @param port: The port number
   * @return LocationBean the found or created location bean
   */
  def getLocationBean(host: String, port: Int): LocationBean = {
    val key = createKey(host, port)
    locationBeans.get(key).getOrElse({
      //create new location
      val bean = new LocationBean(key)
      locationBeans += key -> bean
      registry.registerBean(bean.objectName, bean)
      log.info("new bean for new received location => " + key)
      bean
    })
  }

  /**
   * Create the key using host and port number
   * @param host: The host name
   * @param port: The port number
   * @return The String used as a key for the bean Map
   */
  def createKey(host: String, port: Int): String = {
    host + ":" + port
  }
}

/**
 * The implementation of the JMX bean. This bean will be registered and the Getters
 * defined in LocationMXBean will be shown in the JMX interface
 */
class LocationBean(receiveAddress: String) extends LocationMXBean {
  /**
   * The registry name of the bean.
   * The ":" is replaced by a "-" because ":" isn't a legal character in the Object name.
   */
  val objectName = new ObjectName("registration:type=registration.location,name=%s".format(receiveAddress.replace(":", "-")))

  private var doorModifiedDate: String = ""
  private var doorState: Boolean = false
  private var applicationVersion = "Unknown"
  private var applicationStartTime = ""
  private var lastUpdateTime = ""
  private var sendDuration = -1
  private var startTimeSeq = 0
  private var updateTimeSeq = 0

  /**
   * Update the DoorState change.
   */
  def setNewDoorState(date: String, state: Boolean) {
    doorModifiedDate = date
    doorState = state
  }

  /**
   * Update the location monitor attributes
   */
  def setUpdateLocation(updateTime: Date, version: String, startTime: Date, now: Date) {
    var tmp = BeanAttributeFormat.formatTime(updateTime)
    if (lastUpdateTime != tmp) {
      updateTimeSeq += 1
    }
    lastUpdateTime = tmp
    applicationVersion = version
    tmp = BeanAttributeFormat.formatTime(startTime)
    if (applicationStartTime != tmp) {
      startTimeSeq += 1
    }
    applicationStartTime = tmp
    sendDuration = (now.getTime - updateTime.getTime).toInt
  }

  /**
   * Get the DoorOpen State
   * @return boolean: the current state of the door
   */
  def isDoorOpen() = doorState

  /**
   * Get the Date as a string of the last modification of the DoorState
   * @return string representing the date in the format yyyy/MM/dd HH:mm:ss.SSS
   */
  def getDoorModifiedDate: String = {
    doorModifiedDate
  }

  /**
   * Get the Door status.
   * @return String "open" when the door state is open
   *              "close" when the door state is closed
   */
  def getDoorStatus: String = {
    if (doorState) "open" else "closed"
  }

  /**
   * Get the application version
   */
  def getApplicationVersion = {
    applicationVersion
  }

  /**
   * Get the location.
   */
  def getLocation: String = {
    receiveAddress
  }

  /**
   * Get the application start time in the format yyyy/MM/dd HH:mm:ss.SSS
   */
  def getApplicationStartTime = applicationStartTime

  /**
   * Get the last time a update is received in the format yyyy/MM/dd HH:mm:ss.SSS
   */
  def getLastUpdateTime = lastUpdateTime

  /**
   * Get the time between sending and receiving the message
   */
  def getSendDuration = sendDuration

  /**
   * Get the sequence number of modifications of the start time
   */
  def getApplicationStartTimeSeq = startTimeSeq

  /**
   * Get the sequence number of received updates
   */
  def getLastUpdateTimeSeq = updateTimeSeq
}