/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package admin

import javax.management.ObjectName
import management.ManagementFactory
import java.text.SimpleDateFormat
import java.util.Date

/**
 * Object containing support for formatting bean attributes
 */
object BeanAttributeFormat {
  val timeFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS")
  /**
   * Format a Date object to a string
   * @param time: the time to be formatted
   * @return string: the formatted time
   */
  def formatTime(time: Date): String = {
    timeFormat format time
  }
}
trait BeanRegistry {
  /**
   * Used to register a jmx bean
   */
  def registerBean(name: ObjectName, bean: AnyRef)

  /**
   * Used to unregister a jmx bean
   */
  def unregisterBean(name: ObjectName)

}

/**
 * The JMX information is managed by the
 * component who is responsible for the data shown.
 * That same component also registers the bean
 * by using the MBEanRegistrator. This way we
 * can manage the bean name that is used to
 * be consistent. This is needed because the name
 * determines how the information is shown in
 * the JConsole.
 */
class MXBeanRegistry extends BeanRegistry {
  private val beanServer = ManagementFactory.getPlatformMBeanServer;

  /**
   * Used to register jmx bean
   */
  def registerBean(name: ObjectName, bean: AnyRef) {
    if (!beanServer.isRegistered(name)) {
      beanServer.registerMBean(bean, name)
    }
  }

  /**
   * Used to unregister  jmx bean
   */
  def unregisterBean(name: ObjectName) {
    if (beanServer.isRegistered(name)) {
      beanServer.unregisterMBean(name)
    }
  }

}