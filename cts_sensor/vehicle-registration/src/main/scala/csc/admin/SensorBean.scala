/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package admin

import javax.management.ObjectName
import java.util.Date
import collection.mutable.HashMap
import mxbeans.SensorMXBean
import csc.vehicle.message.{ Lane, VehicleRegistrationMessage, MonitorSensorMessage }
import csc.akka.logging.DirectLogging

/**
 * This class manages the creation and keeping track of the sensor beans.
 * This class is expected to be used from an Actor and is therefor not thread-safe
 * @param registry: the registry which is used to register the beans
 * @param totalVehicleTimeWarn: the Time in msec when the process time log message become a warning
 * @param totalJpegTimeWarn: the Time in msec when the process time log message become a warning
 *
 */
class MonitorSensors(registry: BeanRegistry, totalVehicleTimeWarn: Int, totalJpegTimeWarn: Int) extends DirectLogging {
  /**
   * Map of managed sensor beans. Key is the sensorID
   */
  val sensorMap = new HashMap[String, SensorBean]()

  /**
   * Unregister all sensor beans
   */
  def unregisterSensors() {
    for (bean ← sensorMap.values) {
      registry.unregisterBean(bean.objectName)
    }
    sensorMap.clear()
  }

  /**
   * Process a IPMonitorMessage send by the ProcessAdmin actor on the Image Processors.
   * Find the correct bean for the specified sensor and update the state
   * @param update: time received the message
   * @param ipMonitor: the list of sensors with monitor information
   */
  def processIPMonitorMessage(update: Date, ipMonitor: List[MonitorSensorMessage]) {
    for (msg ← ipMonitor) {
      val bean = getSensorBean(msg.sensor)
      bean.updateMonitorIP(update, msg.lastCameraMsg, msg.lastRadarMsg, msg.radarMsg)
    }
  }

  /**
   * process a VehicleMessage send by DeliveryMesssage
   * @param update: time received the message
   * @param msg: The vehicleMessage
   */
  def processVehicleMessage(update: Long, msg: VehicleRegistrationMessage) {
    val bean = getSensorBean(msg.lane)
    bean.updateProcessedVehicle(new Date(update), msg)
  }

  /**
   * Get the sensor bean, for the specified location.
   * When the bean doesn't exists a new bean is created and registered in the registry.
   * @param lane: The sensor
   * @return SensorBean the found or created sensor bean
   */
  def getSensorBean(lane: Lane): SensorBean = {
    val key = lane.laneId
    sensorMap.get(key).getOrElse({
      //create new sensor
      val bean = new SensorBean(lane = lane, totalVehicleTimeWarn = totalVehicleTimeWarn, totalJpegTimeWarn = totalVehicleTimeWarn)
      sensorMap += key -> bean
      registry.registerBean(bean.objectName, bean)
      log.info("new bean for new received sensor => " + key)
      bean
    })
  }
}

/**
 * The implementation of the SensorMXBean. Keeping track of sensor monitor information.
 * @param lane: The sensor where the information is related too
 * @param totalVehicleTimeWarn: the Time in msec when the process time log message become a warning
 * @param totalJpegTimeWarn: the Time in msec when the process time log message become a warning
 */
class SensorBean(lane: Lane, totalVehicleTimeWarn: Int, totalJpegTimeWarn: Int) extends SensorMXBean with DirectLogging {
  private val sensorName = lane.laneId.toString + lane.name
  val objectName = new ObjectName("registration:type=registration.sensor,name=%s".format(sensorName))

  private var lastMonitorImage = ""
  private var lastMonitorRadar = ""
  private var lastMonitorRadarMsg = ""
  private var modifiedMonitorTime = ""
  private var recvEventTime = ""
  private var jpgRecvTime = ""
  private var jpgNr = 0L
  private var jpgNrLate = 0L
  private var jpgSum = 0L
  private var jpgSumLast = 0L
  private var jpgTotalLast = 0L
  private var jpgLast = 0L

  private var xmlRecvTime = ""
  private var xmlNr = 0L
  private var xmlNrLate = 0L
  private var xmlSum = 0L
  private var xmlSumLast = 0L
  private var xmlTotalLast = 0L
  private var xmlLast = 0L

  private var recognizeNr = 0L
  private var recognizeNrFailed = 0L
  private var recognizeSum = 0L
  private var recognizeLast = 0

  private var monitorModificationTimeSeq = 0L
  private var monitorImageSeq = 0L
  private var monitorRadarSeq = 0L

  /**
   * JMX operaator to reset aal the counters back to zero
   */
  def resetCounters() {
    jpgNr = 0L
    jpgNrLate = 0L
    jpgSum = 0L
    jpgSumLast = 0L
    jpgTotalLast = 0L
    jpgLast = 0L

    xmlNr = 0L
    xmlNrLate = 0L
    xmlSum = 0L
    xmlSumLast = 0L
    xmlTotalLast = 0L
    xmlLast = 0L

    recognizeNr = 0L
    recognizeNrFailed = 0L
    recognizeSum = 0L
    recognizeLast = 0

    monitorModificationTimeSeq = 0L
    monitorImageSeq = 0L
    monitorRadarSeq = 0L
  }

  /**
   * An Image processor monitor message is received
   * @param update: the time the monitor data is modified
   * @param camera: time of the last received data from the camera
   * @param radar: time of the last received data from the radar
   * @param radarMsg: Radar Information message When the last radar message was an information message
   */
  def updateMonitorIP(update: Date, camera: Option[Date], radar: Option[Date], radarMsg: Option[String]) {
    var tmp = BeanAttributeFormat.formatTime(update)
    if (modifiedMonitorTime != tmp) {
      monitorModificationTimeSeq += 1
    }
    modifiedMonitorTime = tmp

    tmp = camera.map(time ⇒ BeanAttributeFormat.formatTime(time)).getOrElse(lastMonitorImage)
    if (lastMonitorImage != tmp) {
      monitorImageSeq += 1
    }
    lastMonitorImage = tmp
    tmp = radar.map(time ⇒ BeanAttributeFormat.formatTime(time)).getOrElse(lastMonitorRadar)
    if (lastMonitorRadar != tmp) {
      monitorRadarSeq += 1
    }
    lastMonitorRadar = radar.map(time ⇒ BeanAttributeFormat.formatTime(time)).getOrElse(lastMonitorRadar)
    lastMonitorRadarMsg = radarMsg.getOrElse("")
  }

  /**
   * A vehicle message is received
   * @param recvTime The time when this message is received
   * @param vehicle: The received vehicle message
   */
  def updateProcessedVehicle(recvTime: Date, vehicle: VehicleRegistrationMessage) {
    xmlRecvTime = BeanAttributeFormat.formatTime(recvTime)
    //performance
    xmlLast = recvTime.getTime - vehicle.eventTimestamp.getOrElse(System.currentTimeMillis())
    xmlSum += xmlLast
    val logmsg = String.format("Perf Endpoint eventId=[%s] priority=[%s] sensor[%s] Vehicle total time=[%s]", vehicle.eventId, vehicle.priority, vehicle.lane.laneId.toString, xmlLast.toString)
    if (xmlLast > totalVehicleTimeWarn) {
      xmlNrLate += 1
      log.warning(logmsg)
    } else {
      xmlNr += 1
      log.info(logmsg)
    }
    //event time
    recvEventTime = BeanAttributeFormat.formatTime(new Date(vehicle.eventTimestamp.get))

    //recognize
    recognizeLast = vehicle.licenseData.license.get.confidence
    if (recognizeLast > 0) {
      recognizeNr += 1
    } else {
      recognizeNrFailed += 1
    }
    recognizeSum += recognizeLast

  }

  /**
   * Get the sensor name
   */
  def getSensor = sensorName

  /**
   * Get the last received event Timestamp
   */
  def getLastReceivedEventTime = recvEventTime
  /**
   * Get the time when the last JPG message is received
   */
  def getLastReceivedJPG = jpgRecvTime
  /**
   * Get the time when the last XML message is received
   */
  def getLastReceivedXML = xmlRecvTime

  /**
   * Get the average of the confidence of all vehicle messages
   */
  def getRecognizedConfidenceAvg(): Int = {
    val totalNr = recognizeNr + recognizeNrFailed
    if (totalNr == 0) {
      return -1
    } else {
      return (recognizeSum / totalNr).toInt
    }
  }
  /**
   * Get the total sum of the confidence of all vehicle messages
   * Used for average
   */
  def getRecognizedConfidenceSum = recognizeSum
  /**
   * Get the received confidence of the last vehicle messages
   * Used for average
   */
  def getRecognizedConfidenceLast = recognizeLast

  /**
   * Get the number of failed recognized vehicles
   */
  def getRecognizedNrFailed = recognizeNrFailed
  /**
   * Get the number of recognized vehicles
   */
  def getRecognizedNr = recognizeNr

  /**
   * Get the Average processing time of the PJG
   */
  def getProcessTimeJPGAvg(): Long = {
    val totalNr = jpgNr + jpgNrLate
    if (totalNr == 0) {
      return -1
    } else {
      return (jpgSum / totalNr)
    }
  }
  /**
   * Get the Average processing time of the PJG from last request
   */
  def getProcessTimeJPGPeriodAvg(): Long = {
    val totalNr = jpgNr + jpgNrLate - jpgTotalLast
    if (totalNr <= 0) {
      return -1
    } else {
      val periodSum = jpgSum - jpgLast
      jpgTotalLast = jpgNr + jpgNrLate
      jpgLast = jpgSum
      return (periodSum / totalNr)
    }
  }
  /**
   * Get the Sum of processing time of all the received the JPG
   */
  def getProcessTimeJPGSum = jpgSum
  /**
   * Get the processing time of last received the JPG
   */
  def getProcessTimeJPGLast = jpgLast

  /**
   * Get the number of received JPG which where to late
   */
  def getProcessTimeJPGNrLate = jpgNrLate
  /**
   * Get the number of received JPG which where processed in time
   */
  def getProcessTimeJPGNr = jpgNr

  /**
   * Get the Average processing time of the XML
   */
  def getProcessTimeXMLAvg(): Long = {
    val totalNr = xmlNr + xmlNrLate
    if (totalNr == 0) {
      return -1
    } else {
      return (xmlSum / totalNr)
    }
  }
  /**
   * Get the Average processing time of the XML from last request
   */
  def getProcessTimeXMLPeriodAvg(): Long = {
    val totalNr = xmlNr + xmlNrLate - xmlTotalLast
    if (totalNr <= 0) {
      return -1
    } else {
      val periodSum = xmlSum - xmlLast
      xmlTotalLast = xmlNr + xmlNrLate
      xmlLast = xmlSum
      return (periodSum / totalNr)
    }
  }
  /**
   * Get the Sum of processing time of all the received the XML
   */
  def getProcessTimeXMLSum = xmlSum
  /**
   * Get the processing time of last received the XML
   */
  def getProcessTimeXMLLast = xmlLast
  /**
   * Get the number of received XML which where to late
   */
  def getProcessTimeXMLNrLate = xmlNrLate
  /**
   * Get the number of received XML which where processed in time
   */
  def getProcessTimeXMLNr = xmlNr

  /**
   * Get the time of the last time the monitor attributes are modified
   */
  def getModifiedLastMonitorTime = modifiedMonitorTime

  /**
   * get the time of the last received data from the radar
   */
  def getLastMonitorRadar = lastMonitorRadar

  /**
   * get the Radar Information message When the last radar message was an information message
   */
  def getLastMonitorRadarMsg = lastMonitorRadarMsg

  /**
   * get the time of the last received data from the camera
   */
  def getLastMonitorImage = lastMonitorImage

  /**
   * The the sequence number of the modified monitor times
   */
  def getModifiedLastMonitorTimeSeq = monitorModificationTimeSeq

  /**
   * The the sequence number of the modified monitor Image times
   */
  def getLastMonitorImageSeq = monitorImageSeq

  /**
   * The the sequence number of the modified monitor radar times
   */
  def getLastMonitorRadarSeq = monitorRadarSeq

  /**
   *  get the percentage of recognized vehicles
   */
  def getRecognizedNrPerc: Float = {
    val totalNr = recognizeNr + recognizeNrFailed
    if (totalNr == 0) {
      return -1
    } else {
      return (recognizeNr.toFloat / totalNr.toFloat) * 100
    }
  }

  /**
   * Get the percentage of in time processed JPG messages
   */
  def getProcessTimeJPGNrPerc: Float = {
    val totalNr = jpgNr + jpgNrLate
    if (totalNr == 0) {
      return -1
    } else {
      return (jpgNr.toFloat / totalNr.toFloat) * 100
    }
  }

  /**
   * Get the percentage of in time processed XML messages
   */
  def getProcessTimeXMLNrPerc: Float = {
    val totalNr = xmlNr + xmlNrLate
    if (totalNr == 0) {
      return -1
    } else {
      return (xmlNr.toFloat / totalNr.toFloat) * 100
    }
  }
}
