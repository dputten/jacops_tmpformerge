package base

//copyright CSC 2010

/**
 * Enumeration of the different Plan Types
 */
object PlanType extends Enumeration {
  type PlanType = Value
  val TC = Value(1)
  val QA = Value(2)
  val NTC = Value(3)
}

/**
 * Enumeration of the different Command Types
 */
object CommandType extends Enumeration {
  type CommandType = Value
  val Activate = Value(1)
  val Deactivate = Value(2)
}

/**
 * Enumeration of the different SensorDirections
 */
object SensorDirection extends Enumeration {
  type SensorDirection = Value
  val Unknown = Value(1)
  val Inbound = Value(2)
  val Outbound = Value(3)
}

/**
 * Different modes
 */
object Mode extends Enumeration {
  val Normal, Test = Value

  def forName(name: String): Option[Mode.Value] = values.find(name == _.toString)
}

/**
 * This class represents on which Sensor the vehicle was detected
 */
case class Sensor(sensorId: Long,
                  sensorName: String = "",
                  sensorDirection: SensorDirection.Value = SensorDirection.Unknown,
                  sensorGPS_longitude: Float = -1,
                  sensorGPS_latitude: Float = -1,
                  remoteHostName: String = "",
                  remotePort: Int = -1) {
}

/**
 * TODO 11082010 RB: NOTE sequence will not be needed anymore when idempotent receiver is implemented.
 * This class represent a Plan received from the IEGI
 */
case class Plan(planId: Long, planType: PlanType.Value, cmdType: CommandType.Value, pictureRequired: Boolean, sensors: Set[Sensor], sequence: Long = -1)
