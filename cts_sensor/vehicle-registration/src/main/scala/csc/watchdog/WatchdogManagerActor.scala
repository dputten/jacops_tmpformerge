/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.watchdog

import akka.actor.{ Actor, ActorLogging, ActorRef, ActorSystem, Props }
import java.text.SimpleDateFormat
import java.util.Date

import csc.akka.process.{ ExecuteScript, ExecuteScriptActor, ExecuteScriptResult }
import csc.amqp.adapter.{ HeartbeatMessage, MQConsumer }
import csc.amqp.{ AmqpConnection, ServicesConfiguration }
import net.liftweb.json.{ DefaultFormats, Formats }

import scala.concurrent.duration.FiniteDuration

class WatchdogManagerActor(config: WatchdogConfig, scriptExecutor: ActorRef)
  extends Actor with ActorLogging {

  implicit val executor = context.system.dispatcher

  var activeTrackers: Map[String, ActorRef] = Map.empty

  override def receive: Receive = {
    case ActivateServiceWatch(name) ⇒ trackService(name)
    case msg: HeartbeatMessage      ⇒ handleHeartbeat(msg)
  }

  def handleHeartbeat(msg: HeartbeatMessage): Unit = activeTrackers.get(msg.component) match {
    case Some(actor) ⇒ actor forward msg //forwarding heartbeat to the correct tracker
    case None ⇒ config.configOf(msg.component) match {
      case None ⇒ log.warning("Unknown service for {}. Ignored", msg)
      case _    ⇒ log.debug("Received {}, but service {} is not being tracked. Ignored", msg, msg.component)
    }
  }

  def trackService(name: String): Unit = activeTrackers.get(name) match {
    case None ⇒ config.configOf(name) match {
      case None ⇒ log.warning("No configuration found for service {}. Ignoring", name)
      case Some(cfg) ⇒
        val tracker = context.actorOf(Props(new ServiceTrackerActor(cfg, scriptExecutor)), s"ServiceTracker_${name}")
        activeTrackers = activeTrackers.updated(name, tracker)
    }
    case Some(_) ⇒ log.info("Service {} is already being tracked. Ignoring")
  }

}

class ServiceTrackerActor(config: ServiceConfig, scriptActor: ActorRef) extends Actor with ActorLogging {

  import WatchdogManagerActor._

  implicit val executor = context.system.dispatcher

  var state: State = State(0, 0)

  val dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS")

  private def component: String = config.name
  private def formatTime(time: Long): String = dateFormat.format(new Date(time))

  context.system.scheduler.schedule(config.heartbeatTimeout * 2, config.heartbeatTimeout, self, CheckHeartbeat)

  override def receive: Receive = {
    case msg @ HeartbeatMessage(name, _) if (name != component) ⇒ log.warning("{} is not for me", msg)
    case msg: HeartbeatMessage                                  ⇒ handleHeartbeat(msg)
    case CheckHeartbeat                                         ⇒ checkHeartbeat()
    case msg: ExecuteScriptResult                               ⇒ handleScriptResult(msg)
  }

  override def toString: String = s"${getClass.getSimpleName}[$component,$state]"

  def now = System.currentTimeMillis()

  private def isOutdated(time: Long): Boolean = time < now - config.heartbeatTimeout.toMillis * 2

  private def handleHeartbeat(msg: HeartbeatMessage): Unit = {

    val outdated = isOutdated(msg.time)

    outdated match {
      case true ⇒ log.info("{} heartbeart is old (). Ignoring", component, formatTime(msg.time))

      case false ⇒
        if (state.heartbeatMissingCount > 0) {
          log.info("{} heartbeats resumed at {}", component, formatTime(msg.time))
          //TODO: generate SystemEvent?
        }
    }

    state = state.update(msg.time, !outdated)
  }

  private def checkHeartbeat(): Unit = {
    if (state.isHeartbeatDue(now)) {
      state = state.incrementFailure
      log.warning("{} missed {} heartbeats. Last one was {}", component, state.heartbeatMissingCount, state.lastHeartbeatWhen)
      if (state.isLimitReached) {
        log.warning("{} reached maximum # of heartbeat failures", component)
        //TODO: generate SystemEvent?
        handleLimitReached()
      }
    }
  }

  def handleLimitReached(): Unit = {
    if (state.askedRestart.isDefined) {

      log.warning("{}: already asked service restart at {}. Got no answer yet", component, formatTime(state.askedRestart.get))

    } else if (state.askedStatus.isDefined) {

      state.statusResult match {
        case None ⇒
          log.warning("{}: already asked service status at {}. Got no answer yet", component, formatTime(state.askedStatus.get))

        case Some(Right(false)) ⇒
          log.warning("{}: service was not running. Will attempt service restart", component)
          state = state.copy(askedRestart = Some(now))
          sendRestartService()

        case other ⇒
          log.warning("{}: status result is {}. Nowhere to go from here", component, other)
        //TODO: generate critical SystemEvent?
      }

    } else {
      log.warning("{}: attempting service status", component)
      state = state.copy(askedStatus = Some(now))
      sendCheckService()
    }
  }

  private def sendCheckService() {
    val script = ExecuteScript(script = config.servicePath, arguments = Seq("status"))
    scriptActor ! script
  }

  private def sendRestartService() {
    val script = ExecuteScript(script = config.servicePath, arguments = Seq("restart"))
    scriptActor ! script
  }

  private def handleScriptResult(scriptResult: ExecuteScriptResult) {

    log.info("{} execute result: {}", component, scriptResult)

    val arg = scriptResult.script.arguments
    if (arg.size != 1) {
      //very strange should not happen
      log.error("Unexpected script result received: " + scriptResult)
    } else {
      //check argument
      arg(0) match {
        case "restart" ⇒ logScriptResult(scriptResult)
        case "status"  ⇒ processStatusResult(scriptResult)
      }
    }
  }

  private def logScriptResult(scriptResult: ExecuteScriptResult) {
    val arg = scriptResult.script.arguments

    scriptResult.resultCode match {
      case Left(tr)                   ⇒ log.error("%s %s: failed".format(arg(0), component), tr)
      case Right(code) if (code == 0) ⇒ log.debug("%s %s: succeeded".format(arg(0), component))
      case Right(code) ⇒ {
        log.warning("%s %s: failed %d".format(arg(0), component, code))
        printStreams(scriptResult)
      }
    }
  }

  private def processStatusResult(scriptResult: ExecuteScriptResult): Unit = {

    if (state.askedStatus.isDefined && state.statusResult.isEmpty) {
      val arg = scriptResult.script.arguments
      scriptResult.resultCode match {
        case Left(throwable) ⇒
          log.error(throwable, "{} {}: failed", arg(0), component)
          state = state.withStatusResult(Some(Left(throwable.toString))) //update state
        case Right(returnCode) ⇒
          val running = returnCode == 0
          state = state.withStatusResult(Some(Right(running))) //update state
          if (running) {
            log.debug("%s is running".format(arg(0))) //service is still running
          } else {
            //service is down
            log.warning("%s is down: trying to restart service".format(arg(0)))
            printStreams(scriptResult)
            handleLimitReached
          }
      }

    } else {
      log.info("{}: was not expecting a status result as this moment. Ignoring", component)
    }
  }

  private def printStreams(scriptResult: ExecuteScriptResult) {
    val arg = scriptResult.script.arguments
    if (!scriptResult.stdOut.isEmpty) {
      log.info("%s %s: StdOut %s".format(arg(0), component, scriptResult.stdOut.mkString("\n")))
    }
    if (!scriptResult.stdError.isEmpty) {
      log.info("%s %s: StdError %s".format(arg(0), component, scriptResult.stdError.mkString("\n")))
    }
  }

  case class State(lastHeartbeat: Long,
                   heartbeatMissingCount: Int,
                   askedStatus: Option[Long] = None,
                   askedRestart: Option[Long] = None,
                   statusResult: StatusResult = None) {

    def update(time: Long, reset: Boolean): State = {
      var result = this.copy(lastHeartbeat = Math.max(lastHeartbeat, time))
      if (reset) {
        result = result.copy(heartbeatMissingCount = 0, askedStatus = None, askedRestart = None, statusResult = None)
      }
      result
    }

    def isHeartbeatDue(time: Long): Boolean = time - lastHeartbeat > config.heartbeatTimeout.toMillis * 2

    def isLimitReached: Boolean = heartbeatMissingCount > config.heartbeatTolerance

    def incrementFailure: State = this.copy(heartbeatMissingCount = heartbeatMissingCount + 1)

    def lastHeartbeatWhen: String = lastHeartbeat match {
      case 0 ⇒ "<never>"
      case x ⇒ s"at ${formatTime(x)}"
    }

    def withStatusResult(result: StatusResult): State = this.copy(statusResult = result)
  }

  object CheckHeartbeat

}

case class ActivateServiceWatch(name: String)

case class ServiceConfig(name: String,
                         servicePath: String,
                         heartbeatTimeout: FiniteDuration,
                         heartbeatTolerance: Int)

case class WatchdogConfig(services: List[ServiceConfig], amqpService: ServicesConfiguration) {
  def configOf(service: String): Option[ServiceConfig] = services.find(_.name == service)
}

trait WatchdogFormats {
  val formats: Formats = DefaultFormats
}

class WatchdogMQConsumer(val target: ActorRef) extends MQConsumer with WatchdogFormats {

  object HeartbeatExtractor extends MessageExtractor[HeartbeatMessage]

  override def ownReceive: Receive = {
    case delivery @ HeartbeatExtractor(msg) ⇒ target ! msg
  }
}

object WatchdogManagerActor {

  type StatusResult = Option[Either[String, Boolean]]

  def create(system: ActorSystem, config: WatchdogConfig, amqp: AmqpConnection): ActorRef = {
    val scriptActor = system.actorOf(Props(new ExecuteScriptActor))
    val manager = system.actorOf(Props(new WatchdogManagerActor(config, scriptActor)), "WatchdogManager")
    system.eventStream.subscribe(manager, classOf[ActivateServiceWatch])
    val consumer = system.actorOf(Props(new WatchdogMQConsumer(manager)))
    amqp.queue(config.amqpService).wireConsumer(consumer, true)
    manager
  }

}
