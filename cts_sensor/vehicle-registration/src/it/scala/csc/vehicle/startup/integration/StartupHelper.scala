/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.vehicle.startup.integration

import csc.gantry.config.{CorridorConfig, CorridorsConfig, SystemConfig, ZkCorridorInfo, _}
import unit.CertificateTestUtil

/**
 * Helper object for startup testing
 */
object StartupHelper {
  class LaneConfigMock(laneConfig: LaneConfig) extends DefaultLaneConfiguration {

    private val _lanes = List(laneConfig)
    override def lanes: List[LaneConfig] = _lanes

    override def getSystemConfig(system: String): Option[SystemConfig] = {
      Some(new SystemConfigImpl(id = "all",
        name = "",
        description = "",
        serialNumber = SerialNumber(""),
        typeCertificates = CertificateTestUtil.createCertificate("cert XXXX"),
        maxSpeed = 130,
        compressionFactor = 20,
        nrDaysKeepTrafficData = 0,
        nrDaysKeepViolations = 0,
        region = "",
        roadNumber = "",
        roadPart = ""))
    }
    override def getCorridorsConfig(system: String): Option[CorridorsConfig] = {
      val redLightCorridor = CorridorConfig(
        id = "1",
        name = "RedLight",
        serviceType = "RedLight",
        info = ZkCorridorInfo(corridorId = 1,
          locationCode = "3",
          reportId = "",
          reportHtId = "",
          reportPerformance = true,
          locationLine1 = "Location X",
          locationLine2 = Some("Secret")))
      val speedCorridor = CorridorConfig(
        id = "2",
        name = "Speed",
        serviceType = "SpeedFixed",
        info = ZkCorridorInfo(corridorId = 2,
          locationCode = "3",
          reportId = "",
          reportHtId = "",
          reportPerformance = true,
          locationLine1 = "Location Y",
          locationLine2 = Some("Secret")))
      Some(CorridorsConfig(corridors = List(redLightCorridor, speedCorridor)))
    }

    //    def getActiveLaneIds(): Seq[String] = {
    //      Seq(laneConfig.lane.laneId)
    //    }
  }
}
