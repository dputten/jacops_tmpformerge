/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.vehicle.startup.integration

import java.awt.{Point => AwtPoint}
import java.io.File
import java.util.Date

import akka.actor.{Actor, ActorSystem, Props}
import akka.testkit.{TestKit, TestProbe}
import scala.concurrent.duration._
import com.typesafe.config.ConfigFactory
import csc.common.Extensions._
import csc.gantry.config.{CameraConfig, ImageMaskVertex, _}
import csc.image.recognize.{RecognizeImageRequest, RecognizeImageResponse, TranslatePlate}
import csc.vehicle.config._
import csc.vehicle.message.{Lane, VehicleRegistrationMessage}
import csc.vehicle.registration.IRoseDataHelper
import csc.vehicle.registration.IRoseDataHelper._
import csc.vehicle.startup.StartupVehicleRegistration
import ftplet.FileReadyNotifier
import imagefunctions.jpg.JpgFunctions
import imagefunctions.{ImageFunctionsFactory, LicensePlate, Point}
import org.scalatest._
import testframe.Resources

class StartupIRoseTest extends TestKit(ActorSystem("StartupIRoseTest", ConfigFactory.parseString(""))) with WordSpecLike with MustMatchers
  with BeforeAndAfterEach with BeforeAndAfterAll {
  import IRoseDataHelper._
  import StartupHelper._
  var startup: Option[StartupVehicleRegistration] = None

  val testRedLightImage = "iRose_RedLightTest.jpg"
  val testOverviewImage = "iRose_OverviewTest.jpg"
  val testMeasureMethod2Image = "iRose_MeasureMethod2Test.jpg"
  val testLicenseTifImage = "iRose_LicenseTest_2.tif"
  val testLicenseJpgImage = "iRose_LicenseTest.jpg"
  val testEnlargedLicenseImage = "iRose_LicenseTest_Enlarged.jpg"

  //  val defaultLicenseTif = "license.tif"
  val defaultLicenseTif = "license.tif"

  val rootDir = Resources.getResourceDirPath()
  val rootPath = rootDir.getAbsolutePath
  val startupIRoseResourceDir = new File(rootDir, "startup")
  val baseTestDir = createSubdirectory(rootDir, "out/irose-startup-test")

  val laneDir = createSubdirectory(baseTestDir, "laneDir")
  val jpgDir = createSubdirectory(baseTestDir, "jpgDir")
  val storeDir = createSubdirectory(baseTestDir, "outputStoreDir")
  val tempDataDir = new File(laneDir, "tempIRoseData")

  override def afterAll() {
    startup.foreach(_.stop())
    system.shutdown()
    deleteDirectory(baseTestDir)
  }

  override def afterEach {
    clearDirectory(laneDir)
    clearDirectory(jpgDir)
    clearDirectory(storeDir)
  }

  val mask = new ImageMaskImpl(
    vertices = List(
      new AwtPoint(1515, 905),
      new AwtPoint(2432, 916),
      new AwtPoint(2270, 1369),
      new AwtPoint(1299, 1302)).map { point ⇒ new ImageMaskVertex(point.x, point.y) })

  val laneConfig = IRoseSensorConfigImpl(
    lane = new Lane(
      laneId = "100",
      name = "Lane 1",
      gantry = "42.6 HM",
      system = "A2",
      sensorGPS_latitude = 0,
      sensorGPS_longitude = 0),
    camera = new CameraConfig(
      relayURI = laneDir.getAbsolutePath,
      cameraHost = "localhost",
      cameraPort = 1400,
      maxTriggerRetries = 10,
      timeDisconnected = 20000),
    imageMask = Some(mask),
    recognizeOptions = Map(),
    seqConfig = LaneSeqConfig(None, None))

  def completeJsonWith3Images(inputDir: File) {
    createCompleteAndCorrectJsonFile(inputDir)
    copyFile(startupIRoseResourceDir, testRedLightImage, inputDir, defaultRedLightJpg)
    copyFile(startupIRoseResourceDir, testOverviewImage, inputDir, defaultOverviewJpg)
    copyFile(startupIRoseResourceDir, testMeasureMethod2Image, inputDir, defaultMeasureMethod2Jpg)
    copyFile(startupIRoseResourceDir, testLicenseTifImage, tempDataDir, defaultLicenseTif)
  }

  "StartupIRoseTest: A started lane, configured for iRose," when {
    val configFile = Resources.getResourcePath("startup/iRoseIntegrationTest.conf")
    System.setProperty("ROOTDIR", rootPath)
    val configuration = Configuration.createFromFile(configFile)

    val licenseJpgFromTif = new File(startupIRoseResourceDir, testLicenseJpgImage)
    val licenseForDecorationImage = new File(startupIRoseResourceDir, testEnlargedLicenseImage)
    val testJpgFunctions = new TestJpgFunctions(licenseJpgFromTif, licenseForDecorationImage)
    ImageFunctionsFactory.setJpgFunctions(testJpgFunctions)

    val probe = TestProbe()
    val laneCfg = new LaneConfigMock(laneConfig)

    val startVR = new StartupVehicleRegistration(configuration, laneCfg)
    startVR.start(Some(probe.ref))
    startup = Some(startVR)

    startVR.system.actorOf(Props(new MockRecognizer(tempDataDir, defaultLicenseTif)), "recognize")

    //wait for Camel to start
    Thread.sleep(2000)
    //CamelExtension(globalSystem).awaitActivation(relayRef, 2 seconds)

    "receiving a message containing a directory path" must {
      "process the iRose data in that directory" in {
        val dataDir = createSubdirectory(laneDir, "tempIRoseData")
        completeJsonWith3Images(dataDir)
        val sender = new RelayDirectorySender(relayPort = 12403, laneDir = laneDir.getAbsolutePath)
        sender.send(dataDir.getAbsolutePath)
        val recvMsg = probe.expectMsgType[VehicleRegistrationMessage](30 seconds)
        recvMsg.lane must be(laneConfig.lane)
        recvMsg.eventId.get must startWith(laneConfig.lane.laneId.toString)
        val storeSubdirectory = getStoreSubdirectory(new Date(recvMsg.eventTimestamp.get))
        directoryIncludes(storeSubdirectory, recvMsg.eventId.get + "_meta.json") must be(true)
        directoryIncludes(storeSubdirectory, recvMsg.eventId.get + "_overview.jpg") must be(false)
        directoryIncludes(storeSubdirectory, recvMsg.eventId.get + "_overviewMasked.jpg") must be(true)
        directoryIncludes(storeSubdirectory, recvMsg.eventId.get + "_overviewRedLight.jpg") must be(true)
        directoryIncludes(storeSubdirectory, recvMsg.eventId.get + "_overviewSpeed.jpg") must be(true)
        directoryIncludes(storeSubdirectory, recvMsg.eventId.get + "_license.jpg") must be(true)
        directoryIncludes(storeSubdirectory, recvMsg.eventId.get + "_redLight.jpg") must be(true)
        directoryIncludes(storeSubdirectory, recvMsg.eventId.get + "_measureMethod2Speed.jpg") must be(true)
      }
    }
  }

  def createSubdirectory(dir: File, subDir: String): File = {
    val newDir = new File(dir, subDir)
    if (!newDir.exists()) {
      newDir.mkdirs()
    }
    newDir
  }

  def getStoreSubdirectory(date: Date): File = {
    new File(storeDir, date.toString("yyyyMMdd_HH", "UTC"))
  }

  def directoryIncludes(dir: File, filename: String): Boolean = {
    new File(dir, filename).exists()
  }
}

class MockRecognizer(licenseDir: File, licenseFilename: String) extends Actor {

  def receive = {
    case req: RecognizeImageRequest ⇒ {
      sender ! RecognizeImageResponse(req.msgId, req.imageFile, TranslatePlate.createLicensePlateResult(Some(recognizeImage())))
    }
  }

  def recognizeImage(): LicensePlate = {
    val snippetFilename = new File(licenseDir, licenseFilename)
    val plate = new LicensePlate()
    plate.setPlateStatus(LicensePlate.PLATESTATUS_PLATE_FOUND)
    plate.setLicenseSnippetFileName(snippetFilename.getAbsolutePath)
    plate.setPlateConfidence(80)
    plate.setPlateCountryCode(LicensePlate.PLATE_NETHERLANDS)
    plate.setPlateNumber("11SXK4")
    plate.setPlateCountryConfidence(80)
    plate.setPlateXOffset(542)
    plate.setPlateYOffset(252)
    plate.setRecognizer("DUMMY")
    plate.setReturnStatus(0)
    plate.setPlateTL(new Point(542, 252))
    plate.setPlateTR(new Point(680, 262))
    plate.setPlateBL(new Point(541, 279))
    plate.setPlateBR(new Point(679, 289))
    plate
  }

}

class RelayDirectorySender(relayPort: Int, laneDir: String) {
  val host = "localhost"
  val relayNotifier = new FileReadyNotifier(host, relayPort)

  /**
   * Sends the directory
   *
   * @param directoryPath The path to a directory
   */
  def send(directoryPath: String) {
    relayNotifier.sendNotification(directoryPath, System.currentTimeMillis(), "put")
  }
}

class TestJpgFunctions(licenseJpgFromTif: File, licenseJpgForDecorationImage: File) extends JpgFunctions {
  def bayerToJpg(inputFile: String, width: Int, height: Int, quality: Int, outputFile: String): Unit = {
    //println("this should not be called")
  }
  def rgbToJpg(inputFile: String, width: Int, height: Int, quality: Int, outputFile: String): Unit = {
    //println("rgbToJpg")
    val output = new File(outputFile)
    copyFile(licenseJpgFromTif.getParentFile, licenseJpgFromTif.getName, output.getParentFile, output.getName)
  }
  def grayToJpg(inputFile: String, width: Int, height: Int, quality: Int, outputFile: String): Unit = {
    //println("this should not be called")
  }
  def jpgToJpg(inputFile: String, width: Int, height: Int, quality: Int, outputFile: String): Unit = {
    //println("jpgToJpg")
    val output = new File(outputFile)
    copyFile(licenseJpgForDecorationImage.getParentFile, licenseJpgForDecorationImage.getName, output.getParentFile, output.getName)
  }
}
