/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.startup.integration

import java.io.File
import java.util.Date

import akka.actor.{ActorSystem, Props}
import akka.testkit.{TestKit, TestProbe}

import scala.concurrent.duration._
import csc.gantry.config.{CameraConfig, RadarCameraSensorConfigImpl, RadarConfig}
import csc.vehicle.config._
import csc.vehicle.message.{VehicleRegistrationMessage, _}
import csc.vehicle.registration.IRoseDataHelper._
import csc.vehicle.startup.StartupVehicleRegistration
import csc.vehicle.tools.vehicle.SendRelayRegistration
import org.scalatest._
import testframe.Resources

class StartupTest extends TestKit(ActorSystem("StartupTest")) with WordSpecLike with MustMatchers with BeforeAndAfterAll {
  import StartupHelper._

  val radarPort = 11600
  var startup: Option[StartupVehicleRegistration] = None

  override def afterAll() {
    startup.foreach(_.stop())
    system.shutdown()
  }
  val rootPath = Resources.getResourceDirPath().getAbsolutePath
  val laneDir = new File(rootPath, "test")
  if (!laneDir.exists()) {
    laneDir.mkdirs()
  }
  val jpgDir = new File(rootPath, "jpgDir")
  if (!jpgDir.exists()) {
    jpgDir.mkdirs()
  }
  val storeDir = new File(rootPath, "outputStore")
  if (!storeDir.exists()) {
    storeDir.mkdirs()
  }

  val laneConfig = new RadarCameraSensorConfigImpl(
    lane = new Lane(
      laneId = "100",
      name = "Lane 1",
      gantry = "42.6 HM",
      system = "A2",
      sensorGPS_latitude = 0,
      sensorGPS_longitude = 0),
    camera = new CameraConfig(
      relayURI = laneDir.getAbsolutePath,
      cameraHost = "localhost",
      cameraPort = 1400,
      maxTriggerRetries = 10,
      timeDisconnected = 20000),
    imageMask = None,
    recognizeOptions = Map(),
    radar = new RadarConfig(
      radarURI = "mina:tcp://localhost:%d?textline=true".format(radarPort),
      measurementAngle = 35,
      height = 5.5,
      surfaceReflection = 1))

  "A started lane" when {
    val configFile = Resources.getResourcePath("startup/integrationTest.conf")
    System.setProperty("ROOTDIR", rootPath)
    val configuration = Configuration.createFromFile(configFile)

    val globalConfig = configuration.globalConfig

    val probe = TestProbe()

    val laneCfg = new LaneConfigMock(laneConfig)
    val startVR = new StartupVehicleRegistration(configuration, laneCfg)
    startVR.start(Some(probe.ref))
    startup = Some(startVR)

    copyFile(new File(rootPath), "license.tif", jpgDir, "license.tif")

    startVR.system.actorOf(Props(new MockRecognizer(jpgDir, "license.tif")), "recognize")
    //wait for Camel to start
    Thread.sleep(2000)
    //CamelExtension(globalSystem).awaitActivation(relayRef, 2 seconds)

    "receiving registration events" must {
      "process the messages" in {
        val sender = new SendRelayRegistration(radarPort = radarPort, relayPort = 12401, laneDir = laneDir.getAbsolutePath)
        val image = Resources.getResourcePath("alfa.tif")
        val now = new Date()
        sender.sendRegistration(image = image, speed = 100, reflection = 500, now = now, radarDelay = 250)

        val recvMsg = probe.expectMsgType[VehicleRegistrationMessage](20 seconds)
        recvMsg.lane must be(laneConfig.lane)
        recvMsg.eventId.get must startWith(laneConfig.lane.laneId.toString)
      }
    }
  }
}
