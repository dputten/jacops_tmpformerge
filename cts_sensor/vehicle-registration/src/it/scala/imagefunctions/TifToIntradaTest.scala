package imagefunctions

import java.io._

import imagefunctions.intrada.IntradaFunctions
import imagefunctions.intrada.impl.IntradaFunctionsImpl
import org.junit.Test
import org.scalatest.junit.JUnitSuite
import testframe.Resources

/**
 * Test the imageToIntrada call to the IntradaFunctions implementation.
 */
class TifToIntradaTest extends JUnitSuite {

  var clib: Option[IntradaFunctions] = None

  def getClib: IntradaFunctions = {
    clib.getOrElse {
      val cl = IntradaFunctionsImpl.getDefaultInstance("CSC", "5EA63-P8GCD-T6WHC-3529X-WRBFF\0")
      clib = Some(cl)
      cl
    }
  }

  @Test
  def testDummy {}

  /**
   * Test the happy flow of the rgbToArh call.
   */
  def testOKSequentially {
    // test finding licenseplate a couple of times sequentially
    val input = Resources.getResourceFile("alfaRGB.tif")
    assert(input.exists(), "Input file doesn't exists: " + input.getAbsolutePath())
    val output = new File(input.getParentFile(), "alfaRGB.tif_plate_arh.tif")

    val license = getClib.imageToIntrada(input.getAbsolutePath(), output.getAbsolutePath);
    assert(license != null, "return value is null");
    assert(license.getPlateNumber() == "18LKRK", "license");
    assert(0 <= license.getPlateConfidence(), "plateConfidence needs to be higher than zero");
    assert(license.getPlateCountryCode() == LicensePlate.PLATE_NETHERLANDS, "country should be Netherlands");
    assert(output.getName() == "alfaRGB.tif_plate_arh.tif", "filename");
    assert(output.exists(), "Output file doesn't exists: " + output.getAbsolutePath());
    output.delete();
  }

  /**
   * a test with an image with no license plate
   */
  def testNoPlateInImage = {

    val input = Resources.getResourceFile("blank.tif")
    assert(input.exists(), "Input file doesn't exists: " + input.getAbsolutePath());
    val plate = new File(input.getParentFile, "blank.tif_plate_arh.tif");

    val license = getClib.imageToIntrada(input.getAbsolutePath(), plate.getAbsolutePath);
    assert(license != null, "return value is null");
    assert(license.getPlateStatus() == LicensePlate.PLATESTATUS_NO_PLATE_FOUND, "plate status");
    assert(license.getPlateNumber() == "", "license");
    assert(license.getPlateConfidence() == 0, "confidence");
    assert(license.getPlateCountryCode() == LicensePlate.PLATE_UNKNOWN, "authority (thus country) should be 0/unknown");
    assert(license.getLicenseSnippetFileName() == "", "snippet filename should not be set");

    assert(!plate.exists(), "Plate file does exist: " + plate.getAbsolutePath());

  }

  /**
   * a test with an non existing image
   */
  def testNoImage = {
    var exceptionThrown: Exception = null;
    val input = new File("src/test/resources/xxxx.tif");
    assert(!input.exists(), "Input file exists: " + input.getAbsolutePath());
    try {
      val license = getClib.imageToIntrada(input.getAbsolutePath(), "test_Snippet.tif");
    } catch {
      case e: ImageFunctionsException ⇒ exceptionThrown = e;
    }
    assert(exceptionThrown.getMessage() == "Status: -1 Problem getting file size", "exception: " + exceptionThrown.getMessage());
  }

}
