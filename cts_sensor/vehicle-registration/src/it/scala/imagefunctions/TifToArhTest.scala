package imagefunctions

/*
 * Copyright © CSC
 * Date: Jun 8, 2010
 * Time: 12:18:11 PM
 */

import java.io._

import imagefunctions.arh.impl.ArhFunctionsImpl
import org.junit.Test
import org.scalatest.junit.JUnitSuite

import scala.collection.mutable.HashMap

/**
 * Test the rgbToArh call to the ImageFunction implementation.
 */
class TifToArhTest2 extends JUnitSuite {

  @Test
  def testDummy() {}

  def initARH() {
    ImageFunctionsFactory.setArhFunctions(ArhFunctionsImpl.getInstance(5, "cmanpr-7.2.7.98", "latin"))
  }

  /**
   * Test the happy flow of the rgbToArh call.
   */
  def testOKSequentially {
    initARH
    // test finding licenseplate a couple of times sequentially
    for (i ← 0 until 10) {
      val input = new File("src/it/resources/alfaRGB.tif");
      assert(input.exists(), "Input file doesn't exists: " + input.getAbsolutePath());

      val clib = ImageFunctionsFactory.getARHFunctions();
      val license = clib.rgbToArh(input.getAbsolutePath());
      assert(license != null, "return value is null");
      assert(license.getPlateNumber() == "18LKRK", "license");
      assert(0 <= license.getPlateConfidence(), "plateConfidence needs to be higher than zero");
      assert(license.getPlateCountryCode() == LicensePlate.PLATE_NETHERLANDS, "country should be Netherlands");
      val output = new File(license.getLicenseSnippetFileName());
      assert(output.getName() == "alfaRGB.tif_plate_arh.tif", "filename");
      assert(output.exists(), "Output file doesn't exists: " + output.getAbsolutePath());
      output.delete();
    }

  }

  /**
   * a test with an image with no license plate
   */
  def testNoPlateInImage = {

    val input = new File("src/it/resources/blank.tif");
    assert(input.exists(), "Input file doesn't exists: " + input.getAbsolutePath());
    initARH
    val clib = ImageFunctionsFactory.getARHFunctions();

    val license = clib.rgbToArh(input.getAbsolutePath());
    assert(license != null, "return value is null");
    assert(license.getPlateStatus() == LicensePlate.PLATESTATUS_NO_PLATE_FOUND, "plate status");
    assert(license.getPlateNumber() == "", "license");
    assert(license.getPlateConfidence() == 0, "confidence");
    assert(license.getPlateCountryCode() == LicensePlate.PLATE_UNKNOWN, "authority (thus country) should be 0/unknown");
    assert(license.getLicenseSnippetFileName() == "", "snippet filename should not be set");

    val plate = new File("src/test/resources/blank.tif_plate_arh.tif");
    assert(!plate.exists(), "Plate file does exist: " + plate.getAbsolutePath());

  }

  /**
   * a test with an non existing image
   */
  def testNoImage = {
    var exceptionThrown: Exception = null;
    val input = new File("src/it/resources/xxxx.tif");
    assert(!input.exists(), "Input file exists: " + input.getAbsolutePath());
    initARH
    val clib = ImageFunctionsFactory.getARHFunctions();
    try {
      val license = clib.rgbToArh(input.getAbsolutePath());
    } catch {
      case e: ImageFunctionsException ⇒ exceptionThrown = e;
    }
    assert(exceptionThrown.getMessage() == "Status: -1 Problem getting file size", "exception: " + exceptionThrown.getMessage());
  }

  def testARHConf {
    val input = new File("src/it/resources/arh_conf.tif");
    assert(input.exists(), "Input file doesn't exists: " + input.getAbsolutePath());
    initARH
    val clib = ImageFunctionsFactory.getARHFunctions();
    val license = clib.bayerToArh(input.getAbsolutePath());
    assert(license != null, "return value is null");
    assert(license.getPlateNumber() == "BE988LZ", "license error: " + license.getPlateNumber());
    assert(0 <= license.getPlateConfidence(), "plateConfidence needs to be higher than zero");
    assert(100 >= license.getPlateConfidence(), "plateConfidence needs to be lower than 100");
    assert(license.getPlateCountryCode() == LicensePlate.PLATE_FRANCE, "country should be france " + license.getPlateCountryCode());
    assert(0 <= license.getPlateCountryConfidence(), "PlateCountryConfidence needs to be higher than zero");
    assert(100 >= license.getPlateCountryConfidence(), "PlateCountryConfidence needs to be lower than 100: " + license.getPlateCountryConfidence());
    val output = new File(license.getLicenseSnippetFileName());
    assert(output.exists(), "Output file doesn't exists: " + output.getAbsolutePath());
    output.delete();
  }

  /*
 * test that TiffToArh delivers a consistent license plate recognition
*/
  def testTiffToArhConsistentResult {
    var separator = System.getProperty("line.separator")
    ImageFunctionsFactory.setArhFunctions(ArhFunctionsImpl.getInstance(5, "cmanpr-7.2.7.98", "latin"))

    var pathToPictures = new File("src/it/resources/imagefunctions/tiff")
    assert(pathToPictures.exists, "Path to pictures doesn't exists: " + pathToPictures.getAbsolutePath());

    var files = pathToPictures.listFiles(new FileFilter {
      def accept(file: File): Boolean = {
        !file.getName.contains("_plate")
      }
    })
    assert(files.length > 0, "No pictures present");

    var result = HashMap.empty[String, HashMap[Int, LicensePlate]]
    val clib = ImageFunctionsFactory.getARHFunctions();

    for (numberOfTries ← 1 until 4) {
      for (i ← 0 until files.length) {
        var file = files(i)

        assert(file.exists, "Input file doesn't exists: " + file.getAbsolutePath());

        val license = clib.bayerToArh(file.getAbsolutePath());
        val resultItem = result.get(file.getName)
        if (resultItem.isDefined) {
          resultItem.get += (numberOfTries -> license)
        } else {
          result += (file.getName -> HashMap(numberOfTries -> license))
        }
      }
    }
    var assertResult = new StringBuilder

    result.foreach {
      case (fileName, resultOfTries) ⇒
        var prevLicensePlate: LicensePlate = null
        var prevRun = -1
        resultOfTries.foreach {
          case (numberOfTry, licensePlate) ⇒
            if (prevLicensePlate == null) {
              prevLicensePlate = licensePlate
              prevRun = numberOfTry
            } else if (prevLicensePlate.getPlateNumber != licensePlate.getPlateNumber) {
              assertResult.append(separator + "file=[" + fileName + "], run=[" + prevRun + "], license=[" + prevLicensePlate.getPlateNumber + "] != run=[" + numberOfTry + "], license=[" + licensePlate.getPlateNumber + "]" + separator)
            }
        }
        prevLicensePlate = null
        prevRun = -1
    }

    assert(assertResult.toString.length == 0, assertResult.toString)
  }
}
