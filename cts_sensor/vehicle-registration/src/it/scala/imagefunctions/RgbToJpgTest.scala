package imagefunctions

/*
 * Copyright © CSC
 * Date: Jun 8, 2010
 * Time: 12:18:11 PM
 */

import java.io._

import org.junit.Test
import org.scalatest.junit.JUnitSuite
import testframe.Resources

/**
 * Test the rgbToJpg call to the ImageFunction implementation.
 * TODO: ST 20100701 Test also licences with different sizes and check result by doing a byte compare
 * TODO: ST 20100701 It look like the JPG lib can't scale-up when resizing
 */
class RgbToJpgTest extends JUnitSuite {
  /**
   * Test the happy flow of the rgbToJpg call.
   */
  @Test
  def testOK {
    val input = Resources.getResourceFile("alfaRGB.tif");
    assert(input.exists(), "Input file doesn't exists: " + input.getAbsolutePath());
    val output = new File(input.getParent, "test_alfa.jpg");
    output.delete();
    output.deleteOnExit
    assert(!output.exists(), "Output file shouldn't exists");
    ImageFunctionsFactory.setDefaultFunctions()

    val clib = ImageFunctionsFactory.getJpgFunctions();
    clib.rgbToJpg(input.getAbsolutePath(), 640, 480, 30, output.getAbsolutePath());

    assert(output.exists(), "Output file doesn't exists");
  }
  @Test
  def testGrayLicense {
    val input = Resources.getResourceFile("license.tif");
    assert(input.exists(), "Input file doesn't exists: " + input.getAbsolutePath());
    val output = new File(input.getParent, "test_license.jpg");
    output.delete();
    //output.deleteOnExit
    assert(!output.exists(), "Output file shouldn't exists");
    ImageFunctionsFactory.setDefaultFunctions()

    val clib = ImageFunctionsFactory.getJpgFunctions();
    clib.grayToJpg(input.getAbsolutePath(), 0, 0, 80, output.getAbsolutePath());

    assert(output.exists(), "Output file doesn't exists");
  }
}