package imagefunctions

import java.io._

import imagefunctions.arh.impl.ArhFunctionsImpl
import org.junit.Test
import org.scalatest.junit.JUnitSuite

import scala.collection.mutable.HashMap

/*
 * used for testing JpgToIcr
*/
class JpgToArh extends JUnitSuite {
  @Test
  def testDummy {}

  /*
   * test that jpgToArh delivers a consistent license plate recognition
  */
  def testConsistentResult {
    var separator = System.getProperty("line.separator")
    ImageFunctionsFactory.setArhFunctions(ArhFunctionsImpl.getInstance(5, "cmanpr-7.2.7.98", "latin"))

    var pathToPictures = new File("src/it/resources/imagefunctions/testplaatjes")
    assert(pathToPictures.exists, "Path to pictures doesn't exists: " + pathToPictures.getAbsolutePath());

    var files = pathToPictures.listFiles(new FileFilter {
      def accept(file: File): Boolean = {
        !file.getName.contains("_plate")
      }
    })
    assert(files.length > 0, "No pictures present");

    var result = HashMap.empty[String, HashMap[Int, LicensePlate]]
    val clib = ImageFunctionsFactory.getARHFunctions();

    for (numberOfTries ← 1 until 4) {
      for (i ← 0 until files.length) {
        var file = files(i)

        assert(file.exists, "Input file doesn't exists: " + file.getAbsolutePath());

        val license = clib.jpgToArh(file.getAbsolutePath());
        val resultItem = result.get(file.getName)
        if (resultItem.isDefined) {
          resultItem.get += (numberOfTries -> license)
        } else {
          result += (file.getName -> HashMap(numberOfTries -> license))
        }
      }
    }
    var assertResult = new StringBuilder

    result.foreach {
      case (fileName, resultOfTries) ⇒
        var prevLicensePlate: LicensePlate = null
        var prevRun = -1
        resultOfTries.foreach {
          case (numberOfTry, licensePlate) ⇒
            if (prevLicensePlate == null) {
              prevLicensePlate = licensePlate
              prevRun = numberOfTry
            } else if (prevLicensePlate.getPlateNumber != licensePlate.getPlateNumber) {
              assertResult.append(separator + "file=[" + fileName + "], run=[" + prevRun + "], license=[" + prevLicensePlate.getPlateNumber + "] != run=[" + numberOfTry + "], license=[" + licensePlate.getPlateNumber + "]" + separator)
            }
        }
        prevLicensePlate = null
        prevRun = -1
    }

    assert(assertResult.toString.length == 0, assertResult.toString)
  }
}