/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package imagefunctions.performance

import java.io.File
import java.util.concurrent.{CyclicBarrier, TimeUnit}

import csc.akka.logging.DirectLogging
import imagefunctions.ImageFunctionsFactory
import org.junit.Test
import org.scalatest.{MustMatchers, BeforeAndAfterAll}
import org.scalatest.junit.JUnitSuite
import testframe.Resources

class CompareJPEG extends JUnitSuite with DirectLogging with MustMatchers with BeforeAndAfterAll {

  /**
   * test the performance change between rgb and bayer function in sequence.
   */
  @Test
  def sequenceBayer {
    ImageFunctionsFactory.setDefaultFunctions()
    val nr = 100
    val clib = ImageFunctionsFactory.getJpgFunctions();
    val input = Resources.getResourceFile("alfa.tif");
    val output = input.getParent + "/test_alfa%s.jpg";

    val start = System.currentTimeMillis()
    for (i ← 0 until nr) {
      val out = String.format(output, i.toString)
      clib.bayerToJpg(input.getAbsolutePath(), 0, 0, 30, out);
    }
    val end = System.currentTimeMillis()
    //clear images
    for (i ← 0 until nr) {
      val out = String.format(output, i.toString)
      new File(out).delete
    }
    val time = end - start
    val avg = time / nr
    log.info("Bayer test Done avg=%d".format(avg))
  }
  /**
   * test the performance change between rgb and bayer function in sequence.
   */
  @Test
  def sequenceRGB {
    ImageFunctionsFactory.setDefaultFunctions()
    val nr = 100
    val clib = ImageFunctionsFactory.getJpgFunctions();
    val input = Resources.getResourceFile("alfaRGB.tif");
    val output = input.getParent + "/test_alfa%s.jpg";

    val start = System.currentTimeMillis()
    for (i ← 0 until nr) {
      val out = String.format(output, i.toString)
      clib.rgbToJpg(input.getAbsolutePath(), 0, 0, 30, out)
    }
    val end = System.currentTimeMillis()
    //clear images
    for (i ← 0 until nr) {
      val out = String.format(output, i.toString)
      new File(out).delete
    }
    val time = end - start
    val avg = time / nr
    log.info("RGB test Done avg=%d".format(avg))
  }

  /**
   * test the performance change between rgb and bayer function in sequence.
   */
  @Test
  def parallelBayer {
    ImageFunctionsFactory.setDefaultFunctions()
    val nr = 50
    val nrThreads = 5
    val clib = ImageFunctionsFactory.getJpgFunctions()
    val input = Resources.getResourceFile("alfa.tif");
    val bar = new CyclicBarrier(nrThreads + 1)
    val output = input.getParent + "/test_alfa%s.jpg";

    val start = System.currentTimeMillis()
    for (i ← 0 until nrThreads) {
      val thread = new Thread {
        override def run() {
          val start = System.currentTimeMillis()
          for (j ← 0 until nr) {
            val out = String.format(output, (i.toString + j.toString))
            clib.bayerToJpg(input.getAbsolutePath(), 0, 0, 30, out)
          }
          val end = System.currentTimeMillis()
          val time = end - start
          val avg = time / nr
          log.info("Bayer parallel thread test Done avg=%d".format(avg))
          bar.await
        }
      }
      thread.start
    }
    bar.await(120, TimeUnit.SECONDS)
    val end = System.currentTimeMillis()
    //clear images
    for (i ← 0 until nrThreads) {
      for (j ← 0 until nr) {
        val out = String.format(output, (i.toString + j.toString))
        new File(out).delete
      }
    }
    val time = end - start
    val avg = time / (nr * nrThreads)
    log.info("Bayer parallel test Done avg=%d".format(avg))
  }

}