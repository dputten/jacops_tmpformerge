package imagefunctions.performance

/*
 * Copyright © CSC
 * Date: Jun 8, 2010
 * Time: 12:18:11 PM
 */

import java.io._
import java.util.concurrent.{BrokenBarrierException, CyclicBarrier, TimeUnit}

import imagefunctions._
import org.junit.Test
import org.scalatest.junit.JUnitSuite
import testframe.Resources

/**
 *
 * Test the rgbToArh call to the ImageFunction implementation in parallel.
 */
class TifToArhParallelTest extends JUnitSuite {

  @Test
  def testDummy {}
  /**
   * Test the happy flow of the rgbToArh call.
   */
  def testOKParallel: Unit =
    {
      var N = 2; // number of threads to start and wait for
      val barrier = new CyclicBarrier(N + 1);

      class Worker(filename: String) extends Runnable {
        def run() {

          val input = new File(this.filename);
          assert(input.exists(), "Input file doesn't exists: " + input.getAbsolutePath());
          ImageFunctionsFactory.setDefaultFunctions();
          val clib = ImageFunctionsFactory.getARHFunctions()
          val license = clib.rgbToArh(input.getAbsolutePath());
          assert(license != null, "return value is null");
          if (this.filename.endsWith("alfaRGB2.tif")) {
            assert(license.getPlateNumber() == "36PVLX", "license");
          } else {
            assert(license.getPlateNumber() == "18LKRK", "license");
          }
          assert(0 <= license.getPlateConfidence(), "plateConfidence needs to be higher than zero");
          assert(license.getPlateCountryCode() == LicensePlate.PLATE_NETHERLANDS, "country should be Netherlands");
          val output = new File(license.getLicenseSnippetFileName());
          assert(output.exists(), "Output file doesn't exists: " + output.getAbsolutePath());
          output.delete();

          try {
            barrier.await();
          } catch {
            case ex: InterruptedException   ⇒ return ;
            case ex: BrokenBarrierException ⇒ return ;
          }
        }
      }

      val filenames = new Array[String](N)
      val dir = Resources.getResourceDirPath()
      for (k ← 0 until N) {
        filenames(k) = dir.getAbsolutePath + "/alfaRGB" + ((k % 10) + 1) + ".tif"
      }

      // test finding licenseplate a couple of times in parallel
      var first = true;
      for (i ← 0 until N) {
        new Thread(new Worker(filenames(i))).start();
        // only wait a while after starting the first thread (to see the initialization speed up)
        if (first) {
          first = false;
          try {
            Thread.sleep(10000);
          } catch {
            case e: Exception ⇒
          }
        }
      }
      try {
        barrier.await(20, TimeUnit.SECONDS);
      } catch {
        case ex: InterruptedException   ⇒ return ;
        case ex: BrokenBarrierException ⇒ return ;
      }
    }

}