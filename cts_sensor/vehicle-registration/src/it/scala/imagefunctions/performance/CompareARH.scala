package imagefunctions.performance

/*
 * Copyright © CSC
 * Date: Aug 19, 2010
 * Time: 2:30:21 PM
 */

import csc.akka.logging.DirectLogging
import imagefunctions.ImageFunctionsFactory
import org.junit.Test
import org.scalatest.MustMatchers
import org.scalatest.junit.JUnitSuite
import testframe.Resources

class CompareARH extends JUnitSuite with MustMatchers with DirectLogging {
  @Test
  def testDummy {}

  /**
   * test the performance change between rgb and bayer function in sequence.
   */
  def sequenceBayer {
    ImageFunctionsFactory.setDefaultFunctions()
    val nr = 100
    val clib = ImageFunctionsFactory.getARHFunctions();
    val input = Resources.getResourceFile("alfa.tif")

    val start = System.currentTimeMillis()
    for (i ← 0 until nr) {
      val licence = clib.bayerToArh(input.getAbsolutePath());
    }
    val end = System.currentTimeMillis()
    val time = end - start
    val avg = time / nr
    log.info("Bayer test Done avg=%d".format(avg))
  }
  /**
   * test the performance change between rgb and bayer function in sequence.
   */
  def sequenceRGB {
    ImageFunctionsFactory.setDefaultFunctions()
    val nr = 100
    val clib = ImageFunctionsFactory.getARHFunctions();
    val input = Resources.getResourceFile("alfaRGB.tif");

    val start = System.currentTimeMillis()
    for (i ← 0 until nr) {
      val licence = clib.rgbToArh(input.getAbsolutePath());
    }
    val end = System.currentTimeMillis()
    val time = end - start
    val avg = time / nr
    log.info("RGB test Done avg=%d".format(avg))
  }
}