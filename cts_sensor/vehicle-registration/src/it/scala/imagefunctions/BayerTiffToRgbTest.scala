package imagefunctions

/*
 * Copyright © CSC
 * Date: Jun 8, 2010
 * Time: 12:18:11 PM
 */

import java.io._
import java.util.concurrent.{CyclicBarrier, TimeUnit}

import csc.akka.logging.DirectLogging
import org.junit.Test
import org.scalatest.junit.JUnitSuite
import testframe.Resources

/**
 * Test the bayerTiffToRgb call to the ImageFunction implementation.
 */
class BayerTiffToRgbTest extends JUnitSuite with DirectLogging {
  /**
   * Test the happy flow of the bayerTiffToRgb call.
   */
  @Test
  def testOK = {
    val input = Resources.getResourceFile("alfa.tif");
    assert(input.exists(), "Input file doesn't exists: " + input.getAbsolutePath());
    val output = new File(input.getParent, "test_alfa.tif");
    output.delete();
    output.deleteOnExit
    assert(!output.exists(), "Output file shouldn't exists");
    ImageFunctionsFactory.setDefaultFunctions()

    val clib = ImageFunctionsFactory.getBayerTifFunctions();
    val imageData = clib.bayerTiffToRgb(input.getAbsolutePath(), output.getAbsolutePath());
    assert(imageData != null, "return value is null");
    assert(imageData.getImageWidth() == 1392, "imageWidth");
    assert(imageData.getImageLength() == 1040, "imageLength");
    assert(imageData.getBitsPerSample() == 8, "bits/Sample");
    assert(imageData.getCompression() == 0, "Compression");
    assert(imageData.getPhotometricInterpretation() == 1, "PhotometricInterpretation");

    assert(output.exists(), "Output file doesn't exists");

  }
  /**
   * Test the happy flow of the bayerTiffToRgb call.
   */
  @Test
  def testGetInfo = {
    val input = Resources.getResourceFile("alfa.tif");
    assert(input.exists(), "Input file doesn't exists: " + input.getAbsolutePath());
    ImageFunctionsFactory.setDefaultFunctions()

    val clib = ImageFunctionsFactory.getBayerTifFunctions();
    val imageData = clib.getTifMetaData(input.getAbsolutePath());
    assert(imageData != null, "return value is null");
    assert(imageData.getImageWidth() == 1392, "imageWidth");
    assert(imageData.getImageLength() == 1040, "imageLength");
    assert(imageData.getBitsPerSample() == 8, "bits/Sample");
    assert(imageData.getCompression() == 0, "Compression");
    assert(imageData.getPhotometricInterpretation() == 1, "PhotometricInterpretation");
  }
  /**
   * Test a partly saved image
   */
  @Test
  def testPartFile = {
    val input = Resources.getResourceFile("alfa_part2.tif")
    assert(input.exists(), "Input file doesn't exists: " + input.getAbsolutePath())
    val output = new File(input.getParent, "test_alfa_part.tif")
    output.delete();
    output.deleteOnExit
    assert(!output.exists(), "Output file shouldn't exists");
    ImageFunctionsFactory.setDefaultFunctions()

    val clib = ImageFunctionsFactory.getBayerTifFunctions();
    try {
      val clib = ImageFunctionsFactory.getBayerTifFunctions();
      val imageData = clib.bayerTiffToRgb(input.getAbsolutePath(), output.getAbsolutePath());
      fail("Should get an exception")
    } catch {
      case e: ImageFunctionsException ⇒ assert(e.getStatus == -3, " unexpected error code " + e.getStatus)
      case _: Throwable                          ⇒ fail("failed by image test")
    }
  }
  /**
   * Test a partly saved image with incomplete header
   */
  @Test
  def testPartHeaderFile = {
    val input = Resources.getResourceFile("alfa_part.tif");
    assert(input.exists(), "Input file doesn't exists: " + input.getAbsolutePath());
    val output = new File(input.getParent, "test_alfa_part.tif")
    output.delete();
    output.deleteOnExit
    assert(!output.exists(), "Output file shouldn't exists");
    ImageFunctionsFactory.setDefaultFunctions()

    try {
      val clib = ImageFunctionsFactory.getBayerTifFunctions();
      val imageData = clib.bayerTiffToRgb(input.getAbsolutePath(), output.getAbsolutePath());
      fail("Should get an exception")
    } catch {
      case e: ImageFunctionsException ⇒ assert(e.getStatus == -3, " unexpected error code " + e.getStatus)
      case _: Throwable                          ⇒ fail("failed by image test")
    }
  }
  /**
   * Test a output dir doesn't exist
   */
  @Test
  def testFailedOutput = {
    val input = Resources.getResourceFile("alfa.tif");
    assert(input.exists(), "Input file doesn't exists: " + input.getAbsolutePath());
    val output = new File("src/testttttttttt/resources/test_alfa_part.tif");
    output.deleteOnExit
    ImageFunctionsFactory.setDefaultFunctions()

    try {
      val clib = ImageFunctionsFactory.getBayerTifFunctions();
      val imageData = clib.bayerTiffToRgb(input.getAbsolutePath(), output.getAbsolutePath());
      fail("Should get an exception")
    } catch {
      case e: ImageFunctionsException ⇒ assert(e.getStatus == -4, " unexpected error code " + e.getStatus)
      case _: Throwable                          ⇒ fail("failed by image test")
    }
  }

  /**
   * Test a multi threading
   * This test uses a lot of memory and diskspace. So this isn't part of the defauls testset
   */
  //@Test
  def testMultiTread = {
    val input = Resources.getResourceFile("alfa.tif")
    val nrThreads = 50
    val thread = new Array[Thread](nrThreads)
    val bar = new CyclicBarrier(nrThreads + 1)
    for (i ← 0 until nrThreads) {
      val output = new File(input.getParent + "/test_alfa.tif" + i);
      output.deleteOnExit
      thread(i) = new Thread(new myThread(input, output, bar, i))
    }

    ImageFunctionsFactory.setDefaultFunctions()
    for (t ← thread) {
      t.start
    }
    val start = System.currentTimeMillis()
    bar.await(nrThreads, TimeUnit.SECONDS)
    val end = System.currentTimeMillis()
    val time = end - start
    val avg = time / nrThreads
    log.info("test Done avg=%d".format(avg))
  }

}

class myThread(input: File, output: File, bar: CyclicBarrier, id: Int) extends Runnable with DirectLogging {
  def run = {
    try {
      val clib = ImageFunctionsFactory.getBayerTifFunctions();
      val imageData = clib.bayerTiffToRgb(input.getAbsolutePath(), output.getAbsolutePath());
      log.info("thread %d Done".format(id))
    } catch {
      case t: Exception ⇒ log.error("Exception %d %s".format(id, t))
    } finally {
      bar.await
    }
  }
}