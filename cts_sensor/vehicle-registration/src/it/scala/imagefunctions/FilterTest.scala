package imagefunctions

/*
 * Copyright © CSC
 * Date: Nov 22, 2010
 * Time: 3:35:44 PM
 */

import org.junit.Test
import org.scalatest.MustMatchers
import org.scalatest.junit.JUnitSuite

/**
 * Test the ImageFunctionsImpl filter functions
 */
class FilterTest extends JUnitSuite with MustMatchers {
  /**
   * Test the RemovePunctuation method.
   */
  @Test
  def filterPunct() {
    LicenseUtil.removeAccentedLetters("123456") must be("123456")
    LicenseUtil.removeAccentedLetters("123?56") must be("123?56")
    LicenseUtil.removeAccentedLetters("abcdef") must be("ABCDEF")
    LicenseUtil.removeAccentedLetters("ABCDEF") must be("ABCDEF")
    LicenseUtil.removeAccentedLetters("123:HJ") must be("123HJ")
    LicenseUtil.removeAccentedLetters("12cD?") must be("12CD?")
    LicenseUtil.removeAccentedLetters("12 cD") must be("12CD")
    LicenseUtil.removeAccentedLetters("12 :@cD!'") must be("12CD")
  }
}