package imagefunctions

/*
 * Copyright © CSC
 * Date: Jun 8, 2010
 * Time: 12:18:11 PM
 */

import imagefunctions.arh.impl.ArhFunctionsImpl
import org.junit.Test
import org.scalatest.junit.JUnitSuite

/**
 * Test the call to the ImageFunction implementation.
 */
class PlateNumberTest extends JUnitSuite {
  /**
   * Test the stripping and removal of punctuation of the platenumber.
   */
  @Test
  def testPlateNumberConversion {
    var license = LicenseUtil.removeAccentedLetters("KL:33KL");
    assert(license == "KL33KL", "license");

    license = LicenseUtil.removeAccentedLetters("KL:33KL");
    assert(license == "KL33KL", "license");

    license = LicenseUtil.removeAccentedLetters("KL:33 KL  ");
    assert(license == "KL33KL", "license");

    license = LicenseUtil.removeAccentedLetters("    KL:33K:;'',,..#$%!@()*&^%L");
    assert(license == "KL33KL", "license");

  }

  /**
   * Test the removal of accented letters of the platenumber.
   */
  @Test
  def testPlateNumberConversionAccented {

    ImageFunctionsFactory.setLicenceFormat(LicenseFormat.BASE_CHAR)
    var license = LicenseUtil.removeAccentedLetters("Jos\u00E9"); // using a composed é;
    assert(license == "JOSE", "license (" + license + ") is not JOSE");

    license = LicenseUtil.removeAccentedLetters("\u00FC\u00DC"); // using a composed u umlaut;
    assert(license == "UU", "license (" + license + ") is not UU");

    ImageFunctionsFactory.setLicenceFormat(LicenseFormat.UMLAUTS)
    license = LicenseUtil.removeAccentedLetters("\u00FC\u00DC"); // using a composed u umlaut;
    assert(license == "ÜÜ", "license (" + license + ") is not ÜÜ");
  }

  /**
   * Test just some random ARH Plate authorities.
   */
  @Test
  def testARHCountryCodeConversion {
    val arhFunc = ArhFunctionsImpl.getDefaultInstance
    var countryCode = arhFunc.convertARHPlateAuthorityToCountryCode(1240);
    assert(countryCode == LicensePlate.PLATE_NETHERLANDS, "licenseplate not NL");

    countryCode = arhFunc.convertARHPlateAuthorityToCountryCode(0);
    assert(countryCode == LicensePlate.PLATE_UNKNOWN, "LP not unknown");

    countryCode = arhFunc.convertARHPlateAuthorityToCountryCode(99999);
    assert(countryCode == LicensePlate.PLATE_UNKNOWN, "LP not unknown");

    countryCode = arhFunc.convertARHPlateAuthorityToCountryCode(1900);
    assert(countryCode == LicensePlate.PLATE_SLOVENIA, "LP not slovenia");

    countryCode = arhFunc.convertARHPlateAuthorityToCountryCode(8400);
    assert(countryCode == LicensePlate.PLATE_KOSOVO, "LP not kosovo");

    countryCode = arhFunc.convertARHPlateAuthorityToCountryCode(9900);
    assert(countryCode == LicensePlate.PLATE_UNKNOWN, "LP not unknown");

  }

}