/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package tools

import java.io.File
import java.util.Date

import csc.vehicle.tools.vehicle.ModifyBayerTiff
import imagefunctions.ImageFunctionsFactory
import org.apache.commons.io.FileUtils
import org.scalatest.WordSpec
import org.scalatest._
import testframe.Resources

class ModifyBayerTiffTest extends WordSpec with MustMatchers {
  ImageFunctionsFactory.setDefaultFunctions()

  "Modify" must {
    "update time gray" in {
      val imageGray = Resources.getResourceFile("100000BE500_11.tif")
      val now = new Date()
      val image = ModifyBayerTiff.replaceImageCreationTime(imageGray, now)
      val updatedImage = new File(imageGray.getParentFile, "100000BE500_11_mod.tif")
      FileUtils.writeByteArrayToFile(updatedImage, image)
      val sensorLib = ImageFunctionsFactory.getBayerTifFunctions
      val result = sensorLib.getTifMetaData(updatedImage.getAbsolutePath)
      result.getImageDateTime must be(now)
    }
    "update time color" in {
      val imageColor = Resources.getResourceFile("alfa.tif")
      val now = new Date()
      val image = ModifyBayerTiff.replaceImageCreationTime(imageColor, now)
      val updatedImage = new File(imageColor.getParentFile, "alfa_mod.tif")
      FileUtils.writeByteArrayToFile(updatedImage, image)
      val sensorLib = ImageFunctionsFactory.getBayerTifFunctions
      val result = sensorLib.getTifMetaData(updatedImage.getAbsolutePath)
      result.getImageDateTime must be(now)
    }
  }
}