package csc.recognizer

import imagefunctions.LicensePlate
import org.scalatest._

class imageRecognitionStubTest extends WordSpecLike with MustMatchers {

  "ImageRecognition stub" must {
    "extract license plate from valid absolute filePath" in {
      val filePath = "/some/path/1449494904823.76_SV_PP-Car-NL-87-74-A4-E1-12123333111.jpg"
      val license = new ImageRecognitionStub().bayerToArh(filePath)
      license.getPlateNumber must be("76SVPP")
    }

    "extract license plate from valid relative filePath with" in {
      val filePath = "1449494904823.76_SV_PP-Car-NL-87-74-A4-E1-12123333111.jpg"
      val license = new ImageRecognitionStub().bayerToArh(filePath)
      license.getPlateNumber must be("76SVPP")
    }

    "fail when the license plate is too short" in {
      val filePath = "/some/path/1449494904823.SV_PP-Car-NL-87-74-A4-E1-12123333111.jpg"
      an[IllegalArgumentException] shouldBe thrownBy {
        new ImageRecognitionStub().bayerToArh(filePath)
      }
    }

    "fail when confidence value is out of bounds" in {
      val filePath = "/some/path/1449494904823.76_SV_PP-Car-NL-1000-100-A4-E112123333111.jpg"
      an[IllegalArgumentException] shouldBe thrownBy {
        new ImageRecognitionStub().bayerToArh(filePath)
      }
    }

    "handle noplate" in {
      val filePath = "534535345noplate434.jpg"
      val license = new ImageRecognitionStub().jpgToArh(filePath)
      license.getPlateStatus must be(LicensePlate.PLATESTATUS_NO_PLATE_FOUND)
    }

  }

}
