/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.recognizer

import org.scalatest.WordSpec
import org.scalatest._
import java.io.File

class BlackListARHTest extends WordSpec with MustMatchers {

  def getFile(name: String): File = {
    new File(getClass.getClassLoader.getResource(name).getFile)
  }

  "BlackList parser" must {
    "parse file with one line" in {
      val file = getFile("csc/recognizer/oneLine.txt")
      val result = BlackListARH.parse(file)
      val expectedResult = Array(10, 20, 30, 40, 50, 60, 70, 80, 90)
      result must be(expectedResult)
    }
    "parse file with one integer per line" in {
      val file = getFile("csc/recognizer/newline.txt")
      val result = BlackListARH.parse(file)
      val expectedResult = Array(10, 20, 30, 40, 50)
      result must be(expectedResult)
    }
    "parse file with comment on line" in {
      val file = getFile("csc/recognizer/comment.txt")
      val result = BlackListARH.parse(file)
      val expectedResult = Array(10)
      result must be(expectedResult)
    }
    "parse file with comment and integer" in {
      val file = getFile("csc/recognizer/combined.txt")
      val result = BlackListARH.parse(file)
      val expectedResult = Array(10, 20, 30, 70)
      result must be(expectedResult)
    }
    "parse empty file" in {
      val file = getFile("csc/recognizer/empty.txt")
      val result = BlackListARH.parse(file)
      result must be(Array())
    }
    "parse file with errors" in {
      val file = getFile("csc/recognizer/error.txt")
      val result = BlackListARH.parse(file)
      val expectedResult = Array(10, 20, 30)
      result must be(expectedResult)
    }
  }

}