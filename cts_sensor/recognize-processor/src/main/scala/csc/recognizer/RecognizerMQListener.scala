package csc.recognizer

import akka.actor.ActorRef
import csc.amqp.adapter.{ HeartbeatAwareMQProducer, MQListener, MQProducer }
import csc.amqp.adapter.MQProducer._
import csc.image.recognize._
import csc.json.lift.EnumerationSerializer
import csc.vehicle.message.{ ImageFormat, Priority }
import net.liftweb.json.DefaultFormats

import scala.concurrent.duration.FiniteDuration

/**
 * Created by carlos on 01/09/16.
 */
object RecognizerMQListener {

  val formats = DefaultFormats + new EnumerationSerializer(Priority, ImageFormat)

}
class RecognizerMQListener(val handler: ActorRef, val producer: ActorRef)
  extends MQListener with RecognizerFormats {

  override type Request = RecognizeImageRequest
  override type Response = RecognizeImageResponse

  object ImageRequestExtractor extends MessageExtractor[RecognizeImageRequest]

  override def ownReceive: Receive = {
    case delivery @ ImageRequestExtractor(msg) ⇒ handleRequest(msg, delivery)
    case response: RecognizeImageResponse      ⇒ handleResponse(response)
  }
}

class RecognizerMQProducer(val producer: ActorRef,
                           val producerEndpoint: String,
                           val handler: ActorRef,
                           val heartbeatFreq: FiniteDuration)
  extends HeartbeatAwareMQProducer with RecognizerFormats {

  override def componentName: String = "recognize-processor"

  override def routeKeyFunction: RouteKeyFunction = MQProducer.prefixedRoute("system")

}

