/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.recognizer

import java.io.File
import org.apache.commons.io.FileUtils
import java.util.StringTokenizer
import org.slf4j.LoggerFactory

object BlackListARH {
  def parse(blackListFile: File): Array[Int] = {
    val fileContent = FileUtils.readFileToString(blackListFile)
    val lines = fileContent.split("\n")
    lines.foldLeft(Seq[Int]()) {
      case (list, line) ⇒ {
        val cleanLine = filterComment(line)
        val tokenizer = new StringTokenizer(cleanLine, ",;:")
        var lineList = List[Int]()
        while (tokenizer.hasMoreTokens) {
          val trim = tokenizer.nextToken().trim
          try {
            lineList = lineList :+ trim.toInt
          } catch {
            case p: NumberFormatException ⇒ {
              val log = LoggerFactory.getLogger(BlackListARH.getClass)
              log.warn("Could not parse [%s] to integer".format(trim), 0)
            }
          }
        }
        list ++ lineList
      }
    }.toArray
  }

  def filterComment(line: String): String = {
    val commentPos1 = line.indexOf("#")
    val tmp = if (commentPos1 >= 0) {
      line.substring(0, commentPos1)
    } else {
      line
    }
    val commentPos2 = tmp.indexOf("\\\\")
    if (commentPos2 >= 0) {
      tmp.substring(0, commentPos2)
    } else {
      tmp
    }
  }
}