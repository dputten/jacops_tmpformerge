/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.recognizer

import base.FavorCriticalProcess
import csc.akka.logging.DirectLogging
import akka.kernel.Bootable
import akka.actor.{ ActorRef, Props, ActorSystem }
import akka.routing.{ RoundRobinRouter, FromConfig }
import csc.amqp.{ ServicesConfiguration, AmqpConfiguration, AmqpConnection }
import csc.image.recognize._
import csc.recognizer.ProcessPipe.ProcessPipe
import csc.vehicle.message.ImageFormat
import imagefunctions.{ LicenseFormat, ImageFunctionsFactory }
import imagefunctions.arh.impl.ArhFunctionsImpl
import com.typesafe.config.{ Config, ConfigFactory }
import java.io.{ FileFilter, File }
import csc.akka.config.ConfigUtil._

import imagefunctions.intrada.{ INTRADA_CONFIG }
import imagefunctions.intrada.impl.IntradaFunctionsImpl

/**
 * Boot class Starts RecognizeProcessor
 */
class Boot extends Bootable with DirectLogging {

  val rootConfig = "recognize-processor"

  val config = ConfigFactory.load("recognize")
  implicit val actorSystem = ActorSystem("RecognizeProcessor", config)

  def startup() {
    log.info("Start RecognizeProcessor")
    log.info("RecognizeProcessor Boot config: {}", config)
    //start all support actors
    //this is a bad idea (causing problems to POCICR) which is not even needed, as individual components will be initialized later
    //ImageFunctionsFactory.setDefaultFunctions()
    ImageFunctionsFactory.setLicenceFormat(LicenseFormat.UMLAUTS)

    val processPipeStr = config.getString(rootConfig + ".processType").toUpperCase
    //Stub system property has precedence over the config
    val processPipe = if (isStubEnabled) ProcessPipe.STUB else ProcessPipe.valueOf(processPipeStr)

    bootImageRecognition(processPipe)

    val recogActor = processPipe match {
      case ProcessPipe.ARH_ONLY                ⇒ createArhOnlyPipe()
      case ProcessPipe.INTRADA_ONLY            ⇒ createIntradaOnlyPipe()
      case ProcessPipe.ARH_INTRADA             ⇒ createArhIntradaPipe()
      case ProcessPipe.ARH_INTRADA_PLATEFINDER ⇒ createArhIntradaPlateFinderPipe()
      case ProcessPipe.PLATEREADER             ⇒ createPlatereaderPipe()
      case ProcessPipe.STUB                    ⇒ createStubPipe()
      case _                                   ⇒ createArhOnlyPipe()
    }

    launchRequestListener(recogActor)
  }

  def launchRequestListener(handler: ActorRef): Unit = {

    val cfg = new ServicesConfiguration(config.getConfig(rootConfig + ".recognitionServices"))
    val con = new AmqpConnection(new AmqpConfiguration(config.getConfig(rootConfig + ".amqp")))
    lazy val heartbeatFreq = config.getFiniteDuration(rootConfig + ".heartbeatFrequency")

    val producer = actorSystem.actorOf(Props(
      new RecognizerMQProducer(con.connection.producer, cfg.producerEndpoint, handler, heartbeatFreq)))

    val listener = actorSystem.actorOf(Props(new RecognizerMQListener(handler, producer)).withRouter(RoundRobinRouter(nrOfInstances = cfg.serversCount)))
    con.queue(cfg).wireConsumer(listener, false) //no auto ack for recognition requests
  }

  def isStubEnabled: Boolean = {
    val IMAGE_RECOGNITION_STUB_ENABLED = "imageRecognitionStubEnabled"
    "true".equalsIgnoreCase(System.getProperty(IMAGE_RECOGNITION_STUB_ENABLED))
  }

  def bootImageRecognition(processPipe: ProcessPipe) = {

    val intradaEnabled = processPipe.toString.contains("INTRADA")
    val arhEnabled = processPipe.toString.contains("ARH")

    if (arhEnabled) {
      log.info("Using ARH for image recognition.")
      configArh()
    }

    if (intradaEnabled) {
      configIntrada()
    }
  }

  def configIntrada() {
    val company = config.getString(rootConfig + ".intrada.company")
    val dacKey = config.getString(rootConfig + ".intrada.dacKey")
    val settingsFile = config.getString(rootConfig + ".intrada.settingsFile")

    val cfg = new INTRADA_CONFIG.ByReference

    val bytes = (company + "\0").getBytes()
    var i: Int = 0
    bytes.foreach(byte ⇒ {
      cfg.company(i) = byte
      i += 1
    })
    cfg.dacOptions(0) = 83.toByte
    cfg.dacOptions(1) = 0.toByte

    val bytesKey = (dacKey + "\0").getBytes()
    i = 0
    bytesKey.foreach(byte ⇒ {
      cfg.dacKey(i) = byte
      i += 1
    })

    val bytesFile = (settingsFile + "\0").getBytes()
    i = 0
    bytesFile.foreach(byte ⇒ {
      cfg.settingFile(i) = byte
      i += 1
    })

    val intrada = IntradaFunctionsImpl.getInstance(cfg)
    ImageFunctionsFactory.setIntradaFunctions(intrada)
  }

  def configArh() {
    val arhPoolSize = config.getInt(rootConfig + ".arh.poolsize")
    val arhModule = config.getString(rootConfig + ".arh.module")
    val arhGroup = config.getString(rootConfig + ".arh.group")

    //Initialize the ARH library
    val arhImpl = ArhFunctionsImpl.getInstance(arhPoolSize, arhModule, arhGroup, false, true)
    val arhBlackList = config.getString(rootConfig + ".arh.blackList")
    if (!arhBlackList.isEmpty) {
      val file = new File(arhBlackList)
      if (file.exists() && file.isFile) {
        val blackList = BlackListARH.parse(file)
        arhImpl.setFilter(blackList.map(new java.lang.Integer(_)))
      } else {
        log.error("BlackList [%s] does not exists or is not a file".format(file.getAbsolutePath))
      }
    }
    ImageFunctionsFactory.setArhFunctions(arhImpl)
  }

  def createArhOnlyPipe(): ActorRef = {
    val recognizer = new ArhRecognizer()
    testImageRun(recognizer)
    actorSystem.actorOf(
      Props(new RecognizeImage(recognizer = recognizer, sectionGuard = guardARH, nextStep = None)).withRouter(FromConfig()),
      "recognize")
  }

  def createIntradaOnlyPipe(): ActorRef = {
    val recognizer = new IntradaRecognizer()
    actorSystem.actorOf(
      Props(new RecognizeImage(recognizer = recognizer, sectionGuard = guardIntrada, nextStep = None)).withRouter(FromConfig()),
      "recognize")
  }

  def createArhIntradaPipe(): ActorRef = {
    val merge = actorSystem.actorOf(Props[MergeARHIntrada], "mergeRecognizers")

    val intradaRecognizer = new IntradaRecognizer()
    val intrada = actorSystem.actorOf(
      Props(new RecognizeImage(recognizer = intradaRecognizer, sectionGuard = guardIntrada, nextStep = Some(merge))).withRouter(FromConfig()),
      "secondRecognize")

    val routeCheck = actorSystem.actorOf(Props(new CheckNeedRecognizer(intrada)), "CheckNeedRecognizer")

    val ArhRecognizer = new ArhRecognizer()
    actorSystem.actorOf(
      Props(new RecognizeImage(recognizer = ArhRecognizer, sectionGuard = guardARH, nextStep = Some(routeCheck))).withRouter(FromConfig()),
      "recognize")

  }

  def guardARH = guardFor("arh")

  def guardIntrada = guardFor("intrada")

  def guardPlatefinder = guardFor("platefinder")

  def guardPlatereader = guardFor("platereader")

  def guardFor(name: String): Option[FavorCriticalProcess] = {
    val configPath = rootConfig + "." + name + ".poolsize"
    Some(new FavorCriticalProcess(config.getInt(configPath)))
  }

  def createArhIntradaPlateFinderPipe(): ActorRef = {

    val merge = actorSystem.actorOf(Props[MergeARHIntrada], "mergeRecognizers")

    val plateFinder = actorSystem.actorOf(
      Props(new FindPlateImage(sectionGuard = guardPlatefinder, nextStep = Some(merge), plateFinder = PlateFinder)).withRouter(FromConfig()),
      "plateFinder")

    val plateFinderCheck = actorSystem.actorOf(Props(new CheckNeedPlateFinder(plateFinder, merge)), "CheckNeedPlateFinder")

    val intradaRecognizer = new IntradaRecognizer()
    val intrada = actorSystem.actorOf(
      Props(new RecognizeImage(recognizer = intradaRecognizer, sectionGuard = guardIntrada, nextStep = Some(plateFinderCheck))).withRouter(FromConfig()),
      "secondRecognize")

    val routeCheck = actorSystem.actorOf(Props(new CheckNeedRecognizer(intrada)), "CheckNeedRecognizer")

    val ArhRecognizer = new ArhRecognizer()
    actorSystem.actorOf(
      Props(new RecognizeImage(recognizer = ArhRecognizer, sectionGuard = guardARH, nextStep = Some(routeCheck))).withRouter(FromConfig()),
      "recognize")

  }

  private def nativeDebug: Boolean =
    try {
      "true".equalsIgnoreCase(config.getString(rootConfig + ".nativeDebug"))
    } catch {
      case ex: Exception ⇒ false
    }

  private def testImageFolder: Option[File] = {
    try {
      Some(new File(config.getString(rootConfig + ".testImageFolder")))
    } catch {
      case ex: Exception ⇒ None
    }
  }

  private def testImageRun(recognizer: Recognizer): Unit = {

    val fileFilter: FileFilter = new FileFilter {
      override def accept(file: File): Boolean = file.isFile && file.getName.endsWith(".jpg")
    }

    testImageFolder foreach { folder ⇒
      val files = folder.listFiles(fileFilter).sortBy(_.getName)
      for (f ← files) {
        try {
          val plate = recognizer.recognizeImage(f, ImageFormat.JPG_RGB, Map.empty)
          log.info("Plate result: image {} status {} number {} confidence {}", f.getName, plate.getPlateStatus, plate.getPlateNumber, plate.getPlateConfidence)
        } catch {
          case ex: Exception ⇒ log.error(ex, "Error recognizing image " + f.getName)
        }
      }
    }
  }

  def createPlatereaderPipe(): ActorRef = {
    val recognizer = new IcrRecognizer(nativeDebug)
    testImageRun(recognizer)
    actorSystem.actorOf(
      //PCL-114: ICR not capable of running more than one recognition at any time
      Props(new RecognizeImage(recognizer = recognizer, sectionGuard = guardPlatereader,
        nextStep = None)), "recognize") //.withRouter(FromConfig()), "recognize")
  }

  def createStubPipe(): ActorRef = {
    val recognizer = new ImageRecognitionStub
    actorSystem.actorOf(
      Props(new RecognizeImage(recognizer = recognizer, sectionGuard = guardARH, nextStep = None)))
  }

  def shutdown() {
    log.info("Stop RecognizeProcessor")
    actorSystem.shutdown()
  }
}

object ProcessPipe extends Enumeration {
  type ProcessPipe = Value
  val ARH_ONLY, INTRADA_ONLY, ARH_INTRADA, ARH_INTRADA_PLATEFINDER, PLATEREADER, STUB = Value

  def valueOf(str: String): ProcessPipe = withName(str.replace('-', '_'))
}

