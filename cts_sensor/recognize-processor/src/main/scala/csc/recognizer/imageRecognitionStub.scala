package csc.recognizer

import java.io.File

import com.typesafe.config.ConfigFactory
import csc.akka.logging.DirectLogging
import csc.image.recognize.Recognizer
import csc.vehicle.VehicleInfo
import csc.vehicle.message.ImageFormat
import imagefunctions.{ Point, LicensePlate }
import imagefunctions.arh.impl.ArhFunctionsEmpty

/**
 * Image recognizer stub.
 * Can be used as an ArhFunctions impl or directly as a Recognizer impl
 */
class ImageRecognitionStub extends ArhFunctionsEmpty with DirectLogging with Recognizer {

  log.info(this.getClass.getSimpleName + " instantiated.")

  val cfg = ConfigFactory.load("recognize")

  override def rgbToArh(filePath: String, snippetFileName: String, isLastLicensePlate: Boolean, moduleName: String, groupName: String) =
    recognizeStub(filePath)

  override def bayerToArh(filePath: String, snippetFileName: String, isLastLicensePlate: Boolean, moduleName: String, groupName: String) =
    recognizeStub(filePath)

  override def jpgToArh(filePath: String, snippetFileName: String, isLastLicensePlate: Boolean, moduleName: String, groupName: String) =
    recognizeStub(filePath)

  override def rgbToArh(filePath: String) = recognizeStub(filePath)

  override def bayerToArh(filePath: String) = recognizeStub(filePath)

  override def jpgToArh(filePath: String) = recognizeStub(filePath)

  /**
   * Do the actual recognition of the license plate.
   * @param imageFile the images used to recognize the license from.
   * @param format indicates the type of the image
   */
  override def recognizeImage(imageFile: File, format: ImageFormat.Value, options: Map[String, String]) =
    jpgToArh(imageFile.getPath)

  override def stop(): Unit = ()

  /**
   * get the name of the Recognizer.
   */
  override def getName(): String = "STUB"

  /**
   * Get the performance points used in the logging.
   */
  override def getPerformancePoints(): (Int, Int) = (33, 34)

  private def recognizeStub(filePath: String) = {
    log.info("Reading image recognition stub input from file " + filePath)
    val recognizedStubData = VehicleInfo(filePath)
    log.debug("Split file name into stub input " + recognizedStubData)
    createPlate(recognizedStubData)
  }

  private def createPlate(input: Option[VehicleInfo]): LicensePlate = {

    input match {
      case None ⇒ Recognizer.noPlate(getName())

      case Some(recognizedStubData) ⇒
        val plate = new LicensePlate()
        plate.setLicenseSnippetFileName("")
        val rootconfig = "recognize-processor"
        plate.setRecognizer("ARH")
        plate.setReturnStatus(0)
        plate.setPlateStatus(LicensePlate.PLATESTATUS_PLATE_FOUND)
        plate.setPlateConfidence(recognizedStubData.confidence)
        plate.setPlateCountryCode(recognizedStubData.country)
        plate.setRawPlateNumber(recognizedStubData.plate)
        plate.setPlateNumber(recognizedStubData.plate.split("_").mkString)
        plate.setPlateCountryConfidence(recognizedStubData.countryConfidence)

        val (x, y) = recognizedStubData.platePosition match {
          case Some(pos) ⇒ (pos.getX, pos.getY)
          case None      ⇒ (cfg.getInt(rootconfig + ".arh.stub.xPosOffset"), cfg.getInt(rootconfig + ".arh.stub.yPosOffset"))
        }
        plate.setPlateXOffset(x)
        plate.setPlateYOffset(y)

        if (recognizedStubData.platePosition.isDefined) {
          val width = 200 //reference values, not important
          val height = 100
          plate.setPlateTL(new Point(x, y))
          plate.setPlateTR(new Point(x + width, y))
          plate.setPlateBR(new Point(x + width, y + height))
          plate.setPlateBL(new Point(x, y + height))
        }

        plate.setReturnStatus(cfg.getInt(rootconfig + ".arh.stub.resultValue"))
        plate
    }
  }
}

object Hex {

  def valueOf(buf: Array[Byte]): String = buf.map("%02X" format _).mkString

}
