/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.tools.startup

import csc.akka.logging.DirectLogging
import akka.actor.{ Props, ActorSystem }
import csc.tools.config.ToolsConfiguration
import csc.tools.server.MinaTestServer

/**
 * Helper to start the MinaServer process
 */
object StartupToolsMinaServer extends DirectLogging {

  def startToolsMinaServer(config: ToolsConfiguration, actorSystem: ActorSystem) {
    log.info("Starting MinaServer process")

    actorSystem.actorOf(Props(
      new MinaTestServer(host = config.minaEndpoint.host, port = config.minaEndpoint.port)),
      "MinaTestServer")

    log.info("MinaServer process started")
  }
}
