/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.tools.startup

import csc.akka.logging.DirectLogging
import akka.kernel.Bootable
import csc.tools.config.ToolsConfiguration
import akka.actor.ActorSystem

/**
 * Boot class starts MinaServer for test
 */
private[startup] class Boot extends Bootable with DirectLogging {
  val actorSystem = ActorSystem("Tools")

  def startup() {
    val config = ToolsConfiguration()
    log.info("Starting Tools-%s".format(config.toolToStart))
    config.toolToStart match {
      case "minaClient" ⇒ StartupToolsMinaClient.startToolsMinaClient(ToolsConfiguration(), actorSystem)
      case "minaServer" ⇒ StartupToolsMinaServer.startToolsMinaServer(ToolsConfiguration(), actorSystem)
    }

  }

  def shutdown() {
    (new ShutdownHook(actorSystem)) run ()
  }
}