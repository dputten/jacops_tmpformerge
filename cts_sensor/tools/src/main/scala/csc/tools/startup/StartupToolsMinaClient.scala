/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.tools.startup

import csc.akka.logging.DirectLogging
import akka.actor.{ Props, ActorSystem }
import csc.tools.config.ToolsConfiguration
import csc.tools.server.MinaTestServer
import csc.tools.client.MinaSender

/**
 * Helper to start the MinaClient process
 */
object StartupToolsMinaClient extends DirectLogging {

  def startToolsMinaClient(config: ToolsConfiguration, actorSystem: ActorSystem) {
    log.info("Starting MinaClient process")

    val sender = actorSystem.actorOf(Props(new MinaSender(config.minaEndpoint.host, config.minaEndpoint.port)), "MinaSender")

    sender ! "Dit is het eerste bericht"
    sender ! "Dit is het tweede bericht"
    sender ! "Dit is het derde bericht"

    log.info("MinaClient process started ... and messages have been sent")
  }
}
