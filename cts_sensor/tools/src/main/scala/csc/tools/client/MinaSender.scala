package csc.tools.client

import akka.actor.{ Props, ActorLogging, Actor }
import akka.camel.Producer

/**
 * Actor that sends message to a Camel mina producer.
 */
class MinaSender(host: String, port: Int) extends Actor with ActorLogging {

  val producerActor = context.actorOf(Props(new MinaTestProducer(host, port)), "MinaTestProducer")
  def receive = {
    case message: String ⇒
      log.info("Sender received string [%s]".format(message))
      log.info("       and will send this to producer")
      producerActor ! message
  }
}

class MinaTestProducer(host: String, port: Int) extends Actor with Producer with ActorLogging {
  def endpointUriTemplate = "mina:tcp://%s:%d?textline=true&sync=true"
  def endpointUri = endpointUriTemplate.format(host, port)

}