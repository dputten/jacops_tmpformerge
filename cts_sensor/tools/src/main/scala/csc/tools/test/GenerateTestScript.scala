package csc.tools.test

import java.io.{ FileWriter, BufferedWriter, FileFilter, File }

import scala.io.Source

/**
 * Generates a Jacops-POC (each gantry has one camera for two lanes) test script, given the baseFolder and 2 sub-folders,
 * matching exactly the entry and exit camera ids, and containing the photos.
 * The photos are expected to have the original filename from camera, that is, it should have the datetime on it.
 * This datetime will determine the order in which the registrations will be placed in the script.
 *
 * Created by carlos on 09/03/16.
 */
object GenerateTestScript {

  val filenamePattern = """(\d+)\.(\d+)\.(\d+)\.(\d+)\.jpg""".r
  val defaultTargetBasePath = "/home/ctes/testscripts"
  val registrationFormat = "1500 LANE-REGISTRATION LANE-ID=%s SPEED=100 CATEGORY=CAR IMAGE=%s"
  val laneFormat = "CAMERA ID=%s HOST=localhost FTP-PORT=2121 FTP-USER=ctes FTP-PWD=kosterij"
  val corridorPattern = "CORRIDOR CORRIDOR-ID=Section1.1 START-LANE-ID=%s END-LANE-ID=%s CORRIDOR-LENGTH=1000"

  def main(args: Array[String]) {
    val baseFolder = new File(args(0))
    val entryCam = args(1)
    val exitCam = args(2)
    val targetBasePath = if (args.size > 3) args(3) else defaultTargetBasePath

    val entryFolder = new File(baseFolder, entryCam)
    val entryFiles = getAllFiles(entryFolder)
    val entryTime = getTime(entryFiles(0).getName)

    val exitFolder = new File(baseFolder, exitCam)

    val allImages: List[Image] = exitFolder.exists() match {
      case true ⇒
        val exitFiles = getAllFiles(exitFolder)
        val exitTime = getTime(exitFiles(0).getName)
        val baseTime = math.min(entryTime, exitTime)
        val entryImages = getAllImages(entryFiles, entryCam, baseTime)
        val exitImages = getAllImages(exitFiles, exitCam, baseTime)
        entryImages ++ exitImages sortBy (_.delta)
      case false ⇒
        val entryImages = getAllImages(entryFiles, entryCam, entryTime)
        entryImages sortBy (_.delta)
    }

    //val allImages = entryImages ++ exitImages sortBy (_.delta)

    val gen = new GenerateTestScript(baseFolder, entryCam, exitCam, targetBasePath)
    val outFile = gen.generateScript(allImages)

    println("Generated script at " + outFile.getAbsolutePath)
  }

  def getAllImages(files: List[File], camId: String, baseTime: Int): List[Image] =
    files map { f ⇒ Image(f, camId, getTime(f.getName) - baseTime) }

  def getTime(filename: String) = filenamePattern.findFirstMatchIn(filename) match {
    case None ⇒ throw new IllegalArgumentException(filename)
    case Some(m) ⇒
      val time = m.group(3)
      val secs1 = time.substring(4, 6).toInt
      val secs2 = time.substring(2, 4).toInt * 60
      val secs3 = time.substring(0, 2).toInt * 60 * 60
      val mills = m.group(4).toInt
      (secs1 + secs2 + secs3) * 1000 + mills
  }

  def getAllFiles(folder: File): List[File] = folder.exists() match {
    case true  ⇒ folder.listFiles(JpgFilter).toList.sortBy(_.getName)
    case false ⇒ Nil
  }
}

object GenerateTestScript2 {

  import GenerateTestScript._

  def main(args: Array[String]) {
    val fileListFile = new File(args(0))
    val cam = args(1)
    val targetBasePath = if (args.size > 2) args(2) else defaultTargetBasePath

    val files: List[File] = Source.fromFile(fileListFile).getLines() map { new File(_) } toList
    val baseTime = getTime(files(0).getName)

    val images = getAllImages(files, cam, baseTime)

    val gen = new GenerateTestScript(new File("/tmp"), cam, "NOEXIT", targetBasePath) {
      override def getImagePath(image: Image): String = image.file.getAbsolutePath
    }
    val outFile = gen.generateScript(new File("/tmp/testset.txt"), images)
    println("Generated script at " + outFile.getAbsolutePath)
  }
}

import GenerateTestScript._

class GenerateTestScript(baseFolder: File, entryCam: String, exitCam: String, targetBasePath: String) {

  def generateScript(images: List[Image]): File = {
    val outFile = new File(baseFolder, baseFolder.getName + ".txt")
    generateScript(outFile, images)
  }

  def generateScript(outFile: File, images: List[Image]): File = {

    val writer = new BufferedWriter(new FileWriter(outFile))

    val lines = getScriptLines(images)

    try {
      lines.foreach { l ⇒
        writer.write(l)
        writer.newLine()
      }
      writer.flush()
    } finally {
      writer.close()
    }

    outFile
  }

  def getImagePath(image: Image) = {
    "%s/%s/%s/%s".format(targetBasePath, baseFolder.getName, image.camId, image.file.getName)
  }

  def getScriptLines(images: List[Image]): List[String] = {
    var result: List[String] = Nil
    result = result :+ "CONFIG-START"
    result = result :+ laneFormat.format(entryCam)
    result = result :+ laneFormat.format(exitCam)
    result = result :+ corridorPattern.format(entryCam, exitCam)
    result = result :+ "CONFIG-END"
    result = result :+ ""
    result = result :+ "SCRIPT-START"

    images.foreach { i ⇒
      val path = getImagePath(i)
      result = result :+ registrationFormat.format(i.camId, path)
    }

    result = result :+ "SCRIPT-END"

    result
  }
}

trait GantryType
case object Entry extends GantryType
case object Exit extends GantryType

case class Image(file: File, camId: String, delta: Int)

object JpgFilter extends FileFilter {
  override def accept(file: File): Boolean = file.isFile && file.getName.endsWith(".jpg")
}

