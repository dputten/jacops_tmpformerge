/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.tools.config

import com.typesafe.config.{ Config, ConfigFactory }
import csc.akka.config.ConfigUtil._

case class HostPortConfig(host: String, port: Int) {

  def this(configPath: String, config: Config) = {
    this(
      config.getReplacedString(configPath + ".host", ""),
      config.getReplacedInteger(configPath + ".port", 0))
  }
}

/**
 * Class holding the configuration for the Tools
 * @param toolToStart, defines which tool will be started
 * @param minaEndpoint, camel Endpoint where the iRose device sends us messages over TCP/IP
 */
case class ToolsConfiguration(toolToStart: String, minaEndpoint: HostPortConfig) {
  def this(configPath: String, config: Config) = {
    this(
      config.getReplacedString(configPath + ".toolToStart", "minaClient"),
      new HostPortConfig(configPath + ".minaEndpoint", config))
  }
}

object ToolsConfiguration {
  def apply() = new ToolsConfiguration("tools", ConfigFactory.load())
  def apply(configPath: String) = {
    val config = ConfigFactory.load()
    if (!config.hasPath(configPath))
      throw new Exception("Path %s does not exist in configuration".format(configPath))
    new ToolsConfiguration(configPath, config)
  }
}

