/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.tools.server

import akka.actor.{ ActorLogging, Actor }
import akka.camel.{ Ack, CamelMessage, Consumer }

/**
 * Simple Camel Consumer actor, which logs every message received to the log
 */
class MinaTestServer(host: String, port: Int) extends Actor with Consumer with ActorLogging {
  def endpointUriTemplate = "mina:tcp://%s:%d?textline=true&sync=true"
  def endpointUri = endpointUriTemplate.format(host, port)

  def receive = {
    case message: CamelMessage ⇒
      log.info("New CamelMessage received:")
      log.info("   Body: " + message.bodyAs[String])
      log.info("   Headers: ")
      message.headers.foreach((header: (String, Any)) ⇒
        log.info("      [%s]: [%s]".format(header._1, header._2)))
      sender ! Ack
  }

}
