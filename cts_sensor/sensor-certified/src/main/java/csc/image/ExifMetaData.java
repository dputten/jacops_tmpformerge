package csc.image;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by carlos on 28/06/16.
 */
public class ExifMetaData {

    private static final String datePatternExif = "yyyy:MM:dd HH:mm:ss";
    private static final String timeZone = "UTC";

    //keeping these fields public, as they are final, and makes the scala-to-java conversion easier
    public final String dateTimeOriginal;
    public final String subSecTimeOriginal;

    public ExifMetaData(String dateTimeOriginal, String subSecTimeOriginal) {
        this.dateTimeOriginal = dateTimeOriginal;
        this.subSecTimeOriginal = subSecTimeOriginal;
    }

    /**
     * Derived property:
     * Return the dateTimeOriginal and the subSecTimeOriginal as a combined long time
     */
    public Long dateTimeOriginalWithSubSecTimeOriginalUtc() throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(datePatternExif);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
        Date date = simpleDateFormat.parse(dateTimeOriginal);
        return date.getTime() + Long.valueOf(subSecTimeOriginal);
    }
}
