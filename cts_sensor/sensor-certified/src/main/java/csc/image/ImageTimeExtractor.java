package csc.image;

import org.apache.commons.io.IOUtils;
import org.apache.sanselan.ImageReadException;
import org.apache.sanselan.Sanselan;
import org.apache.sanselan.SanselanConstants;
import org.apache.sanselan.common.IImageMetadata;
import org.apache.sanselan.formats.jpeg.JpegImageMetadata;
import org.apache.sanselan.formats.tiff.TiffField;
import org.apache.sanselan.formats.tiff.constants.ExifTagConstants;
import org.apache.sanselan.formats.tiff.constants.TagInfo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by carlos on 28/06/16.
 */
public class ImageTimeExtractor {

    /**
     * reads the exif information from the given image bytes and returns the case class filled with the date and te millicseconds
     * @param imageBytes image bytes
     */
    public ExifMetaData readExifMetaData(byte[] imageBytes) throws IOException, ImageReadException {
        IImageMetadata exifMetaData = getMetaData(imageBytes);
        if (exifMetaData != null) {
            String date = readExifField(ExifTagConstants.EXIF_TAG_DATE_TIME_ORIGINAL, exifMetaData);
            String msec = readExifField(ExifTagConstants.EXIF_TAG_SUB_SEC_TIME_ORIGINAL, exifMetaData);
            if (date != null && msec != null) {
                return new ExifMetaData(date, msec);
            }
        }
        return null;
    }

    public ExifMetaData readExifMetaData(File file) throws IOException, ImageReadException {
        return readExifMetaData(getBytes(file));
    }

    public Long getExifImageTime(byte[] imageBytes) throws Exception {
        return getCreationTime(imageBytes);
    }

    public Long getExifImageTime(File file) throws Exception {
        return getCreationTime(getBytes(file));
    }

    private byte[] getBytes(File file) throws IOException {
        InputStream in = new FileInputStream(file);
        try {
            return IOUtils.toByteArray(in);
        } finally {
            in.close();
        }
    }

    /**
     * reads the exif field value from the given image metadata and tag info
     * @param tagInfo tag to obtain
     * @param imageMetadata image metadata
     */
    private String readExifField(TagInfo tagInfo, IImageMetadata imageMetadata) {
        if (imageMetadata instanceof JpegImageMetadata) {
            JpegImageMetadata jpg = (JpegImageMetadata)imageMetadata;
            TiffField field = jpg.findEXIFValue(tagInfo);
            return field == null ? null : removeBeginAndEndQuotes(field.getValueDescription());
        }
        return null;
    }

    /**
     * value from the exif contain enclosing quotes. Remove these if present.
     * @param value string
     */
    private String removeBeginAndEndQuotes(String value) {
        if (value.startsWith("'") && value.endsWith("'")) {
            return value.substring(1, value.length() - 1);
        } else {
            return value;
        }
    }

    private IImageMetadata getMetaData(byte[] imageBytes) throws IOException, ImageReadException {
        Map<String,Boolean> params = new HashMap<String, Boolean>();
        params.put(SanselanConstants.PARAM_KEY_READ_THUMBNAILS, false);
        return Sanselan.getMetadata(imageBytes, params);
    }

    private Long getCreationTime(byte[] imageBytes) throws IOException, ImageReadException, ParseException {
        ExifMetaData md = readExifMetaData(imageBytes);
        return md == null ? null : md.dateTimeOriginalWithSubSecTimeOriginalUtc();
    }

}
