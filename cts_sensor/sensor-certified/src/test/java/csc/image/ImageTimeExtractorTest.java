package csc.image;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import static org.junit.Assert.*;

/**
 * Created by carlos on 28/06/16.
 */
public class ImageTimeExtractorTest {

    private ImageTimeExtractor extractExifMetaData;

    @Before
    public void setUp() {
        extractExifMetaData = new ImageTimeExtractor();
    }

    @Test
    public void testMetadataFromImageWithExif() throws Exception {

        // MetaData with dateTimeOriginal and subSecTimeOriginal
        File file = Resources.getResourceFile("image_with_exif.jpg");
        ExifMetaData exifMetaData = extractExifMetaData.readExifMetaData(file);

        assertNotNull(exifMetaData);
        assertEquals("2014:02:25 13:01:12", exifMetaData.dateTimeOriginal);
        assertEquals("534", exifMetaData.subSecTimeOriginal);

        String datePatternExif = "yyyy:MM:dd HH:mm:ss";
        String timeZone = "Europe/Amsterdam";

        // Check if the CET is one hour later.
        Date date = new Date(exifMetaData.dateTimeOriginalWithSubSecTimeOriginalUtc());

        DateExtensions dateExtensions = new DateExtensions(date);
        assertEquals("2014:02:25 14:01:12", dateExtensions.toString(datePatternExif, timeZone));
        assertEquals(1393333272534L, (long)exifMetaData.dateTimeOriginalWithSubSecTimeOriginalUtc());
    }

    @Test
    public void testMetadataFromImageWithoutExif() throws Exception {
        File input = Resources.getResourceFile("image_without_exif.tif");
        assertNull(extractExifMetaData.readExifMetaData(input));
    }

    @Test
    public void testTimeFromImageWithoutExif() throws Exception {
        assertNull(extractExifMetaData.getExifImageTime(Resources.getResourceFile("image_without_exif.tif")));
    }

    @Test
    public void testExceptionForWrongFileType() throws Exception {
        File file = Resources.getResourceFile("no_image.xml");
        try {
            extractExifMetaData.getExifImageTime(file);
            fail("Expected an exception");
        } catch (Exception ex) {
            //this is ok
        }
    }

}

/**
 * Support class used to get test resources
 */
class Resources {
    private static final String getResourcePath(String resource) {
        return "sensor-certified/src/test/resources/" + resource;
    }

    public static final File getResourceFile(String resource) {
        return new File(getResourcePath(resource));
    }
}

/**
 * Supported utility functions for Date Objects
 */
class DateExtensions {

    private final Date date;

    DateExtensions(Date date) {
        this.date = date;
    }

    /**
   * Create formatted date using given format and timezone
   * @param format: format of the representation of the date
   * @param timeZone: Timezone as String
   */
  public String toString(String format, String timeZone /*= "CET"*/) {
    DateFormat dateFormat = new SimpleDateFormat(format);
    dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
    return dateFormat.format(date);
  }
}

