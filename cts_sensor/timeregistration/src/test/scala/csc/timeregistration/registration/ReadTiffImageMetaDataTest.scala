package csc.timeregistration.registration

import java.util.Date

import _root_.imagefunctions.{ BayerTiffFunctionsTestImpl, ImageFunctionsFactory }
import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import scala.concurrent.duration._
import base.FavorCriticalProcess
import csc.akka.logging.DirectLogging
import csc.base.{ SensorDevice, SensorPosition }
import csc.vehicle.message.{ PeriodRange, RegistrationRelay, SensorEvent, VehicleRegistrationMessage, _ }
import csc.vehicle.registration.events.EventBuilder
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import org.scalatest._

/**
 * Test the ConvertRAW class
 */

class ReadTiffImageMetaDataTest() extends TestKit(ActorSystem("ReadTiffImageMetaDataTest")) with WordSpecLike with DirectLogging with MustMatchers with BeforeAndAfterAll {

  import SensorDevice._
  import SensorPosition._

  val triggerTime = System.currentTimeMillis()

  override def afterAll() {
    try {
      system.shutdown
    } catch {
      case e: Exception ⇒ log.error("COULD NOT SHUTDOWN ACTOR REGISTRY".format(e))
    }
  }

  "ReadTiffImageMetaData" must {

    /**
     * Testing a good scenario
     * This test uses the ImageFunctionsTestImpl which is a mock of the ImageFunctionsTestImpl ImageFunctions implementation.
     * The mock is setup to behave correctly (a good scenario)
     */
    "perform the good scenario" in {
      val testLib = new BayerTiffFunctionsTestImpl(false)
      val imageDate = new Date
      testLib.setImageDate(imageDate)
      ImageFunctionsFactory.setBayerTifFunctions(testLib)
      val probe = TestProbe()
      val readMetaRef = TestActorRef(new ReadTiffImageMetaData(None, EventBuilder.getInstance(Trigger, Camera), false, Set(probe.ref), "DataID=000000FF", ImageMetaDataReader.tiff))
      //create message
      val image = new RegistrationRelay("fileTest")
      val msg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventId = Some("1"),
        eventTimestamp = None,
        event = new SensorEvent(triggerTime, new PeriodRange(triggerTime, triggerTime), "test"),
        priority = Priority.NONCRITICAL,
        files = List(image))

      readMetaRef ! msg
      val expectImage = new RegistrationImage("fileTest", ImageFormat.TIF_BAYER, VehicleImageType.Overview, ImageVersion.Original, imageDate.getTime, imageTimeStamp = Some(imageDate.getTime))
      val expectMsg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventId = Some("1"),
        eventTimestamp = None,
        event = new SensorEvent(imageDate.getTime, new PeriodRange(imageDate.getTime, imageDate.getTime), "Camera"),
        priority = Priority.NONCRITICAL,
        files = List(expectImage))

      probe.expectMsg(500.millis, expectMsg)
    }

    /**
     * Testing a good scenario  with sectionGuard
     * This test uses the ImageFunctionsTestImpl which is a mock of the ImageFunctionsTestImpl ImageFunctions implementation.
     * The mock is setup to behave correctly (a good scenario)
     */
    "perform the good scenario with guard" in {
      val testLib = new BayerTiffFunctionsTestImpl(false)
      val imageDate = new Date
      testLib.setImageDate(imageDate)
      ImageFunctionsFactory.setBayerTifFunctions(testLib)
      val probe = TestProbe()
      val readMetaRef = TestActorRef(new ReadTiffImageMetaData(Option(new FavorCriticalProcess(1)), EventBuilder.getInstance(Trigger, Camera), false, Set(probe.ref), "DataID=000000FF", ImageMetaDataReader.tiff))
      //create message
      val image = new RegistrationRelay("fileTest")
      val msg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventId = Some("1"),
        eventTimestamp = None,
        event = new SensorEvent(triggerTime, new PeriodRange(triggerTime, triggerTime), "test"),
        priority = Priority.NONCRITICAL,
        files = List(image))

      readMetaRef ! msg
      val expectImage = new RegistrationImage("fileTest", ImageFormat.TIF_BAYER, VehicleImageType.Overview, ImageVersion.Original, imageDate.getTime, imageTimeStamp = Some(imageDate.getTime))
      val expectMsg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventId = Some("1"),
        eventTimestamp = None,
        event = new SensorEvent(imageDate.getTime, new PeriodRange(imageDate.getTime, imageDate.getTime), "Camera"),
        priority = Priority.NONCRITICAL,
        files = List(expectImage))
      probe.expectMsg(500.millis, expectMsg)
    }

    /**
     * Testing a failed scenario by letting the CLibrary mock throw an exception.
     *
     */
    "perform the failed scenario throwing CLibrary exception" in {
      ImageFunctionsFactory.setBayerTifFunctions(new BayerTiffFunctionsTestImpl(true))
      val probe = TestProbe()
      val readMetaRef = TestActorRef(new ReadTiffImageMetaData(None, EventBuilder.getInstance(Trigger, Camera), false, Set(probe.ref), "DataID=000000FF", ImageMetaDataReader.tiff))
      //create message
      val image = new RegistrationRelay("fileTest")
      val msg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventId = Some("1"),
        eventTimestamp = None,
        event = new SensorEvent(triggerTime, new PeriodRange(triggerTime, triggerTime), "test"),
        priority = Priority.NONCRITICAL,
        files = List(image))

      readMetaRef ! msg
      val outMsg = probe.expectMsgType[VehicleRegistrationMessage](500.millis)
      outMsg.eventId must be(msg.eventId)
    }

    /**
     * Testing by setting ImageFunctionsFactory.setImageFunctions to null
     */
    "perform with no CLibrary" in {
      log.info("Start testWithNoCLibrary")

      val imageDate = new Date
      val testLib = new BayerTiffFunctionsTestImpl(false)
      testLib.setImageDate(imageDate)
      var success: Boolean = false
      try {

        ImageFunctionsFactory.setBayerTifFunctions(null)
        val probe = TestProbe()
        val readMetaRef = TestActorRef(new ReadTiffImageMetaData(None, EventBuilder.getInstance(Trigger, Camera), false, Set(probe.ref), "DataID=000000FF", ImageMetaDataReader.tiff))

        //create message
        val image = new RegistrationRelay("fileTest")
        val msg = new VehicleRegistrationMessage(
          lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
          eventId = Some("1"),
          eventTimestamp = None,
          event = new SensorEvent(triggerTime, new PeriodRange(triggerTime, triggerTime), "test"),
          priority = Priority.NONCRITICAL,
          files = List(image))

        readMetaRef ! msg
        fail("expected: RuntimeException")
      } catch {
        case ane: RuntimeException ⇒ success = true
        case e: Exception          ⇒ fail("wrong exception thrown, expected: ArgumentNullException", e)
      }
      log.info("End testWithNoCLibrary")
      assert(success)
    }

    /**
     *
     * Testing msg with no image
     */
    "perform with no image" in {
      val testLib = new BayerTiffFunctionsTestImpl(false)
      val imageDate = new Date
      testLib.setImageDate(imageDate)
      ImageFunctionsFactory.setBayerTifFunctions(testLib)
      val probe = TestProbe()
      val readMetaRef = TestActorRef(new ReadTiffImageMetaData(None,
        EventBuilder.getInstance(Trigger, Camera), false, Set(probe.ref), "DataID=000000FF", ImageMetaDataReader.tiff))
      //create message
      val msg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventId = Some("1"),
        eventTimestamp = None,
        event = new SensorEvent(triggerTime, new PeriodRange(triggerTime, triggerTime), "test"),
        priority = Priority.NONCRITICAL,
        files = List())

      readMetaRef ! msg
      val outMsg = probe.expectMsgType[VehicleRegistrationMessage](500 millis)
      //message should be the same except event is filled
      outMsg.lane must be(msg.lane)
      outMsg.eventId must be(msg.eventId)
      outMsg.priority must be(msg.priority)
      outMsg.files must be(msg.files)
      outMsg.event.triggerSource must be("Camera")
    }
    /**
     * Testing a receiving testImage
     * This test uses the ImageFunctionsTestImpl which is a mock of the ImageFunctionsTestImpl ImageFunctions implementation.
     * The mock is setup to behave correctly (a good scenario)
     */
    "perform using test image" in {
      val testLib = new BayerTiffFunctionsTestImpl(false)
      val imageDate = new Date
      testLib.setImageDate(imageDate)
      testLib.setImageDescription("DataID=000000FF")
      ImageFunctionsFactory.setBayerTifFunctions(testLib)
      val probe = TestProbe()
      val readMetaRef = TestActorRef(new ReadTiffImageMetaData(None, EventBuilder.getInstance(Trigger, Camera), false, Set(probe.ref), "DataID=000000FF", ImageMetaDataReader.tiff))
      //create message
      val image = new RegistrationRelay("fileTest")
      val msg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventId = Some("1"),
        eventTimestamp = None,
        event = new SensorEvent(triggerTime, new PeriodRange(triggerTime, triggerTime), "test"),
        priority = Priority.NONCRITICAL,
        files = List(image))

      readMetaRef ! msg

      probe.expectNoMsg(5.seconds)
    }

    /**
     * Testing a receiving testImage
     * This test uses the ImageFunctionsTestImpl which is a mock of the ImageFunctionsTestImpl ImageFunctions implementation.
     * The mock is setup to behave correctly (a good scenario)
     */
    "perform using test image with reg ex" in {
      val testLib = new BayerTiffFunctionsTestImpl(false)
      val imageDate = new Date
      testLib.setImageDate(imageDate)
      testLib.setImageDescription("DataID=000000FF")
      ImageFunctionsFactory.setBayerTifFunctions(testLib)
      val probe = TestProbe()
      val readMetaRef = TestActorRef(new ReadTiffImageMetaData(None, EventBuilder.getInstance(Trigger, Camera), false, Set(probe.ref), "DataID=[A-F0-9]+", ImageMetaDataReader.tiff))
      //create message
      val image = new RegistrationRelay("fileTest")
      val msg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventId = Some("1"),
        eventTimestamp = None,
        event = new SensorEvent(triggerTime, new PeriodRange(triggerTime, triggerTime), "test"),
        priority = Priority.NONCRITICAL,
        files = List(image))

      readMetaRef ! msg

      probe.expectNoMsg(5.seconds)
    }

    /**
     * Testing a good scenario with regex
     * This test uses the ImageFunctionsTestImpl which is a mock of the ImageFunctionsTestImpl ImageFunctions implementation.
     * The mock is setup to behave correctly (a good scenario)
     */
    "perform normal image with reg ex" in {
      val testLib = new BayerTiffFunctionsTestImpl(false)
      val imageDate = new Date
      testLib.setImageDate(imageDate)
      testLib.setImageDescription("DataID=000000ff")
      ImageFunctionsFactory.setBayerTifFunctions(testLib)
      val probe = TestProbe()
      val readMetaRef = TestActorRef(new ReadTiffImageMetaData(None, EventBuilder.getInstance(Trigger, Camera), false, Set(probe.ref), "DataID=[A-F0-9]+", ImageMetaDataReader.tiff))
      //create message
      val image = new RegistrationRelay("fileTest")
      val msg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventId = Some("1"),
        eventTimestamp = None,
        event = new SensorEvent(triggerTime, new PeriodRange(triggerTime, triggerTime), "test"),
        priority = Priority.NONCRITICAL,
        files = List(image))

      readMetaRef ! msg
      val expectImage = new RegistrationImage("fileTest", ImageFormat.TIF_BAYER, VehicleImageType.Overview, ImageVersion.Original, imageDate.getTime, imageTimeStamp = Some(imageDate.getTime))
      val expectMsg = new VehicleRegistrationMessage(
        lane = new Lane("100", "lane1", "gantry1", "route", 0, 0),
        eventId = Some("1"),
        eventTimestamp = None,
        event = new SensorEvent(imageDate.getTime, new PeriodRange(imageDate.getTime, imageDate.getTime), "Camera"),
        priority = Priority.NONCRITICAL,
        files = List(expectImage))

      probe.expectMsg(500.millis, expectMsg)
    }
  }

}
