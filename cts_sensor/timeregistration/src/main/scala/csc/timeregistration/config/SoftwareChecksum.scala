/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.timeregistration.config

import csc.akka.logging.DirectLogging
import csc.common.Signing
import java.io.File
import org.apache.commons.io.FileUtils

case class SoftwareChecksum(fileName: String, checksum: String)

/**
 * used to create a checksum of a given file and/or it's own jar file
 */
object SoftwareChecksum extends DirectLogging {

  def createHashFromRuntimeClass(className: Class[_]): Option[SoftwareChecksum] = {
    val file = new File(className.getProtectionDomain.getCodeSource.getLocation.getPath)
    Some(SoftwareChecksum(file.getName, createChecksum(Seq(file)).getOrElse("")))
  }

  /**
   * used to create a checksum of a given file
   */
  def createChecksum(path: String): Option[String] = {
    createChecksum(Seq(new File(path)))
  }

  def createHashFromRuntime(className: Class[_]): Option[String] = {
    createChecksum(Seq(new File(className.getProtectionDomain.getCodeSource.getLocation.getPath)))
  }

  private def createChecksum(files: Seq[File]): Option[String] = {
    val data = files.foldLeft(new Array[Byte](0))((array, file) ⇒ {
      if (!file.exists) {
        log.error("file does not exists " + file.getAbsolutePath)
        return None
      }
      if (file.isDirectory) {
        log.warning("given path is a directory " + file.getAbsolutePath)
        return None
      }
      array ++ FileUtils.readFileToByteArray(file)
    })

    val hash = Signing.calculateHash(Some(data), "MD5")
    log.info("hash [%s] created for files [%s]".format(hash, files.map(_.getAbsolutePath).mkString(":")))
    Some(hash)
  }

}