/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.timeregistration.registration

import base._
import _root_.java.text.SimpleDateFormat
import _root_.java.util.Date
import _root_.imagefunctions.ImageFunctionsException
import akka.actor.{ ActorLogging, ActorRef }
import collection.mutable.ListBuffer
import util.matching.Regex
import csc.vehicle.registration.events.EventBuilder
import csc.vehicle.message._
import csc.vehicle.message.RegistrationImage
import csc.vehicle.message.RegistrationRelay
import csc.vehicle.message.VehicleRegistrationMessage
import akka.event.LoggingReceive

/**
 * get the imageData
 * @param sectionGuard Guard to use
 * @param eventBuilder The EventBuilder to create events
 * @param setEventTime Should the EventTime be set by this actor
 * @param nextActors  The list op next actors
 * @param testImagePattern The ImageDescription of the test images.
 * @param metadataReader the actual metadata reader
 */
class ReadTiffImageMetaData(sectionGuard: Option[FavorCriticalProcess],
                            eventBuilder: EventBuilder,
                            setEventTime: Boolean,
                            nextActors: Set[ActorRef],
                            testImagePattern: String,
                            metadataReader: ImageMetaDataReader) extends RecipientList(nextActors) with Timing with ActorLogging {

  val testImageRegExPattern = new Regex(testImagePattern).pattern

  def receive = LoggingReceive {
    case msg: RoundtripMessage ⇒ handleRoundtrip("ReadTiffImageMetaData", msg)
    case msg: VehicleRegistrationMessage ⇒ {
      val eventId = msg.eventId.getOrElse("Unknown")

      performanceLogInterval(log, 3, 4, "ReadTiffImageMetaData", eventId,
        {
          process(msg, eventId) match {
            case None             ⇒ //test image: nothing else to do
            case Some(outMessage) ⇒ sendToRecipients(outMessage)
          }
        })
    }
  }

  def process(msg: VehicleRegistrationMessage, eventId: String): Option[VehicleRegistrationMessage] = {
    val relayFile = msg.getRelayFile()

    var testImage = false
    var imageInfo: Option[ImageInfo] = None
    var imageTime: Long = System.currentTimeMillis()
    var realTime: Boolean = false

    val files = new ListBuffer[RegistrationFile]()
    files ++= msg.files.filterNot(file ⇒ file.isInstanceOf[RegistrationRelay])
    relayFile match {
      case Some(image) ⇒ {
        val filename = image.uri
        def getInfo() { imageInfo = metadataReader.apply(filename) }

        try {
          SectionGuard.process(sectionGuard, msg.priority, getInfo())
        } catch {
          case e: ImageFunctionsException ⇒ {
            log.error("getMetaData eventId=[%s] returned %s".format(eventId, e.getMessage))
          }
        }

        var imageFormat: Option[ImageFormat.Value] = None

        imageInfo match {
          case Some(meta) ⇒ {
            if (meta.time.isDefined) {
              realTime = true
              imageTime = meta.time.get
            }

            files += new RegistrationImage(uri = image.uri, imageFormat = meta.format, imageType = VehicleImageType.Overview, imageVersion = ImageVersion.Original, timestamp = imageTime, sha = image.sha, imageTimeStamp = Some(imageTime))
            testImage = meta.imageDescription.map { id ⇒ testImageRegExPattern.matcher(id).matches() }.getOrElse(false)
          }
          case None ⇒ files ++= msg.files
        }
      }
      case None ⇒ files ++= msg.files
    }

    if (!realTime) {
      log.warning("Could not find eventdate using current time for eventId=[%s]".format(eventId))
    }

    val imageTimestamp: Date = new Date(imageTime)
    log.info("Perf eventId=[%s] point 0: EventTimestamp %s".format(eventId, new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS") format imageTimestamp))

    if (testImage) {
      log.info("Perf eventId=[%s] is a test Image so drop this message".format(eventId))
      None
    } else {
      //Add results to message
      val event = eventBuilder.createEvent(imageTime)
      val eventTimeStamp = if (setEventTime) Some(imageTime) else None
      Some(msg.copy(event = event, eventTimestamp = eventTimeStamp, files = files.toList))
    }
  }

}
