package csc.timeregistration.registration

import java.io.{ FileInputStream, File }

import akka.event.LoggingAdapter
import csc.akka.logging.DirectLogging
import csc.image.ImageTimeExtractor
import csc.vehicle.message._
import imagefunctions.ImageFunctionsFactory
import org.apache.commons.io.IOUtils

/**
 * Created by carlos on 24/11/15.
 */
object ImageMetaDataReader extends DirectLogging {

  lazy val tiff = new ImageMetaDataReader(log) {
    override def apply(path: String): Option[ImageInfo] = {
      val sensorLib = ImageFunctionsFactory.getBayerTifFunctions
      Option(sensorLib.getTifMetaData(path)) match {
        case None ⇒ None
        case Some(metadata) ⇒ {
          val format = if (metadata.getBitsPerSample > 8) ImageFormat.TIF_BAYER else ImageFormat.TIF_GRAY
          Some(ImageInfo(format, Some(metadata.getImageDateTime.getTime), Some(metadata.getImageDescription)))
        }
      }
    }

    override def getImageInfo(imageBytes: Array[Byte]): Option[ImageInfo] =
      throw new UnsupportedOperationException("Not implemented in BayerTifFunctions")
  }

  lazy val jpeg = new ImageMetaDataReader(log) {
    override def getImageInfo(imageBytes: Array[Byte]): Option[ImageInfo] = {
      val extractor = new ImageTimeExtractor()
      try {
        Option(extractor.getExifImageTime(imageBytes)) match {
          case Some(time) ⇒ Some(ImageInfo(ImageFormat.JPG_RGB, Some(time), None))
          case None       ⇒ Some(ImageInfo(ImageFormat.JPG_RGB, Some(System.currentTimeMillis()), None))
        }
      } catch {
        case error: Exception ⇒
          log.error(error, "Error getting time from image bytes")
          None
      }

    }
  }
}

abstract class ImageMetaDataReader(log: LoggingAdapter) {

  def getImageInfo(imageBytes: Array[Byte]): Option[ImageInfo]

  def apply(path: String): Option[ImageInfo] = {
    val in = new FileInputStream(path)
    try {
      val bytes = IOUtils.toByteArray(in)
      getImageInfo(bytes)
    } finally {
      in.close()
    }
  }

}

case class ImageInfo(format: ImageFormat.Value, time: Option[Long], imageDescription: Option[String])
