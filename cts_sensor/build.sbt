
import sbt._
import vehicleRegistration._
import csc.sbt._
import csc.sbt.dep._
import csc.sbt.CommonSettings._

lazy val jacopsOnly: Boolean = System.getProperty("jacops-only") != null

lazy val defaultSettings: Seq[sbt.Def.Setting[_]] = commonSettings ++ Seq(

  //resolvers += "JAnalyse Repository" at "http://www.janalyse.fr/repository/",
  //resolvers += "Cloudera Repo" at "https://repository.cloudera.com/artifactory/cloudera-repos/",

//  resolvers := NexusHelper.getResolvers("group_cts_sensor"),
//  externalResolvers <<= resolvers map { rs => Resolver.withDefaultResolvers(rs, mavenCentral = NexusHelper.useMavenCentral) },

  parallelExecution in Test := System.getProperty("ctsSensor.parallelExecution", "false").toBoolean,

  // for excluding tests by name (or use system property: -ctsSensor.test.names.exclude=TimingSpec)
  excludeTestNames := {
    val exclude = System.getProperty("ctsSensor.test.names.exclude", "")
    if (exclude.isEmpty) Seq.empty else exclude.split(",").toSeq
  },

  // for excluding tests by tag (or use system property: -ctsSensor.tags.exclude=timing)
  excludeTestTags := {
    val exclude = System.getProperty("ctsSensor.test.tags.exclude", "")
    if (exclude.isEmpty) defaultExcludedTags else exclude.split(",").toSeq
  },

  // for including tests by tag (or use system property: -ctsSensor.test.tags.include=timing)
  includeTestTags := {
    val include = System.getProperty("ctsSensor.test.tags.include", "")
    if (include.isEmpty) Seq.empty else include.split(",").toSeq
  },

  topProjectFolder := ctsSensor.base
)

lazy val builder = new ProjectBuilder(defaultSettings)

lazy val IntegrationTest = config("it") extend(Test)

lazy val jacopsApps: Seq[ProjectReference] =
  Seq(imageStore, recognizeProcessor, gantryMonitor, inputEvents, vehicleRegistration, vehicleClassifier, ftplet,
    imageRecognizer, baseregistration, gantryConfig, systemEvents, timeregistration, jaiCamera, calibration, swarcoPir)

lazy val nonJacopsApps: Seq[ProjectReference] =
  Seq(calibrationServer, gantryProcessor, siteBuffer, tools)

lazy val projectsToInclude: Seq[ProjectReference] =
  if (jacopsOnly) jacopsApps else jacopsApps ++ nonJacopsApps

lazy val ctsSensor = Project(
  id = "ctsSensor",
  base = file("."),
  settings = parentSettings ++ Publish.versionSettings ++ Seq(
    parallelExecution in GlobalScope := System.getProperty("ctsSensor.parallelExecution", "false").toBoolean,
    Publish.defaultPublishTo in ThisBuild <<= crossTarget / "repository"
  )
).aggregate(projectsToInclude:_*)

lazy val r = SensorRuntimeDeps
lazy val t = SensorTestDeps

lazy val startMemory = "256M"
lazy val maxMemory = "1536M"

import SensorDeps._

//you should use one of project sensorCertified or lib sensorCert
lazy val certifiedLibs = Seq(
  //uncomment when you want to use a released version of the sensorCertified lib
  //sensorCert
)
lazy val certifiedProjects: Seq[ClasspathDep[ProjectReference]] = Seq(
  //uncomment when you have unreleased changes in sensorCertified project
  sensorCertified
)

lazy val vehicleRegistration = builder.distProj(
  "vehicle-registration",
  Seq(akkaKernel, akkaActor, akkaRemote, akkaSlf4j, akkaCamel, akkaAgent, logback, netty, jna,
    slf4jLog4j, slf4jApi, r.camelFtp, r.camelMina, r.camelJetty, comonCompress, amqpClient,
    liftJson, r.mina, arm, cscImageFunc, commonImage, exifExtractor, commonsLang,
    t.transact, t.persistence, commonsNet, cscTestCurator) ++ t.testSet,
  DistSettings("registration", "csc.vehicle.startup.Boot", startMemory, maxMemory),
  commonMappings(None) ++ toolMappings
) dependsOn(baseregistration,gantryConfig, systemEvents, swarcoPir, imageRecognizer, imageStore, timeregistration, ftplet)

lazy val ftplet = builder.simpleDistProj(
  "apache-ftplet",
  Seq(slf4jApi.p, ftpserver.p, rabbitMqClient, t.junit),
  "apache-ftpserver",
  classMappings("/") ++ relocateLib("common/lib/") ++ javaLibMappings
)

lazy val gantryProcessor = builder.distProj(
  "gantry-processor",
  Seq(akkaUtil, akkaKernel, akkaActor, slf4jLog4j,
    logback, t.akkaCamel, t.camelMina) ++ t.testSet,
  DistSettings("gantry", "csc.gantry.startup.Boot", startMemory, maxMemory),
  commonMappings(Some("gantry.log"))
) dependsOn(swarcoPir)

lazy val siteBuffer = builder.distProj(
  "site-buffer",
  Seq(akkaKernel, akkaActor, akkaUtil, slf4jLog4j, logback, akkaCamel, r.camelMina,
    r.camelJetty, liftJson, netty, jassh, jcraft, t.akkaCamel, t.camelMina, commonsNet, cscConfig) ++ t.testSet,
  DistSettings("site-buffer", "csc.sitebuffer.startup.Boot", startMemory, maxMemory),
  commonMappings(Some("sitebuffer.log")) ++ mainScriptMappings
) dependsOn(jaiCamera)

lazy val tools = builder.distProj(
  "tools",
  Seq(akkaKernel, akkaActor, akkaUtil, slf4jLog4j, logback, akkaCamel, r.camelMina,
    r.camelJetty, liftJson, t.akkaCamel, t.camelMina) ++ t.testSet,
  DistSettings("tools", "csc.tools.startup.Boot", startMemory, maxMemory),
  commonMappings(Some("tools.log"))
)

lazy val gantryMonitor = builder.distProj(
  "gantry-monitor",
  Seq(akkaActor, akkaKernel, amqpUtil, r.camelMina, r.mina, akkaKernel, akkaActor, slf4jLog4j, logback,
    akkaCamel, cscImageFunc, commonsIO, httpClient) ++ t.testSet,
  DistSettings("gantryMonitor", "csc.gantrymonitor.startup.Boot", startMemory, maxMemory),
  commonMappings(Some("monitor.log")) ++ toolMappings
) dependsOn(calibration, jaiCamera, gantryConfig, timeregistration, systemEvents)

lazy val calibrationServer = builder.distProj(
  "calibration-server",
  Seq(akkaActor, akkaKernel, amqpUtil, slf4jLog4j, logback, akkaCamel, cscImageFunc,
    hbasewd, CDH5.hadoopCommon, CDH5.hbaseCommon, CDH5.hbaseClient, cscTestCurator) ++ t.testSet,
  DistSettings("calibrationServer", "csc.calibrationserver.startup.Boot", startMemory, maxMemory),
  commonMappings(Some("calibration-server.log"))
) dependsOn(calibration, gantryConfig, systemEvents)

lazy val recognizeProcessor = builder.distProj(
  "recognize-processor",
  Seq(akkaRemote, cscConfig, r.camelMina, r.mina, akkaKernel, akkaActor, logback) ++ t.testSet,
  DistSettings("recognize-processor", "csc.recognizer.Boot", startMemory, maxMemory),
  commonMappings(Some("recognizer.log")) ++ intradaMappings
) dependsOn(imageRecognizer)

lazy val vehicleClassifier = builder.distProj(
  "vehicle-classifier",
  Seq(akkaRemote, cscConfig, akkaKernel, akkaActor, logback) ++ t.testSet,
  DistSettings("vehicle-classifier", "csc.vehicle.classifier.ClassifierBoot", startMemory, maxMemory),
  commonMappings(Some("classifier.log"))
) dependsOn(baseregistration)

lazy val imageStore = builder.distProj(
  "image-store",
  Seq(akkaActor, akkaKernel, commonsIO, commonsCodec, logback, amqpUtil) ++ t.testSet,
  DistSettings("image-store", "csc.image.store.Boot", startMemory, maxMemory),
  commonMappings(Some("image-store.log"))
)

lazy val imageRecognizer = builder.proj(
  "image-recognizer",
  Seq(cscImageFunc, logback, akkaUtil, amqpUtil) ++ t.testSet
) dependsOn(baseregistration)

lazy val gantryConfig = builder.proj(
  "gantry-config",
  Seq(akkaUtil, amqpUtil, cscCurator, commonsLang, cscTestCurator) ++ t.testSet
) dependsOn(baseregistration)

lazy val systemEvents = builder.proj(
  "system-events",
  Seq(akkaActor, akkaKernel, commonsIO, commonsCodec, logback, amqpUtil, cscCurator, cscTestCurator) ++ t.testSet
)

lazy val swarcoPir = builder.proj(
  "swarco-pir",
  Seq(akkaActor,netty, akkaUtil) ++ t.testSet
).dependsOn(gantryConfig)

lazy val jaiCamera = builder.proj(
  "jai-camera",
  Seq(akkaActor,netty, akkaUtil, commonsIO) ++ t.testSet
)

lazy val timeregistration = builder.proj(
  "timeregistration",
  Seq(cscImageFunc, akkaActor, akkaSlf4j, commonsIO, akkaUtil) ++ t.testSet ++ certifiedLibs
).dependsOn(baseregistration).dependsOn(certifiedProjects:_*)

lazy val baseregistration = builder.proj(
  "baseregistration",
  Seq(comonCompress, cscImageFunc, akkaUtil, amqpUtil, t.liftJson) ++ t.testSet
)

lazy val calibration = builder.proj(
  "calibration",
  Seq()
)

lazy val sensorCertified = builder.proj(
  "sensor-certified",
  Seq(commonsIO, Sanselan, t.junit)
)

lazy val inputEvents = builder.distProj(
  "sensor-input-events",
  Seq(akkaActor, akkaKernel, commonsIO, commonsCodec, logback, amqpUtil, commonsLang, r.camelMina, r.mina) ++ t.testSet,
  DistSettings("sensor-input-events", "csc.inputevents.Boot", startMemory, maxMemory),
  commonMappings(Some("input-events.log")) ++ toolMappings
) dependsOn(systemEvents, gantryConfig)

