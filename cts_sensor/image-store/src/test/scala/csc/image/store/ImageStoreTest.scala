/*
 * Copyright (C) 2015. CSC <http://www.csc.com>
 */

package csc.image.store

import java.io.File

import akka.actor.ActorSystem
import akka.testkit.{ ImplicitSender, TestKit }
import scala.concurrent.Await
import scala.concurrent.duration._
import com.rabbitmq.client.{ Channel, Connection, ConnectionFactory }
import com.typesafe.config.ConfigFactory
import csc.akka.logging.DirectLogging
import csc.amqp.AmqpSerializers
import csc.image.store.protocol.{ ImageContentPutRequest, ImageGetRequest, ImageGetResponse }
import org.apache.commons.codec.digest.DigestUtils
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }

/**
 * @author csomogyi
 */
class ImageStoreTest extends TestKit(ActorSystem("amqpClient")) with ImplicitSender with AmqpSerializers with DirectLogging
  with WordSpecLike with MustMatchers with BeforeAndAfterAll with BeforeAndAfterEach with DirectoryFunctions {

  implicit val executor = system.dispatcher
  override implicit val formats = csc.image.store.JsonSerializers.formats
  override val ApplicationId = "ImageStore"

  val tempDir = createTempDirectory()
  val configFile = new File(getClass.getClassLoader.getResource("imageStore-test.conf").getPath)
  val typesafeConfig = ConfigFactory.parseString("image-store.storage.base-directory = \"" + tempDir.getAbsolutePath + "\"")
    .withFallback(ConfigFactory.parseFile(configFile))
  val config = Configuration.load(typesafeConfig)

  var boot: Boot = _
  var connection: Connection = _
  var channel: Channel = _
  var consumer: TestConsumer = _

  override protected def afterAll(): Unit = {
    system.shutdown()
    deleteDirectory(tempDir)
  }

  override def beforeEach(): Unit = {
    cleanupDirectory(tempDir)

    val cf = new ConnectionFactory()
    cf.setUri(config.amqp.amqpUri)
    connection = cf.newConnection()
    channel = connection.createChannel()
    channel.queueDelete(config.services.consumerQueue)
    channel.queueDeclare(config.services.consumerQueue, false, false, true, null)
    // end-point is "" then routeKey == send queue
    //TODO to make this test work again, correct route keys must be used (one per response class)
    channel.queueDelete(config.services.producerRouteKeyPrefix)
    channel.queueDeclare(config.services.producerRouteKeyPrefix, false, false, true, null)
    consumer = new TestConsumer(channel)
    channel.basicConsume(config.services.producerRouteKeyPrefix, true, consumer)

    boot = new Boot {
      override def loadConfiguration(): Configuration = {
        config
      }
    }
    boot.startup()
  }

  override def afterEach(): Unit = {
    channel.close()
    connection.close()
    boot.shutdown()
    boot.actorSystem.shutdown()
  }

  "ImageStore" ignore {
    "give back what we put in" in {
      val creationTime = System.currentTimeMillis()
      val image = Array[Byte](104, 101, 108, 108, 111)
      val checksum = DigestUtils.md5Hex(image)
      val (body, properties) = toPublishPayload(ImageContentPutRequest("alma", creationTime, image, checksum)).right.get
      channel.basicPublish(config.services.producerEndpoint, config.services.consumerQueue, properties, body)
      val (body2, properties2) = toPublishPayload(ImageGetRequest("1", "alma")).right.get
      channel.basicPublish(config.services.producerEndpoint, config.services.consumerQueue, properties2, body2)
      val delivery = Await.result(consumer.future, 5 seconds)
      val response = toMessage[ImageGetResponse](delivery).right.get
      assert(response == ImageGetResponse("1", "alma", creationTime / 1000 * 1000, image, checksum, None))
    }
  }
}
