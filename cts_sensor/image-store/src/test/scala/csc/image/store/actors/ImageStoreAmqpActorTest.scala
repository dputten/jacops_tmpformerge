package csc.image.store.actors

import java.util.concurrent.TimeUnit

import akka.actor.{ ActorRef, ActorSystem, Props }
import akka.testkit.{ ImplicitSender, TestKit }
import akka.util.Timeout
import csc.amqp.AmqpSerializers.JsonDeserializer
import csc.amqp.test.AmqpBridgeActor.MatcherLogic
import csc.amqp.test.{ Acked, AmqpBridgeActor, Outcome }
import csc.image.store.Configuration.{ ServicesConfiguration, StorageConfiguration }
import csc.image.store.protocol.{ ImageGetRequest, ImageGetResponse, ImagePutRequest, Matcheable }
import csc.image.store.{ Configuration, JsonSerializers }
import net.liftweb.json.Formats
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }

import scala.concurrent.duration.Duration

/**
 * Created by carlos on 16.10.15.
 */
class ImageStoreAmqpActorTest
  extends TestKit(ActorSystem("imageStore"))
  with ImplicitSender
  with WordSpecLike
  with MustMatchers
  with BeforeAndAfterAll
  with BeforeAndAfterEach {

  import ImageStoreAmqpActorTest._

  val jsonFormats = csc.image.store.JsonSerializers.formats
  implicit val timeout: Timeout = Timeout(1, TimeUnit.SECONDS)

  var actor: ActorRef = null
  var client: ActorRef = null

  override protected def afterAll(): Unit = {
    super.afterAll()
    system.shutdown()
  }

  def createConfig: Configuration = {
    val amqp = null
    val imageStore = new StorageConfiguration("/tmp/storage", Duration(1, TimeUnit.HOURS), Duration.create(1, TimeUnit.DAYS))
    val services = new ServicesConfiguration("dummy", "dummy", "dummy", 1)
    new Configuration(amqp, imageStore, services)
  }

  override protected def beforeEach(): Unit = {
    super.beforeEach()
    //client is also the producer, so it must be initialized first
    client = system.actorOf(Props(new AmqpBridgeActor(getActor, jsonFormats, matcherLogic, jsonDeserializers)))
    actor = system.actorOf(Props(new ImageStoreAmqpActor(client, createConfig))) //TestActorRef(new ImageStoreAmqpActor(client, createConfig))
  }

  private def getActor = actor

  "ImageStoreActor" must {

    "handle and ack a ImageGetRequest for a non-existing image" in {
      val request = createGetImageRequest("foo")
      val outcome = sendMessage(request)
      outcome must be(Acked)
      val response = expectMsgType[ImageGetResponse]
      response.refId must be(request.refId)
      response.key must be(request.key)
      Option(response.image) must be(None)
    }

    "handle and ack a ImagePutRequest for a non-existing image" in {
      val msg = ImagePutRequest("bar", System.currentTimeMillis(), "/tmp/doesnotexist", "dummy")
      val outcome = sendMessage(msg)
      outcome must be(Acked)
    }

  }

  def sendMessage(msg: AnyRef): Outcome = {
    client ! msg
    expectMsgType[Outcome]
  }
}

object ImageStoreAmqpActorTest extends csc.amqp.JsonSerializers {

  override implicit val formats: Formats = JsonSerializers.formats

  var lastRefId = 0;

  def createGetImageRequest(key: String): ImageGetRequest = {
    lastRefId = lastRefId + 1
    ImageGetRequest(lastRefId.toString, key)
  }

  lazy val matcherLogic: MatcherLogic = msg ⇒ msg match {
    case m: Matcheable ⇒ Some(m.refId, m.multiResponse, m.lastResponse)
    case _             ⇒ None
  }

  lazy val jsonDeserializers: Map[String, JsonDeserializer] = Map(
    "ImageGetResponse" -> fromJson[ImageGetResponse])
}