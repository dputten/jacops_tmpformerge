/*
 * Copyright (C) 2015. CSC <http://www.csc.com>
 */

package csc.image.store

import java.nio.charset.Charset

import com.github.sstone.amqp.Amqp.Delivery
import com.rabbitmq.client.AMQP
import csc.akka.logging.DirectLogging
import csc.amqp.AmqpSerializers
import csc.image.store.protocol._
import net.liftweb.json._
import org.apache.commons.codec.digest.DigestUtils
import org.scalatest.WordSpec
import org.scalatest._

/**
 * @author csomogyi
 */
class AmqpSerializersTest extends WordSpec with MustMatchers with AmqpSerializers with DirectLogging {
  override implicit val formats = csc.image.store.JsonSerializers.formats
  override val ApplicationId = "ImageStore"

  "AmqpSerializers" must {
    "serialize ImageBatchProgressResponse" in {
      val payload = toPublishPayload(ImageBatchProgressResponse("1", 3, 1, 2))
      assert(payload.isRight, "Error in payload")
      val (array, properties) = payload.right.get
      val json = parse(new String(array, Charset.forName("UTF-8")))
      assert(json \\ "refId" === JString("1"))
      assert(json \\ "total" === JInt(3))
      assert(json \\ "count" === JInt(1))
      assert(json \\ "errorCount" === JInt(2))
      assert(properties.getContentType === "application/json")
      assert(properties.getContentEncoding === "UTF-8")
      assert(properties.getAppId === "ImageStore")
      assert(properties.getType === "ImageBatchProgressResponse")
    }
    "serialize ImageGetResponse" in {
      val image = Array[Byte](104, 101, 108, 108, 111)
      val checksum = DigestUtils.md5Hex(image)
      val payload = toPublishPayload(ImageGetResponse("1", "alma", 1234567890, image, checksum, None))
      assert(payload.isRight, "Error in payload")
      val (array, properties) = payload.right.get
      val json = parse(new String(array, Charset.forName("UTF-8")))
      assert(json \\ "refId" === JString("1"))
      assert(json \\ "key" === JString("alma"))
      assert(json \\ "creationTime" === JString("1234567890"))
      assert(json \\ "image" === JArray(List(JInt(104), JInt(101), JInt(108), JInt(108), JInt(111))))
      assert(json \\ "checksum" === JString(checksum))
      assert(json \\ "error" === JNull)
      assert(properties.getContentType === "application/json")
      assert(properties.getContentEncoding === "UTF-8")
      assert(properties.getAppId === "ImageStore")
      assert(properties.getType === "ImageGetResponse")
    }
    "deserialize ImageGetRequest" in {
      val payload = """{ "refId": "1", "key": "alma" }"""
      val builder = new AMQP.BasicProperties.Builder()
      val properties = builder.`type`("ImageGetRequest").contentType("application/json").contentEncoding("UTF-8").build()
      val delivery = Delivery("", null, properties, payload.getBytes("UTF-8"))
      val request = toMessage[ImageGetRequest](delivery)
      assert(request.isRight, "Deserialization error")
      assert(request.right.get === ImageGetRequest("1", "alma"))
    }
    "deserialize ImageBatchGetRequest" in {
      val payload = """{ "refId": "1", "keys": ["alma", "lama"] }"""
      val builder = new AMQP.BasicProperties.Builder()
      val properties = builder.`type`("ImageBatchGetRequest").contentType("application/json").contentEncoding("UTF-8").build()
      val delivery = Delivery("", null, properties, payload.getBytes("UTF-8"))
      val request = toMessage[ImageBatchGetRequest](delivery)
      assert(request.isRight, "Deserialization error")
      assert(request.right.get === ImageBatchGetRequest("1", Seq("alma", "lama")))
    }
    "deserialize ImageContentPutRequest" in {
      val image = Array[Byte](104, 101)
      val payload = """{ "key": "alma", "creationTime": "1234567890", "image": [104, 101], "checksum": "3B" }"""
      val builder = new AMQP.BasicProperties.Builder()
      val properties = builder.`type`("ImageContentPutRequest").contentType("application/json").contentEncoding("UTF-8").build()
      val delivery = Delivery("", null, properties, payload.getBytes("UTF-8"))
      val request = toMessage[ImageContentPutRequest](delivery)
      //assert(request.right.get === "alma")
      assert(request.isRight, "Deserialization error")
      assert(request.right.get === ImageContentPutRequest("alma", 1234567890, image, "3B"))
    }
    "fail to deserialize ImageGetRequest on wrong content type" in {
      val payload = """{ "refId": "1", "key": "alma" }"""
      val builder = new AMQP.BasicProperties.Builder()
      val properties = builder.`type`("ImageGetRequest").contentType("application/xml").contentEncoding("UTF-8").build()
      val delivery = Delivery("", null, properties, payload.getBytes("UTF-8"))
      val request = toMessage[ImageGetRequest](delivery)
      assert(request.isLeft, "Deserialization should have failed")
      assert(request.left.get === "Invalid message content type 'application/xml'")
    }
    "fail to deserialize ImageGetRequest on wrong message type" in {
      val payload = """{ "refId": "1", "key": "alma" }"""
      val builder = new AMQP.BasicProperties.Builder()
      val properties = builder.`type`("WrongType").contentType("application/json").contentEncoding("UTF-8").build()
      val delivery = Delivery("", null, properties, payload.getBytes("UTF-8"))
      val request = toMessage[ImageGetRequest](delivery)
      assert(request.isLeft, "Deserialization should have failed")
      assert(request.left.get === "Invalid message type 'WrongType'")
    }
  }
}
