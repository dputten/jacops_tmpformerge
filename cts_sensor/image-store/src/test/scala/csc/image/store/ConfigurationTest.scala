package csc.image.store

/**
 * Copyright (C) 2015 CSC. <http://www.csc.com>
 */

import scala.concurrent.duration._
import com.typesafe.config.ConfigFactory
import org.scalatest.WordSpec
import org.scalatest._

/**
 * @author csomogyi
 */
class ConfigurationTest extends WordSpec with MustMatchers {
  "configuration" must {
    "load the configuration upon specifying system property" in {
      val cfg = ConfigFactory.load("imageStore-configtest")
      val config = Configuration.load(cfg)
      config.amqp.actorName must be("imagestore-actor")
      config.amqp.amqpUri must be("amqp://localhost:5672/")
      config.amqp.reconnectDelay must be(5000 milliseconds)
      config.imageStore.baseDirectory must be("./store")
      config.imageStore.cleanupFrequency must be(4 hours)
      config.imageStore.imageExpiration must be(7 days)
      config.services.consumerQueue must be("imagestore_req")
      config.services.producerEndpoint must be("")
      config.services.producerRouteKeyPrefix must be("<system>.<gantry>")
      config.services.serversCount must be(5)
    }

  }
}