/*
 * Copyright (C) 2015. CSC <http://www.csc.com>
 */

package csc.image.store.actors

import akka.actor.{ ActorSystem, Props }
import akka.testkit.{ ImplicitSender, TestKit }
import scala.concurrent.duration._
import csc.image.store.Configuration.StorageConfiguration
import csc.image.store.DirectoryFunctions
import csc.image.store.protocol._
import org.apache.commons.codec.digest.DigestUtils
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, WordSpec }

/**
 * @author csomogyi
 */
class ImageStoreActorsTest extends TestKit(ActorSystem("imageStore")) with ImplicitSender
  with WordSpecLike with MustMatchers with BeforeAndAfterAll with DirectoryFunctions {
  val tempDir = createTempDirectory()
  val config = new StorageConfiguration(tempDir.getAbsolutePath, 4 hours, 7 days)

  def assertProgress(progress: ImageBatchProgressResponse, refId: String,
                     total: Int, count: Int, errorCount: Int): Unit = {
    assert(progress.refId === refId)
    assert(progress.total === total)
    assert(progress.count === count)
    assert(progress.errorCount === errorCount)
  }

  override def afterAll(): Unit = {
    system.shutdown()
    deleteDirectory(tempDir)
  }

  "put" must {
    "return error on checksum error" in {
      cleanupDirectory(tempDir)
      val put = system.actorOf(Props(new ImageStorePutActor(config)))
      val get = system.actorOf(Props(new ImageStoreGetActor(config)))

      val refId = "1"
      val key = "alma"
      val creationTime = 987654321L
      val image = Array[Byte](104, 101, 108, 108, 111)
      val checksum = DigestUtils.md5Hex(image)
      put ! ImageContentPutRequest(key, creationTime, image, checksum + "alma")
      expectMsg(ImagePutResponse(key, Some("Checksum error")))

      get ! ImageGetRequest(refId, key)
      val response = expectMsgType[ImageGetResponse]
      assert(response.refId === refId)
      assert(response.key === key)
      assert(response.error.isDefined) // error must not be empty
    }
  }
  "get" must {
    "give back what we put in" in {
      cleanupDirectory(tempDir)
      val put = system.actorOf(Props(new ImageStorePutActor(config)))
      val get = system.actorOf(Props(new ImageStoreGetActor(config)))

      val refId = "1"
      val key = "alma"
      val creationTime = 987654321L
      val image = Array[Byte](104, 101, 108, 108, 111)
      val checksum = DigestUtils.md5Hex(image)
      put ! ImageContentPutRequest(key, creationTime, image, checksum)
      expectMsg(ImagePutResponse(key, None))

      get ! ImageGetRequest(refId, key)
      expectMsg(ImageGetResponse(refId, key, creationTime / 1000 * 1000, image, checksum, None))
    }
    "give error on missing file" in {
      cleanupDirectory(tempDir)
      val get = system.actorOf(Props(new ImageStoreGetActor(config)))

      val refId = "1"
      val key = "alma"
      val creationTime = 987654321L
      val image = Array[Byte](104, 101, 108, 108, 111)

      get ! ImageGetRequest(refId, key)
      val response = expectMsgType[ImageGetResponse]
      assert(response.refId === refId)
      assert(response.key === key)
      assert(response.error.isDefined) // error must not be empty
    }
  }
  "batch get" must {
    "return all progress and individual get responses" in {
      cleanupDirectory(tempDir)
      val put = system.actorOf(Props(new ImageStorePutActor(config)))
      val get = system.actorOf(Props(new ImageStoreGetActor(config)))

      val key = "alma"
      val creationTime = 987654321L
      val image = Array[Byte](104, 101, 108, 108, 111)
      val checksum = DigestUtils.md5Hex(image)
      put ! ImageContentPutRequest(key, creationTime, image, checksum)
      expectMsg(ImagePutResponse(key, None))

      val key2 = "lama"
      val creationTime2 = 987654321L
      val image2 = Array[Byte](104, 101, 108, 108)
      val checksum2 = DigestUtils.md5Hex(image2)
      put ! ImageContentPutRequest(key2, creationTime2, image2, checksum2)
      expectMsg(ImagePutResponse(key2, None))

      val refId = "1"
      get ! ImageBatchGetRequest(refId, Seq("alma", "mala", "lama"))
      assertProgress(expectMsgType[ImageBatchProgressResponse], refId, 3, 0, 0)
      expectMsg(ImageGetResponse(refId, key, creationTime / 1000 * 1000, image, checksum, None))
      val response = expectMsgType[ImageGetResponse]
      assert(response.refId === refId)
      assert(response.key === "mala")
      assert(response.error.isDefined) // error must not be empty
      expectMsg(ImageGetResponse(refId, key2, creationTime2 / 1000 * 1000, image2, checksum2, None))
      assertProgress(expectMsgType[ImageBatchProgressResponse], refId, 3, 2, 1)
    }
  }
}
