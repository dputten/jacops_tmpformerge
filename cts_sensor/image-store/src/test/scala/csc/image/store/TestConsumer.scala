/*
 * Copyright (C) 2015. CSC <http://www.csc.com>
 */

package csc.image.store

import com.github.sstone.amqp.Amqp.Delivery
import com.rabbitmq.client.AMQP.BasicProperties
import com.rabbitmq.client.{ Channel, DefaultConsumer, Envelope }

import scala.concurrent.{ Promise, ExecutionContext }

/**
 * @author csomogyi
 */
class TestConsumer(channel: Channel)(implicit executor: ExecutionContext) extends DefaultConsumer(channel) {
  channel.basicQos(1) // important !!!

  private var _p: Promise[Delivery] = Promise()

  def future = _p.future

  override def handleDelivery(consumerTag: String, envelope: Envelope,
                              properties: BasicProperties, body: Array[Byte]): Unit = {
    _p success Delivery(consumerTag, envelope, properties, body)
  }

  def ack(delivery: Delivery): Unit = {
    _p = Promise()
    channel.basicAck(delivery.envelope.getDeliveryTag, false)
  }
}
