package csc.image.store

import java.io.{ File, IOException }

import org.apache.commons.io.FileUtils

/**
 * @author csomogyi
 */
trait DirectoryFunctions {
  def createTempDirectory(): File = {
    val temp = File.createTempFile("scala-test-", System.nanoTime.toString)

    if (!temp.delete()) throw new IOException("Could not delete temp file: " + temp.getAbsolutePath())

    FileUtils.forceMkdir(temp)

    temp
  }

  def cleanupDirectory(dir: File): Unit = {
    FileUtils.cleanDirectory(dir)
  }

  def deleteDirectory(dir: File): Unit = {
    FileUtils.forceDelete(dir)
  }
}
