package csc.image.store

import java.io.File

import akka.actor._
import akka.testkit.{ ImplicitSender, TestKit }
import akka.util.Timeout
import scala.concurrent.duration._
import com.github.sstone.amqp.Amqp.{ Ok, Publish }
import com.github.sstone.amqp.Amqp
import csc.akka.logging.DirectLogging
import csc.amqp.{ MQConnection, AmqpSerializers }
import csc.image.store.protocol._
import org.apache.commons.codec.digest.DigestUtils
import org.apache.commons.io.FileUtils
import org.scalatest._

/**
 * Created by carlos on 30.09.15.
 */
class ImageStoreLiveTest
  extends TestKit(ActorSystem("ImageStoreClient"))
  with ImplicitSender
  with WordSpecLike
  with MustMatchers
  with BeforeAndAfterAll
  with AmqpSerializers
  with DirectLogging {

  import ImageStoreLiveTest._

  override implicit val formats = JsonSerializers.formats
  implicit val timeout = Timeout(1900 milliseconds)

  override val ApplicationId = "test"

  override protected def afterAll(): Unit = {
    super.afterAll()
    system.shutdown()
  }

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    FileUtils.deleteDirectory(tempFolder)
    tempFolder.mkdirs()
  }

  "ImageStore" ignore {
    val clientCount = 5
    val messageCount = 200
    val timeout = (messageCount * 4) seconds

    "keep the pace handling the messages" in {

      val clients = (0 until clientCount).toList map { i ⇒
        system.actorOf(Props(new ImageStoreClient(getRabbitMQConnection("imagestore-client" + i))))
      }

      val baseTime = System.currentTimeMillis()
      for (i ← 0 to messageCount) {
        val client: ActorRef = clients(i % clientCount)
        val time = baseTime + i
        val imageId = "id" + i
        // val request = ImageContentPutRequest(imageId, time, imageBytes, imageChecksum)
        val file = new File(tempFolder, imageId)
        FileUtils.writeByteArrayToFile(file, imageBytes)
        val request = ImagePutRequest(imageId, time, file.getAbsolutePath, imageChecksum)
        client ! request
      }

      clients.foreach(_ ! PoisonPill)

      def terminated = clients.forall(_.isTerminated)

      awaitCond(terminated, timeout, 1 second)
    }
  }

  def getRabbitMQConnection(actorName: String): MQConnection = {
    MQConnection.create(actorName, uri, 5 seconds)(system)
  }
}

class ImageStoreClient(connection: MQConnection) extends Actor with ActorLogging with AmqpSerializers {

  override val ApplicationId = "ImageStoreClient"
  override val formats = JsonSerializers.formats

  var producer: ActorRef = _

  override def preStart() {
    try {
      producer = connection.producer
    } catch {
      case e: Throwable ⇒
        e.printStackTrace
        throw e
    }
  }

  def receive = {
    //case msg: ImageContentPutRequest ⇒ send(msg)
    case msg: ImagePutRequest ⇒ send(msg)
    case Ok(_, _)             ⇒
    case msg                  ⇒ println("unknown: " + msg)
  }

  def send(request: ImageContentPutRequest): Unit = {
    import ImageStoreLiveTest._
    println("sending " + request.key)
    toPublishPayload[ImageContentPutRequest](request) match {
      case Right((body, properties)) ⇒ {
        val props = properties.builder().deliveryMode(2 /* persistent */ ).build()
        val publish = Publish(imageStoreEndpoint, imageStoreRouteKey, body, Some(props))
        producer ! publish
      }
      case Left(error) ⇒ log.error("Failed to serialize ImageContentPutRequest (id: {}) - {}", request.key, error)
    }
  }

  def send(request: ImagePutRequest): Unit = {
    import ImageStoreLiveTest._
    println("sending " + request.key)
    toPublishPayload[ImagePutRequest](request) match {
      case Right((body, properties)) ⇒ {
        val props = properties.builder().deliveryMode(2 /* persistent */ ).build()
        val publish = Publish(imageStoreEndpoint, imageStoreRouteKey, body, Some(props))
        producer ! publish
      }
      case Left(error) ⇒ log.error("Failed to serialize ImagePutRequest (id: {}) - {}", request.key, error)
    }
  }

}

object ImageStoreLiveTest {
  val uri = "amqp://guest:guest@localhost:5672"
  val imageStoreEndpoint = "gantry-out-exchange"
  val imageStoreRouteKey = "A4.E1.ImagePutRequest"
  val tempFolder = new File("/tmp/ImageStoreLiveTest_pending")

  val imageFile = new File("image-store/src/test/resources/image.jpg")
  lazy val imageBytes = FileUtils.readFileToByteArray(imageFile)
  lazy val imageChecksum = DigestUtils.md5Hex(imageBytes)

}