package csc.image.store

import java.io.{ File, IOException }

import scala.concurrent.duration._
import csc.image.store.Configuration.StorageConfiguration
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, WordSpec }

/**
 * @author csomogyi
 */
class ImageStoreFunctionsTest extends WordSpec with MustMatchers with BeforeAndAfterAll with DirectoryFunctions {
  val tempDir = createTempDirectory()
  val config = new StorageConfiguration(tempDir.getAbsolutePath, 4 hours, 7 days)
  val functions = ImageStoreFunctions(config)

  override def afterAll(): Unit = {
    deleteDirectory(tempDir)
  }

  "image store functions" must {
    "initialize the image store properly" in {
      deleteDirectory(tempDir)
      functions.initializeStore()
      assert(tempDir.exists && tempDir.isDirectory)
    }
    "throw exception on non-existing image" in {
      cleanupDirectory(tempDir)
      intercept[IOException] {
        functions.retrieveImage("alma")
      }
    }
    "create the specified file" in {
      cleanupDirectory(tempDir)
      val key = "alma"
      val creationTime = 987654321L
      val image = Array[Byte](104, 101, 108, 108, 111)
      functions.saveImage(key, creationTime, image)
      val hashCode = key.hashCode % 100
      val hash = if (hashCode < 10) "0" + hashCode else hashCode.toString
      val subdir = new File(tempDir, hash)
      val file = new File(subdir, key)
      assert(subdir.exists && subdir.isDirectory, subdir.getAbsolutePath + " does not exist")
      assert(file.exists && file.isFile, file.getAbsolutePath + " does not exist")
      assert(file.lastModified === creationTime / 1000 * 1000)
    }
    "retrieve the created file" in {
      cleanupDirectory(tempDir)
      val key = "alma"
      val creationTime = 987654321L
      val image = Array[Byte](104, 101, 108, 108, 111)
      functions.saveImage(key, creationTime, image)
      val (gotCreationTime, gotImage) = functions.retrieveImage(key)
      assert(gotImage === image)
      assert(gotCreationTime === gotCreationTime / 1000 * 1000) // should be rounded to second precision
      assert(gotCreationTime === creationTime / 1000 * 1000)
    }
    "overwrite the existing file" in {
      cleanupDirectory(tempDir)
      val key = "alma"
      val creationTime = 987654321L
      val image = Array[Byte](104, 101, 108, 108, 111)
      functions.saveImage(key, creationTime, image)
      val (gotCreationTime, gotImage) = functions.retrieveImage(key)
      assert(gotImage === image)
      assert(gotCreationTime === gotCreationTime / 1000 * 1000) // should be rounded to second precision
      assert(gotCreationTime === creationTime / 1000 * 1000)
      val newImage = Array[Byte](104, 101, 108, 108)
      val newCreationTime = 123456789000L
      functions.saveImage(key, newCreationTime, newImage)
      val (gotCreationTime2, gotImage2) = functions.retrieveImage(key)
      assert(gotImage2 === newImage)
      assert(gotCreationTime2 === gotCreationTime2 / 1000 * 1000) // should be rounded to second precision
      assert(gotCreationTime2 === newCreationTime / 1000 * 1000)
    }
    "fail to save image with negative creation time" in {
      cleanupDirectory(tempDir)
      val key = "alma"
      val creationTime = -5L
      val image = Array[Byte](104, 101, 108, 108, 111)
      val ex = intercept[IOException] {
        functions.saveImage(key, creationTime, image)
      }
      assert(ex.getMessage === "Invalid creation time specified")
    }
    "clean up expired files" in {
      cleanupDirectory(tempDir)
      val expiration = 7 * 24 * 60 * 60 * 1000 // 7 * d * h * m * s * ms
      val key = "alma"
      val creationTime = System.currentTimeMillis() - expiration - 60 * 60 * 1000 // 60 minutes earlier
      val image = Array[Byte](104, 101, 108, 108, 111)
      functions.saveImage(key, creationTime, image)
      val key2 = "lama"
      val image2 = Array[Byte](104, 101, 108, 108)
      val creationTime2 = System.currentTimeMillis() - expiration + 60 * 60 * 1000 // 60 minutes later
      functions.saveImage(key2, creationTime2, image2)

      val count = functions.cleanupOldImages(None)
      assert(count === 1)

      val hashCode = key.hashCode % 100
      val hash = if (hashCode < 10) "0" + hashCode else hashCode.toString
      val subdir = new File(tempDir, hash)
      val file = new File(subdir, key)
      assert(!file.exists(), "Expired file is not deleted")

      val hashCode2 = key2.hashCode % 100
      val hash2 = if (hashCode < 10) "0" + hashCode2 else hashCode2.toString
      val subdir2 = new File(tempDir, hash2)
      val file2 = new File(subdir2, key2)
      assert(file2.exists(), "Expired file must be deleted")
    }
  }
}
