package csc.image.store

import java.io.{ File, FileFilter }

import org.apache.commons.io.FileUtils

/**
 * Runs an inlined ImageStore
 * Created by carlos on 02.10.15.
 */
object InlineImageStore {

  val baseFolder = new File("/app")

  val allFilesFilter: FileFilter = new FileFilter {
    override def accept(pathname: File): Boolean = pathname.isFile
  }

  def main(args: Array[String]): Unit = {
    setAkkaHome("image-store")
    val mainArgs = Array("csc.image.store.Boot")
    akka.kernel.Main.main(mainArgs)
  }

  def setAkkaHome(appFolder: String): Unit = {
    val home = new File(appFolder)
    home.mkdirs()
    System.setProperty("akka.home", home.getAbsolutePath)
  }

  def copyDirContents(source: File, target: File): Unit = {
    if (source.exists()) {
      FileUtils.copyDirectory(source, target, allFilesFilter)
    }
  }

}
