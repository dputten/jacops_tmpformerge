/*
 * Copyright (C) 2015. CSC <http://www.csc.com>
 */

package csc.image.store

import csc.image.store.protocol._
import net.liftweb.json._
import org.apache.commons.codec.digest.DigestUtils
import org.scalatest.WordSpec
import org.scalatest._

/**
 * @author csomogyi
 */
class JsonSerializersTest extends WordSpec with MustMatchers with csc.amqp.JsonSerializers {
  override implicit val formats = csc.image.store.JsonSerializers.formats

  "JsonSerializers" must {
    "serialize ImageBatchProgressResponse" in {
      val payload = toJson(ImageBatchProgressResponse("1", 3, 1, 2))
      assert(payload.isRight)
      val json = parse(payload.right.get)
      assert(json \\ "refId" === JString("1"))
      assert(json \\ "total" === JInt(3))
      assert(json \\ "count" === JInt(1))
      assert(json \\ "errorCount" === JInt(2))
    }
    "serialize ImageGetResponse" in {
      val image = Array[Byte](104, 101, 108, 108, 111)
      val checksum = DigestUtils.md5Hex(image)
      val payload = toJson(ImageGetResponse("1", "alma", 1234567890, image, checksum, None))
      assert(payload.isRight, "Error in payload")
      val json = parse(payload.right.get)
      assert(json \\ "refId" === JString("1"))
      assert(json \\ "key" === JString("alma"))
      assert(json \\ "creationTime" === JString("1234567890"))
      assert(json \\ "image" === JArray(List(JInt(104), JInt(101), JInt(108), JInt(108), JInt(111))))
      assert(json \\ "checksum" === JString(checksum))
      assert(json \\ "error" === JNull)
    }
    "de-serialize ImageGetRequest" in {
      val payload = """{ "refId": "1", "key": "alma" }"""
      val request = fromJson[ImageGetRequest](payload)
      assert(request.isRight, "Deserialization error")
      assert(request.right.get === ImageGetRequest("1", "alma"))
    }
    "deserialize ImageBatchGetRequest" in {
      val payload = """{ "refId": "1", "keys": ["alma", "lama"] }"""
      val request = fromJson[ImageBatchGetRequest](payload)
      assert(request.isRight, "Deserialization error")
      assert(request.right.get === ImageBatchGetRequest("1", Seq("alma", "lama")))
    }
    "deserialize ImagePutRequest" in {
      val image = Array[Byte](104, 101)
      val payload = """{ "key": "alma", "creationTime": "1234567890", "image": [104, 101], "checksum": "3B" }"""
      val request = fromJson[ImageContentPutRequest](payload)
      assert(request.isRight, "Deserialization error")
      assert(request.right.get === ImageContentPutRequest("alma", 1234567890, image, "3B"))
    }
  }
}
