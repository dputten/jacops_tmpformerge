/**
 * Copyright (C) 2015 CSC. <http://www.csc.com>
 */
package csc.image.store.actors

import java.io.{ FileNotFoundException, File, IOException }
import akka.actor.{ ActorLogging, Actor }
import csc.image.store.Configuration.StorageConfiguration
import csc.image.store.ImageStoreFunctions
import csc.image.store.protocol.{ ImagePutRequest, ImagePutResponse, ImageContentPutRequest }
import org.apache.commons.io.FileUtils

/**
 * It is an actor of processing [[ImageContentPutRequest]] messages and responding with [[ImagePutResponse]] to the sender.
 * It attempts to store the image and generates a status response.
 *
 * @author csomogyi
 */
class ImageStorePutActor(storageConfig: StorageConfiguration) extends Actor with ActorLogging {
  val functions = ImageStoreFunctions(storageConfig)

  override def receive: Receive = {
    case ImageContentPutRequest(key, creationTime, image, checksum) ⇒ save(key, creationTime, image, checksum, None)
    case ImagePutRequest(key, creationTime, imagePath, checksum) ⇒ {
      val file = new File(imagePath)
      save(key, creationTime, FileUtils.readFileToByteArray(file), checksum, Some(file))
    }
    case other ⇒ log.debug("Unrecognized message: " + other)
  }

  def save(key: String, creationTime: Long, image: ⇒ Array[Byte], checksum: String, fileToDelete: Option[File]): Unit = {
    try {
      if (functions.verifyChecksum(image, checksum)) {
        functions.saveImage(key, creationTime, image)
        log.debug("Image '{}' with creation time {} created", key, functions.toDisplayDate(creationTime))

        sender ! ImagePutResponse(key, None)

        fileToDelete.foreach(f ⇒
          try {
            f.delete()
          } catch {
            case e: IOException ⇒ log.warning("Failed to delete already processed image '{}'", f.getAbsolutePath)
          })
      } else {
        log.error("Failed to save image '{}' - Checksum error", key)
        sender ! ImagePutResponse(key, Some("Checksum error"))
      }
    } catch {
      case e: FileNotFoundException ⇒ {
        //can only happen for 'old' ImagePutRequest
        log.warning("Cannot save image '{}': file no longer present", key)
        sender ! ImagePutResponse(key, None) //TCA-97: we must ack this message, otherwise it will DOS the imagestore
      }
      case e: IOException ⇒ {
        log.error(e, "Failed to save image '{}'", key)
        sender ! ImagePutResponse(key, Some(e.getMessage))
      }
    }
  }

}
