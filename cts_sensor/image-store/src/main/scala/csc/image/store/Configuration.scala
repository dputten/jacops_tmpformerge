/**
 * Copyright (C) 2015 CSC. <http://www.csc.com>
 */
package csc.image.store

import java.util.concurrent.TimeUnit
import com.typesafe.config.Config
import csc.akka.logging.DirectLogging
import csc.amqp.AmqpConfiguration
import csc.image.store.Configuration.{ ServicesConfiguration, StorageConfiguration }
import csc.akka.config.ConfigUtil._
import scala.concurrent.duration._

/**
 * Configuration for Image Store
 *
 * @author csomogyi
 */
class Configuration(val amqp: AmqpConfiguration,
                    val imageStore: StorageConfiguration,
                    val services: ServicesConfiguration) extends DirectLogging {
}

object Configuration extends DirectLogging {

  class StorageConfiguration(val baseDirectory: String, val cleanupFrequency: FiniteDuration, val imageExpiration: Duration) {
    def this(config: Config) = this(
      config.getString("base-directory"),
      config.getFiniteDuration("cleanup-frequency"),
      config.getFiniteDuration("image-expiration"))
  }

  class ServicesConfiguration(val consumerQueue: String, val producerEndpoint: String, val producerRouteKeyPrefix: String,
                              val serversCount: Int) {
    def this(config: Config) = this(config.getString("consumer-queue"), config.getString("producer-endpoint"),
      config.getString("producer-route-key-prefix"), config.getInt("servers-count"))
  }

  def load(config: Config): Configuration = {
    val root = config.getConfig("image-store")
    val amqp = new AmqpConfiguration(root.getConfig("amqp-connection"))
    val imageStore = new StorageConfiguration(root.getConfig("storage"))
    val services = new ServicesConfiguration(root.getConfig("services"))
    new Configuration(amqp, imageStore, services)
  }
}