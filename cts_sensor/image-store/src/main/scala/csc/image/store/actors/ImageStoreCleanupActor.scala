/*
 * Copyright (C) 2015. CSC <http://www.csc.com>
 */

package csc.image.store.actors

import java.io.IOException

import akka.actor.{ Cancellable, ActorLogging, Actor }
import scala.concurrent.duration._
import csc.image.store.Configuration.StorageConfiguration
import csc.image.store.ImageStoreFunctions

/**
 * TODO RB: missing javadoc
 * @author csomogyi
 */
class ImageStoreCleanupActor(storageConfig: StorageConfiguration) extends Actor with ActorLogging {

  log.info("ImageStoreCleanupActor config: {}", storageConfig)

  implicit val executor = context.system.dispatcher

  val functions = ImageStoreFunctions(storageConfig)
  var ticker: Cancellable = _

  override def preStart(): Unit = {
    super.preStart()
    ticker = context.system.scheduler.schedule(0 second, storageConfig.cleanupFrequency, self, "cleanup")
    log.info("Cleanup scheduled to run in every {} hour(s)", storageConfig.cleanupFrequency.toHours)
  }

  override def postStop(): Unit = {
    ticker.cancel()
    log.info("Cleanup schedule cancelled")
    super.postStop()
  }

  override def receive: Receive = {
    //TODO RB better is to use an empty object instead of a string
    //object CleanupMsg
    // ....
    // case CleanupMsg =>
    case "cleanup" ⇒ {
      try {
        val count = functions.cleanupOldImages(Some(log))
        log.info("Cleaned up {} image(s)", count)
      } catch {
        case e: IOException ⇒ log.error(e, "Exception during cleanup")
      }
    }
  }
}
