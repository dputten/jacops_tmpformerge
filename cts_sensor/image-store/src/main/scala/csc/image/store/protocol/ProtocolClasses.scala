/*
 * Copyright (C) 2015. CSC <http://www.csc.com>
 */

package csc.image.store.protocol

import java.util.Arrays

/**
 * Request message for getting an image. Responded with [[ImageGetResponse]]
 *
 * @author csomogyi
 * @param refId a reference ID specified by the initiator - copied back in the response to provide correlation
 * @param key the key of the image, should contain only alphanumeric and some specials, eg. at-sign (@)
 */
case class ImageGetRequest(refId: String, key: String) extends Matcheable

/**
 *
 * Response to a an [[ImageGetRequest]]. If the image is in the store it returns the [[creationTime]], the image
 * content in the field [[image]] and an MD5 [[checksum]].
 *
 * Custom [[equals()]] must have been implemented due to different behavior on [[Array]] equality
 *
 * @param refId ID specified by the caller in the request
 * @param key Key of the requested image
 * @param creationTime the creation time of the image, the precision is seconds although it is expressed in milliseconds
 * @param image The image in a byte array - null on error
 * @param checksum MD5 checksum of the image - null on error
 * @param error error message when the get has failed
 */
case class ImageGetResponse(refId: String,
                            key: String,
                            creationTime: Long,
                            image: Array[Byte],
                            checksum: String,
                            error: Option[String]) extends ImageStoreResponse with Matcheable {
  override def equals(that: Any): Boolean = that match {
    case ImageGetResponse(refId1, key1, creationTime1, image1, checksum1, error1) ⇒
      refId == refId1 && key == key1 && creationTime == creationTime1 && Arrays.equals(image, image1) &&
        checksum == checksum1 && error == error1
    case _ ⇒ false
  }
}

/**
 * Create a request of batch get of images. It is responded with a series of [[ImageGetResponse]] messages and some
 * [[ImageBatchProgressResponse]] indicating the progress.
 *
 * @param refId arbitrary reference id used by the requestor to distinguish between the different requests
 * @param keys keys of the requested images
 */
case class ImageBatchGetRequest(refId: String, keys: Seq[String]) extends Matcheable {
  lazy val requests = keys.map(ImageGetRequest(refId, _))
  override def multiResponse: Boolean = true
}

/**
 * A progress message indicating how many successful or failed attempts of image requests are processed in the batch
 * so far.
 *
 * @param refId reference id used by the requestor
 * @param total the total number of images requested
 * @param count the number of images sent back so far
 * @param errorCount the number of images not found or failed to provide so far
 */
case class ImageBatchProgressResponse(refId: String, total: Int, count: Int, errorCount: Int)
  extends ImageStoreResponse with Matcheable {
  override def lastResponse: Boolean = total == (count + errorCount)
}

/**
 * Create a request of putting the image in the image store. If an image exists already with the same key it will be
 * overwritten.
 *
 * Custom [[equals()]] must have been implemented due to different behavior on [[Array]] equality
 * TODO RB add a little bit more doc, so the caller knows how to use this interface message
 * @param key the key of the image
 * @param creationTime the creation time of the image (resolution: milliseconds, precision: seconds)
 * @param image the byte array of the image content
 */
case class ImageContentPutRequest(key: String, creationTime: Long, image: Array[Byte], checksum: String) {
  override def equals(that: Any): Boolean = that match {
    case ImageContentPutRequest(key1, creationTime1, image1, checksum1) ⇒
      key == key1 && creationTime == creationTime1 && Arrays.equals(image, image1) && checksum == checksum1
    case _ ⇒ false
  }
}

case class ImagePutRequest(key: String, creationTime: Long, imagePath: String, checksum: String) {
  override def equals(that: Any): Boolean = that match {
    case ImageContentPutRequest(key1, creationTime1, imagePath1, checksum1) ⇒
      key == key1 && creationTime == creationTime1 && imagePath == imagePath1 && checksum == checksum1
    case _ ⇒ false
  }
}

/**
 * This is an internal message of the Image Store module. The image put service ([[ImageContentPutRequest]]) itself is
 * fire-n-forget, no response message is sent back to the client. However, this message is responded by the
 * [[csc.image.store.actors.ImageStorePutActor]]
 *
 * @param key the key used to store the image
 * @param error error message if any
 */
protected[store] case class ImagePutResponse(key: String, error: Option[String]) extends ImageStoreResponse

/**
 * Marker trait for indicating a response
 */
trait ImageStoreResponse
trait Matcheable {
  def refId: String //matching id
  def lastResponse: Boolean = false //indicates if a response and the last
  def multiResponse: Boolean = false //indicates if this is a request for multiple responses
}