package csc.image.store.actors

import java.util.concurrent.TimeUnit

import akka.actor.ActorLogging
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.OneForOneStrategy
import akka.actor.SupervisorStrategy.Restart
import akka.pattern.ask
import akka.util.Timeout
import com.github.sstone.amqp.Amqp.{ Ok, Reject, Ack, Delivery }
import csc.amqp.{ PublisherActor, AmqpSerializers }
import csc.image.store.protocol._
import csc.image.store.{ JsonSerializers, Configuration }

import scala.util.{ Failure, Success }

/**
 * The actor for handling the AMQP communication of image store. It is a bridge between the producer and consumer
 * actors of AMQP and the [[ImageStoreGetActor]] and [[ImageStorePutActor]] actors.
 *
 * @param producer the reference to the AMQP producer actor which sends out messages on the AMQP exchange
 * @param config the image store configuration
 *
 * @author csomogyi
 */
class ImageStoreAmqpActor(val producer: ActorRef, config: Configuration) extends PublisherActor
  with ActorLogging {

  log.info("ImageStoreAmqpActor config: {}", config)

  override val ApplicationId = "ImageStore"
  override implicit val formats = JsonSerializers.formats
  val servicesConfig = config.services
  val producerEndpoint = servicesConfig.producerEndpoint

  def producerRouteKey(msg: AnyRef): String = {
    val extension = msg match {
      case resp: ImageStoreResponse ⇒ msg.getClass.getSimpleName //short name, as it should
      case _                        ⇒ msg.getClass.getName //full name. Wont work, but at least helps troubleshooting
    }
    servicesConfig.producerRouteKeyPrefix + "." + extension
  }

  val getRef = context.actorOf(Props(new ImageStoreGetActor(config.imageStore)))
  val putRef = context.actorOf(Props(new ImageStorePutActor(config.imageStore)))

  // get and put actors are completely stateless servers if something is wrong we can only restart them
  // they will fail only if there's something very wrong (memory or other issue) or certain exceptions
  // are not properly covered because I/O issues are handled
  override val supervisorStrategy = OneForOneStrategy() {
    case _: Exception ⇒ Restart
  }

  object ImageGetRequestExtractor extends MessageExtractor[ImageGetRequest]
  object ImageBatchGetRequestExtractor extends MessageExtractor[ImageBatchGetRequest]
  object ImageContentPutRequestExtractor extends MessageExtractor[ImageContentPutRequest]
  object ImagePutRequestExtractor extends MessageExtractor[ImagePutRequest]

  override def receive: Receive = {
    case delivery @ ImageGetRequestExtractor(msg) ⇒ {
      val consumer = sender // this will be needed in a later version of Akka
      getRef ! msg
      consumer ! Ack(delivery.envelope.getDeliveryTag)
    }
    case delivery @ ImageBatchGetRequestExtractor(msg) ⇒ {
      val consumer = sender // this will be needed in a later version of Akka
      getRef ! msg
      consumer ! Ack(delivery.envelope.getDeliveryTag)
    }
    case delivery @ ImageContentPutRequestExtractor(msg) ⇒ {
      val consumer = sender // this will be needed in a later version of Akka
      // a little flow control is added here
      // we send the Ack only if we processed the request because the payload is big
      implicit val timeout = Timeout(2, TimeUnit.SECONDS) // 2 seconds should be enough
      val future = putRef ? msg

      future onComplete {
        case Success(ImagePutResponse(key, error)) ⇒ {
          log.info("Processed image put of '{}' - {}", key, error.getOrElse("OK"))
          // we don't send response message for ImagePut
          if (error.isEmpty)
            consumer ! Ack(delivery.envelope.getDeliveryTag)
          else
            consumer ! Reject(delivery.envelope.getDeliveryTag, false) // reject since we could not process it
        }
        case Failure(ex) ⇒
          log.error(ex, "Error processing ImageContentPutRequest with key {}", msg.key)
          consumer ! Reject(delivery.envelope.getDeliveryTag, false)
        case other ⇒ log.warning("Unexpected outcome: {}", other)
      }

      // scala 2.9.1
      //      future onSuccess {
      //        case ImagePutResponse(key, error) ⇒ {
      //          log.info("Processed image put of '{}' - {}", key, error.getOrElse("OK"))
      //          // we don't send response message for ImagePut
      //          if (error.isEmpty)
      //            consumer ! Ack(delivery.envelope.getDeliveryTag)
      //          else
      //            consumer ! Reject(delivery.envelope.getDeliveryTag, false) // reject since we could not process it
      //        }
      //      } onFailure {
      //        // we reject the request as we could not process it so the image store should be fixed
      //        case e: Exception ⇒ {
      //          log.error(e, "Error processing ImageContentPutRequest with key {}", msg.key)
      //          consumer ! Reject(delivery.envelope.getDeliveryTag, false)
      //        }
      //      }
    }
    case delivery @ ImagePutRequestExtractor(msg) ⇒ {
      val consumer = sender // this will be needed in a later version of Akka
      // a little flow control is added here
      // we send the Ack only if we processed the request because the payload is big
      implicit val timeout = Timeout(2, TimeUnit.SECONDS) // 2 seconds should be enough
      val future = putRef ? msg

      future onComplete {
        case Success(ImagePutResponse(key, error)) ⇒ {
          log.info("Processed image put of '{}' - {}", key, error.getOrElse("OK"))
          // we don't send response message for ImagePut
          if (error.isEmpty)
            consumer ! Ack(delivery.envelope.getDeliveryTag)
          else
            consumer ! Reject(delivery.envelope.getDeliveryTag, false) // reject since we could not process it
        }
        case Failure(ex) ⇒
          log.error(ex, "Error processing ImagePutRequest with key {}", msg.key)
          consumer ! Reject(delivery.envelope.getDeliveryTag, false)

        case other ⇒ log.warning("Unexpected outcome: {}", other)
      }

      // scala 2.9.1
      //      future onSuccess {
      //        case ImagePutResponse(key, error) ⇒ {
      //          log.info("Processed image put of '{}' - {}", key, error.getOrElse("OK"))
      //          // we don't send response message for ImagePut
      //          if (error.isEmpty)
      //            consumer ! Ack(delivery.envelope.getDeliveryTag)
      //          else
      //            consumer ! Reject(delivery.envelope.getDeliveryTag, false) // reject since we could not process it
      //        }
      //      } onFailure {
      //        // we reject the request as we could not process it so the image store should be fixed
      //        case e: Exception ⇒ {
      //          log.error(e, "Error processing ImagePutRequest with key {}", msg.key)
      //          consumer ! Reject(delivery.envelope.getDeliveryTag, false)
      //        }
      //      }
    }
    case delivery: Delivery ⇒ {
      val consumer = sender // this will be needed in a later version of Akka
      log.error("Dropping message with tag {} - cannot determine message type", delivery.envelope.getDeliveryTag)
      consumer ! Reject(delivery.envelope.getDeliveryTag, false)
    }
    // responses from ImageGetRequest and/or ImageBatchGetRequest
    case response: ImageGetResponse ⇒ {
      val payload = toPublishPayload(response)
      publishOrError(servicesConfig.producerEndpoint, producerRouteKey(response),
        payload, "ImageGetResponse")
    }
    case response: ImageBatchProgressResponse ⇒ {
      val payload = toPublishPayload(response)
      publishOrError(servicesConfig.producerEndpoint, producerRouteKey(response),
        payload, "ImageBatchProgressResponse")
    }
    case Ok(_, _) ⇒ //silently handle Oks
    case unhandled: Any ⇒ {
      log.warning("Unhandled message: {}", unhandled)
    }
  }
}
