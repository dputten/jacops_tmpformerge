/**
 * Copyright (C) 2015 CSC. <http://www.csc.com>
 */
package csc.image.store.actors

import java.io.{ FileNotFoundException, IOException }

import akka.actor.{ ActorLogging, Actor }
import csc.image.store.Configuration.StorageConfiguration
import csc.image.store.ImageStoreFunctions
import csc.image.store.protocol.{ ImageBatchProgressResponse, ImageBatchGetRequest, ImageGetResponse, ImageGetRequest }

/**
 * Actor for serving image store get ([[ImageGetRequest]]) and batch get ([[ImageBatchGetRequest]]) requests.
 * The sender is responded with [[ImageGetResponse]] and/or [[ImageBatchProgressResponse]]
 *
 * @author csomogyi
 */
class ImageStoreGetActor(storageConfig: StorageConfiguration) extends Actor with ActorLogging {
  val functions = ImageStoreFunctions(storageConfig)

  /**
   * Batch progress is sent after every 10th processing of an image
   */
  val ProgressCheckCount = 10

  /**
   * Process a single get request
   *
   * @param request request to be processed
   * @return the response message with the outcome
   */
  def processGetRequest(request: ImageGetRequest): ImageGetResponse = {
    val refId = request.refId
    val key = request.key
    try {
      val (creationTime, image) = functions.retrieveImage(key)
      log.debug("Image '{}' with creation time {} retrieved", key, functions.toDisplayDate(creationTime))
      ImageGetResponse(refId, key, creationTime, image, functions.createChecksum(image), None)
    } catch {
      case e: FileNotFoundException ⇒ {
        log.warning("File not found '{}'", key)
        failedToRetrieve(request, e)
      }
      case e: IOException ⇒ {
        log.error(e, "Failed to retrieve image '{}'", key)
        failedToRetrieve(request, e)
      }
    }
  }

  def failedToRetrieve(request: ImageGetRequest, ex: Exception): ImageGetResponse = {
    val refId = request.refId
    val key = request.key
    ImageGetResponse(refId, key, -1, null, "", Some("Failure retrieving image '" + key + "' - " + ex.getMessage))
  }

  override def receive: Receive = {
    case request: ImageGetRequest ⇒ {
      sender ! processGetRequest(request)
    }
    case batch: ImageBatchGetRequest ⇒ {
      val refId = batch.refId
      val keys = batch.keys
      val total = keys.size
      var count = 0
      var errorCount = 0
      def subTotal = count + errorCount
      sender ! ImageBatchProgressResponse(refId, total, count, errorCount)
      log.debug("Batch progress '{}' ({}+{}/{})", refId, count, errorCount, total)
      for (key ← keys) {
        val response = processGetRequest(ImageGetRequest(refId, key))
        sender ! response // we send the response anyway
        if (response.error.isDefined) errorCount += 1
        else count += 1
        if (subTotal % ProgressCheckCount == 0 || subTotal == total)
          sender ! ImageBatchProgressResponse(refId, total, count, errorCount)
        log.debug("Batch progress '{}' ({}+{}/{})", refId, count, errorCount, total)
      }
      log.info("Completed batch '{}' ({}+{}/{})", refId, count, errorCount, total)
    }
    case msg: Any ⇒ {
      log.debug("Unrecognized message")
    }
  }
}
