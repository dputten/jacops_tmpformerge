/*
 * Copyright (C) 2015. CSC <http://www.csc.com>
 */

package csc.image.store

import akka.kernel.Bootable
import akka.routing.RoundRobinRouter
import com.github.sstone.amqp.Amqp.ChannelParameters
import com.github.sstone.amqp.Amqp
import com.typesafe.config.ConfigFactory
import csc.akka.logging.DirectLogging
import akka.actor.{ Props, ActorSystem }
import csc.amqp.MQConnection
import csc.image.store.actors.{ ImageStoreCleanupActor, ImageStoreAmqpActor }

/**
 * Boot class for Image Store
 *
 * @author csomogyi
 */
private[store] class Boot extends Bootable with DirectLogging {
  implicit val actorSystem = ActorSystem("ImageStore")

  def loadConfiguration(): Configuration = {
    Configuration.load(ConfigFactory.load("imageStore"))
  }

  def startup() = {
    log.info("Starting Image Store")
    val config = loadConfiguration()
    ImageStoreFunctions(config.imageStore).initializeStore()
    log.info("Storage initialized")
    val rmqConnection = MQConnection.create(config.amqp.actorName, config.amqp.amqpUri, config.amqp.reconnectDelay)
    log.info("RabbitMQ connection created")
    actorSystem.actorOf(Props(new ImageStoreCleanupActor(config.imageStore)))
    log.info("Cleanup process started")
    createServer(rmqConnection, config)
    log.info("Image Store started up")
  }

  def createServer(connection: MQConnection, config: Configuration): Unit = {
    val producer = connection.producer
    log.info("Producer created and connected")
    val listener = actorSystem.actorOf(Props(new ImageStoreAmqpActor(producer, config))
      .withRouter(RoundRobinRouter(nrOfInstances = config.services.serversCount)))
    log.info("Listener actor pool ({}) created", config.services.serversCount)
    // with ChannelParameters we set the number of messages being fetched without Ack (prefetch count)
    connection.createSimpleConsumer(config.services.consumerQueue, listener,
      Some(ChannelParameters(config.services.serversCount)), false)
    log.info("Consumer created and connected")
  }

  def shutdown() = {
    log.info("Shutting down Image Store")
    actorSystem.shutdown()
  }
}
