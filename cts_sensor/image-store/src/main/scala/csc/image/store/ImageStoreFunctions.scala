/*
 * Copyright (C) 2015. CSC <http://www.csc.com>
 */

package csc.image.store

import java.io.{ FileFilter, File, FileNotFoundException, IOException }
import java.security.MessageDigest
import java.text.{ SimpleDateFormat, DateFormat }
import java.util.{ Locale, Date }

import akka.event.LoggingAdapter
import csc.akka.logging.DirectLogging
import csc.image.store.Configuration.StorageConfiguration
import org.apache.commons.codec.digest.DigestUtils
import org.apache.commons.io.FileUtils

import scala.util.DynamicVariable

/**
 * @author csomogyi
 */
class ImageStoreFunctions(storageConfig: StorageConfiguration) {
  import ImageStoreFunctions._

  private val formatter = new DynamicVariable[DateFormat](new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ"))

  private val baseDir = new File(storageConfig.baseDirectory)

  private def hash(key: String): String = {
    val hashVal = math.abs(key.hashCode % 100)
    if (hashVal < 10) "0" + hashVal.toString
    else hashVal.toString
  }

  /**
   * The granularity of modification time on many (Unix/Linux) systems is one second. Therefore we round
   * every time value to second precision
   *
   * @param time
   * @return
   */
  private def timeCorrection(time: Long): Long = time / 1000 * 1000

  def initializeStore(): Unit = {
    FileUtils.forceMkdir(baseDir)
  }

  @throws(classOf[IOException])
  def retrieveImage(key: String): (Long, Array[Byte]) = {
    storedFile(key) match {
      case Right(file) ⇒
        val creationTime = timeCorrection(file.lastModified)
        val image = FileUtils.readFileToByteArray(file)
        (creationTime, image)
      case Left(error) ⇒ throw error
    }
  }

  @throws(classOf[IOException])
  def saveImage(key: String, creationTime: Long, image: Array[Byte]): Unit = {
    val subdir = new File(baseDir, hash(key))
    val filename = normalizedFilename(key) //TCA-77: saves with a normalized filename (flat structure inside each folder)
    val file = new File(subdir, filename)
    // TODO should we check if file already exists?
    FileUtils.writeByteArrayToFile(file, image)
    try {
      if (!file.setLastModified(creationTime)) {
        file.delete()
        throw new IOException("Unable to set creation time")
      }
    } catch {
      case _: IllegalArgumentException ⇒ throw new IOException("Invalid creation time specified")
    }
  }

  /**
   * Returns the already stored file for the given uri, of the exception for not finding it
   * @param uri
   * @return
   */
  def storedFile(uri: String): Either[FileNotFoundException, File] = {
    if (baseDir.exists && baseDir.isDirectory) {
      val subdir = new File(baseDir, hash(uri))
      val normalized = new File(subdir, normalizedFilename(uri)) //TCA-77: tries to find first the the normalized name
      if (normalized.isFile && normalized.exists()) {
        Right(normalized)
      } else {
        //TODO: remove this fallback a couple of weeks after the fix for TCA-77 is deployed
        val legacy = new File(subdir, uri) //TCA-77: if not there, tries the legacy file (where the uri is expanded into folders)
        if (legacy.isFile && legacy.exists()) {
          Right(legacy)
        } else {
          Left(new FileNotFoundException("Image with key '" + uri + "' does not exist"))
        }
      }
    } else {
      Left(new FileNotFoundException("Image Store does not exist"))
    }
  }

  @throws(classOf[IOException])
  def cleanupOldImages(log: Option[LoggingAdapter]): Int = {
    var count = 0
    val margin = System.currentTimeMillis() - storageConfig.imageExpiration.toMillis
    val subDirs = baseDir.listFiles(dirFilter)
    log.foreach(_.info("Cleaning files last modified before " + formatter.value.format(new Date(margin))))
    for (subDir ← subDirs) {
      val files = subDir.listFiles(fileFilter) //TCA-77: only checks files for deletion, not folders
      for (file ← files) {
        if (file.lastModified < margin) {
          // file is older than margin
          FileUtils.forceDelete(file)
          log.foreach(_.info("Deleted file " + file.getAbsolutePath))
          count += 1
        }
      }
    }
    count
  }

  def toDisplayDate(time: Long): String = {
    val date = new Date(time)
    formatter.value.format(date)
  }

  def verifyChecksum(image: Array[Byte], checksum: String): Boolean =
    DigestUtils.md5Hex(image) == checksum

  def createChecksum(image: Array[Byte]): String = DigestUtils.md5Hex(image)
}

object ImageStoreFunctions {
  val fileFilter = new FileFilter {
    override def accept(file: File): Boolean = file.isFile
  }
  val dirFilter = new FileFilter {
    override def accept(file: File): Boolean = file.isDirectory && file.getName.matches("[0-9][0-9]")
  }

  def normalizedFilename(uri: String): String = uri.replaceAll("/", "_")

  def apply(storageConfig: StorageConfiguration) = new ImageStoreFunctions(storageConfig)
}
