/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.gantry.config

import org.scalatest.{ MustMatchers, WordSpec }

class GantryConfigurationTest extends WordSpec with MustMatchers {

  "configure" must {
    "with empty config file" in {
      val config = new Configuration(None)
      val cfg = config.gantryConfiguration.get
      cfg.systemId must be("A2")
      cfg.gantryId must be("Unknown")
      cfg.pirOn must be(true)
      config.laneConfiguration.isInstanceOf[EmptyLaneConfiguration] must be(true)
    }
    "with config file" in {
      val configFile = getClass.getClassLoader.getResource("csc/gantry/config/testFullGantry.conf").getPath
      val config = Configuration.createFromFile(configFile)
      val cfg = config.gantryConfiguration.get
      cfg.systemId must be("A12")
      cfg.gantryId must be("44HM")
      cfg.pirOn must be(false)
      config.laneConfiguration.isInstanceOf[EmptyLaneConfiguration] must be(true)
    }
  }
}
