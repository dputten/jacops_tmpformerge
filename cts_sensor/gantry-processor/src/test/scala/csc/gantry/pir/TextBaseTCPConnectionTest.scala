package csc.gantry.pir

import java.io.{ BufferedReader, InputStreamReader, PrintWriter }
import java.net.ServerSocket
import java.util.Date

import scala.concurrent.duration._
import org.scalatest.WordSpec
import org.scalatest._

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

class TextBaseTCPConnectionTest extends WordSpec with MustMatchers {

  "Connection" must {
    "send messages when direct connected" in {
      var recvServer = new ServerSocket(14001)
      var fileName: String = "my_file"
      var testClass = new TextBasedTCPConnection("localhost", 14001, 1.minute)
      testClass.sendText(fileName)
      System.out.println("Start accept")
      var sock = recvServer.accept
      var recv = new BufferedReader(new InputStreamReader(sock.getInputStream))
      System.out.println("Start reading")
      var result: String = recv.readLine
      result must be(fileName)
      recv.close
      recvServer.close
    }
    "send messages when after connected" in {
      var fileName1 = "my_file"
      var testClass = new TextBasedTCPConnection("localhost", 14001, 1.minute)
      testClass.sendText(fileName1)
      System.out.println("Start accept")
      var recvServer = new ServerSocket(14001)
      var fileName2 = "my_second_file"
      testClass.sendText(fileName2)
      var sock = recvServer.accept
      var recv = new BufferedReader(new InputStreamReader(sock.getInputStream))
      System.out.println("Start reading")
      var result = recv.readLine
      result must be(fileName1)
      result = recv.readLine
      result must be(fileName2)
      recv.close
      recvServer.close
    }

    "send messages after stop and start" in {
      var recvServer = new ServerSocket(14001)
      var fileName = "my_file"
      var testClass = new TextBasedTCPConnection("localhost", 14001, 1.minute)
      testClass.sendText(fileName)
      System.out.println("Start accept")
      var sock = recvServer.accept
      var recv = new BufferedReader(new InputStreamReader(sock.getInputStream))
      System.out.println("Start reading")
      var result = recv.readLine
      var writer = new PrintWriter(sock.getOutputStream)
      writer.println("OK")
      writer.flush
      sock.getOutputStream.flush
      result must be(fileName)
      recv.close
      recvServer.close
      recvServer = new ServerSocket(14001)
      var fileName2: String = "my_file2"
      testClass.sendText(fileName2)
      System.out.println("Start accept")
      sock = recvServer.accept
      recv = new BufferedReader(new InputStreamReader(sock.getInputStream))
      System.out.println("Start reading")
      result = recv.readLine
      result must be(fileName2)
      recv.close
      recvServer.close
    }
    "skip delayed messages when after connected" in {
      var fileName1 = "my_file"
      var testClass = new TextBasedTCPConnection("localhost", 14001, 30.seconds)
      testClass.sendText(fileName1)
      System.out.println("Start accept")
      var recvServer = new ServerSocket(14001)
      var fileName2 = "my_second_file"
      val oneMinuteFromNow = new Date(System.currentTimeMillis() + 60000)
      testClass.sendText(fileName2, oneMinuteFromNow)
      var sock = recvServer.accept
      var recv = new BufferedReader(new InputStreamReader(sock.getInputStream))
      System.out.println("Start reading")
      var result = recv.readLine
      result must be(fileName2)
      recv.close
      recvServer.close
    }
    "skip delayed messages when after connected2" in {
      var fileName1 = "my_file"
      var testClass = new TextBasedTCPConnection("localhost", 14001, 30.seconds)
      testClass.sendText(fileName1)
      val oneMinuteFromNow = new Date(System.currentTimeMillis() + 60000)
      var fileName2 = "my_second_file"
      testClass.sendText(fileName2, oneMinuteFromNow)
      System.out.println("Start accept")
      var recvServer = new ServerSocket(14001)
      var fileName3 = "my_last_file"
      val oneAndBitMinuteFromNow = new Date(System.currentTimeMillis() + 50000)
      testClass.sendText(fileName3, oneAndBitMinuteFromNow)
      var sock = recvServer.accept
      var recv = new BufferedReader(new InputStreamReader(sock.getInputStream))
      System.out.println("Start reading")
      var result = recv.readLine
      result must be(fileName2)
      result = recv.readLine
      result must be(fileName3)
      recv.close
      recvServer.close
    }

  }

}