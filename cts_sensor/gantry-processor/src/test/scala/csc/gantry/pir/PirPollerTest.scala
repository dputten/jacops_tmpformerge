/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.gantry.pir

import akka.actor.{ Actor, ActorRef, ActorSystem, Props }
import akka.camel.{ CamelExtension, CamelMessage, Consumer }
import akka.testkit.{ TestKit, TestProbe }
import akka.util.Timeout
import csc.akka.CamelClient
import scala.concurrent.Await
import scala.concurrent.duration._
import csc.akka.logging.DirectLogging
import csc.gantry.config._
import csc.vehicle.message.Lane
import csc.vehicle.pir.VehiclePIRRegistration
import net.liftweb.json.{ DefaultFormats, Serialization }
import org.scalatest._

class PirPollerTest
  extends TestKit(ActorSystem("PirPollerTest"))
  with WordSpecLike
  with MustMatchers
  with BeforeAndAfterAll
  with CamelClient
  with DirectLogging {

  val config = new PIRConfig("localhost", 9000, 1, 1000, vrHost = "localhost", vrPort = 9100)
  val lane = new Lane("A2-20MH-Lane1", "Lane1", "20MH", "A2", 10, 10)

  override def afterAll() {
    system.shutdown
  }

  "pirpoller" must {
    "collect pir messages and send them" in {
      val probe = TestProbe()
      val vrMock = system.actorOf(Props(new VRMock(config.vrHost, config.vrPort, probe.ref)))
      awaitActivation(vrMock, 10 seconds)
      val poller = system.actorOf(Props(new PirPoller(lane, config, 5.minute)))
      val speed = 80F
      val length = 4.50F
      val category = 4
      val confidence = 70
      val time = System.currentTimeMillis()

      val msg = new VehiclePIRRegistration(lane, time, Some(speed), Some(length), Some(category), Some(confidence))
      poller ! msg

      val recv = probe.expectMsgType[VehiclePIRRegistration](20 seconds)
      recv.lane must be(lane)
      recv.eventTime must be(time)
      recv.speed.get must be(speed)
      recv.length.get must be(length)
      recv.category.get must be(category)
      recv.confidence.get must be(confidence)

    }
  }

}

class VRMock(host: String, port: Int, next: ActorRef) extends Actor with Consumer {
  def endpointUri = "mina:tcp://%s:%d?textline=true&sync=false".format(host, port)
  implicit val formats = DefaultFormats
  def receive = {
    case msg: CamelMessage ⇒ {
      val str = msg.bodyAs[String]
      val msgObject = Serialization.read[VehiclePIRRegistration](str)
      next ! msgObject
    }
  }
}