/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.gantry.config

import com.typesafe.config.{ Config, ConfigFactory }
import csc.akka.logging.DirectLogging
import csc.akka.config.ConfigUtil._
import java.io.File
import csc.curator.utils.{ CuratorToolsImpl, Curator }
import org.apache.curator.retry.RetryUntilElapsed
import org.apache.curator.framework.CuratorFrameworkFactory
import net.liftweb.json.DefaultFormats
import csc.json.lift.EnumerationSerializer
import csc.vehicle.message.VehicleImageType
import csc.curator.utils.xml.{ XmlReader, CuratorToolsImplXml }

import scala.concurrent.duration._

case class GantryConfiguration(systemId: String, gantryId: String, pirOn: Boolean, ttl: Duration, maxResponse: FiniteDuration, dynamicConfidence: Boolean, skipLanes: Seq[String])

/**
 * Reading the configuration file and create the VehicleRegistration Configuration classes
 * @param configuration, The application configuration file
 */
class Configuration(configuration: Option[Config]) extends DirectLogging {
  val rootConfig = "gantry-processor"
  var laneConfiguration: LaneConfiguration = new EmptyLaneConfiguration
  var gantryConfiguration: Option[GantryConfiguration] = None
  var curator: Option[Curator] = None
  process

  def getZookeeperCurator(zkServerQuorum: String): Curator = {
    curator.getOrElse {
      val retryPolicy = new RetryUntilElapsed(3.days.toMillis.toInt, 5000)
      val client = CuratorFrameworkFactory.newClient(zkServerQuorum, retryPolicy)
      client.start()
      val formats = DefaultFormats + new EnumerationSerializer(VehicleImageType)
      val impl = new CuratorToolsImpl(Some(client), log, formats)
      curator = Some(impl)
      impl
    }
  }

  def getXmlCurator(fileLocation: String): Curator = {
    curator.getOrElse {
      val formats = DefaultFormats + new EnumerationSerializer(VehicleImageType)
      val impl = new CuratorToolsImplXml(new XmlReader(fileLocation, log), log, formats)
      curator = Some(impl)
      impl
    }
  }

  /**
   * Initialize the different configuration items
   */
  def process() {
    var config = configuration
    val fallback = ConfigFactory.parseResources(this.getClass, "/defaultGantry.conf")
    config match {
      case None      ⇒ config = Some(fallback)
      case Some(cfg) ⇒ config = Some(cfg.withFallback(fallback))
    }
    config match {
      case Some(cfgMerged) ⇒ {
        gantryConfiguration = Some(createGantryConfig(cfgMerged))
        laneConfiguration = createDynamicConfig(cfgMerged)
      }
      case None ⇒ throw new IllegalStateException("Failed to load Configuration")
    }
  }

  /**
   * Create the LaneConfiguration implementation
   * @param config
   * @return The Laneconfiguration implementation which has to be used
   */
  def createDynamicConfig(config: Config): LaneConfiguration = {
    val service = config.getReplacedString(rootConfig + ".laneConfig.zookeeper.service")
    if (service == "on") {
      log.info("Using zookeeper for LaneConfiguration")

      val labelPath = {
        val path = config.getReplacedString(rootConfig + ".laneConfig.zookeeper.labelPath", "")
        if (path.isEmpty) {
          None
        } else {
          Some(path)
        }
      }

      var curator: Curator = null
      val appConfig: Config = ConfigFactory.load("gantry.conf")
      appConfig.getString("curator_implementation") match {
        case "xml" ⇒
          curator = getXmlCurator(appConfig.getString("curator_implementation_xml_file_location"))
        case "zookeeper" ⇒
          val servers = config.getReplacedString(rootConfig + ".laneConfig.zookeeper.servers")
          curator = getZookeeperCurator(servers)
      }

      new ZookeeperLaneConfiguration(curator, "/ctes/systems/", labelPath)

    } else {
      val fileService = config.getReplacedString(rootConfig + ".laneConfig.file.service")
      if (fileService == "on") {
        val file = config.getReplacedString(rootConfig + ".laneConfig.file.fileName")
        log.info("Using jsonFile %s for LaneConfiguration".format(file))
        new FileLaneConfiguration(file, None)
      } else {
        log.info("Using Empty LaneConfiguration")
        new EmptyLaneConfiguration
      }
    }
  }

  /**
   * Create the gantry configuration structure
   * @param config
   * @return GantryConfiguration the gantry configuration
   */
  private def createGantryConfig(config: Config): GantryConfiguration = {
    val systemId = config.getReplacedString(rootConfig + ".location.systemId")
    val gantryId = config.getReplacedString(rootConfig + ".location.gantryId")

    val pirService = config.getReplacedString(rootConfig + ".pirPoller.service") == "on"

    val ttl = config.getReplacedInteger(rootConfig + ".pirPoller.ttl")
    val maxResponse = config.getReplacedInteger(rootConfig + ".pirPoller.maxResponseTime")
    val dynamicConfidence = config.getReplacedString(rootConfig + ".pirPoller.dynamicConfidence").toBoolean

    val skipPath = rootConfig + ".pirPoller.skipLanes"
    val skipList = if (config.hasPath(skipPath)) {
      val skipLanes = config.getReplacedString(skipPath)
      skipLanes.split(",").toSeq
    } else {
      Seq()
    }
    new GantryConfiguration(systemId = systemId, gantryId = gantryId, pirOn = pirService, ttl = ttl.seconds, maxResponse = maxResponse.millis, dynamicConfidence = dynamicConfidence, skipLanes = skipList)
  }

}
/**
 * Create configuration class
 */
object Configuration extends DirectLogging {
  /*
  *create configuration.
  * It first tries to check if there is a systemproperty set
  * if not then the classpath is checked for a file "registration.conf"
  * if not check the AKKA config dir
  * @return Configuration class
  */
  def create(): Configuration = {
    var config: Option[Config] = None
    val gantryConfigFile = "gantry.conf"
    val configFile = System.getProperty(gantryConfigFile, "")
    if (configFile != "") {
      config = Some(ConfigFactory.parseFile(new File(configFile)))
      log.info("Config loaded from -D%s=%s".format(gantryConfigFile, configFile))
    } else {
      val resource = getClass.getClassLoader.getResource(gantryConfigFile)
      if (resource != null) {
        config = Some(ConfigFactory.parseFile(new File(resource.getPath)))
        log.info("Config loaded from resource %s".format(resource.getPath))
      } else {
        Option(System.getenv("AKKA_HOME")) match {
          case Some(akkaHome) ⇒ {
            val configFile = akkaHome + "/config/" + gantryConfigFile
            config = Some(ConfigFactory.parseFile(new File(configFile)))
            log.info("AKKA_HOME is defined as [%s], registration config loaded from [%s].".format(akkaHome, configFile))
          }
          case None ⇒ {
            log.warning("Use default configuration! Can't load 'registration.conf'." +
              "\nOne of the three ways of locating the 'registration.conf' file needs to be defined:" +
              "\n\t1. Define the '-registration.conf=...' system property option." +
              "\n\t2. Put the 'registration.conf' file on the classpath." +
              "\n\t3. Define 'AKKA_HOME' environment variable pointing to the root of the Akka distribution.")
          }
        }
      }
    }
    new Configuration(config)
  }

  /**
   * create configuration baased on the given configuration file
   * @param fileName
   * @return Configuration class
   */
  def createFromFile(fileName: String): Configuration = {
    val config = Some(ConfigFactory.parseFile(new File(fileName)))
    new Configuration(config)
  }

}