/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.gantry.pir

import csc.vehicle.pir.VehiclePIRRegistration
import net.liftweb.json.{ DefaultFormats, Serialization }
import csc.gantry.config.PIRConfig
import akka.actor.{ ActorLogging, Actor }
import csc.vehicle.message.Lane

import scala.concurrent.duration.Duration

/**
 * This class starts connect the Pir connection with the sending of the received vehicleRegistrations
 * It is also responsible for starting and stopping the pir Connection
 * @param config the Pir lane configuration
 */
class PirPoller(lane: Lane, config: PIRConfig, ttl: Duration) extends Actor with ActorLogging {
  val textConnection = new TextBasedTCPConnection(config.vrHost, config.vrPort, ttl)

  def receive = {
    case msg: VehiclePIRRegistration ⇒ {
      log.info("%s: Received Vehicle: %s".format(lane.laneId, msg))
      val strMsg = Serialization.write(msg)(DefaultFormats)
      textConnection.sendText(strMsg)
    }
  }
}