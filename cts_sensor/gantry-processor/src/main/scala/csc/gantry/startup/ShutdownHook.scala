/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.gantry.startup

import csc.akka.logging.DirectLogging
import akka.actor.ActorSystem

/**
 * This is the Shutdown Hook called when receiving an end program interrupt.
 * When shutting down all the known routes are stopped
 */
class ShutdownHook(actorSystem: ActorSystem) extends Thread with DirectLogging {
  override def run() {
    log.info("Start shutdown Gantry processor");
    actorSystem.shutdown()
    log.info("Gantry processor has stopped")
  }
}