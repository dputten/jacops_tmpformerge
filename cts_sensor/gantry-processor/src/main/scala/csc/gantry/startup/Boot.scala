/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.gantry.startup

import csc.akka.logging.DirectLogging
import akka.kernel.Bootable
import csc.gantry.config.Configuration
import akka.actor.ActorSystem

/**
 * Boot class Starts VehicleRegistration
 */
private[startup] class Boot extends Bootable with DirectLogging {
  val actorSystem = ActorSystem("GantryProcessor")
  def startup = {
    log.info("Starting Gantry Processor")
    val config = Configuration.create() //find configuration and process the file

    config.gantryConfiguration match {
      case Some(globalConfig) ⇒ {
        StartupGantryProcessor.startGantry(globalConfig, config.laneConfiguration, actorSystem)
        log.info("Gantry Processor is started")
      }
      case None ⇒ {
        log.error("Configuration missing. Gantry Processor is NOT started")
      }
    }
  }

  def shutdown = {
    (new ShutdownHook(actorSystem)) run ()
  }
}
