/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.gantry.startup

import akka.actor._
import csc.akka.logging.DirectLogging
import csc.gantry.config._
import csc.gantry.pir.PirPoller
import csc.vehicle.pir.impl.{ PIRDirectorActor, PIRLaneConfiguration }
import csc.gantry.config.PirRadarCameraSensorConfig
import csc.gantry.config.GantryConfiguration
import csc.gantry.config.PirCameraSensorConfig

/**
 * Helper to start the complete gantry processor
 */
object StartupGantryProcessor extends DirectLogging {

  /**
   * Creating actors and start the image server.
   * returns when the VehicleRegistration is up and running
   */
  def startGantry(config: GantryConfiguration, laneConfig: LaneConfiguration, actorSystem: ActorSystem) = {

    println("StartupGantryProcessor config: {}", config)

    if (config.pirOn) {
      val laneConfigList = laneConfig.getLanesForGantry(config.systemId, config.gantryId)
      log.info("Found %d LaneConfigurations for system %s and gantry %s".format(laneConfigList.size, config.systemId, config.gantryId))
      val filteredList = laneConfigList.filterNot(cfg ⇒ config.skipLanes.contains(cfg.lane.laneId))
      log.info("Skipping %d lanes %s".format(config.skipLanes.size, config.skipLanes))
      log.info("Starting %d LaneConfigurations".format(filteredList.size))

      val grouped = filteredList.groupBy {
        case pirCfg: PirRadarCameraSensorConfig ⇒ {
          pirCfg.pir.pirHost + ":" + pirCfg.pir.pirPort
        }
        case pirCfg: PirCameraSensorConfig ⇒ {
          pirCfg.pir.pirHost + ":" + pirCfg.pir.pirPort
        }
        case laneCfg: LaneConfig ⇒ {
          log.warning("Lane didn't have PIR configuration %s".format(laneCfg.lane.laneId))
          "NoPir"
        }
      }

      grouped.foreach {
        case (key, laneCfgList) ⇒ {
          if (key != "NoPir") {
            log.info("Starting PIR connection %s".format(key))
            val pirConfig = laneCfgList.map {
              case pirCfg: PirRadarCameraSensorConfig ⇒ {
                val log = actorSystem.actorOf(Props(new PirPoller(pirCfg.lane, pirCfg.pir, config.ttl)), pirCfg.lane.laneId)
                new PIRLaneConfiguration(lane = pirCfg.lane, pir = pirCfg.pir, recipients = Seq(log))
              }
              case pirCfg: PirCameraSensorConfig ⇒ {
                val log = actorSystem.actorOf(Props(new PirPoller(pirCfg.lane, pirCfg.pir, config.ttl)), pirCfg.lane.laneId)
                new PIRLaneConfiguration(lane = pirCfg.lane, pir = pirCfg.pir, recipients = Seq(log))
              }
            }
            log.info("PIRDirectorActor config: {}", pirConfig)
            actorSystem.actorOf(Props(new PIRDirectorActor(timeout = config.maxResponse, config = pirConfig, dynamicConfidence = config.dynamicConfidence)), key)
          }
        }
      }
    }
    log.info("GantryStartUp done")
  }
}

