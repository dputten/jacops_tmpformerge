#! /bin/sh
### BEGIN INIT INFO
# Provides:          startAkkaKernel
# Required-Start:    $remote_fs $network
# Required-Stop:     $remote_fs $network
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# chkconfig: 2345 90 10
# Description:       script to start startAkkaKernel
# Short-Description: startAkkaKernel
### END INIT INFO

if [ -h $0 ] ;then
  SCRIPTLOC=$(readlink -f "$0")
else
  SCRIPTLOC="$0"
fi
AKKA_HOME="$(cd "$(cd "$(dirname "$SCRIPTLOC")"; pwd -P)"/..; pwd)"

serviceName="startAkkaKernel"
serviceLogFile="$AKKA_HOME/logs/$serviceName.srv.log"
serviceUser="ctes"
pidFile="$AKKA_HOME/logs/$serviceName.pid"
maxShutdownTime=15

export AB_LOGFILE=$AKKA_HOME/logs/libreg
export GX_VAR_PATH="/app/arh/var/gx"
export GX_SHARE_PATH="/app/arh/usr/share/gx"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/app/arh/usr/lib64:/app/arh/usr/lib64/gx"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/app/imagefunctions/:/app/intrada/"

#
# Get the Java command
#
if [ -z "$JAVACMD" ] ; then
  if [ -n "$JAVA_HOME"  ] ; then
    if [ -x "$JAVA_HOME/jre/sh/java" ] ; then
      # IBM's JDK on AIX uses strange locations for the executables
      JAVACMD="$JAVA_HOME/jre/sh/java"
    else
      JAVACMD="$JAVA_HOME/bin/java"
    fi
  else
    JAVACMD=`which java 2> /dev/null `
    if [ -z "$JAVACMD" ] ; then
        JAVACMD=java
    fi
  fi
fi

if [ ! -x "$JAVACMD" ] ; then
  echo "Error: JAVA_HOME is not defined correctly."
  echo "  We cannot execute $JAVACMD"
  exit 1
fi

AKKA_CLASSPATH="$AKKA_HOME/lib/*:$AKKA_HOME/config:$AKKA_HOME/deploy/*"
JAVA_OPTS="-Xms1536M -Xmx1536M -Xss1M -XX:+UseParallelGC -XX:+HeapDumpOnOutOfMemoryError -DappSpecificProps"

#
# Execute command
#
javaCommandLine="$JAVACMD $JAVA_OPTS -cp "$AKKA_CLASSPATH" -Dakka.home="$AKKA_HOME" akka.kernel.Main csc.startup.Boot "

# Returns 0 if the process with PID $1 is running.
checkProcessIsRunning() {
   local pid="$1"
   if [ -z "$pid" ]; then return 1; fi
   if [ "$pid" = " " ]; then return 1; fi

   kill "-0" "$pid" 2> "/dev/null"
   if [ $? -ne 0 ]; then
      return 1;
   fi
   return 0; }

# Returns 0 when the service is running and sets the variable $servicePid to the PID.
getServicePid() {
   if [ ! -f $pidFile ]; then echo "file doesn not exist"; return 1; fi
   servicePid=$(cat $pidFile)
   return 0; }

startServiceProcess() {
   if [ -f $pidFile ]; then 
	getServicePid
	checkProcessIsRunning $servicePid
	if [ $? = 0 ]; then
		echo "\n$serviceName already started."
		return 1	
   	fi
   fi

   cd $AKKA_HOME || return 1
   rm -f $pidFile
   export AKKA_HOME
   local cmd="setsid $javaCommandLine >>$serviceLogFile 2>&1 & echo \$! >$pidFile"

   if [ $(/usr/bin/whoami) = "$serviceUser"  ] ;then
     $javaCommandLine >>$serviceLogFile 2>&1 &
     echo $! >$pidFile
   else
     su $serviceUser -c "$cmd" || return 1
   fi

   sleep 2
   getServicePid
   if checkProcessIsRunning $servicePid; then :; else
      echo "\n$serviceName start failed, see logfile."
      return 1
      fi
   return 0; }

stopServiceProcess() {
   getServicePid
   if [ $? -ne 0 ]; then
      return 0
   fi
   checkProcessIsRunning $servicePid
   if [ $? -ne 0 ]; then
      rm -f $pidFile
      return 0
   fi
   kill "$servicePid"
   if [ $? -ne "0" ]; then
      return 1
   fi
  
   i=$maxShutdownTime
   while [ ${i} -ge 0 ] 
   do
      checkProcessIsRunning $servicePid
      if [ $? -ne 0 ]; then
         rm -f $pidFile
         return 0
      fi
      sleep 1
      i=$(expr $i "-" 1)
   done
   echo "\n$serviceName did not terminate within $maxShutdownTime seconds, sending SIGKILL..."
   kill "-9" "$servicePid"
   if [ $? -ne "0" ]; then
      return 1
   fi
   local killWaitTime=15
   i=$killWaitTime
   while [ ${i} -ge 0 ] 
   do
      checkProcessIsRunning $servicePid
      if [ $? -ne 0 ]; then
         rm -f $pidFile
         return 0
         fi
      sleep 1
      i=$(expr $i "-" 1)
   done
   echo "Error: $serviceName could not be stopped within $maxShutdownTime+$killWaitTime seconds!"
   return 1; }

case "$1" in
  start)
	startServiceProcess
    ;;
  stop)
        stopServiceProcess
    ;;
  status)
        if [ -f $pidFile ]; then
          getServicePid
          checkProcessIsRunning $servicePid
          if [ $? = 0 ]; then
            echo "$serviceName (pid  $servicePid) is running..."
            exit 0
          else
            echo "$serviceName is not running..."
            exit 1
          fi
        else
          echo "$serviceName is not running..."
          exit 1
        fi
    ;;
  force-reload|restart)
    $0 stop
    $0 start
    ;;
  *)
    echo "Usage: /etc/init.d/$serviceName {start|stop|status|restart}"
    exit 1
    ;;
esac

exit 0

