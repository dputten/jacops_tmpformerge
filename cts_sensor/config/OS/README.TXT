AIDE
aide.conf   Contains the configuration of aide and have to be placed in the /etc directory
aide        Script for checking if there are any changes on the system. Should be placed into the /etc/cron.daily directory

NTP
ntp.conf    Contains the NTP (time synchronization) configuration and is placed in the /etc directory