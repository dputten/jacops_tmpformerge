package csc.vehicle.classifier

import akka.actor.{ ActorSystem, Props }
import akka.kernel.Bootable
import com.typesafe.config.{ Config, ConfigFactory }
import csc.akka.logging.DirectLogging
import csc.amqp.{ AmqpConfiguration, ServicesConfiguration }
import csc.akka.config.ConfigUtil._

/**
 * Created by carlos on 25/11/16.
 */
class ClassifierBoot extends Bootable with DirectLogging {

  val config = ClassifierConfig(ConfigFactory.load("classifier"))

  implicit val actorSystem = ActorSystem("VehicleClassifier", config.config)

  override def startup(): Unit = {
    log.info("Starting VehicleClassifier")
    log.info("VehicleClassifier Boot config: {}", config)
    log.info("AMQP config: {}", config.amqpConfiguration)
    log.info("Services config: {}", config.services)

    val handler = actorSystem.actorOf(Props(new ClassifyActor(new OpenCVClassifier(config.vehdet))))
    ClassifierMQListener.create(actorSystem, config, handler)
  }

  override def shutdown(): Unit = {
    log.info("Stopping VehicleClassifier")
    actorSystem.shutdown()
  }
}

case class VehdetConfig(configPath: String, logFolder: String)

case class ClassifierConfig(val config: Config) {

  private val own = config.getConfig("classifier")

  lazy val amqpConfiguration = new AmqpConfiguration(own.getConfig("amqp"))
  lazy val services = new ServicesConfiguration(own.getConfig("services"))
  lazy val heartbeatFreq = own.getFiniteDuration("heartbeatFrequency")

  lazy val vehdet: VehdetConfig = {
    val cfg = own.getConfig("vehdet")
    VehdetConfig(cfg.getString("configPath"), cfg.getString("logFolder"))
  }

}

