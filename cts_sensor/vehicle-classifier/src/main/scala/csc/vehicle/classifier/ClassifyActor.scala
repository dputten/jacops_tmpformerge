package csc.vehicle.classifier

import java.io.File

import akka.actor.{ Actor, ActorLogging, Props }
import base.Timing
import csc.amqp.adapter.{ Ping, Pong }
import csc.vehicle.message._
import imagefunctions.{ ClassifiedVehicle, VehicleClass }

/**
 * Created by carlos on 23.11.15.
 */
class ClassifyActor(classifier: Classifier) extends Actor with Timing with ActorLogging {

  import ClassifyActor._

  override def receive = {
    case Ping                        ⇒ sender ! Pong //keep the beat
    case req: ClassifyVehicleRequest ⇒ handleRequest(req)
  }

  def now = System.currentTimeMillis()
  def handleRequest(req: ClassifyVehicleRequest): Unit = {
    val client = sender

    performanceLogInterval(log, 6, 8, "ClassifyActor", req.msgId, {
      val file = new File(req.imagePath)

      file.exists() match {
        case false ⇒
          log.warning("File of message{} no longer exists. Replying with no category", req)
          client ! ClassifyVehicleResponse(req.msgId, None, now)
        case true ⇒
          val cat = classifier.classify(file, req.position)
          log.info("Category for {} is {}", req.msgId, cat map { c ⇒ asString(c) })
          val category = cat map (convertCategory(_))
          client ! ClassifyVehicleResponse(req.msgId, category, now)
      }
    })
  }
}

object ClassifyActor {

  def props(classifier: Classifier): Props = Props(new ClassifyActor(classifier))

  def convertCategory(classified: ClassifiedVehicle): ValueWithConfidence[VehicleCategory.Value] = {

    val cat: VehicleCategory.Value = classified.getClassification match {
      //TODO: separate class conversion from this 'Jacops-specific' logic
      case VehicleClass.TRUCK   ⇒ VehicleCategory.Truck
      case VehicleClass.UNKNOWN ⇒ VehicleCategory.Unknown
      case VehicleClass.VAN     ⇒ VehicleCategory.Van
      case _                    ⇒ VehicleCategory.Car //everything else is a car
    }
    ValueWithConfidence(cat, classified.getConfidenceLevel)
  }

  def asString(classifiedVehicle: ClassifiedVehicle): String = {
    s"ClassifiedVehicle[classification=${classifiedVehicle.getClassification}, confidence=${classifiedVehicle.getConfidenceLevel}]"
  }

}
