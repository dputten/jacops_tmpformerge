package csc.vehicle.classifier

import akka.actor.{ ActorRef, ActorSystem, Props }
import csc.amqp.AmqpConnection
import csc.amqp.adapter.MQProducer.RouteKeyFunction
import csc.amqp.adapter.{ HeartbeatAwareMQProducer, MQListener, MQProducer }
import csc.vehicle.message.{ ClassifierFormats, ClassifyVehicleRequest, ClassifyVehicleResponse }

import scala.concurrent.duration.FiniteDuration

class ClassifierMQListener(val handler: ActorRef,
                           val producer: ActorRef) extends MQListener with ClassifierFormats {

  override type Request = ClassifyVehicleRequest
  override type Response = ClassifyVehicleResponse

  object ClassifyRequestExtractor extends MessageExtractor[ClassifyVehicleRequest]

  override def ownReceive: Receive = {
    case delivery @ ClassifyRequestExtractor(request) ⇒ handleRequest(request, delivery)
    case response: ClassifyVehicleResponse            ⇒ handleResponse(response)
  }

}

class ClassifierMQProducer(val producer: ActorRef,
                           val producerEndpoint: String,
                           val handler: ActorRef,
                           val heartbeatFreq: FiniteDuration) extends HeartbeatAwareMQProducer with ClassifierFormats {

  override def componentName: String = "vehicle-classifier"

  override def routeKeyFunction: RouteKeyFunction = MQProducer.prefixedRoute("system")
}

object ClassifierMQListener {

  def create(system: ActorSystem, config: ClassifierConfig, handler: ActorRef): ActorRef = {
    val con = new AmqpConnection(config.amqpConfiguration)(system)
    val producer = system.actorOf(Props(
      new ClassifierMQProducer(con.connection.producer, config.services.producerEndpoint, handler, config.heartbeatFreq)))
    val listener = system.actorOf(Props(new ClassifierMQListener(handler, producer)))
    con.queue(config.services).wireConsumer(listener, false)
    listener
  }

}