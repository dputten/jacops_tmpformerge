package csc.vehicle.classifier

import java.io.File

import csc.akka.logging.DirectLogging
import csc.vehicle.message.VehicleCategory.VehicleCategory
import csc.vehicle.message._
import imagefunctions.{ ClassifiedVehicle, ImageFunctionsException, VehicleClass }
import imagefunctions.vehdet.impl.OpenCVFunctionsImpl

/**
 * Created by carlos on 17/02/16.
 */
class OpenCVClassifier(cfg: VehdetConfig) extends Classifier with DirectLogging {

  val configFile = new File(cfg.configPath)

  require(configFile.isFile, s"${configFile.getAbsolutePath} does not exist or is not a file")

  val vehicleDetector = OpenCVFunctionsImpl.getInstance(configFile, new File(cfg.logFolder))

  override def classify(imageFile: File, point: Point): Option[ClassifiedVehicle] = {

    val vehicle: ClassifiedVehicle = try {
      vehicleDetector.classify(imageFile, new imagefunctions.Point(point.x, point.y))
    } catch {
      case ex: ImageFunctionsException ⇒
        log.error(ex, "Error calling vehdet. Assuming unknown class")
        new ClassifiedVehicle() // unknown
    }

    Some(vehicle)
  }

  def classify(img: RegistrationImage, position: LicensePosition): ValueWithConfidence[VehicleCategory.Value] = {

    val file = new File(img.uri)
    val point = new imagefunctions.Point(position.TLx, position.TLy)

    val vehicle: ClassifiedVehicle = try {
      vehicleDetector.classify(file, point)
    } catch {
      case ex: ImageFunctionsException ⇒
        log.error(ex, "Error calling vehdet. Assuming unknown class")
        new ClassifiedVehicle() // unknown
    }
    ValueWithConfidence(categoryMap(vehicle.getClassification), vehicle.getConfidenceLevel)
  }

  def categoryMap(cat: VehicleClass): VehicleCategory = cat match {
    case VehicleClass.TRUCK   ⇒ VehicleCategory.Truck
    case VehicleClass.UNKNOWN ⇒ VehicleCategory.Unknown
    case VehicleClass.VAN     ⇒ VehicleCategory.Van
    case _                    ⇒ VehicleCategory.Car //everything else is a car
  }

}
