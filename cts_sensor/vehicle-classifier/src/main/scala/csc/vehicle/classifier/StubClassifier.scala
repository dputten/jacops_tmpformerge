package csc.vehicle.classifier

import java.io.File

import csc.vehicle.VehicleInfo
import csc.vehicle.message._
import imagefunctions.{ ClassifiedVehicle, VehicleClass }

/**
 * Created by carlos on 17/02/16.
 */
object StubClassifier extends Classifier {

  override def classify(image: File, position: Point): Option[ClassifiedVehicle] = VehicleInfo(image.getName) match {
    case Some(vi) ⇒ Some(new ClassifiedVehicle(categoryOf(vi.vehicle), 90))
    case None     ⇒ Some(new ClassifiedVehicle(VehicleClass.UNKNOWN, 0))
  }

  private def categoryOf(source: String): VehicleClass = source match {
    case "Truck" ⇒ VehicleClass.TRUCK
    case _       ⇒ VehicleClass.CAR
  }

}
