package csc.vehicle.classifier

import java.io.File

import csc.vehicle.message._
import imagefunctions.ClassifiedVehicle

/**
 * Created by carlos on 17/02/16.
 */
trait Classifier {

  def classify(image: File, position: Point): Option[ClassifiedVehicle]

}
