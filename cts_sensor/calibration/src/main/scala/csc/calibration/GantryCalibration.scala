/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.calibration

import java.util.Date

case class CameraImage(name: String, imageId: String, checksum: String, image: Seq[Int])

/**
 * @param systemId
 * @param gantryId
 * @param time
 * @param laneIds
 * @param checks the checks/steps that we want to tun. If not specified, it is considered a legacy calibration
 */
case class GantryCalibrationRequest(systemId: String,
                                    gantryId: String,
                                    time: Long,
                                    laneIds: Seq[String],
                                    checks: Option[Seq[String]] = None) {

  /**
   * @return true if this request is for a legacy calibration
   */
  def isLegacyCalibration = checks.isEmpty

}

trait CameraSelftestResponse {
  def systemId: String
  def gantryId: String
  def time: Long
  def sendTime: Long
}

case class GantryCalibrationResponse(systemId: String,
                                     gantryId: String,
                                     time: Long,
                                     sendTime: Long,
                                     images: Seq[CameraImage],
                                     results: Seq[StepResult]) extends CameraSelftestResponse

case class GroupedGantryCalibrationResponse(systemId: String,
                                            gantryId: String,
                                            time: Long,
                                            sendTime: Long,
                                            imageMap: Map[String, Seq[CameraImage]],
                                            resultsMap: Map[String, Seq[StepResult]]) extends CameraSelftestResponse

case class StepResult(stepName: String, success: Boolean, detail: Option[String])

//case class StartCalibration(time: Date, userId: String, reportingOfficerCode: String, reason: String, systemId: String, corridorId: String) {
//  def isMidNight: Boolean = {
//    reason == "MIDNIGHT"
//  }
//}

