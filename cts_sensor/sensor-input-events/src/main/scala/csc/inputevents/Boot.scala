package csc.inputevents

import akka.actor.{ Props, ActorRef, ActorSystem }
import akka.kernel.Bootable
import com.github.sstone.amqp.Amqp.ChannelParameters
import com.github.sstone.amqp.Amqp
import com.typesafe.config.ConfigFactory
import csc.akka.logging.DirectLogging
import csc.amqp.MQConnection
import csc.inputevents.actor._
import csc.inputevents.sensor.{ SensorFactory, Sensor }
import csc.systemevents.{ AmqpSystemEventStore, SystemEventAmqpActor, SystemEventListener }

/**
 * Created by carlos on 17/05/16.
 */
class Boot extends Bootable with DirectLogging {

  val config: Configuration = Configuration.create(Some(ConfigFactory.load))
  val system = ActorSystem("InputEvents")
  val core = new AppCore(system, config)

  log.info("configuration is {}", config)

  override def startup(): Unit = {
    core.startup()
  }

  override def shutdown(): Unit = {
    log.info("Shutting down app")
    system.shutdown()
    log.info("Awaiting for system termination")
    system.awaitTermination()
    log.info("System successfully terminated")
  }

}

class AppCore(sys: ActorSystem,
              config: Configuration,
              eventQueueOverride: ⇒ Option[ActorRef] = None) extends DirectLogging {

  implicit val system = sys

  var amqpConnection: MQConnection = null

  def startup(): Unit = {
    amqpConnection = createAmqpConnection

    createSystemEventDispatcher

    //device receiver handles input messages from sensors (via AMQP), forwarding to the proper sensor actor
    val deviceReceiver = createDeviceInputReceiver //(sensorActorMap)

    val readingHandler = system.actorOf(ReadingHandlerActor.props(config))

    def addSensor(sensor: Sensor): Unit = {
      val actor = system.actorOf(SensorActor.props(sensor, config, readingHandler))
      deviceReceiver ! AddSensor(sensor.config.id, actor)
    }

    var stubManager: Option[ActorRef] = None

    config.stubConfig match {
      case None ⇒ config.sensors foreach { c ⇒
        val sensor = SensorFactory.createSensor(c, config) //no stub config: all sensors are 'real'
        addSensor(sensor)
      }
      case Some(stubConfig) ⇒
        val sm = system.actorOf(Props(new SensorStubManagerActor(stubConfig, addSensor, deviceReceiver)))
        stubManager = Some(sm)
        config.sensors foreach { c ⇒
          c.url match {
            case None ⇒ sm ! AddSensorStub(c) //sensor without url => stub
            case Some(_) ⇒
              val sensor = SensorFactory.createSensor(c, config) //sensor with url => real
              addSensor(sensor)
          }
        }
    }
  }

  def createSystemEventDispatcher: ActorRef = {
    val queue = eventQueueOverride getOrElse {
      val producer = amqpConnection.producer
      val actor = system.actorOf(Props(
        new SystemEventAmqpActor(producer, config.amqpServicesConfig.producerEndpoint, config.amqpServicesConfig.producerRouteKey, "SensorInputEvents")))
      log.debug("SystemEvent queueActor created")
      actor
    }

    val sysEventStore = new AmqpSystemEventStore(queue)

    system.actorOf(Props(new SystemEventListener(sysEventStore)))
  }

  def createAmqpConnection: MQConnection = {
    val connection = MQConnection.create(config.amqpConfig.actorName, config.amqpConfig.amqpUri,
      config.amqpConfig.reconnectDelay)
    log.info("Amqp connection created")
    connection
  }

  def createDeviceInputReceiver: ActorRef = {
    val listener = system.actorOf(Props(new AmqpDeviceReceiverActor))
    wireAmpqConsumer(listener)
    listener
  }

  def wireAmpqConsumer(listener: ActorRef): Unit = {
    amqpConnection.createSimpleConsumer(config.amqpDeviceInputConfig.consumerQueue, listener,
      Some(ChannelParameters(config.amqpDeviceInputConfig.serversCount)), true)
    log.info("Consumer created and connected")
  }

}
