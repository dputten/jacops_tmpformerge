package csc.inputevents.tools

import java.io.{ BufferedReader, File, FileReader, StringReader }

import csc.akka.remote.SocketServer
import csc.base.AppConfigReader
import csc.inputevents.Configuration
import csc.inputevents.actor.SensorStubManagerClient

/**
 * Created by carlos on 23/05/16.
 */
object SensorStubControl {

  def main(args: Array[String]) {
    val cfg = Configuration.create(Some(AppConfigReader.getAppConfig(None)))
    val stubUrl = cfg.stubConfig map { c ⇒ c.stubUrl }
    val ctl = getStubControl(stubUrl)

    ctl foreach { client ⇒
      val (toClose, reader) = getReader(args)
      try {
        client.process(reader)
      } finally {
        if (toClose) reader.close()
      }
    }
  }

  def getReader(args: Array[String]): (Boolean, BufferedReader) = args match {
    case array if args.isEmpty ⇒
      println("Reading from standard input. Issue your stub commands now:")
      (false, Console.in)
    case Array("-f", filename) ⇒
      val file = new File(filename)
      if (file.isFile && file.canRead) {
        println("Reading from file " + file)
        (true, new BufferedReader(new FileReader(file)))
      } else throw new IllegalArgumentException("Unable to read file " + file.getAbsolutePath)
    case Array("-s", cmd) ⇒
      (true, new BufferedReader(new StringReader(cmd)))
    case _ ⇒ throw new IllegalArgumentException("Unsupported args: " + args)
  }

  def getStubControl(uriOpt: Option[String]): Option[SensorStubControl] = uriOpt match {
    case None ⇒
      println("Sensor stub URI not present or not enabled")
      None
    case Some(uri) ⇒ SocketServer(uri) match {
      case Some(server) ⇒ Some(new SensorStubControl(server))
      case None ⇒
        println("Could not parse host/port from uri " + uri)
        None
    }
  }
}

class SensorStubControl(server: SocketServer) extends SensorStubManagerClient(server) {

  def process(reader: BufferedReader): Unit = {
    Iterator.continually(reader.readLine).takeWhile(_ != null).foreach { line ⇒ send(line) }
  }

}
