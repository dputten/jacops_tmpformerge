package csc.inputevents

import java.util.concurrent.TimeUnit

import com.typesafe.config._
import csc.akka.config.ConfigUtil._
import csc.akka.config.ConfigWrapper
import csc.akka.remote.SocketServer
import csc.amqp.{ ServicesConfiguration, AmqpConfiguration }
import csc.inputevents.sensor.{ ReadingId, Component }
import collection.JavaConverters._
import scala.concurrent.duration.{ Duration, FiniteDuration }

/**
 * Created by carlos on 17/05/16.
 */

/**
 * @param id the unique name for this sensor
 * @param component the type of component
 * @param url the url to access the sensor. If none specified and stubConfig enabled. then will be stub
 * @param debug
 */
case class SensorConfig(id: String, component: Component.Value, url: Option[String], debug: Boolean) {
  lazy val server: Option[SocketServer] = url flatMap { e ⇒ SocketServer(e) }
}

object LimitQualifier extends Enumeration {
  type LimitQualifier = Value
  val functioning, critical = Value
}

case class ReadingLimitId(readingId: ReadingId, qualifier: LimitQualifier.Value) {
  def description: String = qualifier match {
    case LimitQualifier.functioning ⇒ readingId.name
    case other                      ⇒ "%s-%s".format(qualifier, readingId.name)
  }
}

case class ReadingLimitConfig(id: ReadingLimitId, limit: ReadingLimit)

case class StubConfig(stubUrl: String, initialValues: Map[ReadingId, Any]) {
  def getInitialValues(component: Component.Value): Map[String, Any] = {
    val list = for (e ← initialValues if (e._1.component == component)) yield e._1.name -> e._2
    list toMap
  }
}

case class Configuration(systemId: String,
                         initialDeviceDelay: Option[FiniteDuration],
                         gantrySensorMap: Map[String, List[SensorConfig]],
                         readingLimitConfigs: List[ReadingLimitConfig],
                         amqpConfig: AmqpConfiguration,
                         amqpServicesConfig: ServicesConfiguration,
                         amqpDeviceInputConfig: ServicesConfiguration,
                         stubConfig: Option[StubConfig] = None) {

  lazy val sensors: List[SensorConfig] = gantrySensorMap.values.toList.flatten

  def getReadingLimits(component: Component.Value): Map[ReadingLimitId, ReadingLimit] = {
    val list = readingLimitConfigs filter { e ⇒ e.id.readingId.component == component }
    list map { e ⇒ e.id -> e.limit } toMap
  }

}

object Configuration {

  private val fallback = ConfigFactory.parseResources(this.getClass, "/reference.conf")

  private val rootConfig = "input-events"

  def create(cfg: Option[Config]): Configuration = {
    val config = cfg match {
      case None      ⇒ fallback
      case Some(cfg) ⇒ cfg.withFallback(fallback)
    }

    val systemId = config.getReplacedString(rootConfig + ".systemId")
    val initialDeviceDelay = config.getStringOpt(rootConfig + ".initialDeviceDelay") map { s ⇒
      val d = Duration(s)
      FiniteDuration(d.toMillis, TimeUnit.MILLISECONDS)
    }
    val sensors = createGantrySensorMap(config)
    val readingEvalConfigs = createReadingLimitConfigs(config)
    val amqpConfig = new AmqpConfiguration(config.getConfig(rootConfig + ".amqp-connection"))
    val amqpServicesConfig = new ServicesConfiguration(config.getConfig(rootConfig + ".systemEventServices"))
    val amqpDeviceInputConfig = new ServicesConfiguration(config.getConfig(rootConfig + ".deviceInputServices"))
    val stubConfig = createStubConfig(config)

    Configuration(systemId, initialDeviceDelay, sensors, readingEvalConfigs, amqpConfig, amqpServicesConfig,
      amqpDeviceInputConfig, stubConfig)
  }

  private def createGantrySensorMap(config: Config): Map[String, List[SensorConfig]] = {
    val obj = config.getObject(rootConfig + ".sensors")
    val gantries = obj.toConfig

    obj.keySet().asScala map { gantry ⇒
      val obj = gantries.getObject(gantry)
      val gantryCfg = obj.toConfig
      val sensors: List[SensorConfig] = obj.keySet().asScala.toList map { id ⇒
        toSensorConfig(id, gantryCfg.getObject(id).toConfig)
      }
      gantry -> sensors
    } toMap
  }

  private def createReadingLimitConfigs(config: Config): List[ReadingLimitConfig] = {
    config.getObjectList(rootConfig + ".readingLimits").asScala.toList map { o ⇒ createReadingLimitConfig(o.toConfig) }
  }

  private def createReadingLimitConfig(config: Config): ReadingLimitConfig = {
    val component = Component.withName(config.getString("component"))
    val reading = config.getString("reading")
    val tp = config.getString("type")
    val values = config.getValue("values").asInstanceOf[ConfigList]
    val limit = createReadingLimit(tp, values)
    val qualifier: String = config.getStringOpt("qualifier").getOrElse(LimitQualifier.functioning.toString)
    val id = ReadingLimitId(ReadingId(component, reading), LimitQualifier.withName(qualifier))
    ReadingLimitConfig(id, limit)
  }

  private def createReadingLimit(tp: String, values: ConfigList): ReadingLimit = tp match {
    case "range" ⇒
      val args = values.unwrapped().asScala map { e ⇒ e.toString.toDouble }
      NumericRangeLimit(args(0), args(1))
    case "match" ⇒
      val args = values.unwrapped().asScala map (_.toString)
      MatchLimit(args: _*)
    case other ⇒ throw new IllegalArgumentException("Unrecognized limit type: " + other)
  }

  private def toSensorConfig(id: String, config: Config): SensorConfig = {
    val cfg = ConfigWrapper(config)
    val component = Component.withName(cfg.string("component"))
    val url = cfg.stringOpt("url")
    val debug = cfg.stringOpt("debug").getOrElse("false")
    SensorConfig(id, component, url, debug.toBoolean)
  }

  private def createStubConfig(config: Config): Option[StubConfig] = {
    config.getReplacedBoolean(rootConfig + ".sensorStub.enabled") match {
      case false ⇒ None
      case true ⇒
        val url = config.getReplacedString(rootConfig + ".sensorStub.uri", null)
        val initialValues: Map[ReadingId, Any] =
          createStubInitialValues(config.getObject(rootConfig + ".sensorStub.initialValues"))

        Some(StubConfig(url, initialValues))
    }
  }

  private def createStubInitialValues(obj: ConfigObject): Map[ReadingId, Any] = {
    val components = obj.toConfig
    var map: Map[ReadingId, Any] = Map.empty

    obj.keySet().asScala foreach { c: String ⇒
      val component = Component.withName(c)
      components.getObject(c).unwrapped().asScala foreach { e ⇒
        map = map.updated(ReadingId(component, e._1), e._2)
      }
    }
    map
  }

}
