package csc.inputevents.actor

import java.util.concurrent.TimeUnit

import akka.actor.{ ActorRef, ActorLogging, Actor }
import com.github.sstone.amqp.Amqp.Delivery
import csc.amqp.JsonSerializers
import net.liftweb.json.{ DefaultFormats, Formats }

import scala.concurrent.duration.FiniteDuration

/**
 * Actor that handles input messages (via AMQP), forwarding them to the correct sensor actor
 *
 * Created by carlos on 26/05/16.
 */
class AmqpDeviceReceiverActor(oldMessageDelay: FiniteDuration = FiniteDuration(30, TimeUnit.SECONDS))
  extends Actor with ActorLogging {

  import AmqpDeviceReceiverActor._

  implicit val executor = context.system.dispatcher

  var deviceActorMap: Map[String, ActorRef] = Map.empty

  var mostRecentMessageTimeMap: Map[String, Long] = Map.empty

  override def receive: Receive = {
    case delivery: Delivery ⇒ parseMessages(delivery) match {
      case Left(error) ⇒ log.warning("Error parsing messages from {}: {}", delivery, error)
      case Right(list) ⇒ handle(list)
    }
    case msg: DeviceInputMessage  ⇒ handle(List(msg))
    case msg: DeviceInputMessages ⇒ handle(msg.list)
    case msg: DelayedMessage      ⇒ handleDelayed(msg)
    case AddSensor(name, actor)   ⇒ addSensor(name, actor)
    case other                    ⇒ log.warning("Don't know how to handle {}", other)
  }

  def addSensor(name: String, actor: ActorRef): Unit = {
    deviceActorMap = deviceActorMap.updated(name, actor)
  }

  def handleDelayed(msg: DelayedMessage): Unit = {
    val lastTime: Long = mostRecentMessageTimeMap.get(msg.sensor).getOrElse(msg.time)
    if (msg.time < lastTime) {
      //message is no longer the most recent: discarder
      log.info("Message no longer the most recent. Discarding {}", msg.msg)
    } else {
      deviceActorMap.get(msg.sensor) map { _ ! msg.msg } // still the most recent
    }
  }

  def handle(list: List[DeviceInputMessage]): Unit = {
    log.debug("Received {}", list)
    list.groupBy(_.sensor) foreach { e ⇒
      val sensor = e._1
      val list = e._2
      deviceActorMap.get(sensor) match {
        case None ⇒ log.warning("Unknown sensor {}. Ignoring messages {}", sensor, list)
        case Some(actor) ⇒
          val messageTime = list(0).time
          val lastTime: Long = mostRecentMessageTimeMap.get(sensor).getOrElse(messageTime)
          mostRecentMessageTimeMap = mostRecentMessageTimeMap.updated(sensor, math.max(messageTime, lastTime))
          val msg = DeviceInputMessages(list)

          //checking if message is 'old'
          if (System.currentTimeMillis() - messageTime < AmqpDeviceReceiverActor.oldMessageThresholdMillis) {
            actor ! msg //recent enough: send immediately
          } else {
            //somewhat old: delay a bit, waiting for more recent messages
            context.system.scheduler.scheduleOnce(oldMessageDelay, self, DelayedMessage(sensor, messageTime, msg))
          }
      }
    }
  }

}

object AmqpDeviceReceiverActor extends JsonSerializers {
  val DefaultEncoding = "UTF-8"

  val oldMessageThresholdMillis = 60000

  implicit val formats: Formats = DefaultFormats

  def parseMessages(delivery: Delivery): Either[String, List[DeviceInputMessage]] = {
    val deliveryEncoding = delivery.properties.getContentEncoding
    val encoding = if (deliveryEncoding != null) deliveryEncoding else DefaultEncoding
    fromJson[List[DeviceInputMessage]](new String(delivery.body, encoding)) match {
      case Left(error)    ⇒ Left(error)
      case Right(message) ⇒ Right(message)
    }
  }

}

case class AddSensor(name: String, actor: ActorRef)

case class DelayedMessage(sensor: String, time: Long, msg: DeviceInputMessages)