package csc.inputevents.actor

import akka.actor.{ ActorRef, ActorLogging, Actor }
import akka.camel.{ CamelMessage, Consumer }
import akka.event.LoggingAdapter
import csc.akka.logging.DirectLogging
import csc.akka.remote.{ SocketServer, SocketMessageSender }
import csc.inputevents.sensor._
import csc.inputevents.{ SensorConfig, StubConfig }
import imagefunctions.sensorctl.SensorStatusEnum

/**
 * Actor layer for SensorStubManager
 *
 * Created by carlos on 17/05/16.
 */
class SensorStubManagerActor(val config: StubConfig,
                             val sensorAction: Sensor ⇒ Unit,
                             inputReceiver: ActorRef,
                             commandListener: Option[String ⇒ Unit] = None)
  extends Actor with SensorStubManager with Consumer with ActorLogging {

  override def endpointUri: String = config.stubUrl

  override def receive: Receive = {
    case AddSensorStub(cfg)                        ⇒ addStub(cfg)
    case SetReadingValue(sensorId, reading, value) ⇒ setValue(sensorId, reading, value)
    case ResetReadingValues(sensorId)              ⇒ resetValues(sensorId)
    case msg: CamelMessage                         ⇒ handleCommand(msg.bodyAs[String])
    case other                                     ⇒ log.warning("Don't know how to handle {}", other)
  }

  override def sendInputMessages(msg: DeviceInputMessages): Unit = inputReceiver ! msg

  override def handleCommand(cmd: String): Unit = {
    super.handleCommand(cmd)
    commandListener map { l ⇒ l(cmd) }
  }
}

class SensorStubManagerClient(val server: SocketServer) extends SocketMessageSender with DirectLogging {

  def send(cmd: String): Unit = sendNotification(cmd)

}

object SensorStubManager {
  val setValuePattern = """SET (\S+) (\S+) (\S+)""".r
  val resetOnePattern = """RESET (\S+)""".r
  val resetAllPattern = "RESET".r
}

/**
 * Trait to manage and maintain sensor stubs.
 */
trait SensorStubManager {

  import SensorStubManager._

  def log: LoggingAdapter
  def sensorAction: Sensor ⇒ Unit
  def config: StubConfig

  private var stubs: Map[String, StubSensor] = Map.empty

  def sendInputMessages(msg: DeviceInputMessages)

  def addStub(config: SensorConfig): Unit = {
    val stub = new StubSensor(config, this)
    stubs = stubs.updated(config.id, stub)
    sensorAction(stub)
  }

  def resetValues(sensorId: Option[String]): Unit = {
    sensorId match {
      case None ⇒ stubs.values foreach { s ⇒
        sendInputMessages(createInputMessages(s.config.id, s.resetValues()))
      }
      case Some(sensorId) ⇒ stubs.get(sensorId) match {
        case None         ⇒ log.warning("Stub for {} not found", sensorId)
        case Some(sensor) ⇒ sendInputMessages(createInputMessages(sensorId, sensor.resetValues()))
      }
    }
  }

  def setValue(sensorId: String, reading: String, value: Any): Unit = {
    stubs.get(sensorId) match {
      case None         ⇒ log.warning("Stub for {} not found", sensorId)
      case Some(sensor) ⇒ sendInputMessages(createInputMessages(sensorId, List(sensor.setValue(reading, value))))
    }
  }

  def handleCommand(cmd: String): Unit = {
    log.info("Handling command {}", cmd)
    cmd match {
      case setValuePattern(sensorId, reading, value) ⇒ setValue(sensorId, reading, value)
      case resetOnePattern(sensorId)                 ⇒ resetValues(Some(sensorId))
      case resetAllPattern()                         ⇒ resetValues(None)
      case other                                     ⇒ log.warning("Could not process command " + other)
    }
  }

  def createInputMessages(sensorId: String, readings: List[Reading[_]]): DeviceInputMessages = {
    val datetime = DeviceInputMessage.format(System.currentTimeMillis())
    val list: List[DeviceInputMessage] = readings map { r ⇒ DeviceInputMessage(sensorId, datetime, r.name, r.value.toString) }
    DeviceInputMessages(list)
  }

}

class StubSensor(val config: SensorConfig, manager: SensorStubManager) extends Sensor {
  var readings: Map[String, Reading[_]] = initialValues
  lazy val _status = SensorStatus(SensorStatusEnum.SENS_RUNNING, None)

  override def init: Either[String, SensorStatus] = {
    //sends a message with the values, as a normal sensor would
    val msg = manager.createInputMessages(config.id, readings.values.toList)
    manager.sendInputMessages(msg)
    Right(_status)
  }

  override def status: Option[SensorStatus] = Some(_status)

  override def stop: Unit = () //nothing to do

  def values: Map[String, Reading[_]] = readings

  private lazy val initialValues: Map[String, Reading[_]] = {
    //initializing reading values based on configuration
    val types = descriptor.types
    val values = manager.config.getInitialValues(config.component)

    values map { v ⇒
      val name = v._1
      val tp = types(name)
      name -> Reading.create(name, tp, v._2)
    } toMap
  }

  def setValue(name: String, value: Any): Reading[_] = {
    manager.log.debug("{}: setting {} to {}", config.id, name, value)
    val tp = descriptor.types(name)
    val reading = Reading.create(name, tp, value)
    readings = readings.updated(name, reading)
    reading
  }

  def resetValues(): List[Reading[_]] = {
    readings = initialValues
    manager.log.debug("{}: values reset to {}", config.id, readings.values.toSet)
    readings.values.toList
  }

}
object SensorStubManagerClient {

  def setValueCommand(sensorId: String, reading: String, value: String): String = "SET %s %s %s".format(sensorId, reading, value)

  def resetCommand(sensorId: Option[String]): String = sensorId match {
    case None     ⇒ "RESET"
    case Some(id) ⇒ "RESET " + id
  }
}

case class AddSensorStub(config: SensorConfig)

case class SetReadingValue(sensorId: String, reading: String, value: Any)

case class ResetReadingValues(sensorId: Option[String])
