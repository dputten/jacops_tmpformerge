package csc.inputevents.actor

import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit

import akka.actor.{ Props, ActorRef, ActorLogging, Actor }
import csc.inputevents.{ ReadingLimitId, Configuration, ReadingLimit }
import csc.inputevents.sensor.{ Reading, Sensor }

import scala.concurrent.duration._

/**
 * Created by carlos on 18/05/16.
 */
object SensorActor {

  def props(sensor: Sensor, config: Configuration, listener: ActorRef): Props = {
    val readingLimits = config.getReadingLimits(sensor.config.component)
    Props(new SensorActor(sensor, config.initialDeviceDelay, readingLimits, listener))
  }

}

class SensorActor(sensor: Sensor, initDelay: Option[FiniteDuration], limitMap: Map[ReadingLimitId, ReadingLimit], listener: ActorRef)
  extends Actor with ActorLogging {

  log.info("Limit map for sensor {} is {}", sensor.config, limitMap)

  implicit val executor = context.system.dispatcher

  lazy val readingLimitIds: Set[ReadingLimitId] = limitMap.keySet

  var preInitMessages: List[DeviceInputMessages] = Nil

  override def preStart(): Unit = {
    initDelay match {
      case None        ⇒ self ! InitializeDevice
      case Some(delay) ⇒ context.system.scheduler.scheduleOnce(delay, self, InitializeDevice)
    }
  }

  override def postStop(): Unit = {
    log.info("Stopping device {}", sensor.config.id)
    sensor.stop
  }

  override def receive: Receive = {
    case InitializeDevice ⇒ initializeDevice()
    case msg: DeviceInputMessages ⇒
      log.warning("Device not yet initialized. Adding {} to pending list", msg)
      preInitMessages = preInitMessages :+ msg
  }

  def receiveWithState(state: Option[SensorState]): Receive = {
    case msg: DeviceInputMessages ⇒ handleInputMessage(msg, state)
    case other                    ⇒ log.warning("Don't know how to handle {}", other)
  }

  def initializeDevice(): Unit = sensor.init match {
    case Right(status) ⇒ {
      log.info("Sensor {} initialized with status {}", sensor.config.id, status)

      //resends all the pending messages
      preInitMessages foreach { self ! _ }
      preInitMessages = Nil

      context.become(receiveWithState(None))
    }
    case Left(error) ⇒ {
      val duration: FiniteDuration = initDelay.getOrElse(FiniteDuration(30, TimeUnit.SECONDS))
      log.error(error, "Sensor {} failed initialization. Retrying again in {}", sensor.config.id, duration)
      context.system.scheduler.scheduleOnce(duration, self, InitializeDevice)
    }
  }

  def toReading(msg: DeviceInputMessage): Reading[_] = {
    val name = sensor.descriptor.aliases.get(msg.name).getOrElse(msg.name)
    val tp = sensor.descriptor.types(name)
    Reading.create(name, tp, msg.value)
  }

  def previousValueFunc(stateOpt: Option[SensorState]): String ⇒ Option[Reading[_]] = stateOpt match {
    case None        ⇒ { _ ⇒ None }
    case Some(state) ⇒ state.readings.get
  }

  def handleInputMessage(msg: DeviceInputMessages, stateOpt: Option[SensorState]) {

    log.debug("Handling {}", msg)

    val (inputMessages, ignored) = msg.list.partition(_.sensor == sensor.config.id)
    if (ignored.size > 0) {
      log.warning("Messages for the wrong sensor ignored {}", ignored)
    }

    val readings: Map[String, Reading[_]] = inputMessages map { m ⇒
      val reading = toReading(m)
      reading.name -> toReading(m)
    } toMap

    val prevValue = previousValueFunc(stateOpt)

    //logging all received readings
    readings.values.toList foreach { r ⇒
      prevValue(r.name) match {
        case None ⇒ log.debug("Reading for {} / {} is {}. No changes since last", sensor.config.id, r.name, r.value)
        case Some(v) ⇒ r.value == v.value match {
          case false ⇒ log.info("Reading for {} / {} is now {} (was {})", sensor.config.id, r.name, r.value, v.value)
          case true  ⇒ log.debug("Reading for {} / {} is {}. No changes since last", sensor.config.id, r.name, r.value)
        }
      }
    }

    val newState = process(readings, stateOpt)
    context.become(receiveWithState(Some(newState)))
  }

  def process(readings: Map[String, Reading[_]], stateOpt: Option[SensorState]): SensorState = {

    log.debug("Processing readings. readings {} state {}", readings, stateOpt)

    val time = System.currentTimeMillis()

    def createMessage(limitId: ReadingLimitId, alarm: Boolean, initial: Boolean): SensorReadingMessage = {
      val name = limitId.readingId.name
      val desc = sensor.descriptor.descriptionOf(readings(name))
      SensorReadingMessage(sensor.config.id, limitId, time, initial, alarm, desc)
    }

    var alertStates = evalState(readings.values.toSet)

    val previousAlertStates: Map[ReadingLimitId, Boolean] = if (stateOpt.isDefined) stateOpt.get.alertState else Map.empty
    val (updates, additions) = alertStates partition { e ⇒ previousAlertStates.contains(e._1) }

    val list1 = additions map { e ⇒ createMessage(e._1, e._2, true) } toList
    val list2 = diffAlertChanges(updates, previousAlertStates) map { e ⇒ createMessage(e._1, e._2, false) } toList

    val toSend: List[SensorReadingMessage] = list1 ++ list2

    toSend foreach { msg ⇒
      log.debug("sending {} to listener", msg)
      listener ! msg
    }

    val prevReadings: Map[String, Reading[_]] = if (stateOpt.isDefined) stateOpt.get.readings else Map.empty
    val newReadings = prevReadings ++ readings //merges readings

    SensorState(newReadings, previousAlertStates ++ alertStates)
  }

  def diffAlertChanges(current: Map[ReadingLimitId, Boolean], previous: Map[ReadingLimitId, Boolean]): Map[ReadingLimitId, Boolean] = {
    val list: List[Option[(ReadingLimitId, Boolean)]] = for (
      e ← current.toList;
      id = e._1;
      curr = current(id);
      prev = previous.get(id)
    ) yield if (Some(curr) == prev) None else Some((id, curr))

    list.flatten.toMap
  }

  def evalState(readings: Set[Reading[_]]): Map[ReadingLimitId, Boolean] = {
    val list = for (
      r ← readings;
      key ← limitMap.keys.filter { e ⇒ e.readingId.name == r.name };
      limit ← limitMap.get(key)
    ) yield key -> limit(r)

    list toMap
  }

}

object InitializeDevice

object CheckStatus

case class SensorState(readings: Map[String, Reading[_]], alertState: Map[ReadingLimitId, Boolean])

case class DeviceInputMessages(list: List[DeviceInputMessage])

object DeviceInputMessage {
  val datetimePattern = "yyyyMMdd.HHmmss.SSS"
  def format(time: Long) = new SimpleDateFormat(datetimePattern).format(time)
}

case class DeviceInputMessage(sensor: String, datetime: String, name: String, value: String) {
  lazy val time = new SimpleDateFormat(DeviceInputMessage.datetimePattern).parse(datetime).getTime
}

case class SensorReadingMessage(sensorId: String,
                                limitId: ReadingLimitId,
                                time: Long,
                                initial: Boolean,
                                alarm: Boolean,
                                description: String)
