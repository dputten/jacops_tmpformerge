package csc.inputevents.actor

import akka.actor.{ Props, ActorLogging, Actor }
import csc.inputevents.{ SensorConfig, Configuration }
import csc.systemevents.SystemEvent

/**
 * Created by carlos on 19/05/16.
 */
class ReadingHandlerActor(systemId: String,
                          gantrySensorMap: Map[String, List[SensorConfig]]) extends Actor with ActorLogging {

  val sensorGantryMap: Map[String, String] =
    gantrySensorMap.toList flatMap { e ⇒ e._2 map { c ⇒ c.id -> e._1 } } toMap

  override def receive: Receive = {
    case msg: SensorReadingMessage ⇒ handle(msg)
  }

  def handle(msg: SensorReadingMessage): Unit = {
    log.debug("Handling {}", msg)
    sensorGantryMap.get(msg.sensorId) match {
      case None ⇒ log.warning("Unknown sensorId {}. Ignoring message {}", msg.sensorId, msg)
      case Some(gantry) ⇒
        val event = createSystemEvent(gantry, msg)
        log.debug("Publishing event {}", event)
        context.system.eventStream.publish(event)
    }
  }

  def eventType(msg: SensorReadingMessage): String = {
    val reason = msg.alarm match {
      case true  ⇒ "ALARM" //alarm has precedence over initial
      case false ⇒ if (msg.initial) "INITIAL" else "SOLVED"
    }
    "%s-%s".format(msg.limitId.description, reason)
  }

  def createSystemEvent(gantry: String, msg: SensorReadingMessage): SystemEvent =
    SystemEvent(componentId = msg.sensorId, timestamp = msg.time, systemId = systemId,
      eventType = eventType(msg), userId = "system", reason = msg.description, gantryId = Some(gantry))

}

object ReadingHandlerActor {

  def props(config: Configuration) = Props(new ReadingHandlerActor(config.systemId, config.gantrySensorMap))

}