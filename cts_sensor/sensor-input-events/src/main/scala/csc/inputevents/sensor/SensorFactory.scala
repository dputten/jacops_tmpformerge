package csc.inputevents.sensor

import csc.inputevents.{ Configuration, SensorConfig }

/**
 * Created by carlos on 17/05/16.
 */
object Component extends Enumeration {
  type Comp = Value
  val cambox, station = Value

  val descriptors: Map[Comp, SensorDescriptor] =
    Map(cambox -> GenericSensorFactory, station -> GenericSensorFactory)

  def descriptionOf(comp: Comp, reading: Reading[_]): String = descriptors(comp).descriptionOf(reading)
}

trait SensorFactory {

  def create(config: SensorConfig, globalConfig: Configuration): Sensor

}

object SensorFactory {

  import csc.inputevents.sensor.Component._

  val factories: Map[Comp, SensorFactory] = Map(cambox -> GenericSensorFactory, station -> GenericSensorFactory)

  def createSensor(cfg: SensorConfig, globalConfig: Configuration): Sensor = factories.get(cfg.component) match {
    case None          ⇒ throw new IllegalArgumentException("No factory found for sensor " + cfg)
    case Some(factory) ⇒ factory.create(cfg, globalConfig)
  }

}

