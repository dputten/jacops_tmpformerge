package csc.inputevents.sensor

import csc.inputevents.SensorConfig
import imagefunctions.sensorctl.SensorStatusEnum

/**
 * Created by carlos on 17/05/16.
 */
trait Sensor {

  final def descriptor: SensorDescriptor = Component.descriptors(config.component)

  def config: SensorConfig

  def status: Option[SensorStatus]

  def init: Either[String, SensorStatus]

  def stop

}

case class SensorStatus(status: SensorStatusEnum, detail: Option[String])

trait SensorDescriptor {

  def descriptionOf(value: Reading[_]): String

  def aliases: Map[String, String]

  def types: Map[String, Class[_]]

}

trait Reading[T] {

  def name: String
  def value: T

  override def hashCode(): Int = name.hashCode

  override def equals(other: scala.Any): Boolean =
    other != null && getClass == other.getClass && name == other.asInstanceOf[Reading[_]].name
}

object Reading {

  def create(name: String, clazz: Class[_], value: Any): Reading[_] = clazz match {
    case c if c == classOf[Double]   ⇒ DoubleReading(name, value.toString.toDouble)
    case c if c == classOf[Boolean]  ⇒ BooleanReading(name, value.toString.toBoolean)
    case c if c == OpenDoor.getClass ⇒ BooleanReading(name, OpenDoor.toBoolean(value.toString))
  }
}

case class DoubleReading(name: String, value: Double) extends Reading[Double]

case class BooleanReading(name: String, value: Boolean) extends Reading[Boolean]

case class ReadingId(component: Component.Value, name: String)

class UnreachableSensorException(msg: String) extends Exception(msg)