package csc.inputevents.sensor

import csc.akka.logging.DirectLogging
import csc.akka.remote.SocketServer
import csc.amqp.ServicesConfiguration
import csc.inputevents.{ Configuration, SensorConfig }
import imagefunctions.sensorctl.impl.SensorControllerImpl
import imagefunctions.sensorctl.{ SensorStatus ⇒ SensorStat, SensorConfiguration, SensorStatusEnum }
import org.apache.commons.lang.builder.ToStringBuilder

class GenericSensor(val config: SensorConfig, val amqpServer: SocketServer, amqpConfig: ServicesConfiguration)
  extends Sensor with DirectLogging {

  val server: SocketServer = config.server match {
    case None      ⇒ throw new IllegalArgumentException("Invalid or no server configuration (host/port) provided")
    case Some(srv) ⇒ srv
  }

  private val agent = SensorControllerImpl.getInstance(server.host, server.port, config.debug)

  log.info("Created agent {} for sensor {}", agent, config.id)

  private val configuration = {
    //val mqHost: String = if ("localhost".equalsIgnoreCase(server.host)) localhost else amqpServer.host
    val mqHost: String = amqpServer.host
    new SensorConfiguration(config.id, mqHost, amqpConfig.producerEndpoint, amqpConfig.producerRouteKey)
    //new SensorConfiguration(config.id, amqpServer.host, amqpConfig.producerEndpoint, amqpConfig.producerRouteKey)
  }

  private def convert(status: SensorStat) = SensorStatus(status.getStatus, Some(status.getDescription))

  override def init: Either[String, SensorStatus] = {
    try {
      log.info("Sending configuration to sensor {}: {}", config.id, ToStringBuilder.reflectionToString(configuration))
      val result = agent.configure(configuration)
      log.debug("Configuration result is {}", result)
      result match {
        case status if (status.getStatus == SensorStatusEnum.SENS_ERROR) ⇒ Left(status.getDescription)
        case _ ⇒ agent.start match {
          case status if (status.getStatus == SensorStatusEnum.SENS_ERROR) ⇒ Left(status.getDescription)
          case status ⇒ Right(convert(status))
        }
      }
    } catch {
      case ex: Exception ⇒
        log.error(ex, "Error configuring sensor {} with {}", config.id, ToStringBuilder.reflectionToString(configuration))
        Left(ex.getMessage)
    }
  }

  override def stop: Unit = {
    try {
      agent.stop()
    } catch {
      case ex: Exception ⇒ log.warning("Error stopping sensor: {}", ex.toString)
    }
  }

  override def status: Option[SensorStatus] = {
    try {
      val status = agent.getStatus
      Some(convert(status))
    } catch {
      case ex: Exception ⇒
        log.error(ex, "Error getting sensor status for {}", config.id)
        None
    }
  }

}

object GenericSensorFactory extends SensorFactory with SensorDescriptor {

  override def create(config: SensorConfig, globalConfig: Configuration): Sensor = {
    globalConfig.amqpConfig.server match {
      case None         ⇒ throw new IllegalArgumentException("Not a valid server for AMQP in config " + globalConfig.amqpConfig)
      case Some(server) ⇒ new GenericSensor(config, server, globalConfig.amqpDeviceInputConfig)
    }
  }

  override def types: Map[String, Class[_]] = Map(
    "temperature" -> classOf[Double],
    "humidity" -> classOf[Double],
    "open-door" -> OpenDoor.getClass)

  override def aliases: Map[String, String] = Map(
    "door" -> "open-door",
    "temp" -> "temperature",
    "hum" -> "humidity")

  override def descriptionOf(value: Reading[_]): String = value.name match {
    case "open-door" ⇒ value.value.asInstanceOf[Boolean] match {
      case true  ⇒ "Door open"
      case false ⇒ "Door closed"
    }
    case _ ⇒ "%s %s".format(value.name, value.value.toString)

  }

}

object OpenDoor {
  def toBoolean(value: String): Boolean = {
    value match {
      case "Door Open"   ⇒ true
      case "true"        ⇒ true
      case "Door Closed" ⇒ false
      case "false"       ⇒ false
    }
  }
}

