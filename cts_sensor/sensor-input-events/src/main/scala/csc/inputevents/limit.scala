package csc.inputevents

import csc.inputevents.sensor.{ BooleanReading, DoubleReading, Reading }

trait ReadingLimit {
  def apply(reading: Reading[_]): Boolean
}

case class NumericRangeLimit(min: Double, max: Double) extends ReadingLimit {

  override def apply(reading: Reading[_]): Boolean = reading match {
    case DoubleReading(_, value) ⇒ value < min || value >= max
  }
}

case class MatchLimit(okValues: String*) extends ReadingLimit {

  override def apply(reading: Reading[_]): Boolean = reading match {
    case BooleanReading(_, value) ⇒ !okValues.contains(value.toString)
  }
}
