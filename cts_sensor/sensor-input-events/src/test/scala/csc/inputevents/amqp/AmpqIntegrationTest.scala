package csc.inputevents.amqp

import akka.actor.{ Actor, ActorRef, ActorSystem, Props }
import akka.testkit.{ TestKit, TestProbe }
import com.github.sstone.amqp.Amqp.Delivery
import csc.amqp.{ MQConnection, PublisherActor }
import csc.inputevents.actor.DeviceInputMessage
import net.liftweb.json.{ DefaultFormats, Formats }

import scala.concurrent.duration._
import org.scalatest._

/**
 * Created by carlos on 12/10/16.
 */
class AmpqIntegrationTest extends TestKit(ActorSystem("AmpqIntegrationTest"))
  with WordSpecLike
  with MustMatchers
  with BeforeAndAfterAll
  with BeforeAndAfterEach {

  "AMQP Producer/Consumer" ignore {
    "publish and consume messages" in {

      val mqc = createConnection()
      val probe = TestProbe()
      val prod = mqc.producer
      val producer = system.actorOf(Props(new TestProducer(prod)))
      val handler = system.actorOf(Props(new TestConsumer(probe.ref)))
      mqc.createSimpleConsumer("gantry-device-events", handler, None, true)

      val now = System.currentTimeMillis()
      val input = List(DeviceInputMessage("sensor", DeviceInputMessage.format(now), "key", "value"))
      producer ! input

      val output = probe.expectMsgType[List[DeviceInputMessage]]
      output must be(input)
    }
  }

  def createConnection(): MQConnection =
    MQConnection.create("AmpqIntegrationTest", "amqp://ctes:ctes@localhost:5672/", 5 seconds)(system)

  def messageParser: Delivery ⇒ Either[String, List[DeviceInputMessage]] =
    csc.inputevents.actor.AmqpDeviceReceiverActor.parseMessages

  class TestConsumer(target: ActorRef) extends Actor {
    override def receive: Receive = {
      case delivery: Delivery ⇒ messageParser(delivery) match {
        case Left(error) ⇒ "Error parsing messages from " + delivery
        case Right(list) ⇒ target ! list
      }
      case other ⇒ println(other)
    }
  }

  class TestProducer(val producer: ActorRef) extends PublisherActor {
    override val producerEndpoint: String = "to-gantry-exchange"
    override def producerRouteKey(msg: AnyRef): String = "SensorInputMessage"
    override val ApplicationId: String = "TestProducer"
    override implicit val formats: Formats = DefaultFormats
  }

  class EchoActor extends Actor {
    override def receive: Receive = {
      case msg ⇒ println("received " + msg)
    }
  }

}

