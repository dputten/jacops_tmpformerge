package csc.inputevents

import java.util.concurrent.TimeUnit

import akka.actor.{ ActorRef, ActorSystem }
import akka.testkit.{ TestKit, TestProbe }
import csc.amqp.MQConnection
import scala.concurrent.duration.{ FiniteDuration, Duration }
import csc.systemevents.SystemEvent
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach }

/**
 * Created by carlos on 19/05/16.
 */
class AppCoreTest extends TestKit(ActorSystem("AppCoreTest"))
  with WordSpecLike
  with MustMatchers
  with BeforeAndAfterAll
  with BeforeAndAfterEach {

  import AppCoreTest._

  override protected def afterAll(): Unit = {
    system.shutdown()
  }

  "AppCore" must {

    val probe = TestProbe()
    val app = new NoAmqpCore(probe.ref)

    "Receive the initial events for all the configured sensors and readings" in {

      app.startup()

      val expectedSensorReadings = expectedSensorAndReadingIds
      val expectedEventCount = expectedSensorReadings.size
      val initial = takeEvents(probe, expectedEventCount)

      verifyEvents(initial, expectedSensorReadings, "INITIAL")
    }
  }

  def verifyEvents(events: List[SystemEvent], expectedSensorReadings: Set[(SensorConfig, ReadingLimitId)], expectedType: String): Unit = {
    events.size must be(expectedSensorReadings.size)

    events foreach { event ⇒
      event.systemId must be(systemId)
      event.laneId must be(None)
      event.corridorId must be(None)
      event.gantryId must be(Some(gantryId))
      event.userId must be("system")
    }

    val expectedComponentAndTypes: Set[(String, String)] = expectedSensorReadings map { e ⇒
      (e._1.id, e._2.description + "-" + expectedType)
    }

    val actualComponentsAndTypes: Set[(String, String)] = events map { e ⇒ (e.componentId, e.eventType) } toSet

    actualComponentsAndTypes must be(expectedComponentAndTypes)
  }

  def takeEvents(probe: TestProbe, expect: Int): List[SystemEvent] = {
    val time = System.currentTimeMillis()
    val totalTimeout = pollFrequency * 2 //some wild guess
    var timeout = totalTimeout
    var result: List[SystemEvent] = Nil
    do {
      val evt = probe.expectMsgType[SystemEvent](timeout)
      val ellapsed = System.currentTimeMillis() - time
      result = result :+ evt
      timeout = totalTimeout - Duration(ellapsed, TimeUnit.MILLISECONDS)
    } while (result.size < expect && timeout.toMillis > 0)

    if (result.size < expect)
      fail("Timeout while waiting for next message. Only got %s out of %s expected messages".format(result.size, expect))

    result
  }

  def repeat(n: Int)(f: ⇒ Unit) = 1 to n foreach (_ ⇒ f)

  class NoAmqpCore(queue: ActorRef) extends AppCore(system, configuration, Some(queue)) {
    override def createAmqpConnection: MQConnection = null //no AMQP connection
    override def wireAmpqConsumer(listener: ActorRef): Unit = () //nothing to be wired
  }
}

object AppCoreTest {
  import ConfigurationTest._
  import csc.inputevents.sensor.Component._

  val systemId = "A4"
  val gantryId = "E1"

  lazy val pollFrequency = FiniteDuration(3, TimeUnit.SECONDS)
  lazy val shoeboxSensor = SensorConfig("ECAMBOX1", cambox, None, false)
  lazy val stationSensor = SensorConfig("ESTATION", station, None, false)
  lazy val allSensors = List(shoeboxSensor, stationSensor)
  lazy val gantrySensorMap = Map(gantryId -> allSensors)
  lazy val initialDelay = pollFrequency

  lazy val configuration = Configuration(systemId, None, gantrySensorMap, readingLimitConfigs, amqpConfig, amqpServicesConfig, amqpDeviceInputConfig, Some(stubConfig))

  def expectedSensorAndReadingIds: Set[(SensorConfig, ReadingLimitId)] = {
    val readingLimitIds: Set[ReadingLimitId] = configuration.readingLimitConfigs map { e ⇒ e.id } toSet
    var result: Set[(SensorConfig, ReadingLimitId)] = Set.empty

    configuration.sensors foreach { s ⇒
      val toAdd = for (r ← readingLimitIds if (r.readingId.component == s.component)) yield (s, r)
      result = result ++ toAdd
    }

    result
  }

}
