package csc.inputevents

import java.util.concurrent.TimeUnit

import scala.concurrent.duration.{ FiniteDuration, Duration }
import com.typesafe.config.ConfigFactory
import csc.amqp.{ ServicesConfiguration, AmqpConfiguration }
import csc.inputevents.sensor.{ Component, ReadingId }
import org.scalatest.WordSpec
import org.scalatest._
import ConfigurationTest._

/**
 * Created by carlos on 18/05/16.
 */
class ConfigurationTest extends WordSpec with MustMatchers {

  "Configuration" must {

    "load the fallback config" in {

      val config = Configuration.create(None)

      config.amqpConfig must be(amqpConfig)
      config.amqpServicesConfig must be(amqpServicesConfig)
      config.readingLimitConfigs must be(readingLimitConfigs)

      config.getReadingLimits(Component.cambox).size must be(4)
      config.getReadingLimits(Component.station).size must be(4)

      //empty items
      config.stubConfig must be(None)
      config.systemId must be("")
      config.gantrySensorMap.size must be(0)
    }

    "load a full config" in {
      val config = Configuration.create(Some(ConfigFactory.load("sample")))
      config.systemId must be("A4")
      config.stubConfig must be(Some(stubConfig))

      config.gantrySensorMap.size must be(2)

      assertSensors(expectedE1Sensors, config.gantrySensorMap.get("E1"))
      assertSensors(expectedX1Sensors, config.gantrySensorMap.get("X1"))

      config.readingLimitConfigs must be(readingLimitConfigs)
    }
  }

  def assertSensors(expected: List[SensorConfig], actual: Option[List[SensorConfig]]): Unit = {
    val expectedMap = expected map { e ⇒ e.id -> e } toMap
    val actualMap = actual match {
      case None       ⇒ Map.empty
      case Some(list) ⇒ list map { e ⇒ e.id -> e } toMap
    }
    actualMap must be(expectedMap)
  }
}

object ConfigurationTest {

  import csc.inputevents.sensor.Component._

  lazy val expectedE1Sensors = List(
    SensorConfig("E1CAMBOX1", cambox, None, false),
    SensorConfig("E1STATION", station, None, false))

  //exit sensors have the same values, and id is Xxxx instead of Exxx
  lazy val expectedX1Sensors = expectedE1Sensors map { e ⇒ e.copy(id = e.id.replaceFirst("E", "X")) }

  lazy val stubInitialValues: Map[ReadingId, Any] = Map(
    ReadingId(station, "open-door") -> false,
    ReadingId(cambox, "open-door") -> false,
    ReadingId(cambox, "temperature") -> 10,
    ReadingId(station, "temperature") -> 10,
    ReadingId(station, "humidity") -> 50,
    ReadingId(cambox, "humidity") -> 50)

  lazy val stubConfig = StubConfig("mina:tcp://localhost:12520?textline=true&sync=false", stubInitialValues)

  lazy val amqpConfig = AmqpConfiguration("SensorInputEventsActor", "amqp://ctes:ctes@localhost:5672/", FiniteDuration(5, TimeUnit.SECONDS))

  import LimitQualifier._

  lazy val readingLimitConfigs = List(
    ReadingLimitConfig(ReadingLimitId(ReadingId(cambox, "temperature"), functioning), NumericRangeLimit(-20, 50)),
    ReadingLimitConfig(ReadingLimitId(ReadingId(cambox, "temperature"), critical), NumericRangeLimit(-20, 55)),
    ReadingLimitConfig(ReadingLimitId(ReadingId(cambox, "humidity"), functioning), NumericRangeLimit(5, 95)),
    ReadingLimitConfig(ReadingLimitId(ReadingId(cambox, "open-door"), functioning), MatchLimit("false")),
    ReadingLimitConfig(ReadingLimitId(ReadingId(station, "temperature"), functioning), NumericRangeLimit(-20, 50)),
    ReadingLimitConfig(ReadingLimitId(ReadingId(station, "temperature"), critical), NumericRangeLimit(-20, 55)),
    ReadingLimitConfig(ReadingLimitId(ReadingId(station, "humidity"), functioning), NumericRangeLimit(5, 95)),
    ReadingLimitConfig(ReadingLimitId(ReadingId(station, "open-door"), functioning), MatchLimit("false")))

  lazy val amqpServicesConfig = ServicesConfiguration("", "gantry-out-exchange", "Gantry.SystemEvent", 5)
  lazy val amqpDeviceInputConfig = ServicesConfiguration("gantry-device-events", "", "", 5)

}