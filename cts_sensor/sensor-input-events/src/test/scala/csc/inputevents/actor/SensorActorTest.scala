package csc.inputevents.actor

import akka.actor._
import akka.testkit.{ TestKit, TestProbe }
import scala.concurrent.duration.Duration
import csc.akka.logging.DirectLogging
import csc.inputevents._
import csc.inputevents.sensor._
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }

/**
 * Created by carlos on 23/05/16.
 */
class SensorActorTest
  extends TestKit(ActorSystem("SensorActorTest"))
  with WordSpecLike
  with MustMatchers
  with BeforeAndAfterAll
  with BeforeAndAfterEach {

  import SensorActorTest._

  var sensorStubManager: SensorStubManager = null
  var probe: TestProbe = null
  var actor: ActorRef = null

  override protected def beforeEach(): Unit = {
    sensorStubManager = new LocalSensorStubManager
    probe = TestProbe()
  }

  override protected def afterEach(): Unit = {
    actor = null //cleanup
  }

  override protected def afterAll(): Unit = {
    system.shutdown()
  }

  private def sensorCreated(sensor: Sensor): Unit = {
    if (actor == null) actor = system.actorOf(Props(new SensorActor(sensor, None, evalMap, probe.ref)))
    else fail("Actor was already created")
  }

  private def _sendInputMessages(msg: DeviceInputMessages): Unit = {
    if (actor == null) fail("Actor should have been created by now")
    else actor ! msg
  }

  def initializeStub(): SensorReadingMessage = {
    sensorStubManager.addStub(sensorConfig)
    probe.expectMsgType[SensorReadingMessage]
  }

  def verifyMessage(msg: SensorReadingMessage, initial: Boolean, alarm: Boolean, tempValue: Double): Unit = {
    msg.sensorId must be(sensorId)
    msg.limitId must be(readingLimitId)
    msg.initial must be(initial)
    msg.alarm must be(alarm)
    val reading = temperatureReadingOf(tempValue)
    msg.description must be(Component.descriptionOf(sensorConfig.component, reading))
  }

  "SensorActor" must {

    "notify the initial value" in {
      val msg = initializeStub()
      verifyMessage(msg, true, false, initialReadingValue)
    }

    "not notify when values change within limits" in {
      initializeStub()
      sensorStubManager.setValue(sensorId, temperatureReading, 0) //value inside limits
      probe.expectNoMsg //(Duration("3 seconds"))
    }

    "notify when values change outside/inside limits" in {
      initializeStub()

      //testing going off-limits
      sensorStubManager.setValue(sensorId, temperatureReading, 5.5) //value outside limits
      val msg1 = probe.expectMsgType[SensorReadingMessage]
      verifyMessage(msg1, false, true, 5.5)

      //testing going back to normal
      sensorStubManager.setValue(sensorId, temperatureReading, -2) //value inside limits
      val msg2 = probe.expectMsgType[SensorReadingMessage]
      verifyMessage(msg2, false, false, -2)
    }

  }

  class LocalSensorStubManager extends SensorStubManager with DirectLogging {

    override def config: StubConfig = SensorActorTest.stubConfig

    override def sendInputMessages(msg: DeviceInputMessages): Unit = _sendInputMessages(msg)

    override val sensorAction: (Sensor) ⇒ Unit = sensorCreated
  }
}

object SensorActorTest {

  val sensorId = "mySensor"
  val temperatureReading = "temperature"
  val readingId = ReadingId(Component.cambox, temperatureReading)
  val readingLimitId = ReadingLimitId(readingId, LimitQualifier.critical)
  val initialReadingValue: Double = 0.5 //chilly

  val initialDelay = Duration("1 second")

  val sensorConfig = SensorConfig(sensorId, Component.cambox, None, false)
  val stubConfig = StubConfig("", Map(readingId -> initialReadingValue))
  val evalMap = Map(readingLimitId -> NumericRangeLimit(-5, 5))

  def temperatureReadingOf(value: Double) = DoubleReading(temperatureReading, value)
}

