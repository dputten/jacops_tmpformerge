package csc.inputevents.actor

import java.util.concurrent.TimeUnit

import akka.actor.{ ActorRef, ActorSystem, Props }
import akka.testkit.{ TestKit, TestProbe }
import scala.concurrent.duration.{ FiniteDuration, Duration }
import com.github.sstone.amqp.Amqp.Delivery
import com.rabbitmq.client.AMQP
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }

/**
 * Created by carlos on 12/06/16.
 */
class AmqpDeviceReceiverActorTest
  extends TestKit(ActorSystem("AmqpDeviceReceiverActorTest"))
  with WordSpecLike
  with MustMatchers
  with BeforeAndAfterAll
  with BeforeAndAfterEach {

  import AmqpDeviceReceiverActor._

  val delay = FiniteDuration(2, TimeUnit.SECONDS)
  val sensor = "MySensor"
  var probe: TestProbe = null
  var actor: ActorRef = null

  override protected def beforeEach(): Unit = {
    probe = TestProbe()
    actor = system.actorOf(Props(new AmqpDeviceReceiverActor(delay)))
    actor ! AddSensor(sensor, probe.ref)
  }

  override protected def afterAll(): Unit = {
    system.shutdown()
  }

  val multipleMsgPayload = """[{"sensor":"E1BOX1", "datetime":"20160613.132118.204", "name":"temperature", "value":"25.0"},{"sensor":"E1BOX1", "datetime":"20160613.132118.204", "name":"humidity", "value":"57.2"}]"""
  val singleMsgPayload = """[{"sensor":"E1BOX1", "datetime":"20160613.132133.133", "name":"humidity", "value":"56.6"}]"""

  def createMessage(time: Long): DeviceInputMessages =
    DeviceInputMessages(List(DeviceInputMessage(sensor, DeviceInputMessage.format(time), "reading", time.toString)))

  "DeviceInputMessageExtractor" must {

    "extract single DeviceInputMessage from a MQ Delivery " in {
      val delivery = createDelivery(singleMsgPayload, None)
      val result = AmqpDeviceReceiverActor.parseMessages(delivery)

      result must be(Right(List(DeviceInputMessage("E1BOX1", "20160613.132133.133", "humidity", "56.6"))))
    }

    "extract single DeviceInputMessage from a MQ Delivery when the content type is given" in {
      val delivery = createDelivery(singleMsgPayload, Some("ISO-8859-1"))
      val result = AmqpDeviceReceiverActor.parseMessages(delivery)

      result must be(Right(List(DeviceInputMessage("E1BOX1", "20160613.132133.133", "humidity", "56.6"))))
    }

    "extract multiple DeviceInputMessages from a MQ Delivery " in {
      val delivery = createDelivery(multipleMsgPayload, None)
      val result = AmqpDeviceReceiverActor.parseMessages(delivery)

      result must be(Right(List(
        DeviceInputMessage("E1BOX1", "20160613.132118.204", "temperature", "25.0"),
        DeviceInputMessage("E1BOX1", "20160613.132118.204", "humidity", "57.2"))))
    }

  }

  "AmqpDeviceReceiverActor" must {

    "Delay old message and send it if still the most recent" in {
      val msg = createMessage(System.currentTimeMillis() - oldMessageThresholdMillis * 2)
      actor ! msg
      val result = probe.expectMsgType[DeviceInputMessages]
      result must be(msg)
    }

    "Delay old message and discard it if no longer the most recent" in {
      val msg1 = createMessage(System.currentTimeMillis() - oldMessageThresholdMillis * 2)
      actor ! msg1
      val msg2 = createMessage(System.currentTimeMillis())
      actor ! msg2
      val result = probe.expectMsgType[DeviceInputMessages]
      result must be(msg2)

      probe.expectNoMsg(delay * 2)
    }
  }

  def createDelivery(payload: String, contentEncoding: Option[String]): Delivery = {
    val builder = new AMQP.BasicProperties.Builder()
    val properties = (contentEncoding match {
      case Some(contentEncoding) ⇒ builder.contentEncoding(contentEncoding)
      case None                  ⇒ builder
    }).`type`("DeviceInputMessages").contentType("application/json").build()
    Delivery("", null, properties, payload.getBytes(contentEncoding.getOrElse("UTF-8")))
  }

}
