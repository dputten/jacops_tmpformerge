package csc.inputevents.actor

import java.util.concurrent.{ CountDownLatch, TimeUnit }

import akka.actor.{ ActorRef, ActorSystem, Props }
import akka.camel.CamelExtension
import akka.testkit.{ TestKit, TestProbe }
import com.typesafe.config.ConfigFactory
import csc.akka.logging.DirectLogging
import csc.akka.remote.SocketServer
import csc.inputevents.Configuration
import csc.inputevents.sensor.Component.Comp
import csc.inputevents.sensor.{ Component, Sensor }
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }

import scala.concurrent.Await
import scala.concurrent.duration._

/**
 * Created by carlos on 24/05/16.
 */
class SensorStubAndClientTest
  extends TestKit(ActorSystem("SensorStubAndClientTest"))
  with WordSpecLike
  with MustMatchers
  with BeforeAndAfterAll
  with BeforeAndAfterEach
  with DirectLogging {

  val config = Configuration.create(Some(ConfigFactory.load("sample")))
  val stubConfig = config.stubConfig.get
  val sensorConfigs = config.sensors filter (_.component == Component.cambox) //only shoeboxes
  val sensorIds: Set[String] = sensorConfigs map { c ⇒ c.id } toSet
  val shoeboxIds = sensorIds.toList
  val sensor1Id = shoeboxIds(0)
  val sensor2Id = shoeboxIds(1)
  val client = new SensorStubManagerClient(SocketServer(stubConfig.stubUrl).get)

  var sensorMap: Map[String, Sensor] = Map.empty //keeps all sensors in a map (by id)
  val sensorsReadyLatch = new CountDownLatch(sensorConfigs.size)
  var commandLatch: Option[CountDownLatch] = None
  val commandListener: String ⇒ Unit = { s ⇒ commandLatch map (_.countDown()) }
  val door = "open-door"
  val temperature = "temperature"
  val humidity = "humidity"

  override protected def afterAll(): Unit = {
    system.shutdown()
  }

  def sensorAdded(sensor: Sensor): Unit = {
    sensorMap = sensorMap.updated(sensor.config.id, sensor)
    sensorsReadyLatch.countDown()
  }

  lazy val allDefaultValues: Map[Comp, Map[String, String]] = {
    val components: Set[Comp] = sensorConfigs map { s ⇒ s.component } toSet

    components map { c ⇒
      val values = stubConfig.getInitialValues(c) map { e ⇒ e._1 -> e._2.toString }
      c -> values
    } toMap
  }

  /**
   * Sends a command from the client to the stub manager, waiting for it to be confirmed (at most 2 seconds)
   * @param cmd
   */
  def clientSend(cmd: String): Unit = {
    val latch = new CountDownLatch(1)
    commandLatch = Some(latch)
    client.send(cmd)
    latch.await(2, TimeUnit.SECONDS)
  }

  def verifyMessage(msg: DeviceInputMessage, sensor: String, name: String, value: String): Unit = {
    msg.sensor must be(sensor)
    msg.name must be(name)
    if (name == door) msg.value must be(value)
    else msg.value.toDouble must be(value.toDouble)
  }

  def verifyInitialValues(msg: DeviceInputMessages): Unit = {
    msg.list foreach { m ⇒
      val sensor = sensorMap(m.sensor)
      val expectedValue = allDefaultValues(sensor.config.component)(m.name)
      verifyMessage(m, m.sensor, m.name, expectedValue)
    }
  }

  "SensorStubManagerActor/SensorStubManagerClient" must {

    val probe = TestProbe()
    val deviceReceiver: ActorRef = probe.ref
    val actor = system.actorOf(Props(new SensorStubManagerActor(stubConfig, sensorAdded, deviceReceiver, Some(commandListener))))

    val camel = CamelExtension(system)
    val future = camel.activationFutureFor(actor)(
      timeout = 10 seconds,
      executor = system.dispatcher)
    Await.ready(future, 10.seconds)

    sensorConfigs.foreach { s ⇒
      actor ! AddSensorStub(s) //add stubs to manager
    }

    sensorsReadyLatch.await(3, TimeUnit.SECONDS) //awaits sensors to be ready
    sensorMap.keySet must be(sensorIds) //making sure they are all there

    import SensorStubManagerClient._

    "work together to change stub reading values and reset them" in {

      val initialTempValue = allDefaultValues(Component.cambox)(temperature)
      val initialHumidityValue = allDefaultValues(Component.cambox)(humidity)
      val initialDoorValue = allDefaultValues(Component.cambox)(door)
      val newTempValue = (initialTempValue.toDouble + 5).toString

      //confirming the stub manager sends a DeviceInputMessages about sensor1 new temperature value
      clientSend(setValueCommand(sensor1Id, temperature, newTempValue))
      val inputMsg1 = probe.expectMsgType[DeviceInputMessages](30.seconds)
      inputMsg1.list.size must be(1)
      verifyMessage(inputMsg1.list(0), sensor1Id, temperature, newTempValue)

      //confirming the stub manager sends a DeviceInputMessages about sensor2 new temperature value
      clientSend(setValueCommand(sensor2Id, temperature, newTempValue))
      val inputMsg2 = probe.expectMsgType[DeviceInputMessages]
      inputMsg2.list.size must be(1)
      verifyMessage(inputMsg2.list(0), sensor2Id, temperature, newTempValue)

      //confirming the stub manager sends a DeviceInputMessages about all sensors1 readings (following a reset)
      clientSend(resetCommand(Some(sensor1Id))) //reset one
      val inputMsg3 = probe.expectMsgType[DeviceInputMessages]
      inputMsg3.list.size must be(3)
      val doorMsg = inputMsg3.list.find(_.name == door)
      verifyMessage(doorMsg.get, sensor1Id, door, initialDoorValue)
      val temperatureMsg = inputMsg3.list.find(_.name == temperature)
      verifyMessage(temperatureMsg.get, sensor1Id, temperature, initialTempValue)
      val humidityMsg = inputMsg3.list.find(_.name == humidity)
      verifyMessage(humidityMsg.get, sensor1Id, humidity, initialHumidityValue)

      //just changing the value of sensor1 temp
      clientSend(setValueCommand(sensor1Id, temperature, newTempValue))
      val inputMsg4 = probe.expectMsgType[DeviceInputMessages]
      inputMsg4.list.size must be(1)
      verifyMessage(inputMsg4.list(0), sensor1Id, temperature, newTempValue)

      //confirming the stub manager sends a DeviceInputMessages for each sensor, with the initial values
      clientSend(resetCommand(None)) //reset all

      val afterResetMsg1 = probe.expectMsgType[DeviceInputMessages]
      verifyInitialValues(afterResetMsg1)
      val afterResetMsg2 = probe.expectMsgType[DeviceInputMessages]
      verifyInitialValues(afterResetMsg2)

      var set: Set[String] = Set.empty
      afterResetMsg1.list foreach { m: DeviceInputMessage ⇒ set = set + m.sensor }
      afterResetMsg2.list foreach { m: DeviceInputMessage ⇒ set = set + m.sensor }
      set must be(Set(sensor1Id, sensor2Id))
    }

  }

}
