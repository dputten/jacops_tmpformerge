/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.pir.impl

import org.jboss.netty.buffer.ChannelBuffer
import org.scalatest.WordSpec
import org.scalatest._

/**
 * Test the Encode and decode classes for the PirMessages
 */
class EncodeDecodeTest extends WordSpec with MustMatchers {

  val decode = new PIRDecoder()
  val encode = new PIREncoder()
  "Encode and decode on OKResponse" must {
    "result in same object" in {

      val response = new OKResponse()
      val buffer = encode.encode(null, null, response)
      //decode
      val decoded = decode.decode(null, null, buffer.asInstanceOf[ChannelBuffer])
      decoded must be(response)
    }
  }
  "Encode and decode on ResetComm" must {
    "result in same object" in {

      val msg = new ResetComm(2)
      val buffer = encode.encode(null, null, msg)
      //decode
      val decoded = decode.decode(null, null, buffer.asInstanceOf[ChannelBuffer])
      decoded must be(msg)
    }
  }
  "Encode and decode on GetTrafficData" must {
    "result in same object using FCB is false" in {
      val msg = new GetTrafficData(2, false)
      val buffer = encode.encode(null, null, msg)
      //decode
      val decoded = decode.decode(null, null, buffer.asInstanceOf[ChannelBuffer])
      decoded must be(msg)
    }
    "result in same object using FCB is true" in {

      val msg = new GetTrafficData(2, true)
      val buffer = encode.encode(null, null, msg)
      //decode
      val decoded = decode.decode(null, null, buffer.asInstanceOf[ChannelBuffer])
      decoded must be(msg)
    }
  }
  "Encode and decode on TrafficResponse" must {
    "result in same object without vehicles" in {
      val msg = new TrafficResponse(
        pirAddress = 3,
        status = 0x04,
        lifetimeVehicleCount = None,
        vehicleList = List())
      val buffer = encode.encode(null, null, msg)
      //decode
      val decoded = decode.decode(null, null, buffer.asInstanceOf[ChannelBuffer])
      decoded must be(msg)
    }
    "result in same object with vehicles" in {
      val vehicle1 = new VehicleData(speed = 125,
        vehiclePos = 2,
        vehicleClass = 1,
        occupancy = 300,
        timeGap = 200,
        length = 4.1)
      val vehicle2 = new VehicleData(speed = 88,
        vehiclePos = 1,
        vehicleClass = 4,
        occupancy = 300,
        timeGap = 200,
        length = 20.7)

      val msg = new TrafficResponse(
        pirAddress = 3,
        status = 0x04,
        lifetimeVehicleCount = Some(2222),
        vehicleList = List(vehicle1, vehicle2))
      val buffer = encode.encode(null, null, msg)
      //decode
      val decoded = decode.decode(null, null, buffer.asInstanceOf[ChannelBuffer])
      decoded must be(msg)
    }
  }

}