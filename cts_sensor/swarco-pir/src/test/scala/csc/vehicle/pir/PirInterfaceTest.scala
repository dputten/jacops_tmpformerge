/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.pir

import akka.actor.{ ActorSystem, Props }
import akka.testkit.{ TestKit, TestProbe }
import scala.concurrent.duration._
import csc.akka.logging.DirectLogging
import csc.gantry.config.PIRConfig
import csc.vehicle.message.Lane
import csc.vehicle.pir.impl.{ PIRDirectorActor, PIRLaneConfiguration }
import csc.vehicle.tools.pir.SendPIRRegistration
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, WordSpec }

/**
 * Test the StoreVehicleRegistrationFile
 */
class PirInterfaceTest extends TestKit(ActorSystem("PirInterfaceTest")) with WordSpecLike with MustMatchers with BeforeAndAfterAll with DirectLogging {
  val config = new PIRConfig("localhost", 11000, 1, 1000, vrHost = "localhost", vrPort = 1100)
  val lane = new Lane("A2-20MH-Lane1", "Lane1", "20MH", "A2", 10, 10)
  val probe = TestProbe()
  val pirSimulator = new SendPIRRegistration(config.pirHost, config.pirPort, config.pirAddress)

  val conf = new PIRLaneConfiguration(lane = lane, pir = config, recipients = Seq(probe.ref))
  val connection = system.actorOf(Props(new PIRDirectorActor(timeout = 200 millis, config = Seq(conf), dynamicConfidence = false)))

  override def afterAll() {
    pirSimulator.stopPIR
    system.shutdown
  }

  "connection with PIR sensor" must {
    "create message" in {
      val speed = 80
      val length = 4.50
      pirSimulator.sendRegistration(speed, length, 0, 1)
      val recv = probe.expectMsgType[VehiclePIRRegistration](20 minutes) //seconds)
      recv.lane must be(lane)
      recv.speed.get must be(speed)
      recv.length.get must be(length)
    }
    "status message" in {
      pirSimulator.UpdateStatus(1)
      probe.expectNoMsg(10 seconds)
      pirSimulator.UpdateStatus(0)
    }
    "create message with multiple vehicles" in {
      val speed = 80
      val length = 4.50
      pirSimulator.sendRegistration(speed, length, 0, 1)
      pirSimulator.sendRegistration(speed, length, 0, 1)
      val recvList = probe.receiveN(2, 20 seconds)
      recvList must have size (2)
      var recv = recvList.head.asInstanceOf[VehiclePIRRegistration]
      recv.lane must be(lane)
      recv.speed.get must be(speed)
      recv.length.get must be(length)
      recv = recvList.last.asInstanceOf[VehiclePIRRegistration]
      recv.lane must be(lane)
      recv.speed.get must be(speed)
      recv.length.get must be(length)
      probe.expectNoMsg(10 seconds)
    }
  }
}