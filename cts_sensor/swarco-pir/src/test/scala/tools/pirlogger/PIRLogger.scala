/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package tools.pirlogger

import csc.akka.logging.DirectLogging
import java.io.{ IOException, InputStreamReader, BufferedReader }
import csc.gantry.config.PIRConfig
import akka.actor.{ ActorSystem, Actor, ActorLogging, Props }
import csc.vehicle.pir.impl.{ PIRLaneConfiguration, PIRDirectorActor }
import scala.concurrent.duration._
import csc.vehicle.message.Lane

/**
 * This is the object which starts the logger.
 * It is responsible
 * to connect to the pir and log the messages
 *
 */
object PIRLogger extends DirectLogging {
  type OptionMap = Map[Symbol, String]
  var reader = new BufferedReader(new InputStreamReader(System.in))

  /**
   * Parse the command arguments
   */
  def nextOption(map: OptionMap, list: List[String]): OptionMap = {
    list match {
      case Nil ⇒ map
      case "--host" :: value :: tail ⇒
        nextOption(map ++ Map('host -> value), tail)
      case "--help" :: tail ⇒
        nextOption(map ++ Map('help -> ""), tail)
      case "--port" :: value :: tail ⇒
        nextOption(map ++ Map('port -> value), tail)
      case "--address" :: value :: tail ⇒
        nextOption(map ++ Map('address -> value), tail)
      case "--timer" :: value :: tail ⇒
        nextOption(map ++ Map('timer -> value), tail)
      case "--confidence" :: value :: tail ⇒
        nextOption(map ++ Map('confidence -> value), tail)
      case option :: tail ⇒
        println("Unknown option " + option)
        sys.exit(1)
    }
  }

  /**
   * Prints the usage string to standard out
   */
  def printUsage() {
    println("PIRLogger Usage --host <host> --port <port> [options]")
    println("\t--host <host>: host name of the PIR")
    println("\t--port <port>: the port of the PIR")
    println("\t--address <piraddress>: the pir address (default 1)")
    println("\t--timer <msec>: time between Pir queries (default 1000 msec)")
    println("\t--confidence <true/false>: does pir deliver confidence)")
    println("\t--help: show the usage and script usage")
  }

  /**
   * The main tread.
   * Parse arguments and check them for correctness
   */
  def main(args: Array[String]): Unit = {
    val options = nextOption(Map(), args.toList)
    println(options)
    options.get('help) foreach {
      value ⇒
        {
          printUsage
          sys.exit(0)
        }
    }
    val hostOpt = options.get('host)
    val portOpt = options.get('port)
    if (hostOpt == None || portOpt == None) {
      println("Logger: host and port should be given")
      printUsage
      sys.exit(1)
    }
    val addr = options.get('address).getOrElse("1").toShort
    val timer = options.get('timer).getOrElse("1000").toLong
    val config = new PIRConfig(hostOpt.get, portOpt.get.toInt, addr, timer, vrHost = "localhost", vrPort = 1100)
    val lane = new Lane("A2-20MH-Lane1", "Lane1", "20MH", "A2", 10, 10)

    val system = ActorSystem("PIRLogger")
    val logActor = system.actorOf(Props(new MsgLogger))
    val conf = new PIRLaneConfiguration(lane = lane, pir = config, recipients = Seq(logActor))
    val useConfidence = options.get('confidence).getOrElse("false").toBoolean
    val connection = system.actorOf(Props(new PIRDirectorActor(timeout = timer millis, config = Seq(conf), dynamicConfidence = useConfidence)))

    //wait 
    var reader = new BufferedReader(new InputStreamReader(System.in))
    var inputLine: String = null;
    //  readLine() method
    try {
      System.out.print("Wait for command input: ");
      inputLine = reader.readLine();
    } catch {
      case ex: IOException ⇒ System.out.println("IO error while waiting for input. resuming scriptprocessing");
    } finally {
      reader.close()
    }

    //stop system
    System.out.println("Stopping Logger")

    system.stop(connection)
    system.stop(logActor)
    system.shutdown()
    System.out.println("Logger Stoped")
    sys.exit(0)
  }

}

class MsgLogger extends Actor with ActorLogging {
  def receive = {
    case any: AnyRef ⇒ log.info("Received: " + any.toString)
  }
}