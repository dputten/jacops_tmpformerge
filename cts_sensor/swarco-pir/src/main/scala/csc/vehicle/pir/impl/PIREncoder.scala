/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.pir.impl

import org.jboss.netty.handler.codec.oneone.OneToOneEncoder
import org.jboss.netty.channel.{ Channel, ChannelHandlerContext }
import org.jboss.netty.buffer.{ ChannelBuffers, ChannelBuffer }
import java.nio.ByteOrder
import csc.akka.logging.DirectLogging

/**
 * This class is responsible to encode the different PirMessages
 */
class PIREncoder extends OneToOneEncoder with DirectLogging {
  /**
   * this method is called by netty framework when a message has to be encoded.
   *
   * @param ctx the netty ChannelHandlerContext
   * @param channel The channel object
   * @param msg the message which has to be encoded
   * @return msg when a unsupported message is received and otherwise a ChannelBuffer with the encoded message
   */
  def encode(ctx: ChannelHandlerContext, channel: Channel, msg: Object): Object = {
    log.debug("Encode message")

    msg match {
      case rst: ResetComm ⇒ {
        val resultBuffer = ChannelBuffers.dynamicBuffer(ByteOrder.BIG_ENDIAN, FieldConstants.SHORTFRAME_SIZE)
        resultBuffer.writeByte(FieldConstants.SHORTFRAME)
        resultBuffer.writeByte(FieldConstants.CTRL_REQUEST_RESET)
        resultBuffer.writeByte(rst.pirAddress)
        val checksum = Checksum.calculateChecksum(FieldConstants.CTRL_REQUEST_RESET, rst.pirAddress, new Array[Byte](0))
        resultBuffer.writeByte(checksum)
        resultBuffer.writeByte(FieldConstants.END_BYTE)
        resultBuffer
      }
      case ok: OKResponse ⇒ {
        val resultBuffer = ChannelBuffers.dynamicBuffer(ByteOrder.BIG_ENDIAN, 1)
        resultBuffer.writeByte(FieldConstants.SINGLECHAR)
        resultBuffer
      }
      case getTraffic: GetTrafficData ⇒ {
        val resultBuffer = ChannelBuffers.dynamicBuffer(ByteOrder.BIG_ENDIAN, FieldConstants.SHORTFRAME_SIZE)
        resultBuffer.writeByte(FieldConstants.SHORTFRAME)
        val control = if (getTraffic.fcb) {
          FieldConstants.CTRL_REQUEST_TRAFFIC2
        } else {
          FieldConstants.CTRL_REQUEST_TRAFFIC
        }
        resultBuffer.writeByte(control)
        resultBuffer.writeByte(getTraffic.pirAddress)
        val checksum = Checksum.calculateChecksum(control, getTraffic.pirAddress, new Array[Byte](0))
        resultBuffer.writeByte(checksum)
        resultBuffer.writeByte(FieldConstants.END_BYTE)
        resultBuffer
      }
      case respTraffic: TrafficResponse ⇒ {
        val applLength = if (respTraffic.vehicleList.isEmpty) {
          3 //control, address and status =>3
        } else {
          //control, address, status and Lifetime vehicle Count (4) =>7
          7 + FieldConstants.LENGTH_VEHICLE_LENGTH * respTraffic.vehicleList.size
        }
        val msgLength = FieldConstants.LONGFRAME_SIZE_HEADER + applLength + FieldConstants.SIZE_TRAILER
        val resultBuffer = ChannelBuffers.dynamicBuffer(ByteOrder.BIG_ENDIAN, msgLength)
        //write header
        resultBuffer.writeByte(FieldConstants.LONGFRAME)
        resultBuffer.writeByte(applLength)
        resultBuffer.writeByte(applLength) //write length twice
        resultBuffer.writeByte(FieldConstants.LONGFRAME)
        //write application
        resultBuffer.writeByte(FieldConstants.CTRL_RESPONSE_TRAFFIC)
        resultBuffer.writeByte(respTraffic.pirAddress)
        //start application different data
        val applBuffer = ChannelBuffers.dynamicBuffer(ByteOrder.BIG_ENDIAN, applLength - 2)
        applBuffer.writeByte(respTraffic.status)
        if (!respTraffic.vehicleList.isEmpty) {
          applBuffer.writeInt(respTraffic.lifetimeVehicleCount.getOrElse(0))
          for (vehicle ← respTraffic.vehicleList) {
            applBuffer.writeByte(vehicle.speed)
            val lane = vehicle.vehiclePos << 6
            val classInfo = lane + vehicle.vehicleClass
            applBuffer.writeByte(classInfo)
            applBuffer.writeShort(vehicle.occupancy / 10)
            applBuffer.writeShort(vehicle.timeGap / 10)
            applBuffer.writeByte((vehicle.length * 10).toInt)
          }
        }
        resultBuffer.writeBytes(applBuffer)
        applBuffer.resetReaderIndex()
        val checksum = Checksum.calculateChecksum(FieldConstants.CTRL_RESPONSE_TRAFFIC, respTraffic.pirAddress, applBuffer.array())
        resultBuffer.writeByte(checksum)
        resultBuffer.writeByte(FieldConstants.END_BYTE)
        resultBuffer
      }
      case msg: AnyRef ⇒ msg //if object isn't supported just return the object itself so another Encoder can encode this one
    }
  }

}