/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.pir.impl

import java.util.Date
import collection.mutable.ListBuffer
import csc.vehicle.pir.VehiclePIRRegistration
import csc.akka.logging.DirectLogging
import csc.vehicle.message.Lane

/**
 * processing the message and create VehicleRegistrations.
 * EventTimes are calculated from the last time a request was send
 * @param lane
 */
class PIRMessageProcessor(lane: Lane, dynamicConfidence: Boolean) extends DirectLogging {
  var lastTimeMessage = new Date()
  var lastVehicleTime = new Date().getTime

  /**
   * A reset has been received
   * @param now
   */
  def noDataReceived(now: Date) {
    lastTimeMessage = now
  }

  /**
   * A traffic response has been received
   * @param now Time received, Should use time send by PIR but this is the next best thing
   * @param msg the received message
   * @return list of VehicleRegistrationMessage
   */
  def processTrafficResponse(now: Date, msg: TrafficResponse): Seq[VehiclePIRRegistration] = {
    val vehicles = new ListBuffer[VehiclePIRRegistration]
    //process messages
    val responseList = msg.vehicleList
    if (!responseList.isEmpty) {
      for (vehicleData ← responseList) {
        //check if the message isn't a "Request in queue"
        //Then speed is 0
        if (vehicleData.speed == 0 && (vehicleData.vehicleClass == 6 || vehicleData.vehicleClass == 32)) {
          log.debug("Vehicle in queue status: %s".format(vehicleData))
        } else {
          lastVehicleTime += vehicleData.timeGap
          val eventTime = new Date(lastVehicleTime)
          val length = if (vehicleData.length <= 0) None
          else {
            Some(vehicleData.length.toFloat)
          }
          val speed = if (vehicleData.speed <= 0) None
          else {
            Some(vehicleData.speed.toFloat)
          }
          val cat = if (vehicleData.vehicleClass <= 0) None
          else {
            Some(vehicleData.vehicleClass)
          }
          val confidence = {
            if (dynamicConfidence) {
              //occupancy is in 10 msec
              //when occupancy contains the confidence
              // dd.dd First part flow  fraction is the level of confidence of the classification.
              //so we can split the fields by dividing by 100
              val occField = vehicleData.occupancy
              val flow = occField / 100
              val confidence = occField % 100
              Some(confidence)
            } else {
              None
            }
          } //can be filled when PIR is supplying this

          vehicles += new VehiclePIRRegistration(
            lane = lane,
            eventTime = getEventTime(eventTime, lastTimeMessage, now).getTime,
            length = length,
            category = cat,
            speed = speed,
            confidence = confidence)
        }
      }
    }

    lastTimeMessage = now
    vehicles
  }

  def getEventTime(time: Date, last: Date, now: Date): Date = {
    val timeCor1 = if (time.before(last)) last else time
    if (time.after(now)) now else timeCor1
  }
}