/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.pir.impl

import java.util.concurrent.TimeUnit
import org.jboss.netty.util.{ Timer, TimerTask, Timeout }
import org.jboss.netty.bootstrap.ClientBootstrap
import akka.actor.ActorRef
import java.util.Date
import csc.akka.logging.DirectLogging
import java.util.concurrent.atomic.AtomicReference
import org.jboss.netty.channel.group.DefaultChannelGroup
import org.jboss.netty.channel.Channel

/**
 * Interface for the MessageProcessor
 */
trait MessageProcessor {
  def processReceivedMessage(msg: AnyRef, channel: Channel)

  def processDisconnect(channel: Channel)

  def processConnect(channel: Channel)
}

/**
 * The possible states of a PIR Connection.
 */
object ConnectionState extends Enumeration {
  val DISCONNECTED, OPERATIONAL, SHUTDOWN = Value
}

case class ReceivedMessage(time: Date, msg: AnyRef)
case class ConnectedMessage(time: Date)
case class DisConnectedMessage(time: Date)

/**
 * This class controls the message protocol of the PIR
 * @param bootstrap
 * @param timer
 * @param timeout
 * @param recipients
 */
class PIRConnectionProcessor(connectionId: String,
                             bootstrap: ClientBootstrap,
                             timer: Timer,
                             timeout: Long,
                             recipients: Set[ActorRef])
  extends MessageProcessor with DirectLogging {
  private val channelGroup = new DefaultChannelGroup()
  private val connectionState = new AtomicReference[ConnectionState.Value](ConnectionState.DISCONNECTED)
  private var timerTimeout: Option[Timeout] = None

  /**
   *  A message with the correct syntax is received and should be processed.
   * @param msg: the received message
   * @param channel: the channel where the message is received from
   */
  def processReceivedMessage(msg: AnyRef, channel: Channel) {
    val now = new Date()

    connectionState.get() match {
      case ConnectionState.DISCONNECTED ⇒ {
        //strange disconnected and still receiving messages or
        // connect event will probably come after this event so this is probably an old message
        // so ignore message
        log.warning("Unexpected message %s received while in state %s".format(msg.getClass.getName, connectionState))
      }
      case ConnectionState.OPERATIONAL ⇒ {
        recipients.foreach(_ ! new ReceivedMessage(now, msg))
      }
      case ConnectionState.SHUTDOWN ⇒ {
        //ignoring because we are shutting down
      }
    }
  }
  /**
   * A disconnect is received.
   */
  def processDisconnect(channel: Channel) {
    channelGroup.remove(channel)
    if (channelGroup.size > 0) {
      //disconnect of another channel
      log.warning(connectionId + ": Disconnect received and channel list is not empty")
    } else {
      //reconnect now when not in shutdown state.
      //the AtomicReference doesn't support the NOT check so we always update to disconnected and when it was a shutdown
      //we put it back. This will work because SHUTDOWN is always the last state.
      if (connectionState.getAndSet(ConnectionState.DISCONNECTED) == ConnectionState.SHUTDOWN) {
        connectionState.set(ConnectionState.SHUTDOWN)
      }
      recipients.foreach(_ ! new DisConnectedMessage(new Date()))
      if (connectionState.get() == ConnectionState.DISCONNECTED) {
        log.debug(connectionId + ": Sleeping for %d".format(timeout))
        timerTimeout = Some(
          timer.newTimeout(new TimerTask() {
            def run(timeout: Timeout) {
              if (Some(timeout) == timerTimeout) {
                log.info(connectionId + ": Reconnecting to: " + channel.getRemoteAddress())
                bootstrap.connect()
              } else {
                log.warning(connectionId + ": Skipped Reconnecting to: " + channel.getRemoteAddress())
              }
            }
          }, timeout, TimeUnit.MILLISECONDS))
      }
    }
  }

  /**
   * Receive a connect event.
   */
  def processConnect(channel: Channel) {
    if (channelGroup.size > 0) {
      log.warning(connectionId + ": Connect received before a disconnect")
    }
    channelGroup.add(channel)
    connectionState.set(ConnectionState.OPERATIONAL)
    recipients.foreach(_ ! new ConnectedMessage(new Date()))
  }

  def getState(): ConnectionState.Value = {
    connectionState.get()
  }

  def sendPIRMessage(msg: AnyRef) {
    if (connectionState.get() == ConnectionState.OPERATIONAL) {
      channelGroup.write(msg)
    } else {
      log.warning(connectionId + ": Send message while not operational")
      throw new IllegalStateException(connectionId + ": Send message while not operational")
    }
  }
  /**
   * Receive a shutdown.
   * Cancel the outstanding timers, change the state to shutdown and close the current connection
   */
  def shutdown() {
    timerTimeout.foreach(time ⇒ {
      if (!time.isExpired) {
        time.cancel
      }
    })
    timerTimeout = None
    connectionState.set(ConnectionState.SHUTDOWN)
    //close all connected channels, should be 0 or 1
    channelGroup.close
    log.info(connectionId + ": Shutdown")
  }
}
