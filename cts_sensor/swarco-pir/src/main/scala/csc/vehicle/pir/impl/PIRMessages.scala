/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.pir.impl

case class ResetComm(pirAddress: Short) {
  require(pirAddress <= 255, "Address is too large")
}

/**
 * Used as an Acknowledgement or no changes
 */
case class OKResponse()

case class UnsupportedResponse(control: Short, pirAddress: Short) {
  require(control <= 255, "Control is too large")
  require(pirAddress <= 255, "Address is too large")
}

/**
 * Get the traffic data
 * Possible responses is OKResponse or TrafficResponse
 * @param pirAddress
 */
case class GetTrafficData(pirAddress: Short, fcb: Boolean) {
  require(pirAddress <= 255, "Address is too large")
}

case class TrafficResponse(pirAddress: Short, status: Short, lifetimeVehicleCount: Option[Int], vehicleList: Seq[VehicleData]) {
  require(status <= 255, "Status is too large")
  require(vehicleList.size <= 4, "VehicleList is too large %d > 4".format(vehicleList.size))

  def hasError(): Boolean = {
    status != 0
  }

  def isIRNotification(): Boolean = {
    (status & 1) != 0
  }

  def isThermoNotification(): Boolean = {
    (status & 2) != 0
  }

  def isWrongWay(): Boolean = {
    (status & 16) != 0
  }

  def isQueue(): Boolean = {
    (status & 32) != 0
  }

  def isHWFault(): Boolean = {
    (status & 128) != 0
  }

  override def toString = {
    val str = new StringBuffer()
    str.append("TrafficResponse(pirAddres=%d, status=%d, ltvCount=%s)\n".format(pirAddress, status, lifetimeVehicleCount))
    for (vehicle ← vehicleList) {
      str.append("\t")
      str.append(vehicle.toString)
      str.append("\n")
    }
    str.toString
  }
}

case class VehicleData(speed: Int,
                       vehiclePos: Int, //0 middle, 1 left, 2 right
                       vehicleClass: Int, //1 vehicle < 5,6 3 truck, 4 trucks with trailer (or long trucks)
                       occupancy: Int, //msec
                       timeGap: Int, //msec
                       length: Double) {
  //m
  require(vehiclePos < 4, "vehiclePos is too large %d > 3".format(vehiclePos))
  require(vehicleClass < 0x3F, "vehicleClass is too large %d > 3".format(vehicleClass))

  override def toString = {
    val vehiclePosStr = vehiclePos match {
      case 0 ⇒ "0:middle"
      case 1 ⇒ "1:left"
      case 2 ⇒ "2:rigth"
    }
    "VehicleData(speed=%d, length=%.2f, vehiclePos=%s, vehicleClass=%d,occupancy=%d, timeGap=%d)".format(
      speed, length, vehiclePosStr, vehicleClass, occupancy, timeGap)
  }
}

object Checksum {
  val CHECKSUM_MOD = 256

  def calculateChecksum(controlByte: Short, addressByte: Short, applicationBytes: Array[Byte]): Int = {
    var total: Int = controlByte
    total += addressByte
    for (tmpByte ← applicationBytes) {
      total += (tmpByte.toInt & 0xFF) //make every byte positive
    }
    total % CHECKSUM_MOD
  }

}