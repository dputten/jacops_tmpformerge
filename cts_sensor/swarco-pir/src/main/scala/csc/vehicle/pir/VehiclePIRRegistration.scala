/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.pir

import java.text.SimpleDateFormat
import java.util.{ TimeZone, Date }
import csc.vehicle.message.Lane

/**
 * Message send when the PIR detects a registration
 * @param lane
 * @param eventTime
 * @param speed
 * @param length
 */
case class VehiclePIRRegistration(lane: Lane, eventTime: Long, speed: Option[Float], length: Option[Float], category: Option[Int], confidence: Option[Int]) {
  private val dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS Z")
  dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"))

  /**
   * toString overloaded because when logging the eventTime needs also be logged in a readable format
   * @return The complete class as a string
   */
  override def toString(): String = {
    val buffer = new StringBuffer()
    buffer.append("VehiclePIRRegistration( ")
    buffer.append(lane)
    buffer.append(" eventTime=").append(eventTime)
    buffer.append("[").append(dateFormat.format(new Date(eventTime))).append("]")
    buffer.append(" speed=").append(speed)
    buffer.append(" length=").append(length)
    buffer.append(" category=").append(category)
    buffer.append(" confidence=").append(confidence)
    buffer.append(")")
    buffer.toString
  }

}
