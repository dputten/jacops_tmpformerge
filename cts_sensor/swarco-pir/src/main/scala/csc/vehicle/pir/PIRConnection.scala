/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.pir

import org.jboss.netty.util.HashedWheelTimer
import org.jboss.netty.channel.socket.nio.NioClientSocketChannelFactory
import org.jboss.netty.bootstrap.ClientBootstrap
import java.net.InetSocketAddress
import org.jboss.netty.channel.{ ChannelPipelineFactory, ChannelPipeline, Channels }
import akka.actor.ActorRef
import csc.vehicle.pir.impl._
import csc.akka.logging.DirectLogging
import java.util.concurrent.Executors

/**
 * A class to bootstrap a connection to the Pir component.
 */
class PIRConnection(pirHost: String, pirPort: Int,
                    recipients: Set[ActorRef]) extends DirectLogging {
  private val bootstrap: ClientBootstrap = new ClientBootstrap(
    new NioClientSocketChannelFactory(
      Executors.newCachedThreadPool(),
      Executors.newCachedThreadPool()));
  private val timer = new HashedWheelTimer()
  private val msgProcessor: PIRConnectionProcessor = new PIRConnectionProcessor(
    connectionId = "%s:%d".format(pirHost, pirPort),
    bootstrap = bootstrap,
    timer = timer,
    timeout = 300L,
    recipients = recipients)
  init()

  /**
   * Initialize the connection by setting all the handlers.
   * The ReadTimeoutHandler is implementing requirement SSS.DYN.6
   */
  private def init() {
    // Configure the connection.
    val remoteHost = new InetSocketAddress(pirHost, pirPort)
    val channelHandler = new PIRChannelHandler(msgProcessor, remoteHost)
    val decoder = new PIRDecoder()
    val encoder = new PIREncoder

    bootstrap.setPipelineFactory(new ChannelPipelineFactory {
      def getPipeline(): ChannelPipeline = {
        val pipeline = Channels.pipeline();
        pipeline.addLast("decoder", decoder);
        pipeline.addLast("encoder", encoder);
        pipeline.addLast("handler", channelHandler);
        pipeline;
      }
    })
    bootstrap.setOption("remoteAddress", remoteHost);
  }

  /**
   * Start the connection by trying to connect for the first time. After tis initial start the handlers will manage the connection
   * with the Pir
   */
  def start() {
    bootstrap.connect
  }

  /**
   * Stop the connection and release all the resources , like sockets and threads.
   *
   */
  def shutdown() {
    try {
      msgProcessor.shutdown
    } catch {
      case ex: Exception ⇒ log.error("Exception while shutting down Processor: %s", ex.getMessage)
    }
    try {
      timer.stop
    } catch {
      case ex: Exception ⇒ log.error("Exception while shutting down timer: %s", ex.getMessage)
    }
    try {
      // Shut down thread pools to exit.
      bootstrap.releaseExternalResources()
    } catch {
      case ex: Exception ⇒ log.error("Exception while shutting down bootsrtap: %s", ex.getMessage)
    }
  }

  def getState(): ConnectionState.Value = {
    msgProcessor.getState()
  }

  def sendPIRMessage(msg: AnyRef) {
    msgProcessor.sendPIRMessage(msg)
  }

}
