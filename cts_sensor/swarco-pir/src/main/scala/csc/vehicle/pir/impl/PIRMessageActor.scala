/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.pir.impl

import akka.actor.{ ActorRef, ActorLogging, Actor }
import csc.vehicle.message.Lane

import scala.concurrent.duration._

object WakeUp
object SyncTimeOut

class PIRMessageActor(dynamicConfidence: Boolean, refreshTime: FiniteDuration, timeout: FiniteDuration, pirAddress: Short, lane: Lane, pirChannel: ActorRef, recipients: Seq[ActorRef]) extends Actor with ActorLogging {

  implicit val executor = context.system.dispatcher

  private val messageProcessor = new PIRMessageProcessor(lane, dynamicConfidence)
  private var fcb = true
  private var vehicleCount = 0

  override def preStart() {
    SendSyncMessage()
    context.system.scheduler.schedule(refreshTime, refreshTime, self, WakeUp)
  }

  override def preRestart(reason: scala.Throwable, message: scala.Option[scala.Any]) {
    log.warning("restarting: %s".format(reason.toString))
    super.preRestart(reason, message)
  }

  def receive = {
    case ConnectedMessage(time) ⇒ {
      //send sync message
      SendSyncMessage()
      log.debug(lane.laneId + " : Connected Send Sync ")
    }
    case ReceivedMessage(now, msg) ⇒ {
      log.debug(lane.laneId + " : Received message: " + msg)
      msg match {
        case ok: OKResponse ⇒ {
          fcb = true
          messageProcessor.noDataReceived(now)
          context.become(polling)
        }
        case other ⇒ {
          log.warning(lane.laneId + " received unecpected message " + msg.toString())
        }
      }
    }
    case SyncTimeOut ⇒ {
      //todo: what to do when timeout on sync
      log.debug(lane.laneId + " : Sync Timeout retry")
      //retry
      SendSyncMessage()
    }
  }

  def polling: Receive = {
    case ConnectedMessage(time) ⇒ {
      log.debug(lane.laneId + " : Connected Send Sync ")
      SendSyncMessage()
      context.unbecome()
    }
    case WakeUp ⇒ {
      log.debug(lane.laneId + " : Requesting TrafficInfo: ")
      pirChannel ! new GetTrafficData(pirAddress, fcb)
      fcb = !fcb
    }
    case ReceivedMessage(now, msg) ⇒ {
      log.debug(lane.laneId + " : Received message: " + msg)
      msg match {
        case ok: OKResponse ⇒ {
          //No changes
          messageProcessor.noDataReceived(now)
        }
        case response: TrafficResponse ⇒ {
          //check status
          if (response.hasError()) {
            //there is a problem
            processErrors(response)
          }
          if (response.lifetimeVehicleCount.getOrElse(vehicleCount + 1) > vehicleCount) {
            //check vehicles
            val vehicles = messageProcessor.processTrafficResponse(now, response)
            for (registration ← vehicles) {
              for (rec ← recipients) {
                rec ! registration
              }
            }
          } else {
            log.debug(lane.laneId + " Old message: VehicleCount isn't raised was %d is %d".format(vehicleCount, response.lifetimeVehicleCount.getOrElse(vehicleCount + 1)))
          }
        }
      }
    }
    case SyncTimeOut ⇒ {
      //delayed timeout
    }
  }
  def processErrors(response: TrafficResponse) {
    //todo generate events not implemented
    if (response.isHWFault()) {
      log.warning("%d: has a hardware fault".format(response.pirAddress))
    }
    if (response.isQueue()) {
      log.warning("%d: has a queue failure".format(response.pirAddress))
    }
    if (response.isWrongWay()) {
      log.warning("%d: has a wrongway driver failure".format(response.pirAddress))
    }
    if (response.isThermoNotification()) {
      log.warning("%d: has a thermo notification".format(response.pirAddress))
    }
    if (response.isIRNotification()) {
      log.warning("%d: has a IR notification".format(response.pirAddress))
    }
  }

  def SendSyncMessage() {
    pirChannel ! new ResetComm(pirAddress)
    context.system.scheduler.scheduleOnce(timeout, self, SyncTimeOut)
  }

}