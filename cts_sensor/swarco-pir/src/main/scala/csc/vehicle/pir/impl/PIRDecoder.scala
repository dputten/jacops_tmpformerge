/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.pir.impl

import org.jboss.netty.buffer.ChannelBuffer
import org.jboss.netty.channel.{ Channel, ChannelHandlerContext }
import org.jboss.netty.handler.codec.frame.{ CorruptedFrameException, FrameDecoder }
import collection.mutable.ListBuffer
import csc.akka.logging.DirectLogging

/**
 * Sizes of different fields
 * The Byte values are implemented as Shorts because these are unsigned Bytes
 */
object FieldConstants {
  val LONGFRAME_SIZE_HEADER = 4
  val SIZE_TRAILER = 2
  val SHORTFRAME_SIZE = 5

  val LONGFRAME = 0x68.toShort
  val SHORTFRAME = 0x10.toShort
  val SINGLECHAR = 0xE5.toShort

  val END_BYTE = 0x16.toShort
  val MASK_CLASSINFO = 0x3F

  val LENGTH_VEHICLE_BASE = 6
  val LENGTH_VEHICLE_LENGTH = 7
  val LENGTH_VEHICLE_MAX = 11
  val CTRL_REQUEST_TRAFFIC = 0x58.toShort //FCB is 0
  val CTRL_REQUEST_TRAFFIC2 = 0x78.toShort //FCB is 1
  val CTRL_RESPONSE_TRAFFIC = 0x08.toShort

  val CTRL_REQUEST_RESET = 0x40.toShort

}

/**
 * This class is responsible to decode the received stream into the PirMessages.
 */
class PIRDecoder() extends FrameDecoder with DirectLogging {
  /**
   * This method is called by the netty framework to decode the messages.
   * @param ctx the channelContext
   * @param channel the channel object
   * @param buffer the buffer containing the encoded message
   * @return null when not all the bytes are received otherwise the decoded Message
   * @throws CorruptedFrameException when the sizes doesn't match
   */
  def decode(ctx: ChannelHandlerContext, channel: Channel, buffer: ChannelBuffer): Object = {
    if (channel == null) {
      //This happens only in unit tests
      log.debug("Decode message")
    } else {
      log.debug("Decode message from %s".format(channel.getLocalAddress))
    }
    // Wait until the message type and length is available.
    var nrBytes = buffer.readableBytes()
    if (nrBytes < 1) {
      log.debug("Not enough bytes %d needed %d".format(nrBytes, 1))
      return null
    }
    if (log.isDebugEnabled) {
      buffer.markReaderIndex()
      val bytes = buffer.array()
      val strBuf = new StringBuffer()

      for (pos ← 0 until nrBytes) {
        val str = bytes(pos).toInt.toHexString
        val hexByte = str.length() match {
          case 1 ⇒ "0" + str
          case 2 ⇒ str
          case l ⇒ str.substring(l - 2)
        }
        strBuf.append(hexByte)
        strBuf.append(" ")
      }
      log.debug("Received bytes= %s".format(strBuf.toString))
      buffer.resetReaderIndex()
    }
    //mark start in buffer
    buffer.markReaderIndex()

    // Check the message Type.
    val msgType = buffer.readUnsignedByte()
    val frameSize = msgType match {
      case FieldConstants.LONGFRAME ⇒ {
        if (nrBytes >= FieldConstants.LONGFRAME_SIZE_HEADER) {
          //read rest of header
          val applLength = buffer.readUnsignedByte
          val applLength2 = buffer.readUnsignedByte //length is repeated in second byte
          if (applLength != applLength2) {
            //header isn't correct
            //reset the complete connection
            throw new CorruptedFrameException("Both length bytes are not equal")
          }
          val headerEnd = buffer.readUnsignedByte()
          if (headerEnd != FieldConstants.LONGFRAME) {
            //header isn't correct
            //reset the complete connection
            throw new CorruptedFrameException("Long frame header isn't correctly closed")
          }
          FieldConstants.LONGFRAME_SIZE_HEADER + applLength + FieldConstants.SIZE_TRAILER
        } else {
          //actual frame size is larger but the next test will result in waiting for more data
          FieldConstants.LONGFRAME_SIZE_HEADER
        }
      }
      case FieldConstants.SHORTFRAME ⇒ FieldConstants.SHORTFRAME_SIZE
      case FieldConstants.SINGLECHAR ⇒ 1
      case _                         ⇒ throw new CorruptedFrameException("Unknown message Type %d".format(msgType))
    }
    if (nrBytes < frameSize) {
      buffer.resetReaderIndex()
      log.debug("Not enough bytes %d needed %d".format(nrBytes, frameSize))
      return null
    }
    //complete frame is available => create message
    msgType match {
      case FieldConstants.LONGFRAME  ⇒ readLongFrame(buffer, frameSize)
      case FieldConstants.SHORTFRAME ⇒ readShortFrame(buffer)
      case FieldConstants.SINGLECHAR ⇒ new OKResponse()
    }
  }

  /**
   * Decode a LongFrame
   * @param buffer the buffer with the encoded message
   * @return the  KSIdentificationMessage
   */
  def readLongFrame(buffer: ChannelBuffer, frameSize: Int): Object = {
    log.debug("Decode LongFrame")
    //traffic data
    //read Application data
    val controlByte = buffer.readUnsignedByte()
    val addressByte = buffer.readUnsignedByte()
    //read message dependent Application data
    val appSize = (frameSize - FieldConstants.LONGFRAME_SIZE_HEADER - FieldConstants.SIZE_TRAILER - 2)
    if (appSize < 0) {
      throw new CorruptedFrameException("ApplicationData has invalid frame size %d: framesize=%d".format(appSize, frameSize))
    }

    val applicationData = buffer.readBytes(appSize)
    val checksum = buffer.readUnsignedByte()
    val endByte = buffer.readUnsignedByte()
    //check data
    if (endByte != FieldConstants.END_BYTE) {
      throw new CorruptedFrameException("Long frame isn't correctly closed")
    }
    val applBytes = new Array[Byte](applicationData.readableBytes())
    applicationData.readBytes(applBytes)
    applicationData.resetReaderIndex()
    checkChecksum(controlByte, addressByte, applBytes, checksum)
    //message is in correct format
    //parse applicationData
    if (controlByte == FieldConstants.CTRL_RESPONSE_TRAFFIC) {
      val status = applicationData.readUnsignedByte()
      val nrApplBytes = applicationData.readableBytes()
      var vehicleList = new ListBuffer[VehicleData]()
      var vehicleCount: Option[Int] = None
      if (nrApplBytes > 0) {
        vehicleCount = Some(applicationData.readInt())
        //rest of the data is 1 to 4 blocks of Vehicle data.
        //The Data can have 3 possible sizes 6, 7 or 11
        //so we have to find the size and number of vehicles
        val (size: Int, nrVehicles: Int) = getSizeAndNumberVehicles(nrApplBytes - 4)
        for (i ← 0 until nrVehicles) {
          val speed = applicationData.readUnsignedByte()
          val classInfo = applicationData.readUnsignedByte()
          val occupancy = applicationData.readShort()
          val timeGap = applicationData.readShort()
          val length = if (size >= FieldConstants.LENGTH_VEHICLE_LENGTH) {
            applicationData.readUnsignedByte()
          } else {
            0
          }
          val timeStamp = if (size >= FieldConstants.LENGTH_VEHICLE_MAX) {
            var reserved = applicationData.readUnsignedByte()
            val time = applicationData.readShort()
            reserved = applicationData.readUnsignedByte()
            time
          } else {
            0
          }
          val vehicleClass = classInfo & FieldConstants.MASK_CLASSINFO
          val laneInfo = classInfo >> 6
          //all data is read create vehicleData
          vehicleList += new VehicleData(speed = speed,
            vehiclePos = laneInfo,
            vehicleClass = vehicleClass,
            occupancy = occupancy * 10,
            timeGap = timeGap * 10,
            length = length.toDouble / 10)
        }
      }
      new TrafficResponse(addressByte, status, vehicleCount, vehicleList.toList)
    } else {
      log.warning("Received unexpected LongFrame message with control byte %d".format(controlByte))
      //Unsupported message
      //bytes are already read so the protocol is kept in sync
      new UnsupportedResponse(controlByte, addressByte)
    }
  }

  /**
   * Decode a ShortFrame
   * @param buffer the buffer with the encoded message
   * @return the  KSIdentificationMessage
   */
  def readShortFrame(buffer: ChannelBuffer): Object = {
    log.debug("Decode ShortFrame")
    val controlByte = buffer.readUnsignedByte()
    val addressByte = buffer.readUnsignedByte()
    val checksum = buffer.readUnsignedByte()
    val endByte = buffer.readUnsignedByte()
    //check data
    if (endByte != FieldConstants.END_BYTE) {
      throw new CorruptedFrameException("Short frame isn't correctly closed")
    }
    checkChecksum(controlByte, addressByte, new Array[Byte](0), checksum)
    //message is in correct format

    controlByte match {
      case FieldConstants.CTRL_REQUEST_RESET ⇒ {
        //Reset Communication
        new ResetComm(addressByte)
      }
      case FieldConstants.CTRL_REQUEST_TRAFFIC ⇒ {
        //GetTrafficData
        new GetTrafficData(addressByte, false)
      }
      case FieldConstants.CTRL_REQUEST_TRAFFIC2 ⇒ {
        //GetTrafficData
        new GetTrafficData(addressByte, true)
      }
    }
  }

  /**
   * the data is 1 to 4 blocks of Vehicle data.
   * The Data can have 3 possible sizes 6, 7 or 11
   *
   * @param nrBytes total number of bytes of vehicle data blocks
   * @return (size of blocks, number of blocks)
   */
  def getSizeAndNumberVehicles(nrBytes: Int): Tuple2[Int, Int] = {
    if (nrBytes % FieldConstants.LENGTH_VEHICLE_BASE == 0) {
      (FieldConstants.LENGTH_VEHICLE_BASE, (nrBytes / FieldConstants.LENGTH_VEHICLE_BASE).toInt)
    } else if (nrBytes % FieldConstants.LENGTH_VEHICLE_LENGTH == 0) {
      (FieldConstants.LENGTH_VEHICLE_LENGTH, (nrBytes / FieldConstants.LENGTH_VEHICLE_LENGTH).toInt)
    } else if (nrBytes % FieldConstants.LENGTH_VEHICLE_MAX == 0) {
      (FieldConstants.LENGTH_VEHICLE_MAX, (nrBytes / FieldConstants.LENGTH_VEHICLE_MAX).toInt)
    } else {
      throw new CorruptedFrameException("Unexpected Length of blocks vehicleData total size = %d".format(nrBytes))
    }
  }

  /**
   * Check the checksum of the message
   * @param controlByte the control byte
   * @param addressByte the address byte
   * @param applicationBytes bytes depending on the request
   * @param checksum the received checksum
   * @throws CorruptedFrameException when checksum isn't correct
   */
  def checkChecksum(controlByte: Short, addressByte: Short, applicationBytes: Array[Byte], checksum: Int) {
    val calcChecksum = Checksum.calculateChecksum(controlByte, addressByte, applicationBytes)
    if (checksum != calcChecksum) {
      throw new CorruptedFrameException("Long frame checksum isn't correct expected %d found %d".format(checksum, calcChecksum))
    }
  }
}
