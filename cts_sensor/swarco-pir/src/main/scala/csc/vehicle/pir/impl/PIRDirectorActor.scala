/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.pir.impl

import java.util.concurrent.TimeUnit

import akka.actor.{ Props, ActorLogging, ActorRef, Actor }
import csc.vehicle.pir.PIRConnection
import collection.mutable.ListBuffer
import csc.gantry.config.PIRConfig
import csc.vehicle.message.Lane

import scala.concurrent.duration._

case class PendingRequest(recvTime: Long, msg: AnyRef, sender: ActorRef)

case class PIRLaneConfiguration(lane: Lane, pir: PIRConfig, recipients: Seq[ActorRef])

case class RequestTimeOut(request: PendingRequest)

class PIRDirectorActor(timeout: FiniteDuration, config: Seq[PIRLaneConfiguration], dynamicConfidence: Boolean) extends Actor with ActorLogging {

  implicit val executor = context.system.dispatcher

  val firstLane = config.head.pir
  val connection = new PIRConnection(firstLane.pirHost, firstLane.pirPort, Set(self))

  val pendingMessages = new ListBuffer[PendingRequest]()
  var currentRequest: Option[PendingRequest] = None

  var actors = Map[Short, ActorRef]()

  override def preStart() {
    super.preStart()
    connection.start()
    //create PIRMessageActors
    actors = config.map(pirLane ⇒ {
      val actor = context.actorOf(Props(new PIRMessageActor(dynamicConfidence = dynamicConfidence,
        refreshTime = FiniteDuration(pirLane.pir.refreshPeriod, TimeUnit.MILLISECONDS),
        timeout = timeout,
        pirAddress = pirLane.pir.pirAddress, lane = pirLane.lane, pirChannel = self, recipients = pirLane.recipients)),
        pirLane.lane.laneId)
      pirLane.pir.pirAddress -> actor
    }).toMap
  }
  override def postStop() {
    connection.shutdown()
    super.postStop()
  }

  override def preRestart(reason: scala.Throwable, message: scala.Option[scala.Any]) {
    log.warning("restarting: %s".format(reason.toString))
    super.preRestart(reason, message)
  }

  def receive = {
    case conn @ ConnectedMessage(time) ⇒ {
      actors.values.foreach(_ ! conn)
      context.become(connected)
      //todo: delete messages when connection is lost??
      //start with an empty queue
      pendingMessages.clear()
    }
    case DisConnectedMessage(time) ⇒ {
      log.warning("Received Disconnect message while disconnected")
    }
    case RequestTimeOut(pending) ⇒ {
      //old timer received
      log.debug("Old Timeout received for message %s in state disconnected".format(pending.msg.toString))
    }
    case ReceivedMessage(time, msg) ⇒ {
      log.warning("Unexpected message %s".format(msg.toString))
    }
    case msg: AnyRef ⇒ {
      log.debug("drop message because channel is disconnected")
    }
  }

  def waitForReply: Receive = {
    case ConnectedMessage(time) ⇒ {
      log.warning("Received Connect message while waitForReply")
    }
    case dis @ DisConnectedMessage(time) ⇒ {
      log.warning("Received Disconnect message")
      actors.values.foreach(_ ! dis)
      context.unbecome()
    }
    case recv: ReceivedMessage ⇒ {
      //handle message
      //make sure that the TrafficResponse always is returned to the correct actor
      log.debug("Received message %s for %s".format(recv.toString, currentRequest.map(_.sender)))
      recv.msg match {
        case traffic: TrafficResponse ⇒ {
          actors.get(traffic.pirAddress) match {
            case Some(ref) ⇒ ref ! recv
            case None      ⇒ currentRequest.foreach(_.sender ! recv)
          }
        }
        case other ⇒ currentRequest.foreach(_.sender ! recv)
      }
      processNext()
    }
    case RequestTimeOut(pending) ⇒ {
      if (currentRequest.isEmpty) {
        log.debug("Old Timeout received for message %s".format(pending.msg.toString))
      }
      currentRequest.foreach(cur ⇒ if (cur == pending) {
        //timeout
        log.warning("Timeout for message %s".format(pending.msg.toString))
        //process next
        processNext()
      } else {
        log.debug("Old Timeout received for message %s current %s".format(pending.msg.toString, cur.msg.toString))
      })
    }
    case msg: AnyRef ⇒ {
      log.debug("Set pending Message %s".format(msg.toString))
      pendingMessages += new PendingRequest(System.currentTimeMillis(), msg, sender)
      log.debug("Pending Message size %d".format(pendingMessages.size))
    }
  }

  def connected: Receive = {
    case ConnectedMessage(time) ⇒ {
      log.warning("Received Connect message while connected")
    }
    case DisConnectedMessage(time) ⇒ {
      log.warning("Received Disconnect message")
      context.unbecome()
    }
    case ReceivedMessage(time, msg) ⇒ {
      log.warning("Unexpected message %s".format(msg.toString))
    }
    case RequestTimeOut(pending) ⇒ {
      log.debug("Old Timeout received for message %s in state connected".format(pending.msg.toString))
    }
    case msg: AnyRef ⇒ {
      currentRequest = Some(new PendingRequest(System.currentTimeMillis(), msg, sender))
      sendPIRMessage(msg)
      context.become(waitForReply)
    }
  }

  private def sendPIRMessage(request: AnyRef) {
    log.debug("Send message %s".format(request.toString))
    try {
      connection.sendPIRMessage(request)
      context.system.scheduler.scheduleOnce(timeout, self, new RequestTimeOut(currentRequest.get))
    } catch {
      case ex: IllegalStateException ⇒ {
        log.warning("Received illigal state " + ex.getMessage)
        context.unbecome()
      }
    }
  }
  private def processNext() {
    if (pendingMessages.isEmpty) {
      currentRequest = None
      context.become(connected)
    } else {
      val request = pendingMessages(0)
      currentRequest = Some(request)
      pendingMessages.remove(0)
      sendPIRMessage(request.msg)
    }
  }

}