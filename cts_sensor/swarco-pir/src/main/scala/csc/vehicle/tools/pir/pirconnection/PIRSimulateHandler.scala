/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.tools.pir.pirconnection

import org.jboss.netty.channel._
import java.net.ConnectException
import csc.akka.logging.DirectLogging
import akka.actor.ActorRef
import akka.pattern.ask
import scala.concurrent.duration._
import scala.concurrent.Await

/**
 * The Netty connection handler of the PIR simulator
 * @param processorActor The processor Actor which is keeping track which replies have to be send back.
 */
class PIRSimulateHandler(processorActor: ActorRef) extends SimpleChannelUpstreamHandler with DirectLogging {
  private var startTime = -1L

  /**
   * Get the configured remote address.
   */
  def getRemoteAddress(channel: Channel): String = {
    if (channel != null && channel.getRemoteAddress != null) {
      channel.getRemoteAddress.toString
    } else {
      "Not connected yet"
    }
  }

  /**
   * A disconnect event is received.
   * @param ctx: channel context
   * @param e: the channel event
   */
  override def channelDisconnected(ctx: ChannelHandlerContext, e: ChannelStateEvent) {
    log.info("Disconnected from: " + getRemoteAddress(e.getChannel))
  }

  /**
   * A connection close event is received
   * @param ctx: channel context
   * @param e: the channel event
   */
  override def channelClosed(ctx: ChannelHandlerContext, e: ChannelStateEvent) {
    log.info("Connection closed from: " + getRemoteAddress(e.getChannel))
    log.info("Uptime was %d s".format(System.currentTimeMillis() - startTime))
    startTime = -1L
  }

  /**
   * A connected event is received
   * @param ctx: channel context
   * @param e: the channel event
   */
  override def channelConnected(ctx: ChannelHandlerContext, e: ChannelStateEvent) {
    if (startTime < 0) {
      startTime = System.currentTimeMillis()
    }
    log.info("Connected to: " + getRemoteAddress(e.getChannel))
  }

  /**
   * A Exception event is received
   * @param ctx: channel context
   * @param e: the exception event
   */
  override def exceptionCaught(ctx: ChannelHandlerContext, e: ExceptionEvent) {
    val cause = e.getCause()
    cause match {
      case con: ConnectException ⇒ {
        log.error(con, "Failed to connect: " + con.getMessage())
      }
      case ex: Exception ⇒ {
        log.error(ex, "Channel exception: " + ex.getMessage())
      }
    }
    startTime = -1
    ctx.getChannel().close()
  }

  /**
   * A Message is received PIR interface
   */
  override def messageReceived(ctx: ChannelHandlerContext, e: MessageEvent) {
    log.info("Message received from: " + getRemoteAddress(e.getChannel))
    val receivedMsg = e.getMessage
    if (receivedMsg != null) {
      log.info("Message received: " + receivedMsg.toString)
    }
    try {
      val future = processorActor.ask(receivedMsg)(5 seconds)
      e.getChannel.write(Await.result(future, 5 seconds))
    } catch {
      case ex: Exception ⇒ log.warning("couldn't get response")
    }
  }
}