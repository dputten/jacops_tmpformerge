/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.tools.pir

import java.util.Date
import collection.mutable.ListBuffer
import akka.actor.{ Props, ActorSystem, ActorLogging, Actor }
import csc.vehicle.pir.impl._
import pirconnection.PIRSimulateConnection

/**
 * Message to indicate there was a registration for the PIR simulator
 * @param speed
 * @param length
 * @param lanePos
 * @param vehicleClass
 */
case class PIRRegistration(speed: Int, length: Double, lanePos: Short, vehicleClass: Short) {
  require(lanePos >= 0, "lanePos is negative")
  require(lanePos < 4, "lanePos is too large")
}

/**
 * Message to set the status of the PIR simulator
 * @param status
 */
case class PIRStatus(status: Short) {
  require(status <= 255, "status is too large")
  require(status >= 0, "status is negative")
}

/**
 * Simulate a pir registration
 */
class SendPIRRegistration(host: String, port: Int, pirAddress: Short) {
  private val system = ActorSystem("simulate")
  private val actor = system.actorOf(Props(new SimulatePIRActor(pirAddress = pirAddress)))
  private val pirConnection = new PIRSimulateConnection(host, port, actor)
  pirConnection.start()

  def stopPIR() {
    try {
      pirConnection.shutdown()
    } finally {
      system.shutdown()
    }
  }

  /**
   * Send the registration to the radar listener
   * @param speed The simulated speed of the vehicle
   * @param length The simulated reflection of the vehicle
   * @param lanePos the position of vehicle 0 middle, 1 left and 3 right
   */
  def sendRegistration(speed: Int, length: Double, lanePos: Short, vehicleClass: Short) {
    actor ! new PIRRegistration(speed = speed, length = length, lanePos = lanePos, vehicleClass = vehicleClass)
  }

  /**
   * Send a status update
   * @param status
   */
  def UpdateStatus(status: Short) {
    actor ! new PIRStatus(status)
  }

}

class SimulatePIRActor(pirAddress: Short) extends Actor with ActorLogging {
  private var lastVehicle = new Date().getTime
  private var changes = false
  private var status: Short = 0
  private val vehicleBuffer = new ListBuffer[Tuple2[Date, PIRRegistration]]
  private var lifetimeVehicleCount = 0

  def receive = {
    case reset: ResetComm ⇒ {
      lastVehicle = new Date().getTime
      context.sender ! new OKResponse
    }
    case getTraffic: GetTrafficData ⇒ {
      if (changes) {
        //create message
        val msgList = new ListBuffer[VehicleData]
        for ((time, pir) ← vehicleBuffer) {
          msgList += new VehicleData(speed = pir.speed,
            vehiclePos = pir.lanePos, //0 middle, 1 left, 2 right
            vehicleClass = pir.vehicleClass, //1 vehicle < 5,6 3 truck, 4 trucks with trailer (or long trucks)
            occupancy = 180, //msec just a value i have seen in PIR communication
            timeGap = (time.getTime - lastVehicle).toInt, //msec
            length = pir.length)
          lastVehicle = time.getTime
        }
        val response = new TrafficResponse(pirAddress, status = status, lifetimeVehicleCount = Some(lifetimeVehicleCount), vehicleList = msgList.toList)
        vehicleBuffer.clear()
        changes = false
        context.sender ! response
      } else {
        context.sender ! new OKResponse
      }
    }
    case pir: PIRRegistration ⇒ {
      changes = true
      lifetimeVehicleCount += 1
      if (vehicleBuffer.size < 4) {
        val entry = (new Date(), pir)
        vehicleBuffer += entry
      } else {
        log.warning("Received 4 Vehicle passages without any requests. Ignoring this vehicle")
      }
    }
    case stat: PIRStatus ⇒ {
      changes = true
      status = stat.status
    }
    case any: AnyRef ⇒ {
      log.warning("Unexpected request. Ignore message")
    }
  }
}
