/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.tools.pir.pirconnection

import java.net.InetSocketAddress
import java.util.concurrent.Executors
import akka.actor.ActorRef
import csc.akka.logging.DirectLogging
import csc.vehicle.pir.impl.{ PIREncoder, PIRDecoder }
import org.jboss.netty.channel.{ Channel, ChannelPipelineFactory, ChannelPipeline, Channels }
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory
import org.jboss.netty.bootstrap.ServerBootstrap

/**
 * A class to bootstrap a simulate connection.
 * This class isn't placed in the test part of this project, because other test projects are depended on this
 * class and at this point we don't know how to make this dependency work in the project build files
 */
class PIRSimulateConnection(host: String, port: Int, processorActor: ActorRef) extends DirectLogging {
  private val bootstrap = new ServerBootstrap(
    new NioServerSocketChannelFactory(
      Executors.newCachedThreadPool(),
      Executors.newCachedThreadPool()))
  private var channel: Option[Channel] = None
  init()

  /**
   * Initialize the connection by setting all the handlers.
   * The ReadTimeoutHandler is implementing requirement SSS.DYN.6
   */
  private def init() {
    // Configure the connection.

    val channelHandler = new PIRSimulateHandler(processorActor)
    val decoder = new PIRDecoder
    val encoder = new PIREncoder

    bootstrap.setPipelineFactory(new ChannelPipelineFactory {
      def getPipeline(): ChannelPipeline = {
        val pipeline = Channels.pipeline()
        pipeline.addLast("decoder", decoder)
        pipeline.addLast("encoder", encoder)
        pipeline.addLast("handler", channelHandler)
        pipeline
      }
    })
  }

  /**
   * Start the connection by trying to connect for the first time.
   */
  def start() {
    channel match {
      case Some(ch) ⇒ throw new IllegalStateException("BaseSimulatorConnection already started")
      case None ⇒ {
        channel = Some(bootstrap.bind(new InetSocketAddress(host, port)))
      }
    }
  }

  /**
   * Stop the connection and release all the resources , like sockets and threads.
   *
   */
  def shutdown() {
    try {
      channel.map { ch ⇒
        {
          ch.close
          channel = None
        }
      }
    } catch {
      case ex: Exception ⇒ log.error("Exception while closing channel: %s", ex.getMessage)
    }
    try {
      // Shut down thread pools to exit.
      bootstrap.releaseExternalResources()
    } catch {
      case ex: Exception ⇒ log.error("Exception while shutting down bootstrap: %s", ex.getMessage)
    }
  }

}