/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.systemevents

import java.util.concurrent.{ CyclicBarrier, TimeUnit }

import akka.actor.{ ActorSystem, Props }
import akka.testkit.TestKit
import org.scalatest._

import scala.collection.mutable.ListBuffer

class SystemEventListenerTest
  extends TestKit(ActorSystem("SystemEventListenerTest"))
  with MustMatchers
  with WordSpecLike
  with BeforeAndAfterAll {

  override def afterAll() {
    system.shutdown
  }

  "SystemEventListener" must {
    "register when Systemevent is triggered" in {
      val barrier = new CyclicBarrier(2)
      val store = new SystemEventStoreMock(barrier)
      val listener = system.actorOf(Props(new SystemEventListener(store)))
      Thread.sleep(1000) //needed sleep for correct running in SBT
      val event = new SystemEvent(componentId = "TestComponent",
        timestamp = System.currentTimeMillis(),
        systemId = "A2",
        eventType = "TestEvents",
        userId = "system",
        reason = "bla")
      system.eventStream.publish(event)
      barrier.await(10, TimeUnit.SECONDS)
      val storeList = store.list
      storeList must have size (1)
      storeList.head must be(event)
      system.stop(listener)
    }
  }

}

/**
 * Mock test class for the SystemEventStore
 * @param barrier is triggered when an append is called
 */
class SystemEventStoreMock(barrier: CyclicBarrier) extends SystemEventStore {
  val list = new ListBuffer[SystemEvent]()

  def getAllEvents(): Seq[SystemEvent] = {
    Seq()
  }

  def getEventsForSystem(systemId: String): Seq[SystemEvent] = {
    Seq()
  }

  def appendEvent(event: SystemEvent) {
    list += event
    barrier.await(30, TimeUnit.SECONDS)
  }
}