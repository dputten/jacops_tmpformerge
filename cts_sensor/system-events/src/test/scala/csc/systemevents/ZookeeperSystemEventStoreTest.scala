/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.systemevents

import csc.akka.logging.DirectLogging
import csc.curator.CuratorTestServer
import csc.curator.utils.CuratorToolsImpl
import org.scalatest.WordSpec
import org.scalatest._

class ZookeeperSystemEventStoreTest extends WordSpec with MustMatchers with DirectLogging with CuratorTestServer {
  var store: ZookeeperSystemEventStore = _

  override def beforeEach() {
    super.beforeEach()

    val curator = new CuratorToolsImpl(clientScope, log)
    store = new ZookeeperSystemEventStore(curator)
  }

  override def afterEach() {
    store.shutdown()
    super.afterEach()
  }
  "The ZooKeeperSystemEventStore" must {

    "append events" in {
      val event = new SystemEvent(componentId = "TestComponent",
        timestamp = System.currentTimeMillis(),
        systemId = "A2",
        eventType = "TestEvents",
        userId = "system",
        reason = "bla")
      store.appendEvent(event)
      var list = store.getAllEvents()
      list must have size (1)
      list.head must be(event)
      list = store.getEventsForSystem("A22")
      list must have size (0)
      list = store.getEventsForSystem("A2")
      list must have size (1)
      list.head must be(event)
    }
  }

}