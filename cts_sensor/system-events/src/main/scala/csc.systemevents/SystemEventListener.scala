/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.systemevents

import akka.actor.{ ActorLogging, Actor }
import akka.event.LoggingReceive

/**
 * Actor which listen on the eventStream to get all the SystemEvents and store them to the eventStore.
 * @param eventStore The event store
 */
class SystemEventListener(eventStore: SystemEventStore) extends Actor with ActorLogging {

  override def preStart() {
    context.system.eventStream.subscribe(self, classOf[SystemEvent])
  }
  override def postStop() {
    context.system.eventStream.unsubscribe(self)
  }
  def receive = LoggingReceive {
    case event: SystemEvent ⇒ {
      //add new event
      eventStore.appendEvent(event)
    }
  }
}
