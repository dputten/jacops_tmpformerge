/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.systemevents

import csc.akka.logging.DirectLogging
import csc.config.Path
import org.apache.commons.io.FileUtils
import java.io.File
import net.liftweb.json.{ Serialization, DefaultFormats }
import collection.mutable.ListBuffer
import csc.curator.utils.Curator
import akka.actor.{ ActorLogging, ActorRef }
import csc.amqp.PublisherActor
import akka.actor.SupervisorStrategy.Restart
import akka.actor.OneForOneStrategy

/**
 * Interface of getting the SystemEvents.
 */
trait SystemEventStore {
  /**
   * Get all systemEvents of all systems
   * @return List of SystemEvent
   */
  def getAllEvents(): Seq[SystemEvent]
  /**
   * Get all systemEvents of the specified system
   * @param systemId of the system
   * @return List of SystemEvent
   */
  def getEventsForSystem(systemId: String): Seq[SystemEvent]

  /**
   * append event to EventStore
   * @param event the new event
   */
  def appendEvent(event: SystemEvent)

}

/**
 * Empty implementation of the SystemEventStore
 */
class EmptySystemEventStore() extends SystemEventStore {
  def getAllEvents(): Seq[SystemEvent] = {
    Seq()
  }
  def getEventsForSystem(systemId: String): Seq[SystemEvent] = {
    Seq()
  }

  def appendEvent(event: SystemEvent) {
    //done
  }
}

/**
 * Implementation of the SystemEventStore using ZooKeeper
 * @param curator The connection to Zookeeper
 */
class ZookeeperSystemEventStore(curator: Curator) extends SystemEventStore with DirectLogging {
  val keyPrefix = Path("/ctes/systems")
  val keyEvents = "events"

  def shutdown() {
  }

  /**
   * Get all System Events. Only used in tests.
   * @return List of SystemEvent
   */
  def getAllEvents(): Seq[SystemEvent] = {
    val systemList = curator.getChildren(keyPrefix)
    val systemEventList = new ListBuffer[SystemEvent]()
    for (systemPath ← systemList) {
      systemEventList ++= getEventsForSystemPath(systemPath)
    }
    systemEventList.toSeq
  }

  /**
   * Get all System Events for a system. Only used in tests.
   * @param systemId of the system
   * @return List of SystemEvent
   */
  def getEventsForSystem(systemId: String): Seq[SystemEvent] = {
    getEventsForSystemPath(keyPrefix / systemId)
  }
  def getEventsForSystemPath(systemPath: Path): Seq[SystemEvent] = {
    val eventsPath = systemPath / keyEvents
    val systemEventList = new ListBuffer[SystemEvent]()
    if (curator.exists(eventsPath)) {
      val eventList = curator.getChildren(eventsPath)
      for (eventPath ← eventList) {
        systemEventList ++= curator.get[SystemEvent](eventPath)
      }
    }
    systemEventList.toSeq
  }

  def appendEvent(event: SystemEvent) {
    val eventsPath = keyPrefix / event.systemId / keyEvents
    if (!curator.exists(eventsPath)) {
      curator.createEmptyPath(eventsPath)
    }
    curator.appendEvent(eventsPath / "qn-", event)
  }

}

/**
 * Json File implementation of the LaneConfiguration
 * @param inputFile The file containing the lane configuration in JSON format
 */
class FileSystemEventStore(inputFile: String) extends SystemEventStore {
  implicit val formats = DefaultFormats
  private val file = new File(inputFile)
  private val eventList = new ListBuffer[SystemEvent]
  if (file.exists()) {
    val json = FileUtils.readFileToString(file)
    eventList ++= Serialization.read[List[SystemEvent]](json)
  }

  def getAllEvents(): Seq[SystemEvent] = {
    eventList.toSeq
  }
  def getEventsForSystem(systemId: String): Seq[SystemEvent] = {
    eventList.filter(event ⇒ event.systemId == systemId).toSeq
  }

  def appendEvent(event: SystemEvent) {
    eventList += event
  }
}

class AmqpSystemEventStore(queue: ActorRef) extends SystemEventStore with DirectLogging {
  implicit val formats = DefaultFormats

  def getAllEvents(): Seq[SystemEvent] = {
    throw new Exception("not implemented")
  }
  def getEventsForSystem(systemId: String): Seq[SystemEvent] = {
    throw new Exception("not implemented")
  }

  def appendEvent(event: SystemEvent) {
    log.debug("event {} arrived in appendEvent", event)
    queue ! event
  }
}

class SystemEventAmqpActor(val producer: ActorRef, val producerEndpoint: String,
                           producerRouteKey: String, val ApplicationId: String) extends PublisherActor
  with ActorLogging {

  override implicit val formats = DefaultFormats
  def responseName = "SystemEvent"

  override val supervisorStrategy = OneForOneStrategy() {
    case _: Exception ⇒ Restart
  }

  def producerRouteKey(msg: AnyRef): String = {
    msg match {
      case systemEvent: SystemEvent ⇒ {
        systemEvent.systemId + "." + systemEvent.gantryId.map(_ + ".").getOrElse("") + responseName
      }
      case other ⇒ msg.getClass.getName
    }
  }
}

