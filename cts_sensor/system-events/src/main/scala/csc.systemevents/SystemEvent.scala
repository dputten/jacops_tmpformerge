/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.systemevents

/**
 * used by SystemEvent to indicate if a request for an action required one or more requests/events
 * @param token
 * @param last
 */
case class SystemEventToken(token: String, last: Boolean)

/**
 * Used to request the backend for an action
 * @param componentId
 * @param timestamp
 * @param systemId
 * @param eventType
 * @param userId
 * @param reason
 * @param token
 * @param corridorId
 * @param gantryId
 * @param laneId
 */
case class SystemEvent(componentId: String,
                       timestamp: Long,
                       systemId: String,
                       eventType: String,
                       userId: String,
                       reason: String = "",
                       token: Option[SystemEventToken] = None,
                       corridorId: Option[String] = None,
                       gantryId: Option[String] = None,
                       laneId: Option[String] = None)

/**
 * The Configuration of the ZooKeeper implementation of the LaneConfiguration
 * @param zkServers
 * @param sessionTimeout
 * @param connectionTimeout
 */
case class ZookeeperConfig(zkServers: String, sessionTimeout: Int, connectionTimeout: Int)

