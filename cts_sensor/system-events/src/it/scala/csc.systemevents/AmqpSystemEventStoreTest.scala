package csc.systemevents

import akka.actor._
import akka.testkit.{ ImplicitSender, TestKit, TestProbe }
import scala.concurrent.duration._
import com.github.sstone.amqp.Amqp.{ Ack, ChannelParameters, Delivery }
import com.github.sstone.amqp.Amqp
import csc.amqp.{MQConnection, JsonSerializers}
import net.liftweb.json.DefaultFormats
import org.scalatest._

class AmqpSystemEventStoreTest extends TestKit(ActorSystem("systemEventStore")) with JsonSerializers with ImplicitSender with WordSpecLike with MustMatchers with BeforeAndAfterAll {
  override implicit val formats = DefaultFormats

  override protected def afterAll() {
    super.afterAll()
    system.shutdown()
  }

  def initializeQueue(connection: MQConnection, receiver: ActorRef): ActorRef = {
    val producer = connection.producer
    val listener = system.actorOf(Props(new SystemEventAmqpActor(producer, "gantry-out-exchange", "", "Test")))
    connection.createSimpleConsumer("gantry-out", receiver, Some(ChannelParameters(5)), false)
    listener
  }

  "get" must {
    "give back what we put in" in {
      val rmqConnection = MQConnection.create( "SystemEventAmqp-Actor-Test", "amqp://localhost:5672/", Duration.create(5000, "seconds"))
      val consumerProbe = TestProbe()
      val systemEventAmqpActor = initializeQueue(rmqConnection, consumerProbe.ref)
      val event = new SystemEvent(componentId = "TestComponent",
        timestamp = 0,
        systemId = "A4",
        eventType = "TestEvents",
        userId = "system",
        reason = "none",
        corridorId = Some("S1"),
        gantryId = Some("E1"))

      systemEventAmqpActor ! event

      val deliver = consumerProbe.expectMsgType[Delivery](5.seconds)
      consumerProbe.send(consumerProbe.sender, Ack(deliver.envelope.getDeliveryTag))
      val payload = fromJson[SystemEvent](new String(deliver.body))
      payload match {
        case Right(msg) ⇒
          msg.componentId must be("TestComponent")
          msg.timestamp must be(0)
          msg.systemId must be("A4")
          msg.eventType must be("TestEvents")
          msg.userId must be("system")
          msg.reason must be("none")
          msg.corridorId must be(Some("S1"))
          msg.gantryId must be(Some("E1"))
        case Left(error) ⇒ fail("error")
      }
    }
  }
}
