package vehicleRegistration

import csc.sbt.dep._
import sbt._

object SensorVersions extends CommonVersions {

  //override val project: String =

  val sensorCert  = project
}

trait SensorVersionContext extends VersionContext {
  def v = SensorVersions
}

object SensorDeps
  extends SensorVersionContext
    with Dependency
    with CommonsDependency
    with UnclassifiedDependency {

  val sensorCert  = "com.csc.traffic"             % "sensor-certified"              % v.sensorCert
}

// Runtime
object SensorRuntimeDeps extends SensorVersionContext with RuntimeDependency

// Test
object SensorTestDeps extends SensorVersionContext with TestDependency {

  override def testSet: Seq[ModuleID] = super.testSet :+ SensorDeps.testUtil

}

//CDH5
object CDH5 extends SensorVersionContext with CalderaDependency