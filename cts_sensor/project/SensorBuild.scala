package vehicleRegistration

import sbt._
import sbt.Keys._
import csc.sbt._
import com.typesafe.sbt.SbtNativePackager._
import com.typesafe.sbt.packager.Keys._
import com.typesafe.sbt.SbtScalariform
import com.typesafe.sbt.SbtScalariform.ScalariformKeys

object SensorBuild extends Build with CommonSettings {

  lazy val buildSettings = Seq(
    organization := "com.csc.traffic",
    version      := SensorVersions.project,
    scalaVersion := SensorVersions.scala
  )

  // Settings
  override lazy val settings = super.settings ++ buildSettings ++ Seq(
    resolvers += Resolver.defaultLocal
  )

  override def formatSettings: Seq[Setting[_]] = SbtScalariform.scalariformSettings ++ Seq(
    ScalariformKeys.preferences in Compile := formattingPreferences,
    ScalariformKeys.preferences in Test    := formattingPreferences
  )

  lazy val formattingPreferences = {
    import scalariform.formatter.preferences._
    FormattingPreferences()
      .setPreference(RewriteArrowSymbols, true)
      .setPreference(AlignParameters, true)
      .setPreference(AlignSingleLineCaseStatements, true)
  }

  //  lazy val multiJvmSettings = SbtMultiJvm.multiJvmSettings ++ inConfig(MultiJvm)(SbtScalariform.scalariformSettings) ++ Seq(
  //    compileInputs in MultiJvm <<= (compileInputs in MultiJvm) dependsOn (ScalariformKeys.format in MultiJvm),
  //    ScalariformKeys.preferences in MultiJvm := formattingPreferences
  //  )

  import csc.sbt.PackageMappings._

  def commonMappings(targetLogFile: Option[String]): Seq[sbt.Setting[_]] = Seq(
    mappings in Universal <+= akkaAppLauncher,
    mappings in Universal <+= packageJarInDeploy,
    mappings in Universal <++= multiConfig,
    mappings in Universal <++= loggingConfig(targetLogFile),
    mappings in Universal <+= emptyLogs
  )

  def toolMappings(): Seq[sbt.Setting[_]] = Seq(
    mappings in Universal <++= sourceDirectory map { dir =>
      Dist.getFileMappings(dir / "main/scripts", "bin")
    },
    mappings in Universal <++= sourceDirectory map { _ =>
      Dist.getFileMappings(file("config/tools"), "config/tools")
    }
  )

  def classMappings(targetPath: String): Seq[sbt.Setting[_]] = Seq(
    mappings in Universal := Dist.getFileMappings((classDirectory in Compile).value, targetPath)
  )

  def relocateLib(target: String): Seq[sbt.Setting[_]] = Seq(
    scriptClasspathOrdering <<= scriptClasspathOrdering map { seq =>
      seq map { elem =>elem._1 -> elem._2.replace("lib/", target) }
    }
  )

  def javaLibMappings: Seq[sbt.Setting[_]] = Seq(
    mappings in Universal <++= scriptClasspathOrdering,
    autoScalaLibrary := false
  )

  def testingToolMappings(toolsJar: String): Seq[sbt.Setting[_]] = Seq(
    mappings in Universal <++= sourceDirectory map { dir =>
      Dist.getFileMappings(dir / "test/scripts", "tools")
    },
    mappings in Universal <+= (packageBin in Test) map { file =>
      file -> s"tools/$toolsJar"
    }
  )

  val mainScriptMappings: Seq[sbt.Setting[_]] = Seq(
    mappings in Universal <++= sourceDirectory map { dir =>
      Dist.getFileMappings(dir / "main/scripts", "bin")
    }
  )

  val intradaMappings: Seq[sbt.Setting[_]] = Seq(
    mappings in Universal <++= sourceDirectory map { dir =>
      Dist.getFileMappings(dir / "main/config/intrada_env.txt", "/")
    }
  )
}
