package ftplet;
/*
 * Copyright © CSC
 * Date: Jun 8, 2010
 * Time: 12:18:11 PM
 */

import junit.framework.TestCase;

/**
 * // TODO later translate to scala, do all tests in scalatest
 *
 * Test the rgbToJpg call to the ImageFunction implementation.
 */
public class NotifyFTPletTest extends TestCase {
    @Override
    protected void setUp() throws Exception {
    }

   /**
     * Test the creation of the Notifier
     */
    public void testNotifyCreation() {

        //test correct value
        try {
            NotifyFTPlet testClass = new NotifyFTPlet();
            testClass.setNotifyHost("localhost:4001");
        } catch(IllegalArgumentException ex) {
            fail("Unexpected exception " + ex);
        }

        //test only host part
        try {
            NotifyFTPlet testClass = new NotifyFTPlet();
            testClass.setNotifyHost("localhost");
            fail("only host should fail");
        } catch(IllegalArgumentException ex) {
            //is correct behaviour
        }
        //test too many parts
        try {
            NotifyFTPlet testClass = new NotifyFTPlet();
            testClass.setNotifyHost("localhost:4001:a");
            fail("too many should fail");
        } catch(IllegalArgumentException ex) {
            //is correct behaviour
        }
        //test not a number for port parts
        try {
            NotifyFTPlet testClass = new NotifyFTPlet();
            testClass.setNotifyHost("localhost:ab");
            fail("not a number should fail");
        } catch(IllegalArgumentException ex) {
                //is correct behaviour
        }
       //test empty host
       try {
           NotifyFTPlet testClass = new NotifyFTPlet();
           testClass.setNotifyHost(":4001");
           fail("empty parts should fail");
       } catch(IllegalArgumentException ex) {
               //is correct behaviour
       }
       //test empty port
       try {
           NotifyFTPlet testClass = new NotifyFTPlet();
           testClass.setNotifyHost("localhost:");
           fail("empty parts should fail");
       } catch(IllegalArgumentException ex) {
               //is correct behaviour
       }
       //test empty parts
       try {
           NotifyFTPlet testClass = new NotifyFTPlet();
           testClass.setNotifyHost(":");
           fail("empty parts should fail");
       } catch(IllegalArgumentException ex) {
               //is correct behaviour
       }
       //test empty string
       try {
           NotifyFTPlet testClass = new NotifyFTPlet();
           testClass.setNotifyHost("");
           fail("empty should fail");
       } catch(IllegalArgumentException ex) {
               //is correct behaviour
       }
       //test null
       try {
           NotifyFTPlet testClass = new NotifyFTPlet();
           testClass.setNotifyHost(null);
           fail("null should fail");
       } catch(IllegalArgumentException ex) {
               //is correct behaviour
       }
    }
}