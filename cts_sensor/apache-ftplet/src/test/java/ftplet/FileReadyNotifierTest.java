package ftplet;
/*
 * Copyright © CSC
 * Date: Jul 6, 2010
 * Time: 4:23:54 PM
 */

import junit.framework.TestCase;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.net.ServerSocket;
import java.net.Socket;
// TODO later translate to scala, do all tests in scalatest

/**
 * Test the FileReadyNotifier class. This is used in the FTPlet running on the FTP server.
 *
 */
public class FileReadyNotifierTest extends TestCase {

    @Override
    protected void setUp() throws Exception {
    }

  /**
     * Test normal flow receive server is already present
     * @throws Exception
     */
    public void testNormalFlow() throws Exception {
       ServerSocket recvServer = new ServerSocket(14001);
       long time = System.currentTimeMillis();

       String fileName = "my_file";
       FileReadyNotifier testClass = new FileReadyNotifier("localhost",14001);
       testClass.sendNotification(fileName, time, "put");

       System.out.println("Start accept");
       Socket sock = recvServer.accept();
       BufferedReader recv = new BufferedReader(new InputStreamReader(
                                  sock.getInputStream()));
       System.out.println("Start reading");
       String result = recv.readLine();
       assertEquals("FileName:", FileReadyNotifier.getPayload(fileName, time), result);
       recv.close();
       recvServer.close();
   }

    /**
       * Test receive server is NOT present and will be started after first send
       * @throws Exception
       */
    public void testServerNotReady() throws Exception {

       String fileName1 = "my_file";
        long time = System.currentTimeMillis();
       FileReadyNotifier testClass = new FileReadyNotifier("localhost",14001);
       testClass.sendNotification(fileName1, time, "");

       System.out.println("Start accept");
       ServerSocket recvServer = new ServerSocket(14001);
        long time2 = System.currentTimeMillis();

        String fileName2 = "my_second_file";
        testClass.sendNotification(fileName2, time2, "");

       Socket sock = recvServer.accept();
       BufferedReader recv = new BufferedReader(new InputStreamReader(
                                  sock.getInputStream()));


       System.out.println("Start reading");
       String result = recv.readLine();
       assertEquals("FileName1:", FileReadyNotifier.getPayload(fileName1, time), result);
       result = recv.readLine();
       assertEquals("FileName2:", FileReadyNotifier.getPayload(fileName2, time2), result);
       recv.close();
       recvServer.close();
   }

    /**
     * test reconnection. Send notify stop and start server and send a new Notify
     * @throws Exception
     */
   public void testServerKilledInbetween() throws Exception {
       ServerSocket recvServer = new ServerSocket(14001);
       long time = System.currentTimeMillis();

       String fileName = "my_file";
       FileReadyNotifier testClass = new FileReadyNotifier("localhost",14001);
       testClass.sendNotification(fileName, time, "");

       System.out.println("Start accept");
       Socket sock = recvServer.accept();
       BufferedReader recv = new BufferedReader(new InputStreamReader(
                                  sock.getInputStream()));
       System.out.println("Start reading");
       String result = recv.readLine();
       PrintWriter writer = new PrintWriter(sock.getOutputStream());
       writer.println("OK");
        writer.flush();
        sock.getOutputStream().flush();

       assertEquals("FileName:", FileReadyNotifier.getPayload(fileName, time), result);
       //stop server
       recv.close();
       recvServer.close();
       //And start it again
       recvServer = new ServerSocket(14001);
       long time2 = System.currentTimeMillis();

       String fileName2 = "my_file2";
       testClass.sendNotification(fileName2, time2, "");

       System.out.println("Start accept");
       sock = recvServer.accept();
       recv = new BufferedReader(new InputStreamReader(
                                  sock.getInputStream()));
       System.out.println("Start reading");
       result = recv.readLine();
       assertEquals("FileName:", FileReadyNotifier.getPayload(fileName2, time2), result);
       recv.close();
       recvServer.close();
   }

}
