package ftplet;

/**
 * Created by carlos on 22/11/16.
 */
public interface Notifier {

    void sendNotification(String path, long time, String action);

    void destroy();

}

