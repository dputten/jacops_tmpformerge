/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package ftplet;

import org.apache.ftpserver.ftplet.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

/**
 * Notify a TCPListener when a new file is completely received.
 */
public class ExportNotifyFTPlet extends NotifyFTPlet {
    private Logger log = LoggerFactory.getLogger(ExportNotifyFTPlet.class);

    @Override
    public FtpletResult onUploadEnd(FtpSession session, FtpRequest request) throws FtpException, IOException {
        File currentDir = getCurrentDir(session);
        //get fileName
        String filename = new File(request.getArgument()).getName();
        File currentFile = new File(currentDir, filename);
        if (!currentFile.exists()) {
            //very strange - new uploaded file doesn't exist
            log.warn("Uploaded file " + currentFile.getCanonicalPath() + " doesn't exists");
            return FtpletResult.DEFAULT;
        } else {
            return onEnd(session, request, "Verzonden");
        }
    }

    @Override
    public FtpletResult onDownloadEnd(FtpSession session, FtpRequest request) throws FtpException, IOException {
        return onEnd(session, request, "Opgehaald");
    }

    @Override
    public FtpletResult onRmdirEnd(FtpSession session, FtpRequest request) throws FtpException, IOException {
        File currentDir = getCurrentDir(session);
        //get fileName
        String filename = new File(request.getArgument()).getName();
        File currentFile = new File(currentDir, filename);
        if (currentFile.exists()) {
            //very strange - removed folder still exist
            log.warn("Deleted file " + currentFile.getCanonicalPath() + " still exists.");
            return FtpletResult.DEFAULT;
        } else {
            return onEnd(session, request, "Verwijderd");
        }
    }

    private FtpletResult onEnd(FtpSession session, FtpRequest request, String action) throws FtpException, IOException {
        File currentDir = getCurrentDir(session);
        //get fileName
        String filename = new File(request.getArgument()).getName();
        File currentFile = new File(currentDir, filename);
        //notify
        if (notifier != null) {
            notifier.sendNotification(currentFile.getCanonicalPath(), System.currentTimeMillis(), action);
        } else {
            log.warn("Notifier is null");
        }
        return FtpletResult.DEFAULT;
    }

    @Override
    public FtpletResult onConnect(FtpSession session) throws FtpException, IOException {
        return logSession(session, "geopend");
    }

    @Override
    public FtpletResult onDisconnect(FtpSession session) throws FtpException, IOException {
        return logSession(session, "gesloten");
    }

    private FtpletResult logSession(FtpSession session, String description) throws FtpException, IOException {
        if (notifier != null) {
            User user = session.getUser();
            String username;
            if (user != null) {
                username = "voor " + user.getName();
            } else {
                username = "";
            }
            String hostname = "";
            if (session != null) {
                if (session.getClientAddress() != null) {
                    hostname = "@" + session.getClientAddress().getHostName();
                }
            }
            notifier.sendNotification("", System.currentTimeMillis(), "FTP verbinding " + description + " " + username + hostname);
        } else {
            log.warn("Notifier is null");
        }
        return FtpletResult.DEFAULT;
    }
}