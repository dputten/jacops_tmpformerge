/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package ftplet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.Socket;

/**
 * Class to send a notify string through a TCP connection.
 * A new connection is made every time, detecting a lost TCP session is not always fail proof.
 */
public class FileReadyNotifier implements Notifier {
    private Logger log = LoggerFactory.getLogger(FileReadyNotifier.class);

    private String hostName;
    private int hostPort;
    private StringWriter queue = null;

    public FileReadyNotifier(String hostName, int hostPort) {
        this.hostName = hostName;
        this.hostPort = hostPort;
    }

    @Override
    public void sendNotification(String path, long time, String action) {
        String payload = getPayload(path, time);
        log.info("Sending payload {}", payload);
        sendNotification(payload);
    }

    @Override
    public void destroy() {
        //nothing to close
    }

    public static String getPayload(String path, long time) {
        return String.format("X1:%s:%s", path, time);
    }

    private synchronized void sendNotification(String payload) {
        Socket socket = null;
        PrintWriter writer = null;

        if (log.isDebugEnabled()) {
            log.debug("Connect to " + hostName + ":" + hostPort);
        }
        try {
            socket = new Socket(hostName, hostPort);
            socket.setKeepAlive(true);
            writer = new PrintWriter(socket.getOutputStream(), false); //autoflush is true
            //send all remaining messages
            if (queue != null) {
                StringWriter tmp = queue;
                queue = null;
                writer.write(tmp.toString());
                writer.flush();
            }
            writer.println(payload);
        } catch (Exception ex) {
            log.error("ConnectToHost failed ",ex);
            //couldn't connect to host
            //store notifications
            if (queue == null) {
                queue = new StringWriter(512);
                //first to fail notification so start retry thread
            }
            log.debug("Use queue");
            queue.write(payload);
            queue.write("\n");
        } finally {
            cleanUpSession(socket, writer);
        }
    }

    private void cleanUpSession(Socket socket, PrintWriter writer) {
        log.debug("CleanUp connection");
        try {
            if (writer != null) {
                writer.flush();
                writer.close();
            }
        } finally {
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    log.warn("Socket close failed ", e);
                }
            }
        }
    }
}
