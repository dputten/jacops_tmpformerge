/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package ftplet;

import org.apache.ftpserver.ftplet.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.File;
import java.io.IOException;


/**
 * Notify a TCPListener when a new file is completely received.
 */
public class NotifyFTPlet extends DefaultFtplet {
    private Logger log = LoggerFactory.getLogger(NotifyFTPlet.class);

    protected Notifier notifier;
    private AMQPNotifier.Config amqpConfig = new AMQPNotifier.Config();

    private static String START_TIME = "last-start-time";

    protected File getCurrentDir(FtpSession session) throws FtpException{
        String homeDir = session.getUser().getHomeDirectory();
        FileSystemView fileSystem = session.getFileSystemView();
        return  new File(homeDir,fileSystem.getWorkingDirectory().getAbsolutePath());
    }

    @Override
    public FtpletResult onUploadStart(FtpSession session, FtpRequest request) throws FtpException, IOException {
        long time = System.currentTimeMillis();
        session.setAttribute(START_TIME, time);
        return super.onUploadStart(session, request);
    }

    @Override
    public FtpletResult onUploadEnd(FtpSession session, FtpRequest request) throws FtpException, IOException {

        File currentDir = getCurrentDir(session);
        //get fileName
        String filename = new File(request.getArgument()).getName();
        File currentFile = new File(currentDir, filename);
        if(currentFile.exists()) {
            //notify
            if(notifier != null) {
                notifier.sendNotification(currentFile.getAbsolutePath(), getTime(session), "put");
            } else {
                log.warn("Notifier is null");
            }
        } else {
            //very strange new uploaded file doesn't exist
            log.warn("Uploaded file "+currentFile.getAbsolutePath()+" doesn't exists");
        }
        return FtpletResult.DEFAULT;
    }

    public void setNotifyHost(String host) {
      if(host != null) {
          String [] temp = null;
          temp = host.split(":");
          if(temp.length == 2) {
              String hostName = temp[0];
              if(hostName.length() > 0) {
                  try {
                    int port = Integer.valueOf(temp[1]).intValue();
                    notifier = new FileReadyNotifier(hostName,port);
                    log.info("Using Camel/Mina Notifier");
                  } catch(NumberFormatException ex) {
                      //do nothing will be logged when notifier is null
                  }
              }
          }
      }
      if(notifier == null) {
          String msg = "Notify configuration failed: "+host;
          log.error(msg);
          throw new IllegalArgumentException(msg);
      }
    }

    public void setAmqpUri(String amqpUri) {
        amqpConfig.uri = amqpUri;
        checkAmqpNotifier();
    }

    public void setProducerEndpoint(String producerEndpoint) {
        amqpConfig.producerEndpoint = producerEndpoint;
        checkAmqpNotifier();
    }

    public void setProducerRouteKey(String producerRouteKey) {
        amqpConfig.producerRouteKey = producerRouteKey;
        checkAmqpNotifier();
    }

    private void checkAmqpNotifier() {
        if (this.amqpConfig.isComplete()) {
            try {
                this.notifier = new AMQPNotifier(amqpConfig);
                log.info("Using AQMP Notifier");
            } catch (Exception ex) {
                log.error("Error creating AMQPNotifier", ex);
            }
        }
    }

    private static long getTime(FtpSession session) {
        Object obj = session.getAttribute(START_TIME);
        return (obj instanceof Long) ? (Long)obj : System.currentTimeMillis();
    }

    @Override
    public void destroy() {
        if (this.notifier != null) {
            this.notifier.destroy();
        }
        super.destroy();
    }
}