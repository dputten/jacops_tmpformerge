package ftplet;

import com.rabbitmq.client.*;
import com.rabbitmq.tools.json.JSONWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by carlos on 22/11/16.
 */
public class AMQPNotifier implements Notifier {

    private Logger log = LoggerFactory.getLogger(AMQPNotifier.class);

    private ConnectionFactory factory;
    private Connection conn;
    private Channel channel;
    private String exchange;
    private String routeKey;
    private AMQP.BasicProperties.Builder builder;
    private JSONWriter jsonWriter;

    public AMQPNotifier(Config config) throws Exception {
        if (!config.isComplete()) throw new IllegalArgumentException("Configuration is not complete");

        this.exchange = config.producerEndpoint;
        this.routeKey = config.producerRouteKey;

        this.builder = propertiesBuilder();
        this.jsonWriter = new JSONWriter(false);

        this.factory = new ConnectionFactory();
        factory.setUri(config.uri);
        factory.setAutomaticRecoveryEnabled(true);
        try {
            this.conn = factory.newConnection();
            this.channel = conn.createChannel();
        } catch(Exception t) {
            log.error("Could not connect to AMQP " + t.toString(),t);
        }
    }


    @Override
    public void sendNotification(String path, long time, String action) {
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("version", 1);
        map.put("path", path);
        map.put("time", time);
        map.put("action", action);

        String payload = jsonWriter.write(map);
        try {
            publish(payload);
            log.info("Published " + payload);
        } catch (AlreadyClosedException ex) {
            //we don't show the whole stack trace in this case to prevent log flood when broker is down
            log.warn("Could not publish " + payload + ". Cause: " + ex.getMessage());
        } catch (Exception ex) {
            log.warn("Could not publish " + payload, ex);
        }
    }

    private void checkAMQP() throws Exception {
        try {
            if (conn == null) {
                conn = factory.newConnection();
            }
            if (channel == null) {
                channel = conn.createChannel();
            }
        } catch(Exception t) {
            log.error("Could not connect to AMQP " + t.toString(),t);
            throw t;
        }
    }

    private void publish(String payload) throws Exception {
        checkAMQP();
        byte[] bytes = payload.getBytes();
        builder.timestamp(new Date());
        builder.messageId(UUID.randomUUID().toString());
        AMQP.BasicProperties props = builder.build();
        channel.basicPublish(exchange, routeKey, props, bytes);
    }

    private AMQP.BasicProperties.Builder propertiesBuilder() {
        AMQP.BasicProperties.Builder builder = new AMQP.BasicProperties().builder();
        builder.appId("FTPLet image notifier");
        builder.contentEncoding("UTF-8");
        builder.contentType("application/json");
        builder.type("CameraImageMessage");
        builder.deliveryMode(2); //persistent
        return builder;
    }

    @Override
    public void destroy() {
        try {
            if (this.channel != null) {
                channel.close();
            }
        } catch (Exception ex) {
            log.warn("Error closing channel: " + ex.toString(), ex);
        }
        try {
            if (this.conn != null) {
                conn.close();
            }
        } catch (Exception ex) {
            log.warn("Error closing connection: " + ex.toString(), ex);
        }
    }

    static class Config {
        public String uri = null;
        public String producerEndpoint = null;
        public String producerRouteKey = null;

        public boolean isComplete() {
            return uri != null && producerEndpoint != null && producerRouteKey != null;
        }
    }

}
