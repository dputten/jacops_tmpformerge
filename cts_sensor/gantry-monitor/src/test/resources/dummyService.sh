#!/usr/bin/env bash

case "$1" in
    registration)
        echo "$1 is running"
        exit 0
        ;;
    fail)
        exit 1
        ;;
    *)
        echo "$1 is not running"
        exit 0
        ;;
esac

