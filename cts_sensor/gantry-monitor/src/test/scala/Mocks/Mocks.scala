package Mocks

import csc.gantry.config._
import csc.gantry.config.ZkCorridorInfo
import csc.gantry.config.CorridorsConfig
import csc.gantry.config.PIRConfig
import csc.gantry.config.CorridorConfig
import csc.gantry.config.PirCameraSensorConfigImpl
import csc.vehicle.message.Lane
import csc.gantry.config.CameraConfig

class LaneConfigurationMock extends DefaultLaneConfiguration {

  import LaneConfigurationMock._

  private var _lanes: List[PirCameraSensorConfig] = List(cfg1, cfg2, cfg3, cfg4)

  override def lanes: List[LaneConfig] = _lanes

  def deleteLane(lane: Lane) {
    _lanes = _lanes.filterNot(cfg ⇒ cfg.lane == lane)
  }
  def addLane(lane: PirCameraSensorConfig) {
    _lanes = _lanes :+ lane
  }

  private var corridors: List[CorridorConfig] = List(corridor1, corridor2)

  def deleteCorridor(corridorId: String) {
    corridors = corridors.filterNot(cfg ⇒ cfg.id == corridorId)
  }
  def addCorridor(cor: CorridorConfig) {
    corridors = corridors :+ cor
  }

  override def getCorridorsConfig(system: String): Option[CorridorsConfig] = Some(CorridorsConfig(corridors))

}

object LaneConfigurationMock {
  val corridorId1 = "Cor1"
  val corridorId2 = "Cor2"
  val systemId = "A4"

  val lane1 = new Lane(laneId = "A2-40HM-Lane1",
    name = "Lane1",
    gantry = "E1",
    system = systemId,
    sensorGPS_longitude = 1,
    sensorGPS_latitude = 1)
  val lane2 = new Lane(laneId = "A2-40HM-Lane2",
    name = "Lane2",
    gantry = "E1",
    system = systemId,
    sensorGPS_longitude = 1,
    sensorGPS_latitude = 1)
  val lane3 = new Lane(laneId = "A2-80HM-Lane1",
    name = "Lane1",
    gantry = "80HM",
    system = systemId,
    sensorGPS_longitude = 1,
    sensorGPS_latitude = 1)
  val lane4 = new Lane(laneId = "A2-80HM-Lane2",
    name = "Lane2",
    gantry = "80HM",
    system = systemId,
    sensorGPS_longitude = 1,
    sensorGPS_latitude = 1)

  val camera = new CameraConfig(relayURI = "Dummy", cameraHost = "localhost", cameraPort = 5000, maxTriggerRetries = 5, timeDisconnected = 1000)
  val pir = new PIRConfig(pirHost = "Dummy", pirPort = 6000, pirAddress = 1, refreshPeriod = 1000, vrHost = "Dummy", vrPort = 6001)
  val cfg1 = new PirCameraSensorConfigImpl(
    lane = lane1,
    camera = camera,
    imageMask = None,
    recognizeOptions = Map(),
    pir = pir)
  val cfg2 = new PirCameraSensorConfigImpl(
    lane = lane2,
    camera = camera,
    imageMask = None,
    recognizeOptions = Map(),
    pir = pir)
  val cfg3 = new PirCameraSensorConfigImpl(
    lane = lane3,
    camera = camera,
    imageMask = None,
    recognizeOptions = Map(),
    pir = pir)
  val cfg4 = new PirCameraSensorConfigImpl(
    lane = lane4,
    camera = camera,
    imageMask = None,
    recognizeOptions = Map(),
    pir = pir)

  val corridor1 = CorridorConfig(id = corridorId1,
    name = corridorId1,
    serviceType = "SectionControl",
    info = ZkCorridorInfo(corridorId = 1,
      locationCode = "2",
      reportId = "",
      reportHtId = "",
      reportPerformance = true,
      locationLine1 = ""))

  val corridor2 = corridor1.copy(
    id = corridorId2,
    name = corridorId2,
    info = corridor1.info.copy(corridorId = 2))

}