/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.gantrymonitor.config

import java.util.concurrent.TimeUnit

import scala.concurrent.duration._
import csc.gantry.config.LaneConfiguration
import csc.gantrymonitor.selftest._
import csc.systemevents.{ SystemEventStore, AmqpSystemEventStore }
import org.scalatest.{ MustMatchers, WordSpec }

class GantryMonitorConfigurationTest
  extends WordSpec
  with MustMatchers
  with SelftestTestData {

  "configure" must {

    val config = new Configuration()
    val selfCfg = config.monitorConfiguration.selfTest

    "with config file" in {
      selfCfg.imageTimeout must be(20.seconds)
      selfCfg.timeout must be(1.minutes)
      selfCfg.quality must be(80)

      val laneConfiguration = config.laneConfiguration
      laneConfiguration.getLanes()

      val gantryConfiguration = config.gantryConfiguration
      gantryConfiguration.systemId must be("A4")
      gantryConfiguration.gantryIds must be(List("E1"))

      config.laneConfiguration.isInstanceOf[LaneConfiguration] must be(true)

      sysEvtStore(config).isInstanceOf[AmqpSystemEventStore] must be(true)
    }

    "load amqp settings" in {
      val amqpConfiguration = config.amqpConfiguration
      amqpConfiguration.actorName must be("GantryMonitorActor")
      amqpConfiguration.amqpUri must be("amqp://localhost:5672/")
      amqpConfiguration.reconnectDelay must be(Duration.create(5000, TimeUnit.MILLISECONDS))

      val amqpSystemEventConfiguration = config.amqpSystemEventServicesConfiguration
      amqpSystemEventConfiguration.producerEndpoint must be("")
      amqpSystemEventConfiguration.producerRouteKey must be("systemevents_res")
      amqpSystemEventConfiguration.serversCount must be(5)

      val amqpCalibrationConfiguration = config.amqpCalibrationServicesConfiguration
      amqpCalibrationConfiguration.consumerQueue must be("calibration-in")
      amqpCalibrationConfiguration.producerEndpoint must be("gantry-out-exchange")
      amqpCalibrationConfiguration.producerRouteKey must be("")
      amqpCalibrationConfiguration.serversCount must be(5)

      val laneConfiguration = config.laneConfiguration
      laneConfiguration.getLanes()

      val gantryConfiguration = config.gantryConfiguration
      gantryConfiguration.systemId must be("A4")
      gantryConfiguration.gantryIds must be(List("E1"))

      sysEvtStore(config).isInstanceOf[AmqpSystemEventStore] must be(true)
    }

    "load selftest profiles" in {
      val profiles = selfCfg.profiles
      profiles.size must be(1)

      profiles.get("default") match {
        case Some(profile) ⇒ profile must be(defaultProfile)
        case None          ⇒ fail("No profile")
      }
    }
  }

  private def sysEvtStore(config: Configuration): SystemEventStore =
    config.createSystemEventStore(config.config)

}

