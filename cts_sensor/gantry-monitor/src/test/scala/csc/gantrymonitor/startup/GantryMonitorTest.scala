/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.gantrymonitor.startup

import Mocks.LaneConfigurationMock
import akka.actor.{ Actor, ActorRef, ActorSystem, Props }
import akka.testkit.TestKit
import scala.concurrent.duration._
import csc.akka.logging.DirectLogging
import csc.gantry.config.{ CameraConfig, EmptyLaneConfiguration, LaneConfiguration, MultiLaneCamera }
import csc.gantrymonitor.calibration.LegacyCalibrationPropsFactory
import csc.gantrymonitor.config.{ Configuration, GantryConfiguration, GantryMonitorConfiguration, SelfTestConfiguration }
import csc.util.test.ObjectBuilder
import csc.vehicle.jai.StatusCode
import org.scalatest._
import _root_.tools.jaiconnection.SimulateJai

class GantryMonitorTest
  extends TestKit(ActorSystem("GantryMonitorTest"))
  with WordSpecLike
  with MustMatchers
  with DirectLogging
  with BeforeAndAfterAll {

  import GantryMonitorTest._

  val jaiSimulator = new SimulateJai(cameraHost, cameraPort)
  val configuration = new Configuration()
  val selftestActorFactory = new LegacyCalibrationPropsFactory(configuration)

  override def beforeAll() {
    super.beforeAll()
    jaiSimulator.updateStatus(StatusCode.ERROR_STATUS, 0L)
    jaiSimulator.updateStatus(StatusCode.NTP_ESTIMATE_ERROR, 0L)
    jaiSimulator.updateStatus(StatusCode.NTP_RESTART_COUNTER, 0L)
    jaiSimulator.updateStatus(StatusCode.NTP_STATUS, 1L)
    jaiSimulator.setImage("IMAGE".getBytes())
    Thread.sleep(1000)
  }

  override def afterAll() {
    //stop actors
    system.shutdown
    jaiSimulator.stopJaiCamera()
    super.afterAll()
  }

  def createAmqpProducer: ActorRef = system.actorOf(Props(new NoOpActor))

  "GantryMonitor" must {

    "start and manage the single lane JAI monitors" in {
      val laneConfig = new LaneConfigurationMock
      val gantryConfiguration = GantryConfiguration("A4", List("E1"))
      val startupSensorMonitor =
        new GantryMonitor(config, laneConfig, gantryConfiguration, configuration, None, createAmqpProducer, selftestActorFactory, system)

      startupSensorMonitor.start

      // The LaneConfigurationMock has only 2 lanes for gantry E1
      startupSensorMonitor.startedJaiMonitors.size must be(2)

      startupSensorMonitor.startedJaiMonitors.head.camera.id must be(LaneConfigurationMock.lane1.laneId)
      startupSensorMonitor.startedJaiMonitors.last.camera.id must be(LaneConfigurationMock.lane2.laneId)
      startupSensorMonitor.startedJaiMonitors.head.ref must not be null

      startupSensorMonitor.actors.size must be(2)
      //      startupSensorMonitor.selftestActor.isDefined must be(true)
      //      startupSensorMonitor.selftestConsumer.isDefined must be(true)
    }

    "start and manage the multi lane JAI monitors" in {

      val laneConfig = multiLaneConfiguratiom
      val startupSensorMonitor =
        new GantryMonitor(config, laneConfig, gantryConfiguration, configuration, None, createAmqpProducer, selftestActorFactory, system)
      startupSensorMonitor.start

      // The LaneConfigurationMock has only 2 lanes for gantry E1
      startupSensorMonitor.startedJaiMonitors.size must be(1)

      val sjm = startupSensorMonitor.startedJaiMonitors(0)
      sjm.camera.id must be(laneConfig.getMultiLaneCameras(0).config.cameraId)
      sjm.ref must not be null

      startupSensorMonitor.startedJaiMonitors.head.ref

      startupSensorMonitor.actors.size must be(2)
    }
  }
}

object GantryMonitorTest extends ObjectBuilder {

  val cameraHost = "localhost"
  val cameraPort = 5000
  val systemId = "A4"
  val gantryId = "E1"

  val config = GantryMonitorConfiguration(
    selfTest = SelfTestConfiguration(20.seconds, 5.minutes, 80, None, None, Map.empty),
    delaySystemEvent = 1.minute,
    maxRetriesGetImage = 3,
    timeoutGetImage = 1.minute,
    errorCheck = Some(1.minutes))

  lazy val gantryConfiguration = GantryConfiguration(systemId, List(gantryId))

  lazy val cameraConfig = create[CameraConfig].copy(relayURI = "/data/images/E1CAM1", cameraHost = cameraHost, cameraPort = cameraPort)

  lazy val multiLaneCameras = List(MultiLaneCamera(cameraConfig, systemId, gantryId, Some("lane1"), Some("lane2")))

  lazy val multiLaneConfiguratiom: LaneConfiguration = new EmptyLaneConfiguration {
    override def getMultiLaneCameras: List[MultiLaneCamera] = multiLaneCameras
  }
}

class NoOpActor extends Actor {
  override def receive: Receive = {
    case _ ⇒ //just ignore
  }
}

