/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.gantrymonitor.jai

import java.util.Date

import _root_.tools.jaiconnection.SimulateJai
import akka.actor.{ ActorSystem, Props }
import scala.concurrent.Await
import akka.pattern.ask
import akka.testkit.{ TestKit, TestProbe }
import scala.concurrent.duration._
import csc.akka.logging.DirectLogging
import csc.gantry.config.CameraConfig
import csc.systemevents.SystemEvent
import csc.vehicle.jai.{ JaiErrorMessage, _ }
import csc.vehicle.message.Lane
import org.scalatest._
import testframe.Compare

class JaiMonitoringTest extends TestKit(ActorSystem("PirPollerTest")) with WordSpecLike with MustMatchers with BeforeAndAfterAll with DirectLogging {
  val config = new CameraConfig(relayURI = "TEST_DIR",
    cameraHost = "localhost", cameraPort = 14140, maxTriggerRetries = 10, timeDisconnected = 20000)
  val lane = new Lane("A2-20MH-Lane1", "Lane1", "20MH", "A2", 10, 10)

  val jaiSimulator = new SimulateJai(config.cameraHost, config.cameraPort)

  override def beforeAll() {
    jaiSimulator.updateStatus(StatusCode.ERROR_STATUS, 0L)
    jaiSimulator.updateStatus(StatusCode.NTP_ESTIMATE_ERROR, 0L)
    jaiSimulator.updateStatus(StatusCode.NTP_RESTART_COUNTER, 0L)
    jaiSimulator.updateStatus(StatusCode.NTP_STATUS, 1L)
  }

  override def afterAll() {
    system.shutdown
    jaiSimulator.stopJaiCamera()
  }

  "The camera monitor" must {

    "collect camera messages and send them to eventbus" in {
      val probe = TestProbe()
      system.eventStream.subscribe(probe.ref, classOf[SystemEvent])

      val monitor = system.actorOf(Props(new JaiMonitor(lane, config, 0xFF, 0 seconds, 5, 30 seconds)))
      val error = new JaiErrorMessage(new Date(), ErrorCodeMessage.FTP_DOWN, ErrorStateMessage.PRESENT)
      Thread.sleep(2000) //Connection between simulator and monitor has to be established
      jaiSimulator.sendError(error)
      val expectedMsg = new SystemEvent(componentId = lane.laneId + "-camera",
        timestamp = error.timestamp.getTime,
        systemId = lane.system,
        eventType = DisturbanceTypes.CAMERA_FTP,
        userId = "system",
        reason = "DOWN",
        gantryId = Some(lane.gantry),
        laneId = Some(lane.laneId))
      probe.expectMsg(5 seconds, expectedMsg)
      system.stop(monitor)
      system.eventStream.unsubscribe(probe.ref)
    }
    "Skip camera messages when cleared" in {
      val probe = TestProbe()
      system.eventStream.subscribe(probe.ref, classOf[SystemEvent])

      val monitor = system.actorOf(Props(new JaiMonitor(lane, config, 0xFF, 0 seconds, 5, 30 seconds)))
      val error = new JaiErrorMessage(new Date(), ErrorCodeMessage.FTP_DOWN, ErrorStateMessage.CLEARED)
      Thread.sleep(2000) //Connection between simulator and monitor has to be established
      jaiSimulator.sendError(error)
      probe.expectNoMsg(10 seconds)
      system.stop(monitor)
      system.eventStream.unsubscribe(probe.ref)
    }
    "collect camera status when requesting status" in {
      val monitor = system.actorOf(Props(new JaiMonitor(lane, config, 0xFF, 0 seconds, 5, 30 seconds)))
      Thread.sleep(2000) //Connection between simulator and monitor has to be established
      jaiSimulator.updateStatus(StatusCode.ERROR_STATUS, 0L)
      jaiSimulator.updateStatus(StatusCode.NTP_ESTIMATE_ERROR, 0L)
      jaiSimulator.updateStatus(StatusCode.NTP_RESTART_COUNTER, 0L)
      jaiSimulator.updateStatus(StatusCode.NTP_STATUS, 1L)

      val future = monitor.ask(JaiStatusRequest)(20 second)
      val status = Await.result(future, 20 seconds).asInstanceOf[JaiStatusResponse]
      val expectedMsg = new JaiStatusResponse(lane.laneId, Nil)
      status must be(expectedMsg)
      system.stop(monitor)
    }
    "collect camera image when requesting trigger" in {
      val image = "IMAGE".getBytes()
      val monitor = system.actorOf(Props(new JaiMonitor(lane, config, 0xFF, 0 seconds, 5, 30 seconds)))
      Thread.sleep(2000) //Connection between simulator and monitor has to be established
      jaiSimulator.updateStatus(StatusCode.ERROR_STATUS, 0L)
      jaiSimulator.updateStatus(StatusCode.NTP_ESTIMATE_ERROR, 0L)
      jaiSimulator.updateStatus(StatusCode.NTP_RESTART_COUNTER, 0L)
      jaiSimulator.updateStatus(StatusCode.NTP_STATUS, 1L)
      jaiSimulator.setImage(image)
      val future = monitor.ask(JaiImageRequest)(20 second)
      val imageResponse = Await.result(future, 20 seconds).asInstanceOf[JaiImageResponse]
      Compare.compareArrays(imageResponse.image, image)
      system.stop(monitor)
    }
    "collect camera error when requesting trigger" in {
      val monitor = system.actorOf(Props(new JaiMonitor(lane, config, 0xFF, 0 seconds, 5, 30 seconds)))
      Thread.sleep(2000) //Connection between simulator and monitor has to be established
      jaiSimulator.updateStatus(StatusCode.ERROR_STATUS, 0L)
      jaiSimulator.updateStatus(StatusCode.NTP_ESTIMATE_ERROR, 0L)
      jaiSimulator.updateStatus(StatusCode.NTP_RESTART_COUNTER, 0L)
      jaiSimulator.updateStatus(StatusCode.NTP_STATUS, 1L)
      val future = monitor.ask(JaiImageRequest)(20 second)
      val imageResponse = Await.result(future, 20 seconds).asInstanceOf[JaiImageError]
      imageResponse.error must be(Seq())
      system.stop(monitor)
    }
    "collect delayed camera messages and send them to eventbus" in {
      val probe = TestProbe()
      system.eventStream.subscribe(probe.ref, classOf[SystemEvent])

      val monitor = system.actorOf(Props(new JaiMonitor(lane, config, 0xFF, 5 seconds, 5, 30 seconds)))
      val error = new JaiErrorMessage(new Date(), ErrorCodeMessage.FTP_DOWN, ErrorStateMessage.PRESENT)
      Thread.sleep(2000) //Connection between simulator and monitor has to be established
      jaiSimulator.sendError(error)
      val expectedMsg = new SystemEvent(componentId = lane.laneId + "-camera",
        timestamp = error.timestamp.getTime,
        systemId = lane.system,
        eventType = DisturbanceTypes.CAMERA_FTP,
        userId = "system",
        reason = "DOWN",
        gantryId = Some(lane.gantry),
        laneId = Some(lane.laneId))
      probe.expectNoMsg(3 seconds)
      probe.expectMsg(5 seconds, expectedMsg)
      system.stop(monitor)
      system.eventStream.unsubscribe(probe.ref)
    }
    "Skip delayed error when cleared" in {
      val probe = TestProbe()
      system.eventStream.subscribe(probe.ref, classOf[SystemEvent])

      val monitor = system.actorOf(Props(new JaiMonitor(lane, config, 0xFF, 5 seconds, 5, 30 seconds)))
      val error = new JaiErrorMessage(new Date(), ErrorCodeMessage.FTP_DOWN, ErrorStateMessage.PRESENT)
      Thread.sleep(2000) //Connection between simulator and monitor has to be established
      jaiSimulator.sendError(error)
      probe.expectNoMsg(3 seconds)
      val errorCleared = new JaiErrorMessage(new Date(), ErrorCodeMessage.FTP_DOWN, ErrorStateMessage.CLEARED)
      jaiSimulator.sendError(errorCleared)
      probe.expectNoMsg(10 seconds)

      system.stop(monitor)
      system.eventStream.unsubscribe(probe.ref)
    }

  }
}
