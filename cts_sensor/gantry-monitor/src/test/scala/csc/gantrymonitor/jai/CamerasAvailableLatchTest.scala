package csc.gantrymonitor.jai

import java.util.concurrent.TimeoutException

import scala.concurrent.duration.Duration
import csc.gantrymonitor.stub.{ CamerasAvailableLatch, JaiCameraStub }
import org.scalatest.WordSpec
import org.scalatest._
import _root_.tools.jaiconnection.CameraStarted

/**
 * Created by carlos on 29/04/16.
 */
class CamerasAvailableLatchTest extends WordSpec with MustMatchers {

  import JaiCameraStubAndClientTest._

  val timeout = Duration("500 milliseconds")

  def expectReady(latch: CamerasAvailableLatch, expect: Boolean): Unit = {
    val time = System.currentTimeMillis()
    try {
      latch.awaitReady(timeout)
      if (!expect) fail("Should be failing with TimeoutException")
    } catch {
      case ex: TimeoutException ⇒
        if (expect) fail(ex)
        else {
          val diff = System.currentTimeMillis() - time
          assert(diff >= timeout.toMillis) //making sure it times out
        }
      case other: Throwable ⇒ fail(other)
    }
  }

  "CamerasAvailableLatch" must {

    "Be ready only when all cameras started" in {
      val cams = JaiCameraStub.suitableCameras(cameras)
      val camSet: Set[String] = cams.map(c ⇒ c.cameraId).toSet
      camSet must be(Set("CAM1", "CAM2"))

      val camA = cams(0)
      val camB = cams(1)

      val latch = new CamerasAvailableLatch(cams)
      latch.remaining.size must be(2)

      latch.add(CameraStarted(camA.cameraHost, camA.cameraPort))
      latch.remaining must be(List(camB))

      expectReady(latch, false)
      latch.add(CameraStarted(camB.cameraHost, camB.cameraPort))
      latch.remaining must be(Nil)

      expectReady(latch, true)
    }
  }
}
