package csc.gantrymonitor.jai

import akka.actor.{ ActorRef, ActorSystem }
import akka.testkit.{ TestKit, TestProbe }
import scala.concurrent.duration.Duration
import csc.akka.logging.DirectLogging
import csc.akka.remote.SocketServer
import csc.gantry.config.CameraConfig
import csc.gantrymonitor.stub.{ JaiCameraStub, JaiCameraStubClient }
import csc.util.test.ObjectBuilder
import csc.vehicle.jai.ErrorCodeMessage.ErrorCodeMessage
import csc.vehicle.jai._
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }

/**
 * Created by carlos on 29/04/16.
 */
class JaiCameraStubAndClientTest
  extends TestKit(ActorSystem("CameraStubTest"))
  with WordSpecLike
  with MustMatchers
  with BeforeAndAfterAll
  with BeforeAndAfterEach
  with DirectLogging {

  import JaiCameraStubAndClientTest._

  implicit val timeout = Duration("5 seconds")

  var actor: ActorRef = null
  var client: JaiCameraStubClient = null
  var probe: TestProbe = null
  var connections: List[JaiCameraConnection] = Nil

  "JaiCameraStubClient/JaiCameraStub" must {

    "Pass NTP errors to the proper camera" in {

      expectCamerasStatuses

      sendErrorAndVerify(camId1, ErrorCodeMessage.NTP_DOWN, true, DisturbanceTypes.CAMERA_NTP_DOWN)

      sendErrorAndVerify(camId2, ErrorCodeMessage.NTP_DOWN, true, DisturbanceTypes.CAMERA_NTP_DOWN)

      sendErrorAndVerify(camId2, ErrorCodeMessage.NTP_DOWN, false, DisturbanceTypes.CAMERA_NTP_DOWN)

      sendErrorAndVerify(camId1, ErrorCodeMessage.NTP_DOWN, false, DisturbanceTypes.CAMERA_NTP_DOWN)

    }
  }

  def sendErrorAndVerify(camId: String, code: ErrorCodeMessage, on: Boolean, expectedEventType: String): Unit = {
    val state = if (on) "on" else "off"
    val cmd = "ERROR %s %s".format(code, state)
    client.send(Some(camId), cmd)

    val msg = probe.expectMsgType[DisturbanceEvent]
    val expectedState = if (on) DisturbanceState.DETECTED else DisturbanceState.SOLVED
    msg.laneId must be(camId)
    msg.eventType.eventType must be(expectedEventType)
    msg.state must be(expectedState)
  }

  def expectCamerasStatuses: Unit = for (i ← 0 until connections.size) probe.expectMsgType[CameraStatus]

  override protected def beforeEach(): Unit = {
    probe = TestProbe()
    connections = JaiCameraStub.suitableCameras(cameras) map { c ⇒
      val con = new JaiCameraConnection(c.cameraId, c.cameraHost, c.cameraPort, c.maxTriggerRetries, c.timeDisconnected, 0xFF, Set(probe.ref))
      con.start()
      con
    }
  }

  override protected def afterEach(): Unit = {
    connections.foreach(_.shutdown())
  }

  override protected def beforeAll(): Unit = {
    actor = JaiCameraStub(system, uri, cameras, None) //creates actor and waits for cameras to start
    client = new JaiCameraStubClient(SocketServer(host, port))
  }

  override protected def afterAll(): Unit = {
    system.shutdown()
  }
}

object JaiCameraStubAndClientTest extends ObjectBuilder {

  val systemId = "A4"
  val gantryId = "E1"
  val host = "localhost"
  val port = 12410
  val uri = "mina:tcp://%s:%s?textline=true&sync=false".format(host, port)

  val camId1 = "CAM1"
  val camId2 = "CAM2"

  val cameras = List(
    CameraConfig("folder/" + camId1, "localhost", 18301, 5, 5000),
    CameraConfig("folder/" + camId2, "localhost", 18302, 5, 5000),
    CameraConfig("folder/CAM3", "somewhere", 18303, 5, 5000))

}