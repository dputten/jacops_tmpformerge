/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.gantrymonitor.calibration

import java.io.File

import akka.actor.{ ActorSystem, Props }
import akka.testkit.{ TestKit, TestProbe }
import scala.concurrent.duration._
import csc.calibration.{ GantryCalibrationRequest, GantryCalibrationResponse, StepResult }
import csc.gantrymonitor.caseclasses.StartedJaiMonitor
import csc.gantrymonitor.config.SelfTestConfiguration
import csc.gantrymonitor.jai.{ JaiImageRequest, JaiImageResponse }
import csc.vehicle.message.Lane
import imagefunctions.ImageFunctionsFactory
import org.scalatest._
import testframe.Resources

class CalibrationActorTest extends TestKit(ActorSystem("CalibrationActorTest")) with WordSpecLike with MustMatchers with BeforeAndAfterAll {
  val systemId = "A4"
  val gantryId = "E1"
  val laneIdR1 = Lane(
    laneId = "A4-E1-E1R1",
    name = "R1",
    gantry = gantryId,
    system = systemId,
    sensorGPS_longitude = 52.4,
    sensorGPS_latitude = 5.4)

  val laneIdR2 = Lane(
    laneId = "A4-E1-E1R2",
    name = "R2",
    gantry = gantryId,
    system = systemId,
    sensorGPS_longitude = 52.4,
    sensorGPS_latitude = 5.4)

  val currentTime = System.currentTimeMillis()

  val testImage = Resources.getResourceFile("test_image.tif")
  val workingDir = new File(testImage.getParentFile, "work")

  val imageFunctions: ImageFunctions = NativeImageFunctions

  override protected def beforeAll() {
    super.beforeAll()
    if (!workingDir.exists())
      workingDir.mkdirs()
  }

  override protected def afterAll() {
    workingDir.delete()
    system.shutdown()
    super.afterAll()
  }

  "CalibrationActor" must {

    "perform calibration with happy flow" in {
      ImageFunctionsFactory.setJpgFunctions(new JpgFunctionsMock(false))
      val producer = TestProbe()
      val cameraR1 = TestProbe()
      val cameraR2 = TestProbe()
      val monitors = Seq(
        StartedJaiMonitor(laneIdR1, cameraR1.ref, None),
        StartedJaiMonitor(laneIdR2, cameraR2.ref, None))

      val config = SelfTestConfiguration(
        imageTimeout = 2.seconds,
        timeout = 5.minutes,
        quality = 80,
        workingDirectory = Some(workingDir.getAbsolutePath),
        maxTimeDiffMillis = None,
        profiles = Map.empty)

      val calibration = system.actorOf(Props(new CalibrationActor(
        startedJaiMonitor = monitors,
        targetActor = producer.ref,
        selfTestConfiguration = config,
        imageFunctions)))

      val calReq = GantryCalibrationRequest(systemId, gantryId, currentTime, Seq(laneIdR1.laneId, laneIdR2.laneId))
      calibration ! calReq
      //first step send image Requests to monitors
      cameraR1.expectMsg(JaiImageRequest)
      cameraR2.expectMsg(JaiImageRequest)
      //send responses
      val jaiResp1 = JaiImageResponse(laneIdR1, "Image1".getBytes, None)
      cameraR1.send(calibration, jaiResp1)
      val jaiResp2 = JaiImageResponse(laneIdR2, "Image2".getBytes, None)
      cameraR2.send(calibration, jaiResp2)
      //expect the calibration response
      val response = producer.expectMsgType[GantryCalibrationResponse](500.millis)
      response.systemId must be(calReq.systemId)
      response.gantryId must be(calReq.gantryId)
      response.time must be(calReq.time)
      response.images.size must be(2)
      response.images.head.image must be(jaiResp1.image.map(_.toInt).toSeq)
      response.images.last.image must be(jaiResp2.image.map(_.toInt).toSeq)
      response.results.size must be(2)
      response.results.head must be(StepResult("camera-image", true, None))
      response.results.last must be(StepResult("image-conversion", true, None))
      system.stop(calibration)
    }
    "retry calibration image when imagerrequests fails" in {
      ImageFunctionsFactory.setJpgFunctions(new JpgFunctionsMock(false))
      val producer = TestProbe()
      val cameraR1 = TestProbe()
      val cameraR2 = TestProbe()
      val monitors = Seq(
        StartedJaiMonitor(laneIdR1, cameraR1.ref, None),
        StartedJaiMonitor(laneIdR2, cameraR2.ref, None))

      val config = SelfTestConfiguration(
        imageTimeout = 500.millis, //small retry
        timeout = 5.minutes,
        quality = 80,
        workingDirectory = Some(workingDir.getAbsolutePath),
        maxTimeDiffMillis = None,
        profiles = Map.empty)

      val calibration = system.actorOf(Props(new CalibrationActor(
        startedJaiMonitor = monitors,
        targetActor = producer.ref,
        selfTestConfiguration = config,
        imageFunctions)))

      val calReq = GantryCalibrationRequest(systemId, gantryId, currentTime, Seq(laneIdR1.laneId, laneIdR2.laneId))
      calibration ! calReq
      //first step send image Requests to monitors
      cameraR1.expectMsg(JaiImageRequest)
      cameraR2.expectMsg(JaiImageRequest)
      //send responses
      val jaiResp1 = JaiImageResponse(laneIdR1, "Image1".getBytes, None)
      cameraR1.send(calibration, jaiResp1)
      //expect retransmission for lane2

      cameraR2.expectMsg(JaiImageRequest)
      cameraR1.expectNoMsg(100.millis)

      val jaiResp2 = JaiImageResponse(laneIdR2, "Image2".getBytes, None)
      cameraR2.send(calibration, jaiResp2)

      //expect the calibration response
      val response = producer.expectMsgType[GantryCalibrationResponse](500.millis)
      response.systemId must be(calReq.systemId)
      response.gantryId must be(calReq.gantryId)
      response.time must be(calReq.time)
      response.images.size must be(2)
      response.images.head.image must be(jaiResp1.image.map(_.toInt).toSeq)
      response.images.last.image must be(jaiResp2.image.map(_.toInt).toSeq)
      response.results.size must be(2)
      response.results.head must be(StepResult("camera-image", true, None))
      response.results.last must be(StepResult("image-conversion", true, None))
      system.stop(calibration)
    }
    "send response after calibrationTimeout" in {
      ImageFunctionsFactory.setJpgFunctions(new JpgFunctionsMock(false))
      val producer = TestProbe()
      val cameraR1 = TestProbe()
      val cameraR2 = TestProbe()
      val monitors = Seq(
        StartedJaiMonitor(laneIdR1, cameraR1.ref, None),
        StartedJaiMonitor(laneIdR2, cameraR2.ref, None))

      val config = SelfTestConfiguration(
        imageTimeout = 2.seconds,
        timeout = 500.millis, //small timeout
        quality = 80,
        workingDirectory = Some(workingDir.getAbsolutePath),
        maxTimeDiffMillis = None,
        profiles = Map.empty)

      val calibration = system.actorOf(Props(new CalibrationActor(
        startedJaiMonitor = monitors,
        targetActor = producer.ref,
        selfTestConfiguration = config,
        imageFunctions)))

      val calReq = GantryCalibrationRequest(systemId, gantryId, currentTime, Seq(laneIdR1.laneId, laneIdR2.laneId))
      calibration ! calReq
      //first step send image Requests to monitors
      cameraR1.expectMsg(JaiImageRequest)
      cameraR2.expectMsg(JaiImageRequest)
      //don't send responses

      //expect the calibration response
      val response = producer.expectMsgType[GantryCalibrationResponse](1.second)
      response.systemId must be(calReq.systemId)
      response.gantryId must be(calReq.gantryId)
      response.time must be(calReq.time)
      response.images.size must be(0)
      response.results.size must be(2)
      response.results.head must be(StepResult("camera-image", false, Some("Failed to get image for lanes: A4-E1-E1R1, A4-E1-E1R2")))
      response.results.last must be(StepResult("image-conversion", false, Some("Conversion skipped")))
      system.stop(calibration)
    }

  }
}