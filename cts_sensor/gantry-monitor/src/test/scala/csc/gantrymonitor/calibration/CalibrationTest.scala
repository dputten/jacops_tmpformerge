/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.gantrymonitor.calibration

import java.io.File
import java.util.concurrent.TimeUnit

import scala.concurrent.duration._
import csc.calibration.{ GantryCalibrationRequest, GantryCalibrationResponse, StepResult }
import csc.gantrymonitor.config.SelfTestConfiguration
import csc.gantrymonitor.jai.JaiImageResponse
import csc.vehicle.message.Lane
import imagefunctions.jpg.JpgFunctions
import imagefunctions.{ ImageFunctionsException, ImageFunctionsFactory }
import org.apache.commons.io.FileUtils
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import testframe.Resources

class CalibrationTest extends WordSpec with MustMatchers with BeforeAndAfterAll {
  val systemId = "A4"
  val gantryId = "E1"
  val laneIdR1 = "A4-E1-E1R1"
  val laneIdR2 = "A4-E1-E1R2"
  val laneIdR3 = "A4-E1-E1R3"

  val currentTime = System.currentTimeMillis()

  val testImage = Resources.getResourceFile("test_image.tif")
  val workingDir = new File(testImage.getParentFile, "work")

  val timeout = FiniteDuration(1, TimeUnit.SECONDS)
  val config = SelfTestConfiguration(timeout, timeout, 80, Some(workingDir.getAbsolutePath), None, Map.empty)

  val imageFunctions: ImageFunctions = NativeImageFunctions

  override protected def beforeAll() {
    super.beforeAll()
    if (!workingDir.exists())
      workingDir.mkdirs()
  }

  override protected def afterAll() {
    super.afterAll()
    workingDir.delete()
  }

  def createCalibration(lanes: Seq[String]): Calibration =
    new Calibration(GantryCalibrationRequest(systemId, gantryId, currentTime, lanes), config, imageFunctions)

  "getLaneIdsForImageRequests" must {

    "get lanes for image requests" in {
      val lanes = Seq(laneIdR1, laneIdR2, laneIdR3)
      val calibration = createCalibration(lanes)
      calibration.getLaneIdsForImageRequests must be(lanes)
    }
    "get lanes for image requests when responses are received" in {
      val lanes = Seq(laneIdR1, laneIdR2, laneIdR3)
      val calibration = createCalibration(lanes)
      val imageResponse1 = JaiImageResponse(
        camera = Lane(laneId = laneIdR1,
          name = "R1",
          gantry = gantryId,
          system = systemId,
          sensorGPS_longitude = 52.4,
          sensorGPS_latitude = 5.4,
          bpsLaneId = None),
        image = "image1".getBytes(), time = None)
      calibration.receivedImageResponse(imageResponse1)

      calibration.getLaneIdsForImageRequests must be(Seq(laneIdR2, laneIdR3))
    }
    "get no lanes for image requests when all responses are received" in {
      val lanes = Seq(laneIdR1, laneIdR2)
      val calibration = createCalibration(lanes)
      val imageResponse1 = JaiImageResponse(
        camera = Lane(laneId = laneIdR1,
          name = "R1",
          gantry = gantryId,
          system = systemId,
          sensorGPS_longitude = 52.4,
          sensorGPS_latitude = 5.4,
          bpsLaneId = None),
        image = "image1".getBytes(), time = None)
      calibration.receivedImageResponse(imageResponse1)
      val imageResponse2 = JaiImageResponse(
        camera = Lane(laneId = laneIdR2,
          name = "R2",
          gantry = gantryId,
          system = systemId,
          sensorGPS_longitude = 52.4,
          sensorGPS_latitude = 5.4,
          bpsLaneId = None),
        image = "image2".getBytes(), time = None)
      calibration.receivedImageResponse(imageResponse2)

      calibration.getLaneIdsForImageRequests must be(Seq())
    }
  }
  "isFinished" must {
    "return false when not finished" in {
      val lanes = Seq(laneIdR1, laneIdR2, laneIdR3)
      val calibration = createCalibration(lanes)
      calibration.isFinished() must be(false)
    }
    "return false when not finished when receiving double responses" in {
      val lanes = Seq(laneIdR1, laneIdR2)
      val calibration = createCalibration(lanes)
      val imageResponse1 = JaiImageResponse(
        camera = Lane(laneId = laneIdR1,
          name = "R1",
          gantry = gantryId,
          system = systemId,
          sensorGPS_longitude = 52.4,
          sensorGPS_latitude = 5.4,
          bpsLaneId = None),
        image = "image1".getBytes(), time = None)
      calibration.receivedImageResponse(imageResponse1)
      //send response double
      calibration.receivedImageResponse(imageResponse1)

      calibration.isFinished() must be(false)
    }
    "return false when not finished when receiving unexpected responses" in {
      val lanes = Seq(laneIdR1, laneIdR2)
      val calibration = createCalibration(lanes)
      val imageResponse1 = JaiImageResponse(
        camera = Lane(laneId = laneIdR1,
          name = "R1",
          gantry = gantryId,
          system = systemId,
          sensorGPS_longitude = 52.4,
          sensorGPS_latitude = 5.4,
          bpsLaneId = None),
        image = "image1".getBytes(), time = None)
      calibration.receivedImageResponse(imageResponse1)
      //send unexpected response
      val imageResponse2 = JaiImageResponse(
        camera = Lane(laneId = laneIdR3,
          name = "R3",
          gantry = gantryId,
          system = systemId,
          sensorGPS_longitude = 52.4,
          sensorGPS_latitude = 5.4,
          bpsLaneId = None),
        image = "image3".getBytes(), time = None)
      calibration.receivedImageResponse(imageResponse2)

      calibration.isFinished() must be(false)
    }
    "return true when finished" in {
      val lanes = Seq(laneIdR1, laneIdR2)
      val calibration = createCalibration(lanes)
      val imageResponse1 = JaiImageResponse(
        camera = Lane(laneId = laneIdR1,
          name = "R1",
          gantry = gantryId,
          system = systemId,
          sensorGPS_longitude = 52.4,
          sensorGPS_latitude = 5.4,
          bpsLaneId = None),
        image = "image1".getBytes(), time = None)
      calibration.receivedImageResponse(imageResponse1)
      //send unexpected response
      val imageResponse2 = JaiImageResponse(
        camera = Lane(laneId = laneIdR2,
          name = "R2",
          gantry = gantryId,
          system = systemId,
          sensorGPS_longitude = 52.4,
          sensorGPS_latitude = 5.4,
          bpsLaneId = None),
        image = "image2".getBytes(), time = None)
      calibration.receivedImageResponse(imageResponse2)

      calibration.isFinished() must be(true)
    }
  }

  "createResponse" must {
    "create error when no images are received" in {
      val lanes = Seq(laneIdR1, laneIdR2)
      val req = GantryCalibrationRequest(systemId, gantryId, currentTime, lanes)
      val calibration = new Calibration(req, config, imageFunctions)
      val response = calibration.createResponse(currentTime + 1.minute.toMillis).asInstanceOf[GantryCalibrationResponse]
      response.systemId must be(req.systemId)
      response.gantryId must be(req.gantryId)
      response.time must be(req.time)
      response.results.size must be(2)
      response.results.head must be(StepResult("camera-image", false, Some("Failed to get image for lanes: A4-E1-E1R1, A4-E1-E1R2")))
      response.results.last must be(StepResult("image-conversion", false, Some("Conversion skipped")))
    }
    "create error when some images are received" in {
      val lanes = Seq(laneIdR1, laneIdR2)
      val req = GantryCalibrationRequest(systemId, gantryId, currentTime, lanes)
      val calibration = new Calibration(req, config, imageFunctions)
      val imageResponse1 = JaiImageResponse(
        camera = Lane(laneId = laneIdR1,
          name = "R1",
          gantry = gantryId,
          system = systemId,
          sensorGPS_longitude = 52.4,
          sensorGPS_latitude = 5.4,
          bpsLaneId = None),
        image = "image1".getBytes(), time = None)
      calibration.receivedImageResponse(imageResponse1)

      val response = calibration.createResponse(currentTime + 1.minute.toMillis).asInstanceOf[GantryCalibrationResponse]
      response.systemId must be(req.systemId)
      response.gantryId must be(req.gantryId)
      response.time must be(req.time)
      response.images must be(Seq())
      response.results.size must be(2)
      response.results.head must be(StepResult("camera-image", false, Some("Failed to get image for lanes: A4-E1-E1R2")))
      response.results.last must be(StepResult("image-conversion", false, Some("Conversion skipped")))
    }
    "create error when conversion failed" in {
      ImageFunctionsFactory.setJpgFunctions(new JpgFunctionsMock(true))
      val lanes = Seq(laneIdR1)
      val req = GantryCalibrationRequest(systemId, gantryId, currentTime, lanes)
      val calibration = new Calibration(req, config, imageFunctions)
      val imageResponse1 = JaiImageResponse(
        camera = Lane(laneId = laneIdR1,
          name = "R1",
          gantry = gantryId,
          system = systemId,
          sensorGPS_longitude = 52.4,
          sensorGPS_latitude = 5.4,
          bpsLaneId = None),
        image = "image1".getBytes(), time = None)
      calibration.receivedImageResponse(imageResponse1)

      val response = calibration.createResponse(currentTime + 1.minute.toMillis).asInstanceOf[GantryCalibrationResponse]
      response.systemId must be(req.systemId)
      response.gantryId must be(req.gantryId)
      response.time must be(req.time)
      response.images must be(Seq())
      response.results.size must be(2)
      response.results.head must be(StepResult("camera-image", true, None))
      val step = response.results.last
      step.stepName must be("image-conversion")
      step.success must be(false)
      step.detail.get must startWith("JPG Conversion failed for lane A4-E1-E1R1")
    }
    "create success when all images are received" in {
      ImageFunctionsFactory.setJpgFunctions(new JpgFunctionsMock(false))
      val lanes = Seq(laneIdR1)
      val req = GantryCalibrationRequest(systemId, gantryId, currentTime, lanes)
      val calibration = new Calibration(req, config, imageFunctions)
      val image = FileUtils.readFileToByteArray(testImage)
      val imageResponse1 = JaiImageResponse(
        camera = Lane(laneId = laneIdR1,
          name = "R1",
          gantry = gantryId,
          system = systemId,
          sensorGPS_longitude = 52.4,
          sensorGPS_latitude = 5.4,
          bpsLaneId = None),
        image = "image1".getBytes(), time = None)
      calibration.receivedImageResponse(imageResponse1)

      val response = calibration.createResponse(currentTime + 1.minute.toMillis).asInstanceOf[GantryCalibrationResponse]
      response.systemId must be(req.systemId)
      response.gantryId must be(req.gantryId)
      response.time must be(req.time)
      response.images.size must be(1)
      response.images.head.image must be(imageResponse1.image.map(_.toInt).toSeq)
      response.results.size must be(2)
      response.results.head must be(StepResult("camera-image", true, None))
      response.results.last must be(StepResult("image-conversion", true, None))
    }
  }

  //private def createImageResponse(lane: Lane, image: Array[Byte], time: Option[Date]) = JaiImageResponse(lane, image, time)
}

class JpgFunctionsMock(error: Boolean) extends JpgFunctions {
  def rgbToJpg(p1: String, p2: Int, p3: Int, p4: Int, p5: String) {}

  def grayToJpg(p1: String, p2: Int, p3: Int, p4: Int, p5: String) {}

  def bayerToJpg(source: String, resizeX: Int, resizeY: Int, quality: Int, dest: String) {
    if (error) {
      throw new ImageFunctionsException(1, "Failed to convert")
    }
    FileUtils.copyFile(new File(source), new File(dest))
  }

  def jpgToJpg(p1: String, p2: Int, p3: Int, p4: Int, p5: String) {}
}
