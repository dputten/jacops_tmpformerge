package csc.gantrymonitor.selftest

import com.typesafe.config.{ ConfigFactory, Config }
import csc.gantrymonitor.selftest.Selftest.ServiceChecks
import org.scalatest.WordSpec
import org.scalatest._

import scala.collection.JavaConversions

/**
 * Created by carlos on 24/08/16.
 */
class ServiceAuditTest
  extends WordSpec
  with MustMatchers
  with ServiceAuditTestData
  with CommonAuditTestVerifier {

  "ServiceAudit" must {

    "return the proper response for a service running" in {
      executeAndVerify(List("registration"), true)
    }

    "return the proper response for a service not running" in {
      executeAndVerify(List("recognize-processor"), false)
    }

    "return the proper response when service status command fails with errCode != 0" in {
      executeAndVerify(List("fail"), false)
    }

  }

  def executeAndVerify(services: List[String],
                       expectValid: Boolean): Unit = {

    val plan = createServicePlan(services)
    val audit = new ServiceAudit(plan)
    audit.execute() match {
      case Right(components) ⇒ verify(plan, expectValid, components)
      case Left(error)       ⇒ fail("unexpected: " + error)
    }
  }

}

trait ServiceAuditTestData extends MustMatchers {

  val allServices = List("apache_ftpd", "vehicle-registration", "recognize-processor", "image-store", "sensor-input-events")

  val statusCommandParts = List("gantry-monitor/src/test/resources/dummyService.sh", "_", "status")

  def serviceConfigs(statusCommandParts: List[String]): Map[String, Config] = {
    val map: Map[String, AnyRef] =
      Map(ServiceAudit.statusCommandPatternKey -> JavaConversions.seqAsJavaList(statusCommandParts))
    val settings = ConfigFactory.parseMap(JavaConversions.mapAsJavaMap(map))
    Map(Selftest.settingsKey -> settings)
  }

  def createServicePlan(services: List[String], commandParts: List[String] = statusCommandParts): SelftestPlan =
    SelftestPlan(ComponentType.service, services, ServiceChecks.all, serviceConfigs(commandParts))

}
