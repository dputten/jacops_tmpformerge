package csc.gantrymonitor.selftest

import java.util.concurrent.TimeUnit

import akka.actor.{ ActorRef, Props }
import scala.concurrent.duration.FiniteDuration
import com.typesafe.config.ConfigFactory
import csc.gantry.config.CameraConfig
import csc.gantrymonitor.calibration.CameraFunctionsStub
import csc.gantrymonitor.caseclasses.StartedJaiMonitor
import csc.gantrymonitor.jai.JaiMonitor
import csc.gantrymonitor.selftest.Selftest.CameraChecks
import csc.gantrymonitor.stub.JaiCameraStub
import csc.util.test.UniqueIntGenerator
import csc.vehicle.message.Camera
import org.scalatest._
import _root_.tools.jaiconnection.SimulateJai.ImageSupplier

/**
 * Created by carlos on 04/08/16.
 */
class SelftestWithStubCameraTest extends AbstractSelftestTest("SelftestWithStubCameraTest") {

  import SelftestWithStubCameraTest._
  import SelftestActorTest._

  "JaiCameraStub" must {

    "reply accordingly during a selftest, happy case" in {
      testScenario(0, true, true)
    }

    //testing more scenarios will require a big refactoring on the test, because of port clashing
    //    "reply accordingly during a selftest, image time exceeded" in {
    //      testScenario(maxTimeDiffMillis + 1, true, false)
    //    }
  }

  private def testScenario(imageDelay: Long, expectImage: Boolean, validTime: Boolean): Unit = {
    val stub = createCameraStub(imageDelay) //camera stub: manages the simulated cameras
    val monitors = launchMonitors()

    val ownConfig = config.copy(profiles = Map(defaultProfileId -> cameraOnlyProfile)) //only interested in camera
    val propsFactory = new GenericSelftestProps(ownConfig, "notused", monitors, CameraFunctionsStub)

    val actor = system.actorOf(propsFactory.actorProps(probe.ref))

    val req = createRequest(gantryE, defaultProfileId)
    actor ! req

    val res = getResponse(req)
    res.components.size must be(1)
    verifyComponent(res.resultFor(cameraE1.id), expectImage, validTime)
  }

  def createCameraStub(imageDelay: Long): ActorRef = {
    val imageSupplier: ImageSupplier = createImage(imageDelay)
    JaiCameraStub(system, uri, cameraConfigMap.values.toList, Some(imageSupplier))
  }

  def createImage(imageDelay: Long)(): Option[Array[Byte]] = {
    val imageTime = System.currentTimeMillis() - imageDelay //introduces a fake delay, by making the image 'older'
    Some(CameraFunctionsStub.createImageContents(imageTime))
  }

  def launchMonitors(): Seq[StartedJaiMonitor] = {
    val timeout = FiniteDuration(1, TimeUnit.SECONDS)
    cameras map {
      case cam ⇒
        val name = cam.id + "-camera-" + nextUnique
        val config = cameraConfigMap(cam.id)
        val props = Props(new JaiMonitor(cam, config, JaiMonitor.cameraTriggerId, timeout, 1, timeout))
        val actor = system.actorOf(props, name)
        StartedJaiMonitor(cam, actor, None)
    }
  }

}

object SelftestWithStubCameraTest extends MustMatchers {

  val portGenerator = new UniqueIntGenerator(9000)

  val configString =
    """{
      |akka.loggers = ["akka.event.Logging$DefaultLogger"]
      |akka.loglevel = "DEBUG"
      |akka.stdout-loglevel = "DEBUG"
      |akka.debug.lifecycle = on
      |akka.debug.receive = on
      |akka.debug.autoreceive = on
      |akka.debug.unhandled = on
      |}""".stripMargin

  val sysConfig = ConfigFactory.parseString(configString)

  def uri = "mina:tcp://%s:%s?textline=true&sync=false".format("localhost", nextPort)

  val cameras: Seq[Camera] = Seq(SelftestActorTest.cameraE1)

  def nextPort: Int = portGenerator.nextInt

  val cameraConfigMap: Map[String, CameraConfig] = cameras.zipWithIndex.map {
    case (cam, idx) ⇒ cam.id -> CameraConfig("/tmp", "localhost", nextPort, 1, 2000)
  }.toMap

  def verifyComponent(component: Option[Component], expectImage: Boolean, timeValid: Boolean): Unit = {
    component match {
      case Some(comp) ⇒
        val expectValid = expectImage && timeValid
        comp.checks.size must be(2)
        comp.checks(0) must be(Check(CameraChecks.cameraImage, expectImage))
        comp.checks(1) must be(Check(CameraChecks.imageTime, timeValid))
        comp.valid must be(expectValid)
      case None ⇒ fail("Missing component")
    }
  }

}