package csc.gantrymonitor.selftest

import com.typesafe.config.{ Config, ConfigFactory }
import csc.akka.process.{ ExecuteScript, ExecuteScriptResult, ScriptExecutor }
import csc.gantrymonitor.selftest.Selftest.{ ClockChecks, ClockProperties }
import org.scalatest.WordSpec
import org.scalatest._

import scala.collection.JavaConversions

/**
 * Created by carlos on 25/08/16.
 */
class ClockAuditTest
  extends WordSpec
  with MustMatchers
  with CommonAuditTestVerifier
  with ClockAuditTestData {

  "ClockAudit" must {

    "return the proper response for ntp with valid offset" in {
      executeAndVerify(NTPPeerResult("", "", "", "", "", "1000", "", (ntpOffsetThreshold - 1).toString), true)
    }

    "return the proper response for ntp with invalid offset (too high)" in {
      executeAndVerify(NTPPeerResult("", "", "", "", "", "900", "", (ntpOffsetThreshold + 1).toString), false)
    }
  }

  def executeAndVerify(peer: NTPPeerResult, expectValid: Boolean): Unit = {
    val audit = createClockAudit(peer)
    audit.execute() match {
      case Right(components) ⇒
        val map = verify(clockPlan, expectValid, components, ClockProperties.all, ClockAudit.allItems)
        val props = propsMap(map(ClockAudit.ntp).properties)
        props.get(ClockProperties.offset) must be(Some(peer.offset))
        props.get(ClockProperties.poll) must be(Some(peer.poll))
      case Left(error) ⇒ fail("unexpected: " + error)
    }
  }

}

trait ClockAuditTestData extends MustMatchers {

  lazy val ntpqCommandParts = List("ntpq", "-p")

  val ntpOffsetThreshold: Long = 300

  lazy val clockPlan = SelftestPlan(ComponentType.clock, Nil, ClockChecks.all, clockConfigs)

  def clockConfigs: Map[String, Config] = {
    val map: Map[String, AnyRef] =
      Map(ClockAudit.ntpqCommandPartsKey -> JavaConversions.seqAsJavaList(ntpqCommandParts),
        ClockAudit.offsetThresholdKey -> java.lang.Long.valueOf(ntpOffsetThreshold))
    val settings = ConfigFactory.parseMap(JavaConversions.mapAsJavaMap(map))
    Map(Selftest.settingsKey -> settings)
  }

  def createClockAudit(activePeer: NTPPeerResult): ClockAudit = new ClockAudit(clockPlan) {

    //overrides the active peer, so we can make sure the results are predicable, but makes sure the script works
    override protected def ntpActivePeer(peers: Seq[NTPPeerResult]): Option[NTPPeerResult] = {
      if (peers.isEmpty) fail("Script returned no peers")
      Some(activePeer)
    }

    override protected def executeScript(script: ExecuteScript): ExecuteScriptResult = {
      ExecuteScriptResult(script = script,
        resultCode = Right(0),
        stdOut = Seq(
          "     remote           refid      st t when poll reach   delay   offset  jitter",
          "==============================================================================",
          "*ntp.ram.nl      213.136.0.252    2 u  553 1024  377   39.814  -26.744  21.854",
          "+ntp2.ram.nl     213.154.236.182  3 u  485 1024  377   78.172   -5.621  13.610",
          "x192.168.1.129   0.0.0.0          2 u  539 1024  377   84.124  257.077  45.249",
          "x192.168.1.137   0.0.0.0          2 u  273 1024  377   91.425  142.411  62.974",
          " LOCAL(1)        .LOCL.          12 l  46h   64    0    0.000    0.000   0.000"),
        stdError = Seq())
    }

  }

}
