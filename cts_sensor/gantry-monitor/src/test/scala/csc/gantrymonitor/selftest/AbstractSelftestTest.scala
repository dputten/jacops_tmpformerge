package csc.gantrymonitor.selftest

import java.util.concurrent.TimeUnit

import akka.actor.{ ActorRef, ActorSystem }
import akka.testkit.{ TestProbe, TestKit }
import csc.gantrymonitor.config.SelfTestConfiguration
import csc.gantrymonitor.selftest.ComponentType._
import csc.gantrymonitor.selftest.Selftest.{ StorageChecks, CameraChecks }
import csc.gantrymonitor.selftest.SelftestActor._
import csc.util.test.UniqueGenerator
import csc.vehicle.message.Camera
import org.scalatest._

import scala.concurrent.duration.{ FiniteDuration, Duration }

/**
 * Created by carlos on 22/08/16.
 */
abstract class AbstractSelftestTest(name: String)
  extends TestKit(ActorSystem(name))
  with WordSpecLike
  with MustMatchers
  with BeforeAndAfterAll
  with BeforeAndAfterEach
  with SelftestVerifier {

  var probe: TestProbe = null
  var actor: ActorRef = null

  override protected def beforeEach(): Unit = {
    probe = TestProbe()
  }

  override protected def afterAll(): Unit = {
    system.shutdown()
  }

}

trait SelftestVerifier extends MustMatchers with SelftestTestData {

  def probe: TestProbe
  def timeout: FiniteDuration

  def getResponse(req: SelftestRequest)(implicit timeout: FiniteDuration): SelftestResponse = {
    val res = probe.expectMsgType[SelftestResponse](timeout)
    //println(res)
    res.correlationId must be(req.correlationId)
    res.system must be(req.system)
    res.gantry must be(req.gantry)
    res.profile must be(req.profile)
    res
  }

  def getComponentResponse(req: ComponentSelftestRequest)(implicit timeout: FiniteDuration): ComponentSelftestResponse = {
    val res = probe.expectMsgType[ComponentSelftestResponse](timeout)
    //println(res)
    res.id must be(req.id)
    res.componentType must be(req.plan.componentType)
    res
  }

}

trait SelftestTestData
  extends UniqueGenerator
  with StorageAuditTestData
  with ServiceAuditTestData
  with ClockAuditTestData
  with QueueAdminTestData
  with FtpAuditTestData {

  val defaultProfileId = "default"

  val systemId = "A4"
  val gantryE = "E1"
  val gantryX = "X1"

  val cameraE1 = Camera("camE1", gantryE, systemId)
  val cameraE2 = Camera("camE2", gantryE, systemId)
  val cameraE3 = Camera("camE3", gantryE, systemId)
  val cameraX1 = Camera("camX1", gantryX, systemId)

  val maxTimeDiffMillis = 100
  def timeoutMillis: Long = timeout.toMillis //1000
  implicit val timeout = Duration(1000, TimeUnit.MILLISECONDS)

  val cameraPlan = SelftestPlan(ComponentType.camera, Nil, CameraChecks.selftest, Map.empty)
  val storagePlan = storageSelftestPlan(List("/app", "/data"), StorageChecks.all, Map.empty, Some("10GB"))
  val servicePlan = createServicePlan(allServices, List("/usr/bin/service", "_", "status"))
  val queuePlan = createQueuePlan("http://ctes:ctes@localhost:15672")
  val ftpPlan = createFtpPlan("/app/apache-ftpserver/res/log/ftpd.log", 30)

  val cameraOnlyProfile = SelftestProfile(Map(ComponentType.camera -> cameraPlan))

  val defaultPlans: List[SelftestPlan] = List(cameraPlan, storagePlan, servicePlan, clockPlan, queuePlan, ftpPlan)
  val defaultPlanMap = defaultPlans.map { p ⇒ p.componentType -> p }.toMap
  val defaultProfile = SelftestProfile(defaultPlanMap)

  val profiles: Map[String, SelftestProfile] = Map(defaultProfileId -> defaultProfile)

  val config: SelfTestConfiguration = SelfTestConfiguration(timeout, timeout, 80, None, Some(maxTimeDiffMillis), profiles)

  def createRequest(gantry: String, profile: String) =
    SelftestRequest(nextUnique, systemId, gantry, System.currentTimeMillis(), profile)

  def createComponentRequest(gantry: String, plan: SelftestPlan)(implicit cp: ComponentType): ComponentSelftestRequest = {
    val id = SelftestId(nextUnique, systemId, gantry, defaultProfileId)
    ComponentSelftestRequest(id, plan, now)
  }

}

trait CommonAuditTestVerifier extends MustMatchers {

  /**
   * @param plan
   * @param expectValid
   * @param components actual components
   * @param expectedProps expected property keys (for all components)
   * @param overrideItems not Nil to override the items (not derived from plan in this case)
   * @return component map
   */
  def verify(plan: SelftestPlan,
             expectValid: Boolean,
             components: List[Component],
             expectedProps: List[String] = Nil,
             overrideItems: List[String] = Nil): Map[String, Component] = {
    val map = componentMap(components)
    verifyComponents(plan, map, expectValid, expectedProps, overrideItems)
    map
  }

  /**
   * verifies that:
   * -components are the expected
   * -valid is the expected (must all be the same)
   * -property keys are the expected
   *
   * @param plan
   * @param componentMap
   * @param expectValid
   * @param expectedProps
   * @param overrideItems
   */
  def verifyComponents(plan: SelftestPlan,
                       componentMap: Map[String, Component],
                       expectValid: Boolean,
                       expectedProps: List[String],
                       overrideItems: List[String]): Unit = {

    val items = if (overrideItems.size > 0) overrideItems else plan.items //items might be hardcoded (not configured)
    componentMap.keySet must be(items.toSet)

    componentMap.values.foreach { c ⇒
      val checkNames = c.checks map (_.name)
      checkNames must be(plan.checks) //order is relevant

      val propNames = c.properties.map(_.name)
      propNames.toSet must be(expectedProps.toSet) //order is irrelevant

      c.valid must be(expectValid)
    }
  }

  def componentMap(list: List[Component]): Map[String, Component] = list map { e ⇒ e.name -> e } toMap

  def propsMap(list: List[Property]): Map[String, String] = list.map(p ⇒ p.name -> p.value).toMap

}
