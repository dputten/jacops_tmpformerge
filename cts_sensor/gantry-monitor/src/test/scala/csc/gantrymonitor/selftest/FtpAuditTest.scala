package csc.gantrymonitor.selftest

import java.io.{ FileWriter, BufferedWriter, File }
import java.util.{ Date, Calendar, GregorianCalendar }

import com.typesafe.config.{ ConfigFactory, Config }
import csc.gantrymonitor.selftest.Selftest.{ FtpProperties, FtpChecks }
import csc.util.test.UniqueGenerator
import org.scalatest.WordSpec
import org.scalatest._

import scala.collection.JavaConversions

/**
 * Created by carlos on 30/08/16.
 */
class FtpAuditTest
  extends WordSpec
  with MustMatchers
  with UniqueGenerator
  with CommonAuditTestVerifier
  with FtpAuditTestData {

  val thresholdMinutes = 10

  "FtpAudit" must {

    "return the proper selftest result when ftp shows recent activity " in {
      executeAndVerify(Some(0), true)
    }

    "return the proper selftest result when ftp does not show recent activity " in {
      executeAndVerify(Some(thresholdMinutes + 1), false)
    }

    "return the proper selftest result when ftp log does not have any parseable activity" in {
      executeAndVerify(None, false)
    }

  }

  def executeAndVerify(activityMinutesAgo: Option[Int], expectValid: Boolean): Unit = {
    val lastActivity = activityMinutesAgo map (minutesAgo(_))
    val logPath = createDummyLogFile(lastActivity).getAbsolutePath
    val plan = createFtpPlan(logPath, thresholdMinutes)
    val audit = new FtpAudit(plan)

    val expectedActivity = lastActivity match {
      case None       ⇒ Selftest.unknownValue
      case Some(date) ⇒ FtpAudit.dateFormat.format(date)
    }

    audit.execute() match {
      case Right(components) ⇒
        val map = verify(plan, expectValid, components, FtpProperties.all, FtpAudit.ftpItems)
        val props = propsMap(map(FtpAudit.ftp).properties)
        props.get(FtpProperties.lastActivity) must be(Some(expectedActivity))
      case Left(error) ⇒ fail("unexpected: " + error)
    }
  }

  def minutesAgo(value: Int): Date = {
    val gc = new GregorianCalendar()
    gc.add(Calendar.MINUTE, -value)
    gc.getTime
  }

  def createDummyLogFile(lastActivity: Option[Date]): File = {
    val file = File.createTempFile("FtpAuditTest-", nextUnique)
    val writer = new BufferedWriter(new FileWriter(file))

    try {
      lastActivity foreach { date ⇒
        val time = FtpAudit.dateFormat.format(date)
        val line = """[ INFO] %s [ctes] [192.168.1.139] SENT: 226 Transfer complete.""".format(time)
        writer.write("just some test line without a parseable date")
        writer.newLine()
        writer.write(line)
        writer.newLine()
      }
    } finally {
      writer.flush()
      writer.close()
    }

    file.deleteOnExit()
    file
  }

}

trait FtpAuditTestData {

  def createFtpPlan(logPath: String, activityThreshold: Int): SelftestPlan =
    SelftestPlan(ComponentType.ftp, Nil, FtpChecks.all, ftpConfigs(logPath, activityThreshold))

  def ftpConfigs(logPath: String, activityThreshold: Int): Map[String, Config] = {
    val map: Map[String, AnyRef] =
      Map(FtpAudit.logFilePathKey -> logPath,
        FtpAudit.activityThresholdMinutesKey -> java.lang.Long.valueOf(activityThreshold))
    val settings = ConfigFactory.parseMap(JavaConversions.mapAsJavaMap(map))
    Map(Selftest.settingsKey -> settings)
  }

}
