package csc.gantrymonitor.selftest

import java.util.Date
import java.util.concurrent.TimeUnit

import akka.actor.{ Props, ActorRef, Actor }
import scala.concurrent.duration.Duration
import csc.gantrymonitor.calibration.CameraFunctionsStub
import csc.gantrymonitor.caseclasses.StartedJaiMonitor
import csc.gantrymonitor.jai.{ JaiImageResponse, JaiImageRequest }
import csc.gantrymonitor.selftest.Selftest.CameraChecks
import csc.vehicle.message.{ Camera, CameraUnit }

/**
 * Created by carlos on 22/08/16.
 */
class CameraSelftesterTest extends AbstractSelftestTest("CameraSelftesterTest") {

  implicit val executor = system.dispatcher
  implicit val componentType = ComponentType.camera
  val plan = cameraPlan

  import SelftestActorTest._

  "CameraSelftester" must {

    "send back an empty response when no monitors exist" in {
      val actor = createActor()
      val req = createComponentRequest(gantryE, plan)

      probe.send(actor, req)

      val res = getComponentResponse(req)
      res.errorMessage.isDefined must be(true)
      res.results must be(Nil)
    }

    "send back a response for a camera with acceptable photo time difference" in {
      val cam = cameraE1
      val actor = createActor(createMonitor(cam, 0))
      val req = createComponentRequest(gantryE, plan)

      probe.send(actor, req)

      val res = getComponentResponse(req)
      res.results.size must be(1)
      verifyComponent(res.resultFor(cam.id), true, true)
    }

    "send back a response for a camera with exceeded photo time difference" in {
      val cam = cameraE1
      val actor = createActor(createMonitor(cam, maxTimeDiffMillis + 1))
      val req = createComponentRequest(gantryE, plan)

      probe.send(actor, req)

      val res = getComponentResponse(req)
      res.results.size must be(1)
      verifyComponent(res.resultFor(cam.id), true, false)
    }

    "send back a response for complex camera setup, with 2 cameras on the gantry of interest (one happy, one photo time exceeded)" in {

      val actor = createActor(
        createMonitor(cameraX1, 0), //different gantry
        createMonitor(cameraE1, 0),
        createMonitor(cameraE2, maxTimeDiffMillis + 1))

      val req = createComponentRequest(gantryE, plan)

      probe.send(actor, req)

      val res = getComponentResponse(req)
      res.results.size must be(2) //only cameras from E1

      verifyComponent(res.resultFor(cameraE1.id), true, true)
      verifyComponent(res.resultFor(cameraE2.id), true, false)
    }

  }

  def createActor(monitors: StartedJaiMonitor*): ActorRef =
    system.actorOf(Props(new CameraSelftester(monitors, config, CameraFunctionsStub)))

  def createMonitor(camera: Camera, imageExifTimeDelta: Long): StartedJaiMonitor = {
    val actor = system.actorOf(Props(new MockMonitorActor(camera, imageExifTimeDelta, None)))
    StartedJaiMonitor(camera, actor, None)
  }

  def verifyComponent(component: Option[Component], expectImage: Boolean, timeValid: Boolean): Unit = {
    component match {
      case Some(comp) ⇒
        val expectValid = expectImage && timeValid
        comp.checks.size must be(2)
        comp.checks(0) must be(Check(CameraChecks.cameraImage, expectImage))
        comp.checks(1) must be(Check(CameraChecks.imageTime, timeValid))
        comp.valid must be(expectValid)
      case None ⇒ fail("Missing component")
    }
  }

}

class MockMonitorActor(camera: CameraUnit, imageExifTimeDelta: Long, imageDelayMillis: Option[Long]) extends Actor {

  implicit val executor = context.system.dispatcher

  override def receive: Receive = {
    case JaiImageRequest ⇒
      val client = sender
      val millis: Long = imageDelayMillis getOrElse 0
      val delay = Duration(millis, TimeUnit.MILLISECONDS)
      context.system.scheduler.scheduleOnce(delay, self, ImageReady(System.currentTimeMillis(), client))

    case msg: ImageReady ⇒
      val date = new Date()
      val imageBytes = CameraFunctionsStub.createImageContents(date.getTime - imageExifTimeDelta)
      msg.client ! JaiImageResponse(camera, imageBytes, Some(date))

    case msg ⇒
      println("DummyCalibrationActor@" + camera.id + " received " + msg)
  }

  case class ImageReady(requestTime: Long, client: ActorRef)
}
