package csc.gantrymonitor.selftest

import akka.actor.Props
import csc.gantrymonitor.selftest.GenericAuditActor.AuditFactory
import csc.gantrymonitor.selftest.Selftest.StorageProperties

/**
 * This only tests the actor behaviour (for the happy case with storage).
 * All other exotic cases will be tested in each specific audit test.
 *
 * Created by carlos on 23/08/16.
 */
class GenericAuditActorTest
  extends AbstractSelftestTest("StorageSelftesterTest")
  with StorageAuditTestData
  with CommonAuditTestVerifier {

  implicit val componentType = ComponentType.storage

  val plan = happyPlan

  "GenericAuditActor" must {

    "send back the correct response for the happy case" in {
      val actor = system.actorOf(Props(new GenericAuditActor(componentType, auditFactory)))
      val req = createComponentRequest(gantryE, plan)

      probe.send(actor, req)

      val res = getComponentResponse(req)
      res.errorMessage.isDefined must be(false)
      verify(plan, true, res.results, StorageProperties.all)
    }
  }

  val auditFactory: AuditFactory = { plan ⇒ Right(new MockStorageAudit(plan)) }

}

class MockStorageAudit(plan: SelftestPlan) extends StorageAudit(plan) {

  override def freeSpaceCheck(volume: String): CheckResult = {
    val total = 100000000
    val stats = DiskUsageStats(total, total / 2, 50)
    CheckResult(volume, true, stats.toProps)
  }

}
