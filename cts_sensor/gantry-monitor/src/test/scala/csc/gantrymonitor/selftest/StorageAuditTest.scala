package csc.gantrymonitor.selftest

import com.typesafe.config.{ Config, ConfigFactory }
import csc.akka.logging.DirectLogging
import csc.gantrymonitor.selftest.Selftest.{ StorageProperties, StorageChecks }
import org.scalatest.WordSpec
import org.scalatest._

import scala.collection.JavaConversions

/**
 * Created by carlos on 23/08/16.
 */
class StorageAuditTest
  extends WordSpec
  with MustMatchers
  with DirectLogging
  with StorageAuditTestData
  with CommonAuditTestVerifier {

  val oneKB: Long = 1024
  val oneMB: Long = oneKB * 1024
  val oneGB: Long = oneMB * 1024

  "StorageUnits" must {

    "parse properly values in multiple units" in {
      StorageUnits.getStorageBytes("2GB") must be(Some(oneGB * 2))
      StorageUnits.getStorageBytes("3 MB") must be(Some(oneMB * 3))
      StorageUnits.getStorageBytes(" 4  kb  ") must be(Some(oneKB * 4))
    }

    "parse by default in GB" in {
      StorageUnits.getStorageBytes("1") must be(Some(oneGB))
    }

    "not parse for invalid input" in {
      StorageUnits.getStorageBytes("   ") must be(None)
      StorageUnits.getStorageBytes("foobar5") must be(None)
      StorageUnits.getStorageBytes("3foobars") must be(None)
    }
  }

  "StorageAudit" must {

    "return the correct results when checks pass (happy case)" in {
      executeAndVerify(happyPlan, true)
    }

    "return the corrects results for checks don' pass" in {
      executeAndVerify(failingPlan, false)
    }

  }

  def executeAndVerify(plan: SelftestPlan, expectValid: Boolean): Unit = {
    val audit = new StorageAudit(plan)
    audit.execute() match {
      case Right(components) ⇒ verify(plan, expectValid, components, StorageProperties.all)
      case Left(error)       ⇒ fail("unexpected: " + error)
    }
  }

}

trait StorageAuditTestData extends MustMatchers {

  lazy val testVolumes = List("/tmp", "/")

  lazy val happyPlan = storageSelftestPlan(testVolumes, StorageChecks.all, Map("/" -> "2GB"), Some("1MB"))
  lazy val failingPlan = storageSelftestPlan(testVolumes, StorageChecks.all, Map.empty, Some("8000GB"))

  def storageSelftestPlan(volumes: List[String],
                          checks: List[String],
                          minSizeMap: Map[String, String],
                          defaultMinSize: Option[String]): SelftestPlan = {

    SelftestPlan(ComponentType.storage, volumes, checks, storageConfigs(minSizeMap, defaultMinSize))
  }

  def storageConfigs(minSizeMap: Map[String, String],
                     defaultMinSize: Option[String]): Map[String, Config] = {

    val map: Map[String, String] = defaultMinSize match {
      case None        ⇒ minSizeMap
      case Some(value) ⇒ minSizeMap.updated(Selftest.fallbackKey, value)
    }
    val minSizeCfg = ConfigFactory.parseMap(JavaConversions.mapAsJavaMap(map))
    Map(StorageChecks.freeSpace.toString -> minSizeCfg)
  }

}