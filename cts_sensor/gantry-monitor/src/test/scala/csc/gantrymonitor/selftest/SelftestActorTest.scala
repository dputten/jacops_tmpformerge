package csc.gantrymonitor.selftest

import java.util.concurrent.TimeUnit

import akka.actor._
import scala.concurrent.duration.{ FiniteDuration, Duration }
import csc.gantrymonitor.selftest.ComponentType.ComponentType
import csc.gantrymonitor.selftest.SelftestActor._
import SelftestActorTest.SelftesterBehaviour

/**
 * Created by carlos on 31/07/16.
 */
class SelftestActorTest extends AbstractSelftestTest("SelftestActorTest") {

  import SelftestActorTest._

  "SelftestActor" must {

    "send back an response with empty component results when profile unknown" in {
      val propsFactory = createPropsFactory(List(resultBehaviour(true, 0)))
      val profiles = singleTypeProfile
      actor = createActor(propsFactory, profiles)
      executeAndVerify(TestCase(false, true, profiles, "dummyProfile"))
    }

    "send back a response for a quick and valid test" in {
      val propsFactory = createPropsFactory(List(resultBehaviour(true, 0)))
      actor = createActor(propsFactory, singleTypeProfile)
      executeAndVerify(TestCase(true, false))
    }

    "send back a response for a quick and invalid test" in {
      val propsFactory = createPropsFactory(List(resultBehaviour(false, 0)))
      actor = createActor(propsFactory, singleTypeProfile)
      executeAndVerify(TestCase(false, false))
    }

    "send back a response for a slow, timed out test" in {
      val propsFactory = createPropsFactory(List(resultBehaviour(true, timeoutMillis + 1)))
      actor = createActor(propsFactory, singleTypeProfile)
      executeAndVerify(TestCase(false, true))(timeout * 2) //needs a bigger probe receive timeout
    }

    "process 2 requests sequentially" in {
      val propsFactory = createPropsFactory(List(
        resultBehaviour(false, 0),
        resultBehaviour(true, 0)))

      actor = createActor(propsFactory, singleTypeProfile)
      executeAndVerify(TestCase(false, false)) //first invalid
      executeAndVerify(TestCase(true, false)) //second valid
    }

    "process a request on a profile with multiple component types" in {

      val componentCount = multipleTypesProfile(defaultProfileId).plans.size //one component per type/plan

      val behaviours: List[SelftesterBehaviour] = {
        val seq = for (i ← 0 until componentCount - 1) yield resultBehaviour(true, 0) //all but one tests succeed immediately
        seq.toList :+ resultBehaviour(false, timeoutMillis - 100) //last test fails, and is somewhat slow
      }

      val propsFactory = createPropsFactory(behaviours)

      actor = createActor(propsFactory, multipleTypesProfile)
      executeAndVerify(TestCase(false, false, profiles)) //overall the selftest fails, but has results
    }
  }

  def executeAndVerify(tc: TestCase)(implicit timeout: FiniteDuration): Unit = {
    val req = createRequest(gantryE, tc.profile)
    actor ! req

    val res = getResponse(req)(timeout)
    res.valid must be(tc.expectValid)
    res.errorMessage.isDefined must be(tc.expectedError)

    res.components match {
      case Nil ⇒
        tc.expectedError must be(true)
        res.errorMessage.isDefined must be(true)
      case list ⇒
        res.errorMessage.isDefined must be(false)
        val actualComponents = list.map(_.name).toSet
        val expectedComponents = tc.profiles(tc.profile).plans.keySet map (_.toString)
        actualComponents must be(expectedComponents) //one component for every type in the profile
    }
  }

  def createActor(propsFactory: SelftesterActorPropsFactory, profiles: Map[String, SelftestProfile]): ActorRef =
    system.actorOf(Props(new SelftestActor(profiles, timeout, propsFactory, probe.ref)))

  case class TestCase(expectValid: Boolean,
                      expectedError: Boolean,
                      profiles: Map[String, SelftestProfile] = singleTypeProfile,
                      profile: String = defaultProfileId)

}

class MockSelftester(behaviour: SelftesterBehaviour) extends Actor with ActorLogging {

  implicit val executor = context.system.dispatcher

  override def receive: Receive = {
    case msg: DelayedResponse ⇒ msg.client ! createResponse(msg.req, Right(msg.valid))

    case msg: ComponentSelftestRequest ⇒ behaviour match {
      case Right((valid, delay)) ⇒ delay match {
        case 0 ⇒ sender ! createResponse(msg, Right(valid)) //immediate response
        case x ⇒
          //delayed response
          context.system.scheduler.scheduleOnce(Duration(x, TimeUnit.MILLISECONDS), self, DelayedResponse(sender, msg, valid))
      }
      case Left(errorMsg) ⇒ sender ! msg.toResponse(Nil, Some(errorMsg))
    }
  }

  def createResponse(req: ComponentSelftestRequest, outcome: Either[String, Boolean]): ComponentSelftestResponse = {
    val (components, errorMessage) = outcome match {
      case Right(valid) ⇒
        val name = req.plan.componentType.toString
        val comp = Component(name, name, valid, Nil, Nil)
        (List(comp), None)
      case Left(msg) ⇒ (Nil, Some(msg))
    }

    req.toResponse(components, errorMessage)
  }

  case class DelayedResponse(client: ActorRef, req: ComponentSelftestRequest, valid: Boolean)

}

object SelftestActorTest extends SelftestTestData {

  type SelftesterBehaviour = Either[String, (Boolean, Long)] //either error message, or (valid, delay) (0 delay means immediate)

  lazy val multipleTypesProfile: Map[String, SelftestProfile] =
    Map(defaultProfileId -> createDummyProfile(defaultProfile.plans.keySet.toArray: _*))

  lazy val singleTypeProfile: Map[String, SelftestProfile] =
    Map(defaultProfileId -> createDummyProfile(ComponentType.camera))

  /**
   * @param types
   * @return profile with a dummy plan for each of the given component types.
   *         Each plan has one component with the name of the component type
   */
  def createDummyProfile(types: ComponentType*): SelftestProfile = {
    val plans: Map[ComponentType, SelftestPlan] = types.map { e ⇒
      e -> SelftestPlan(e, List(e.toString), Nil, Map.empty)
    }.toMap

    SelftestProfile(plans)
  }

  def resultBehaviour(valid: Boolean, delay: Long): SelftesterBehaviour = Right((valid, delay))
  def errorBehaviour(msg: String): SelftesterBehaviour = Left(msg)

  /**
   * Creates a SelftesterActorPropsFactory given the sequence of behaviour for the selftest actor
   * @param seq sequence of selftest actor behaviours
   * @return
   */
  def createPropsFactory(seq: List[SelftesterBehaviour]): SelftesterActorPropsFactory =
    new MockedSelftesterPropsFactory(seq)

  class MockedSelftesterPropsFactory(seq: List[SelftesterBehaviour]) extends SelftesterActorPropsFactory {
    private var behaviours = seq
    override def apply(tp: ComponentType): Option[Props] = behaviours match {
      case Nil ⇒ None //no more behaviour to follow
      case head :: tail ⇒
        behaviours = tail //consume head
        Some(Props(new MockSelftester(head)))
    }
  }

}
