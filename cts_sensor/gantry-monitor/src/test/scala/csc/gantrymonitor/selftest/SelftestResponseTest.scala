package csc.gantrymonitor.selftest

import org.scalatest.WordSpec
import org.scalatest._

class SelftestResponseTest extends WordSpec with MustMatchers {
  val CorrelationId = "correlationId"
  val system = "A4"
  val Gantry = "gantry"
  val Time = System.currentTimeMillis()
  val Profile = "profile"
  val ValidCheck1 = Check("check1", valid = true)
  val ValidCheck2 = Check("check2", valid = true)
  val ValidCheck3 = Check("check3", valid = true)
  val InvalidCheck4 = Check("check4", valid = false)
  val ValidComponent1 = Component("component1", "family", valid = true, List(ValidCheck1, ValidCheck2), List())
  val ValidComponent2 = Component("component2", "family", valid = true, List(ValidCheck3, InvalidCheck4), List())
  val InvalidComponent3 = Component("component3", "family", valid = false, List(ValidCheck3, InvalidCheck4), List())

  "SelftestResponse" must {
    "give the correct value for false when there are no components" in {
      SelftestResponse(CorrelationId, system, Gantry, Time, Profile, List(), None).valid must be(false)
    }

    "give the correct value for valid when all components are valid" in {
      SelftestResponse(CorrelationId, system, Gantry, Time, Profile, List(ValidComponent1, ValidComponent2), None).valid must be(true)
    }

    "give the correct value for valid when one component isn't valid" in {
      SelftestResponse(CorrelationId, system, Gantry, Time, Profile, List(ValidComponent1, ValidComponent2, InvalidComponent3), None).valid must be(false)
    }
  }
}
