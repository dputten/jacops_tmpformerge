package csc.gantrymonitor.selftest

import com.typesafe.config.{ ConfigFactory, Config }
import csc.gantrymonitor.selftest.QueueAdmin._
import csc.gantrymonitor.selftest.Selftest.QueueChecks
import org.scalatest.WordSpec
import org.scalatest._

import scala.collection.JavaConversions

/**
 * Created by carlos on 26/08/16.
 */
class QueueAdminTest
  extends WordSpec
  with MustMatchers
  with QueueAdminTestData {

  "RabbitMQRestAdmin" must {
    val admin = new RabbitMQRestAdmin(createApiClient(httpResponses))

    "parse list of queues from a RabbitMQ JSON api String" in {
      admin.getQueueNames() match {
        case Right(list) ⇒ list must be(List("registrations", "system-events"))
        case Left(error) ⇒ fail(error)
      }
    }

    "parse queue details from a RabbitMQ JSON api String" in {
      admin.getQueueDetails("registrations") match {
        case Right(detail) ⇒ detail must be(QueueDetail("registrations", "running", 3, Some(2.6), Some(2.0)))
        case Left(error)   ⇒ fail(error)
      }
    }
  }
}

object LiveQueueAdmin {

  def main(args: Array[String]) {
    val url = args(0)
    val admin = new RabbitMQRestAdmin(QueueAdmin.defaultHttpClientFactory(url))

    val queues: List[String] = admin.getQueueNames() match {
      case Right(list) ⇒ list
      case Left(error) ⇒
        println(error)
        Nil
    }

    println(queues)
    queues foreach { queue ⇒ println(admin.getQueueDetails(queue)) }
  }
}

trait QueueAdminTestData extends RabbitMQRestApi {

  val dummyHttpFactory: HttpClientFactory = { url ⇒ createApiClient(httpResponses) }

  def createApiClient(results: Map[String, String]): HttpClient = new HttpClient {
    override def get(path: String): Either[String, String] = results.get(path) match {
      case None        ⇒ Left("404 Not found: " + path)
      case Some(value) ⇒ Right(value)
    }
  }

  val queuesJson =
    """
      |[{"name":"registrations"},{"name":"system-events"}]
    """.stripMargin

  val registrationsJson =
    """
      |{"message_stats":{"ack":3487366,"ack_details":{"rate":2.0},"deliver":3487376,"deliver_details":{"rate":2.0},"deliver_get":3487376,"deliver_get_details":{"rate":2.0},"disk_reads":12,"disk_reads_details":{"rate":0.0},"disk_writes":3487354,"disk_writes_details":{"rate":2.0},"publish":3487361,"publish_details":{"rate":2.6},"redeliver":10,"redeliver_details":{"rate":0.0}},"messages":3,"state":"running"}
    """.stripMargin

  val systemEventsJson =
    """
      |{"message_stats":{"ack":635,"ack_details":{"rate":0.0},"deliver":635,"deliver_details":{"rate":0.0},"deliver_get":643,"deliver_get_details":{"rate":0.0},"get":8,"get_details":{"rate":0.0},"publish":635,"publish_details":{"rate":0.0},"redeliver":8,"redeliver_details":{"rate":0.0}},"messages":0,"state":"running"}
    """.stripMargin

  val queues = List("registrations", "system-events")

  val httpResponses = Map(
    queueListUrl -> queuesJson,
    queueDetailUrl("registrations") -> registrationsJson,
    queueDetailUrl("system-events") -> systemEventsJson)

  def createQueuePlan(adminUrl: String) =
    SelftestPlan(ComponentType.queue, Nil, QueueChecks.all, queueConfigs(adminUrl))

  def queueConfigs(adminUrl: String): Map[String, Config] = {
    val map: Map[String, AnyRef] =
      Map(QueueAudit.adminBaseUrlKey -> adminUrl)
    val settings = ConfigFactory.parseMap(JavaConversions.mapAsJavaMap(map))
    Map(Selftest.settingsKey -> settings)
  }

}

