package csc.gantrymonitor.selftest

import com.typesafe.config.{ ConfigFactory, Config }
import csc.gantrymonitor.selftest.Selftest.{ QueueProperties, QueueChecks }
import org.scalatest.WordSpec
import org.scalatest._

/**
 * Created by carlos on 26/08/16.
 */
class QueueAuditTest
  extends WordSpec
  with MustMatchers
  with CommonAuditTestVerifier
  with QueueAdminTestData {

  "QueueAudit" must {
    "return the proper response for a rabbitMQ server" in {
      val plan = createQueuePlan("dummy")

      val audit = new QueueAudit(plan, dummyHttpFactory)
      val expectedProps = QueueProperties.all
      val expectedComponents = queues
      audit.execute() match {
        case Right(components) ⇒ verify(plan, true, components, expectedProps, expectedComponents)
        case Left(error)       ⇒ fail("unexpected: " + error)
      }
    }
  }

}
