/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.gantrymonitor.config

import com.typesafe.config.{ Config, ConfigFactory }
import csc.akka.logging.DirectLogging
import csc.akka.config.ConfigUtil._
import csc.amqp.{ AmqpConfiguration, ServicesConfiguration }
import csc.gantry.config._
import csc.gantrymonitor.calibration.{ NativeImageFunctions, CameraFunctionsStub, ImageFunctions }
import csc.gantrymonitor.selftest.{ SelftestPlan, SelftestProfile, ComponentType }
import ComponentType.ComponentType
import csc.systemevents._
import csc.akka.config.{ ConfigWrapper, ConfigUtil }
import csc.curator.utils.{ CuratorToolsImpl, Curator }
import org.apache.curator.framework.CuratorFrameworkFactory
import org.apache.curator.retry.RetryUntilElapsed
import net.liftweb.json.DefaultFormats
import csc.json.lift.EnumerationSerializer
import csc.vehicle.message.VehicleImageType
import csc.curator.utils.xml.{ XmlReader, CuratorToolsImplXml }
import akka.actor.ActorRef
import java.util.concurrent.TimeUnit

import scala.concurrent.duration._

case class GantryConfiguration(systemId: String, gantryIds: List[String])

/**
 * The configuration used for the self test functionality
 * @param timeout The total timeout of selftest/calibration before returning an error response
 * @param imageTimeout The timeout of retrieving an image
 * @param quality
 * @param workingDirectory the working directory used for converting images to jpeg
 * @param maxTimeDiffMillis
 * @param profiles
 */
case class SelfTestConfiguration(
  timeout: FiniteDuration,
  imageTimeout: FiniteDuration,
  quality: Int,
  workingDirectory: Option[String],
  maxTimeDiffMillis: Option[Long],
  profiles: Map[String, SelftestProfile] = Map.empty)

/**
 * Configuration of the gantry monitor application
 * @param selfTest The selftest configuration
 */
case class GantryMonitorConfiguration(selfTest: SelfTestConfiguration,
                                      delaySystemEvent: FiniteDuration,
                                      maxRetriesGetImage: Int,
                                      timeoutGetImage: FiniteDuration,
                                      errorCheck: Option[FiniteDuration] = None,
                                      systemEventSolvedSending: Boolean = false)

case class StubConfiguration(cameraStubUri: String, imageFunctions: Boolean)

/**
 * Reading the configuration file and create the VehicleRegistration Configuration classes
 */
class Configuration(val config: Config) extends DirectLogging {

  def this() = this(ConfigFactory.load())

  private val rootConfig = "gantry-monitor"
  val amqpConfiguration = createAmqpConfig
  val amqpSystemEventServicesConfiguration = createAmqpSystemEventServicesConfig
  val amqpCalibrationServicesConfiguration = createAmqpCalibrationServicesConfig
  val gantryConfiguration = createGantryConfig
  val monitorConfiguration = createSystemMonitor
  val laneConfiguration = createLaneConfig
  val stubConfiguration = createStubConfig

  lazy val curator: Curator = {
    //val appConfig: Config = ConfigFactory.load()
    config.getString("curator_implementation") match {
      case "xml" ⇒
        log.info("Using xml for Configuration")
        getXmlCurator(config.getString("curator_implementation_xml_file_location"))
      case "zookeeper" ⇒
        log.info("Using zookeeper for Configuration")
        val servers = config.getReplacedString(rootConfig + ".laneConfig.zookeeper.servers")
        getZookeeperCurator(servers)
    }

  }

  def systemEventStore(actorRef: ActorRef): SystemEventStore = createSystemEventStore(config, actorRef)

  def closeCurator() {
    curator.curator.foreach(_.close())
  }

  private def getXmlCurator(fileLocation: String): Curator = {
    val formats = DefaultFormats + new EnumerationSerializer(VehicleImageType)
    new CuratorToolsImplXml(new XmlReader(fileLocation, log), log, formats)
  }

  private def getZookeeperCurator(zkServerQuorum: String): Curator = {
    val retryPolicy = new RetryUntilElapsed(3.days.toMillis.toInt, 5000)
    val client = CuratorFrameworkFactory.newClient(zkServerQuorum, retryPolicy)
    client.start()
    val formats = DefaultFormats + new EnumerationSerializer(VehicleImageType, ServiceType, ZkCaseFileType)
    new CuratorToolsImpl(Some(client), log, formats)
  }

  /**
   * Create the amqp-services configuration structure
   * @return GantryConfiguration the gantry configuration
   */
  private def createAmqpSystemEventServicesConfig: ServicesConfiguration = {
    val consumerQueue = "notused"
    val producerEndpoint = config.getReplacedString(rootConfig + ".systemEventServices.producer-endpoint")
    val producerRouteKey = config.getReplacedString(rootConfig + ".systemEventServices.producer-route-key")
    val serversCount = config.getReplacedInteger(rootConfig + ".systemEventServices.servers-count")
    new ServicesConfiguration(consumerQueue, producerEndpoint, producerRouteKey, serversCount)
  }

  /**
   * Create the amqp-services configuration structure
   * @return GantryConfiguration the gantry configuration
   */
  private def createAmqpCalibrationServicesConfig: ServicesConfiguration = {
    val consumerQueue = config.getReplacedString(rootConfig + ".calibrationServices.consumer-queue")
    val producerEndpoint = config.getReplacedString(rootConfig + ".calibrationServices.producer-endpoint")
    val producerRouteKey = config.getReplacedString(rootConfig + ".calibrationServices.producer-route-key")
    val serversCount = config.getReplacedInteger(rootConfig + ".calibrationServices.servers-count")
    new ServicesConfiguration(consumerQueue, producerEndpoint, producerRouteKey, serversCount)
  }

  /**
   * Create the amqp configuration structure
   * @return GantryConfiguration the gantry configuration
   */
  private def createAmqpConfig: AmqpConfiguration = {
    val actorName = config.getReplacedString(rootConfig + ".amqp-connection.actor-name")
    val amqpUri = config.getReplacedString(rootConfig + ".amqp-connection.amqp-uri")
    val reconnectDelay = Duration.create(config.getReplacedMilliseconds(rootConfig + ".amqp-connection.reconnect-delay"), TimeUnit.MILLISECONDS)
    new AmqpConfiguration(actorName, amqpUri, reconnectDelay)
  }

  /**
   * Create the gantry configuration structure
   * @return GantryConfiguration the gantry configuration
   */
  private def createGantryConfig: GantryConfiguration = {
    import collection.JavaConversions._
    val systemId = config.getReplacedString(rootConfig + ".location.systemId")
    val gantryIds = config.getStringList(rootConfig + ".location.gantryIds")
    new GantryConfiguration(systemId = systemId, gantryIds = gantryIds.toList)
  }
  /**
   * Create the LaneConfiguration implementation
   * @return The LaneConfiguration implementation which has to be used
   */
  def createLaneConfig: LaneConfiguration = {
    val service = config.getReplacedString(rootConfig + ".laneConfig.zookeeper.service")
    if (service == "on") {
      log.info("Using zookeeper for LaneConfiguration")
      val servers = config.getReplacedString(rootConfig + ".laneConfig.zookeeper.servers")
      val labelPath = {
        val path = config.getReplacedString(rootConfig + ".laneConfig.zookeeper.labelPath", "")
        if (path.isEmpty) {
          None
        } else {
          Some(path)
        }
      }
      new ZookeeperLaneConfiguration(curator, "/ctes/systems", labelPath)
    } else {
      val fileService = config.getReplacedString(rootConfig + ".laneConfig.file.service")
      if (fileService == "on") {
        val file = config.getReplacedString(rootConfig + ".laneConfig.file.fileName")
        log.info("Using jsonFile %s for LaneConfiguration".format(file))

        val cameraFile = ConfigUtil.getReplacedString(config, rootConfig + ".laneConfig.file.cameraFilename")
        log.info("Using camera jsonFile %s for LaneConfiguration".format(cameraFile))

        new FileLaneConfiguration(file, None, cameraFile)
      } else {
        log.info("Using Empty LaneConfiguration")
        new EmptyLaneConfiguration
      }
    }
  }

  /**
   * Create the SystemEventStore implementation
   * @param config
   * @return The SystemEventStore implementation which has to be used
   */
  //TODO RB: use option for actorRef and default None
  def createSystemEventStore(config: Config, actorRef: ActorRef = null): SystemEventStore = {
    val service = config.getReplacedString(rootConfig + ".systemEvent.zookeeper.service")
    val fileService = config.getReplacedString(rootConfig + ".systemEvent.file.service")
    val amqpService = config.getReplacedString(rootConfig + ".systemEvent.amqp.service")

    if (service == "on") {
      log.info("Using zookeeper for SystemEventsStore")
      val servers = config.getReplacedString(rootConfig + ".systemEvent.zookeeper.servers")
      new ZookeeperSystemEventStore(curator)
    } else if (fileService == "on") {
      val file = config.getReplacedString(rootConfig + ".systemEvent.file.fileName")
      log.info("Using jsonFile %s for SystemEventsStore".format(file))
      new FileSystemEventStore(file)
    } else if (amqpService == "on") {
      log.info("Using amqp for SystemEventsStore")
      new AmqpSystemEventStore(actorRef)
    } else {
      log.info("Using Empty SystemEventsStore")
      new EmptySystemEventStore
    }
  }

  /**
   * Create the gantry configuration structure
   * @return GantryConfiguration the gantry configuration
   */
  private def createSystemMonitor: GantryMonitorConfiguration = {

    val cameraPingFreq = config.getLongOpt(rootConfig + ".monitor.camera.pingFrequency") map (_.millis)
    val delaySystemEvent = config.getReplacedInteger(rootConfig + ".monitor.camera.delayEvent")
    val nrRetries = config.getReplacedInteger(rootConfig + ".monitor.camera.nrRetries")
    val timeout = config.getReplacedInteger(rootConfig + ".monitor.camera.timeout")
    val errorCheck = {
      val oneMinute = 1.minute
      val conf = config.getReplacedInteger(rootConfig + ".monitor.camera.errorcheck")
      if (conf >= oneMinute.toMillis) {
        Some(conf.millis)
      } else {
        log.warning("Camera errorcheck value is too low: {}. Using default value {}", conf, oneMinute.toMillis)
        Some(oneMinute)
      }
    }

    val solvedSending = config.getReplacedBoolean(rootConfig + ".systemEvent.solvedSending", false)

    val cfg = new GantryMonitorConfiguration(
      selfTest = createSelftestConfig,
      delaySystemEvent = delaySystemEvent milliseconds,
      maxRetriesGetImage = nrRetries,
      timeoutGetImage = timeout milliseconds,
      errorCheck = errorCheck,
      systemEventSolvedSending = solvedSending)

    log.info("Found GantryMonitorConfiguration: " + cfg)
    cfg
  }

  private def createSelftestConfig: SelfTestConfiguration = {
    val cfg = config.getConfig(rootConfig + ".monitor.selftest")

    val profiles: Map[String, SelftestProfile] = cfg.configOpt("profiles") match {
      case None ⇒ Map.empty
      case Some(c) ⇒ c.getKeySet().map { name ⇒
        name -> createSelftestProfile(c.getConfig(name))
      }.toMap
    }

    if (profiles.isEmpty) log.warning("No profiles defined in SelfTestConfiguration. Selftest will hold no results")

    new SelfTestConfiguration(
      timeout = cfg.getReplacedLong("timeout", 60000).millis, //replaces calibrationTimeout
      imageTimeout = cfg.getReplacedLong("imageTimeout", 10000).millis,
      quality = cfg.getReplacedInteger("quality", 80),
      workingDirectory = ConfigUtil.getReplacedString(cfg, "workingDirectory"),
      maxTimeDiffMillis = ConfigUtil.getReplacedString(cfg, "maxTimeDiffMillis").map(_.toLong),
      profiles = profiles)
  }

  private def createSelftestProfile(cfg: Config): SelftestProfile = {
    val types = cfg.getKeySet().filter(ComponentType.names).toList
    val plans: List[SelftestPlan] = for (
      tp ← types;
      plan ← createSelftestPlan(ComponentType.withName(tp), cfg.getConfig(tp))
    ) yield plan

    val planMap = plans.map { p ⇒ p.componentType -> p }.toMap
    SelftestProfile(planMap)
  }

  private def createSelftestPlan(ct: ComponentType, cfg: Config): Option[SelftestPlan] = {
    val items = cfg.list[String]("items")
    val checks = cfg.list[String]("checks")

    Some(SelftestPlan(ct, items, checks, cfg.configMap))
  }

  def createStubConfig: Option[StubConfiguration] = {
    config.getReplacedBoolean(rootConfig + ".stub.enabled", false) match {
      case false ⇒ None
      case true ⇒
        val cfg = new ConfigWrapper(config.getConfig(rootConfig + ".stub"))
        Some(StubConfiguration(cfg.string("cameraStubUri"), cfg.config.getBoolean("imageFunctions")))
    }
  }

  def stubUri: Option[String] = stubConfiguration map (_.cameraStubUri)

  def imageFunctions: ImageFunctions = stubConfiguration match {
    case Some(cfg) if (cfg.imageFunctions) ⇒ CameraFunctionsStub
    case _                                 ⇒ NativeImageFunctions
  }

}
