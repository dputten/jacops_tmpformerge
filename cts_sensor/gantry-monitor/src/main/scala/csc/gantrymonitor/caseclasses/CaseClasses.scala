package csc.gantrymonitor.caseclasses

import akka.actor.{ Cancellable, ActorRef }
import csc.vehicle.message.CameraUnit

/**
 * Class to bundle the lane and the jaimonitor
 *
 * @ param camera: CameraConfig
 * @param camera: the CameraUnit for the started jaimonitor
 * @param ref: the started jaimonitor
 *
 */
case class StartedJaiMonitor(camera: CameraUnit, ref: ActorRef, schedule: Option[Cancellable])
