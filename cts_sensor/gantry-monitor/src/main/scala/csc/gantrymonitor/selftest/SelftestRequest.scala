package csc.gantrymonitor.selftest

case class SelftestRequest(correlationId: String, system: String, gantry: String, time: Long, profile: String)