package csc.gantrymonitor.selftest

import csc.akka.config.ConfigUtil.configUtil
import csc.akka.process.{ ExecuteScript, ExecuteScriptResult, ScriptExecutor }
import csc.gantrymonitor.selftest.Selftest.{ ClockChecks, ClockProperties }
import csc.akka.config.ConfigUtil

/**
 * Created by carlos on 25/08/16.
 */
class ClockAudit(val plan: SelftestPlan) extends Audit {

  lazy val offsetThreshold = plan.settings flatMap { cfg ⇒ cfg.getLongOpt(ClockAudit.offsetThresholdKey) }

  override protected def items: List[String] = ClockAudit.allItems //items are hardcoded, not configured

  override def executors: PartialFunction[String, CheckExecutor] = {
    case ClockChecks.offset ⇒ offsetCheck
  }

  private def offsetCheck(comp: String): CheckResult = if (comp == ClockAudit.ntp) {
    val activePeer = ntpQueryScript match {
      case Left(errorMessage) ⇒
        log.warning("Unable to get a ntpq script: {}", errorMessage)
        None
      case Right(script) ⇒ getNtpActivePeer(script)
    }
    val valid = activePeer map (isOffsetValid(_)) getOrElse false
    val props = activePeer map (ntpProps(_)) getOrElse Map.empty
    CheckResult(comp, valid, props)
  } else {
    CheckResult(comp, false, Map.empty)
  }

  private def ntpProps(peer: NTPPeerResult): Map[String, String] =
    Map(ClockProperties.offset -> peer.offset, ClockProperties.poll -> peer.poll)

  private def isOffsetValid(peer: NTPPeerResult): Boolean = offsetThreshold match {
    case None ⇒
      log.warning("offset threshold not configured")
      false
    case Some(threshold) ⇒ math.abs(peer.getOffsetValue) < threshold
  }

  private def ntpQueryScript: Either[String, ExecuteScript] = plan.settings match {
    case None ⇒ Left("No settings")
    case Some(cfg) ⇒ cfg.getStringListOpt(ClockAudit.ntpqCommandPartsKey) match {
      case None ⇒ Left("No command")
      case Some(value) ⇒ value match {
        case Nil ⇒ Left("empty value")
        case list ⇒ Right(ExecuteScript(list.head, list.tail.toSeq))
      }
    }
  }

  private def getNtpActivePeer(script: ExecuteScript): Option[NTPPeerResult] = {
    val scriptResult = executeScript(script)
    log.debug("Script {} result is {}", script, scriptResult)

    scriptResult.resultCode match {
      case Left(ex) ⇒ None
      case Right(code) ⇒ code match {
        case 0 ⇒
          val (errors, peers) = NTPParser.parseNtpqOutput(scriptResult.stdOut, log)
          if (errors.size > 0) log.warning("ntpq script errors: \n" + errors.mkString("\n"))
          ntpActivePeer(peers)
        case x ⇒ None
      }
    }
  }
  //Introduced to be able to test
  protected def executeScript(script: ExecuteScript): ExecuteScriptResult = ScriptExecutor.executeScript(script, log)
  protected def ntpActivePeer(peers: Seq[NTPPeerResult]): Option[NTPPeerResult] = NTPParser.activePeer(peers)

}

object ClockAudit {

  val ntp = "ntp"
  val allItems = List(ntp)
  val ntpqCommandPartsKey = "ntpqCommand"
  val offsetThresholdKey = "offsetThreshold"
}
