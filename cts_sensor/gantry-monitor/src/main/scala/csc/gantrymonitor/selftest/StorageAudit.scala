/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.gantrymonitor.selftest

import java.io.File

import csc.akka.config.ConfigUtil._
import csc.gantrymonitor.selftest.Selftest.{ StorageProperties, StorageChecks }

class StorageAudit(val plan: SelftestPlan) extends Audit {

  lazy val planChecks = plan.checks

  lazy val minSizeMap: Map[String, Long] = plan.configs.get(StorageChecks.freeSpace.toString) match {
    case None ⇒
      log.warning("No configuration for free space thresholds")
      Map.empty

    case Some(config) ⇒ {
      val list = for (
        key ← config.getKeySet().toList;
        value ← StorageUnits.getStorageBytes(config.getString(key))
      ) yield key -> value

      val allMap = list.toMap
      val defValue = allMap.get(Selftest.fallbackKey) getOrElse (StorageUnits.oneGB)
      val map = allMap.filterKeys(items.toSet)

      map.withDefaultValue(defValue)
    }
  }

  override val executors: PartialFunction[String, CheckExecutor] = {
    case StorageChecks.freeSpace ⇒ freeSpaceCheck
  }

  def freeSpaceCheck(volume: String): CheckResult = {
    val file = new java.io.File(volume)
    file.exists() match {
      case true ⇒
        val minSize = minSizeMap(volume)
        val stats = getUsageStats(file)
        val valid = stats.usable >= minSize
        CheckResult(volume, valid, stats.toProps)
      case false ⇒
        CheckResult(volume, false, Map("message" -> "Path doesn't exist"))
    }
  }

  protected def getUsageStats(file: File): DiskUsageStats = {
    val totalSpace = file.getTotalSpace //total disk space in bytes.
    val usableSpace = file.getUsableSpace ///unallocated / free disk space in bytes.
    val ratio = 1 - usableSpace.toFloat / totalSpace.toFloat
    DiskUsageStats(totalSpace, usableSpace, (ratio * 100).round)
  }

}

case class DiskUsageStats(total: Long, usable: Long, ratio: Int) {

  def toProps: Map[String, String] = Map(
    StorageProperties.freeBytes -> usable.toString,
    StorageProperties.freeRatio -> (ratio + "%"))
}

object StorageUnits {
  val base = 1024
  val defaultUnit = "gb"

  def oneGB: Long = math.pow(base, 3).toLong //hard fallback, just in case

  val sizeMap: Map[String, Long] = Map(
    "kb" -> base,
    "mb" -> math.pow(base, 2).toLong,
    defaultUnit -> math.pow(base, 3).toLong)

  def getStorageBytes(source: String): Option[Long] = {
    val str = source.trim
    val numeric: Option[String] = str.takeWhile(_.isDigit) match {
      case "" ⇒ None
      case v  ⇒ Some(v)
    }

    val unit: Option[String] = numeric match {
      case None ⇒ None
      case Some(prefix) ⇒ str.substring(prefix.length).trim.toLowerCase match {
        case "" ⇒ Some(defaultUnit)
        case r  ⇒ Some(r)
      }
    }

    (numeric, unit) match {
      case (Some(num), Some(un)) ⇒ try {
        val multiplier = sizeMap(un)
        Some(num.toLong * multiplier)
      } catch {
        case ex: Exception ⇒ None
      }
      case _ ⇒ None
    }
  }

}
