package csc.gantrymonitor.selftest

import java.io.File
import java.text.SimpleDateFormat
import java.util.{ Calendar, GregorianCalendar, Date }

import csc.gantrymonitor.selftest.Selftest.{ FtpProperties, FtpChecks }
import csc.akka.config.ConfigUtil._
import org.apache.commons.io.input.ReversedLinesFileReader

/**
 * Created by carlos on 30/08/16.
 */
class FtpAudit(val plan: SelftestPlan) extends Audit {

  import FtpAudit._

  val logFile: Option[File] = plan.settings flatMap (_.getStringOpt(logFilePathKey)) map (new File(_))

  var lastActivity: Option[Date] = None

  /**
   * Strategy method to get the components for this audit. By default returns the items in the plan
   * Should be overridden if the items are not configured, but dynamic
   * @return components to go through
   */
  override protected def items: List[String] = ftpItems

  /**
   * Override to add more errors or supress the existing ones
   * @return List of errors that are an impediment for running the audit
   */
  override protected def setupErrors: List[String] = super.setupErrors ++ {
    logFile match {
      case None ⇒ List("No log file path defined")
      case _    ⇒ Nil
    }
  }

  override def executors: PartialFunction[String, CheckExecutor] = {
    case FtpChecks.report         ⇒ reportCheck
    case FtpChecks.recentActivity ⇒ recentActivityCheck
  }

  private def reportCheck(comp: String): CheckResult = {
    lastActivity = getLastActivity()
    val when = lastActivity match {
      case None       ⇒ Selftest.unknownValue
      case Some(date) ⇒ dateFormat.format(date)
    }
    CheckResult(comp, true, Map(FtpProperties.lastActivity -> when))
  }

  private def recentActivityCheck(comp: String): CheckResult = {
    val valid = lastActivity match {
      case None       ⇒ false
      case Some(date) ⇒ isActivityRecent(date)
    }
    CheckResult(comp, valid, Map.empty)
  }

  private def isActivityRecent(date: Date): Boolean = {
    val threshold: Option[Int] = plan.settings flatMap (_.getIntOpt(activityThresholdMinutesKey))

    threshold match {
      case None ⇒
        log.warning("{} not configured", activityThresholdMinutesKey)
        false
      case Some(minutes) ⇒
        val gc = new GregorianCalendar()
        val now = System.currentTimeMillis()
        gc.setTime(date)
        gc.add(Calendar.MINUTE, minutes)
        now < gc.getTime.getTime
    }
  }

  private def getLastActivity(): Option[Date] = logFile flatMap { file ⇒
    val reader = new ReversedLinesFileReader(file)
    val line = reader.readLine()
    getTimestamp(line)
  }

  private def getTimestamp(line: String): Option[Date] = {
    val result: Option[Date] = line match {
      case logLineExpression(str) ⇒ try {
        Some(dateFormat.parse(str.trim))
      } catch {
        case ex: Exception ⇒ None
      }
      case other ⇒
        None
    }

    if (result.isEmpty) log.warning("Unable to parse any date from {}", line)

    result
  }

}

object FtpAudit {

  private val datePattern = "yyyy-MM-dd HH:mm:ss,SSS"
  def dateFormat = new SimpleDateFormat(datePattern)

  val logLineExpression = """\[.+\](.+)\[.+\] \[.*\] .*""".r

  val ftp = "ftp"
  val ftpItems = List(ftp)
  val logFilePathKey = "logFilePath"
  val activityThresholdMinutesKey = "activityThresholdMinutes"

}
