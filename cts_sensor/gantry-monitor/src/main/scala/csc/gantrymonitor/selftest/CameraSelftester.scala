package csc.gantrymonitor.selftest

import akka.actor._
import csc.calibration.{ GroupedGantryCalibrationResponse, GantryCalibrationRequest }
import csc.gantrymonitor.calibration.{ ImageFunctions, CalibrationActor }
import csc.gantrymonitor.caseclasses.StartedJaiMonitor
import csc.gantrymonitor.config.SelfTestConfiguration
import ComponentType.ComponentType

/**
 * Actor taking care of selftest for cameras, responsible for:
 * -receive ComponentSelftestRequest for component type 'camera'
 * -send one GantryCalibrationRequest to Calibrationctor (for all the managed cameras in the gantry of interest)
 * -wait for the GroupedGantryCalibrationResponse, and when received, send the ComponentSelftestResponse to the client
 *
 * This actor is meant to process only one request, and doesn't manage timeouts.
 *
 * Created by carlos on 18/08/16.
 */
class CameraSelftester(cameraMonitors: Seq[StartedJaiMonitor],
                       selfTestConfiguration: SelfTestConfiguration,
                       imageFunctions: ImageFunctions) extends Actor with ActorLogging {

  case class State(req: ComponentSelftestRequest, client: ActorRef) {

    def shouldHandle(msg: GroupedGantryCalibrationResponse): Boolean = req.time == msg.time
  }

  var calibrationActor: Option[ActorRef] = None

  override def preStart(): Unit = {
    calibrationActor = Some(context.actorOf(Props(new CalibrationActor(cameraMonitors, self, selfTestConfiguration, imageFunctions))))
  }

  override def postStop(): Unit = {
    calibrationActor map (_ ! PoisonPill)
  }

  private def receiveWithState(state: State): Receive = {
    case msg: GroupedGantryCalibrationResponse ⇒ handleCalibrationResponse(state, msg)
  }

  override def receive: Receive = {
    case req: ComponentSelftestRequest if (req.plan.componentType == ComponentType.camera) ⇒
      val client = sender
      triggerCameraSelftests(req) match {
        case None         ⇒ context.become(receiveWithState(State(req, client)))
        case errorMessage ⇒ client ! req.toResponse(Nil, errorMessage)
      }

    case msg: ComponentSelftestRequest ⇒
      log.warning("Not for me: {}", msg)
      sender ! msg.toResponse(Nil, Some("Wrong handler"))
  }

  private def triggerCameraSelftests(req: ComponentSelftestRequest): Option[String] = {

    val checks = req.plan.checks

    checks match {
      case Nil ⇒ Some("No camera checks")

      case cameraChecks ⇒
        val cameras: Seq[String] = getCamerasFor(req.id.system, req.id.gantry)

        if (cameras.size > 0) {
          val calibrationReq = GantryCalibrationRequest(req.id.system, req.id.gantry, req.time, cameras, Some(checks))
          calibrationActor foreach { actor ⇒
            log.info("Requesting calibration: {} ", calibrationReq)
            actor ! calibrationReq
          }
          None //successfully triggered

        } else {
          Some("No cameras matching the criteria")
        }
    }
  }

  private def handleCalibrationResponse(state: State, msg: GroupedGantryCalibrationResponse): Unit = {
    if (state.shouldHandle(msg)) {
      log.info("Received calibration response {} for {}", msg, state.req)
      val results: Map[String, Component] = msg.resultsMap.map { e ⇒
        val camId = e._1
        val res = e._2
        val valid = res.forall(_.success)
        val checks = res map { r ⇒ Check(r.stepName, r.success) }
        val family = ComponentType.camera.toString
        camId -> Component(camId, family, valid, checks.toList, Nil)
      }.toMap

      state.client ! state.req.toResponse(results.values.toList, None)

    } else {
      log.warning("Received {}, but was expecting a response for calibration {}. Discarding", msg, state.req)
    }
  }

  private def getCamerasFor(system: String, gantry: String): List[String] =
    for (m ← cameraMonitors.toList if (m.camera.gantry == gantry && m.camera.system == system)) yield m.camera.id

}
