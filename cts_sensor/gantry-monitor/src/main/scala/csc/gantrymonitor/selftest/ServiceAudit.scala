package csc.gantrymonitor.selftest

import csc.akka.config.ConfigUtil._
import csc.akka.process.{ ExecuteScript, ScriptExecutor }
import csc.gantrymonitor.selftest.Selftest.ServiceChecks

/**
 * Created by carlos on 24/08/16.
 */
class ServiceAudit(val plan: SelftestPlan) extends Audit {

  val statusCommandParts: Option[List[String]] =
    plan.settings flatMap { c ⇒ c.getStringListOpt(ServiceAudit.statusCommandPatternKey) }

  /**
   * @return map of check name to CheckExecutor
   */
  override def executors: PartialFunction[String, CheckExecutor] = {
    case ServiceChecks.running ⇒ runningCheck
  }

  private def runningCheck(service: String): CheckResult = {
    val valid = statusScriptFor(service) match {
      case Left(errorMessage) ⇒
        log.warning("Unable to get a status script: {}", errorMessage)
        false
      case Right(script) ⇒
        val result = ScriptExecutor.executeScript(script, log)
        result.resultCode match {
          case Left(error) ⇒ log.warning("Error executing script {}: {}", script, error.toString)
          case Right(code) ⇒ if (code != 0) log.info("Ret code {} executing script {}", code, script)
        }
        result.stdOut.find(_.contains("is running")).isDefined
    }
    CheckResult(service, valid, Map.empty)
  }

  def statusScriptFor(service: String): Either[String, ExecuteScript] = statusCommandParts match {
    case None ⇒ Left("not configured")
    case Some(parts) ⇒ parts match {
      case Nil        ⇒ Left("empty value")
      case cmd :: Nil ⇒ Left("not valid: " + parts)
      case cmd :: tail ⇒ tail.indexOf("_") match {
        case -1 ⇒ Left("no placeholder for service: " + parts)
        case x  ⇒ Right(ExecuteScript(cmd, tail.updated(x, service).toSeq))
      }
    }
  }

}

object ServiceAudit {
  val statusCommandPatternKey = "statusCommandPattern"
}

