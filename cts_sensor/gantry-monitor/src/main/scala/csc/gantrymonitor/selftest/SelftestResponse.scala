package csc.gantrymonitor.selftest

case class SelftestResponse(correlationId: String,
                            system: String,
                            gantry: String,
                            time: Long,
                            profile: String,
                            components: List[Component],
                            errorMessage: Option[String]) {

  val valid = components.size > 0 && components.forall(_.valid)

  def resultFor(name: String): Option[Component] = components.find(_.name == name)
}

case class Component(name: String,
                     family: String,
                     valid: Boolean,
                     checks: List[Check],
                     properties: List[Property])

case class Check(name: String, valid: Boolean)

case class Property(name: String, value: String)