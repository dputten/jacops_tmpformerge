package csc.gantrymonitor.selftest

import csc.akka.logging.DirectLogging

/**
 * trait Audit, following a typical template design pattern.
 * This should be used and extended when:
 * -the audit is 'simple' or its ok to be blocking (run synchronously)
 * -the audit is performed on several components (either configured or dynamic)
 * -there are several checks to be run on the components
 * -the checks can add properties to the Component final result
 *
 * Created by carlos on 24/08/16.
 */
trait Audit extends DirectLogging {

  def plan: SelftestPlan

  /**
   * Represents the function that runs a check on a given component.
   */
  type CheckExecutor = String ⇒ CheckResult

  val unknownCheck: CheckExecutor = { item ⇒ CheckResult(item, false, Map.empty) } //unknown check

  /**
   * Strategy method to get the components for this audit. By default returns the items in the plan
   * Should be overridden if the items are not configured, but dynamic
   * @return components to go through
   */
  protected def items: List[String] = plan.items

  def executors: PartialFunction[String, CheckExecutor]

  /**
   * Override to add more errors or supress the existing ones
   * @return List of errors that are an impediment for running the audit
   */
  protected def setupErrors: List[String] = {
    var result: List[String] = Nil
    if (items.isEmpty) result = result :+ "No components"
    if (plan.checks.isEmpty) result = result :+ "No checks"
    result
  }

  def execute(): Either[String, List[Component]] = {

    var results: Map[String, ComponentResult] = Map.empty

    def addResult(check: String, result: CheckResult): Unit = {
      val key = result.component
      results.get(key) match {
        case None      ⇒ results = results.updated(key, ComponentResult(Map(check -> result.valid), result.props))
        case Some(res) ⇒ results = results.updated(key, res.add(check, result))
      }
    }

    val errorMessages: List[String] = setupErrors

    errorMessages match {
      case Nil ⇒
        plan.checks foreach { check ⇒
          val executor = executors.lift.apply(check) getOrElse unknownCheck
          items foreach { item ⇒ addResult(check, executor(item)) }
        }
        val components = results.map { e ⇒ e._2.toComponent(e._1) }.toList
        Right(components)
      case list ⇒ Left(list.mkString(" | "))
    }
  }

  case class CheckResult(component: String, valid: Boolean, props: Map[String, String])

  case class ComponentResult(checks: Map[String, Boolean], props: Map[String, String]) {
    def add(check: String, result: CheckResult): ComponentResult =
      copy(checks = checks.updated(check, result.valid), props = props ++ result.props)

    def toComponent(name: String): Component = {
      val valid = !checks.values.toSet.contains(false)
      val properties = props map { e ⇒ Property(e._1, e._2) }
      val chks = plan.checks map { c ⇒ Check(c, checks.get(c) getOrElse (false)) }
      val family = plan.componentType.toString
      Component(name, family, valid, chks, properties.toList)
    }
  }
}
