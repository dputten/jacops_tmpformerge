/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.gantrymonitor.selftest

import akka.event.LoggingAdapter
import csc.akka.logging.DirectLogging

/**
 * The result of a NTP synchronisation poll for one peer.
 *
 * @param tallyCode
 * @param remote
 * @param refId
 * @param stratum
 * @param when
 * @param poll
 * @param reach
 * @param offset
 */
case class NTPPeerResult(tallyCode: String,
                         remote: String,
                         refId: String,
                         stratum: String,
                         when: String,
                         poll: String,
                         reach: String,
                         offset: String) {

  /**
   * return the Offset as float
   * @return
   */
  def getOffsetValue = offset.toFloat

  def getPollTimeMilliSeconds = poll.toLong * 1000 // exception will not happen (see regex used for parsing)

  /**
   * Return the when value in seconds
   *
   * @return
   */
  def getWhenMilliSeconds: Option[Long] = {
    val regex = """^(\d+)([smdh]?)$""".r
    when match {
      case regex(value, unit) ⇒ {
        val longValue = value.toLong //exception will never happen (see regex used for parsing)
        unit match {
          case "" | "s" ⇒ Some(1000 * longValue)
          case "m"      ⇒ Some(1000 * longValue * 60) // minutes
          case "h"      ⇒ Some(1000 * longValue * 60 * 60) // hours
          case "d"      ⇒ Some(1000 * longValue * 60 * 60 * 24) // days
        }
      }
      case other ⇒ None //This is the case "-"
    }
  }

  /**
   * Is this a local peer
   * @return
   */
  def isLocal: Boolean = remote.contains("LOCAL") || refId == ".LOCL."

  /**
   * get the reach as a binary string
   * example "11111111"
   * @return string representation of the reach buffer
   */
  def getReachAsBinaryString: String = {
    val intReach = Integer.parseInt(reach, 8)
    val bin = Integer.toString(intReach, 2)
    //pad the result with 0 until 8 positions
    "0000000" + bin takeRight 8
  }
}

object TrackNTPPeer {
  def apply(record: NTPPeerResult, now: Long) = {
    val track = new TrackNTPPeer(record)
    track.init(record, now)
    track
  }
}

/**
 * Trace the NTP synchronisation failures for one peer
 *
 * @param initialRecord
 */
class TrackNTPPeer(initialRecord: NTPPeerResult) extends DirectLogging {
  private var startNtpFailureTime = Long.MaxValue
  private var lastNtpRecord = initialRecord
  init(initialRecord, System.currentTimeMillis())

  /**
   * Initialize this class
   * @param record the initial NTP sync poll result
   * @param now the current time
   */
  def init(record: NTPPeerResult, now: Long) {
    lastNtpRecord = record
    startNtpFailureTime = if (lastNtpRecord.getReachAsBinaryString.endsWith("1")) {
      //No NTP failure
      Long.MaxValue
    } else {
      log.warning("Initialize NTP tracking. Estimate NTP failure start time")
      estimateMissedStart(record, now)
    }
  }

  /**
   * Estimate the start time of the failures using the number of failed polls (reach) and
   * the current poll time.
   * @param record The NTP sync poll result
   * @param now the current time
   * @return the estimated start time
   */
  def estimateMissedStart(record: NTPPeerResult, now: Long): Long = {
    val reach = record.getReachAsBinaryString
    //estimate the start time using poll time and reach
    //when there are no 1's lastOne will be -1 and the nrFailures is added 1
    //which results in the correct estimation
    val lastOne = reach.lastIndexOf("1")
    val nrFailures = (reach.length - 1) - lastOne
    if (nrFailures == 0) {
      Long.MaxValue
    } else {
      val when = record.getWhenMilliSeconds
      val pollTime = record.getPollTimeMilliSeconds

      //it looks like it differs how 'when' is filled.
      //CENTOS seems to fill it with the last successful poll
      //and in documentation it looks like it is filled with the last poll
      //When the "when" field is bigger than the polltime assume it contains the
      //time from the last successful
      //otherwise use the when and the number of poll times after the last poll
      val totalPollTime = when match {
        case None                                    ⇒ nrFailures * pollTime
        case Some(whenValue) if whenValue < pollTime ⇒ (nrFailures - 1) * pollTime + whenValue
        case Some(whenValue)                         ⇒ whenValue
      }
      now - totalPollTime
    }
  }

  /**
   * Update the start failure time.
   * The start time is needed when
   * the last sync failed OR when the previous sync failed, because the out of Sync detection
   * of the camera can happen when the NTP sync is start working again.
   *
   * @param record NTP poll record
   * @param now current time
   */
  def update(record: NTPPeerResult, now: Long) {

    //Update startTime
    if (lastNtpRecord.getReachAsBinaryString.endsWith("1")) {
      //previous NTP was OK
      //is current a start failure?
      startNtpFailureTime = record.getReachAsBinaryString match {
        case str if str.endsWith("10") ⇒
          record.getWhenMilliSeconds match {
            case None       ⇒ now - record.getPollTimeMilliSeconds
            case Some(when) ⇒ now - when
          }

        //strange looks like we missed the start
        case str if str.endsWith("00") ⇒
          log.warning("NTP synchronisation check. Missing NTP update. estimate start time")
          estimateMissedStart(record, now)
        //no error- at this moment there are minimal 2 correct synchronisations
        //the start time isn't used anymore
        case other ⇒ Long.MaxValue
      }
    } else {
      //previous NTP was in failure
      //so we still need the previous start failure time => no update
    }
    //update the record
    lastNtpRecord = record
  }

  /**
   * Get the NTP start failure time.
   *
   * @return start failure time. When there isn't a failure Long.maxValue is returned
   */
  def getStartTimeNtpFailureTime(): Long = {
    startNtpFailureTime
  }

  def setStartNtpFailureTime(time: Long): Unit = {
    startNtpFailureTime = time
  }
}

/**
 * Trace the NTP synchronisation over multiple peers
 */
class TrackNTP extends DirectLogging {

  /**
   * collection of all current NTP peers
   */
  var peers = Map[String, TrackNTPPeer]()

  /**
   * Update it state using the ntpq output
   * @param lines ntpq output
   * @param now current time
   */
  def update(lines: Seq[String], now: Long) {
    val (errors, records) = NTPParser.parseNtpqOutput(lines, log)
    if (!errors.isEmpty) {
      log.warning("problems found while parsing ntpq result")
      errors.foreach(log.info)
    }
    val peerRecords = records.filterNot(_.isLocal)
    peers = if (peerRecords.isEmpty) {
      log.warning("Did not find any ntp peers")
      peers.mapValues((peerTracker: TrackNTPPeer) ⇒ {
        // Todo: make TrackNTPPeer a case class
        if (peerTracker.getStartTimeNtpFailureTime() == Long.MaxValue)
          peerTracker.setStartNtpFailureTime(now)
        peerTracker
      })
    } else {
      peerRecords.map(rec ⇒ {
        val peerRemote = rec.remote
        val peerTrack = peers.get(peerRemote) match {
          case Some(peer) ⇒ {
            peer.update(rec, now)
            peer
          }
          case None ⇒ TrackNTPPeer(rec, now)
        }
        (peerRemote, peerTrack)
      }).toMap
    }
  }

  /**
   * Get the NTP start failure time.
   * @return start failure time. When there isn't a failure Long.maxValue is returned, any other value indicates an error
   */
  def getStartTimeNtpFailure: Long = {
    peers.values.foldLeft(0L) { (time, peerTrack) ⇒
      {
        val peerTime = peerTrack.getStartTimeNtpFailureTime()
        math.max(time, peerTime)
      }
    }
  }

}

object NTPParser {

  /**
   * Parse all the ntpq output lines
   * @param lines the ntpq output lines
   * @return list of parse errors and a list of peer poll results
   */
  def parseNtpqOutput(lines: Seq[String], log: LoggingAdapter): (Seq[String], Seq[NTPPeerResult]) = {
    log.info("Start parsing [%s]".format(lines))
    /* output example
    remote           refid           st t  when poll reach delay    offset  jitter
    ==============================================================================
    *CTES-A4-1.a4.ct 89.221.177.165   4 u  940 1024  377   53.994   -5.386   7.134
     LOCAL(1)        .LOCL.          12 l  51d   64    0    0.000    0.000   0.000

    */

    val reg = """(.)(\S+)\s+(\S+)\s+(\d+)\s+\S\s+(-|\d+[smdh]?)\s+(\d+)\s+([0-7]+)\s+[-\d.]+\s+([-\d.]+)\s+[-\d.]+""".r
    val header = """\s*remote[\s]+refid[\s]+st[\s]+t[\s]+when[\s]+poll[\s]+reach[\s]+delay[\s]+offset[\s]+jitter""".r
    val headerLine = """=+""".r

    val results = lines.map(line ⇒ {
      line match {
        case reg(tallyCode, remote, refId, stratum, when, poll, reach, offset) ⇒ {
          Right(NTPPeerResult(tallyCode, remote, refId, stratum, when, poll, reach, offset))
        }
        case header()     ⇒ Left("") //nothing to do
        case headerLine() ⇒ Left("") //nothing to do
        case other        ⇒ Left("Parsing Failed for line [%s]".format(line))
      }
    })
    val split = results.groupBy(_ match {
      case Left("") ⇒ "empty"
      case Left(_)  ⇒ "errors"
      case Right(_) ⇒ "NTPLine"
    })

    var errors = split.getOrElse("errors", List()).map(_.left.get)
    val ntp = split.getOrElse("NTPLine", List()).map(_.right.get)
    (errors, ntp)
  }

  def activePeer(peers: Seq[NTPPeerResult]): Option[NTPPeerResult] = peers.find(_.tallyCode == "*")

}