package csc.gantrymonitor.selftest

import csc.akka.json.JsonUtil
import csc.akka.logging.DirectLogging
import org.apache.commons.io.IOUtils
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.DefaultHttpClient

import scala.util.parsing.json.JSON

/**
 * Created by carlos on 26/08/16.
 */

trait QueueAdmin {

  def getQueueNames(): Either[String, List[String]]

  def getQueueDetails(name: String): Either[String, QueueDetail]

}

case class QueueDetail(name: String, state: String, messageCount: Long, publishRate: Option[Double], deliverRate: Option[Double])

trait HttpClient {

  def get(path: String): Either[String, String]

}

class RealHttpClient(baseUrl: String) extends HttpClient {

  val client = new DefaultHttpClient()

  private def apiUrlFor(path: String): String = "%s/api/%s".format(baseUrl, path)

  def get(path: String): Either[String, String] = {
    try {
      val request = new HttpGet(apiUrlFor(path))
      val response = client.execute(request);
      val status = response.getStatusLine
      status.getStatusCode match {
        case 200   ⇒ Right(IOUtils.toString(response.getEntity().getContent()))
        case other ⇒ Left(status.toString)
      }

    } catch {
      case ex: Exception ⇒ Left(ex.toString)
    }
  }

}

class RabbitMQRestAdmin(client: HttpClient)
  extends QueueAdmin
  with RabbitMQRestApi
  with DirectLogging {

  override def getQueueNames(): Either[String, List[String]] = {
    client.get(queueListUrl) match {
      case Left(error) ⇒ Left(error)
      case Right(content) ⇒ JSON.parseFull(content) match {
        case Some(list: List[Map[String, _]]) ⇒ Right(list map (e ⇒ e("name").toString))
        case other                            ⇒ Left("Unexpected JSON: " + other)
      }
    }
  }

  override def getQueueDetails(name: String): Either[String, QueueDetail] = {
    client.get(queueDetailUrl(name)) match {
      case Left(error) ⇒ Left(error)
      case Right(content) ⇒ JSON.parseFull(content) match {
        case Some(map: Map[_, _]) ⇒ QueueAdmin.parseQueueDetail(name, map)
        case other                ⇒ Left("Unexpected JSON: " + other)
      }
    }
  }

}

trait RabbitMQRestApi {

  private val queueDetailColumns = List("messages", "message_stats", "state")

  def queueListUrl = restApiUrl("queues", List("name"))

  def queueDetailUrl(name: String) = restApiUrl("queues/%2f/" + name, queueDetailColumns)

  def restApiUrl(path: String, columns: List[String]): String = columns match {
    case Nil  ⇒ path
    case list ⇒ path + "?columns=" + list.mkString(",")
  }

}

object QueueAdmin {

  type HttpClientFactory = String ⇒ HttpClient

  val defaultHttpClientFactory: HttpClientFactory = { baseUrl ⇒ new RealHttpClient(baseUrl) }

  def parseQueueDetail(name: String, map: Map[_, _]): Either[String, QueueDetail] = {
    try {
      val json = new JsonUtil(map)
      val state = json.stringOpt("state").get
      val messageCount = json.longOpt("messages").get
      val publishRate = json.doubleOpt("message_stats.publish_details.rate")
      val deliverRate = json.doubleOpt("message_stats.deliver_details.rate")
      Right(QueueDetail(name, state, messageCount, publishRate, deliverRate))

    } catch {
      case ex: Exception ⇒
        Left("Error %s parsing json for queue %s in %s".format(ex.toString, name, map))
    }
  }

}

