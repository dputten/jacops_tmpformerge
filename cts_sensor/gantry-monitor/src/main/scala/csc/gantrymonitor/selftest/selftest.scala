package csc.gantrymonitor.selftest

import akka.actor.ActorRef
import akka.actor.Props
import com.typesafe.config.Config
import csc.gantrymonitor.selftest.ComponentType.ComponentType

object Selftest {

  def fallbackKey = "default"
  def settingsKey = "settings"
  val unknownValue = "???"

  object CameraChecks {
    val cameraImage = "camera-image" //requests an image capture
    val imageConversion = "image-conversion" //gets and converts the image to jpg
    val imageTime = "image-time" //checks time differences
    val legacy = List(cameraImage, imageConversion)
    val selftest = List(cameraImage, imageTime)
  }

  object StorageChecks {
    val freeSpace = "freeSpace"
    val all = List(freeSpace)
  }

  object StorageProperties {
    val freeBytes = "freeBytes"
    val freeRatio = "freeRatio"
    val all = List(freeBytes, freeRatio)
  }

  object ServiceChecks {
    val running = "running"
    val all = List(running)
  }

  object ClockChecks {
    val offset = "offset"
    val all = List(offset)
  }

  object ClockProperties {
    val offset = "offset"
    val poll = "poll"
    val all = List(offset, poll)
  }

  object QueueChecks {
    val report = "report"
    val all = List(report)
  }

  object QueueProperties {
    val state = "state"
    val messageCount = "messageCount"
    val publishRate = "publishRate"
    val deliverRate = "deliverRate"
    val all = List(state, messageCount, publishRate, deliverRate)
  }

  object FtpChecks {
    val report = "report"
    val recentActivity = "recentActivity"
    val all = List(report, recentActivity)
  }

  object FtpProperties {
    val lastActivity = "lastActivity"
    val all = List(lastActivity)
  }
}

object ComponentType extends Enumeration {
  type ComponentType = Value
  val camera, storage, clock, service, queue, ftp = Value

  lazy val names: Set[String] = values map (_.toString)
}

case class SelftestProfile(plans: Map[ComponentType, SelftestPlan])

case class SelftestPlan(componentType: ComponentType,
                        items: List[String],
                        checks: List[String],
                        configs: Map[String, Config]) {

  lazy val settings: Option[Config] = configs.get(Selftest.settingsKey)

}

/**
 * Trait that abstracts the creation (akka Props) of the top selftest actor, producer and consumer
 */
trait SelftestActorProps {

  def producerProps(amqpProducer: ActorRef): Props

  def actorProps(producer: ActorRef): Props

  def consumerProps(actor: ActorRef): Props

}

