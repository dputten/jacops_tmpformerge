package csc.gantrymonitor.selftest

import csc.akka.config.ConfigUtil.configUtil
import csc.gantrymonitor.selftest.QueueAdmin.HttpClientFactory
import csc.gantrymonitor.selftest.Selftest.{ QueueProperties, QueueChecks }

/**
 * Created by carlos on 25/08/16.
 */
class QueueAudit(val plan: SelftestPlan, clientFactory: HttpClientFactory) extends Audit {

  import QueueAudit._

  val queueAdmin: Option[QueueAdmin] = {
    val baseUrl: Option[String] = plan.settings flatMap (cfg ⇒ cfg.getStringOpt(adminBaseUrlKey))
    val httpClient: Option[HttpClient] = baseUrl map (clientFactory(_))
    httpClient map (new RabbitMQRestAdmin(_))
  }

  lazy val queueNames: List[String] = queueAdmin match {
    case None ⇒ Nil
    case Some(admin) ⇒ admin.getQueueNames() match {
      case Right(list) ⇒ list
      case Left(error) ⇒
        log.warning("Error getting queue names: {}", error.toString)
        Nil
    }
  }

  override protected def items: List[String] = queueNames //items are dynamic (queues in server)

  override def executors: PartialFunction[String, CheckExecutor] = {
    case QueueChecks.report ⇒ reportCheck
  }

  def reportCheck(queue: String): CheckResult = {
    val detail: Option[QueueDetail] = queueAdmin flatMap { admin ⇒
      admin.getQueueDetails(queue) match {
        case Right(detail) ⇒ Some(detail)
        case Left(error) ⇒
          log.warning("Unable to get details for queue {}: {}", queue, error)
          None
      }
    }
    detail match {
      case Some(value) ⇒ CheckResult(queue, true, toProps(value))
      case None        ⇒ CheckResult(queue, false, Map.empty)
    }
  }

}

object QueueAudit {

  val adminBaseUrlKey = "adminBaseUrl"

  def toProps(queue: QueueDetail): Map[String, String] = Map(
    QueueProperties.state -> queue.state,
    QueueProperties.messageCount -> queue.messageCount.toString,
    QueueProperties.publishRate -> queue.publishRate.map(_.toString).getOrElse(Selftest.unknownValue),
    QueueProperties.deliverRate -> queue.deliverRate.map(_.toString).getOrElse(Selftest.unknownValue))

}