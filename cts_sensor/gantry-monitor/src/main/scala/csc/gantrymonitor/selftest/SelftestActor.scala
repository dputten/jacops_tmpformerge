package csc.gantrymonitor.selftest

import akka.actor.SupervisorStrategy.Restart
import akka.actor._
import com.github.sstone.amqp.Amqp.{ Reject, Delivery, Ack }
import csc.amqp.{ PublisherActor, AmqpSerializers }
import csc.gantrymonitor.calibration.ImageFunctions
import csc.gantrymonitor.caseclasses.StartedJaiMonitor

import ComponentType.ComponentType
import csc.gantrymonitor.config.{ SelfTestConfiguration, Configuration }
import csc.gantrymonitor.selftest.SelftestActor.SelftesterActorPropsFactory
import net.liftweb.json.DefaultFormats

import SelftestActor.now

import scala.concurrent.duration._

/**
 * Actor managing the selftest, responsible for:
 * -receive SelftestRequests
 * -obtain the selftest plans for the given profile
 * -send a ComponentSelftestRequest for every component type in the selftest profile plans
 * -wait for all the ComponentSelftestResponses
 * -when no more ComponentSelftestResponses are expected, send the SelftestResponse to targetActor
 *
 * This actor will only perform one selftest at any moment.
 * So if a SelftestRequest is received while another is in progress, its added to a pending list and picked up later.
 *
 * Created by carlos on 26/07/16.
 */
class SelftestActor(profiles: Map[String, SelftestProfile],
                    timeout: FiniteDuration,
                    actorPropsFactory: SelftesterActorPropsFactory,
                    targetActor: ActorRef)
  extends Actor with ActorLogging {

  implicit val executor = context.system.dispatcher

  var currentProcess: Option[Process] = None

  var pendingRequests: Seq[SelftestRequest] = Nil

  override def receive: Receive = {
    case msg: SelftestRequest           ⇒ handleRequest(msg)
    case msg: ComponentSelftestResponse ⇒ handleComponentResponse(msg)
    case msg: SelftestTimeout           ⇒ handleTimeout(msg)
    case other                          ⇒ log.warning("Don't know how to handle {}", other)
  }

  private def handleTimeout(msg: SelftestTimeout): Unit = currentProcess match {
    case Some(proc) if (proc.id == msg.id) ⇒
      log.info("Selftest {} timedout, waiting for responses of {}. Partial results are {}. Sending response",
        proc.id, proc.remainingTypes, proc.getResults)
      finish(proc.withFatalError("Timed out")) //send back the timed out response

    case _ ⇒ //dry timeout: ignore silently
  }

  private def handleRequest(req: SelftestRequest): Unit = currentProcess match {
    case None ⇒
      log.info("No current processes. Immediately start processing {}", req)
      processRequest(req)
    case Some(_) ⇒
      log.info("Some selftest in progress. Adding {} to pending", req)
      pendingRequests = pendingRequests :+ req
  }

  private def handleComponentResponse(msg: ComponentSelftestResponse): Unit = currentProcess match {
    case Some(proc) if (proc.shouldHandle(msg)) ⇒
      log.info("Received selftest component response {}", msg)
      checkProgress(proc.withResponse(msg))
    case Some(proc) ⇒
      log.warning("Received {}, but was expecting a response for {}. Discarding", msg, proc.id)
    case None ⇒
      log.warning("Not expecting message {}, probably late or timed out. Discarding", msg)
  }

  private def processRequest(req: SelftestRequest): Unit = profiles.get(req.profile) match {
    case None ⇒
      log.info("Request {} got no profile. Sending empty (invalid) response", req)
      targetActor ! emptyResponse(req, "Unsupported profile")
    case Some(profile) ⇒
      process(req, profile)
  }

  private def checkProgress(proc: Process): Unit = proc.done match {
    case true  ⇒ finish(proc)
    case false ⇒ currentProcess = Some(proc)
  }

  private def finish(entry: Process): Unit = {
    val response = entry.toResponse
    log.info("Sending response for {}: {}", entry.id, response)
    targetActor ! response
    currentProcess = None

    pendingRequests match {
      case Nil ⇒
      case req :: tail ⇒
        //handle the 1st request pending
        pendingRequests = tail
        log.info("Processing first pending request {}", req)
        processRequest(req)
    }
  }

  private def process(req: SelftestRequest, profile: SelftestProfile): Unit = {

    val startTime = now
    val id = SelftestId(req.correlationId, req.system, req.gantry, req.profile)

    var errorMessages: List[String] = Nil
    var remainingTypes: Set[ComponentType] = profile.plans.keySet

    //delegates selftest for each component type to another actor
    profile.plans foreach { e ⇒
      val componentType = e._1
      val plan = e._2
      actorPropsFactory(componentType) match {
        case None ⇒
          errorMessages = errorMessages :+ ("Unable to obtain a selftester actor for " + componentType)
        case Some(props) ⇒
          val actor = context.actorOf(props, getActorName(id, componentType))
          actor ! ComponentSelftestRequest(id, plan, startTime)
          remainingTypes = remainingTypes + componentType //adds to the waiting responses list
      }
    }

    val proc = Process(id, remainingTypes, startTime, Map.empty, errorMessages, None)

    proc.endTime match {
      case None ⇒
        currentProcess = Some(proc) //respond later...
        context.system.scheduler.scheduleOnce(timeout, self, SelftestTimeout(id)) //...with a timeout
      case _ ⇒ finish(proc) //respond now
    }

    log.debug("Current process is {}", proc)
  }

  def getActorName(id: SelftestId, ct: ComponentType): String =
    "%s-%s-%s-%s-%s".format(id.correlationId, id.system, id.gantry, id.profile, ct.toString)

  private def emptyResponse(req: SelftestRequest, reason: String): SelftestResponse =
    SelftestResponse(req.correlationId, req.system, req.gantry, now, req.profile, Nil, Some(reason))

  case class Process(id: SelftestId,
                     remainingTypes: Set[ComponentType],
                     startTime: Long,
                     responses: Map[ComponentType, ComponentSelftestResponse],
                     errorMessages: List[String],
                     endTime: Option[Long]) {

    def toResponse: SelftestResponse = {
      val time = endTime getOrElse now

      val (components, errorMessage) = errorMessages match {
        case Nil  ⇒ (getResults, None)
        case list ⇒ (Nil, Some(list.mkString(" / ")))
      }

      SelftestResponse(id.correlationId, id.system, id.gantry, time, id.profile, components, errorMessage)
    }

    def getResults: List[Component] = responses.values.toList.map(_.results).flatten

    def shouldHandle(response: ComponentSelftestResponse): Boolean = id == response.id

    def withResponse(response: ComponentSelftestResponse): Process =
      copy(remainingTypes = remainingTypes - response.componentType,
        responses = responses.updated(response.componentType, response),
        errorMessages = errorMessages ++ response.errorMessage)

    def withFatalError(errorMessage: String): Process =
      copy(endTime = Some(now), errorMessages = errorMessages :+ errorMessage)

    def done: Boolean = endTime.isDefined || remainingTypes.isEmpty
  }

}

object SelftestActor {

  type SelftesterActorPropsFactory = ComponentType ⇒ Option[Props]

  def now: Long = System.currentTimeMillis()

}

/**
 * Represents all the fields that identify or locate a unique single selftest request
 * @param correlationId
 * @param system
 * @param gantry
 * @param profile
 */
case class SelftestId(correlationId: String, system: String, gantry: String, profile: String)

case class SelftestTimeout(id: SelftestId)

case class ComponentSelftestRequest(id: SelftestId, plan: SelftestPlan, time: Long) {

  def toResponse(components: List[Component], error: Option[String]): ComponentSelftestResponse =
    ComponentSelftestResponse(id, plan.componentType, now, components, error)

}

case class ComponentSelftestResponse(id: SelftestId,
                                     componentType: ComponentType,
                                     sendTime: Long,
                                     results: List[Component],
                                     errorMessage: Option[String]) {

  def resultFor(name: String): Option[Component] = results.find(_.name == name)

}

class SelftestConsumer(target: ActorRef) extends Actor with ActorLogging with AmqpSerializers {

  implicit val system = context.system
  override implicit val formats = DefaultFormats
  override val ApplicationId = "SelftestConsumer"

  object SelftestRequestExtractor extends MessageExtractor[SelftestRequest]

  def receive = {
    case delivery @ SelftestRequestExtractor(msg) ⇒ {
      val consumer = sender
      log.info("Received SelftestRequest {}", msg)
      target ! msg
      consumer ! Ack(delivery.envelope.getDeliveryTag)
    }
    case delivery: Delivery ⇒ {
      log.info("Received Delivery")
      val consumer = sender
      log.error("Dropping message with tag {} - cannot determine message type", delivery.envelope.getDeliveryTag)
      consumer ! Reject(delivery.envelope.getDeliveryTag, false)
    }
  }
}

class SelftestProducer(val producer: ActorRef, val producerEndpoint: String)
  extends PublisherActor with ActorLogging {

  override val ApplicationId = "selftest"
  override implicit val formats = DefaultFormats
  def responseName = "SelftestResponse"

  override val supervisorStrategy = OneForOneStrategy() {
    case _: Exception ⇒ Restart
  }

  def producerRouteKey(msg: AnyRef): String = {
    msg match {
      case response: SelftestResponse ⇒ {
        response.system + "." + response.gantry + "." + responseName
      }
      case other ⇒ msg.getClass.getName
    }
  }
}

/**
 * SelftestActorProps impl for the new generic selftest design
 * @param config
 */
class GenericSelftestProps(config: SelfTestConfiguration,
                           producerEndpoint: String,
                           jaiMonitors: ⇒ Seq[StartedJaiMonitor],
                           imageFunctions: ImageFunctions)
  extends SelftestActorProps {

  def this(config: Configuration, jaiMonitors: ⇒ Seq[StartedJaiMonitor]) =
    this(config.monitorConfiguration.selfTest, config.amqpCalibrationServicesConfiguration.producerEndpoint,
      jaiMonitors, config.imageFunctions)

  override def producerProps(amqpProducer: ActorRef): Props =
    Props(new SelftestProducer(amqpProducer, producerEndpoint))

  override def actorProps(producer: ActorRef): Props =
    Props(new SelftestActor(config.profiles, config.timeout, selftesterPropsFactory, producer))

  override def consumerProps(actor: ActorRef): Props =
    Props(new SelftestConsumer(actor))

  val selftesterPropsFactory: SelftesterActorPropsFactory = { componentType ⇒
    val props: Props = componentType match {
      case ComponentType.camera  ⇒ Props(new CameraSelftester(jaiMonitors, config, imageFunctions))
      case ComponentType.storage ⇒ GenericAuditActor.props(componentType)
      case ComponentType.service ⇒ GenericAuditActor.props(componentType)
      case ComponentType.clock   ⇒ GenericAuditActor.props(componentType)
      case ComponentType.queue   ⇒ GenericAuditActor.props(componentType)
      case ComponentType.ftp     ⇒ GenericAuditActor.props(componentType)
      case other                 ⇒ null
    }
    Option(props)
  }
}
