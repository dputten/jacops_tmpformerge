package csc.gantrymonitor.selftest

import akka.actor.{ Props, ActorLogging, Actor }
import csc.gantrymonitor.selftest.ComponentType.ComponentType
import csc.gantrymonitor.selftest.GenericAuditActor.AuditFactory

/**
 * Created by carlos on 24/08/16.
 */
class GenericAuditActor(compType: ComponentType, auditFactory: AuditFactory) extends Actor with ActorLogging {

  def receive = {
    case req: ComponentSelftestRequest if req.plan.componentType == compType ⇒ handleRequest(req)
    case msg: ComponentSelftestRequest ⇒
      log.warning("Not for me: {}", msg)
      sender ! msg.toResponse(Nil, Some("Wrong handler"))
  }

  def handleRequest(req: ComponentSelftestRequest): Unit = {
    val client = sender
    val (components, errorMessage) = auditFactory.apply(req.plan) match {
      case Right(process) ⇒ process.execute() match {
        case Right(list) ⇒ (list, None)
        case Left(error) ⇒ (Nil, Some(error))
      }

      case Left(msg) ⇒ (Nil, Some(msg))

    }
    client ! req.toResponse(components, errorMessage)
  }

}

object GenericAuditActor {

  type AuditFactory = SelftestPlan ⇒ Either[String, Audit]

  def props(compType: ComponentType): Props = {
    Props(new GenericAuditActor(compType, AuditFactory))
  }

}

object AuditFactory extends AuditFactory {
  override def apply(plan: SelftestPlan): Either[String, Audit] = plan.componentType match {
    case ComponentType.storage ⇒ Right(new StorageAudit(plan))
    case ComponentType.service ⇒ Right(new ServiceAudit(plan))
    case ComponentType.clock   ⇒ Right(new ClockAudit(plan))
    case ComponentType.queue   ⇒ Right(new QueueAudit(plan, QueueAdmin.defaultHttpClientFactory))
    case ComponentType.ftp     ⇒ Right(new FtpAudit(plan))
    case other                 ⇒ Left("Not supported: " + other)
  }
}
