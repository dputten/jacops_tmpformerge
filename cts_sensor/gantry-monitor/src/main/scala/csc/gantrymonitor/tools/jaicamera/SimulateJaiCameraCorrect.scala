package csc.gantrymonitor.tools.jaicamera

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

import csc.akka.logging.DirectLogging
import java.io.{ File, IOException, InputStreamReader, BufferedReader }
import org.apache.commons.io.FileUtils
import _root_.tools.jaiconnection.SimulateJai
import csc.vehicle.jai.StatusCode

/**
 * This is the object which starts the camera simulator.
 *
 */
object SimulateJaiCameraCorrect extends DirectLogging {
  type OptionMap = Map[Symbol, String]

  /**
   * Parse the command arguments
   */
  def nextOption(map: OptionMap, list: List[String]): OptionMap = {
    list match {
      case Nil ⇒ map
      case "--host" :: value :: tail ⇒
        nextOption(map ++ Map('host -> value), tail)
      case "--help" :: tail ⇒
        nextOption(map ++ Map('help -> ""), tail)
      case "--port" :: value :: tail ⇒
        nextOption(map ++ Map('port -> value), tail)
      case "--image" :: value :: tail ⇒
        nextOption(map ++ Map('image -> value), tail)
      case option :: tail ⇒
        println("Unknown option " + option)
        sys.exit(1)
    }
  }

  /**
   * Prints the usage string to standard out
   */
  def printUsage() {
    println("Simulator Usage  [--host <host>] [--port <port>] [--image <image>] [options]")
    println("\t--host <host>: interface to bind too. default localhost")
    println("\t--port <port>: the port of the camera. default 1400")
    println("\t--image <image>: the image the camera sends when triggered")
    println("\t--help: show the usage and script usage")
  }

  /**
   * The main tread.
   * Parse arguments and check them for correctness
   */
  def main(args: Array[String]): Unit = {
    val options = nextOption(Map(), args.toList)
    println(options)
    options.get('help) foreach {
      value ⇒
        {
          printUsage
          sys.exit(0)
        }
    }

    val host = options.get('host).getOrElse("localhost")
    val port = options.get('port).getOrElse("1400")

    val jaiSimulator = new SimulateJai(host, port.toInt)
    resetErrors(jaiSimulator)
    val imageOpt = options.get('image)
    imageOpt.foreach(image ⇒ {
      sendImage(jaiSimulator, image)
    })
    //wait
    try {
      var process = true
      val stopFile = new File("stop.txt")
      println("wait until file [%s] exists".format(stopFile.getAbsolutePath))
      while (process) {
        Thread.sleep(60000)
        if (stopFile.exists()) {
          process = false
        }
      }
    } catch {
      case ex: IOException ⇒ System.out.println("IO error while waiting. resuming simulator");
    }

    //stop system
    System.out.println("Stoping Simulator")

    jaiSimulator.stopJaiCamera()
    System.out.println("Logger Simulator")
    sys.exit(0)
  }

  def sendImage(sim: SimulateJai, fileName: String) {
    val file = new File(fileName)
    if (file.exists() && file.isFile) {
      try {
        val bytes = FileUtils.readFileToByteArray(file)
        sim.setImage(bytes, true)
      } catch {
        case ex: Exception ⇒ println("Unable to read File %s".format(file.getAbsolutePath))
      }
    } else {
      println("File %s doesn't exists".format(file.getAbsolutePath))
    }
  }

  def resetErrors(sim: SimulateJai) {
    sim.updateStatus(StatusCode.ERROR_STATUS, 0L)
    sim.updateStatus(StatusCode.NTP_ESTIMATE_ERROR, 0L)
    sim.updateStatus(StatusCode.NTP_RESTART_COUNTER, 0L)
    sim.updateStatus(StatusCode.NTP_STATUS, 1L)
  }
}
