package csc.gantrymonitor.tools.jaicamera

import java.io.{ BufferedReader, File, FileReader, StringReader }

import csc.akka.remote.SocketServer
import csc.base.AppConfigReader
import csc.gantrymonitor.config.Configuration
import csc.gantrymonitor.stub.JaiCameraStubClient

/**
 * Created by carlos on 28/04/16.
 */
object JaiCameraStubControl {

  def main(args: Array[String]) {

    val cfg = new Configuration(AppConfigReader.getAppConfig(None))
    val ctl = getStubControl(cfg.stubUri)

    ctl foreach { client ⇒
      val (toClose, reader) = getReader(args)
      try {
        client.process(reader)
      } finally {
        if (toClose) reader.close()
      }
    }
  }

  def getReader(args: Array[String]): (Boolean, BufferedReader) = args match {
    case array if args.isEmpty ⇒
      println("Reading from standard input. Issue your stub commands now:")
      (false, Console.in)
    case Array("-f", filename) ⇒
      val file = new File(filename)
      if (file.isFile && file.canRead) {
        println("Reading from file " + file)
        (true, new BufferedReader(new FileReader(file)))
      } else throw new IllegalArgumentException("Unable to read file " + file.getAbsolutePath)
    case Array("-s", cmd) ⇒
      (true, new BufferedReader(new StringReader(cmd)))
    case _ ⇒ throw new IllegalArgumentException("Unsupported args: " + args)
  }

  def getStubControl(uriOpt: Option[String]): Option[JaiCameraStubControl] = uriOpt match {
    case None ⇒
      println("Camera stub URI not present or not enabled")
      None
    case Some(uri) ⇒ SocketServer(uri) match {
      case Some(server) ⇒ Some(new JaiCameraStubControl(server))
      case None ⇒
        println("Could not parse host/port from uri " + uri)
        None
    }
  }

}

class JaiCameraStubControl(server: SocketServer) extends JaiCameraStubClient(server) {

  def process(reader: BufferedReader): Unit = {
    Iterator.continually(reader.readLine).takeWhile(_ != null).foreach { line ⇒
      send(None, line)
    }
  }

}
