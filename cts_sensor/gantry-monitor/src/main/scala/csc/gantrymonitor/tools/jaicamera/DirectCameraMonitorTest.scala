package csc.gantrymonitor.tools.jaicamera

import java.io.BufferedReader
import java.net.InetAddress
import java.util.Date
import java.util.concurrent.TimeUnit

import akka.actor._
import scala.concurrent.duration.{ FiniteDuration, Duration }
import csc.gantry.config.CameraConfig
import csc.gantrymonitor.jai._
import csc.systemevents.SystemEvent
import csc.vehicle.message.Camera

/**
 * Created by carlos on 10/08/16.
 */
object DirectCameraMonitorTest {

  lazy val system = ActorSystem("DirectCameraMonitorTest")
  private var monitor: Option[ActorRef] = None

  def main(args: Array[String]) {
    val (camId: String, watchdogFreq: Option[FiniteDuration]) = args.toList match {
      case camId :: freq :: _ ⇒ (camId, Some(Duration(freq.toLong, TimeUnit.MILLISECONDS)))
      case camId :: _         ⇒ (camId, None)
      case _                  ⇒ throw new IllegalArgumentException("Usage: cameraMonitorTest.sh <cameraId> [<watchdogFrequency>]")
    }

    monitor = Some(createMonitor(camId, watchdogFreq))

    monitor.get ! "Just checking"

    while (true) {
      val time = new Date()
      println("Sleeping at " + time + ", " + time.getTime)
      Thread.sleep(60000)
    }

    system.shutdown()
  }

  private def processInput(reader: BufferedReader): Unit = {
    var done = false

    Iterator.continually(reader.readLine).takeWhile(l ⇒ !done).foreach {
      line ⇒
        line.toLowerCase.trim match {
          case "exit"   ⇒ done = true
          case "status" ⇒
          case "image"  ⇒
        }
    }
  }

  val alarmDelay = Duration(5, TimeUnit.MINUTES)

  def createMonitor(camId: String, watchdogFreq: Option[FiniteDuration]): ActorRef = {
    val pingActor = JaiMonitor.connectionPingProps(watchdogFreq) map (system.actorOf(_))

    val camera = Camera(camId, camId.substring(0, 2), "A4")
    val ipAddress = InetAddress.getByName(camId).getHostAddress
    println("ip address is " + ipAddress)
    val config = CameraConfig("/data/images/raw/" + camId, ipAddress, 1400, 5, 5000)

    val timeout = watchdogFreq getOrElse FiniteDuration(5, TimeUnit.SECONDS)
    val props = Props(new JaiMonitor(camera, config, JaiMonitor.cameraTriggerId, alarmDelay, 0, timeout, pingActor))
    system.actorOf(props)
  }

}

class ClientActor(target: ActorRef) extends Actor with ActorLogging {
  override def receive: Receive = {
    case msg: CameraRequest     ⇒ target ! msg
    case msg: JaiStatusResponse ⇒
    case msg: JaiImageError     ⇒
    case msg: SystemEvent       ⇒ //subscribe on stream
  }
}
