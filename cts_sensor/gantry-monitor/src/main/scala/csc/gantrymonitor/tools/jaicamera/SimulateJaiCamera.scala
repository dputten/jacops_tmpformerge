/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.gantrymonitor.tools.jaicamera

import csc.akka.logging.DirectLogging
import java.io.{ IOException, InputStreamReader, BufferedReader }
import _root_.tools.jaiconnection.{ JaiCameraSimulator, SimulateJai }

/**
 * This is the object which starts the camera simulator.
 *
 */
object SimulateJaiCamera extends DirectLogging {
  type OptionMap = Map[Symbol, String]
  var reader = new BufferedReader(new InputStreamReader(System.in))

  /**
   * Parse the command arguments
   */
  def nextOption(map: OptionMap, list: List[String]): OptionMap = {
    list match {
      case Nil ⇒ map
      case "--host" :: value :: tail ⇒
        nextOption(map ++ Map('host -> value), tail)
      case "--help" :: tail ⇒
        nextOption(map ++ Map('help -> ""), tail)
      case "--port" :: value :: tail ⇒
        nextOption(map ++ Map('port -> value), tail)
      case "--image" :: value :: tail ⇒
        nextOption(map ++ Map('image -> value), tail)
      case option :: tail ⇒
        println("Unknown option " + option)
        sys.exit(1)
    }
  }

  /**
   * Prints the usage string to standard out
   */
  def printUsage() {
    println("Simulator Usage  [--host <host>] [--port <port>] [--image <image>] [options]")
    println("\t--host <host>: interface to bind too. default localhost")
    println("\t--port <port>: the port of the camera. default 1400")
    println("\t--image <image>: the image the camera sends when triggered")
    println("\t--help: show the usage and script usage")
  }

  /**
   * The main tread.
   * Parse arguments and check them for correctness
   */
  def main(args: Array[String]): Unit = {
    val options = nextOption(Map(), args.toList)
    println(options)
    options.get('help) foreach {
      value ⇒
        {
          printUsage
          sys.exit(0)
        }
    }

    val host = options.get('host).getOrElse("localhost")
    val port = options.get('port).getOrElse("1400")

    val jaiSimulator = new JaiCameraSimulator(new SimulateJai(host, port.toInt))
    jaiSimulator.resetErrors()
    val imageOpt = options.get('image)
    imageOpt.foreach(image ⇒ {
      jaiSimulator.sendImage(image)
    })
    //wait
    var reader = new BufferedReader(new InputStreamReader(System.in))
    var inputLine: String = null;
    //  readLine() method
    println("Logger: To use a new image: image <image><enter>")
    println("Logger: To clear errors: reset<enter>")
    println("Logger: To set NTP error: NTP<enter>")
    println("Logger: To end process type: exit<enter>")

    try {
      var process = true
      while (process) {
        System.out.print("Wait for command input: ");
        inputLine = reader.readLine();
        if (inputLine.startsWith("exit")) {
          process = false
        } else {
          jaiSimulator.process(inputLine)
        }
      }
    } catch {
      case ex: IOException ⇒ System.out.println("IO error while waiting for input. resuming scriptprocessing");
    } finally {
      reader.close()
    }

    //stop system
    System.out.println("Stopping Simulator")

    jaiSimulator.sim.stopJaiCamera()
    System.out.println("Logger Simulator")
    sys.exit(0)
  }

}
