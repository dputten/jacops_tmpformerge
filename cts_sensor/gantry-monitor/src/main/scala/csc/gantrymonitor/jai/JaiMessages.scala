/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.gantrymonitor.jai

import csc.vehicle.jai.CameraError
import java.util.Date
import csc.vehicle.message.CameraUnit

trait CameraRequest

/**
 * Message to request to get the status of the camera.
 */
case object JaiStatusRequest extends CameraRequest

/**
 * Message to request to get an image from the camera
 */
case object JaiImageRequest extends CameraRequest

/**
 * Successfull Response to request to get an image from the camera
 */
case class JaiImageResponse(camera: CameraUnit, image: Array[Byte], time: Option[Date]) {
  def name = camera.name
  def laneId = camera.id
}

/**
 * Error Response to request to get an image from the camera
 */
case class JaiImageError(camId: String, error: Seq[CameraError])

/**
 * Response to request to get the status of the camera
 */
case class JaiStatusResponse(camId: String, errors: Seq[CameraError])
