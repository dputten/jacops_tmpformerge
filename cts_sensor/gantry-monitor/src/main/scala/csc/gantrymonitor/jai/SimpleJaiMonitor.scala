package csc.gantrymonitor.jai

import akka.actor.{ ActorRef, ActorLogging, Actor }
import csc.gantry.config.CameraConfig
import csc.systemevents.SystemEvent
import csc.vehicle.jai._
import csc.vehicle.message.CameraUnit

import scala.collection.mutable.ListBuffer
import scala.concurrent.duration._

/**
 * This actor hides the Jai Camera connection, and is able to handle camera errors.
 *
 * @param config
 * @param cameraTriggerId
 * @param alarmDelay
 * Created by carlos on 24/04/16.
 */
class SimpleJaiMonitor(camera: CameraUnit,
                       config: CameraConfig,
                       cameraTriggerId: BigInt,
                       alarmDelay: FiniteDuration,
                       systemEventSolvedSending: Boolean,
                       extraRecipients: Set[ActorRef] = Set.empty)
  extends Actor with ActorLogging {

  def cameraId: String = camera.id

  implicit val executor = context.system.dispatcher

  private val recipients = extraRecipients + self

  protected val jaiConnection = new JaiCameraConnection(laneId = cameraId,
    host = config.cameraHost, port = config.cameraPort,
    maxTriggerRetries = config.maxTriggerRetries, timeDisconnected = config.timeDisconnected, cameraTriggerId = cameraTriggerId,
    recipients = recipients)

  private val pendingErrors = new ListBuffer[DisturbanceEvent]()

  override def preStart() {
    //jaiConnection.start()
    jaiConnection.startAndWait() //await seems to be necessary for some time constrained tests to work
    log.info("Starting Camera monitor %s with path %s".format(cameraId, self.path))
  }
  override def postStop() {
    jaiConnection.shutdown()
    log.info("Stopping Camera monitor %s with path %s".format(cameraId, self.path))
  }

  override def receive: Receive = {
    case error: DisturbanceEvent ⇒
      if (systemEventSolvedSending) sendSystemEvent(error) //we send immediately the event, if eventSolvedSending is enabled
      else handleDisturbance(error) //otherwise we use the legacy logic (using solved just to cancel pending errors)
    case delay: DelayedError ⇒ handleDelayed(delay)
  }

  private def getEventType(error: DisturbanceEvent): String = error.state match {
    case DisturbanceState.DETECTED ⇒ error.eventType.eventType
    case DisturbanceState.SOLVED   ⇒ DisturbanceTypes.solvedType(error.eventType.eventType)
  }

  private def handleDisturbance(error: DisturbanceEvent): Unit = {
    //error occurred
    error.state match {
      case DisturbanceState.SOLVED ⇒ {
        log.info("Lane %s: Solved Error %s".format(error.laneId, error))
        val position = pendingErrors.indexWhere(err ⇒ {
          err.eventType == error.eventType
        })
        if (position >= 0) {
          pendingErrors.remove(position)
        }
      }
      case DisturbanceState.DETECTED ⇒ {
        if (alarmDelay.toMillis == 0) {
          sendSystemEvent(error)
        } else {
          context.system.scheduler.scheduleOnce(alarmDelay, self, new DelayedError(error))
          pendingErrors += error
        }
      }
    }
  }

  protected def handleDelayed(delay: DelayedError): Unit = {
    val error = delay.error
    val position = pendingErrors.indexWhere(err ⇒ {
      err.eventType == error.eventType
    })
    if (position >= 0) {
      pendingErrors.remove(position)
      sendSystemEvent(error)
    }
  }

  def sendSystemEvent(error: DisturbanceEvent) {
    val eventType = getEventType(error)
    val event = new SystemEvent(componentId = error.laneId + "-camera",
      timestamp = error.timestamp.getTime,
      systemId = camera.system,
      eventType = eventType,
      userId = "system",
      reason = error.eventType.details.getOrElse(""),
      gantryId = Some(camera.gantry),
      laneId = Some(cameraId))
    context.system.eventStream.publish(event)
  }

}
