/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.gantrymonitor.jai

import java.text.SimpleDateFormat
import csc.gantrymonitor.config.GantryMonitorConfiguration
import csc.vehicle.jai._
import csc.systemevents.SystemEvent
import csc.gantry.config.{ MultiLaneCamera, LaneConfig, CameraConfig }
import collection.mutable.ListBuffer
import akka.actor._
import java.util.Date
import csc.vehicle.message.CameraUnit

import scala.concurrent.duration._

case class DelayedError(error: DisturbanceEvent)

case class ImageTimeout(nrRetries: Int)

case class StatusTimeout(nrRetries: Int)

/**
 * This actor extends the SimpleJaiMonitor, adding status and trigger handling capabilities..
 *
 * @param camera the camera identifier
 * @param config The camera configuration
 * @param cameraTriggerId The Id used to request an image from the camera
 */
class JaiMonitor(camera: CameraUnit,
                 val config: CameraConfig,
                 val cameraTriggerId: BigInt,
                 val alarmDelay: FiniteDuration,
                 maxRetries: Int,
                 timeoutDuration: FiniteDuration,
                 connectionPingActor: Option[ActorRef] = None)
  extends SimpleJaiMonitor(camera, config, cameraTriggerId, alarmDelay, false, connectionPingActor.toSet) {

  private val senderListStatus = new ListBuffer[ActorRef]()
  private val senderListImage = new ListBuffer[ActorRef]()
  private var imageTime: Option[Date] = None
  private val systemId = camera.system
  private val gantryId = camera.gantry

  override def receive: Receive = super.receive orElse myReceive orElse unhandled

  connectionPingActor match {
    case None ⇒
      log.info("Camera connection ping is disabled")
    case Some(actor) ⇒
      log.info("Camera connection ping is enabled")
      actor ! Init(self)
  }

  connectionPingActor map (_ ! Init(self))

  def myReceive: Receive = {
    case status: CameraStatus ⇒ {
      //status update received
      log.info("Lane %s: Received status".format(status.laneId))
      for (error ← status.errors) {
        val event = SystemEvent(componentId = status.laneId + "-camera",
          timestamp = status.time.getTime,
          systemId = systemId,
          eventType = error.eventType,
          userId = "system",
          reason = error.details.getOrElse(""),
          gantryId = Some(gantryId),
          laneId = Some(cameraId))
        context.system.eventStream.publish(event)
      }
      if (!senderListStatus.isEmpty) {
        //there are processes waiting for a status update
        val statusMsg = new JaiStatusResponse(cameraId, status.errors)
        for (requester ← senderListStatus) {
          requester ! statusMsg
        }
        senderListStatus.clear()
      }
    }
    case triggerOK: CameraTriggerSuccess ⇒ {
      val dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS z")
      log.info("Lane %s: Received a CameraTrigger result: OK time=%s".format(triggerOK.laneId, dateFormat.format(triggerOK.imageCreationTime)))
      imageTime = Some(triggerOK.imageCreationTime)
    }
    case triggerFailure: CameraTriggerFailure ⇒ {
      log.info("Lane %s: Received a CameraTrigger result: FAIL %s".format(triggerFailure.laneId, triggerFailure.error))
      if (!senderListImage.isEmpty) {
        //there are processes waiting for a status update
        val err = createImageError(triggerFailure.error)
        for (requester ← senderListImage) {
          requester ! err
        }
        senderListImage.clear()
      }
    }
    case image: CameraTriggerImage ⇒ {
      log.info("Lane %s: Received a image result".format(image.laneId))
      if (!senderListImage.isEmpty) {
        //there are processes waiting for a status update
        val imageMsg = new JaiImageResponse(camera, image.image, imageTime)
        for (requester ← senderListImage) {
          requester ! imageMsg
        }
        senderListImage.clear()
      }
      imageTime = None
    }
    case image: CameraTriggerImageFailure ⇒ {
      log.info("Lane %s: Received a image result failure".format(image.laneId))
      if (!senderListImage.isEmpty) {
        //there are processes waiting for a status update
        val err = new JaiImageError(cameraId, image.errors)
        for (requester ← senderListImage) {
          requester ! err
        }
        senderListImage.clear()
      }
      imageTime = None
    }
    case JaiStatusRequest ⇒ {
      try {
        jaiConnection.sendCameraStatus()
        senderListStatus += sender
        context.system.scheduler.scheduleOnce(timeoutDuration, self, new StatusTimeout(0))
        //wait for result messages
      } catch {
        case ex: IllegalStateException ⇒ {
          if (ex.getMessage.contains("[DISCONECTED]") || ex.getMessage.contains("[CONNECT_ERROR]")) {
            context.system.scheduler.scheduleOnce(timeoutDuration, self, new StatusTimeout(0))
          } else
            sender ! createStatusResponse(CameraError(DisturbanceTypes.INTERFACE_ERROR, Some(ex.getMessage)))
        }
        case ex: Exception ⇒ {
          //error sending message to camera
          // result fail
          sender ! createStatusResponse(CameraError(DisturbanceTypes.INTERFACE_ERROR, Some(ex.getMessage)))
        }
      }
    }
    case JaiImageRequest ⇒ {
      try {
        jaiConnection.sendCameraTrigger()
        senderListImage += sender
        context.system.scheduler.scheduleOnce(timeoutDuration, self, new ImageTimeout(0))
        //wait for result messages
      } catch {
        case ex: IllegalStateException ⇒ {
          if (ex.getMessage.contains("[DISCONECTED]") || ex.getMessage.contains("[CONNECT_ERROR]"))
            context.system.scheduler.scheduleOnce(timeoutDuration, self, new ImageTimeout(0))
          else
            sender ! createImageError(CameraError(DisturbanceTypes.INTERFACE_ERROR, Some(ex.getMessage)))
        }
        case ex: Exception ⇒ {
          //error sending message to camera
          // result fail
          sender ! createImageError(CameraError(DisturbanceTypes.INTERFACE_ERROR, Some(ex.getMessage)))
        }
      }
    }
    case timeout: ImageTimeout ⇒ {
      if (!senderListImage.isEmpty) {
        if (timeout.nrRetries < maxRetries) {
          //resent message
          context.system.scheduler.scheduleOnce(timeoutDuration, self, timeout.copy(nrRetries = timeout.nrRetries + 1))
          try {
            jaiConnection.sendCameraTrigger()
          } catch {
            case ex: Exception ⇒ {
              log.error("Lane %s: Failed to resend CameraTrigger %s".format(cameraId, ex))
              if (timeout.nrRetries == maxRetries) {
                if (ex.getMessage.contains("[DISCONECTED]") || ex.getMessage.contains("[CONNECT_ERROR]")) {
                  sender ! createImageError(CameraError(DisturbanceTypes.CAMERA_CONNECTION))
                } else {
                  sender ! createImageError(CameraError(DisturbanceTypes.INTERFACE_ERROR, Some(ex.getMessage)))
                }
              }
            }
          }
        }
      }
    }
    case timeout: StatusTimeout ⇒ {
      if (!senderListStatus.isEmpty) {
        if (timeout.nrRetries < maxRetries) {
          //resent message
          context.system.scheduler.scheduleOnce(timeoutDuration, self, timeout.copy(nrRetries = timeout.nrRetries + 1))
          try {
            jaiConnection.sendCameraStatus()
          } catch {
            case ex: Exception ⇒ {
              log.error("Lane %s: Failed to resend StatusTrigger %s".format(cameraId, ex))
              if (timeout.nrRetries == maxRetries) {
                if (ex.getMessage.contains("[DISCONECTED]") || ex.getMessage.contains("[CONNECT_ERROR]")) {
                  sender ! createStatusResponse(CameraError(DisturbanceTypes.CAMERA_CONNECTION))
                  //generate also system event
                  val event = SystemEvent(componentId = cameraId + "-camera",
                    timestamp = System.currentTimeMillis(),
                    systemId = systemId,
                    eventType = DisturbanceTypes.CAMERA_CONNECTION,
                    userId = "system",
                    reason = "Camera is niet bereikbaar tijdens status controle",
                    gantryId = Some(gantryId),
                    laneId = Some(cameraId))
                  context.system.eventStream.publish(event)
                } else
                  sender ! createStatusResponse(CameraError(DisturbanceTypes.INTERFACE_ERROR, Some(ex.getMessage)))
              }
            }
          }
        }
      }
    }
  }

  private def unhandled: Receive = {
    case msg ⇒ log.info("unhandled: Don' know how to handle {}", msg)
  }

  private def createImageError(error: CameraError) = JaiImageError(cameraId, Seq(error))

  private def createStatusResponse(error: CameraError) = JaiStatusResponse(cameraId, Seq(error))

}

case class Init(actor: ActorRef)

class CameraConnectionPingActor(frequency: FiniteDuration) extends Actor with ActorLogging {

  implicit val executor = context.system.dispatcher

  private var lastCameraMsgTime: Option[Long] = None
  private var statusRequestSent = false

  override def receive: Receive = {
    case msg: Init ⇒
      log.info("Camera will now be ping'ed every {} millis with JaiRequestStatus", frequency.toMillis)
      context.system.scheduler.schedule(frequency, frequency, self, ConnectionCheck)
      context.become(receiveWithMonitor(msg.actor))
    case msg ⇒ println("not initialized yet")
  }

  private def receiveWithMonitor(monitor: ActorRef): Receive = {
    case ConnectionCheck        ⇒ checkConnection(monitor)
    case msg: CameraMessage     ⇒ lastCameraMsgTime = Some(System.currentTimeMillis())
    case msg: DisturbanceEvent  ⇒ handleDisturbance(msg)
    case msg: JaiStatusResponse ⇒ statusRequestSent = false //clearing the flag so we can request again
  }

  def handleDisturbance(msg: DisturbanceEvent): Unit = {
    if (msg.eventType.eventType == DisturbanceTypes.CAMERA_CONNECTION) msg.state match {
      case DisturbanceState.SOLVED ⇒
        log.info("Camera appears to be back online. Resuming status ping")
        lastCameraMsgTime = Some(System.currentTimeMillis())
      case DisturbanceState.DETECTED ⇒
        log.info("Camera appears to be offline. Stopping status ping until connection resumed")
        lastCameraMsgTime = None
    }
  }

  def checkConnection(monitor: ActorRef): Unit = lastCameraMsgTime map { time ⇒
    val old: Boolean = (System.currentTimeMillis() - time) > frequency.toMillis
    (old, statusRequestSent) match {
      case (true, false) ⇒
        log.debug("Pinging camera with JaiStatusRequest")
        monitor ! JaiStatusRequest //requests a status
        statusRequestSent = true //making sure we don't flood it
      case _ ⇒ //just continuing
    }
  }

  case object ConnectionCheck
}

object JaiMonitor {

  val cameraTriggerId: BigInt = 0xFF

  def props(config: GantryMonitorConfiguration, lane: LaneConfig, pingActor: Option[ActorRef]): Props =
    Props(new JaiMonitor(lane.lane, lane.camera, cameraTriggerId, config.delaySystemEvent,
      config.maxRetriesGetImage, config.timeoutGetImage, pingActor))

  def props(config: GantryMonitorConfiguration, camera: MultiLaneCamera, pingActor: Option[ActorRef]): Props =
    Props(new JaiMonitor(camera.id, camera.config, cameraTriggerId, config.delaySystemEvent,
      config.maxRetriesGetImage, config.timeoutGetImage, pingActor))

  def connectionPingProps(frequency: Option[FiniteDuration]): Option[Props] =
    frequency map { d ⇒ Props(new CameraConnectionPingActor(d)) }

}