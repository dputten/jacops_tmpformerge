/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.gantrymonitor.calibration

import akka.actor._
import com.github.sstone.amqp.Amqp.{ Reject, Ack, Delivery }
import csc.amqp.AmqpSerializers
import csc.calibration.GantryCalibrationRequest
import net.liftweb.json.DefaultFormats

/**
 * This class listens to the queue for calibration requests and send the request to the Calibration
 */
class CalibrationConsumer(calibrationActor: ActorRef)
  extends Actor with ActorLogging with AmqpSerializers {
  implicit val system = context.system
  override implicit val formats = DefaultFormats
  override val ApplicationId = "CalibrationConsumer"

  object GantryCalibrationRequestExtractor extends MessageExtractor[GantryCalibrationRequest]

  def receive = {
    case delivery @ GantryCalibrationRequestExtractor(msg) ⇒ {
      val consumer = sender
      log.info("Received GantryCalibrationRequest {}", msg)
      calibrationActor ! msg
      consumer ! Ack(delivery.envelope.getDeliveryTag)
    }
    case delivery: Delivery ⇒ {
      log.info("Received Delivery")
      val consumer = sender
      log.error("Dropping message with tag {} - cannot determine message type", delivery.envelope.getDeliveryTag)
      consumer ! Reject(delivery.envelope.getDeliveryTag, false)
    }
  }

  override def preStart() {
    super.preStart()
    log.info("Started CalibrationConsumer")
  }

  override def postStop() {
    super.postStop()
    log.info("Stopped CalibrationConsumer")
  }
}