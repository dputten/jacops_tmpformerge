package csc.gantrymonitor.calibration

import akka.actor.SupervisorStrategy.Restart
import akka.actor.{ ActorLogging, ActorRef, OneForOneStrategy }
import csc.amqp.PublisherActor
import csc.calibration.GantryCalibrationResponse
import net.liftweb.json.DefaultFormats

/**
 * This class listens for GantryCalibrationResponse requests and send the request to the queue
 */
class CalibrationProducer(val producer: ActorRef, val producerEndpoint: String) extends PublisherActor
  with ActorLogging {
  override val ApplicationId = "calibration"
  override implicit val formats = DefaultFormats
  def responseName = "GantryCalibrationResponse"

  override val supervisorStrategy = OneForOneStrategy() {
    case _: Exception ⇒ Restart
  }

  def producerRouteKey(msg: AnyRef): String = {
    msg match {
      case gantryCalibrationResponse: GantryCalibrationResponse ⇒ {
        gantryCalibrationResponse.systemId + "." + gantryCalibrationResponse.gantryId + "." + responseName
      }
      case other ⇒ msg.getClass.getName
    }
  }
}
