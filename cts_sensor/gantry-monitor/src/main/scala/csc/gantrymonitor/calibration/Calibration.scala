/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.gantrymonitor.calibration

import java.io.File

import csc.akka.logging.DirectLogging
import csc.calibration._
import csc.gantrymonitor.config.SelfTestConfiguration
import csc.gantrymonitor.jai.{ JaiImageError, JaiImageResponse }
import csc.gantrymonitor.selftest.Selftest
import Selftest.CameraChecks
import Selftest.CameraChecks._
import csc.timeregistration.registration.ImageMetaDataReader
import imagefunctions.ImageFunctionsFactory
import org.apache.commons.codec.digest.DigestUtils
import org.apache.commons.io.FileUtils
import _root_.tools.jaiconnection.SimulateJai.ImageSupplier

import scala.collection.mutable.ListBuffer

/**
 * Keep track of received images before creating a response.
 * The response will be either:
 * -GantryCalibrationResponse (for legacy calibration)
 * -GroupedGantryCalibrationResponse (for generic selftest)
 * @param req The original request. The reques
 * @param config configuration
 * @param imageFunctions
 */
class Calibration(req: GantryCalibrationRequest,
                  config: SelfTestConfiguration,
                  imageFunctions: ImageFunctions) extends DirectLogging {

  case class CalibrationContext(camId: Option[String], results: Seq[StepResult], images: Seq[CameraImage]) {

    def resultOf(name: String): Option[StepResult] = results.find(_.stepName == name)

    def withResult(result: StepResult, imgs: Seq[CameraImage] = Nil): CalibrationContext =
      copy(results = results :+ result, images = images ++ imgs)

  }

  type StepExecute = CalibrationContext ⇒ CalibrationContext

  lazy val stepNames: Seq[String] = req.checks getOrElse CameraChecks.legacy

  lazy val workingDirectory = config.workingDirectory.getOrElse(FileUtils.getTempDirectoryPath)

  def legacyCalibration: Boolean = req.isLegacyCalibration

  var imageResponses = Seq[JaiImageResponse]()

  val activeLanes = req.laneIds

  /**
   * get a unique Id
   * @return an Id
   */
  def getId: String = {
    req.gantryId + "-" + req.time
  }

  def findImageResponse(camId: String, gantry: String): Option[JaiImageResponse] =
    imageResponses.find { r ⇒ r.camera.gantry == gantry && r.camera.id == camId }

  /**
   * Get all lanes which need a image response
   * @return list of laneIds
   */
  def getLaneIdsForImageRequests: Seq[String] = {
    activeLanes.foldLeft(Seq[String]()) {
      case (missingList, laneId) ⇒ {
        imageResponses.find(_.laneId == laneId) match {
          case Some(resp) ⇒ missingList
          case None       ⇒ missingList :+ laneId
        }
      }
    }
  }

  /**
   * A image response is received
   * @param response the response
   */
  def receivedImageResponse(response: JaiImageResponse) {
    if (activeLanes.contains(response.laneId)) {
      if (imageResponses.exists(_.laneId == response.laneId)) {
        log.info("Received a double image response for lane  %s".format(response.laneId))
      } else {
        log.info("Received a image response for lane %s".format(response.laneId))
        imageResponses = imageResponses :+ response
      }
    } else {
      log.info("Received a unexpected image response for lane %s".format(response.laneId))
    }
  }

  /**
   * An error is received.
   * Do nothing just logging
   * @param imageError
   */
  def receivedImageError(imageError: JaiImageError) {
    log.warning("Received a image error response for lane/camera %s: %s".format(imageError.camId, imageError.error))
  }

  def executeSteps(remaining: Seq[String], ctx: CalibrationContext): CalibrationContext = remaining match {
    case Nil          ⇒ ctx
    case head :: tail ⇒ executeSteps(tail, execute(head, ctx))
  }

  def execute(name: String, ctx: CalibrationContext): CalibrationContext = name match {
    case CameraChecks.cameraImage     ⇒ imageStep(ctx)
    case CameraChecks.imageConversion ⇒ conversionStep(ctx)
    case CameraChecks.imageTime       ⇒ timeStep(ctx)
    case other                        ⇒ ctx.withResult(StepResult(other, false, Some("Unknown check. Skipped")))
  }

  def initialContext(camId: Option[String]) = CalibrationContext(camId, Nil, Nil)

  /**
   * Create a response using the current state
   * @param currentTime The current time
   * @return The response
   */
  def createResponse(currentTime: Long): CameraSelftestResponse = legacyCalibration match {
    case true ⇒
      val ctx = executeSteps(stepNames, initialContext(None))
      GantryCalibrationResponse(req.systemId, req.gantryId, req.time, currentTime, ctx.images, ctx.results)
    case false ⇒
      var images: Map[String, Seq[CameraImage]] = Map.empty
      var results: Map[String, Seq[StepResult]] = Map.empty

      activeLanes foreach { camId ⇒
        val ctx = executeSteps(stepNames, initialContext(Some(camId)))
        images = images.updated(camId, ctx.images)
        results = results.updated(camId, ctx.results)
      }

      GroupedGantryCalibrationResponse(req.systemId, req.gantryId, req.time, currentTime, images, results)
  }

  /**
   * Are all responses received
   * @return true when all responses are received
   */
  def isFinished(): Boolean = {
    val expectedResponses = activeLanes.size
    imageResponses.size == expectedResponses
  }

  /**
   * Time step. Not implemented for multiple cameras
   * @param ctx
   * @return
   */
  private def timeStep(ctx: CalibrationContext): CalibrationContext = {

    val imageSuccess = ctx.resultOf(cameraImage) map (_.success)

    val errorMessage: Option[String] = (ctx.camId, imageSuccess) match {
      case (Some(camId), Some(true)) ⇒
        val image = findImageResponse(camId, req.gantryId)

        (config.maxTimeDiffMillis, image) match {
          case (Some(maxDiff), Some(response)) ⇒ {
            (response.time, imageFunctions.getImageCreationTime(response.image)) match {
              case (Some(date), Some(exifTime)) ⇒ math.abs(date.getTime - exifTime) match {
                case x if (x <= maxDiff) ⇒ None //happy case
                case other               ⇒ Some("Millisecond difference was " + other)
              }
              case _ ⇒ Some("Unable to get exif time from image or response")
            }
          }

          case (_, None) ⇒ Some("No image")
          case (None, _) ⇒ Some("No maxTimeDiffMillis configured. Skipping")
        }

      case (None, Some(true)) ⇒ Some("Not implemented")
      case _                  ⇒ Some("No image")
    }

    ctx.withResult(StepResult(imageTime, errorMessage.isEmpty, errorMessage))
  }

  /**
   * Create the Step result for getting the images
   * @return
   */
  private def imageStep(ctx: CalibrationContext): CalibrationContext = {

    val errorMsg: Option[String] = ctx.camId match {
      case None ⇒ {
        //legacy: result for all cameras
        val errorLanes = activeLanes.foldLeft(Seq[String]()) {
          case (missingList, laneId) ⇒ {
            imageResponses.find(_.laneId == laneId) match {
              case Some(resp) ⇒ missingList
              case None       ⇒ missingList :+ laneId
            }
          }
        }
        errorLanes match {
          case Nil ⇒ None //no error
          case _   ⇒ Some("Failed to get image for lanes: " + errorLanes.mkString(", "))
        }
      }
      case Some(camId) ⇒ imageResponses.find(_.laneId == camId) match {
        case None ⇒ Some("Failed to get image for " + camId)
        case _    ⇒ None //no error
      }
    }
    ctx.withResult(StepResult(cameraImage, errorMsg.isEmpty, errorMsg))
  }

  private def conversionStep(ctx: CalibrationContext): CalibrationContext = {
    val imageSuccess = ctx.resultOf(cameraImage) map (_.success) getOrElse false

    val (images, result) = imageSuccess match {
      case true ⇒ createImages(ctx.camId, workingDirectory, config.quality) match {
        case Left(errors)  ⇒ (Nil, StepResult(imageConversion, false, Some(errors.mkString(", "))))
        case Right(Nil)    ⇒ (Nil, StepResult(imageConversion, false, Some("No images created")))
        case Right(images) ⇒ (images, StepResult(imageConversion, true, None))
      }
      case false ⇒ (Nil, StepResult(imageConversion, false, Some("Conversion skipped")))
    }

    ctx.withResult(result, images)
  }

  /**
   * Create list of converted images
   * @param camId optional camId to filter images to create. None creates all
   * @param workingDirectory used workingdirectory
   * @param quality the quality to use
   * @return Right the list of images Left when the conversion failed
   */
  private def createImages(camId: Option[String], workingDirectory: String, quality: Int): Either[Seq[String], Seq[CameraImage]] = {

    val responsesOfInterest = camId match {
      case None     ⇒ imageResponses
      case Some(id) ⇒ imageResponses filter (_.laneId == id)
    }

    val (jpgImages, convertErrors) = imageFunctions.convertImagesToJPG(responsesOfInterest, workingDirectory, quality)
    if (convertErrors.isEmpty) {
      val cameraImages = jpgImages.map(x ⇒ CameraImage(x.name, x.laneId, createChecksum(x.image), x.image.map(_.toInt).toSeq))
      Right(cameraImages)
    } else {
      Left(convertErrors)
    }
  }

  /**
   * create a checksum of the created image
   * @param image the image
   * @return the checksum
   */
  private def createChecksum(image: Array[Byte]): String = DigestUtils.md5Hex(image)

}

trait ImageFunctions {

  def convertImagesToJPG(images: Seq[JaiImageResponse], workingDirectory: String, quality: Int): (Seq[JaiImageResponse], Seq[String])

  def getImageCreationTime(imageContent: Array[Byte]): Option[Long]

  def overrideImageSupplier: Option[ImageSupplier]

}

/**
 * Impl of ImageFunctions that:
 * -supplies fake images (byte[] of a System.currentTimeMillis.toString)
 * -does no image conversion
 * -parses EXIF imagetime expecting a byte[] of the time millis
 */
object CameraFunctionsStub extends ImageFunctions {

  //fake image created
  val overrideImageSupplier: Option[ImageSupplier] = Some({ () ⇒ Some(createImageContents(System.currentTimeMillis())) })

  //no conversion done
  override def convertImagesToJPG(images: Seq[JaiImageResponse],
                                  workingDirectory: String,
                                  quality: Int): (Seq[JaiImageResponse], Seq[String]) = (images, Nil)

  override def getImageCreationTime(imageContent: Array[Byte]): Option[Long] = try {
    Some(new String(imageContent).toLong)
  } catch {
    case ex: Exception ⇒ None
  }

  def createImageContents(time: Long): Array[Byte] = time.toString.getBytes

  def imageExifTime(imageBytes: Array[Byte]): Option[Long] = {
    val str = new String(imageBytes)
    try {
      Some(str.toLong)
    } catch {
      case ex: Exception ⇒ None
    }
  }

}

object NativeImageFunctions extends ImageFunctions {

  override def overrideImageSupplier: Option[ImageSupplier] = None //no mocking

  /**
   * convert all the images to a jpg
   * @param images List of the images
   * @param workingDirectory the woring directory
   * @param quality the quality to use
   * @return (<list of images>,<list of errors)
   */
  override def convertImagesToJPG(images: Seq[JaiImageResponse],
                                  workingDirectory: String, quality: Int): (Seq[JaiImageResponse], Seq[String]) = {
    try {
      val workingDir = createWorkingDirectory(workingDirectory)

      val sensorLib = ImageFunctionsFactory.getJpgFunctions
      val errorList = new ListBuffer[String]()
      val jpgImages = images.flatMap(image ⇒ {
        val tmpTifFile = File.createTempFile("selftest", ".tif", workingDir)
        val tmpJPGFile = new File(tmpTifFile.getAbsolutePath + ".jpg")
        try {

          FileUtils.writeByteArrayToFile(tmpTifFile, image.image)
          sensorLib.bayerToJpg(tmpTifFile.getAbsolutePath,
            0,
            0,
            quality,
            tmpJPGFile.getAbsolutePath)

          if (tmpJPGFile.exists()) {
            val jpg = FileUtils.readFileToByteArray(tmpJPGFile)
            Some(image.copy(image = jpg))
          } else {
            errorList += "JPG Conversion failed for lane %s (temp file %s)".format(image.laneId, tmpTifFile)
            None
          }
        } catch {
          case e: Exception ⇒
            errorList += "JPG Conversion failed for lane %s (temp file: %s converted: %s): %s".format(image.laneId, tmpTifFile.getAbsolutePath, tmpJPGFile.getAbsolutePath, e.getMessage)
            None

        } finally {
          tmpTifFile.delete()
          tmpJPGFile.delete()
        }
      })
      (jpgImages, errorList)
    } catch {
      case ex: Exception ⇒
        (Seq(), Seq("JPG Conversion failed: %s".format(ex.getMessage)))
    }
  }

  override def getImageCreationTime(imageContent: Array[Byte]): Option[Long] =
    ImageMetaDataReader.jpeg.getImageInfo(imageContent) flatMap (_.time)

  /**
   * create a working directory when it doesn't exists
   * @param workingDirectory the working directory
   * @return The directory
   */
  private def createWorkingDirectory(workingDirectory: String): File = {
    val workingDir = new File(workingDirectory)
    if (!workingDir.exists()) {
      workingDir.mkdirs()
    }
    workingDir
  }
}

