/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.gantrymonitor.calibration

import akka.actor.{ Props, Actor, ActorLogging, ActorRef }
import csc.calibration.GantryCalibrationRequest
import csc.gantrymonitor.caseclasses.StartedJaiMonitor
import csc.gantrymonitor.config.{ Configuration, SelfTestConfiguration }
import csc.gantrymonitor.jai.{ JaiImageError, JaiImageRequest, JaiImageResponse }
import csc.gantrymonitor.selftest.SelftestActorProps
import csc.gantrymonitor.startup.GantryMonitor.SelftestActorFactory

object ImageTimeout

case class GantryCalibrationTimeout(calibrationId: String)

/**
 * This actor is responsible for receiving and sending Calibration related messages.
 *
 */
class CalibrationActor(startedJaiMonitor: Seq[StartedJaiMonitor],
                       targetActor: ActorRef,
                       selfTestConfiguration: SelfTestConfiguration,
                       imageFunctions: ImageFunctions) extends Actor with ActorLogging {

  implicit val executor = context.system.dispatcher

  var currentCalibration = Seq[Calibration]()

  def receive = {
    case gantryCalibrationRequest: GantryCalibrationRequest ⇒ {
      log.info("Received GantryCalibrationRequest: " + gantryCalibrationRequest)
      val calibration = new Calibration(gantryCalibrationRequest, selfTestConfiguration, imageFunctions)
      if (currentCalibration.exists(_.getId == calibration.getId)) {
        log.info("Received a gantryCalibrationRequest for id %s while already processing this request".format(calibration.getId))
      } else {
        currentCalibration = currentCalibration :+ calibration
        sendMessages(calibration.getLaneIdsForImageRequests)
        context.system.scheduler.scheduleOnce(selfTestConfiguration.timeout,
          self, GantryCalibrationTimeout(calibration.getId))
      }
    }
    case response: JaiImageResponse ⇒ {
      //getting a image by receiving responses of the ImageRequest
      for (calibration ← currentCalibration) {
        calibration.receivedImageResponse(response)
      }
      val (finished, current) = currentCalibration.partition(_.isFinished())
      currentCalibration = current
      finished.foreach(sendResponse(_))
    }
    case response: JaiImageError ⇒ {
      //received an error as responses of the ImageRequest
      for (calibration ← currentCalibration) {
        calibration.receivedImageError(response)
      }
    }
    case ImageTimeout ⇒ {
      //timeout check if we need to resent ImageRequests
      val laneIds = currentCalibration.flatMap(_.getLaneIdsForImageRequests).distinct
      sendMessages(laneIds)
    }
    case GantryCalibrationTimeout(id) ⇒ {
      //calibration timeout, if no response is send yet sent a failure calibration response
      val (timeout, current) = currentCalibration.partition(_.getId == id)
      currentCalibration = current
      timeout.foreach(sendResponse(_))
    }
  }

  /**
   * Create a response and send it to the producer
   * @param calibration The calibration state
   */
  private def sendResponse(calibration: Calibration) {
    val response = calibration.createResponse(System.currentTimeMillis())
    log.info("Sending calibration response for system %s gantry %s".format(response.systemId, response.gantryId))
    targetActor ! response
  }

  /**
   * Send messages to the camera monitors
   * @param laneIds list of laneIds which still need an image
   */
  private def sendMessages(laneIds: Seq[String]) {
    for (laneId ← laneIds) {
      val monitor = startedJaiMonitor.find(m ⇒ m.camera.id == laneId)
      monitor.foreach(_.ref ! JaiImageRequest)
    }
    context.system.scheduler.scheduleOnce(selfTestConfiguration.imageTimeout, self, ImageTimeout)
  }
}

class LegacyCalibrationPropsFactory(config: Configuration) extends SelftestActorFactory {

  override def apply(monitors: Seq[StartedJaiMonitor]): SelftestActorProps = new LegacyCalibrationProps(monitors)

  class LegacyCalibrationProps(jaiMonitors: Seq[StartedJaiMonitor]) extends SelftestActorProps {

    override def producerProps(amqpProducer: ActorRef): Props =
      Props(new CalibrationProducer(amqpProducer, config.amqpCalibrationServicesConfiguration.producerEndpoint))

    override def actorProps(producer: ActorRef): Props =
      Props(new CalibrationActor(jaiMonitors, producer, config.monitorConfiguration.selfTest, config.imageFunctions))

    override def consumerProps(actor: ActorRef): Props =
      Props(new CalibrationConsumer(actor))

  }

}
