package csc.gantrymonitor.stub

import java.util.concurrent.{ TimeoutException, CountDownLatch }

import akka.actor._
import akka.camel.{ CamelMessage, Consumer }
import scala.concurrent.{ CanAwait, Awaitable, Await }
import csc.akka.logging.DirectLogging
import csc.akka.remote.{ SocketServer, SocketMessageSender }
import csc.gantry.config.CameraConfig
import _root_.tools.jaiconnection.SimulateJai.ImageSupplier
import _root_.tools.jaiconnection.{ CameraStarted, JaiCameraSimulator, SimulateJai }

import scala.concurrent.duration._

/**
 * Created by carlos on 27/04/16.
 */
class JaiCameraStub(uri: String,
                    listener: Option[CameraStubListener],
                    imageSupplier: Option[ImageSupplier]) extends Actor with Consumer with ActorLogging {

  val expression = """:(\S+) (.+)""".r

  override def endpointUri: String = uri

  var simulatorMap: Map[String, JaiCameraSimulator] = Map.empty

  override def preStart(): Unit = {
    super.preStart()
    listener foreach { _ ⇒ context.system.eventStream.subscribe(self, classOf[CameraStarted]) }
  }

  override def postStop(): Unit = {
    simulatorMap.values.foreach(_.sim.stopJaiCamera())
    super.postStop()
  }

  override def receive: Receive = {
    case InitCameras(cameras) ⇒ init(cameras)
    case msg: CamelMessage    ⇒ handleCommand(msg.bodyAs[String])
    case msg: CameraStarted   ⇒ listener foreach (_.add(msg))
    case other                ⇒ log.warning("Don't know how to handle {}", other)
  }

  def init(cameras: List[CameraConfig]): Unit = cameras match {
    case Nil ⇒ log.warning("No cameras defined for stub")
    case list ⇒ simulatorMap = list map { c ⇒
      log.info("Initializing JaiCameraStub for {}", c)
      c.cameraId -> new JaiCameraSimulator(new SimulateJai(c.cameraHost, c.cameraPort, Some(context.system), imageSupplier))
    } toMap
  }

  def handleCommand(str: String): Unit = {
    str match {
      case expression(cam, cmd) ⇒ simulatorMap.get(cam) match {
        case None      ⇒ log.warning("Unknown camera {}. Present: {}", cam, simulatorMap.keys.toList)
        case Some(sim) ⇒ sim.process(cmd)
      }
      case cmd ⇒ simulatorMap.size match {
        case 1 ⇒ simulatorMap.values.head.process(cmd)
        case _ ⇒ log.warning("No camera specified and multiple available. Ignoring command {}" + cmd)
      }
    }
  }
}

object JaiCameraStub extends DirectLogging {

  def suitableCameras(cameras: List[CameraConfig]) = cameras.filter(_.cameraHost == "localhost")

  def apply(system: ActorSystem, uri: String, cameras: List[CameraConfig], imageSupplier: Option[ImageSupplier]): ActorRef = {
    val cams = suitableCameras(cameras)
    val listener = new CamerasAvailableLatch(cams) //creates the listener
    val actor = system.actorOf(Props(new JaiCameraStub(uri, Some(listener), imageSupplier)), "JaiCameraStub-" + System.currentTimeMillis())

    log.info(s"JaiCameraStub started: $actor ")
    actor ! InitCameras(cams) //triggers initialization

    try {
      listener.awaitReady(Duration("5 seconds"))
    } catch {
      case ex: TimeoutException ⇒ log.warning(ex.getMessage)
    }
    log.info(s"JaiCameraStub cameras initialized: $cams")

    actor
  }
}

trait CameraStubListener {
  def add(msg: CameraStarted): Boolean
}

class CamerasAvailableLatch(cameras: List[CameraConfig])
  extends Awaitable[Unit] with CameraStubListener with DirectLogging {

  var remaining: List[CameraConfig] = cameras
  private val latch = new CountDownLatch(cameras.size)

  @scala.throws(classOf[TimeoutException])
  override def ready(atMost: Duration)(implicit permit: CanAwait): this.type = {
    if (atMost.isFinite()) {
      if (!latch.await(atMost.length, atMost.unit))
        throw new TimeoutException("Not all cameras were initialized. Remaining: " + remaining)
    } else latch.await()
    this
  }

  @scala.throws(classOf[Exception])
  override def result(atMost: Duration)(implicit permit: CanAwait): Unit = ready(atMost)

  def add(msg: CameraStarted): Boolean = remaining.find(c ⇒ c.cameraPort == msg.port && c.cameraHost == msg.host) match {
    case None ⇒ false
    case Some(cfg) ⇒
      remaining = remaining.filterNot(_ == cfg)
      latch.countDown()
      log.debug("Camera ready {}. Remaining {}", cfg, remaining)
      true
  }

  def awaitReady(timeout: Duration): Unit = {
    log.info("Waiting for stub cameras to be ready")
    Await.ready(this, timeout) //awaits for all the cameras to be ready
    log.info("All stub cameras are ready")
  }

}

class JaiCameraStubClient(val server: SocketServer) extends SocketMessageSender with DirectLogging {

  def send(cam: Option[String], msg: String): Unit = {
    val prefix = cam match {
      case None    ⇒ ""
      case Some(c) ⇒ ":%s ".format(c)
    }
    sendNotification(prefix + msg)
  }
}

case class InitCameras(cameras: List[CameraConfig])
case object CamerasInitialized
