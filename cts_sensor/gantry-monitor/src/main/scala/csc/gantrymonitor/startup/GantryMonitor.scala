/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.gantrymonitor.startup

import akka.actor._
import csc.akka.logging.DirectLogging
import csc.amqp.QueueConnection
import csc.gantry.config.{ MultiLaneCamera, LaneConfiguration }
import csc.gantrymonitor.config.{ Configuration, GantryConfiguration, GantryMonitorConfiguration }
import csc.gantrymonitor.selftest._
import csc.gantrymonitor.jai.{ JaiStatusRequest, JaiMonitor }
import csc.gantrymonitor.caseclasses.StartedJaiMonitor
import csc.gantrymonitor.startup.GantryMonitor.SelftestActorFactory

/**
 * Helper to start the Monitor
 */
class GantryMonitor(gantryMonitorConfiguration: GantryMonitorConfiguration,
                    laneConfiguration: LaneConfiguration,
                    gantryConfiguration: GantryConfiguration,
                    configuration: Configuration,
                    calibrationAmqp: Option[QueueConnection],
                    amqpProducer: ActorRef,
                    selftestActorFactory: SelftestActorFactory,
                    actorSystem: ActorSystem) extends DirectLogging {

  implicit val executor = actorSystem.dispatcher

  var startedJaiMonitors = Seq[StartedJaiMonitor]()

  var actors: Seq[ActorRef] = Nil

  def start() {
    startedJaiMonitors = launchJaiMonitors()
    startSelfTest()
  }

  def stop() {
    startedJaiMonitors.foreach { monitor ⇒
      actorSystem.stop(monitor.ref)
      monitor.schedule.foreach(_.cancel())
    }
    stopSelfTest()
  }

  private def stopSelfTest(): Unit = {
    actors foreach (actorSystem.stop(_))
  }

  private def startSelfTest(): Unit = {

    val selftestActorProps = selftestActorFactory(startedJaiMonitors)

    val producer = actorSystem.actorOf(selftestActorProps.producerProps(amqpProducer))

    val selftestActor = actorSystem.actorOf(selftestActorProps.actorProps(producer))
    actors = actors :+ selftestActor

    val selftestConsumer = actorSystem.actorOf(selftestActorProps.consumerProps(selftestActor))
    actors = actors :+ selftestConsumer

    calibrationAmqp match {
      case None       ⇒ log.info("AMQP disabled (testing only)")
      case Some(conn) ⇒ conn.wireConsumer(selftestConsumer, true)
    }
  }

  def launchJaiMonitors(): Seq[StartedJaiMonitor] = laneConfiguration.getMultiLaneCameras match {
    case Nil  ⇒ launchJaiSingleLaneMonitors()
    case list ⇒ launchJaiMultiLaneMonitors(list)
  }

  private def createCancellable(actor: ActorRef): Option[Cancellable] = {
    gantryMonitorConfiguration.errorCheck.map(errorPolTime ⇒ {
      //send a get status request every errorPolTime
      //The result will be send to the deadletter, but all existing errors will be reported again
      actorSystem.scheduler.schedule(errorPolTime, errorPolTime, actor, JaiStatusRequest)
    })
  }

  //Not using the ping actor, as the Cancellable seems to do the job, when properly configured
  private def pingActor(camId: String): Option[ActorRef] = None
  //JaiMonitor.connectionPingProps(gantryMonitorConfiguration.errorCheck) map (actorSystem.actorOf(_, camId + "-ping"))

  def launchJaiMultiLaneMonitors(cameras: List[MultiLaneCamera]): Seq[StartedJaiMonitor] =
    cameras map { cam ⇒
      val name = cam.config.cameraId + "-camera"
      val actor = actorSystem.actorOf(JaiMonitor.props(gantryMonitorConfiguration, cam, pingActor(cam.config.cameraId)), name)
      log.info("Started JAI monitor for gantry %s camera %s lane %s".format(cam.gantry, cam.config.cameraId, None))
      StartedJaiMonitor(cam.id, actor, createCancellable(actor))
    }

  def launchJaiSingleLaneMonitors(): Seq[StartedJaiMonitor] =
    for {
      gantryId ← gantryConfiguration.gantryIds;
      laneConfig ← laneConfiguration.getLanesForGantry(gantryConfiguration.systemId, gantryId)
    } yield {
      val name = laneConfig.lane.laneId + "-camera"
      val actor = actorSystem.actorOf(JaiMonitor.props(gantryMonitorConfiguration, laneConfig, pingActor(laneConfig.lane.laneId)), name)
      log.info("Started JAI monitor for gantry %s camera %s lane %s".format(gantryId, laneConfig.camera.cameraId, laneConfig.lane.laneId))
      StartedJaiMonitor(laneConfig.lane, actor, createCancellable(actor))
    }

}

object GantryMonitor {
  type SelftestActorFactory = Seq[StartedJaiMonitor] ⇒ SelftestActorProps
}