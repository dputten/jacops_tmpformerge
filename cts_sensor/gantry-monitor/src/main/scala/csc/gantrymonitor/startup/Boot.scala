/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.gantrymonitor.startup

import akka.kernel.Bootable
import akka.actor.{ ActorRef, ActorSystem, Props }
import csc.gantrymonitor.calibration.LegacyCalibrationPropsFactory
import csc.gantrymonitor.config.Configuration
import csc.gantrymonitor.selftest.GenericSelftestProps
import csc.gantrymonitor.startup.GantryMonitor.SelftestActorFactory
import csc.gantrymonitor.stub.JaiCameraStub
import csc.gantrymonitor.systemevent.SystemEventListener
import csc.akka.logging.DirectLogging
import csc.systemevents.SystemEventAmqpActor
import csc.amqp.{ AmqpConnection, JsonSerializers }
import net.liftweb.json.DefaultFormats
import imagefunctions.ImageFunctionsFactory
import imagefunctions.jpg.impl.JpgFunctionsImpl

/**
 * Boot class Starts Gantry monitor
 */
class Boot extends Bootable with DirectLogging with JsonSerializers {
  implicit val system = ActorSystem("GantryMonitor")
  override implicit val formats = DefaultFormats
  var gantryMonitor: Option[GantryMonitor] = None
  var configuration: Option[Configuration] = None
  var cameraStub: Option[ActorRef] = None

  def initializeSystemEventQueue(config: Configuration, producer: ActorRef): ActorRef = {
    val queueActor = system.actorOf(Props(new SystemEventAmqpActor(producer, config.amqpSystemEventServicesConfiguration.producerEndpoint,
      config.amqpSystemEventServicesConfiguration.producerRouteKey, "GantryMonitor")))
    log.debug("queueActor created")

    queueActor
  }

  def startup() = {
    val config = new Configuration()
    configuration = Some(config)
    log.info("configuration is {}", config)
    log.info("stub configuration is {}", config.stubConfiguration)

    val amqpConnection = new AmqpConnection(config.amqpConfiguration)

    log.info("Starting Gantry Monitor")

    initializeImageFunctions()

    val imgFunctions = config.imageFunctions

    log.info("ImageFunctions instance: {}", imgFunctions)

    cameraStub = config.stubConfiguration map { cfg ⇒
      //creates JaiCameraStub and block until cameras ready (or times out)
      JaiCameraStub(system, cfg.cameraStubUri, config.laneConfiguration.getCameras, imgFunctions.overrideImageSupplier)
    }

    val sysEvtQueue = initializeSystemEventQueue(config, amqpConnection.connection.producer)
    val sysEvtStore = config.systemEventStore(sysEvtQueue)
    system.actorOf(Props(new SystemEventListener(sysEvtStore)))

    val propsFactory: SelftestActorFactory = { monitors ⇒
      config.laneConfiguration.isMultiLaneSystem match {
        case true  ⇒ new GenericSelftestProps(config, monitors)
        case false ⇒ new LegacyCalibrationPropsFactory(config).apply(monitors)
      }
    }

    val calibrationQueue = amqpConnection.queue(config.amqpCalibrationServicesConfiguration)

    gantryMonitor = Some(new GantryMonitor(config.monitorConfiguration,
      config.laneConfiguration, config.gantryConfiguration,
      config, Some(calibrationQueue), amqpConnection.connection.producer, propsFactory, system))

    gantryMonitor.get.start()
    log.debug("startupSensorMonitor is started")
  }

  def shutdown() = {
    log.info("Shutting down the Gantry Monitor")
    gantryMonitor.foreach(mon ⇒ mon.stop())
    configuration.foreach(_.curator.curator.foreach(_.close))
    system.shutdown()
    system.awaitTermination() //making sure its terminated
    log.info("Gantry Monitor terminated")
  }

  protected def initializeImageFunctions() {
    ImageFunctionsFactory.setJpgFunctions(JpgFunctionsImpl.getInstance())
  }
}

