AKKA_HOME="$(cd "$(cd "$(dirname "$0")"; pwd -P)"/..; pwd)"
AKKA_CLASSPATH="$AKKA_HOME/lib/*:$AKKA_HOME/deploy/*"

java -cp $AKKA_CLASSPATH csc.gantrymonitor.tools.jaicamera.SimulateJaiCameraCorrect "$@"
