AKKA_HOME="$(cd "$(cd "$(dirname "$0")"; pwd -P)"/..; pwd)"
AKKA_CLASSPATH="$AKKA_HOME/lib/*:$AKKA_HOME/config:$AKKA_HOME/deploy/*:$AKKA_HOME/tools/jai-camera-0.1-SNAPSHOT-test.jar:$AKKA_HOME/tools/jai-camera-0.1-SNAPSHOT.jar"

java -cp $AKKA_CLASSPATH tools.jailogger.JaiLogger "$@"
