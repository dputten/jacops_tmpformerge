/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.jai

import java.util.Date

import akka.actor.ActorSystem
import akka.testkit.{ TestKit, TestProbe }
import scala.concurrent.duration._
import csc.akka.logging.DirectLogging
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import _root_.tools.jaiconnection.SimulateJai

class JaiCameraConnectionTest extends TestKit(ActorSystem("JaiInterfaceTest")) with WordSpecLike with MustMatchers with BeforeAndAfterAll with DirectLogging {
  val probe = TestProbe()
  val host = "localhost"
  val port = 12345
  val jaiSimulator = new SimulateJai(host, port)
  val connection = new JaiCameraConnection(
    laneId = "simLane", host = host, port = port,
    maxTriggerRetries = 10,
    timeDisconnected = 20000,
    cameraTriggerId = 0xFF,
    recipients = Set(probe.ref))

  override def beforeAll() {
    jaiSimulator.updateStatus(StatusCode.ERROR_STATUS, 0L)
    jaiSimulator.updateStatus(StatusCode.NTP_ESTIMATE_ERROR, 0L)
    jaiSimulator.updateStatus(StatusCode.NTP_RESTART_COUNTER, 0L)
    jaiSimulator.updateStatus(StatusCode.NTP_STATUS, 1L)
    connection.start()
    probe.expectMsgType[CameraStatus](20 seconds) //on connection the status is send
  }

  override def afterAll() {
    connection.shutdown()
    jaiSimulator.stopJaiCamera()
    system.shutdown
  }

  "connection with Jai Camera" must {
    "create message when request a status" in {
      jaiSimulator.updateStatus(StatusCode.ERROR_STATUS, 0L)
      jaiSimulator.updateStatus(StatusCode.NTP_ESTIMATE_ERROR, 0L)
      jaiSimulator.updateStatus(StatusCode.NTP_RESTART_COUNTER, 0L)
      jaiSimulator.updateStatus(StatusCode.NTP_STATUS, 1L)

      connection.sendCameraStatus()
      val recv = probe.expectMsgType[CameraStatus](20 seconds)
      recv.errors must have size (0)
    }
    "create message when request a NTP_STATUS with error" in {
      jaiSimulator.updateStatus(StatusCode.NTP_STATUS, 65L) //NTP SYNC error

      connection.sendCameraStatus()
      val recv = probe.expectMsgType[CameraStatus](20 seconds)
      recv.errors must have size (1)
      val error = recv.errors.head
      error.eventType must be(DisturbanceTypes.CAMERA_NTP_SYNC)
    }
    "create message with CAMERA_NTP_DOWN when request a status with error" in {
      jaiSimulator.updateStatus(StatusCode.NTP_STATUS, 1L) //NTP Ok
      jaiSimulator.updateStatus(StatusCode.ERROR_STATUS, 1024L) //NTP DOWN error

      connection.sendCameraStatus()
      val recv = probe.expectMsgType[CameraStatus](20 seconds)
      recv.errors must have size (1)
      val error = recv.errors.head
      error.eventType must be(DisturbanceTypes.CAMERA_NTP_DOWN)
    }
    "create message with CAMERA_NTP_DOWN when request a status with NTP_DOWN error" in {
      val error = JaiErrorMessage(new Date(), ErrorCodeMessage.NTP_DOWN, ErrorStateMessage.PRESENT)
      jaiSimulator.sendError(error)
      val expectedMsg = DisturbanceEvent(
        "simLane",
        error.timestamp,
        CameraError(DisturbanceTypes.CAMERA_NTP_DOWN, Some("DOWN")),
        DisturbanceState.DETECTED)
      probe.expectMsg(20 seconds, expectedMsg)
    }
    "create message when a error is created" in {
      val error = new JaiErrorMessage(new Date(), ErrorCodeMessage.FTP_DOWN, ErrorStateMessage.PRESENT)
      jaiSimulator.sendError(error)
      val expectedMsg = new DisturbanceEvent(
        "simLane",
        error.timestamp,
        new CameraError(DisturbanceTypes.CAMERA_FTP, Some("DOWN")),
        DisturbanceState.DETECTED)
      probe.expectMsg(20 seconds, expectedMsg)
    }
    "create message when failing requesting trigger" in {
      connection.sendCameraTrigger()
      probe.expectMsgType[CameraTriggerSuccess](20 seconds)
      val expectedMsg = new CameraTriggerImageFailure(laneId = "simLane", errors = Seq())
      probe.expectMsg(20 seconds, expectedMsg)
    }
    "create message when requesting trigger" in {
      val image = "IMAGE".getBytes
      jaiSimulator.setImage(image)
      connection.sendCameraTrigger()

      probe.expectMsgType[CameraTriggerSuccess](20 seconds)
      val msg = probe.expectMsgType[CameraTriggerImage](20 seconds)
      msg.laneId must be("simLane")
      compareArrays(msg.image, image)
    }
  }
  def compareArrays(decoded: Array[Byte], src: Array[Byte]) {
    decoded.length must be(src.length)
    for (i ← 0 until src.length) {
      decoded(i) must be(src(i))
    }
  }
}