/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.vehicle.jai.impl

import java.util.Date

import csc.vehicle.jai._
import org.jboss.netty.buffer.ChannelBuffer
import org.scalatest.WordSpec
import org.scalatest._

/**
 * Test the Encode and decode classes for the PirMessages
 */
class EncodeDecodeTest extends WordSpec with MustMatchers {

  val decode = new JaiDecoder()
  val encode = new JaiEncoder()
  "Encode and decode on JaiErrorMessage" must {
    "result in same object" in {

      val response = new JaiErrorMessage(new Date(), ErrorCodeMessage.CAMERA_BUSY, ErrorStateMessage.ONE_TIME)
      val buffer = encode.encode(null, null, response)
      //decode
      val decoded = decode.decode(null, null, buffer.asInstanceOf[ChannelBuffer])
      decoded must be(response)
    }
  }
  "Encode and decode on JaiErrorAck" must {
    "result in same object" in {

      val msg = new JaiErrorAck()
      val buffer = encode.encode(null, null, msg)
      //decode
      val decoded = decode.decode(null, null, buffer.asInstanceOf[ChannelBuffer])
      decoded must be(msg)
    }
  }
  "Encode and decode on JaiStatusMessage" must {
    "result in same object" in {
      val msg = new JaiStatusMessage(Seq[Short](1, 3, 5, 255))
      val buffer = encode.encode(null, null, msg)
      //decode
      val decoded = decode.decode(null, null, buffer.asInstanceOf[ChannelBuffer])
      decoded must be(msg)
    }
    "result in same object with empty list" in {

      val msg = new JaiStatusMessage(Seq())
      val buffer = encode.encode(null, null, msg)
      //decode
      val decoded = decode.decode(null, null, buffer.asInstanceOf[ChannelBuffer])
      decoded must be(msg)
    }
  }
  "Encode and decode on JaiStatusAck" must {
    "result in same object with items" in {

      val msg = new JaiStatusAck(Seq(new StatusItem(1, 4L), new StatusItem(80, 0L)))
      val buffer = encode.encode(null, null, msg)
      //decode
      val decoded = decode.decode(null, null, buffer.asInstanceOf[ChannelBuffer])
      decoded must be(msg)
    }
    "result in same object without items" in {
      val msg = new JaiStatusAck(Seq())
      val buffer = encode.encode(null, null, msg)
      //decode
      val decoded = decode.decode(null, null, buffer.asInstanceOf[ChannelBuffer])
      decoded must be(msg)
    }
  }
  "Encode and decode on TriggerMessage" must {
    "result in same object with trigger" in {

      val msg = new JaiTriggerMessage(CPU_Trigger = true, Repetitive = true, id = BigInt(0xFFFFFF), triggerTime = 60000)
      val buffer = encode.encode(null, null, msg)
      //decode
      val decoded = decode.decode(null, null, buffer.asInstanceOf[ChannelBuffer])
      decoded must be(msg)
    }
  }
  "Encode and decode on TriggerMessage" must {
    "result in same object without trigger" in {

      val msg = new JaiTriggerMessage(CPU_Trigger = true, Repetitive = false, id = BigInt(0xFFFFFF), triggerTime = 0)
      val buffer = encode.encode(null, null, msg)
      //decode
      val decoded = decode.decode(null, null, buffer.asInstanceOf[ChannelBuffer])
      decoded must be(msg)
    }
  }
  "Encode and decode on TriggerAck" must {
    "result in same object" in {

      val msg = new JaiTriggerAck(id = BigInt(0xFFFFFFFF), timestamp = new Date(), errorCodes = 255)
      val buffer = encode.encode(null, null, msg)
      //decode
      val decoded = decode.decode(null, null, buffer.asInstanceOf[ChannelBuffer])
      decoded must be(msg)
    }
  }
  "Encode and decode on TriggerAckFailure" must {
    "result in same object" in {

      val msg = new JaiTriggerError(255)
      val buffer = encode.encode(null, null, msg)
      //decode
      val decoded = decode.decode(null, null, buffer.asInstanceOf[ChannelBuffer])
      decoded must be(msg)
    }
  }
  "Encode and decode on DataReady" must {
    "result in same object" in {

      val msg = new JaiDataReadyMessage(id = BigInt(0xFFFFFFFF), imageSeq = 1, dataType = 1, timestamp = new Date())
      val buffer = encode.encode(null, null, msg)
      //decode
      val decoded = decode.decode(null, null, buffer.asInstanceOf[ChannelBuffer])
      decoded must be(msg)
    }
  }
  "Encode and decode on DataReadyAck" must {
    "result in same object" in {

      val msg = new JaiDataReadyAck()
      val buffer = encode.encode(null, null, msg)
      //decode
      val decoded = decode.decode(null, null, buffer.asInstanceOf[ChannelBuffer])
      decoded must be(msg)
    }
  }
  "Encode and decode on sendData" must {
    "result in same object" in {

      val msg = new JaiSendDataMessage(id = BigInt(0xFFFFFFFF), imageSeq = 1, dataType = 1)
      val buffer = encode.encode(null, null, msg)
      //decode
      val decoded = decode.decode(null, null, buffer.asInstanceOf[ChannelBuffer])
      decoded must be(msg)
    }
  }
  "Encode and decode on sendDataAck" must {
    "result in same object" in {
      val sec = System.currentTimeMillis() / 1000L
      val msg = new JaiSendDataAck(
        id = BigInt(0xFFFFFFFF),
        error = 0,
        imageSeq = 1,
        dataType = 1,
        acquireTime = new Date(sec * 1000L), //msec need to be zero
        present = true,
        auxWidth = 20,
        auxHeight = 40,
        auxData = "AUX".getBytes(),
        packageData = "package".getBytes(),
        image = "IMAGE".getBytes())
      val buffer = encode.encode(null, null, msg)
      //decode
      val decoded = decode.decode(null, null, buffer.asInstanceOf[ChannelBuffer]).asInstanceOf[JaiSendDataAck]
      decoded.id must be(msg.id)
      decoded.error must be(msg.error)
      decoded.imageSeq must be(msg.imageSeq)
      decoded.dataType must be(msg.dataType)
      decoded.acquireTime must be(msg.acquireTime)
      decoded.present must be(msg.present)
      decoded.auxWidth must be(msg.auxWidth)
      decoded.auxHeight must be(msg.auxHeight)
      compareArrays(decoded.auxData, msg.auxData)
      compareArrays(decoded.packageData, msg.packageData)
      compareArrays(decoded.image, msg.image)
    }
  }
  "Encode and decode on sendDataAckError" must {
    "result in same object" in {
      val msg = new JaiSendDataAckError(
        id = BigInt(0xFFFFFFFF),
        error = 7,
        imageSeq = 1,
        dataType = 1)
      val buffer = encode.encode(null, null, msg)
      //decode
      val decoded = decode.decode(null, null, buffer.asInstanceOf[ChannelBuffer])
      decoded must be(msg)
    }
  }
  def compareArrays(decoded: Array[Byte], src: Array[Byte]) {
    decoded.length must be(src.length)
    for (i ← 0 until src.length) {
      decoded(i) must be(src(i))
    }
  }
}