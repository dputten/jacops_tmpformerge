/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package tools.jailogger

import csc.akka.logging.DirectLogging
import java.io.{ File, IOException, InputStreamReader, BufferedReader }
import org.apache.commons.io.FileUtils
import akka.actor.{ Props, ActorSystem, Actor, ActorLogging }
import csc.vehicle.jai.{ CameraTriggerImage, JaiSendDataAck, JaiCameraConnection }

/**
 * This is the object which starts the logger.
 * It is responsible
 * to connect to the pir and log the messages
 *
 */
object JaiLogger extends DirectLogging {
  type OptionMap = Map[Symbol, String]
  var reader = new BufferedReader(new InputStreamReader(System.in))

  /**
   * Parse the command arguments
   */
  def nextOption(map: OptionMap, list: List[String]): OptionMap = {
    list match {
      case Nil ⇒ map
      case "--host" :: value :: tail ⇒
        nextOption(map ++ Map('host -> value), tail)
      case "--help" :: tail ⇒
        nextOption(map ++ Map('help -> ""), tail)
      case "--port" :: value :: tail ⇒
        nextOption(map ++ Map('port -> value), tail)
      case "--retries" :: value :: tail ⇒
        nextOption(map ++ Map('retries -> value), tail)
      case "--disconectTime" :: value :: tail ⇒
        nextOption(map ++ Map('disconectTime -> value), tail)
      case option :: tail ⇒
        println("Unknown option " + option)
        sys.exit(1)
    }
  }

  /**
   * Prints the usage string to standard out
   */
  def printUsage() {
    println("JaiLogger Usage --host <host> --port <port> [options]")
    println("\t--host <host>: host name of the PIR")
    println("\t--port <port>: the port of the PIR")
    println("\t--retries <nr>: the number of retries of sending the camera trigger")
    println("\t--disconectTime <time>: time to detect disconnect")
    println("\t--help: show the usage and script usage")
  }

  /**
   * The main tread.
   * Parse arguments and check them for correctness
   */
  def main(args: Array[String]): Unit = {
    val options = nextOption(Map(), args.toList)
    println(options)
    options.get('help) foreach {
      value ⇒
        {
          printUsage
          sys.exit(0)
        }
    }
    val hostOpt = options.get('host)
    val portOpt = options.get('port)
    if (hostOpt == None || portOpt == None) {
      println("Logger: host and port should be given")
      printUsage
      sys.exit(1)
    }
    val retries = options.get('retries).getOrElse("10").toInt
    val disconnect = options.get('disconectTime).getOrElse("10000").toLong
    val system = ActorSystem("JaiLogger")
    val logger = system.actorOf(Props[MsgLogger], "CameraMessageLogger")
    val connection = new JaiCameraConnection("JaiLogger", hostOpt.get, portOpt.get.toInt, retries, disconnect, 0xFF, Set(logger))

    connection.start()
    //wait
    var reader = new BufferedReader(new InputStreamReader(System.in))
    var inputLine: String = null;
    //  readLine() method
    println("Logger: To send a image trigger type: send<enter>")
    println("Logger: To send a get status type: status1<enter>")
    println("Logger: To send a get all status and configuration type: status2<enter>")
    println("Logger: To end process type: exit<enter>")
    try {
      var process = true
      while (process) {
        System.out.print("Wait for command input: ");
        inputLine = reader.readLine();
        if (inputLine.startsWith("exit")) {
          process = false
        }
        if (inputLine.startsWith("send")) {
          connection.sendCameraTrigger()
        } else if (inputLine.startsWith("status1")) {
          connection.sendCameraStatus()
        } else if (inputLine.startsWith("status2")) {
          connection.sendAllCameraStatus()
        }
      }
    } catch {
      case ex: IOException ⇒ System.out.println("IO error while waiting for input. resuming scriptprocessing");
    } finally {
      reader.close()
    }

    //stop system
    System.out.println("Stopping Logger")

    connection.shutdown()
    system.stop(logger)
    system.shutdown()
    System.out.println("Logger Stoped")
    sys.exit(0)
  }

}

class MsgLogger extends Actor with ActorLogging {
  var numberImages = 0
  def receive = {
    case image: CameraTriggerImage ⇒ {
      numberImages += 1
      val file = new File("cameraImage%d.tif".format(numberImages))
      log.info("creating file %s Received: %s".format(file.getAbsolutePath, image.toString))
      FileUtils.writeByteArrayToFile(file, image.image)
    }

    case any: AnyRef ⇒ log.info("Received: " + any.toString)
  }
}