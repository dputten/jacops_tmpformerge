/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.jai

import impl._
import org.jboss.netty.util.HashedWheelTimer
import org.jboss.netty.channel.socket.nio.NioClientSocketChannelFactory
import org.jboss.netty.bootstrap.ClientBootstrap
import java.net.InetSocketAddress
import org.jboss.netty.channel.{ ChannelPipelineFactory, ChannelPipeline, Channels }
import akka.actor.ActorRef
import csc.akka.logging.DirectLogging
import java.util.concurrent.Executors

/**
 * A class to bootstrap a connection to a Jai Camera.
 * @param laneId
 * @param host
 * @param port
 * @param maxTriggerRetries
 * @param timeDisconnected
 * @param cameraTriggerId
 * @param recipients
 */
class JaiCameraConnection(laneId: String,
                          host: String,
                          port: Int,
                          maxTriggerRetries: Int,
                          timeDisconnected: Long,
                          cameraTriggerId: BigInt,
                          recipients: Set[ActorRef]) extends DirectLogging {

  private val bootstrap: ClientBootstrap = new ClientBootstrap(
    new NioClientSocketChannelFactory(
      Executors.newCachedThreadPool(),
      Executors.newCachedThreadPool()))

  private val timer = new HashedWheelTimer()

  private val msgProcessor = new JaiConnectionProcessor(
    laneId = laneId,
    bootstrap = bootstrap,
    timer = timer,
    timeout = 5000L,
    maxTriggerRetries = maxTriggerRetries,
    timeDisconnected = timeDisconnected,
    cameraTriggerId = cameraTriggerId,
    recipients = recipients,
    channelGroup = new NettyConnectionGroup)

  init()

  /**
   * Initialize the connection by setting all the handlers.
   */
  private def init() {
    // Configure the connection.

    val remoteHost = new InetSocketAddress(host.trim(), port)
    val channelHandler = new JaiChannelHandler(msgProcessor, remoteHost)
    val decoder = new JaiDecoder()
    val encoder = new JaiEncoder

    bootstrap.setPipelineFactory(new ChannelPipelineFactory {
      def getPipeline(): ChannelPipeline = {
        val pipeline = Channels.pipeline()
        pipeline.addLast("decoder", decoder)
        pipeline.addLast("encoder", encoder)
        pipeline.addLast("handler", channelHandler)
        pipeline
      }
    })
    bootstrap.setOption("remoteAddress", remoteHost)
    //PCL-56: now using SO keepAlive to recover as soon as possible
    //PCL-56: later commented out, as it works EVERYWHERE except with a real camera
    //bootstrap.setOption("child.tcpNoDelay", true)
    //bootstrap.setOption("child.keepAlive", true)
  }

  /**
   * Start the connection by trying to connect for the first time.
   * After tis initial start the handlers will manage the connection with the Camera
   */
  def start() {
    bootstrap.connect
  }

  /**
   * Starts the connection and waits for it to complete
   */
  def startAndWait() {
    bootstrap.connect.await()
  }

  /**
   * Stop the connection and release all the resources , like sockets and threads.
   *
   */
  def shutdown() {
    try {
      msgProcessor.shutdown
    } catch {
      case ex: Exception ⇒ log.error("Exception while shutting down Processor: %s", ex.getMessage)
    }
    try {
      timer.stop
    } catch {
      case ex: Exception ⇒ log.error("Exception while shutting down timer: %s", ex.getMessage)
    }
    try {
      // Shut down thread pools to exit.
      bootstrap.releaseExternalResources()
    } catch {
      case ex: Exception ⇒ log.error("Exception while shutting down bootsrtap: %s", ex.getMessage)
    }
  }

  def sendCameraTrigger() {
    msgProcessor.sendCameraTrigger()
  }
  def sendCameraStatus() {
    msgProcessor.sendStatus()
  }
  def sendAllCameraStatus() {
    msgProcessor.sendAllStatus()
  }

}
