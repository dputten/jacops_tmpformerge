/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.jai.impl

import org.jboss.netty.buffer.ChannelBuffer
import org.jboss.netty.channel.{ Channel, ChannelHandlerContext }
import org.jboss.netty.handler.codec.frame.{ CorruptedFrameException, FrameDecoder }
import collection.mutable.ListBuffer
import csc.akka.logging.DirectLogging
import java.util.Date
import csc.vehicle.jai._
import java.text.SimpleDateFormat
import math.min

/**
 * Sizes of different fields
 * The Byte values are implemented as Shorts because these are unsigned Bytes
 */
object FieldConstants {
  val SIZE_HEADER = 8
  val SOURCE_CMD_SEPARATION = 0x2000
  val ERROR_CMD = 0x1001
  val ERROR_CMD_ACK = 0x2001
  val STATUS = 0x2008
  val STATUS_ACK = 0x1008
  val TRIGGER = 0x2004
  val TRIGGER_ACK = 0x1004
  val SEND_DATA = 0x2005
  val SEND_DATA_ACK = 0x1005
  val DATA_READY = 0x1002
  val DATA_READY_ACK = 0x2002

  val BIT_1 = 0x01
  val BIT_7 = 0x40

  val SOURCE_EN = 0x10
  val SOURCE_LC = 0x20
}

/**
 * This class is responsible to decode the received stream into the PirMessages.
 */
class JaiDecoder() extends FrameDecoder with DirectLogging {
  /**
   * This method is called by the netty framwork to decode the messages.
   * @param ctx the channleGandlerContext
   * @param channel the channel object
   * @param buffer the buffer containg the encoded message
   * @return null when not all the bytes are received otherwise the decoded Message
   * @throws CorruptedFrameException when the sizes doesn't match
   */
  def decode(ctx: ChannelHandlerContext, channel: Channel, buffer: ChannelBuffer): Object = {
    if (channel == null) {
      //This happens only in unit tests
      log.debug("Decode message")
    } else {
      log.debug("Decode message from %s".format(channel.getLocalAddress))
    }
    // Wait until the message type and length is available.
    var nrBytes = buffer.readableBytes()
    if (nrBytes < FieldConstants.SIZE_HEADER) {
      log.debug("Not enough bytes %d needed %d".format(nrBytes, FieldConstants.SIZE_HEADER))
      return null
    }
    if (log.isDebugEnabled) {
      buffer.markReaderIndex()
      val bytes = new Array[Byte](nrBytes)
      buffer.readBytes(bytes)
      val strBuf = new StringBuffer()
      for (i ← 0 until (min(bytes.length, 100))) {
        val str = bytes(i).toInt.toHexString
        val hexByte = str.length() match {
          case 1 ⇒ "0" + str
          case 2 ⇒ str
          case l ⇒ str.substring(l - 2)
        }
        strBuf.append(hexByte)
        strBuf.append(" ")
      }
      if (bytes.length > 100) {
        strBuf.append(".......")
      }
      log.debug("Received bytes= %s".format(strBuf.toString))
      buffer.resetReaderIndex()
    }
    //mark start in buffer
    buffer.markReaderIndex()

    // Check the message Type.
    val imageDataLength = buffer.readUnsignedInt()
    val msgDataLength = buffer.readUnsignedShort()
    val command = buffer.readUnsignedByte()
    val source = buffer.readUnsignedByte()
    val commandType = source << 8 | command

    val dynamicPackageSize = imageDataLength + msgDataLength
    val availableBytes = buffer.readableBytes()
    if (availableBytes < dynamicPackageSize) {
      buffer.resetReaderIndex()
      log.debug("Not enough bytes %d needed %d".format(availableBytes, dynamicPackageSize))
      return null
    }
    val msgData = buffer.readBytes(msgDataLength)
    val imageData = buffer.readBytes(imageDataLength.toInt)
    //when messages is not supported no data is returned but dat bytes are read
    var decodedObject: Object = null
    commandType match {
      case FieldConstants.ERROR_CMD ⇒ {
        log.debug("Received Message Command is ERROR")
        decodedObject = readErrorMessage(command, msgData, msgDataLength)
      }
      case FieldConstants.DATA_READY ⇒ {
        log.debug("Received Message Command is DATA_RDY")
        decodedObject = readDataReady(command, msgData, msgDataLength)
      }
      case 0x1003 ⇒ log.debug("Received Message Command is TRIG/DATA_ID_ACK")
      case FieldConstants.TRIGGER_ACK ⇒ {
        log.debug("Received Message Command is TRIGGER_ACK")
        decodedObject = readTriggerAck(command, msgData, msgDataLength)
      }
      case FieldConstants.SEND_DATA_ACK ⇒ {
        log.debug("Received Message Command is SEND_DATA_ACK")
        val image = new Array[Byte](imageData.readableBytes())
        imageData.readBytes(image)
        decodedObject = readSendDataAck(command, msgData, msgDataLength, image)
      }
      case 0x1006 ⇒ log.debug("Received Message Command is DEL_DATA_ACK")
      case 0x1007 ⇒ log.debug("Received Message Command is CONFIG_ACK")
      case FieldConstants.STATUS_ACK ⇒ {
        log.debug("Received Message Command is STATUS_ACK")
        decodedObject = readStatusAck(command, msgData, msgDataLength)
      }
      case 0x1009 ⇒ log.debug("Received Message Command is TX_CAMERA_ACK")
      case 0x100A ⇒ log.debug("Received Message Command is RX_CAMERA")
      case 0x100B ⇒ log.debug("Received Message Command is TX_LASER_ACK")
      case 0x100C ⇒ log.debug("Received Message Command is RX_LASER")
      case 0x100D ⇒ log.debug("Received Message Command is FTP_ACK")
      case 0x100E ⇒ log.debug("Received Message Command is F/W_UPDATE_ACK")
      case 0x100F ⇒ log.debug("Received Message Command is USER_INFO_ACK")
      case 0x1010 ⇒ log.debug("Received Message Command is STORAGE_ACK")
      case 0x1011 ⇒ log.debug("Received Message Command is DIRECTORY_ACK")
      case 0x1012 ⇒ log.debug("Received Message Command is CAMERA_CFG_ACK")
      case 0x1013 ⇒ log.debug("Received Message Command is USER_DYNAMIC_DATA_ACK")
      case 0x1014 ⇒ log.debug("Received Message Command is CONTINUOS_TRIGGERACK (TBD)")
      case 0x1015 ⇒ log.debug("Received Message Command is MULTIPLE_TRIGGER\nACK(TBD)")
      case 0x1016 ⇒ log.debug("Received Message Command is TTLTRIG_MSG")
      case FieldConstants.ERROR_CMD_ACK ⇒ {
        log.debug("Received Message Command is ERROR_ACK")
        decodedObject = new JaiErrorAck()
      }
      case FieldConstants.DATA_READY_ACK ⇒ {
        log.debug("Received Message Command is DATA_RDY_ACK")
        decodedObject = new JaiDataReadyAck()
      }
      case FieldConstants.TRIGGER ⇒ {
        log.debug("Received Message Command is TRIGGER")
        decodedObject = readTriggerMessage(command, msgData, msgDataLength)
      }
      case FieldConstants.SEND_DATA ⇒ {
        log.debug("Received Message Command is SEND_DATA")
        decodedObject = readSendDataMessage(command, msgData, msgDataLength)
      }
      case 0x2006 ⇒ log.debug("Received Message Command is DEL_DATA")
      case 0x2007 ⇒ log.debug("Received Message Command is CONFIG")
      case FieldConstants.STATUS ⇒ {
        log.debug("Received Message Command is STATUS")
        decodedObject = readStatusMessage(command, msgData, msgDataLength)
      }
      case 0x2009 ⇒ log.debug("Received Message Command is TX_CAMERA")
      case 0x200A ⇒ log.debug("Received Message Command is RX_CAMERA_ACK")
      case 0x200B ⇒ log.debug("Received Message Command is TX_LASER")
      case 0x200C ⇒ log.debug("Received Message Command is RX_LASER_ACK")
      case 0x200D ⇒ log.debug("Received Message Command is FTP")
      case 0x200E ⇒ log.debug("Received Message Command is F/W_UPDATE")
      case 0x200F ⇒ log.debug("Received Message Command is USER_INFO")
      case 0x2010 ⇒ log.debug("Received Message Command is STORAGE")
      case 0x2011 ⇒ log.debug("Received Message Command is DIRECTORY")
      case 0x2012 ⇒ log.debug("Received Message Command is CAMERA_CFG")
      case 0x2013 ⇒ log.debug("Received Message Command is USER_DYNAMIC_DATA")
      case 0x2014 ⇒ log.debug("Received Message Command is CONTINUOS_TRIGGER")
      case 0x2015 ⇒ log.debug("Received Message Command is MULTIPLE_TRIGGER")
      case 0x2016 ⇒ log.debug("Received Message Command is TTLTRIG_MSG_ACK")
      case _      ⇒ log.warning("Received Unknown command %d".format(command))
    }
    decodedObject
  }

  def readErrorMessage(command: Int, msgData: ChannelBuffer, msgDataLength: Int): JaiErrorMessage = {
    val errorCode = msgData.readUnsignedByte()
    val state = msgData.readUnsignedByte()
    val realTimestampSec = msgData.readUnsignedInt() //Time that the image was taken. UTC Seconds from 1/1/1970
    val timeMsec = msgData.readShort()
    val timestamp = new Date(realTimestampSec * 1000 + timeMsec)
    val dataSize = msgData.readUnsignedByte()
    //ignoring data for now
    val errCode = {
      try {
        ErrorCodeMessage(errorCode.toInt)
      } catch {
        case ex: Exception ⇒ {
          log.error("Unexpected error code %d".format(errorCode))
          ErrorCodeMessage.UNKNOWN
        }
      }
    }
    val errState = {
      try {
        ErrorStateMessage(state.toInt)
      } catch {
        case ex: Exception ⇒ {
          log.error("Unexpected error state %d".format(state))
          ErrorStateMessage.UNKNOWN
        }
      }

    }
    new JaiErrorMessage(timestamp, errCode, errState)
  }

  def readStatusMessage(command: Int, msgData: ChannelBuffer, msgDataLength: Int): JaiStatusMessage = {
    val nrItems = msgData.readUnsignedByte()
    val statusCodes = new ListBuffer[Short]()
    for (item ← 0 until nrItems) {
      statusCodes += msgData.readUnsignedByte()
    }
    new JaiStatusMessage(statusCodes.toSeq)
  }

  def readStatusAck(command: Int, msgData: ChannelBuffer, msgDataLength: Int): JaiStatusAck = {
    val nrItems = msgDataLength / 5 //5 bytes per item
    val statusItems = new ListBuffer[StatusItem]()
    for (item ← 0 until nrItems) {
      val statusCode = msgData.readUnsignedByte()
      val statusValue = msgData.readUnsignedInt()
      statusItems += new StatusItem(statusCode, statusValue)
    }
    new JaiStatusAck(statusItems.toSeq)
  }
  def readTriggerAck(command: Int, msgData: ChannelBuffer, msgDataLength: Int): Object = {
    val errorCodes = msgData.readUnsignedByte()
    if (msgDataLength == 1) {
      new JaiTriggerError(errorCodes)
    } else {
      val triggerIdSize = msgData.readUnsignedByte()
      //unclear how it should be interpreted
      //doc says: The Trigger/Data ID of the data that the LC is requesting.
      // size: Var 1 to 10 bytes
      //value: 1 to FFFFFFFF (this is 4 bytes why can it be 10 bytes)

      val bytes = new Array[Byte](triggerIdSize)
      msgData.readBytes(bytes)
      val id = BigInt(bytes)
      //TEST
      val extraBytes = msgDataLength - (8 + triggerIdSize)
      if (extraBytes > 0) {
        //read extra bytes
        for (i ← 0 until extraBytes) {
          if (msgData.readUnsignedByte() != 0) {
            log.warning("read a none empty byte")
          }
        }
      }
      //END TEST
      val realTimestampSec = msgData.readUnsignedInt() //Time that the image was taken. UTC Seconds from 1/1/1970
      val timeMsec = msgData.readUnsignedShort()

      val timestamp = new Date(realTimestampSec * 1000 + timeMsec)
      log.debug("ReadTriggerTimes Seconds %d msec %d total %d Time %s".format(
        realTimestampSec, timeMsec, timestamp.getTime, (new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS z")).format(timestamp)))

      new JaiTriggerAck(id, timestamp, errorCodes)
    }
  }
  def readTriggerMessage(command: Int, msgData: ChannelBuffer, msgDataLength: Int): JaiTriggerMessage = {
    val command = msgData.readUnsignedByte()
    val triggerIdSize = msgData.readUnsignedByte()
    //unclear how it should be interpreted
    //doc says: The Trigger/Data ID of the data that the LC is requesting.
    // size: Var 1 to 10 bytes
    //value: 1 to FFFFFFFF (this is 4 bytes why can it be 10 bytes)

    val bytes = new Array[Byte](triggerIdSize)
    msgData.readBytes(bytes)
    val id = BigInt(bytes)
    val triggerTime = if (msgData.readableBytes() >= 2)
      msgData.readUnsignedShort()
    else
      0
    new JaiTriggerMessage((command & FieldConstants.BIT_1) != 0, (command & FieldConstants.BIT_7) != 0, id, triggerTime)
  }

  def readDataReady(command: Int, msgData: ChannelBuffer, msgDataLength: Int): JaiDataReadyMessage = {
    val triggerIdSize = msgData.readUnsignedByte()
    val bytes = new Array[Byte](triggerIdSize)
    msgData.readBytes(bytes)
    val id = BigInt(bytes)
    val imageSequence = msgData.readUnsignedByte()
    val dataType = msgData.readUnsignedByte()
    val realTimestampSec = msgData.readUnsignedInt() //Time that the image was taken. UTC Seconds from 1/1/1970
    val timeMsec = msgData.readUnsignedShort()

    val timestamp = new Date(realTimestampSec * 1000 + timeMsec)

    new JaiDataReadyMessage(id = id, imageSeq = imageSequence, dataType = dataType, timestamp = timestamp)
  }

  def readSendDataMessage(command: Int, msgData: ChannelBuffer, msgDataLength: Int): JaiSendDataMessage = {
    val triggerIdSize = msgData.readUnsignedByte()
    val bytes = new Array[Byte](triggerIdSize)
    msgData.readBytes(bytes)
    val id = BigInt(bytes)
    val imageSequence = msgData.readUnsignedByte()
    val dataType = msgData.readUnsignedByte()

    new JaiSendDataMessage(id = id, imageSeq = imageSequence, dataType = dataType)
  }
  def readSendDataAck(command: Int, msgData: ChannelBuffer, msgDataLength: Int, image: Array[Byte]): AnyRef = {
    val error = msgData.readUnsignedByte()
    val triggerIdSize = msgData.readUnsignedByte()
    val bytes = new Array[Byte](triggerIdSize)
    msgData.readBytes(bytes)
    val id = BigInt(bytes)
    val imageSequence = msgData.readUnsignedByte()
    val dataType = msgData.readUnsignedByte()
    if (msgData.readableBytes() == 0) {
      //Probably an error received and rest of data isn't pressent
      return new JaiSendDataAckError(id = id, error = error, imageSeq = imageSequence, dataType = dataType)
    }
    val realTimestampSec = msgData.readUnsignedInt()
    val present = msgData.readUnsignedByte()
    val auxSize = msgData.readUnsignedByte()
    val auxWidth = msgData.readUnsignedShort()
    val auxHeight = msgData.readUnsignedShort()
    val auxMiscBytes = new Array[Byte](auxSize)
    msgData.readBytes(auxMiscBytes)
    val packageBytes = if (msgData.readableBytes() > 0) {
      val packageSize = msgData.readUnsignedInt()
      val bytes = new Array[Byte](packageSize.toInt)
      msgData.readBytes(bytes)
      bytes
    } else {
      new Array[Byte](0)
    }

    new JaiSendDataAck(id = id, error = error, imageSeq = imageSequence, dataType = dataType,
      acquireTime = new Date(realTimestampSec * 1000), present = (present == 0), auxWidth = auxWidth, auxHeight = auxHeight,
      auxData = auxMiscBytes, packageData = packageBytes, image = image)
  }

}
