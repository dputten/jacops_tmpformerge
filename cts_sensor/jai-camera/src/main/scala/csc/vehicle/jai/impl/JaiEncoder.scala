/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.jai.impl

import org.jboss.netty.handler.codec.oneone.OneToOneEncoder
import org.jboss.netty.channel.{ Channel, ChannelHandlerContext }
import org.jboss.netty.buffer.ChannelBuffers
import java.nio.ByteOrder
import csc.akka.logging.DirectLogging
import csc.vehicle.jai._

/**
 * This class is responsible to encode the different PirMessages
 */
class JaiEncoder extends OneToOneEncoder with DirectLogging {
  /**
   * this method is called by netty framework when a message has to be encoded.
   *
   * @param ctx the netty ChannelHandlerContext
   * @param channel The channel object
   * @param msg the message which has to be encoded
   * @return msg when a unsupported message is received and otherwise a ChannelBuffer with the encoded message
   */
  def encode(ctx: ChannelHandlerContext, channel: Channel, msg: Object): Object = {
    log.debug("Encode message %s".format(msg.toString))

    val (dataChannel, command, imageOpt) = msg match {
      case errMsg: JaiErrorMessage ⇒ {

        val resultBuffer = ChannelBuffers.dynamicBuffer(ByteOrder.BIG_ENDIAN, 9)
        resultBuffer.writeByte(errMsg.errorCode.id)
        resultBuffer.writeByte(errMsg.state.id)
        resultBuffer.writeInt((errMsg.timestamp.getTime / 1000).toInt)
        resultBuffer.writeShort((errMsg.timestamp.getTime % 1000).toInt)
        resultBuffer.writeByte(0) //Optional data not supported at this time
        (resultBuffer, FieldConstants.ERROR_CMD, None)
      }
      case ack: JaiErrorAck ⇒ {
        val resultBuffer = ChannelBuffers.dynamicBuffer(ByteOrder.BIG_ENDIAN, 0)
        (resultBuffer, FieldConstants.ERROR_CMD_ACK, None)
      }
      case statusMsg: JaiStatusMessage ⇒ {
        val resultBuffer = ChannelBuffers.dynamicBuffer(ByteOrder.BIG_ENDIAN, 1 + statusMsg.statusCodes.length)
        resultBuffer.writeByte(statusMsg.statusCodes.length)
        for (code ← statusMsg.statusCodes) {
          resultBuffer.writeByte(code)
        }
        (resultBuffer, FieldConstants.STATUS, None)
      }
      case ack: JaiStatusAck ⇒ {
        val resultBuffer = ChannelBuffers.dynamicBuffer(ByteOrder.BIG_ENDIAN, ack.statusItems.length * 5)
        for (item ← ack.statusItems) {
          resultBuffer.writeByte(item.code)
          resultBuffer.writeInt(item.value.toInt)
        }
        (resultBuffer, FieldConstants.STATUS_ACK, None)
      }
      case triggerMsg: JaiTriggerMessage ⇒ {
        val idBytes = triggerMsg.id.toByteArray
        val repeatSize = if (triggerMsg.Repetitive) 2 else 0
        val resultBuffer = ChannelBuffers.dynamicBuffer(ByteOrder.BIG_ENDIAN, 2 + idBytes.length + repeatSize)
        var command = 0x00
        if (triggerMsg.CPU_Trigger) {
          command |= FieldConstants.BIT_1
        }
        if (triggerMsg.Repetitive) {
          command |= FieldConstants.BIT_7
        }
        resultBuffer.writeByte(command)
        resultBuffer.writeByte(idBytes.length)
        resultBuffer.writeBytes(idBytes)
        if (triggerMsg.Repetitive) {
          resultBuffer.writeShort(triggerMsg.triggerTime)
        }
        (resultBuffer, FieldConstants.TRIGGER, None)
      }
      case ack: JaiTriggerAck ⇒ {
        val idBytes = ack.id.toByteArray
        val resultBuffer = ChannelBuffers.dynamicBuffer(ByteOrder.BIG_ENDIAN, 8 + idBytes.length)
        resultBuffer.writeByte(ack.errorCodes)
        resultBuffer.writeByte(idBytes.length)
        resultBuffer.writeBytes(idBytes)
        resultBuffer.writeInt((ack.timestamp.getTime / 1000).toInt)
        resultBuffer.writeShort((ack.timestamp.getTime % 1000).toInt)

        (resultBuffer, FieldConstants.TRIGGER_ACK, None)
      }
      case ack: JaiTriggerError ⇒ {
        val resultBuffer = ChannelBuffers.dynamicBuffer(ByteOrder.BIG_ENDIAN, 1)
        resultBuffer.writeByte(ack.errorCodes)
        (resultBuffer, FieldConstants.TRIGGER_ACK, None)
      }
      case sendMsg: JaiDataReadyMessage ⇒ {
        val idBytes = sendMsg.id.toByteArray
        val resultBuffer = ChannelBuffers.dynamicBuffer(ByteOrder.BIG_ENDIAN, 9 + idBytes.length)
        resultBuffer.writeByte(idBytes.length)
        resultBuffer.writeBytes(idBytes)
        resultBuffer.writeByte(sendMsg.imageSeq)
        resultBuffer.writeByte(sendMsg.dataType)
        resultBuffer.writeInt((sendMsg.timestamp.getTime / 1000).toInt)
        resultBuffer.writeShort((sendMsg.timestamp.getTime % 1000).toInt)

        (resultBuffer, FieldConstants.DATA_READY, None)
      }
      case ack: JaiDataReadyAck ⇒ {
        val resultBuffer = ChannelBuffers.dynamicBuffer(ByteOrder.BIG_ENDIAN, 0)
        (resultBuffer, FieldConstants.DATA_READY_ACK, None)
      }
      case sendMsg: JaiSendDataMessage ⇒ {
        val idBytes = sendMsg.id.toByteArray
        val resultBuffer = ChannelBuffers.dynamicBuffer(ByteOrder.BIG_ENDIAN, 3 + idBytes.length)
        resultBuffer.writeByte(idBytes.length)
        resultBuffer.writeBytes(idBytes)
        resultBuffer.writeByte(sendMsg.imageSeq)
        resultBuffer.writeByte(sendMsg.dataType)

        (resultBuffer, FieldConstants.SEND_DATA, None)
      }
      case sendAck: JaiSendDataAck ⇒ {
        val idBytes = sendAck.id.toByteArray
        val resultBuffer = ChannelBuffers.dynamicBuffer(ByteOrder.BIG_ENDIAN, 18 + idBytes.length + sendAck.auxData.length + sendAck.packageData.length)
        resultBuffer.writeByte(sendAck.error)
        resultBuffer.writeByte(idBytes.length)
        resultBuffer.writeBytes(idBytes)
        resultBuffer.writeByte(sendAck.imageSeq)
        resultBuffer.writeByte(sendAck.dataType)
        resultBuffer.writeInt((sendAck.acquireTime.getTime / 1000).toInt)
        resultBuffer.writeByte(if (sendAck.present) 0 else 1)
        resultBuffer.writeByte(sendAck.auxData.length)
        resultBuffer.writeShort(sendAck.auxWidth)
        resultBuffer.writeShort(sendAck.auxHeight)
        resultBuffer.writeBytes(sendAck.auxData)
        resultBuffer.writeInt(sendAck.packageData.length)
        resultBuffer.writeBytes(sendAck.packageData)
        (resultBuffer, FieldConstants.SEND_DATA_ACK, Some(sendAck.image))
      }
      case sendAck: JaiSendDataAckError ⇒ {
        val idBytes = sendAck.id.toByteArray
        val resultBuffer = ChannelBuffers.dynamicBuffer(ByteOrder.BIG_ENDIAN, 4 + idBytes.length)
        resultBuffer.writeByte(sendAck.error)
        resultBuffer.writeByte(idBytes.length)
        resultBuffer.writeBytes(idBytes)
        resultBuffer.writeByte(sendAck.imageSeq)
        resultBuffer.writeByte(sendAck.dataType)
        (resultBuffer, FieldConstants.SEND_DATA_ACK, None)
      }
      case msg: AnyRef ⇒ return msg //if object isn't supported just return the object itself so another Encoder can encode this one
    }
    //create envelope
    val msgLength = dataChannel.readableBytes()
    val resultBuffer = ChannelBuffers.dynamicBuffer(ByteOrder.BIG_ENDIAN, 8 + msgLength)
    val imageSize = imageOpt match {
      case Some(bytes) ⇒ bytes.length
      case None        ⇒ 0
    }
    resultBuffer.writeInt(imageSize)
    resultBuffer.writeShort(msgLength)
    resultBuffer.writeByte(command & 0x00FF)
    val source = command >> 8
    resultBuffer.writeByte(source)
    resultBuffer.writeBytes(dataChannel)
    imageOpt.foreach(bytes ⇒ resultBuffer.writeBytes(bytes))
    resultBuffer
  }

}