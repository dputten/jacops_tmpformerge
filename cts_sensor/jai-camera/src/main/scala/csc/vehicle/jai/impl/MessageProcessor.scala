/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.jai.impl

import java.util.concurrent.TimeUnit
import org.jboss.netty.util.{ Timer, TimerTask, Timeout }
import org.jboss.netty.bootstrap.ClientBootstrap
import akka.actor.ActorRef
import csc.akka.logging.DirectLogging
import csc.vehicle.jai._
import java.text.SimpleDateFormat
import java.util.{ TimeZone, Date }
import collection.mutable.ListBuffer
import scala.concurrent.duration._

/**
 * Interface for the MessageProcessor
 */
trait MessageProcessor {
  def processReceivedMessage(msg: Object, channel: ConnectionChannel)

  def processDisconnect(channel: ConnectionChannel)

  def processConnect(channel: ConnectionChannel)
}

/**
 * The possible states of a KS Connection.
 */
object ConnectionState extends Enumeration {
  val DISCONNECTED, CONNECT_ERROR, CONNECTED, SHUTDOWN = Value
}

/**
 * This class controls the message protocol of the PIR
 * //TODO 20120501 RR->RB: this looks like it is abstracted away from netty, but it still uses the netty bootstrap.
 * @param laneId
 * @param bootstrap
 * @param timer
 * @param timeout
 * @param recipients
 * @param channelGroup
 */
class JaiConnectionProcessor(laneId: String,
                             bootstrap: ClientBootstrap,
                             timer: Timer,
                             timeout: Long,
                             maxTriggerRetries: Int,
                             timeDisconnected: Long,
                             cameraTriggerId: BigInt,
                             recipients: Set[ActorRef],
                             channelGroup: ConnectionGroup)
  extends MessageProcessor with DirectLogging {
  val statusRequest = new JaiStatusMessage(Seq[Short](
    202, //ErrorStatus
    238, //NTP estimated Error
    239, //NTP status
    248 //NTP restart Counter
    ))
  val errorAck = new JaiErrorAck()
  var triggerMessage = new JaiTriggerMessage(CPU_Trigger = true, Repetitive = false, id = cameraTriggerId, triggerTime = 0)
  val dateFormat = new SimpleDateFormat("HH:mm:ss.SSS z")
  dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"))
  var timerTimeout: Timeout = null
  var connectionState = ConnectionState.DISCONNECTED
  var triggerFailures = 0
  var disconnectTime: Option[Long] = None
  var nrSendDisconnect = 0

  /**
   *  A message with the correct syntax is received and should be processed.
   * @param msg: the received message
   * @param channel: the channel where the message is received from
   */
  def processReceivedMessage(msg: Object, channel: ConnectionChannel) {

    msg match {
      case errorMsg: JaiErrorMessage ⇒ {
        //handle error
        log.debug("%s: JaiErrorMessage received: %s".format(laneId, errorMsg))
        processErrors(errorMsg)
        //send ack
        try {
          channel.write(errorAck)
        } catch {
          case ex: Exception ⇒ log.error("%s: Send error Ack failed: %s".format(laneId, ex.toString))
        }
      }
      case errorMsg: JaiStatusAck ⇒ {
        //handle Status result
        val errorList = processErrors(errorMsg)
        sendEvent(new CameraStatus(laneId, new Date(), errorList))
        //send ack
        try {
          channel.write(errorAck)
        } catch {
          case ex: Exception ⇒ log.error("%s: Send error Ack failed: %s".format(laneId, ex.toString))
        }
      }
      case dataReady: JaiDataReadyMessage ⇒ {
        log.debug("%s: DataReadyMessage received: %s".format(laneId, dataReady))
        channel.write(new JaiDataReadyAck())
        if (triggerMessage.id == dataReady.id) {
          //received data ready for our trigger request so get the image
          channel.write(new JaiSendDataMessage(id = dataReady.id, imageSeq = dataReady.imageSeq, dataType = dataReady.dataType))
        }
      }
      case triggerAck: JaiTriggerAck ⇒ {
        log.debug("%s: TriggerAck received: %s".format(laneId, triggerAck))
        //process trigger
        triggerFailures = 0
        processErrors(triggerAck.errorCodes)
        sendEvent(new CameraTriggerSuccess(laneId, new Date()))
      }
      case triggerError: JaiTriggerError ⇒ {
        log.debug("%s: TriggerError received: %s".format(laneId, triggerError))
        triggerFailures += 1
        if ((triggerError.errorCodes & 1) != 0) {
          if (triggerFailures <= maxTriggerRetries) {
            sendCameraTrigger()
          } else {
            log.error("%s: reached maximum Retries ".format(laneId))
            sendEvent(new CameraTriggerFailure(laneId, new Date(), new CameraError(DisturbanceTypes.CAMERA_TRIGGER_RETRIES, Some(triggerFailures.toString))))
            triggerFailures = 0
          }
        } else {
          log.error("%s: No System memory ?".format(laneId))
        }
        //process error
        processErrors(triggerError.errorCodes)
      }
      case sendAck: JaiSendDataAck ⇒ {
        log.debug("%s: JaiSendDataAck received: %s".format(laneId, JaiSendDataAck))
        //process trigger
        processErrors(sendAck.error)
        sendEvent(new CameraTriggerImage(laneId = laneId, image = sendAck.image))
      }
      case sendAck: JaiSendDataAckError ⇒ {
        log.debug("%s: JaiSendDataAckError received: %s".format(laneId, JaiSendDataAck))
        //process trigger
        val errors = processErrors(sendAck.error)
        sendEvent(new CameraTriggerImageFailure(laneId, errors))
      }

      case unknownMsg: AnyRef ⇒ log.warning("%s: Unexpected message %s received".format(laneId, unknownMsg.getClass.getName))
    }

  }

  def processErrors(errors: JaiStatusAck): Seq[CameraError] = {
    val errorList = new ListBuffer[CameraError]()

    for (error ← errors.statusItems) {
      error.code match {
        case StatusCode.ERROR_STATUS ⇒ {
          errorList ++= processErrors(error.value.toInt)
        }
        case StatusCode.NTP_ESTIMATE_ERROR ⇒ {
          if (error.value == 0xFFFFFFFF) {
            log.error("%s No NTP detected".format(laneId))
          } else {
            log.info("%s Estimated NTP error from ntp_gettime() in microseconds %d".format(laneId, error.value))
          }
        }
        case StatusCode.NTP_STATUS ⇒ {
          val bitMask = error.value
          if (error.value == 0x00000001) {
            log.info("%s: NTP_STATUS-NTP is running normal".format(laneId))
          } else {
            val details = new StringBuffer()
            if ((bitMask & 1) != 1) {
              log.error("%s: NTP_STATUS-STA_PLL: Disable PLL updates".format(laneId))
              details.append(" NTP_STATUS-STA_PLL")
            }
            if ((bitMask & 2) != 0) {
              log.error("%s: NTP_STATUS-STA_PPSFREQ is Set: Enable PPS freq discipline".format(laneId))
              details.append(" NTP_STATUS-STA_PPSFREQ")
            }
            if ((bitMask & 4) != 0) {
              log.error("%s: NTP_STATUS-STA_PPSTIME is Set: Enable PPS time discipline".format(laneId))
              details.append(" NTP_STATUS-STA_PPSTIME")
            }
            if ((bitMask & 8) != 0) {
              log.error("%s: NTP_STATUS-STA_FLL is Set: Select frequencylock mode".format(laneId))
              details.append(" NTP_STATUS-STA_FLL")
            }
            if ((bitMask & 16) != 0) {
              log.error("%s: NTP_STATUS-STA_INS is Set: Insert leap".format(laneId))
              details.append(" NTP_STATUS-STA_INS")
            }
            if ((bitMask & 32) != 0) {
              log.error("%s: NTP_STATUS-STA_DEL is Set: Delete leap".format(laneId))
              details.append(" NTP_STATUS-STA_DEL")
            }
            if ((bitMask & 64) != 0) {
              log.error("%s: NTP_STATUS-STA_UNSYNC is Set: Clock unsynchronized".format(laneId))
              details.append(" NTP_STATUS-STA_UNSYNC")
            }
            if ((bitMask & 128) != 0) {
              log.error("%s: NTP_STATUS-STA_FREQHOLD is Set: Hold frequency".format(laneId))
              details.append(" NTP_STATUS-STA_FREQHOLD")
            }
            if ((bitMask & 256) != 0) {
              log.error("%s: NTP_STATUS-STA_PPSSIGNAL is Set: PPS signal present.".format(laneId))
              details.append(" NTP_STATUS-STA_PPSSIGNAL")
            }
            if ((bitMask & 512) != 0) {
              log.error("%s: NTP_STATUS-STA_PPSJITTER is Set: PPS signal jitter exceeded.".format(laneId))
              details.append(" NTP_STATUS-STA_PPSJITTER")
            }
            if ((bitMask & 1024) != 0) {
              log.error("%s: NTP_STATUS-STA_PPSWANDER is Set: PPS signal wander exceeded.".format(laneId))
              details.append(" NTP_STATUS-STA_PPSWANDER")
            }
            if ((bitMask & 2048) != 0) {
              log.error("%s: NTP_STATUS-STA_PPSERROR is Set: PPS signal.".format(laneId))
              details.append(" NTP_STATUS-STA_PPSERROR")
            }
            if ((bitMask & 4294967296L) != 0) { //bit 31 
              log.error("%s: NTP_STATUS-NTP DISABLE is Set.".format(laneId))
              details.append(" NTP_STATUS-NTP DISABLE")
            }
            errorList += new CameraError(DisturbanceTypes.CAMERA_NTP_SYNC, Some(details.toString))
          }
        }
        case StatusCode.NTP_RESTART_COUNTER ⇒
          log.info("%s: NTP_RESTART_COUNTER-NTP restart counter=%d".format(laneId, error.value))

        case StatusCode.NTP_IP0 ⇒
          log.info("%s: NTP_IP0=%d.%d.%d.%d".format(laneId, (error.value >> 24) & 0xFF, (error.value >> 16) & 0xFF, (error.value >> 8) & 0xFF, error.value & 0xFF))

        case StatusCode.NTP_IP1 ⇒
          log.info("%s: NTP_IP1=%d.%d.%d.%d".format(laneId, (error.value >> 24) & 0xFF, (error.value >> 16) & 0xFF, (error.value >> 8) & 0xFF, error.value & 0xFF))

        case StatusCode.NTP_IP2 ⇒
          log.info("%s: NTP_IP2=%d.%d.%d.%d".format(laneId, (error.value >> 24) & 0xFF, (error.value >> 16) & 0xFF, (error.value >> 8) & 0xFF, error.value & 0xFF))

        case StatusCode.REAL_TIME_CLOCK ⇒
          log.info("%s: EN_REAL_TIME_CLOCK=%s".format(laneId, new Date(error.value * 1000).toString))

        case StatusCode.EN_IP ⇒
          log.info("%s: EN_IP=%d.%d.%d.%d".format(laneId, (error.value >> 24) & 0xFF, (error.value >> 16) & 0xFF, (error.value >> 8) & 0xFF, error.value & 0xFF))

        case StatusCode.EN_SUBNET ⇒
          log.info("%s: EN_SUBNET=%d.%d.%d.%d".format(laneId, (error.value >> 24) & 0xFF, (error.value >> 16) & 0xFF, (error.value >> 8) & 0xFF, error.value & 0xFF))

        case StatusCode.EN_GATEWAY ⇒
          log.info("%s: EN_GATEWAY=%d.%d.%d.%d".format(laneId, (error.value >> 24) & 0xFF, (error.value >> 16) & 0xFF, (error.value >> 8) & 0xFF, error.value & 0xFF))

        case _ ⇒
          log.info("%s: errorCode [%s] value=%d".format(laneId, error.code, error.value))
      }
    }
    errorList.toSeq
  }
  def processErrors(errors: Int): Seq[CameraError] = {
    val list = new ListBuffer[CameraError]()
    if (errors != 0) {
      log.error("%s: errors found %d".format(laneId, errors))
      if ((errors & 1) != 0)
        log.error("%s: System Memory: Image arrays could not be allocated".format(laneId))
      var comErrorDetail = ""
      if ((errors & 2) != 0) {
        log.error("%s: 232 Serial Communication Error: Communication was lost".format(laneId))
        comErrorDetail += "SERIAL232"
      }
      if ((errors & 4) != 0) {
        log.error("%s: IO board Serial Communication Error: Communication was lost".format(laneId))
        comErrorDetail += ", IO-BOARD"
      }
      if ((errors & 8) != 0) {
        log.error("%s: Camera Module Serial Comm. Error: Camera Module is not responding".format(laneId))
        comErrorDetail += ", MODULE"
      }
      //if one of the previous errors occurred, create CAMERA_COMM error
      if ((errors & 14) != 0) {
        list += new CameraError(DisturbanceTypes.CAMERA_COMM, Some(comErrorDetail))
      }
      if ((errors & 16) != 0) {
        log.error("%s: Light Sensor 1 Comm. Error: No LS data received in last 10 sec".format(laneId))
        list += new CameraError(DisturbanceTypes.CAMERA_IMAGE, Some("Mist comunicatie met Licht Sensor 1"))
      }
      if ((errors & 32) != 0) {
        log.error("%s: Light Sensor 2 Comm. Error: No LS data received in last 10 sec".format(laneId))
        list += new CameraError(DisturbanceTypes.CAMERA_IMAGE, Some("Mist comunicatie met Licht Sensor 2"))
      }
      if ((errors & 64) != 0) {
        log.error("%s: Housing Open (I): Housing switch changed state (1 = Error Set).".format(laneId))
        list += new CameraError(DisturbanceTypes.CAMERA_HOUSING)
      }
      if ((errors & 128) != 0)
        log.error("%s: Spare Input State (I): Spare input changed state (1 = Error Set).".format(laneId))
      if ((errors & 256) != 0) {
        log.error("%s: FTP server Comm.: Unable to open the FTP link. Check User name, password or rights.".format(laneId))
        list += new CameraError(DisturbanceTypes.CAMERA_FTP)
      }
      if ((errors & 512) != 0)
        log.error("%s: FTP destination down: Destination drive is full or cannot be written.".format(laneId))
      if ((errors & 1024) != 0) {
        log.error("%s: NTP server down: NTP server is disabled or estimated time error exceeded the max allowed in the config.".format(laneId))
        list += new CameraError(DisturbanceTypes.CAMERA_NTP_DOWN)
      }
      if ((errors & 2048) != 0)
        log.error("%s: Light Alarm State (I): Flash or Night Light not functioning.".format(laneId))
      if ((errors & 4096) != 0)
        log.error("%s: FTP2 server Comm.: Unable to open the FTP2 link. Check User name,\npassword or rights.".format(laneId))
      if ((errors & 8192) != 0)
        log.error("%s: FTP2 destination down: Destination drive is full or cannot be written.".format(laneId))
    }
    list.toSeq
  }

  def processErrors(error: JaiErrorMessage) {

    val state = error.state match {
      case ErrorStateMessage.CLEARED ⇒ DisturbanceState.SOLVED
      case ErrorStateMessage.PRESENT ⇒ DisturbanceState.DETECTED
      case _                         ⇒ return
    }
    val cameraError = error.errorCode match {
      case ErrorCodeMessage.SERIAL232 ⇒ {
        new CameraError(DisturbanceTypes.CAMERA_COMM, Some("SERIAL232"))
      }
      case ErrorCodeMessage.IO_BOARD ⇒ {
        new CameraError(DisturbanceTypes.CAMERA_COMM, Some("IO-BOARD"))
      }
      case ErrorCodeMessage.CAMERA_MODULE ⇒ {
        new CameraError(DisturbanceTypes.CAMERA_COMM, Some("MODULE"))
      }
      case ErrorCodeMessage.HOUSING_OPEN ⇒ {
        new CameraError(DisturbanceTypes.CAMERA_HOUSING)
      }
      case ErrorCodeMessage.FTP_COMM ⇒ {
        new CameraError(DisturbanceTypes.CAMERA_FTP, Some("COMMUNICATION"))
      }
      case ErrorCodeMessage.FTP_DOWN ⇒ {
        new CameraError(DisturbanceTypes.CAMERA_FTP, Some("DOWN"))
      }
      case ErrorCodeMessage.NTP_DOWN ⇒ {
        new CameraError(DisturbanceTypes.CAMERA_NTP_DOWN, Some("DOWN"))
      }
      case ErrorCodeMessage.FTP_FAIL ⇒ {
        new CameraError(DisturbanceTypes.CAMERA_FTP, Some("FAILED"))
      }
      case _ ⇒ {
        log.warning("%s: time=%s ignoring received errorCode=%s state=%s, because no influence on correctly working camara".format(laneId, dateFormat.format(error.timestamp), error.errorCode, error.state))
        return
      }
    }
    sendEvent(new DisturbanceEvent(laneId = laneId,
      timestamp = error.timestamp,
      eventType = cameraError,
      state = state))
    log.error("%s: time=%s errorCode=%s state=%s".format(laneId, dateFormat.format(error.timestamp), error.errorCode, error.state))
  }

  /**
   * A disconnect is received.
   */
  def processDisconnect(channel: ConnectionChannel) {
    log.info("Processing disconnect on {}. Connection state is {}. Disconnect time is {}", laneId, connectionState, disconnectTime)
    channelGroup.remove(channel)
    if (channelGroup.size > 0) {
      //disconnect of another channel
      log.warning(laneId + ": Disconnect received and channel list is not empty")
      return
    }
    if (connectionState != ConnectionState.SHUTDOWN) {
      val now = System.currentTimeMillis()
      if (connectionState != ConnectionState.CONNECT_ERROR) {
        connectionState = ConnectionState.DISCONNECTED
        disconnectTime match {
          case Some(time) ⇒ {
            //check time
            if (now - time >= timeDisconnected) {
              connectionState = ConnectionState.CONNECT_ERROR
              nrSendDisconnect = 1
              log.error("%s: Could not connect to %s".format(laneId, channel.getRemoteAddress()))
              sendEvent(new DisturbanceEvent(
                laneId = laneId,
                timestamp = new Date(now),
                state = DisturbanceState.DETECTED,
                eventType = new CameraError(DisturbanceTypes.CAMERA_CONNECTION)))
            }
          }
          case None ⇒ disconnectTime = Some(now)
        }
      } else {
        //still in connect error resend error each hour
        val sendTime = disconnectTime.getOrElse(now) + nrSendDisconnect * 1.hour.toMillis
        if (sendTime < now) {
          nrSendDisconnect += 1
          log.error("%s: Could still not connect to %s".format(laneId, channel.getRemoteAddress()))
          sendEvent(new DisturbanceEvent(
            laneId = laneId,
            timestamp = new Date(now),
            state = DisturbanceState.DETECTED,
            eventType = new CameraError(DisturbanceTypes.CAMERA_CONNECTION)))
        }
      }
      //reconnect now
      log.debug(laneId + ": Sleeping for %d".format(timeout))
      timerTimeout = timer.newTimeout(new TimerTask() {
        def run(timeout: Timeout) {
          if (timeout == timerTimeout) {
            log.info(laneId + ": Reconnecting to: " + channel.getRemoteAddress())
            bootstrap.connect()
          }
        }
      }, timeout, TimeUnit.MILLISECONDS);
    }
  }

  /**
   * Receive a connect event.
   */
  def processConnect(channel: ConnectionChannel) {
    if (channelGroup.size > 0) {
      log.warning(laneId + ": Connect received before a disconnect")
    }
    channelGroup.add(channel)
    if (connectionState == ConnectionState.CONNECT_ERROR) {
      sendEvent(new DisturbanceEvent(
        laneId = laneId,
        timestamp = new Date(),
        state = DisturbanceState.SOLVED,
        eventType = new CameraError(DisturbanceTypes.CAMERA_CONNECTION)))
    }
    connectionState = ConnectionState.CONNECTED
    disconnectTime = None
    try {
      channel.write(statusRequest)
    } catch {
      case ex: Exception ⇒ log.error("%s: Send status request failed: %s".format(laneId, ex.toString))
    }
  }

  /**
   * Receive a shutdown.
   * Cancel the outstanding timers, change the state to shutdown and close the current connection
   */
  def shutdown() {
    if (timerTimeout != null && !timerTimeout.isExpired) {
      timerTimeout.cancel
    }
    timerTimeout = null
    connectionState = ConnectionState.SHUTDOWN
    //close all connected channels, should be 0 or 1
    channelGroup.close
    log.info(laneId + ": Shutdown")
  }

  def sendEvent(msg: AnyRef) {
    for (actor ← recipients) {
      actor ! msg
    }
  }
  /**
   * Send a camera trigger
   */
  def sendCameraTrigger() {
    if (connectionState != ConnectionState.CONNECTED) {
      throw new IllegalStateException("%s: Try to send Camera Trigger when in state [%s]".format(laneId, connectionState))
    }
    if (channelGroup.size() <= 0) {
      //should not happen when in connected state a channel should be present
      throw new IllegalStateException("%s: no channels in channelGroup".format(laneId))
    }
    try {
      channelGroup.get(0).write(triggerMessage)
      log.info(laneId + ": Send trigger request %s and %s".format(
        if (triggerMessage.CPU_Trigger) "CPU" else "TTL",
        triggerMessage.id.toString(16)))
    } catch {
      case ex: Exception ⇒ {
        log.error("%s: Send trigger request failed: %s".format(laneId, ex.toString))
        throw ex
      }
    }
  }

  def sendStatus() {
    if (connectionState != ConnectionState.CONNECTED) {
      throw new IllegalStateException("%s: Try to send Status when in state [%s]".format(laneId, connectionState))
    }
    if (channelGroup.size() <= 0) {
      //should not happen when in connected state a channel should be present
      //racecondition?
      throw new IllegalStateException("%s: no channels in chanalGroup".format(laneId))
    }
    try {
      channelGroup.get(0).write(statusRequest)
      log.info(laneId + ": Send status")
    } catch {
      case ex: Exception ⇒ {
        log.error("%s: Send status request failed: %s".format(laneId, ex.toString))
        throw ex
      }
    }
  }
  def sendAllStatus() {
    if (connectionState != ConnectionState.CONNECTED) {
      throw new IllegalStateException("%s: Try to send Status when in state [%s]".format(laneId, connectionState))
    }
    if (channelGroup.size() <= 0) {
      //should not happen when in connected state a channel should be present
      throw new IllegalStateException("%s: no channels in chanalGroup".format(laneId))
    }
    try {
      channelGroup.get(0).write(new JaiStatusMessage(Seq()))
      log.info(laneId + ": Send all status")
    } catch {
      case ex: Exception ⇒ {
        log.error("%s: Send status request failed: %s".format(laneId, ex.toString))
        throw ex
      }
    }
  }

}