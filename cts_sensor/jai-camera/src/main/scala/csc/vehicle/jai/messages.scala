/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.jai

import java.text.SimpleDateFormat
import java.util.{ TimeZone, Date }

object ErrorStateMessage extends Enumeration {
  type ErrorStateMessage = Value
  val UNKNOWN = Value(0)
  val ONE_TIME = Value(1)
  val PRESENT = Value(2)
  val CLEARED = Value(3)
}
object ErrorCodeMessage extends Enumeration {
  type ErrorCodeMessage = Value
  val UNKNOWN = Value(0)
  val SYSTEM_MEMORY = Value(1) //Image arrays couldn't be allocated
  val SERIAL232 = Value(2)
  val IO_BOARD = Value(3)
  val CAMERA_MODULE = Value(4)
  val LIGHT_SENSOR_1 = Value(5)
  val LIGHT_SENSOR_2 = Value(6)
  val HOUSING_OPEN = Value(7)
  val SPARE_INPUT_STATE = Value(8)
  val FTP_COMM = Value(9)
  val FTP_DOWN = Value(10)
  val NTP_DOWN = Value(11)
  val LIGHT_ALARM_STATE = Value(12)
  val FTP2_COMM = Value(13)
  val FTP2_DOWN = Value(14)
  val IO_INPUT_CHANGED = Value(20)
  val RS485_OVFL = Value(22)
  val RS485_CRC = Value(23)
  val RS485_COMMAND = Value(24)
  val RS485_DATA = Value(25)
  val RS232_OVERFLOW = Value(26)
  val RS232_BUSY = Value(27)
  val FALSE_TRIGGER = Value(30)
  val SPURIOS_TRIGGER = Value(31)
  val IMAGE_QUEUE = Value(32)
  val BAD_DATA_ID = Value(33)
  val TRIGGER_MISSING = Value(34)
  val TRIGGER_ASSOCIATION = Value(35)
  val CAMERA_BUSY = Value(36)
  val JPEG_FAILED = Value(37)
  val USER_DYNAMIC_MISSING = Value(38)
  val COMMAND = Value(40)

  val FTP_FAIL = Value(41)
  val IRQ = Value(42)
  val SYSTEM_EXE = Value(43)
  val FTP_FAIL_RECOVERED = Value(44)
}

object StatusCode {
  val ERROR_STATUS = 202 //ErrorStatus
  val NTP_ESTIMATE_ERROR = 238 //NTP estimated Error
  val NTP_STATUS = 239 //NTP status
  val NTP_RESTART_COUNTER = 248 //NTP restart Counter

  val NTP_IP0 = 61
  val NTP_IP1 = 62
  val NTP_IP2 = 63
  val REAL_TIME_CLOCK = 102
  val EN_IP = 104
  val EN_SUBNET = 105
  val EN_GATEWAY = 106
}

case class JaiErrorMessage(timestamp: Date, errorCode: ErrorCodeMessage.Value, state: ErrorStateMessage.Value)
case class JaiErrorAck()

case class JaiStatusMessage(statusCodes: Seq[Short])
case class JaiStatusAck(statusItems: Seq[StatusItem])
case class StatusItem(code: Short, value: Long)

case class JaiTriggerMessage(CPU_Trigger: Boolean, Repetitive: Boolean, id: BigInt, triggerTime: Int)
case class JaiTriggerAck(id: BigInt, timestamp: Date, errorCodes: Short) {
  override def toString(): String = {
    val dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS z")
    dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"))
    val buffer = new StringBuffer()
    buffer.append("JaiTriggerAck(")
    buffer.append(id)
    buffer.append(",")
    buffer.append(dateFormat.format(timestamp))
    buffer.append(",")
    buffer.append(errorCodes)
    buffer.append(")")
    buffer.toString()
  }
}
case class JaiTriggerError(errorCodes: Short)

case class JaiDataReadyMessage(id: BigInt, imageSeq: Short, dataType: Short, timestamp: Date)
case class JaiDataReadyAck()

case class JaiSendDataMessage(id: BigInt, imageSeq: Short, dataType: Short)
case class JaiSendDataAck(id: BigInt, error: Short, imageSeq: Short, dataType: Short, acquireTime: Date, present: Boolean, auxWidth: Int, auxHeight: Int, auxData: Array[Byte], packageData: Array[Byte], image: Array[Byte])
case class JaiSendDataAckError(id: BigInt, error: Short, imageSeq: Short, dataType: Short)

case class NoReply()