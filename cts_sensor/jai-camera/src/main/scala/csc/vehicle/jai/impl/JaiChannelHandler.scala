/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.jai.impl

import org.jboss.netty.channel._
import java.net.{ SocketAddress, ConnectException }
import csc.akka.logging.DirectLogging

/**
 * This is the netty interface handle for the jai connection. All the network events are caucht in this class
 * @param msgProcessor The processor which will process the different events
 * @param remoteHost The host where this connection is connected to
 */
class JaiChannelHandler(msgProcessor: MessageProcessor, remoteHost: SocketAddress) extends SimpleChannelUpstreamHandler with DirectLogging {
  private var startTime = -1L

  /**
   * Get the configured remote address.
   */
  def getRemoteAddress(channel: Channel): String = {
    if (channel != null && channel.getRemoteAddress != null) {
      channel.getRemoteAddress.toString
    } else {
      remoteHost.toString + " (Not connected yet)"
    }
  }

  /**
   * A disconnect event is received.
   * @param ctx: channel context
   * @param e: the channel event
   */
  override def channelDisconnected(ctx: ChannelHandlerContext, e: ChannelStateEvent) {
    log.info("Disconnected from: " + getRemoteAddress(e.getChannel));
  }

  /**
   * A connection close event is received
   * @param ctx: channel context
   * @param e: the channel event
   */
  override def channelClosed(ctx: ChannelHandlerContext, e: ChannelStateEvent) {
    log.info("Connection closed from: " + getRemoteAddress(e.getChannel));
    log.info("Uptime was %d msec".format(System.currentTimeMillis() - startTime))
    startTime = -1L
    msgProcessor.processDisconnect(new NettyConnectionChannel(e.getChannel))
  }

  /**
   * A connected event is received
   * @param ctx: channel context
   * @param e: the channel event
   */
  override def channelConnected(ctx: ChannelHandlerContext, e: ChannelStateEvent) {
    if (startTime < 0) {
      startTime = System.currentTimeMillis();
    }
    log.info("Connected to: " + getRemoteAddress(e.getChannel));
    msgProcessor.processConnect(new NettyConnectionChannel(e.getChannel))
  }

  /**
   * A Exception event is received
   * @param ctx: channel context
   * @param e: the exception event
   */
  override def exceptionCaught(ctx: ChannelHandlerContext, e: ExceptionEvent) {
    val cause = e.getCause();
    cause match {
      case con: ConnectException ⇒ {
        log.error(con, "Failed to connect: " + con.getMessage());
      }
      case ex: Exception ⇒ {
        log.error(ex, "Channel exception: " + ex.getMessage());
      }
    }
    startTime = -1;
    ctx.getChannel().close();
  }

  /**
   * A Message is received from Jai camera
   */
  override def messageReceived(ctx: ChannelHandlerContext, e: MessageEvent) {
    log.debug("Message received from: " + getRemoteAddress(e.getChannel));
    val receivedMsg = e.getMessage
    if (receivedMsg != null) {
      log.debug("Message received: " + receivedMsg.toString);
    }
    msgProcessor.processReceivedMessage(receivedMsg, new NettyConnectionChannel(e.getChannel))
  }

}