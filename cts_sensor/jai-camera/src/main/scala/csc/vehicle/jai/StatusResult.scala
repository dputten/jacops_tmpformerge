/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.jai

import java.util.Date

/**
 * Used with DisturbanceEvent to indicate what state
 * the disturbance is.
 */
object DisturbanceState extends Enumeration {
  type State = Value
  val DETECTED = Value(0)
  val SOLVED = Value(1)
}

object DisturbanceTypes {

  val solvedStringMapping: List[(String, String)] = Map("ERROR" -> "SOLVED", "OPEN" -> "CLOSED").toList

  val CAMERA_CONNECTION = "JAI-CAMERA-CONNECTION-ERROR"
  val CAMERA_NTP_DOWN = "JAI-CAMERA-NTP-DOWN-ERROR"
  val CAMERA_NTP_SYNC = "JAI-CAMERA-NTP_SYNC-ERROR"
  val CAMERA_FTP = "JAI-CAMERA-FTP-ERROR"
  val CAMERA_COMM = "JAI-CAMERA-COMMUNICATION-ERROR"
  val CAMERA_HOUSING = "JAI-CAMERA-HOUSING-OPEN"
  val CAMERA_TRIGGER_RETRIES = "JAI-CAMERA-TRIGGER-RETRIES"
  val INTERFACE_ERROR = "JAI-INTERFACE-ERROR"
  val CAMERA_IMAGE = "JAI-CAMERA-LIGHTSENSOR"

  def solvedType(source: String): String = {

    //tried to find a replaceable token
    def convert(list: List[(String, String)]): Option[String] = list match {
      case Nil ⇒ None
      case head :: tail ⇒
        if (source.contains(head._1)) Some(source.replace(head._1, head._2))
        else convert(tail)
    }

    convert(solvedStringMapping).getOrElse(source)
  }

}

trait CameraMessage

case class CameraError(eventType: String, details: Option[String] = None)

case class CameraStatus(laneId: String, time: Date, errors: Seq[CameraError]) extends CameraMessage

case class CameraTriggerSuccess(laneId: String, imageCreationTime: Date) extends CameraMessage
case class CameraTriggerFailure(laneId: String, time: Date, error: CameraError) extends CameraMessage
case class CameraTriggerImage(laneId: String, image: Array[Byte]) extends CameraMessage
case class CameraTriggerImageFailure(laneId: String, errors: Seq[CameraError]) extends CameraMessage

/*
 * Used to signal an event from Jai camera.
 * @param laneId
 * @param timestamp
 * @param eventType
 * @param state
 * @param reason
 */
case class DisturbanceEvent(laneId: String,
                            timestamp: Date,
                            eventType: CameraError,
                            state: DisturbanceState.Value,
                            reason: String = "")
