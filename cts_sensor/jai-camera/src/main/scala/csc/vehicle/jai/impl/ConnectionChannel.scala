/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.vehicle.jai.impl

import org.jboss.netty.channel.Channel
import org.jboss.netty.channel.group.DefaultChannelGroup
//TODO 20120501 RR->RB: remove these abstractions, they are leaky, leave this for later though.->flitscloud
//TODO 20120501 RR->RB: Hey where are the docs?
trait ConnectionGroup {
  def close()

  def add(channel: ConnectionChannel)

  def remove(channel: ConnectionChannel)

  def size(): Int

  def get(pos: Int): ConnectionChannel
}
//TODO 20120501 RR->RB: remove these abstractions, they are leaky, leave this for later though.->flitscloud
//TODO 20120501 RR->RB: add docs
trait ConnectionChannel {
  def close()

  def disconnect()

  def write(p1: Object)

  def getRemoteAddress(): String
}
//TODO 20120501 RR->RB: add docs and this should be a case class.
class NettyConnectionChannel(channel: Channel) extends ConnectionChannel {
  //TODO 20120501 RR->RB: use require to check for null on channel here. no need to check later.

  def close() {
    if (channel != null) {
      channel.close
    }
  }

  def disconnect() {
    if (channel != null) {
      channel.disconnect
    }
  }

  def write(msg: Object) {
    if (channel != null) {
      channel.write(msg)
    }
  }
  //TODO 20120501 RR->RB: use option
  def getRemoteAddress(): String = {
    if (channel != null && channel.getRemoteAddress != null) {
      channel.getRemoteAddress.toString
    } else {
      "(Not connected yet)"
    }
  }

  def getChannel(): Channel = {
    channel
  }
}

//TODO 20120501 RR->RB: move netty specifics to netty package.
class NettyConnectionGroup() extends ConnectionGroup {

  val group = new DefaultChannelGroup()

  def close() {
    group.close
  }

  def add(channel: ConnectionChannel) {
    //TODO 20120501 RR->RB: use channel match { c:NettyConnectionChannel and _ => throw exception
    if (channel.isInstanceOf[NettyConnectionChannel]) {
      val ch = channel.asInstanceOf[NettyConnectionChannel].getChannel
      if (ch != null) {
        group.add(ch)
      }
    } else {
      throw new IllegalArgumentException("Wrong channel instance")
    }
  }

  def remove(channel: ConnectionChannel) {
    //TODO 20120501 RR->RB: use channel match { c:NettyConnectionChannel and _ => throw exception
    //TODO 20120501 RR->RB:  strange though, parameter is ConnectionChannel but it is only ok if it os NettyConnectionChannel?
    //TODO 20120501 RR->RB: that breaks liskov.
    if (channel.isInstanceOf[NettyConnectionChannel]) {
      val ch = channel.asInstanceOf[NettyConnectionChannel].getChannel
      if (ch != null) {
        group.remove(ch)
      }
    } else {
      throw new IllegalArgumentException("Wrong channel instance")
    }
  }

  def size(): Int = {
    group.size
  }

  def get(pos: Int): ConnectionChannel = {
    //TODO 20120501 RR->RB: DefaultChannelGroup is always of type Channel, it's a subclass of   AbstractSet<Channel>
    new NettyConnectionChannel(group.toArray()(pos).asInstanceOf[Channel])
  }
}
