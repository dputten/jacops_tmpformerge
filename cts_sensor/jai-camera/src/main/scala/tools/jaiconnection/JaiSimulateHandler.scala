/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package tools.jailogger.pirconnection

import java.util.concurrent.TimeUnit

import akka.util.Timeout

import scala.concurrent.duration.{ FiniteDuration, Duration }
import org.jboss.netty.channel._
import group.DefaultChannelGroup
import java.net.ConnectException
import csc.akka.logging.DirectLogging
import akka.actor.ActorRef
import akka.pattern.ask
import scala.concurrent.Await
import csc.vehicle.jai.NoReply

/**
 * This is the Jai Simulate handler. And is used as connection handler when simulating a JaiCamera
 * @param processorActor The simulate processor.
 */
class JaiSimulateHandler(processorActor: ActorRef) extends SimpleChannelUpstreamHandler with DirectLogging {
  private var startTime = -1L
  private val group = new DefaultChannelGroup()

  /**
   * Get the configured remote address.
   */
  def getRemoteAddress(channel: Channel): String = {
    if (channel != null && channel.getRemoteAddress != null) {
      channel.getRemoteAddress.toString
    } else {
      "Not connected yet"
    }
  }

  /**
   * A disconnect event is received.
   * @param ctx: channel context
   * @param e: the channel event
   */
  override def channelDisconnected(ctx: ChannelHandlerContext, e: ChannelStateEvent) {
    log.info("Disconnected from: " + getRemoteAddress(e.getChannel))
    val ch = e.getChannel
    if (ch != null) {
      group.remove(ch)
    }
  }

  /**
   * A connection close event is received
   * @param ctx: channel context
   * @param e: the channel event
   */
  override def channelClosed(ctx: ChannelHandlerContext, e: ChannelStateEvent) {
    log.info("Connection closed from: " + getRemoteAddress(e.getChannel));
    log.info("Uptime was %d s".format(System.currentTimeMillis() - startTime))
    startTime = -1L
  }

  /**
   * A connected event is received
   * @param ctx: channel context
   * @param e: the channel event
   */
  override def channelConnected(ctx: ChannelHandlerContext, e: ChannelStateEvent) {
    if (startTime < 0) {
      startTime = System.currentTimeMillis();
    }
    val ch = e.getChannel
    if (ch != null) {
      group.add(ch)
    }
    log.info("Connected to: " + getRemoteAddress(e.getChannel));
  }

  /**
   * A Exception event is received
   * @param ctx: channel context
   * @param e: the exception event
   */
  override def exceptionCaught(ctx: ChannelHandlerContext, e: ExceptionEvent) {
    val cause = e.getCause();
    cause match {
      case con: ConnectException ⇒ {
        log.error(con, "Failed to connect: " + con.getMessage());
      }
      case ex: Exception ⇒ {
        log.error(ex, "Channel exception: " + ex.getMessage());
      }
    }
    startTime = -1;
    ctx.getChannel().close();
  }

  private val askDuration = FiniteDuration(5, TimeUnit.SECONDS)
  private val askTimeout = Timeout(askDuration)

  /**
   * A Message is received
   */
  override def messageReceived(ctx: ChannelHandlerContext, e: MessageEvent) {
    log.info("Message received from: {}", getRemoteAddress(e.getChannel));
    val receivedMsg = e.getMessage
    if (receivedMsg != null) {
      log.info("Message received: {}", receivedMsg.toString)
    }
    try {
      val future = processorActor.ask(receivedMsg)(askTimeout)
      val response = Await.result(future, askDuration)
      if (!response.isInstanceOf[NoReply]) {
        e.getChannel.write(response)
      }

    } catch {
      case ex: Exception ⇒ log.warning("couldn't get response: {}", ex.toString)
    }
  }
  def close() {
    group.close()
  }
  /**
   * Send a message
   */
  def sendMessage(msg: AnyRef) {
    if (group.size() <= 0) {
      throw new IllegalStateException("No connections available to send message")
    }
    val ch = group.toArray()(0).asInstanceOf[Channel]
    log.info("Message Send to: {}", getRemoteAddress(ch));
    ch.write(msg)
  }
}