/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package tools.jailogger.pirconnection

import java.net.InetSocketAddress
import java.util.concurrent.Executors
import akka.actor.ActorRef
import csc.akka.logging.DirectLogging
import org.jboss.netty.channel.{ Channel, ChannelPipelineFactory, ChannelPipeline, Channels }
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory
import org.jboss.netty.bootstrap.ServerBootstrap
import csc.vehicle.jai.impl.{ JaiDecoder, JaiEncoder }
import csc.vehicle.jai.{ JaiDataReadyMessage, JaiErrorMessage }

/**
 * A class to bootstrap a simulate connection
 */
class JaiSimulateConnection(host: String, port: Int, processorActor: ActorRef) extends DirectLogging {
  private val bootstrap = new ServerBootstrap(
    new NioServerSocketChannelFactory(
      Executors.newCachedThreadPool(),
      Executors.newCachedThreadPool()))
  private var channel: Option[Channel] = None
  private val channelHandler = new JaiSimulateHandler(processorActor)
  init()

  /**
   * Initialize the connection by setting all the handlers.
   */
  private def init() {
    // Configure the connection.

    val decoder = new JaiDecoder
    val encoder = new JaiEncoder

    bootstrap.setPipelineFactory(new ChannelPipelineFactory {
      def getPipeline(): ChannelPipeline = {
        val pipeline = Channels.pipeline();
        pipeline.addLast("decoder", decoder);
        pipeline.addLast("encoder", encoder);
        pipeline.addLast("handler", channelHandler);
        pipeline;
      }
    })
  }

  /**
   * start listener
   */
  def start() {
    channel match {
      case Some(ch) ⇒ throw new IllegalStateException("BaseSimulatorConnection already started")
      case None ⇒ {
        channel = Some(bootstrap.bind(new InetSocketAddress(host, port)))
      }
    }
  }

  /**
   * Stop the connection and release all the resources , like sockets and threads.
   *
   * F
   */
  def shutdown() {
    channelHandler.close()
    try {
      channel.map {
        ch ⇒
          {
            ch.close
            channel = None
          }
      }
    } catch {
      case ex: Exception ⇒ log.error("Exception while closing channel: %s", ex.getMessage)
    }
    try {
      // Shutdown thread pools to exit.
      bootstrap.releaseExternalResources()
    } catch {
      case ex: Exception ⇒ log.error("Exception while shutting down bootstrap: %s", ex.getMessage)
    }
  }
  def sendError(error: JaiErrorMessage) {
    channelHandler.sendMessage(error)
  }
  def sendDataReady(ready: JaiDataReadyMessage) {
    channelHandler.sendMessage(ready)
  }
}