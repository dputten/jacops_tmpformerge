package tools.jaiconnection

import java.io.File
import java.util.Date

import csc.vehicle.jai.ErrorCodeMessage.ErrorCodeMessage
import csc.vehicle.jai.ErrorStateMessage.ErrorStateMessage
import csc.vehicle.jai.{ ErrorStateMessage, ErrorCodeMessage, JaiErrorMessage, StatusCode }
import org.apache.commons.io.FileUtils

/**
 * Created by carlos on 28/04/16.
 */
class JaiCameraSimulator(val sim: SimulateJai) {

  val cameraMessage = """image (.+)""".r
  val errorMessage = """ERROR (\S+) (\S+)""".r

  def process(cmd: String): Unit = cmd match {
    case cameraMessage(img)        ⇒ sendImage(img)
    case errorMessage(code, state) ⇒ sendError(code, state)
    case "reset"                   ⇒ resetErrors()
    case "NTP"                     ⇒ setNTPError()
    case other                     ⇒ println("Unknown command: %s".format(other))
  }

  def parseErrorCode(code: String): ErrorCodeMessage = try {
    ErrorCodeMessage(code.toInt)
  } catch {
    case ex: NumberFormatException ⇒ ErrorCodeMessage.withName(code)
  }

  def parseErrorState(state: String): ErrorStateMessage = state.toLowerCase match {
    case "on"  ⇒ ErrorStateMessage.PRESENT
    case "off" ⇒ ErrorStateMessage.CLEARED
    case _     ⇒ throw new IllegalArgumentException("Error state " + state)
  }

  def sendError(code: String, state: String): Unit =
    sim.sendError(JaiErrorMessage(new Date(), parseErrorCode(code), parseErrorState(state)))

  def resetErrors() {
    sim.updateStatus(StatusCode.ERROR_STATUS, 0L)
    sim.updateStatus(StatusCode.NTP_ESTIMATE_ERROR, 0L)
    sim.updateStatus(StatusCode.NTP_RESTART_COUNTER, 0L)
    sim.updateStatus(StatusCode.NTP_STATUS, 1L)
  }

  def setNTPError() {
    sim.updateStatus(StatusCode.NTP_STATUS, 65L) //NTP SYNC error
  }

  def sendImage(fileName: String) {
    val file = new File(fileName)
    if (file.exists() && file.isFile) {
      try {
        val bytes = FileUtils.readFileToByteArray(file)
        sim.setImage(bytes, true)
      } catch {
        case ex: Exception ⇒ println("Unable to read File %s".format(file.getAbsolutePath))
      }
    } else {
      println("File %s doesn't exists".format(file.getAbsolutePath))
    }
  }

}
