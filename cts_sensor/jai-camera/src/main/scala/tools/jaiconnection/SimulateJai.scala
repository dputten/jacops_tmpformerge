/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package tools.jaiconnection

import java.util.Date
import akka.actor.{ Props, ActorSystem, ActorLogging, Actor }
import csc.vehicle.jai._
import _root_.tools.jaiconnection.SimulateJai.ImageSupplier
import collection.mutable.HashMap
import tools.jailogger.pirconnection.JaiSimulateConnection

case class SetImage(image: Array[Byte], reuseImage: Boolean = false)

/**
 * Simulate a Jai Camera
 */
class SimulateJai(host: String,
                  port: Int,
                  systemOverride: Option[ActorSystem] = None,
                  imageSupplier: Option[ImageSupplier] = None) {

  private val system = systemOverride getOrElse ActorSystem("simulate")
  private val actor = system.actorOf(Props(new SimulateJaiActor(host, port, imageSupplier)))

  def stopJaiCamera() {
    if (systemOverride.isEmpty) system.shutdown() //shutdown system if internally created system
  }

  def sendError(error: JaiErrorMessage) {
    actor ! error
  }

  /**
   * Update status
   *
   * @param code the code of the error
   * @param value the value of the error
   */
  def updateStatus(code: Int, value: Long) {
    actor ! new StatusItem(code.toShort, value)
  }

  /**
   * set the image which can be used for the next trigger even
   *
   * @param image the image
   */
  def setImage(image: Array[Byte]) {
    setImage(image, false)
  }
  /**
   * set the image which can be used for the next trigger even
   *
   * @param image the image
   */
  def setImage(image: Array[Byte], reuseImage: Boolean) {
    actor ! new SetImage(image, reuseImage)
  }

}

object SimulateJai {
  type ImageSupplier = () ⇒ Option[Array[Byte]]
}

/**
 * This actor will coordinate the responses of the Jai Camera simulator
 * You can pass an imageSupplier that will produce an image when requested.
 */
class SimulateJaiActor(host: String, port: Int, imageSupplier: Option[ImageSupplier] = None) extends Actor with ActorLogging {
  private var errorCodes: Short = 0
  private val statusItems = new HashMap[Short, StatusItem]
  private var triggeredImage: Option[Array[Byte]] = None
  private val jaiConnection = new JaiSimulateConnection(host, port, self)
  private var reuseImage = false

  private def discardImage = imageSupplier.isDefined || !reuseImage

  override def preStart() {
    jaiConnection.start()
    context.system.eventStream.publish(CameraStarted(host, port))
  }

  override def postStop() {
    context.system.eventStream.publish(CameraStopped(host, port))
    jaiConnection.shutdown()
  }

  def receive = {
    case trigger: JaiErrorAck ⇒ {
      //response from sendError
      context.sender ! new NoReply()
    }
    case trigger: JaiTriggerMessage ⇒ {
      context.sender ! new JaiTriggerAck(trigger.id, timestamp = new Date(), errorCodes = errorCodes)
      jaiConnection.sendDataReady(new JaiDataReadyMessage(trigger.id, 1, 1, new Date()))
    }
    case sendData: JaiSendDataMessage ⇒ {
      val imageOpt: Option[Array[Byte]] = imageSupplier match {
        case Some(func) ⇒ func() //if there is a supplier, use it
        case None       ⇒ triggeredImage //otherwise use the image set before
      }

      imageOpt match {
        case Some(image) ⇒ {
          context.sender ! new JaiSendDataAck(sendData.id,
            error = 0,
            imageSeq = sendData.imageSeq,
            dataType = sendData.dataType,
            acquireTime = new Date(),
            present = true,
            auxWidth = 30,
            auxHeight = 30,
            auxData = "AuxData".getBytes(),
            packageData = new Array[Byte](0),
            image = image)
          if (discardImage) {
            triggeredImage = None
          }
        }
        case None ⇒ {
          context.sender ! new JaiSendDataAckError(sendData.id,
            error = 1,
            imageSeq = sendData.imageSeq,
            dataType = sendData.dataType)
        }
      }
    }
    case status: JaiStatusMessage ⇒ {
      val items = status.statusCodes.flatMap(code ⇒ statusItems.get(code))
      context.sender ! new JaiStatusAck(items)
    }
    case status: StatusItem ⇒ {
      statusItems += status.code -> status
      if (status.code == StatusCode.ERROR_STATUS) {
        errorCodes = status.value.toShort //first part are the same errors
      }
    }
    case err: JaiErrorMessage ⇒ {
      jaiConnection.sendError(err)
    }
    case image: SetImage ⇒ {
      triggeredImage = Some(image.image)
      reuseImage = image.reuseImage
    }
    case msg: JaiDataReadyAck ⇒ {
      context.sender ! new NoReply()
    }
    case any: AnyRef ⇒ {
      log.warning("Unexpected request. Ignore message: " + any.toString())
      context.sender ! new NoReply()
    }
  }
}

case class CameraStarted(host: String, port: Int)
case class CameraStopped(host: String, port: Int)
