package tools

import akka.kernel.Bootable
import csc.akka.logging.DirectLogging
import java.util.Date
import csc.sitebuffer.startup.{ ShutdownHook }
import csc.sitebuffer.config.SiteBufferConfiguration
import csc.sitebuffer.calibration.{ CalibrationCreateTextResult, CalibrationConsumer }
import csc.sitebuffer.process.send.{ TextlineTcpSender, BufferedTextMessageSender }
import akka.actor.{ ActorRef, Props, ActorSystem }
import java.io.File
import com.typesafe.config.ConfigFactory

class Boot extends Bootable with DirectLogging {

  val actorSystem = ActorSystem("SiteBufferSimulator")

  def startup = {

    log.info("Starting SiteBuffer Simulator")
    val simulatorConfig = ConfigFactory.load()
    val sitebufferConfig = SiteBufferConfiguration()

    val sender = createSender(sitebufferConfig)
    val simulator = createSimulator(simulatorConfig.getString("simulator.pathToResponseFile"), sender)
    val receiver = createReceiver(sitebufferConfig, Set(simulator))
  }

  def createSimulator(responseFile: String, resultSender: ActorRef): ActorRef = {
    actorSystem.actorOf(Props(
      new SimulatorActor(
        responseFile = new File(responseFile),
        resultSender = resultSender)), "SimulatorActor")
  }

  def createReceiver(config: SiteBufferConfiguration, recipients: Set[ActorRef]): ActorRef = {
    actorSystem.actorOf(Props(
      new CalibrationConsumer(
        calibrationEndpointHost = config.calibration.consumerHost,
        calibrationEndpointPort = config.calibration.consumerPort,
        recipients = recipients)), "CalibrateConsumer")
  }

  def createSender(config: SiteBufferConfiguration): ActorRef = {
    val calibrationTextOverTcpSender = actorSystem.actorOf(Props(
      new BufferedTextMessageSender(config.calibration.msgRetryDelayMs) with TextlineTcpSender {
        val host = config.calibration.centralHost
        val port = config.calibration.centralport
      }),
      "CalibrationMessageSender")

    actorSystem.actorOf(Props(
      new CalibrationCreateTextResult(config.calibration.maxMessageLength, Set(calibrationTextOverTcpSender))), "CalibrationCreateText")
  }

  def shutdown = {
    log.info("Shutdown SiteBuffer Simulator")
    (new ShutdownHook(actorSystem)) run ()
  }
}