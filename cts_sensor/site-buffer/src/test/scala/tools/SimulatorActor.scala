package tools

import akka.actor.{ ActorRef, ActorLogging, Actor }
import java.io.File
import net.liftweb.json.{ Serialization, DefaultFormats }
import org.apache.commons.io.FileUtils
import csc.sitebuffer.calibration._
import csc.sitebuffer.common.Signing

object SimulatorActor {

  case class SimulatorCalibrationImage(name: String, imageId: String, path: String)
  case class SimulatorResponse(
    systemId: Option[String],
    gantryId: Option[String],
    time: Option[Long],
    reportingOfficerCode: Option[String],
    serialNr: Option[String],
    activeCertificates: Seq[ComponentCertificate],
    images: Seq[SimulatorCalibrationImage],
    results: Seq[StepResult])

  def defaultSteps: Seq[StepResult] = {
    Seq(
      StepResult(stepName = StepResult.STEP_Certificates, success = true, detail = None),
      StepResult(stepName = StepResult.STEP_Loops, success = true, detail = None),
      StepResult(stepName = StepResult.STEP_VehicleRecords, success = true, detail = None),
      StepResult(stepName = StepResult.STEP_Camera, success = true, detail = None),
      StepResult(stepName = StepResult.STEP_NTP, success = true, detail = None))
  }

  def getImages(images: Seq[SimulatorCalibrationImage]): List[CalibrationImage] = {
    images.foldLeft(List[CalibrationImage]()) {
      case (acc, image) ⇒
        val data = FileUtils.readFileToByteArray(new File(image.path)).toArray

        CalibrationImage(
          name = image.name,
          imageId = image.imageId,
          checksum = Signing.digest(data = data, algorithm = "MD5"),
          image = data.map(_.toInt)) :: acc
    }
  }

  def getResponseMessage(responseFile: File, request: GantryCalibrationRequest): GantryCalibrationResponse = {
    implicit val formats = DefaultFormats
    val data = FileUtils.readFileToString(responseFile)
    val msg = Serialization.read[SimulatorResponse](data)

    GantryCalibrationResponse(
      systemId = msg.systemId getOrElse request.systemId,
      gantryId = msg.gantryId getOrElse request.gantryId,
      time = msg.time getOrElse request.time,
      reportingOfficerCode = msg.reportingOfficerCode getOrElse request.reportingOfficerCode,
      serialNr = msg.serialNr getOrElse request.serialNr.serialNumber,
      activeCertificates = if (msg.activeCertificates.isEmpty) request.mustCertificates else msg.activeCertificates,
      images = getImages(msg.images),
      results = if (msg.results.isEmpty) defaultSteps else msg.results)
  }
}

class SimulatorActor(responseFile: File, resultSender: ActorRef) extends Actor with ActorLogging {
  import SimulatorActor._

  override def preStart() {
    log.info("path to reponse file [%s]".format(responseFile.getAbsolutePath))
  }

  def receive = {
    case req: GantryCalibrationRequest ⇒
      log.info("Received Request: " + req)

      val msg = getResponseMessage(responseFile, req)
      log.info("Response with [%s]".format(msg))

      resultSender ! msg
  }
}
