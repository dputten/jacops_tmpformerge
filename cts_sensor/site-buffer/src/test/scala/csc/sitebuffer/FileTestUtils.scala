/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer

import java.awt.{ Color, Dimension }
import java.io.{ File, FileInputStream, FileOutputStream }
import javax.imageio.ImageIO

import org.apache.commons.io.FileUtils

/**
 * Object containing some file related helper methods
 */
object FileTestUtils {

  def clearDirectory(dir: File) {
    dir.listFiles.foreach { f ⇒ deleteFile(f) }
  }

  def deleteDirectory(dir: File) = {
    deleteFile(dir)
  }

  def deleteFile(file: File): Unit = {
    if (file.isDirectory)
      file.listFiles.foreach { f ⇒ deleteFile(f) }
    file.delete
  }

  def copyFile(fromFile: File, toFile: File): File = {
    new FileOutputStream(toFile).getChannel().transferFrom(new FileInputStream(fromFile).getChannel(), 0, Long.MaxValue)
    toFile
  }

  def copyFile(filename: String, fromDir: File, toDir: File): File = {
    copyFile(new File(fromDir, filename), new File(toDir, filename))
  }

  def getImageDimension(file: File): Option[Dimension] = {
    ImageIO.createImageInputStream(file) match {
      case null ⇒ None
      case inputStream ⇒ {
        val readers = ImageIO.getImageReaders(inputStream)
        try {
          val reader = readers.next
          reader.setInput(inputStream, true, true)
          try Some(new Dimension(reader.getWidth(0), reader.getHeight(0)))
          catch {
            case _: Exception ⇒ None
          } finally reader.dispose
        } catch {
          case _: Exception ⇒ None
        } finally inputStream.close
      }
    }
  }

  def filesAreEqual(testImageFile: File, refImageFile: File): Boolean = {
    val testFile = FileUtils.readFileToByteArray(testImageFile)
    val refFile = FileUtils.readFileToByteArray(refImageFile)
    testFile == refFile
  }

  /**
   * Calculates the root-mean-square deviation of two files. This is aan indication of how much these files are alike.
   * Will return 0.0 if files ate the same.
   * @param file1 First file
   * @param file2 Second file
   * @return root-mean-square deviation
   */
  def rmsdFiles(file1: File, file2: File): Double = {
    val testFile = FileUtils.readFileToByteArray(file1)
    val refFile = FileUtils.readFileToByteArray(file2)

    val n = testFile.length - 1
    var sum = 0d
    for (i ← 0 to n) {
      val diff = testFile(i) - refFile(i)
      sum += (diff * diff)
    }
    math.sqrt(sum / n)
  }

  /**
   * Calculates the root-mean-square deviation of two images. This is aan indication of how much these images are alike.
   * Will return 0.0 if files ate the same.
   * @param image1 First image
   * @param image2 Second image
   * @return root-mean-square deviation
   */
  def rmsdImages(image1: File, image2: File): Double = {
    val bufferedImage1 = ImageIO.read(image1)
    val bufferedImage2 = ImageIO.read(image2)
    val width1 = bufferedImage1.getWidth
    val height1 = bufferedImage1.getHeight
    val width2 = bufferedImage2.getWidth
    val height2 = bufferedImage2.getHeight
    require((width1 == width2) && (height1 == height2), "Image sizes must be equal")

    val dataBuffer1 = bufferedImage1.getRGB(0, 0, width1, height1, null, 0, width1)
    val dataBuffer2 = bufferedImage2.getRGB(0, 0, width2, height2, null, 0, width2)

    val n = dataBuffer1.length - 1
    var sum = 0d
    for (i ← 0 to n) {
      val color1 = new Color(dataBuffer1(i));
      val color2 = new Color(dataBuffer2(i));

      val diffRed = color1.getRed - color2.getRed
      val diffGreen = color1.getGreen - color2.getGreen
      val diffBlue = color1.getBlue - color2.getBlue
      sum += (diffRed * diffRed)
      sum += (diffGreen * diffGreen)
      sum += (diffBlue * diffBlue)
    }
    math.sqrt(sum / (3 * n))

  }

}

