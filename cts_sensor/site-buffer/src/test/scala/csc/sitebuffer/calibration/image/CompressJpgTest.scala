/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.calibration.image

import org.scalatest.WordSpec
import org.scalatest._
import testframe.Resources

class CompressJpgTest extends WordSpec with MustMatchers {
  "compressJpg" must {
    "read and compres a jpg" in {
      val file = Resources.getResourceFile("csc/sitebuffer/calibration/image/calibrationImage.jpg")
      val output = CompressJpg.readAndCompress(file, 35)
      output.size < file.length() must be(true)
      //      val test = new File(file.getParentFile, "out.jpg")
      //      println("wrote testfile %s".format(test.getAbsolutePath))
      //      FileUtils.writeByteArrayToFile(test,output)
    }
  }

}