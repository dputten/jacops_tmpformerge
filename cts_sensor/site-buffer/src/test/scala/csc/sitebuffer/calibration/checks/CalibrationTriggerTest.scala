/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.calibration.checks

import java.util.Date

import csc.sitebuffer.calibration._
import csc.sitebuffer.calibration.ssh.{ CommandResult, SSHCommandMock }
import csc.sitebuffer.messages.{ Direction, VehicleMessage }
import org.scalatest.WordSpec
import org.scalatest._

class CalibrationTriggerTest extends WordSpec with MustMatchers {

  "retriggerLoopsCheck" must {
    "Don't trigger when all detectors have responded" in {
      val now = System.currentTimeMillis()
      val commands = new SSHCommandMock(now) {
        var called = Seq[Int]()
        override def calibrateLane(detectorId: Int): CommandResult[Boolean] = {
          called = called :+ detectorId
          CommandResult(List(), true)
        }
      }
      val detectors = Seq(0, 1)
      val responses = Seq(
        createResponse(0, now),
        createResponse(1, now))
      CalibrationTrigger.retriggerLoopsCheck(commands, detectors, responses, "host", 23, "usr", "pad", 3000)
      commands.called.size must be(0)
    }
    "trigger when one detector has responded" in {
      val now = System.currentTimeMillis()
      val commands = new SSHCommandMock(now) {
        var called = Seq[Int]()
        override def calibrateLane(detectorId: Int): CommandResult[Boolean] = {
          called = called :+ detectorId
          CommandResult(List(), true)
        }
      }
      val detectors = Seq(0, 1)
      val responses = Seq(
        createResponse(0, now))
      CalibrationTrigger.retriggerLoopsCheck(commands, detectors, responses, "host", 23, "usr", "pad", 3000)
      commands.called.size must be(1)
      commands.called.head must be(1)
    }
    "trigger when no detector has responded" in {
      val now = System.currentTimeMillis()
      val commands = new SSHCommandMock(now) {
        var called = Seq[Int]()
        override def calibrateLane(detectorId: Int): CommandResult[Boolean] = {
          called = called :+ detectorId
          CommandResult(List(), true)
        }
      }
      val detectors = Seq(0, 1)
      val responses = Seq()
      CalibrationTrigger.retriggerLoopsCheck(commands, detectors, responses, "host", 23, "usr", "pad", 3000)
      commands.called.size must be(2)
      commands.called.contains(0) must be(true)
      commands.called.contains(1) must be(true)
    }
    "doen't fail when no detector is pressent" in {
      val now = System.currentTimeMillis()
      val commands = new SSHCommandMock(now) {
        var called = Seq[Int]()
        override def calibrateLane(detectorId: Int): CommandResult[Boolean] = {
          called = called :+ detectorId
          CommandResult(List(), true)
        }
      }
      CalibrationTrigger.retriggerLoopsCheck(commands, Seq(), Seq(), "host", 23, "usr", "pad", 3000)
      commands.called.size must be(0)
      val responses = Seq(createResponse(0, now))
      CalibrationTrigger.retriggerLoopsCheck(commands, Seq(), responses, "host", 23, "usr", "pad", 3000)
      commands.called.size must be(0)
    }
  }

  def createResponse(detector: Int, now: Long): GetRegistrationResponse = {
    GetRegistrationResponse(refId = detector.toString, detector = detector,
      registration = new VehicleMessage(dateTime = now,
        detector = detector, direction = Direction.Outgoing,
        speed = 325, length = 4.5F, loop1RiseTime = 0, loop1FallTime = 200,
        loop2RiseTime = 300, loop2FallTime = 400, yellowTime2 = 0, redTime2 = 0,
        dateTime1 = now - 200, yellowTime1 = 0, redTime1 = 0, dateTime3 = now + 400,
        yellowTime3 = 0, redTime3 = 0, valid = false,
        serialNr = "SN123", offset1 = 0, offset2 = 0, offset3 = 0))
  }

  def createState(now: Long): CalibrationState = {
    CalibrationState(request = GantryCalibrationRequest(
      systemId = "3213", gantryId = "1", time = now,
      reportingOfficerCode = "XX1234",
      mustCertificates = Seq(ComponentCertificate("test", "1234")),
      serialNr = SerialNumber("", "PCD", 5)), timeReceived = new Date(now))
  }

}