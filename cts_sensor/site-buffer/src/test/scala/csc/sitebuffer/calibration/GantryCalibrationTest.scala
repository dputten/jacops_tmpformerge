/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.calibration

import akka.actor.{ ActorSystem, Props }
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import scala.concurrent.duration._
import csc.akka.logging.DirectLogging
import csc.sitebuffer.calibration.ssh.{ CommandResult, SSHCommandMock, TimeDifference }
import csc.sitebuffer.config.{ CalibrationConfig, CameraConfig }
import csc.sitebuffer.messages.{ Direction, VehicleMessage }
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }

class GantryCalibrationTest extends TestKit(ActorSystem("GantryCalibrationTest")) with WordSpecLike with MustMatchers
  with DirectLogging with BeforeAndAfterEach with BeforeAndAfterAll {
  override def afterAll() {
    try {
      system.shutdown
    } catch {
      case e: Exception ⇒ log.error("COULD NOT SHUTDOWN ACTOR REGISTRY".format(e))
    }
  }

  "GantryCalibration" must {
    "process happy flow" in {
      val imageTap = TestProbe()
      val vehicleTap = TestProbe()
      val cameraDif = TestProbe()
      val resultSender = TestProbe()
      val now = System.currentTimeMillis()
      val commands = new SSHCommandMock(now)

      val calibration = TestActorRef(Props(
        new GantryCalibration(calibrationCfg = createCalibrationCfg,
          cameraConfigs = createCamerasCfg(),
          imageTap = imageTap.ref,
          registrationTap = vehicleTap.ref,
          cameraTimeDiff = cameraDif.ref,
          commands = commands,
          resultSender = resultSender.ref)))
      val req = GantryCalibrationRequest("3213", "1", now, "XX1234", Seq(ComponentCertificate("Dummy", "1234")), SerialNumber("", "PCB", 5))
      calibration ! req
      //check expected msgs
      imageTap.expectMsg(StoreImageRequest(refId = "CA0", cameraDir = "A0", targetDir = "/data/calibration"))
      imageTap.expectMsg(StoreImageRequest(refId = "CA1", cameraDir = "A1", targetDir = "/data/calibration"))
      vehicleTap.expectMsg(GetRegistrationRequest(refId = "0", detector = 0))
      vehicleTap.expectMsg(GetRegistrationRequest(refId = "1", detector = 1))
      vehicleTap.expectMsg(GetRegistrationRequest(refId = "2", detector = 2))
      cameraDif.expectMsg(CameraTimeDifferenceRequest(refId = "A0", host = "1", port = 23, user = "root", pwd = "PWD"))
      cameraDif.expectMsg(CameraTimeDifferenceRequest(refId = "A1", host = "2", port = 23, user = "root", pwd = "PWD"))
      //send responses
      calibration ! StoreImageResponse(refId = "A0", cameraDir = "A0", targetDir = "/data/calibration", fileName = "image0.jpg")
      calibration ! StoreImageResponse(refId = "A1", cameraDir = "A1", targetDir = "/data/calibration", fileName = "image1.jpg")
      calibration ! GetRegistrationResponse(refId = "0", detector = 0, registration = createVehicleMessage(0, now))
      calibration ! GetRegistrationResponse(refId = "1", detector = 1, registration = createVehicleMessage(0, now))
      calibration ! GetRegistrationResponse(refId = "2", detector = 2, registration = createVehicleMessage(0, now))
      calibration ! CameraTimeDifferenceResponse(refId = "A0", host = "1", port = 23, succeed = true, timeDif = TimeDifference(now, 0), errors = List())
      resultSender.expectNoMsg()
      calibration ! CameraTimeDifferenceResponse(refId = "A1", host = "2", port = 23, succeed = true, timeDif = TimeDifference(now, 0), errors = List())
      val response = resultSender.expectMsgType[GantryCalibrationResponse](1.seconds)

      response.systemId must be(req.systemId)
      response.gantryId must be(req.gantryId)
      response.time must be(req.time)
      response.reportingOfficerCode must be(req.reportingOfficerCode)
      response.serialNr must be("PCB01234")
      response.activeCertificates must be(req.mustCertificates)
      val stepResults = Seq(
        StepResult(StepResult.STEP_Camera, false, Some("Missing physical calibration images for [A0, A1]")),
        StepResult(StepResult.STEP_Certificates, true, None),
        StepResult(StepResult.STEP_Loops, true, None),
        StepResult(StepResult.STEP_NTP, true, None),
        StepResult(StepResult.STEP_VehicleRecords, true, None)).sortBy(_.stepName)
      response.results.sortBy(_.stepName) must be(stepResults)
      response.images must be(Seq())
    }
    "process happy flow with double request receied" in {
      val imageTap = TestProbe()
      val vehicleTap = TestProbe()
      val cameraDif = TestProbe()
      val resultSender = TestProbe()
      val now = System.currentTimeMillis()
      val commands = new SSHCommandMock(now)

      val calibration = TestActorRef(Props(
        new GantryCalibration(calibrationCfg = createCalibrationCfg,
          cameraConfigs = createCamerasCfg(),
          imageTap = imageTap.ref,
          registrationTap = vehicleTap.ref,
          cameraTimeDiff = cameraDif.ref,
          commands = commands,
          resultSender = resultSender.ref)))
      val req = GantryCalibrationRequest("3213", "1", now, "XX1234", Seq(ComponentCertificate("Dummy", "1234")), SerialNumber("", "PCB", 5))
      calibration ! req
      //check expected msgs
      imageTap.expectMsg(StoreImageRequest(refId = "CA0", cameraDir = "A0", targetDir = "/data/calibration"))
      imageTap.expectMsg(StoreImageRequest(refId = "CA1", cameraDir = "A1", targetDir = "/data/calibration"))
      vehicleTap.expectMsg(GetRegistrationRequest(refId = "0", detector = 0))
      vehicleTap.expectMsg(GetRegistrationRequest(refId = "1", detector = 1))
      vehicleTap.expectMsg(GetRegistrationRequest(refId = "2", detector = 2))
      cameraDif.expectMsg(CameraTimeDifferenceRequest(refId = "A0", host = "1", port = 23, user = "root", pwd = "PWD"))
      cameraDif.expectMsg(CameraTimeDifferenceRequest(refId = "A1", host = "2", port = 23, user = "root", pwd = "PWD"))
      //send responses
      calibration ! StoreImageResponse(refId = "A0", cameraDir = "A0", targetDir = "/data/calibration", fileName = "image0.jpg")
      calibration ! StoreImageResponse(refId = "A1", cameraDir = "A1", targetDir = "/data/calibration", fileName = "image1.jpg")
      calibration ! GetRegistrationResponse(refId = "0", detector = 0, registration = createVehicleMessage(0, now))
      calibration ! GetRegistrationResponse(refId = "1", detector = 1, registration = createVehicleMessage(0, now))
      calibration ! GetRegistrationResponse(refId = "2", detector = 2, registration = createVehicleMessage(0, now))
      calibration ! CameraTimeDifferenceResponse(refId = "A0", host = "1", port = 23, succeed = true, timeDif = TimeDifference(now, 0), errors = List())
      //Send the request double. should not have any inpact on result
      calibration ! req

      resultSender.expectNoMsg()
      calibration ! CameraTimeDifferenceResponse(refId = "A1", host = "2", port = 23, succeed = true, timeDif = TimeDifference(now, 0), errors = List())
      val response = resultSender.expectMsgType[GantryCalibrationResponse](1.seconds)

      response.systemId must be(req.systemId)
      response.gantryId must be(req.gantryId)
      response.time must be(req.time)
      response.reportingOfficerCode must be(req.reportingOfficerCode)
      response.serialNr must be("PCB01234")
      response.activeCertificates must be(req.mustCertificates)
      val stepResults = Seq(
        StepResult(StepResult.STEP_Camera, false, Some("Missing physical calibration images for [A0, A1]")),
        StepResult(StepResult.STEP_Certificates, true, None),
        StepResult(StepResult.STEP_Loops, true, None),
        StepResult(StepResult.STEP_NTP, true, None),
        StepResult(StepResult.STEP_VehicleRecords, true, None)).sortBy(_.stepName)
      response.results.sortBy(_.stepName) must be(stepResults)
      response.images must be(Seq())
    }
    "process when reciveing second request" in {
      val imageTap = TestProbe()
      val vehicleTap = TestProbe()
      val cameraDif = TestProbe()
      val resultSender = TestProbe()
      val now = System.currentTimeMillis()
      val commands = new SSHCommandMock(now)

      val calibration = TestActorRef(Props(
        new GantryCalibration(calibrationCfg = createCalibrationCfg,
          cameraConfigs = createCamerasCfg(),
          imageTap = imageTap.ref,
          registrationTap = vehicleTap.ref,
          cameraTimeDiff = cameraDif.ref,
          commands = commands,
          resultSender = resultSender.ref)))
      val req = GantryCalibrationRequest("3213", "1", now, "XX1234", Seq(ComponentCertificate("Dummy", "1234")), SerialNumber("", "PCB", 5))
      calibration ! req
      //check expected msgs
      imageTap.expectMsg(StoreImageRequest(refId = "CA0", cameraDir = "A0", targetDir = "/data/calibration"))
      imageTap.expectMsg(StoreImageRequest(refId = "CA1", cameraDir = "A1", targetDir = "/data/calibration"))
      vehicleTap.expectMsg(GetRegistrationRequest(refId = "0", detector = 0))
      vehicleTap.expectMsg(GetRegistrationRequest(refId = "1", detector = 1))
      vehicleTap.expectMsg(GetRegistrationRequest(refId = "2", detector = 2))
      cameraDif.expectMsg(CameraTimeDifferenceRequest(refId = "A0", host = "1", port = 23, user = "root", pwd = "PWD"))
      cameraDif.expectMsg(CameraTimeDifferenceRequest(refId = "A1", host = "2", port = 23, user = "root", pwd = "PWD"))

      calibration ! GantryCalibrationRequest("3213", "1", now + 1, "XX1234", Seq(ComponentCertificate("Dummy", "1234")), SerialNumber("", "PCB", 5))
      //expect response when geting a new calibration request
      val response = resultSender.expectMsgType[GantryCalibrationResponse](1.seconds)
      response.systemId must be(req.systemId)
      response.gantryId must be(req.gantryId)
      response.time must be(req.time)
      response.reportingOfficerCode must be(req.reportingOfficerCode)
      response.serialNr must be("")
      response.activeCertificates must be(req.mustCertificates)
      val stepResults = Seq(
        StepResult(StepResult.STEP_Certificates, true, None),
        StepResult(StepResult.STEP_Loops, true, None),
        StepResult(StepResult.STEP_Camera, false, Some("Missing calibration images: [A0, A1]")),
        StepResult(StepResult.STEP_NTP, false, Some("Missing time difference camera: [A0, A1]")),
        StepResult(StepResult.STEP_VehicleRecords, false, Some("Missing vehicle registrations for detectors: [0, 1, 2]"))).sortBy(_.stepName)
      response.results.sortBy(_.stepName) must be(stepResults)
      response.images must be(Seq())
    }
    "process Timeout with Camera Difference" in {
      val imageTap = TestProbe()
      val vehicleTap = TestProbe()
      val cameraDif = TestProbe()
      val resultSender = TestProbe()
      val now = System.currentTimeMillis()
      val commands = new SSHCommandMock(now)

      val calibration = TestActorRef(Props(
        new GantryCalibration(calibrationCfg = createCalibrationCfg,
          cameraConfigs = createCamerasCfg(),
          imageTap = imageTap.ref,
          registrationTap = vehicleTap.ref,
          cameraTimeDiff = cameraDif.ref,
          commands = commands,
          resultSender = resultSender.ref)))
      val req = GantryCalibrationRequest("3213", "1", now, "XX1234", Seq(ComponentCertificate("Dummy", "1234")), SerialNumber("", "PCB", 5))
      calibration ! req
      //check expected msgs
      imageTap.expectMsg(StoreImageRequest(refId = "CA0", cameraDir = "A0", targetDir = "/data/calibration"))
      imageTap.expectMsg(StoreImageRequest(refId = "CA1", cameraDir = "A1", targetDir = "/data/calibration"))
      vehicleTap.expectMsg(GetRegistrationRequest(refId = "0", detector = 0))
      vehicleTap.expectMsg(GetRegistrationRequest(refId = "1", detector = 1))
      vehicleTap.expectMsg(GetRegistrationRequest(refId = "2", detector = 2))
      cameraDif.expectMsg(CameraTimeDifferenceRequest(refId = "A0", host = "1", port = 23, user = "root", pwd = "PWD"))
      cameraDif.expectMsg(CameraTimeDifferenceRequest(refId = "A1", host = "2", port = 23, user = "root", pwd = "PWD"))
      //send responses
      calibration ! StoreImageResponse(refId = "A0", cameraDir = "A0", targetDir = "/data/calibration", fileName = "image0.jpg")
      calibration ! StoreImageResponse(refId = "A1", cameraDir = "A1", targetDir = "/data/calibration", fileName = "image1.jpg")
      calibration ! GetRegistrationResponse(refId = "0", detector = 0, registration = createVehicleMessage(0, now))
      calibration ! GetRegistrationResponse(refId = "1", detector = 1, registration = createVehicleMessage(0, now))
      calibration ! GetRegistrationResponse(refId = "2", detector = 2, registration = createVehicleMessage(0, now))
      calibration ! CameraTimeDifferenceResponse(refId = "A0", host = "1", port = 23, succeed = true, timeDif = TimeDifference(now, 0), errors = List())
      resultSender.expectNoMsg()
      calibration ! CalibrationTimeout
      cameraDif.expectMsg(CameraTimeDifferenceRequest(refId = "A1", host = "2", port = 23, user = "root", pwd = "PWD"))
      calibration ! CameraTimeDifferenceResponse(refId = "A1", host = "2", port = 23, succeed = true, timeDif = TimeDifference(now, 0), errors = List())
      val response = resultSender.expectMsgType[GantryCalibrationResponse](1.seconds)

      response.systemId must be(req.systemId)
      response.gantryId must be(req.gantryId)
      response.time must be(req.time)
      response.reportingOfficerCode must be(req.reportingOfficerCode)
      response.serialNr must be("PCB01234")
      response.activeCertificates must be(req.mustCertificates)
      val stepResults = Seq(
        StepResult(StepResult.STEP_Camera, false, Some("Missing physical calibration images for [A0, A1]")),
        StepResult(StepResult.STEP_Certificates, true, None),
        StepResult(StepResult.STEP_Loops, true, None),
        StepResult(StepResult.STEP_NTP, true, None),
        StepResult(StepResult.STEP_VehicleRecords, true, None)).sortBy(_.stepName)
      response.results.sortBy(_.stepName) must be(stepResults)
      response.images must be(Seq())
    }
    "process Timeout with failed Camera Difference" in {
      val imageTap = TestProbe()
      val vehicleTap = TestProbe()
      val cameraDif = TestProbe()
      val resultSender = TestProbe()
      val now = System.currentTimeMillis()
      val commands = new SSHCommandMock(now)

      val calibration = TestActorRef(Props(
        new GantryCalibration(calibrationCfg = createCalibrationCfg,
          cameraConfigs = createCamerasCfg(),
          imageTap = imageTap.ref,
          registrationTap = vehicleTap.ref,
          cameraTimeDiff = cameraDif.ref,
          commands = commands,
          resultSender = resultSender.ref)))
      val req = GantryCalibrationRequest("3213", "1", now, "XX1234", Seq(ComponentCertificate("Dummy", "1234")), SerialNumber("", "PCB", 5))
      calibration ! req
      //check expected msgs
      imageTap.expectMsg(StoreImageRequest(refId = "CA0", cameraDir = "A0", targetDir = "/data/calibration"))
      imageTap.expectMsg(StoreImageRequest(refId = "CA1", cameraDir = "A1", targetDir = "/data/calibration"))
      vehicleTap.expectMsg(GetRegistrationRequest(refId = "0", detector = 0))
      vehicleTap.expectMsg(GetRegistrationRequest(refId = "1", detector = 1))
      vehicleTap.expectMsg(GetRegistrationRequest(refId = "2", detector = 2))
      cameraDif.expectMsg(CameraTimeDifferenceRequest(refId = "A0", host = "1", port = 23, user = "root", pwd = "PWD"))
      cameraDif.expectMsg(CameraTimeDifferenceRequest(refId = "A1", host = "2", port = 23, user = "root", pwd = "PWD"))
      //send responses
      calibration ! StoreImageResponse(refId = "A0", cameraDir = "A0", targetDir = "/data/calibration", fileName = "image0.jpg")
      calibration ! StoreImageResponse(refId = "A1", cameraDir = "A1", targetDir = "/data/calibration", fileName = "image1.jpg")
      calibration ! GetRegistrationResponse(refId = "0", detector = 0, registration = createVehicleMessage(0, now))
      calibration ! GetRegistrationResponse(refId = "1", detector = 1, registration = createVehicleMessage(0, now))
      calibration ! GetRegistrationResponse(refId = "2", detector = 2, registration = createVehicleMessage(0, now))
      calibration ! CameraTimeDifferenceResponse(refId = "A0", host = "1", port = 23, succeed = true, timeDif = TimeDifference(now, 0), errors = List())
      calibration ! CameraTimeDifferenceResponse(refId = "A1", host = "2", port = 23, succeed = false, timeDif = TimeDifference(now, 0), errors = List("connect error"))
      resultSender.expectNoMsg()
      calibration ! CalibrationTimeout
      cameraDif.expectMsg(CameraTimeDifferenceRequest(refId = "A1", host = "2", port = 23, user = "root", pwd = "PWD"))
      calibration ! CameraTimeDifferenceResponse(refId = "A1", host = "2", port = 23, succeed = true, timeDif = TimeDifference(now, 0), errors = List())
      val response = resultSender.expectMsgType[GantryCalibrationResponse](1.seconds)

      response.systemId must be(req.systemId)
      response.gantryId must be(req.gantryId)
      response.time must be(req.time)
      response.reportingOfficerCode must be(req.reportingOfficerCode)
      response.serialNr must be("PCB01234")
      response.activeCertificates must be(req.mustCertificates)
      val stepResults = Seq(
        StepResult(StepResult.STEP_Camera, false, Some("Missing physical calibration images for [A0, A1]")),
        StepResult(StepResult.STEP_Certificates, true, None),
        StepResult(StepResult.STEP_Loops, true, None),
        StepResult(StepResult.STEP_NTP, true, None),
        StepResult(StepResult.STEP_VehicleRecords, true, None)).sortBy(_.stepName)
      response.results.sortBy(_.stepName) must be(stepResults)
      response.images must be(Seq())
    }
    "process Timeout with failed vehicleRegistration" in {
      val imageTap = TestProbe()
      val vehicleTap = TestProbe()
      val cameraDif = TestProbe()
      val resultSender = TestProbe()
      val now = System.currentTimeMillis()
      val commands = new SSHCommandMock(now) {
        var called = Seq[Int]()
        override def calibrateLane(detectorId: Int): CommandResult[Boolean] = {
          called = called :+ detectorId
          CommandResult(List(), true)
        }

      }

      val calibration = TestActorRef(Props(
        new GantryCalibration(calibrationCfg = createCalibrationCfg,
          cameraConfigs = createCamerasCfg(),
          imageTap = imageTap.ref,
          registrationTap = vehicleTap.ref,
          cameraTimeDiff = cameraDif.ref,
          commands = commands,
          resultSender = resultSender.ref)))
      val req = GantryCalibrationRequest("3213", "1", now, "XX1234", Seq(ComponentCertificate("Dummy", "1234")), SerialNumber("", "PCB", 5))
      calibration ! req
      //check expected msgs
      imageTap.expectMsg(StoreImageRequest(refId = "CA0", cameraDir = "A0", targetDir = "/data/calibration"))
      imageTap.expectMsg(StoreImageRequest(refId = "CA1", cameraDir = "A1", targetDir = "/data/calibration"))
      vehicleTap.expectMsg(GetRegistrationRequest(refId = "0", detector = 0))
      vehicleTap.expectMsg(GetRegistrationRequest(refId = "1", detector = 1))
      vehicleTap.expectMsg(GetRegistrationRequest(refId = "2", detector = 2))
      cameraDif.expectMsg(CameraTimeDifferenceRequest(refId = "A0", host = "1", port = 23, user = "root", pwd = "PWD"))
      cameraDif.expectMsg(CameraTimeDifferenceRequest(refId = "A1", host = "2", port = 23, user = "root", pwd = "PWD"))
      commands.called must be(Seq(0, 1, 2))
      //send responses
      calibration ! StoreImageResponse(refId = "A0", cameraDir = "A0", targetDir = "/data/calibration", fileName = "image0.jpg")
      calibration ! StoreImageResponse(refId = "A1", cameraDir = "A1", targetDir = "/data/calibration", fileName = "image1.jpg")
      calibration ! GetRegistrationResponse(refId = "0", detector = 0, registration = createVehicleMessage(0, now))
      calibration ! GetRegistrationResponse(refId = "2", detector = 2, registration = createVehicleMessage(0, now))
      calibration ! CameraTimeDifferenceResponse(refId = "A0", host = "1", port = 23, succeed = true, timeDif = TimeDifference(now, 0), errors = List())
      calibration ! CameraTimeDifferenceResponse(refId = "A1", host = "2", port = 23, succeed = true, timeDif = TimeDifference(now, 0), errors = List())
      resultSender.expectNoMsg()
      commands.called = Seq()
      calibration ! CalibrationTimeout
      commands.called must be(Seq(1))
      calibration ! GetRegistrationResponse(refId = "1", detector = 1, registration = createVehicleMessage(0, now))
      val response = resultSender.expectMsgType[GantryCalibrationResponse](1.seconds)

      response.systemId must be(req.systemId)
      response.gantryId must be(req.gantryId)
      response.time must be(req.time)
      response.reportingOfficerCode must be(req.reportingOfficerCode)
      response.serialNr must be("PCB01234")
      response.activeCertificates must be(req.mustCertificates)
      val stepResults = Seq(
        StepResult(StepResult.STEP_Camera, false, Some("Missing physical calibration images for [A0, A1]")),
        StepResult(StepResult.STEP_Certificates, true, None),
        StepResult(StepResult.STEP_Loops, true, None),
        StepResult(StepResult.STEP_NTP, true, None),
        StepResult(StepResult.STEP_VehicleRecords, true, None)).sortBy(_.stepName)
      response.results.sortBy(_.stepName) must be(stepResults)
      response.images must be(Seq())
    }

  }

  def createCalibrationCfg(): CalibrationConfig = {
    CalibrationConfig(telnetUser = "root", telnetPwd = "PWD", telnetPort = 23,
      sshHost = "1.2.3.4", sshPort = 22, sshUser = "root", sshPwd = "pwd", sshTimeout = 30000,
      NTPmaxDiff_ms = 10,
      ftpRootDir = "/data",
      calibrationImageDir = "calibration",
      speedThreshold = 300,
      centralHost = "localhost",
      centralport = 12300,
      msgRetryDelayMs = 20000,
      consumerHost = "127.0.0.1",
      consumerPort = 12350,
      retriggerTime = 20.seconds,
      maxProcessTime = 5.minutes,
      maxMessageLength = 1000,
      jpgQuality = 100)
  }
  def createVehicleMessage(detector: Int, now: Long): VehicleMessage = {
    new VehicleMessage(dateTime = now,
      detector = detector, direction = Direction.Outgoing,
      speed = 325, length = 4.5F, loop1RiseTime = 0, loop1FallTime = 200,
      loop2RiseTime = 300, loop2FallTime = 400, yellowTime2 = 0, redTime2 = 0,
      dateTime1 = now - 200, yellowTime1 = 0, redTime1 = 0, dateTime3 = now + 400,
      yellowTime3 = 0, redTime3 = 0, valid = false,
      serialNr = "1234", offset1 = 0, offset2 = 0, offset3 = 0)
  }

  def createCamerasCfg(): Map[String, CameraConfig] = {
    Map("CA0" ->
      CameraConfig(cameraId = "A0", cameraHost = "1", cameraPort = 0,
        maxTriggerRetries = 0, timeDisconnectedMs = 0,
        rebootScript = "", detectors = Seq(0, 1), imageDir = "A0",
        maxWaitForImage = 0L, maxWaitForReboot = 0L, waitForFixedMsg = 0L),
      "CA1" ->
        CameraConfig(cameraId = "A1", cameraHost = "2", cameraPort = 0,
          maxTriggerRetries = 0, timeDisconnectedMs = 0,
          rebootScript = "", detectors = Seq(2), imageDir = "A1",
          maxWaitForImage = 0L, maxWaitForReboot = 0L, waitForFixedMsg = 0L))

  }

}