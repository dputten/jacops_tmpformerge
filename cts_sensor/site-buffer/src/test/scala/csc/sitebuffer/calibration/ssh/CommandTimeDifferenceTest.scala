/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.calibration.ssh

import org.scalatest.WordSpec
import org.scalatest._

class CommandTimeDifferenceTest extends WordSpec with MustMatchers {
  "parseRawRecords" must {
    "parse correct lines" in {
      val lines = List("""Thu May 28 14:19:27 UTC 2015: -0.000003 (mode -j)""",
        """Thu May 28 14:19:57 UTC 2015: -0.000652 (mode -j)""",
        """Thu May 28 14:20:27 UTC 2015: -0.229 (mode -j)""",
        """Thu May 28 14:20:57 UTC 2015:  0.219 (mode -j)""",
        """Thu May 28 14:21:27 UTC 2015: -1.235 (mode -j)""",
        """Thu May 28 14:21:57 UTC 2015: 2.00240 (mode -j)""")
      val result = CommandTimeDifference.parseRawRecords(lines)

      result.size must be(6)
      result(0) must be(Right(TimeDifference(1432822767000L, 0L)))
      result(1) must be(Right(TimeDifference(1432822797000L, -1L)))
      result(2) must be(Right(TimeDifference(1432822827000L, -229L)))
      result(3) must be(Right(TimeDifference(1432822857000L, 219L)))
      result(4) must be(Right(TimeDifference(1432822887000L, -1235L)))
      result(5) must be(Right(TimeDifference(1432822917000L, 2002L)))
    }
    "parse mixed lines" in {
      val lines = List("""Thu May 28 14:19:27 UTC 2015: -0.000003 (mode -j)""",
        """Thu May 28 14:19:57 UTC 2015: -0.000652 (mode -j)""",
        """Thu May 28 14:20:27 UTC 2015: -0.229 mode -j)""",
        """Thu May 28 14:20:57 UTC 2015:  0.219 (mode -j""",
        """Thu May 28 14:21:27 UTC 2015: error (mode -j)""",
        """May 28 14:21:57 UTC 2015: 2.00240 (mode -j)""")
      val result = CommandTimeDifference.parseRawRecords(lines)
      result.size must be(6)
      result(0) must be(Right(TimeDifference(1432822767000L, 0L)))
      result(1) must be(Right(TimeDifference(1432822797000L, -1L)))
      result(2) must be(Left(lines(2)))
      result(3) must be(Left(lines(3)))
      result(4) must be(Left(lines(4)))
      result(5).isLeft must be(true)
    }
  }

}