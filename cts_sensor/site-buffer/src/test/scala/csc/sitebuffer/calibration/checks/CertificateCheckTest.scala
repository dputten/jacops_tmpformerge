/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.calibration.checks

import java.util.Date

import csc.sitebuffer.calibration.{ CalibrationState, ComponentCertificate, GantryCalibrationRequest, SerialNumber, _ }
import csc.sitebuffer.calibration.ssh.{ CommandResult, SSHCommandMock }
import org.scalatest.WordSpec
import org.scalatest._

class CertificateCheckTest extends WordSpec with MustMatchers {

  "CertificateCheck" must {
    "Update state when ssh succeeds and certificates is valid" in {
      val now = System.currentTimeMillis()
      val commands = new SSHCommandMock(now)
      val state = createState(now)
      val mustCert = Seq(ComponentCertificate("Dummy", "1234"))
      val updateState = CertificateCheck.check(state, commands, mustCert)

      updateState.certificates.size must be(1)
      updateState.certificates must be(mustCert)
      updateState.stepResults.size must be(1)
      val step = updateState.stepResults.head
      step.success must be(true)
      step.stepName must be(StepResult.STEP_Certificates)
      step.detail must be(None)
    }
    "Update state when ssh succeeds and certificates is invalid" in {
      val now = System.currentTimeMillis()
      val commands = new SSHCommandMock(now)
      val state = createState(now)
      val mustCert = Seq(ComponentCertificate("Dummy", "4444"))
      val updateState = CertificateCheck.check(state, commands, mustCert)

      updateState.certificates.size must be(1)
      updateState.certificates.head must be(ComponentCertificate("Dummy", "1234"))
      updateState.stepResults.size must be(1)
      val step = updateState.stepResults.head
      step.success must be(false)
      step.stepName must be(StepResult.STEP_Certificates)
      step.detail.get.startsWith("Failed components") must be(true)
    }
    "Update state when ssh succeeds and certificates not in must" in {
      val now = System.currentTimeMillis()
      val commands = new SSHCommandMock(now)
      val state = createState(now)
      val mustCert = Seq(ComponentCertificate("test", "4444"))
      val updateState = CertificateCheck.check(state, commands, mustCert)

      updateState.certificates.size must be(1)
      updateState.certificates.head must be(ComponentCertificate("Dummy", "1234"))
      updateState.stepResults.size must be(1)
      val step = updateState.stepResults.head
      step.success must be(true)
      step.stepName must be(StepResult.STEP_Certificates)
      step.detail must be(None)
    }
    "update state when ssh fails" in {
      val now = System.currentTimeMillis()
      val commands = new SSHCommandMock(now) {
        override def getIRoseCertificates: CommandResult[List[ComponentCertificate]] = {
          CommandResult(List("Connect error"), List())
        }

      }
      val state = createState(now)
      val mustCert = Seq(ComponentCertificate("Dummy", "1234"))
      val updateState = CertificateCheck.check(state, commands, mustCert)

      updateState.certificates.size must be(0)
      updateState.stepResults.size must be(1)
      val step = updateState.stepResults.head
      step.success must be(false)
      step.stepName must be(StepResult.STEP_Certificates)
      step.detail.get.startsWith("Errors getting Certificates") must be(true)
    }
  }

  def createState(now: Long): CalibrationState = {
    CalibrationState(request = GantryCalibrationRequest(
      systemId = "3213", gantryId = "1", time = now,
      reportingOfficerCode = "XX1234",
      mustCertificates = Seq(ComponentCertificate("test", "1234")),
      serialNr = SerialNumber("", "PCD", 5)), timeReceived = new Date(now))
  }

}