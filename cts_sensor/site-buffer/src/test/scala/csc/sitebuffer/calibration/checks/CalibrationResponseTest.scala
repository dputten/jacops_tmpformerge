/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.calibration.checks

import java.io.File
import java.util.Date

import csc.sitebuffer.calibration.{ CalibrationState, ComponentCertificate, GantryCalibrationRequest, SerialNumber, _ }
import csc.sitebuffer.calibration.ssh.TimeDifference
import csc.sitebuffer.config.CameraConfig
import csc.sitebuffer.messages.{ Direction, VehicleMessage }
import org.apache.commons.io.FileUtils
import org.scalatest.WordSpec
import org.scalatest._
import testframe.Resources

class CalibrationResponseTest extends WordSpec with MustMatchers {
  "createImageResult" must {
    "create correct step result when OK" in {
      val rootDir = Resources.getResourceDirPath()
      val targetDir = new File(rootDir, "output/calibration1")
      createFile(targetDir, "image1.jpg")
      createFile(targetDir, "image2.jpg")

      val cameras = createCamerasCfg()
      val now = System.currentTimeMillis()
      var state = createState(now)
      val images = Seq(
        StoreImageResponse("A0", "A0", targetDir.getAbsolutePath, "image1.jpg"),
        StoreImageResponse("A1", "A1", targetDir.getAbsolutePath, "image2.jpg"))
      state = state.copy(images = images)
      val updateState = CalibrationResponse.createImageResult(state, cameras)
      val step = updateState.stepResults.head
      step.success must be(true)
      step.stepName must be(StepResult.STEP_Camera)
      step.detail must be(None)
      FileUtils.deleteQuietly(targetDir)
    }
    "create correct step result when missing files" in {
      val rootDir = Resources.getResourceDirPath()
      val targetDir = new File(rootDir, "output/calibration2")

      val cameras = createCamerasCfg()
      val now = System.currentTimeMillis()
      var state = createState(now)
      val images = Seq(
        StoreImageResponse("A0", "A0", targetDir.getAbsolutePath, "image1.jpg"),
        StoreImageResponse("A1", "A1", targetDir.getAbsolutePath, "image2.jpg"))
      state = state.copy(images = images)
      val updateState = CalibrationResponse.createImageResult(state, cameras)
      val step = updateState.stepResults.head
      step.success must be(false)
      step.stepName must be(StepResult.STEP_Camera)
      step.detail.get must be("Missing physical calibration images for [A0, A1]")
    }
    "create correct step result when missing responses" in {
      val cameras = createCamerasCfg()
      val now = System.currentTimeMillis()
      var state = createState(now)
      val images = Seq(
        StoreImageResponse("A0", "A0", "calibration", "image1.jpg"))
      state = state.copy(images = images)
      val updateState = CalibrationResponse.createImageResult(state, cameras)
      val step = updateState.stepResults.head
      step.success must be(false)
      step.stepName must be(StepResult.STEP_Camera)
      step.detail.get.startsWith("Missing calibration images") must be(true)
    }
  }
  "createVehicleResult" must {
    "create correct step result when OK" in {
      val cameras = createCamerasCfg()
      val now = System.currentTimeMillis()
      var state = createState(now)
      val vehicles = Seq(
        createVehicleResponse(0, now),
        createVehicleResponse(1, now),
        createVehicleResponse(2, now))
      state = state.copy(vehicles = vehicles)
      val updateState = CalibrationResponse.createVehicleResult(state, cameras)
      val step = updateState.stepResults.head
      step.success must be(true)
      step.stepName must be(StepResult.STEP_VehicleRecords)
      step.detail must be(None)
    }
    "create correct step result when missing responses" in {
      val cameras = createCamerasCfg()
      val now = System.currentTimeMillis()
      var state = createState(now)
      val vehicles = Seq(
        createVehicleResponse(0, now),
        createVehicleResponse(2, now))
      state = state.copy(vehicles = vehicles)
      val updateState = CalibrationResponse.createVehicleResult(state, cameras)
      val step = updateState.stepResults.head
      step.success must be(false)
      step.stepName must be(StepResult.STEP_VehicleRecords)
      step.detail.get.startsWith("Missing vehicle registrations for detectors") must be(true)
    }
  }
  "createNTPResult" must {
    "create correct step result when OK" in {
      val cameras = createCamerasCfg()
      val now = System.currentTimeMillis()
      var state = createState(now)
      val cameraTimeDiff = Seq(
        CameraTimeDifferenceResponse(refId = "A0", host = "", port = 23, succeed = true, timeDif = TimeDifference(now, 1), errors = List()),
        CameraTimeDifferenceResponse(refId = "A1", host = "", port = 23, succeed = true, timeDif = TimeDifference(now, 2), errors = List()))
      state = state.copy(iRoseTimeDiff = Some(TimeDifference(now, 0L)), cameraTimeDiff = cameraTimeDiff)
      val updateState = CalibrationResponse.createNTPResult(state, cameras, 10)
      val step = updateState.stepResults.head
      step.success must be(true)
      step.stepName must be(StepResult.STEP_NTP)
      step.detail must be(None)
    }
    "create correct step result when NTP difference is too big" in {
      val cameras = createCamerasCfg()
      val now = System.currentTimeMillis()
      var state = createState(now)
      val cameraTimeDiff = Seq(
        CameraTimeDifferenceResponse(refId = "A0", host = "", port = 23, succeed = true, timeDif = TimeDifference(now, 1), errors = List()),
        CameraTimeDifferenceResponse(refId = "A1", host = "", port = 23, succeed = true, timeDif = TimeDifference(now, 6), errors = List()))
      state = state.copy(iRoseTimeDiff = Some(TimeDifference(now, -5L)), cameraTimeDiff = cameraTimeDiff)
      val updateState = CalibrationResponse.createNTPResult(state, cameras, 10)
      val step = updateState.stepResults.head
      step.success must be(false)
      step.stepName must be(StepResult.STEP_NTP)
      step.detail.get must be("NTPCheck failed for camera = [A1]")
    }
    "create correct step result when irose NTP difference is missing" in {
      val cameras = createCamerasCfg()
      val now = System.currentTimeMillis()
      var state = createState(now)
      val cameraTimeDiff = Seq(
        CameraTimeDifferenceResponse(refId = "A0", host = "", port = 23, succeed = true, timeDif = TimeDifference(now, 1), errors = List()),
        CameraTimeDifferenceResponse(refId = "A1", host = "", port = 23, succeed = true, timeDif = TimeDifference(now, 6), errors = List()))
      state = state.copy(cameraTimeDiff = cameraTimeDiff)
      val updateState = CalibrationResponse.createNTPResult(state, cameras, 10)
      val step = updateState.stepResults.head
      step.success must be(false)
      step.stepName must be(StepResult.STEP_NTP)
      step.detail.get must be("Failed to retrieve iRose NTP information")

    }
    "create correct step result when one camera NTP difference has failed" in {
      val cameras = createCamerasCfg()
      val now = System.currentTimeMillis()
      var state = createState(now)
      val cameraTimeDiff = Seq(
        CameraTimeDifferenceResponse(refId = "A0", host = "", port = 23, succeed = true, timeDif = TimeDifference(now, 1), errors = List()),
        CameraTimeDifferenceResponse(refId = "A1", host = "", port = 23, succeed = false, timeDif = TimeDifference(now, 6), errors = List()))
      state = state.copy(iRoseTimeDiff = Some(TimeDifference(now, -5L)), cameraTimeDiff = cameraTimeDiff)
      val updateState = CalibrationResponse.createNTPResult(state, cameras, 10)
      val step = updateState.stepResults.head
      step.success must be(false)
      step.stepName must be(StepResult.STEP_NTP)
      step.detail.get must be("Failed to retrieve Camera NTP information: [A1]")
    }
    "create correct step result when one camera NTP difference is missing" in {
      val cameras = createCamerasCfg()
      val now = System.currentTimeMillis()
      var state = createState(now)
      val cameraTimeDiff = Seq(
        CameraTimeDifferenceResponse(refId = "A0", host = "", port = 23, succeed = true, timeDif = TimeDifference(now, 1), errors = List()))
      state = state.copy(iRoseTimeDiff = Some(TimeDifference(now, -5L)), cameraTimeDiff = cameraTimeDiff)
      val updateState = CalibrationResponse.createNTPResult(state, cameras, 10)
      val step = updateState.stepResults.head
      step.success must be(false)
      step.stepName must be(StepResult.STEP_NTP)
      step.detail.get must be("Missing time difference camera: [A1]")
    }
  }
  "checkCalibrationFinished" must {
    "return response when finished" in {
      val rootDir = Resources.getResourceDirPath()

      val cameras = createCamerasCfg()
      val now = System.currentTimeMillis()
      var state = createState(now)
      val images = Seq(
        StoreImageResponse("A0", "A0", rootDir.getAbsolutePath, "image1.jpg"),
        StoreImageResponse("A1", "A1", rootDir.getAbsolutePath, "image2.jpg"))
      state = state.copy(images = images)
      val vehicles = Seq(
        createVehicleResponse(0, now),
        createVehicleResponse(1, now),
        createVehicleResponse(2, now))
      state = state.copy(vehicles = vehicles)
      val cameraTimeDiff = Seq(
        CameraTimeDifferenceResponse(refId = "A0", host = "", port = 23, succeed = true, timeDif = TimeDifference(now, 1), errors = List()),
        CameraTimeDifferenceResponse(refId = "A1", host = "", port = 23, succeed = true, timeDif = TimeDifference(now, 6), errors = List()))
      state = state.copy(iRoseTimeDiff = Some(TimeDifference(now, 0L)), cameraTimeDiff = cameraTimeDiff)

      val cert = Seq(ComponentCertificate("Dummy", "1234"))
      val steps = Seq(
        StepResult(StepResult.STEP_Certificates, true, None),
        StepResult(StepResult.STEP_Loops, true, None))
      state = state.copy(stepResults = steps, certificates = cert)

      val responseOpt = CalibrationResponse.checkCalibrationFinished(state = state,
        cameras = cameras,
        NTPmaxDiff_ms = 10,
        calibrationImageDir = "calibration",
        100)
      responseOpt.isDefined must be(true)
      val response = responseOpt.get
      val req = state.request
      response.systemId must be(req.systemId)
      response.gantryId must be(req.gantryId)
      response.time must be(req.time)
      response.reportingOfficerCode must be(req.reportingOfficerCode)
      response.serialNr must be("PCD01234")
      response.activeCertificates must be(cert)
      val stepResults = Seq(
        StepResult(StepResult.STEP_Camera, false, Some("Missing physical calibration images for [A0, A1]")),
        StepResult(StepResult.STEP_Certificates, true, None),
        StepResult(StepResult.STEP_Loops, true, None),
        StepResult(StepResult.STEP_NTP, true, None),
        StepResult(StepResult.STEP_VehicleRecords, true, None)).sortBy(_.stepName)
      response.results.sortBy(_.stepName) must be(stepResults)
      response.images must be(Seq())
    }
    "return None when missing cameras" in {
      val cameras = createCamerasCfg()
      val now = System.currentTimeMillis()
      var state = createState(now)
      val vehicles = Seq(
        createVehicleResponse(0, now),
        createVehicleResponse(1, now),
        createVehicleResponse(2, now))
      state = state.copy(vehicles = vehicles)
      val cameraTimeDiff = Seq(
        CameraTimeDifferenceResponse(refId = "A0", host = "", port = 23, succeed = true, timeDif = TimeDifference(now, 1), errors = List()),
        CameraTimeDifferenceResponse(refId = "A1", host = "", port = 23, succeed = true, timeDif = TimeDifference(now, 6), errors = List()))
      state = state.copy(iRoseTimeDiff = Some(TimeDifference(now, 0L)), cameraTimeDiff = cameraTimeDiff)

      val cert = Seq(ComponentCertificate("Dummy", "1234"))
      val steps = Seq(
        StepResult(StepResult.STEP_Certificates, true, None),
        StepResult(StepResult.STEP_Loops, true, None))
      state = state.copy(stepResults = steps, certificates = cert)

      val response = CalibrationResponse.checkCalibrationFinished(state = state,
        cameras = cameras,
        NTPmaxDiff_ms = 10,
        calibrationImageDir = "calibration",
        100)
      response must be(None)
    }
  }

  "createCalibrationResult" must {
    "create response when fully successful filled" in {
      val rootDir = Resources.getResourceDirPath()
      val targetDir = new File(rootDir, "output/calibration3")
      createFile(targetDir, "image1.jpg")
      createFile(targetDir, "image2.jpg")

      val cameras = createCamerasCfg()
      val now = System.currentTimeMillis()
      var state = createState(now)
      val images = Seq(
        StoreImageResponse("A0", "A0", targetDir.getAbsolutePath, "image1.jpg"),
        StoreImageResponse("A1", "A1", targetDir.getAbsolutePath, "image2.jpg"))
      state = state.copy(images = images)
      val vehicles = Seq(
        createVehicleResponse(0, now),
        createVehicleResponse(1, now),
        createVehicleResponse(2, now))
      state = state.copy(vehicles = vehicles)
      val cameraTimeDiff = Seq(
        CameraTimeDifferenceResponse(refId = "A0", host = "", port = 23, succeed = true, timeDif = TimeDifference(now, 1), errors = List()),
        CameraTimeDifferenceResponse(refId = "A1", host = "", port = 23, succeed = true, timeDif = TimeDifference(now, 6), errors = List()))
      state = state.copy(iRoseTimeDiff = Some(TimeDifference(now, 0L)), cameraTimeDiff = cameraTimeDiff)

      val cert = Seq(ComponentCertificate("Dummy", "1234"))
      val steps = Seq(
        StepResult(StepResult.STEP_Certificates, true, None),
        StepResult(StepResult.STEP_Loops, true, None))
      state = state.copy(stepResults = steps, certificates = cert)

      val response = CalibrationResponse.createCalibrationResult(state = state,
        cameras = cameras,
        NTPmaxDiff_ms = 10,
        calibrationImageDir = "calibration",
        100)
      val req = state.request

      response.systemId must be(req.systemId)
      response.gantryId must be(req.gantryId)
      response.time must be(req.time)
      response.reportingOfficerCode must be(req.reportingOfficerCode)
      response.serialNr must be("PCD01234")
      response.activeCertificates must be(cert)
      val stepResults = Seq(
        StepResult(StepResult.STEP_Camera, true, None),
        StepResult(StepResult.STEP_Certificates, true, None),
        StepResult(StepResult.STEP_Loops, true, None),
        StepResult(StepResult.STEP_NTP, true, None),
        StepResult(StepResult.STEP_VehicleRecords, true, None)).sortBy(_.stepName)
      response.results.sortBy(_.stepName) must be(stepResults)
      response.images.size must be(2)
      val image1 = response.images.find(_.name == "image1.jpg").get
      image1.imageId must be("%s-%s-A0".format(req.systemId, req.gantryId))
      image1.checksum must be("900eaceca7709068968595061ddbd724")

      new String(image1.name) must be("image1.jpg")
      val file1 = new File(targetDir, "image1.jpg")
      file1.exists() must be(false)
      val image2 = response.images.find(_.name == "image2.jpg").get
      image2.imageId must be("%s-%s-A1".format(req.systemId, req.gantryId))
      image2.checksum must be("8ceae0716d7f9857c769703320221c35")

      new String(image2.name) must be("image2.jpg")
      val file2 = new File(targetDir, "image2.jpg")
      file2.exists() must be(false)

      FileUtils.deleteDirectory(targetDir)
    }
    "create response when missing physical files" in {
      val rootDir = Resources.getResourceDirPath()

      val cameras = createCamerasCfg()
      val now = System.currentTimeMillis()
      var state = createState(now)
      val images = Seq(
        StoreImageResponse("A0", "A0", rootDir.getAbsolutePath, "image1.jpg"),
        StoreImageResponse("A1", "A1", rootDir.getAbsolutePath, "image2.jpg"))
      state = state.copy(images = images)
      val vehicles = Seq(
        createVehicleResponse(0, now),
        createVehicleResponse(1, now),
        createVehicleResponse(2, now))
      state = state.copy(vehicles = vehicles)
      val cameraTimeDiff = Seq(
        CameraTimeDifferenceResponse(refId = "A0", host = "", port = 23, succeed = true, timeDif = TimeDifference(now, 1), errors = List()),
        CameraTimeDifferenceResponse(refId = "A1", host = "", port = 23, succeed = true, timeDif = TimeDifference(now, 6), errors = List()))
      state = state.copy(iRoseTimeDiff = Some(TimeDifference(now, 0L)), cameraTimeDiff = cameraTimeDiff)

      val cert = Seq(ComponentCertificate("Dummy", "1234"))
      val steps = Seq(
        StepResult(StepResult.STEP_Certificates, true, None),
        StepResult(StepResult.STEP_Loops, true, None))
      state = state.copy(stepResults = steps, certificates = cert)

      val response = CalibrationResponse.createCalibrationResult(state = state,
        cameras = cameras,
        NTPmaxDiff_ms = 10,
        calibrationImageDir = "calibration",
        100)
      val req = state.request

      response.systemId must be(req.systemId)
      response.gantryId must be(req.gantryId)
      response.time must be(req.time)
      response.reportingOfficerCode must be(req.reportingOfficerCode)
      response.serialNr must be("PCD01234")
      response.activeCertificates must be(cert)
      val stepResults = Seq(
        StepResult(StepResult.STEP_Camera, false, Some("Missing physical calibration images for [A0, A1]")),
        StepResult(StepResult.STEP_Certificates, true, None),
        StepResult(StepResult.STEP_Loops, true, None),
        StepResult(StepResult.STEP_NTP, true, None),
        StepResult(StepResult.STEP_VehicleRecords, true, None)).sortBy(_.stepName)
      response.results.sortBy(_.stepName) must be(stepResults)
      response.images must be(Seq())
    }
    "create response when missing all data" in {
      val cameras = createCamerasCfg()
      val now = System.currentTimeMillis()
      var state = createState(now)

      val response = CalibrationResponse.createCalibrationResult(state = state,
        cameras = cameras,
        NTPmaxDiff_ms = 10,
        calibrationImageDir = "calibration",
        100)
      val req = state.request

      response.systemId must be(req.systemId)
      response.gantryId must be(req.gantryId)
      response.time must be(req.time)
      response.reportingOfficerCode must be(req.reportingOfficerCode)
      response.serialNr must be("")
      response.activeCertificates must be(Seq())
      val stepResults = Seq(
        StepResult(StepResult.STEP_Camera, false, Some("Missing calibration images: [A0, A1]")),
        StepResult(StepResult.STEP_Certificates, false, Some("Step not finished")),
        StepResult(StepResult.STEP_Loops, false, Some("Step not finished")),
        StepResult(StepResult.STEP_NTP, false, Some("Failed to retrieve iRose NTP information")),
        StepResult(StepResult.STEP_VehicleRecords, false, Some("Missing vehicle registrations for detectors: [0, 1, 2]"))).sortBy(_.stepName)
      response.results.sortBy(_.stepName) must be(stepResults)
      response.images must be(Seq())
    }
  }

  "getStepResults" must {
    "return all steps when comclete" in {
      val stepResults = Seq(
        StepResult(StepResult.STEP_Camera, true, None),
        StepResult(StepResult.STEP_Certificates, true, None),
        StepResult(StepResult.STEP_Loops, false, Some("error")),
        StepResult(StepResult.STEP_NTP, true, None),
        StepResult(StepResult.STEP_VehicleRecords, true, None))
      CalibrationResponse.getStepResults(stepResults).sortBy(_.stepName) must be(stepResults.sortBy(_.stepName))
    }
    "return all steps when incomclete" in {
      val stepResults = Seq(
        StepResult(StepResult.STEP_Camera, true, None),
        StepResult(StepResult.STEP_Certificates, true, None),
        StepResult(StepResult.STEP_Loops, false, Some("error")))
      val addedResults = Seq(
        StepResult(StepResult.STEP_NTP, false, Some("Step not finished")),
        StepResult(StepResult.STEP_VehicleRecords, false, Some("Step not finished")))

      CalibrationResponse.getStepResults(stepResults).sortBy(_.stepName) must be((stepResults ++ addedResults).sortBy(_.stepName))
    }
    "return all steps when empty" in {
      val addedResults = Seq(
        StepResult(StepResult.STEP_Camera, false, Some("Step not finished")),
        StepResult(StepResult.STEP_Certificates, false, Some("Step not finished")),
        StepResult(StepResult.STEP_Loops, false, Some("Step not finished")),
        StepResult(StepResult.STEP_NTP, false, Some("Step not finished")),
        StepResult(StepResult.STEP_VehicleRecords, false, Some("Step not finished")))

      CalibrationResponse.getStepResults(Seq()).sortBy(_.stepName) must be(addedResults.sortBy(_.stepName))
    }
  }
  "getSerialNr" must {
    "format serialNumber" in {
      val now = System.currentTimeMillis()
      val vehicles = Seq(
        createVehicleResponse(0, now))
      CalibrationResponse.getSerialNr(vehicles, SerialNumber("", "PCB", 5)) must be("PCB01234")
      CalibrationResponse.getSerialNr(vehicles, SerialNumber("", "PCB", 4)) must be("PCB1234")
      CalibrationResponse.getSerialNr(vehicles, SerialNumber("", "", 4)) must be("1234")
      CalibrationResponse.getSerialNr(Seq(), SerialNumber("", "", 4)) must be("")
      CalibrationResponse.getSerialNr(Seq(), SerialNumber("123", "", 4)) must be("123")
    }
  }
  /*
  checkCalibrationFinished
  createCalibrationResult
 */

  def createVehicleResponse(detector: Int, now: Long): GetRegistrationResponse = {
    GetRegistrationResponse(refId = detector.toString, detector = detector,
      registration = new VehicleMessage(dateTime = now,
        detector = detector, direction = Direction.Outgoing,
        speed = 325, length = 4.5F, loop1RiseTime = 0, loop1FallTime = 200,
        loop2RiseTime = 300, loop2FallTime = 400, yellowTime2 = 0, redTime2 = 0,
        dateTime1 = now - 200, yellowTime1 = 0, redTime1 = 0, dateTime3 = now + 400,
        yellowTime3 = 0, redTime3 = 0, valid = false,
        serialNr = "1234", offset1 = 0, offset2 = 0, offset3 = 0))
  }

  def createCamerasCfg(): Seq[CameraConfig] = {
    Seq(
      CameraConfig(cameraId = "A0", cameraHost = "1", cameraPort = 0,
        maxTriggerRetries = 0, timeDisconnectedMs = 0,
        rebootScript = "", detectors = Seq(0, 1), imageDir = "A0",
        maxWaitForImage = 0L, maxWaitForReboot = 0L, waitForFixedMsg = 0L),
      CameraConfig(cameraId = "A1", cameraHost = "2", cameraPort = 0,
        maxTriggerRetries = 0, timeDisconnectedMs = 0,
        rebootScript = "", detectors = Seq(2), imageDir = "A1",
        maxWaitForImage = 0L, maxWaitForReboot = 0L, waitForFixedMsg = 0L))

  }

  def createState(now: Long): CalibrationState = {
    CalibrationState(request = GantryCalibrationRequest(
      systemId = "3213", gantryId = "1", time = now,
      reportingOfficerCode = "XX1234",
      mustCertificates = Seq(ComponentCertificate("test", "1234")),
      serialNr = SerialNumber("", "PCD", 5)), timeReceived = new Date(now))
  }

  def createFile(cameraDirFile: File, name: String): File = {
    if (!cameraDirFile.exists()) {
      cameraDirFile.mkdirs()
    }
    val file = new File(cameraDirFile, name)
    FileUtils.write(file, "test-image_" + name)
    file
  }

}