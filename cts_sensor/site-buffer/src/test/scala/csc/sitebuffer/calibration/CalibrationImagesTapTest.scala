/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.calibration

import java.io.File

import akka.actor.{ ActorSystem, Props }
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import scala.concurrent.duration._
import csc.akka.logging.DirectLogging
import csc.sitebuffer.process.images.NewImage
import org.apache.commons.io.FileUtils
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }
import testframe.Resources

class CalibrationImagesTapTest extends TestKit(ActorSystem("CalibrationImagesTapTest")) with WordSpecLike with MustMatchers
  with DirectLogging with BeforeAndAfterEach with BeforeAndAfterAll {
  val rootDir = Resources.getResourceDirPath()
  val inputDir = new File(rootDir, "input")
  val targetDir = new File(rootDir, "output")

  override def afterAll() {
    try {
      system.shutdown
    } catch {
      case e: Exception ⇒ log.error("COULD NOT SHUTDOWN ACTOR REGISTRY".format(e))
    }
  }

  override protected def afterEach() {
    super.afterEach()
    FileUtils.deleteDirectory(targetDir)
    FileUtils.deleteDirectory(inputDir)
  }

  "CalibrationImages" must {
    "pass Image without requests" in {
      val output = TestProbe()
      val cameraDir = "A0"
      val tap = TestActorRef(Props(new CalibrationImagesTap(Set(output.ref))))
      val file = createFile(cameraDir, "image1.jpg")
      val image = NewImage(file.getAbsolutePath, System.currentTimeMillis())
      tap ! image
      output.expectMsg(image)
    }
    "pass Images with other request" in {
      val output = TestProbe()
      val requestor = TestProbe()
      val cameraDir = "A0"
      val tap = TestActorRef(Props(new CalibrationImagesTap(Set(output.ref))))

      requestor.send(tap, StoreImageRequest("Camera1", "A1", targetDir.getAbsolutePath))
      val file = createFile(cameraDir, "image2.jpg")
      val image = NewImage(file.getAbsolutePath, System.currentTimeMillis())
      tap ! image
      output.expectMsg(image)
      //check if image isn't copied
      val targetFile = new File(targetDir, file.getName)
      targetFile.exists() must be(false)
      requestor.expectNoMsg(100.millis)
    }
    "pass Images with request" in {
      val output = TestProbe()
      val requestor = TestProbe()
      val cameraDir = "A0"
      val tap = TestActorRef(Props(new CalibrationImagesTap(Set(output.ref))))
      val req = StoreImageRequest("Camera1", cameraDir, targetDir.getAbsolutePath)
      requestor.send(tap, req)
      val file = createFile(cameraDir, "image3.jpg")
      val image = NewImage(file.getAbsolutePath, System.currentTimeMillis())
      tap ! image
      output.expectMsg(image)
      //check if image isn't copied
      val targetFile = new File(targetDir, "0_" + file.getName)
      targetFile.exists() must be(true)
      val msg = StoreImageResponse(req.refId, req.cameraDir, req.targetDir, targetFile.getName)
      requestor.expectMsg(msg)
    }
    "pass Images with multiple requests" in {
      val output = TestProbe()
      val requestor = TestProbe()
      val cameraDir = "A0"
      val tap = TestActorRef(Props(new CalibrationImagesTap(Set(output.ref))))
      val req = StoreImageRequest("Camera1", cameraDir, targetDir.getAbsolutePath)
      requestor.send(tap, req)
      val req2 = StoreImageRequest("Camera1_2", cameraDir, targetDir.getAbsolutePath)
      requestor.send(tap, req2)
      val file = createFile(cameraDir, "image4.jpg")
      val image = NewImage(file.getAbsolutePath, System.currentTimeMillis())
      tap ! image
      output.expectMsg(image)
      //check if image isn't copied
      val targetFile = new File(targetDir, "0_" + file.getName)
      targetFile.exists() must be(true)
      val msg = StoreImageResponse(req.refId, req.cameraDir, req.targetDir, targetFile.getName)
      requestor.expectMsg(msg)
      val targetFile2 = new File(targetDir, "1_" + file.getName)
      targetFile2.exists() must be(true)
      val msg2 = StoreImageResponse(req2.refId, req2.cameraDir, req2.targetDir, targetFile2.getName)
      requestor.expectMsg(msg2)
    }
    "send only one response" in {
      val output = TestProbe()
      val requestor = TestProbe()
      val cameraDir = "A0"
      val tap = TestActorRef(Props(new CalibrationImagesTap(Set(output.ref))))
      val req = StoreImageRequest("Camera1", cameraDir, targetDir.getAbsolutePath)
      requestor.send(tap, req)
      val file = createFile(cameraDir, "image5.jpg")
      val image = NewImage(file.getAbsolutePath, System.currentTimeMillis())
      tap ! image
      output.expectMsg(image)
      //check if image isn't copied
      val targetFile = new File(targetDir, "0_" + file.getName)
      targetFile.exists() must be(true)
      val msg = StoreImageResponse(req.refId, req.cameraDir, req.targetDir, targetFile.getName)
      requestor.expectMsg(msg)

      tap ! image
      output.expectMsg(image)
      requestor.expectNoMsg(100.millis)
    }
  }

  def createFile(cameraDir: String, name: String): File = {
    val cameraDirFile = new File(inputDir, cameraDir)
    if (!cameraDirFile.exists()) {
      cameraDirFile.mkdirs()
    }
    val file = new File(cameraDirFile, name)
    FileUtils.write(file, "test-image")
    file
  }
}