/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.calibration

import net.liftweb.json.{ DefaultFormats, Serialization }
import org.scalatest.WordSpec
import org.scalatest._

class SerializeResponseTest extends WordSpec with MustMatchers {
  implicit val formats = DefaultFormats
  "Lift serialize" must {
    "be able to code and decode" in {
      val resp = GantryCalibrationResponse(systemId = "3213", gantryId = "1", time = System.currentTimeMillis(),
        reportingOfficerCode = "XX1234",
        serialNr = "SN1000",
        activeCertificates = Seq(ComponentCertificate("Dummy", "SDGHGH")),
        images = Seq(CalibrationImage(name = "image.jpg", imageId = "3213-1-A1", checksum = "asdsada", image = "Image".getBytes.map(_.toInt))),
        results = Seq(StepResult(stepName = "NTP", success = true, detail = Some("detail"))))

      val json = Serialization.write(resp)
      println(json)
      val response = Serialization.read[GantryCalibrationResponse](json)
      response.systemId must be(resp.systemId)
      response.gantryId must be(resp.gantryId)
      response.time must be(resp.time)
      response.reportingOfficerCode must be(resp.reportingOfficerCode)
      response.serialNr must be(resp.serialNr)
      response.activeCertificates must be(resp.activeCertificates)
      response.images must be(resp.images)
    }
  }
}