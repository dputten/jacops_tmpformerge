/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.calibration.checks

import java.util.Date

import akka.actor.ActorSystem
import akka.testkit.{ TestKit, TestProbe }
import csc.akka.logging.DirectLogging
import csc.sitebuffer.calibration.{ CalibrationState, ComponentCertificate, GantryCalibrationRequest, SerialNumber, _ }
import csc.sitebuffer.calibration.ssh.{ CommandResult, SSHCommandMock, TimeDifference }
import csc.sitebuffer.config.CameraConfig
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import org.scalatest._

class TimeDifferenceCheckTest extends TestKit(ActorSystem("TimeDifferenceCheckTest")) with WordSpecLike with MustMatchers
  with DirectLogging with BeforeAndAfterAll {
  override def afterAll() {
    try {
      system.shutdown
    } catch {
      case e: Exception ⇒ log.error("COULD NOT SHUTDOWN ACTOR REGISTRY".format(e))
    }
  }

  "retrieveIroseTimeDifference" must {
    "update state when ssh succeeds" in {
      val now = System.currentTimeMillis()
      val commands = new SSHCommandMock(now)
      val state = createState(now)
      val updateState = TimeDifferenceCheck.retrieveIroseTimeDifference(state, commands)
      updateState.iRoseTimeDiff must be(Some(TimeDifference(now, 0L)))
      updateState.stepResults.size must be(0)
    }
    "update state when ssh fails" in {
      val now = System.currentTimeMillis()
      val commands = new SSHCommandMock(now) {
        override def getIRoseTimeDifference: CommandResult[TimeDifference] = {
          CommandResult(List("Connection failure"), TimeDifference(0L, 0L))
        }
      }
      val state = createState(now)
      val updateState = TimeDifferenceCheck.retrieveIroseTimeDifference(state, commands)
      updateState.iRoseTimeDiff must be(None)
      updateState.stepResults.size must be(1)
      val step = updateState.stepResults.head
      step.success must be(false)
      step.stepName must be(StepResult.STEP_NTP)
      step.detail.get.startsWith("Failed to get the iRose Time difference") must be(true)
    }
  }
  "retriggerCameraCheck" must {
    "do nothing when all requests are received" in {
      val cameraTimeDiff = TestProbe()
      val cameras = Seq(
        CameraConfig(cameraId = "A0", cameraHost = "", cameraPort = 0,
          maxTriggerRetries = 0, timeDisconnectedMs = 0,
          rebootScript = "", detectors = Seq(0, 1), imageDir = "A0",
          maxWaitForImage = 0L, maxWaitForReboot = 0L, waitForFixedMsg = 0L),
        CameraConfig(cameraId = "A1", cameraHost = "", cameraPort = 0,
          maxTriggerRetries = 0, timeDisconnectedMs = 0,
          rebootScript = "", detectors = Seq(2), imageDir = "A1",
          maxWaitForImage = 0L, maxWaitForReboot = 0L, waitForFixedMsg = 0L))
      val now = System.currentTimeMillis()
      val responses = Seq(
        CameraTimeDifferenceResponse(refId = "A0", host = "", port = 23, succeed = true, timeDif = TimeDifference(now, 1), errors = List()),
        CameraTimeDifferenceResponse(refId = "A1", host = "", port = 23, succeed = true, timeDif = TimeDifference(now, 2), errors = List()))

      TimeDifferenceCheck.retriggerCameraCheck(cameraTimeDiffActor = cameraTimeDiff.ref,
        cameras = cameras,
        cameraTimeDiff = responses,
        telnetPort = 23, telnetUser = "root", telnetPwd = "PWD")
      cameraTimeDiff.expectNoMsg()
    }
    "send new trigger when one requests is missing" in {
      val cameraTimeDiff = TestProbe()
      val cameras = Seq(
        CameraConfig(cameraId = "A0", cameraHost = "1", cameraPort = 0,
          maxTriggerRetries = 0, timeDisconnectedMs = 0,
          rebootScript = "", detectors = Seq(0, 1), imageDir = "A0",
          maxWaitForImage = 0L, maxWaitForReboot = 0L, waitForFixedMsg = 0L),
        CameraConfig(cameraId = "A1", cameraHost = "2", cameraPort = 0,
          maxTriggerRetries = 0, timeDisconnectedMs = 0,
          rebootScript = "", detectors = Seq(2), imageDir = "A1",
          maxWaitForImage = 0L, maxWaitForReboot = 0L, waitForFixedMsg = 0L))
      val now = System.currentTimeMillis()
      val responses = Seq(
        CameraTimeDifferenceResponse(refId = "A0", host = "", port = 23, succeed = true, timeDif = TimeDifference(now, 1), errors = List()))

      TimeDifferenceCheck.retriggerCameraCheck(cameraTimeDiffActor = cameraTimeDiff.ref,
        cameras = cameras,
        cameraTimeDiff = responses,
        telnetPort = 23, telnetUser = "root", telnetPwd = "PWD")
      cameraTimeDiff.expectMsg(
        CameraTimeDifferenceRequest(refId = "A1", host = "2",
          port = 23,
          user = "root",
          pwd = "PWD"))
    }
    "send new trigger when one requests has failed" in {
      val cameraTimeDiff = TestProbe()
      val cameras = Seq(
        CameraConfig(cameraId = "A0", cameraHost = "1", cameraPort = 0,
          maxTriggerRetries = 0, timeDisconnectedMs = 0,
          rebootScript = "", detectors = Seq(0, 1), imageDir = "A0",
          maxWaitForImage = 0L, maxWaitForReboot = 0L, waitForFixedMsg = 0L),
        CameraConfig(cameraId = "A1", cameraHost = "2", cameraPort = 0,
          maxTriggerRetries = 0, timeDisconnectedMs = 0,
          rebootScript = "", detectors = Seq(2), imageDir = "A1",
          maxWaitForImage = 0L, maxWaitForReboot = 0L, waitForFixedMsg = 0L))
      val now = System.currentTimeMillis()
      val responses = Seq(
        CameraTimeDifferenceResponse(refId = "A0", host = "", port = 23, succeed = true, timeDif = TimeDifference(now, 1), errors = List()),
        CameraTimeDifferenceResponse(refId = "A1", host = "", port = 23, succeed = false, timeDif = TimeDifference(now, 2), errors = List("connecterror")))

      TimeDifferenceCheck.retriggerCameraCheck(cameraTimeDiffActor = cameraTimeDiff.ref,
        cameras = cameras,
        cameraTimeDiff = responses,
        telnetPort = 23, telnetUser = "root", telnetPwd = "PWD")
      cameraTimeDiff.expectMsg(
        CameraTimeDifferenceRequest(refId = "A1", host = "2",
          port = 23,
          user = "root",
          pwd = "PWD"))
    }
  }

  def createState(now: Long): CalibrationState = {
    CalibrationState(request = GantryCalibrationRequest(
      systemId = "3213", gantryId = "1", time = now,
      reportingOfficerCode = "XX1234",
      mustCertificates = Seq(ComponentCertificate("test", "1234")),
      serialNr = SerialNumber("", "PCD", 5)), timeReceived = new Date(now))
  }
}