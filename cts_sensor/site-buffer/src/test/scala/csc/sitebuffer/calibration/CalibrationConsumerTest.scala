/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.calibration

import akka.actor.{ Actor, ActorRef, ActorSystem, Props }
import akka.camel.{ CamelExtension, Producer }
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import akka.util.Timeout
import csc.akka.CamelClient

import scala.concurrent.duration._
import csc.akka.remote.wire.Protocol
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, WordSpec }

import scala.concurrent.Await

/**
 * Test van calibrationConsumer actor.
 */
class CalibrationConsumerTest
  extends TestKit(ActorSystem("CalibrationConsumerTest"))
  with WordSpecLike
  with MustMatchers
  with BeforeAndAfterAll
  with CamelClient
  with Protocol {

  val calibrationProducer = TestActorRef(new TestCalibrationProducer())

  val camel = CamelExtension(system)
  implicit val timeout = Timeout(10 seconds)
  implicit val executor = system.dispatcher

  override def beforeAll() {
    super.beforeAll()
  }

  override def afterAll() {
    calibrationProducer.stop()
    system.shutdown()
  }

  def createCalibrationConsumer(calibrationRecipients: Set[ActorRef]): ActorRef = {
    system.actorOf(Props(
      new CalibrationConsumer(
        calibrationEndpointHost = "localhost",
        calibrationEndpointPort = 12321,
        recipients = calibrationRecipients)))
  }

  "An calibrationConsumer actor" when {

    "receiving a message" must {
      "send an calibrationRequest to all recipients" in {
        val output = TestProbe()
        val consumer = createCalibrationConsumer(Set(output.ref))
        val future = camel.activationFutureFor(consumer)
        Await.ready(future, 30.seconds)
        val calibrationPlainMessage = """{"systemId":"A2", "gantryId":"1", "time": 1432891605000, "reportingOfficerCode":"XX1234", """ ++
          """ "mustCertificates":[{"name":"iRoseDep", "checksum":"432432432"}],""" ++
          """ "serialNr":{"serialNumber":"", "serialNumberPrefix":"PCB",  "serialNumberLength":5} }"""

        val msgFragments = makeFragments(calibrationPlainMessage.getBytes, 900)

        msgFragments.foreach(calibrationProducer ! _)
        val expectedMessage = GantryCalibrationRequest("A2", "1", 1432891605000L, "XX1234", Seq(ComponentCertificate("iRoseDep", "432432432")), SerialNumber("", "PCB", 5))
        output.expectMsg(expectedMessage)
        system.stop(consumer)
        val future2 = camel.deactivationFutureFor(consumer)
        Await.ready(future2, 30.seconds)
      }
    }

    "receiving an invalid message" in {
      val output = TestProbe()
      val consumer = createCalibrationConsumer(Set(output.ref))
      val future = camel.activationFutureFor(consumer)
      Await.ready(future, 30.seconds)
      //missing systemID
      val calibrationPlainMessage = """{"gantryId":"1", "time": 1432891605000,""" ++
        """ "mustCertificates":[{"name":"iRoseDep", "checksum":"432432432"}],""" ++
        """ "config":[]}"""
      val msgFragments = makeFragments(calibrationPlainMessage.getBytes, 900)

      msgFragments.foreach(calibrationProducer ! _)
      output.expectNoMsg(100.millis)
      system.stop(consumer)
      val future2 = camel.deactivationFutureFor(consumer)
      Await.ready(future2, 30.seconds)
    }
  }

  class TestCalibrationProducer extends Actor with Producer {
    def endpointUri = "mina:tcp://localhost:12321?textline=true&sync=false"
  }

}
