/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.calibration.checks

import java.util.Date

import csc.sitebuffer.calibration.{ CalibrationState, ComponentCertificate, GantryCalibrationRequest, _ }
import csc.sitebuffer.calibration.ssh.{ CommandResult, SSHCommandMock }
import csc.sitebuffer.process.irose.IRoseLoopError
import org.scalatest.WordSpec
import org.scalatest._

class IRoseLoopsCheckTest extends WordSpec with MustMatchers {
  "IRoseLoopCheck" must {
    "Update state when ssh succeeds" in {
      val now = System.currentTimeMillis()
      val commands = new SSHCommandMock(now)
      val state = createState(now)
      val updateState = IRoseLoopsCheck.check(state, commands)

      updateState.iRoseLoopErrors.size must be(0)
      updateState.stepResults.size must be(1)
      val step = updateState.stepResults.head
      step.success must be(true)
      step.stepName must be(StepResult.STEP_Loops)
      step.detail must be(None)
    }
    "update state when ssh succeeds with looperors" in {
      val now = System.currentTimeMillis()
      val error = IRoseLoopError("1", "error")
      val commands = new SSHCommandMock(now) {
        override def getIRoseLoopErrors: CommandResult[List[IRoseLoopError]] = {
          CommandResult(List(), List(error))
        }
      }
      val state = createState(now)
      val updateState = IRoseLoopsCheck.check(state, commands)

      updateState.iRoseLoopErrors.size must be(1)
      updateState.iRoseLoopErrors.head must be(error)
      updateState.stepResults.size must be(1)

      val step = updateState.stepResults.head
      step.success must be(false)
      step.stepName must be(StepResult.STEP_Loops)
      step.detail.get must be("Failed loops: [detector=1 state=error]")
    }
    "update state when ssh fails" in {
      val now = System.currentTimeMillis()
      val commands = new SSHCommandMock(now) {
        override def getIRoseLoopErrors: CommandResult[List[IRoseLoopError]] = {
          CommandResult(List("Connect error"), List())
        }
      }
      val state = createState(now)
      val updateState = IRoseLoopsCheck.check(state, commands)

      updateState.iRoseLoopErrors.size must be(0)
      updateState.stepResults.size must be(1)

      val step = updateState.stepResults.head
      step.success must be(false)
      step.stepName must be(StepResult.STEP_Loops)
      step.detail.get.startsWith("Errors getting Loop errors") must be(true)
    }
  }

  def createState(now: Long): CalibrationState = {
    CalibrationState(request = GantryCalibrationRequest(
      systemId = "3213", gantryId = "1", time = now,
      reportingOfficerCode = "XX1234",
      mustCertificates = Seq(ComponentCertificate("test", "1234")),
      serialNr = SerialNumber("", "PCD", 5)), timeReceived = new Date(now))
  }

}