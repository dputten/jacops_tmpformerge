/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.calibration.ssh

import csc.sitebuffer.calibration.ComponentCertificate
import csc.sitebuffer.process.irose.IRoseLoopError

class SSHCommandMock(now: Long) extends SSHCommands {
  def connect(host: String, port: Int, user: String, pwd: String, timeout: Int) {}

  def disconnect() {}

  def getIRoseCertificates: CommandResult[List[ComponentCertificate]] = {
    CommandResult(List(), List(ComponentCertificate("Dummy", "1234")))
  }

  def getIRoseLoopErrors: CommandResult[List[IRoseLoopError]] = {
    CommandResult(List(), List())
  }

  def getIRoseTimeDifference: CommandResult[TimeDifference] = {
    CommandResult(List(), TimeDifference(now, 0L))
  }

  def calibrateLane(detectorId: Int): CommandResult[Boolean] = {
    CommandResult(List(), true)
  }
}