/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.calibration

import akka.actor.{ ActorSystem, Props }
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import scala.concurrent.duration._
import csc.akka.logging.DirectLogging
import csc.sitebuffer.messages.{ Direction, VehicleMessage }
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }

class CalibrationRegistrationTapTest
  extends TestKit(ActorSystem("CalibrationRegistrationTapTest"))
  with WordSpecLike
  with MustMatchers
  with DirectLogging
  with BeforeAndAfterAll {

  override def afterAll() {
    try {
      system.shutdown()
    } catch {
      case e: Exception ⇒ log.error("COULD NOT SHUTDOWN ACTOR REGISTRY".format(e))
    }
  }

  "CalibrationRegistrations" must {
    "pass valid Message without requests" in {
      val output = TestProbe()
      val detector = 0
      val tap = TestActorRef(Props(new CalibrationRegistrationTap(300F, Set(output.ref))))
      val vehicle = createVehicleMsg(detector, 200F, true)
      tap ! vehicle
      output.expectMsg(vehicle)
    }
    "pass invalid Message without requests" in {
      val output = TestProbe()
      val detector = 0
      val tap = TestActorRef(Props(new CalibrationRegistrationTap(300F, Set(output.ref))))
      val vehicle = createVehicleMsg(detector, 200F, false)
      tap ! vehicle
      output.expectMsg(vehicle)
    }
    "filter calibration Message without requests" in {
      val output = TestProbe()
      val detector = 0
      val tap = TestActorRef(Props(new CalibrationRegistrationTap(300F, Set(output.ref))))
      val vehicle = createVehicleMsg(detector, 300.1F, false)
      tap ! vehicle
      output.expectNoMsg(100.millis)
    }
    "filter calibration Message with other requests" in {
      val output = TestProbe()
      val requestor = TestProbe()

      val detector = 0
      val tap = TestActorRef(Props(new CalibrationRegistrationTap(300F, Set(output.ref))))

      val req = GetRegistrationRequest(refId = "lane1", detector = 1)
      requestor.send(tap, req)
      val vehicle = createVehicleMsg(detector, 300.1F, false)
      tap ! vehicle
      output.expectNoMsg(100.millis)
      requestor.expectNoMsg(100.millis)
    }
    "filter calibration Message with requests" in {
      val output = TestProbe()
      val requestor = TestProbe()

      val detector = 0
      val tap = TestActorRef(Props(new CalibrationRegistrationTap(300F, Set(output.ref))))

      val req = GetRegistrationRequest(refId = "lane0", detector = detector)
      requestor.send(tap, req)
      val vehicle = createVehicleMsg(detector, 300.1F, false)
      tap ! vehicle
      output.expectNoMsg(100.millis)
      val msg = GetRegistrationResponse(req.refId, req.detector, vehicle)
      requestor.expectMsg(msg)
    }
    "pass Message with requests" in {
      val output = TestProbe()
      val requestor = TestProbe()

      val detector = 0
      val tap = TestActorRef(Props(new CalibrationRegistrationTap(300F, Set(output.ref))))

      val req = GetRegistrationRequest(refId = "lane0", detector = detector)
      requestor.send(tap, req)
      val vehicle = createVehicleMsg(detector, 200F, true)
      tap ! vehicle
      output.expectMsg(vehicle)
      val msg = GetRegistrationResponse(req.refId, req.detector, vehicle)
      requestor.expectMsg(msg)
    }
    "pass Message with multiple requests" in {
      val output = TestProbe()
      val requestor = TestProbe()

      val detector = 0
      val tap = TestActorRef(Props(new CalibrationRegistrationTap(300F, Set(output.ref))))

      val req = GetRegistrationRequest(refId = "lane0", detector = detector)
      requestor.send(tap, req)
      val req2 = GetRegistrationRequest(refId = "lane0_2", detector = detector)
      requestor.send(tap, req2)
      val vehicle = createVehicleMsg(detector, 200F, true)
      tap ! vehicle
      output.expectMsg(vehicle)
      val msg = GetRegistrationResponse(req.refId, req.detector, vehicle)
      requestor.expectMsg(msg)
      val msg2 = GetRegistrationResponse(req2.refId, req2.detector, vehicle)
      requestor.expectMsg(msg2)
    }
    "send only one response" in {
      val output = TestProbe()
      val requestor = TestProbe()

      val detector = 0
      val tap = TestActorRef(Props(new CalibrationRegistrationTap(300F, Set(output.ref))))

      val req = GetRegistrationRequest(refId = "lane0", detector = detector)
      requestor.send(tap, req)
      val vehicle = createVehicleMsg(detector, 200F, true)
      tap ! vehicle
      output.expectMsg(vehicle)
      val msg = GetRegistrationResponse(req.refId, req.detector, vehicle)
      requestor.expectMsg(msg)

      tap ! vehicle
      output.expectMsg(vehicle)
      requestor.expectNoMsg(100.millis)
    }
  }

  def createVehicleMsg(detector: Int, speed: Float, valid: Boolean): VehicleMessage = {
    val now = System.currentTimeMillis()
    new VehicleMessage(dateTime = now,
      detector = detector, direction = Direction.Outgoing,
      speed = speed, length = 4.5F, loop1RiseTime = 0, loop1FallTime = 200,
      loop2RiseTime = 300, loop2FallTime = 400, yellowTime2 = 0, redTime2 = 0,
      dateTime1 = now - 200, yellowTime1 = 0, redTime1 = 0, dateTime3 = now + 400,
      yellowTime3 = 0, redTime3 = 0, valid = valid,
      serialNr = "SN123", offset1 = 0, offset2 = 0, offset3 = 0)
  }
}