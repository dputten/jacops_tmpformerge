/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.startup

import java.io.{ File, PrintWriter }
import java.util.Date

import akka.actor.Status.Failure
import akka.actor.{ Actor, ActorRef, ActorSystem }
import akka.camel._
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import akka.util.Timeout
import csc.akka.CamelClient
import scala.concurrent.Await
import scala.concurrent.duration._
import com.typesafe.config.ConfigFactory
import csc.sitebuffer.FileTestUtils
import csc.sitebuffer.config.SiteBufferConfiguration
import csc.sitebuffer.process.recording.StopRecordingInput
import csc.vehicle.jai.StatusCode
import org.scalatest._
import testframe.Resources
import _root_.tools.jaiconnection.SimulateJai

import scala.io.Source

/**
 * Test van StartupSiteBuffer
 */
class StartupSiteBufferTest
  extends TestKit(ActorSystem("StartupSiteBufferTest"))
  with WordSpecLike
  with MustMatchers
  with BeforeAndAfterEach
  with CamelClient
  with BeforeAndAfterAll {

  import FileTestUtils._

  val jaiSimulator: SimulateJai = new SimulateJai("localhost", 14147)

  // Create the directories needed: one for outgoing json files (= vehicle registration) and onf for outgoing anpr files.
  val resourceDir = new File(Resources.getResourceDirPath().getAbsolutePath)
  val startupResourceDir = new File(resourceDir, "csc/sitebuffer/startup")
  val testBaseDir = new File(resourceDir, "out/startsitebuffer")
  if (!testBaseDir.exists()) {
    testBaseDir.mkdirs()
  } else {
    clearDirectory(testBaseDir)
  }
  val imagesDir = new File(testBaseDir, "images")
  imagesDir.mkdir()
  val imagesDir1 = new File(imagesDir, "IM1")
  imagesDir1.mkdir()
  val imagesDir2 = new File(imagesDir, "IM2")
  imagesDir2.mkdir()
  val outImagesDir1 = new File(imagesDir, "out-IM1")
  val outImagesDir2 = new File(imagesDir, "out-IM2")
  val jsonDir = new File(testBaseDir, "json")
  jsonDir.mkdir()
  val anprDir = new File(testBaseDir, "anpr")
  anprDir.mkdir()
  val timestampDir = new File(testBaseDir, "timestamp")
  timestampDir.mkdir()
  val recordingBaseDir = new File(testBaseDir, "recording")
  recordingBaseDir.mkdir()

  // function to handle a mock time
  var mockCurrentTime: Long = _
  val dummyCurrentTime = () ⇒ mockCurrentTime

  // input (= producer) and output (= consumer + receiver) actors for this test.
  val serverMessageReceiver = TestProbe()
  val serverConsumer = TestActorRef(new TestConsumer(validMessageReceiver = serverMessageReceiver.ref))
  awaitActivation(serverConsumer, 10 seconds)
  val iRoseProducer = TestActorRef(new TestIRoseProducer())
  val imagePathProducer = TestActorRef(new TestImagePathProducer())

  override def beforeAll() {
    super.beforeAll()

    mockCurrentTime = 1360654093200L
    createTimestampFile(mockCurrentTime - 102000) // To trigger WasDown

    val config = new SiteBufferConfiguration("site-buffer", ConfigFactory.parseString(TestConfig.getConfig(jsonDir.getAbsolutePath, imagesDir.getAbsolutePath, anprDir.getAbsolutePath, timestampDir.getAbsolutePath, true, recordingBaseDir.getAbsolutePath)))
    StartupSiteBuffer.startSiteBuffer(config = config, actorSystem = system, getCurrentTime = dummyCurrentTime)
  }

  override def afterAll() {
    iRoseProducer.stop()
    awaitDeactivation(iRoseProducer, 10 seconds)
    imagePathProducer.stop()
    awaitDeactivation(imagePathProducer, 10 seconds)
    serverConsumer.stop()
    awaitDeactivation(serverConsumer, 10 seconds)
    system.shutdown()
    jaiSimulator.stopJaiCamera()
    clearDirectory(testBaseDir)
    testBaseDir.delete()
    super.afterAll()
  }

  val defaultStatuses = Map(
    StatusCode.ERROR_STATUS -> 0L,
    StatusCode.NTP_ESTIMATE_ERROR -> 0L,
    StatusCode.NTP_RESTART_COUNTER -> 0L,
    StatusCode.NTP_STATUS -> 1L)

  private def setStatuses(data: Map[Int, Long] = defaultStatuses) {
    data.foreach {
      case (statusCode, codeValue) ⇒
        jaiSimulator.updateStatus(statusCode, codeValue)
    }
  }

  "The StartupSiteBuffer object" when {

    "startSiteBuffer is called with a valid configuration" must {
      "create a working SiteBuffer" in {
        // startup should trigger WasDown event
        serverMessageReceiver.expectMsg(20 seconds, """{"sensor":"WasDown","timeStamp":1360654093200,"systemId":"","event":"OutOfRange","sendingComponent":"Sitebuffer","data":{"downStartTime":"1360653991200"}}""")

        // recording directories have been created
        val recordingDir = new File(recordingBaseDir, mockCurrentTime.toString)
        val recordingMessagesDir = new File(recordingDir, "messages")
        val recordingImagesDir = new File(recordingDir, "images")
        recordingMessagesDir.exists() must be(true)
        recordingImagesDir.exists() must be(true)

        // simulate an alarm
        mockCurrentTime = 1360654093208L
        val tooHotAlarmIRose = createMessageWithCrc("130212;072813,208;2;8;1;something;")
        iRoseProducer ! tooHotAlarmIRose
        serverMessageReceiver.expectMsg(20 seconds, """{"sensor":"Temperature","timeStamp":1360654093208,"systemId":"","event":"OutOfRange","sendingComponent":"Sitebuffer","data":{"details":"Too hot"}}""")
        new File(recordingMessagesDir, "1360654093208-2.txt").exists must be(true)

        // simulate a clock out-of-sync situation
        mockCurrentTime = 1360654093210L
        val outOfSyncMillis = (mockCurrentTime % 1000) + TestConfig.clockOutOfSyncMarginMillis + 12
        val sensorMessage = createMessageWithCrc("130212;072813,%s;0;010,500;060,000;0003,500;-004,600;005,700;".format(outOfSyncMillis))
        iRoseProducer ! sensorMessage
        serverMessageReceiver.expectMsg(20 seconds, """{"sensor":"Clock","timeStamp":1360654093210,"systemId":"","event":"Deviation","sendingComponent":"Sitebuffer","data":{}}""")
        new File(recordingMessagesDir, "1360654093210-0.txt").exists must be(true)

        // simulate a failed thermometer
        mockCurrentTime = 1360654093212L
        val inSyncMillis = (mockCurrentTime % 1000) + TestConfig.clockOutOfSyncMarginMillis - 12
        val failedThermometerMessage = createMessageWithCrc("130212;072813,%s;0;999,999;060,000;0003,500;-004,600;005,700;".format(inSyncMillis))
        iRoseProducer ! failedThermometerMessage
        serverMessageReceiver.expectMsg(20 seconds, """{"sensor":"Temperature","timeStamp":1360654093700,"systemId":"","event":"SensorFailure","sendingComponent":"Sitebuffer","data":{}}""")
        new File(recordingMessagesDir, "1360654093212-0.txt").exists must be(true)

        // simulate loop failure (triggered by the second message)
        mockCurrentTime = 1360654093300L
        val loopFailure1 = createMessageWithCrc("130212;072813,123;4;1;000;000;00000;00065;00232;00000;")
        iRoseProducer ! loopFailure1
        Thread.sleep(50) // Allow some time for recording (with current mockCurrentTime)
        mockCurrentTime = 1360654093350L
        val loopFailure2 = createMessageWithCrc("130212;072813,173;4;1;000;000;00000;00035;00132;00000;")
        iRoseProducer ! loopFailure2
        serverMessageReceiver.expectMsg(20 seconds, """{"sensor":"DetectionLoop","timeStamp":1360654093350,"systemId":"","event":"SensorFailure","sendingComponent":"Sitebuffer","data":{"loopId":"1"}}""")
        new File(recordingMessagesDir, "1360654093300-4.txt").exists must be(true)
        new File(recordingMessagesDir, "1360654093350-4.txt").exists must be(true)

        // simulate ntp failure (triggered by the second message)
        mockCurrentTime = 1360654093400L
        val ntpFailure1 = createMessageWithCrc("700101;000000,000;4;1;000;000;00000;00065;00232;00000;")
        iRoseProducer ! ntpFailure1
        Thread.sleep(50) // Allow some time for recording (with current mockCurrentTime)
        mockCurrentTime = 1360654093450L
        val ntpFailure2 = createMessageWithCrc("700101;000000,000;4;1;000;000;00000;00035;00132;00000;")
        iRoseProducer ! ntpFailure2
        serverMessageReceiver.expectMsg(20 seconds, """{"sensor":"Ntp","timeStamp":1360654093450,"systemId":"","event":"SensorFailure","sendingComponent":"Sitebuffer","data":{}}""")
        new File(recordingMessagesDir, "1360654093400-4.txt").exists must be(true)
        new File(recordingMessagesDir, "1360654093450-4.txt").exists must be(true)

        // simulate a camera reporting problems in status report
        mockCurrentTime = 1360654093500L
        // Set statuses that simulator will return
        val statuses = Map(
          StatusCode.ERROR_STATUS -> 0L,
          StatusCode.NTP_ESTIMATE_ERROR -> 0L,
          StatusCode.NTP_RESTART_COUNTER -> 0L,
          StatusCode.NTP_STATUS -> 9L)
        setStatuses(statuses)
        serverMessageReceiver.expectMsg(13 seconds, """{"sensor":"Camera","timeStamp":1360654093500,"systemId":"","event":"CameraNtpSync","sendingComponent":"Sitebuffer","data":{"cameraId":"camera1","details":" NTP_STATUS-STA_FLL"}}""")
        setStatuses() // Reset statuses the avoid errors being produced

        //simulate a valid vehicle message: must result in 1 file in a subdirectory of both the json and anpr directories.
        val detector = 1
        mockCurrentTime = 1360654093600L
        val validVehicleMessage = createMessageWithCrc("130212;072813,123;1;%d;023;500;00000;00400;00450;00875;0000,0;0000,0;130212;072813,005;0000,0;0000,0;700101;010000,000;0000,0;0000,0;0;nr123;10;20;0;".format(detector))
        iRoseProducer ! validVehicleMessage

        waitForFileInDirectory(jsonDir, 5000) must be(true)
        new File(recordingMessagesDir, "1360654093600-1-1.txt").exists must be(true)

        val expectedContent = """{"dateTime":1360654093123,"detector":1,"direction":"Direction$Outgoing","speed":23.0,"length":50.0,"loop1RiseTime":0,"loop1FallTime":450,"loop2RiseTime":400,"loop2FallTime":875,"yellowTime2":0,"redTime2":0,"dateTime1":1360654093005,"yellowTime1":0,"redTime1":0,"dateTime3":3600000,"yellowTime3":0,"redTime3":0,"valid":true,"serialNr":"nr123","offset1":10,"offset2":20,"offset3":0}d771748ed9945e3043d5cb71015ee5ad""".format(detector)

        val jsonDetectorSubdirectory = new File(jsonDir, detector.toString)
        val jsonFiles = new File(jsonDetectorSubdirectory.getAbsolutePath).listFiles()
        jsonFiles.size must be(1)
        val jsonFile = jsonFiles.head
        jsonFile.getName.startsWith("1360654093123") must be(true)
        io.Source.fromFile(jsonFile).mkString must be(expectedContent)

        val anprDetectorSubdirectory = new File(anprDir, detector.toString)
        val anprFiles = new File(anprDetectorSubdirectory.getAbsolutePath).listFiles()
        anprFiles.size must be(1)
        val anprFile = jsonFiles.head
        anprFile.getName.startsWith("1360654093123") must be(true)
        io.Source.fromFile(anprFile).mkString must be(expectedContent)

        // simulate an invalid vehicle message (-> valid = 1): now also results in a json message on disk
        mockCurrentTime = 1360654093620L
        val otherDetector = 3 // lane
        val inValidVehicleMessage = createMessageWithCrc("130212;072813,456;1;%d;023;500;00000;00400;00450;00875;0000,0;0000,0;130212;072813,005;0000,0;0000,0;700101;010000,000;0000,0;0000,0;1;".format(otherDetector))
        iRoseProducer ! inValidVehicleMessage

        waitForFileInDirectory(jsonDir, 5000, 1) must be(true)

        val jsonDetectorSubdirectoryForInvalidMessages = new File(jsonDir, otherDetector.toString)
        val jsonFilesForInvalidMessages = new File(jsonDetectorSubdirectoryForInvalidMessages.getAbsolutePath).listFiles()
        jsonFilesForInvalidMessages.size must be(1)
        val jsonFileForInvalidMessage = jsonFilesForInvalidMessages.head
        jsonFileForInvalidMessage.getName.startsWith("1360654093456") must be(true)

        val jsonContent = """{"dateTime":1360654093456,"detector":3,"direction":"Direction$Outgoing","speed":23.0,"length":50.0,"loop1RiseTime":0,"loop1FallTime":450,"loop2RiseTime":400,"loop2FallTime":875,"yellowTime2":0,"redTime2":0,"dateTime1":1360654093005,"yellowTime1":0,"redTime1":0,"dateTime3":3600000,"yellowTime3":0,"redTime3":0,"valid":false,"serialNr":"DummyValue","offset1":0,"offset2":0,"offset3":0}214d94da163ed40ae000c5b1162611c4""".format(otherDetector)

        io.Source.fromFile(jsonFileForInvalidMessage).mkString must be(jsonContent)
        new File(recordingMessagesDir, "1360654093620-1-3.txt").exists must be(true)

        // simulate a failed vehicle message (-> speed = 000): must result in a SystemSensorMessage.
        mockCurrentTime = 1360654093777L
        val numberOfJsonSubdirectoriesBeforeFailed = getNumberOfSubdirectories(jsonDetectorSubdirectory)
        val numberOfAnprSubdirectoriesBeforeFailed = getNumberOfSubdirectories(anprDetectorSubdirectory)

        val failedVehicleMessage = createMessageWithCrc("130212;072813,123;1;%d;000;500;00000;00400;00450;00875;0000,0;0000,0;130212;072813,005;0000,0;0000,0;700101;010000,000;0000,0;0000,0;1;".format(detector))
        iRoseProducer ! failedVehicleMessage
        serverMessageReceiver.expectMsg(20 seconds, """{"sensor":"Oscillator","timeStamp":1360654093777,"systemId":"","event":"SensorFailure","sendingComponent":"Sitebuffer","data":{}}""")

        getNumberOfSubdirectories(jsonDetectorSubdirectory) must be(numberOfJsonSubdirectoriesBeforeFailed)
        getNumberOfSubdirectories(anprDetectorSubdirectory) must be(numberOfAnprSubdirectoriesBeforeFailed)
        new File(recordingMessagesDir, "1360654093777-1-1.txt").exists must be(true)

        // Check update of saved timestamp (for WasDown)
        mockCurrentTime = 1360650493999L
        Thread.sleep(6000)
        filesCount(timestampDir) must be(1)
        timestampInTimestampFile must be(mockCurrentTime)

        // simulate 3 incoming images. Should all be moved and renamed.
        mockCurrentTime = 1360650494000L
        val image1 = copyTestResource("image.jpg", new File(imagesDir1, "02120130628155503.111.jpg"))
        val image2 = copyTestResource("image.jpg", new File(imagesDir2, "02120130628155503.222.jpg"))
        val image3 = copyTestResource("image.jpg", new File(imagesDir1, "02120130628155503.333.jpg"))
        image1.exists() must be(true)
        image2.exists() must be(true)
        image3.exists() must be(true)
        imagePathProducer ! image1.getAbsolutePath
        Thread.sleep(50) // Allow some time for recording (with current mockCurrentTime)
        mockCurrentTime = 1360650494010L
        imagePathProducer ! image2.getCanonicalPath
        Thread.sleep(50) // Allow some time for recording (with current mockCurrentTime)
        mockCurrentTime = 1360650494020L
        imagePathProducer ! image3.getAbsolutePath
        Thread.sleep(2000)
        image1.exists() must be(false)
        image2.exists() must be(false)
        image3.exists() must be(false)
        outImagesDir1.exists() must be(true)
        val outImage1 = new File(outImagesDir1, "02120130628155503.111-731fd6059075df227b892db80149d320.jpg")
        val outImage2 = new File(outImagesDir2, "02120130628155503.222-731fd6059075df227b892db80149d320.jpg")
        val outImage3 = new File(outImagesDir1, "02120130628155503.333-731fd6059075df227b892db80149d320.jpg")
        outImage1.exists() must be(true)
        outImage2.exists() must be(true)
        outImage3.exists() must be(true)
        val recordingImagesDir1 = new File(recordingImagesDir, "IM1")
        val recordingImagesDir2 = new File(recordingImagesDir, "IM2")
        new File(recordingImagesDir1, "1360650494000-02120130628155503.111.jpg").exists must be(true)
        new File(recordingImagesDir2, "1360650494010-02120130628155503.222.jpg").exists must be(true)
        new File(recordingImagesDir1, "1360650494020-02120130628155503.333.jpg").exists must be(true)

        // Now stop recording
        system.eventStream.publish(StopRecordingInput)
        // Save current number of files in different recording directories
        val nofRecondingMessages = filesCount(recordingMessagesDir)
        val nofRecondingImages1 = filesCount(recordingImagesDir1)
        val nofRecondingImages2 = filesCount(recordingImagesDir2)
        // Send some stuff
        mockCurrentTime = 1360650494300L
        val validVehicleMessage2 = createMessageWithCrc("130212;072813,123;1;%d;023;500;00000;00400;00450;00875;0000,0;0000,0;130212;072813,005;0000,0;0000,0;700101;010000,000;0000,0;0000,0;0;nr123;10;20;0;".format(detector))
        iRoseProducer ! validVehicleMessage2
        mockCurrentTime = 1360650494310L
        val tooHotAlarmIRose2 = createMessageWithCrc("130212;072813,208;2;8;1;something;")
        iRoseProducer ! tooHotAlarmIRose2
        mockCurrentTime = 1360650494320L
        val image4 = copyTestResource("image.jpg", new File(imagesDir1, "02120130628155503.444.jpg"))
        imagePathProducer ! image4.getAbsolutePath
        Thread.sleep(2000)
        // Nothing recorded
        filesCount(recordingMessagesDir) must be(nofRecondingMessages)
        filesCount(recordingImagesDir1) must be(nofRecondingImages1)
        filesCount(recordingImagesDir2) must be(nofRecondingImages2)
      }
    }

  }

  def createMessageWithCrc(message: String): String = {
    def calcCrc(message: String): Int = {
      message.foldLeft(0)((accu, ch) ⇒ accu ^ ch)
    }
    message + "%02X".format(calcCrc(message))
  }

  // Wait a maximum of durationMs milliseconds for a file to appear in dir.
  // Return whether file has appeared.
  def waitForFileInDirectory(dir: File, durationMs: Long, numberOfFolders: Int = 0): Boolean = {

    def waitForFileInDirectoryWithMax(maxTime: Long): Boolean = {

      val FILE_COUNT = 0 + numberOfFolders

      dir.listFiles().size match {
        case FILE_COUNT ⇒
          if (new Date().getTime > maxTime)
            false
          else
            waitForFileInDirectoryWithMax(maxTime)
        case _ ⇒ true
      }
    }
    waitForFileInDirectoryWithMax(new Date().getTime + durationMs)
  }

  def createTimestampFile(timestamp: Long) {
    val file = new File(timestampDir, "sitebufferTimestamp.txt")
    try {
      val writer = new PrintWriter(file)
      writer.write(timestamp.toString)
      writer.close()
    }
  }

  def timestampInTimestampFile: Long = {
    val file = new File(timestampDir, "sitebufferTimestamp.txt")
    if (file.exists()) {
      val lines = Source.fromFile(file).getLines().toList
      lines.size match {
        case 0 ⇒ -2L
        case 1 ⇒ lines.head.toLong
        case _ ⇒ -3L
      }
    } else {
      -1L
    }
  }

  def filesCount(dir: File): Int = {
    dir.listFiles.size
  }

  def getNumberOfSubdirectories(file: File): Int = {
    file.listFiles.filter(_.isDirectory).size
  }

  def copyTestResource(resourceName: String, toFilePath: File): File = {
    copyFile(new File(startupResourceDir, resourceName), toFilePath)
  }

}

// This Consumer simulates the behavior of the real Consumer, that will run on the server.
//    - Ack to sender when valid message received.
//    - Failure to sender otherwise
class TestConsumer(validMessageReceiver: ActorRef) extends Actor with Consumer {
  def endpointUri = "mina:tcp://localhost:16204?textline=true&sync=true"

  def receive = {
    case message: CamelMessage ⇒
      message.bodyAs[String] match {
        case text: String ⇒
          sender ! Ack
          validMessageReceiver ! text
        case _ ⇒
          throw new Exception("CamelMessage with unknown body")
      }
  }

  override def preRestart(reason: Throwable, message: Option[Any]) {
    sender ! Failure(reason)
  }
}

class TestIRoseProducer extends Actor with Producer {
  def endpointUri = "mina:tcp://localhost:12356?textline=true&sync=false"
}

class TestImagePathProducer extends Actor with Producer {
  def endpointUri = "mina:tcp://localhost:12400?textline=true&sync=false"
}

object TestConfig {
  val configTemplate = """site-buffer {
                         |    iRoseEndpoint {
                         |        host="localhost"
                         |        port=12356
                         |    }
                         |    vrEndpoint {
                         |        directory="%s"
                         |    }
                         |    vrThrottling {
                         |        inputPath = "%s"
                         |        outputPath = "/var/tmp/messages_out"
                         |        subDirectoryMask = "[0-9]+"
                         |        fileMask = ".*\\.json"
                         |        threshold = 5
                         |        interval_ms = 10000
                         |    }
                         |    imageThrottling {
                         |        inputPath = "%s"
                         |        outputPath = "/var/tmp/images_out"
                         |        subDirectoryMask = "PA[0-9]"
                         |        fileMask = ".*\\.jpg"
                         |        threshold = 30
                         |        interval_ms = 8000
                         |    }
                         |    systemSensorMessageEndpoint {
                         |        host="localhost"
                         |        port=16204
                         |    }
                         |    anprEndpoint {
                         |       directory="%s"
                         |    }
                         |    consecutiveFailedSensorReadingsCountForAlarm=1
                         |    isAliveCheckInitialDelayMs=7000
                         |    isAliveCheckFrequencyMs=25000
                         |    clockOutOfSyncMarginMs=%d
                         |    systemSensorMessageRetryDelayMs=2340
                         |    repeatingSystemSensorMessagesFilterWindowMs=10
                         |    minimalSpeedForProcessingInvalidVehicleMessageKmh=20
                         |    loopFailure {
                         |        maxAllowedMessages = 1
                         |        testWindowMs = 1000
                         |    }
                         |    # Configuration for the loop state.
                         |    loopState {
                         |        checkFrequencyMs=300000                         # Frequency with which iroses are checked.
                         |        cusStatusIRosePath="bin/cusStatusIRose.sh"      # Location of the script
                         |    }
                         |    cabinetDoor {
                         |        inputNumber=0       # 0..3: Number of the General Purpose I/O used
                         |        closedEventNumber=3
                         |        openEventNumber=4
                         |    }
                         |    # Configuration for cameras
                         |    cameras {
                         |      camera1 {
                         |        cameraHost="localhost"
                         |        cameraPort=14147
                         |        maxTriggerRetries=4
                         |        timeDisconnectedMs=23
                         |      }
                         |    }
                         |    # Configuration for the monitoring of cameras
                         |    cameraMonitor {
                         |      enabled=true
                         |      checkFrequencyMs=2000
                         |      alarmDelayMs=500
                         |      maxRetries=3
                         |      timeoutDurationMs=700
                         |    }
                         |    # Configuration for the ntp failure events.
                         |    ntpFailure {
                         |        maxAllowedMessages=1
                         |        testWindowMs=1000
                         |    }
                         |    # Configuration for processing of incoming images
                         |    imageProcessing {
                         |        newImagesEndpoint {
                         |           host="localhost"
                         |           port=12400
                         |        }
                         |        directoryPrefixForOutgoingImages="out-"
                         |        maxDeleteRetries=3         # Maximum number of retries for deleting an image.
                         |        minDelayBeforeRetryMs=200  # Minimal delay (in millis) before retry to delete an image.
                         |    }
                         |    # Configuration of WasDown monitor
                         |    wasDownMonitor {
                         |      enabled=true
                         |      timestampDirectory="%s"
                         |      timestampUpdateFrequencyInSeconds = 5
                         |      minimalDeviationForWasDownInSeconds = 100
                         |    }
                         |    # Configuration for recording Sitebuffer input (messages, images)
                         |    inputRecording {
                         |        enabled=%s               # Is inputRecording enabled?
                         |        baseDirectory="%s"       # Base directory where recordings are stored (will be created automatically)
                         |        imageFilenamesOnly=false # Only record the filenames of images
                         |    }
                         |    calibration {
                         |        iRose {
                         |            sshHost = "127.0.0.1"
                         |            sshPort = 22
                         |            sshUser = "root"
                         |            sshPwd = "XXX"
                         |            sshTimeout = 30000
                         |        }
                         |        camera {
                         |            telnetUser ="root"
                         |            telnetPwd = "XXX",
                         |            telnetPort = 23
                         |        }
                         |        NTPmaxDiff_ms= 10
                         |        ftpRootDir="/data"
                         |        calibrationImageDir="calibration"
                         |        speedThreshold=300
                         |        response {
                         |            msgRetryDelayMs=15000
                         |            centralHost="127.0.0.1"
                         |            centralport=12350
                         |        }
                         |        request {
                         |            host="127.0.0.1"
                         |            port=12300
                         |        }
                         |    }
                         |}"""

  val clockOutOfSyncMarginMillis = 500

  def getConfig(jsonDir: String, imagesDir: String, anprDir: String, timestampDir: String, recordingEnabled: Boolean, recordingDir: String): String = {

    configTemplate.stripMargin.format(jsonDir, jsonDir, imagesDir, anprDir, clockOutOfSyncMarginMillis, timestampDir, recordingEnabled, recordingDir)
  }
}