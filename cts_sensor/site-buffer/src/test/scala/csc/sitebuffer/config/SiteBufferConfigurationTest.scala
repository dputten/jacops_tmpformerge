/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.config

import scala.concurrent.duration._
import com.typesafe.config.ConfigFactory
import org.scalatest.{ MustMatchers, WordSpec }

class SiteBufferConfigurationTest extends WordSpec with MustMatchers {

  "SiteBufferConfiguration" must {
    "find settings with correct given path " in {
      val config = SiteBufferConfiguration("site-buffer")
      assert(config.isInstanceOf[SiteBufferConfiguration])
    }

    "produce an exception with incorrect given path" in {
      an[Exception] must be thrownBy { SiteBufferConfiguration("blabla") }
    }

    "correctly read settings from config file(s) " in {
      val config = new SiteBufferConfiguration("site-buffer", ConfigFactory.load("test.conf"))

      assert(config.isInstanceOf[SiteBufferConfiguration])
      assert(config.iRoseEndpoint.host == "127.0.0.1")
      assert(config.iRoseEndpoint.port == 12321)
      assert(config.vrEndpoint.directory == "/var/tmp/voertuigpassage")
      assert(config.anprEndpoint.directory == "/var/tmp/anpr")
      assert(config.systemSensorMessageEndpoint.host == "hostie")
      assert(config.systemSensorMessageEndpoint.port == 1234)
      assert(config.consecutiveFailedSensorReadingsCountForAlarm == 7)
      assert(config.isAliveCheck.isAliveCheckInitialDelayMs == 7000)
      assert(config.isAliveCheck.isAliveCheckFrequencyMs == 25000)
      assert(config.clockOutOfSyncMarginMs == 550)
      assert(config.systemSensorMessageRetryDelayMs == 2340)
      assert(config.repeatingSystemSensorMessagesFilterWindowMs == 7667)
      assert(config.minimalSpeedForProcessingInvalidVehicleMessageKmh == 23)
      assert(config.loopFailure.maxAllowedMessages == 3)
      assert(config.loopFailure.testWindowMs == 12345)
      assert(config.cabinetDoorConfig.inputNumber == 0)
      assert(config.cabinetDoorConfig.closedEventNumber == 3)
      assert(config.cabinetDoorConfig.openEventNumber == 4)
      assert(config.cameraConfigs("camera1").cameraHost == "127.0.0.1")
      config.cameraConfigs("camera1").cameraId must be("camera1")
      assert(config.cameraConfigs("camera1").cameraPort == 12345)
      assert(config.cameraConfigs("camera1").maxTriggerRetries == 4)
      assert(config.cameraConfigs("camera1").timeDisconnectedMs == 23)
      assert(config.cameraConfigs("camera1").rebootScript == "rebootCamera1.sh")
      assert(config.cameraConfigs("camera1").detectors == Seq(0, 1))
      assert(config.cameraConfigs("camera1").imageDir == "A0")
      assert(config.cameraConfigs("camera1").maxWaitForImage == 60000)
      assert(config.cameraConfigs("camera1").maxWaitForReboot == 600000)
      assert(config.cameraConfigs("camera1").waitForFixedMsg == 1800000)
      assert(config.cameraConfigs("camera2").cameraId == "camera2")
      assert(config.cameraConfigs("camera2").cameraHost == "127.0.0.2")
      assert(config.cameraConfigs("camera2").cameraPort == 67890)
      assert(config.cameraConfigs("camera2").maxTriggerRetries == 2)
      assert(config.cameraConfigs("camera2").timeDisconnectedMs == 11)
      assert(config.cameraConfigs("camera2").rebootScript == "rebootCamera2.sh")
      assert(config.cameraConfigs("camera2").detectors == Seq(2))
      assert(config.cameraConfigs("camera2").imageDir == "A1")
      assert(config.cameraConfigs("camera2").maxWaitForImage == 60000)
      assert(config.cameraConfigs("camera2").maxWaitForReboot == 600000)
      assert(config.cameraConfigs("camera2").waitForFixedMsg == 1800000)
      assert(config.cameraMonitorConfig.enabled == false)
      assert(config.cameraMonitorConfig.checkFrequency == 76431.millis)
      assert(config.cameraMonitorConfig.alarmDelay == 456.millis)
      assert(config.cameraMonitorConfig.maxRetries == 37)
      assert(config.cameraMonitorConfig.timeoutDuration == 545.millis)
      assert(config.ntpFailure.maxAllowedMessages == 23)
      assert(config.ntpFailure.testWindowMs == 7654)
      assert(config.wasDown.enabled == true)
      assert(config.wasDown.timestampDirectory == "this/is/a/directory")
      assert(config.wasDown.timestampUpdateFrequency == 54.seconds)
      assert(config.wasDown.minimalDeviationForWasDown == 123.seconds)
      assert(config.imageProcessing.newImagesEndpoint.host == "127.1.2.3")
      assert(config.imageProcessing.newImagesEndpoint.port == 12789)
      assert(config.imageProcessing.directoryPrefixForOutgoingImages == "OUT")
      assert(config.imageProcessing.maxDeleteRetries == 32)
      assert(config.imageProcessing.minDelayBeforeRetryMs == 323)
      assert(config.inputRecording.enabled == true)
      assert(config.inputRecording.baseDirectory == "this/is/a/directory")
      assert(config.inputRecording.imageFilenamesOnly == true)
      assert(config.imageFilter.enable == true)
      assert(config.imageFilter.imageExtension == "tif")
      assert(config.imageFilter.maxDelayTime == 1.second)
    }
  }
}
