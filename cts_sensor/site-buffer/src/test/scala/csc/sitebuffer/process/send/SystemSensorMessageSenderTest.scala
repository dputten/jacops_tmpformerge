/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.send

import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import scala.concurrent.duration._
import csc.sitebuffer.process.SystemSensorMessage
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, WordSpec }

/**
 * Test van SystemSensorMessageSender
 */
class SystemSensorMessageSenderTest extends TestKit(ActorSystem("SystemSensorMessageSenderTest")) with WordSpecLike with MustMatchers
  with BeforeAndAfterAll {

  var testProbe: TestProbe = TestProbe()
  // This sender actor will send all outgoing messages to testProbe
  var systemSensorMessageSenderRef: TestActorRef[SystemSensorMessageSender] = TestActorRef(new SystemSensorMessageSender((text: String) ⇒ (testProbe.ref ! text)))

  override def beforeAll() {
    super.beforeAll()
  }

  override def afterAll() {
    system.shutdown
  }

  "A SystemSensorMessageSender actor" when {

    "receiving a SystemSensorMessage" must {
      "call the send function passing the JSON string for the received message" in {
        // create message
        val message = SystemSensorMessage("sensor", 123456789L, "SystemID", "Bloep", "Component", Map())
        // construct expected message
        val expectedMessage = """{"sensor":"sensor","timeStamp":123456789,"systemId":"SystemID","event":"Bloep","sendingComponent":"Component","data":{}}"""
        // send message
        systemSensorMessageSenderRef ! message
        // test for expected result
        testProbe.expectMsg(1000 millis, expectedMessage)
      }
    }

    "receiving a message other than SystemSensorMessage" must {
      "ignore this message" in {
        // create message
        val message = "Some kind of non SystemSensorMessage"
        // send message
        systemSensorMessageSenderRef ! message
        // test if expected message is received
        testProbe.expectNoMsg(1000 millis)
      }
    }

  }
}
