/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.irose

import org.scalatest.WordSpec
import org.scalatest._

/**
 * Test van IRoseString
 */
class IRoseStringTest extends WordSpec with MustMatchers {

  "Calling typeChar on an IRoseString object" when {

    "contained string is a valid iRose message" must {
      "return Some(<type char>)" in {
        IRoseString(string = "130212;072813,123;0;1;023;500;00000;00400;00450;00875;0000,0;0000,0", timestamp = 123456).typeChar must be(Some('0'))
        IRoseString(string = "130212;072813,123;1;1;023;500;00000;00400;00450;00875;0000,0;0000,0", timestamp = 123456).typeChar must be(Some('1'))
        IRoseString(string = "130212;072813,123;2;1;023;500;00000;00400;00450;00875;0000,0;0000,0", timestamp = 123456).typeChar must be(Some('2'))
        IRoseString(string = "130212;072813,123;3;1;023;500;00000;00400;00450;00875;0000,0;0000,0", timestamp = 123456).typeChar must be(Some('3'))
      }
    }

    "contained string is empty" must {
      "return None" in {
        IRoseString(string = "", timestamp = 123456).typeChar must be(None)
      }
    }

  }

  "Calling detectorChar on an IRoseString object" when {

    "contained string is a valid iRose vehicle message (type = 1)" must {
      "return Some(<type char>)" in {
        IRoseString(string = "130212;072813,123;1;0;023;500;00000;00400;00450;00875;0000,0;0000,0", timestamp = 123456).detectorChar must be(Some('0'))
        IRoseString(string = "130212;072813,123;1;1;023;500;00000;00400;00450;00875;0000,0;0000,0", timestamp = 123456).detectorChar must be(Some('1'))
        IRoseString(string = "130212;072813,123;1;2;023;500;00000;00400;00450;00875;0000,0;0000,0", timestamp = 123456).detectorChar must be(Some('2'))
        IRoseString(string = "130212;072813,123;1;3;023;500;00000;00400;00450;00875;0000,0;0000,0", timestamp = 123456).detectorChar must be(Some('3'))
      }
    }

    "contained string is a valid iRose message, but not a vehicle message (type <> 1)" must {
      "return None" in {
        IRoseString(string = "130212;072813,123;0;5;023;500;00000;00400;00450;00875;0000,0;0000,0", timestamp = 123456).detectorChar must be(None)
        IRoseString(string = "130212;072813,123;2;5;023;500;00000;00400;00450;00875;0000,0;0000,0", timestamp = 123456).detectorChar must be(None)
        IRoseString(string = "130212;072813,123;3;5;023;500;00000;00400;00450;00875;0000,0;0000,0", timestamp = 123456).detectorChar must be(None)
        IRoseString(string = "130212;072813,123;4;5;023;500;00000;00400;00450;00875;0000,0;0000,0", timestamp = 123456).detectorChar must be(None)
      }
    }

    "contained string is empty" must {
      "return None" in {
        IRoseString(string = "", timestamp = 123456).detectorChar must be(None)
      }
    }

  }
}
