/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.images

import java.io.File

import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit }
import csc.sitebuffer.FileTestUtils
import csc.sitebuffer.process.images.ImageDeleter.{ DeleteImage, DeleteImageNextAttempt }
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }
import testframe.Resources

/**
 * Test van ImageDeleter
 */
class ImageDeleterTest extends TestKit(ActorSystem("ImageDeleterTest")) with WordSpecLike with MustMatchers with BeforeAndAfterEach with BeforeAndAfterAll {
  import FileTestUtils._

  var imageDeleter: TestActorRef[ImageDeleter] = _

  var failDelete = false

  val resourceDir = new File(Resources.getResourceDirPath().getAbsolutePath)
  val imageDeleterResourceDir = new File(resourceDir, "csc/sitebuffer/process/images/imageDeleter")
  val testBaseDir = new File(resourceDir, "out/imageDeleter")
  if (!testBaseDir.exists()) {
    testBaseDir.mkdirs()
  } else {
    clearDirectory(testBaseDir)
  }

  override def beforeAll() {
    super.beforeAll()
  }

  override def afterAll() {
    system.shutdown()

    clearDirectory(testBaseDir)
    testBaseDir.delete()
  }

  override def afterEach() {
    clearDirectory(testBaseDir)
  }

  def createTestActor(maxDeleteRetries: Int): TestActorRef[ImageDeleter] = {
    TestActorRef(new ImageDeleter(maxDeleteRetries = maxDeleteRetries, minDelayBeforeRetryMs = 200) with FileDelete {
      // Make it possible to mock a delete failure
      override def deleteFile(file: File): Either[String, Boolean] = {
        if (failDelete) {
          Left("Mock delete failure")
        } else {
          super.deleteFile(file)
        }
      }
    })
  }

  "An ImageDeleter actor" when {
    "receiving a DeleteImage message (and image is not locked)" must {
      "delete that image" in {
        // Prepare input
        val inputDir = new File(testBaseDir, "input")
        inputDir.mkdir
        val testFile = copyTestResource("image.jpg", new File(inputDir, "02120130628155503.347.jpg"))
        failDelete = false
        // Create test actor
        imageDeleter = createTestActor(maxDeleteRetries = 2)
        // Send test message
        val imagePath = testFile.getCanonicalPath
        imageDeleter ! DeleteImage(imagePath)
        // Check expected results
        testFile.exists() must be(false)
      }
    }

    "receiving a DeleteImage message (and image is temporarily locked)" must {
      "keep sending DeleteImageNextAttempt's to itself until delete succeeds" in {
        // Prepare input
        val inputDir = new File(testBaseDir, "input")
        inputDir.mkdir
        val testFile = copyTestResource("image.jpg", new File(inputDir, "02120130628155503.347.jpg"))
        failDelete = true
        // Create test actor
        imageDeleter = createTestActor(maxDeleteRetries = 9)
        // Send test message
        val imagePath = testFile.getCanonicalPath
        imageDeleter ! DeleteImage(imagePath)
        // Check expected results
        testFile.exists() must be(true)
        Thread.sleep(1000) // Allow for several attempts
        failDelete = false
        Thread.sleep(1000) // Allow for delete to occur
        testFile.exists() must be(false)
      }
    }

    "receiving a DeleteImage message (and image stays locked)" must {
      "keep sending DeleteImageNextAttempt's to itself until maxDeleteRetries is reached" in {
        // Prepare input
        val inputDir = new File(testBaseDir, "input")
        inputDir.mkdir
        val testFile = copyTestResource("image.jpg", new File(inputDir, "02120130628155503.347.jpg"))
        failDelete = true
        // Create test actor
        imageDeleter = createTestActor(maxDeleteRetries = 9)
        // Send test message
        val imagePath = testFile.getCanonicalPath
        imageDeleter ! DeleteImage(imagePath)
        // Check expected results
        testFile.exists() must be(true)
        Thread.sleep(4000) // Allow for enough attempts
        failDelete = false
        Thread.sleep(1000)
        testFile.exists() must be(true) // file should still be there
      }
    }

    "receiving a DeleteImage message and image that does not exist" must {
      "do nothing" in {
        // Prepare input
        val inputDir = new File(testBaseDir, "input")
        inputDir.mkdir
        val testFile = new File(inputDir, "02120130628155503.347.jpg")
        failDelete = false
        // Create test actor
        imageDeleter = createTestActor(maxDeleteRetries = 2)
        // Send test message
        val imagePath = testFile.getCanonicalPath
        imageDeleter ! DeleteImage(imagePath)
        // Check expected results
        testFile.exists() must be(false)
      }
    }

    "receiving a DeleteImageNextAttempt message and image that does not exist" must {
      "do nothing" in {
        // Prepare input
        val inputDir = new File(testBaseDir, "input")
        inputDir.mkdir
        val testFile = new File(inputDir, "02120130628155503.347.jpg")
        failDelete = false
        // Create test actor
        imageDeleter = createTestActor(maxDeleteRetries = 9)
        // Send test message
        val imagePath = testFile.getCanonicalPath
        imageDeleter ! DeleteImageNextAttempt(imagePath, 5)
        // Check expected results
        testFile.exists() must be(false)
      }
    }

  }

  def copyTestResource(resourceName: String, toFilePath: File): File = {
    copyFile(new File(imageDeleterResourceDir, resourceName), toFilePath)
  }

}

