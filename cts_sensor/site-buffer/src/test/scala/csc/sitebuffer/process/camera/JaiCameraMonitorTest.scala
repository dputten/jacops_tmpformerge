/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.camera

import java.util.Date

import _root_.tools.jaiconnection.SimulateJai
import akka.actor.{ ActorRef, ActorSystem, Props }
import akka.testkit.{ TestKit, TestProbe }
import scala.concurrent.duration._
import com.typesafe.config.ConfigFactory
import csc.sitebuffer.config.{ CameraConfig, CameraMonitorConfig }
import csc.sitebuffer.process.SystemSensorMessage
import csc.sitebuffer.process.camera.JaiCameraMonitor.CheckStatusRequest
import csc.vehicle.jai.{ CameraError, DisturbanceEvent, _ }
import org.scalatest._

// PLEASE NOTE: This test may sometimes raise a TimeoutException whilst run. My advise: try again!

/**
 * Test van JaiCameraMonitor
 */
import csc.sitebuffer.process.camera.JaiCameraMonitorTest._ // See bottom of file. Defines testSystem with custom configuration.

class JaiCameraMonitorTest extends TestKit(testSystem) with WordSpecLike with MustMatchers
  with BeforeAndAfterEach with BeforeAndAfterAll {

  val cameraId = "camera-0"
  val cameraHost = "localhost"
  val cameraPort = 14142

  var systemSensorMessageSenderProbe: TestProbe = TestProbe()
  var jaiCameraMonitor: ActorRef = _

  var currentTime: Long = _
  val dummyCurrentTime = () ⇒ currentTime

  /**
   * The next <numberOfExceptions> calls to the jaiConnectionSendCameraStatus method will
   * result in an exception. Exception type is determined by <exceptionType>
   */
  var exceptionCounter: Int = 0
  var exceptionType: TestJaiConnectionExceptionType.Value = TestJaiConnectionExceptionType.ConnectionIllegalStateException
  def setExceptionMode(newExceptionType: TestJaiConnectionExceptionType.Value, numberOfExceptions: Int) {
    exceptionType = newExceptionType
    exceptionCounter = numberOfExceptions
  }

  val jaiSimulator: SimulateJai = new SimulateJai(cameraHost, cameraPort)

  val defaultCameraConfig = CameraConfig(
    cameraId = cameraId,
    cameraHost = cameraHost,
    cameraPort = cameraPort,
    maxTriggerRetries = 3,
    timeDisconnectedMs = 1000,
    rebootScript = "reboot.sh",
    detectors = Seq(0, 1),
    imageDir = "A0",
    maxWaitForImage = 60000,
    maxWaitForReboot = 600000,
    waitForFixedMsg = 1800000)
  val defaultCameraMonitorConfig = CameraMonitorConfig(
    enabled = true,
    checkFrequency = 600000.seconds, // Extra large so it won't be triggered during normal test
    alarmDelay = 50.millis,
    maxRetries = 3,
    timeoutDuration = 200.millis)

  override def beforeAll() {
    super.beforeAll()
  }

  override def afterAll() {
    system.shutdown()
    jaiSimulator.stopJaiCamera()
    super.afterAll()
  }

  override def beforeEach() {
    setStatuses() // Default = no problems with camera
  }

  override def afterEach() {
    system.stop(jaiCameraMonitor)
  }

  val defaultStatuses = Map(
    StatusCode.ERROR_STATUS -> 0L,
    StatusCode.NTP_ESTIMATE_ERROR -> 0L,
    StatusCode.NTP_RESTART_COUNTER -> 0L,
    StatusCode.NTP_STATUS -> 1L)

  private def setStatuses(data: Map[Int, Long] = defaultStatuses) {
    data.foreach {
      case (statusCode, codeValue) ⇒
        jaiSimulator.updateStatus(statusCode, codeValue)
    }
  }

  private def createJaiCameraMonitor(
    aCameraConfig: CameraConfig = defaultCameraConfig,
    aCameraMonitorConfig: CameraMonitorConfig = defaultCameraMonitorConfig) {
    jaiCameraMonitor = system.actorOf(Props(
      new JaiCameraMonitor(
        cameraId = "camera-0",
        cameraMonitorConfig = aCameraMonitorConfig,
        getCurrentTime = dummyCurrentTime,
        systemSensorMessageSender = systemSensorMessageSenderProbe.ref) with TestJaiConnectionProvider {
        def createJaiConnection: Either[String, String] = {
          jaiConnection = new JaiCameraConnection(laneId = cameraId,
            host = aCameraConfig.cameraHost, port = aCameraConfig.cameraPort,
            maxTriggerRetries = aCameraConfig.maxTriggerRetries, timeDisconnected = aCameraConfig.timeDisconnectedMs, cameraTriggerId = 0xFF,
            recipients = Set(self))
          Right("Done")
        }
      }))
    Thread.sleep(2000) //Connection between simulator and monitor has to be established
  }

  "A JaiCameraMonitor actor, receiving a CheckStatusRequest," when {

    "there are no problems with the camera" must {
      "ask the camera for status data and wait for a CameraStatus reply and do nothing" in {
        currentTime = 4796
        // Create camera
        createJaiCameraMonitor()
        // Set statuses that simulator will return
        setStatuses()
        // Send test message
        jaiCameraMonitor ! CheckStatusRequest
        // Check expected result
        systemSensorMessageSenderProbe.expectNoMsg(10 seconds)
      }
    }

    "the camera has problems" must {
      "ask the camera for status data and wait for a CameraStatus reply and send a SystemSensorMessage for each error contained" in {
        currentTime = 4796
        // Create camera
        createJaiCameraMonitor()
        // Set statuses that simulator will return
        val statuses = Map(
          StatusCode.ERROR_STATUS -> 2L,
          StatusCode.NTP_ESTIMATE_ERROR -> 0L,
          StatusCode.NTP_RESTART_COUNTER -> 0L,
          StatusCode.NTP_STATUS -> 9L)
        setStatuses(statuses)
        // Send test message
        jaiCameraMonitor ! CheckStatusRequest
        // Check expected result
        val expectedMsg1 = SystemSensorMessage(
          sensor = "Camera",
          timeStamp = 4796,
          systemId = "",
          event = "CameraFtp",
          sendingComponent = "Sitebuffer",
          data = Map("cameraId" -> cameraId, "details" -> "SERIAL232"))
        val expectedMsg2 = SystemSensorMessage(
          sensor = "Camera",
          timeStamp = 4796,
          systemId = "",
          event = "CameraNtpSync",
          sendingComponent = "Sitebuffer",
          data = Map("cameraId" -> cameraId, "details" -> " NTP_STATUS-STA_FLL"))
        systemSensorMessageSenderProbe.expectMsg(5 seconds, expectedMsg1)
        systemSensorMessageSenderProbe.expectMsg(5 seconds, expectedMsg2)
      }
    }

    "there are problems connecting to the camera" must {
      "keep asking the camera for status data until a CameraStatus is received and then send a SystemSensorMessage for each error contained" in {
        currentTime = 4796
        // Create camera
        createJaiCameraMonitor()
        setExceptionMode(TestJaiConnectionExceptionType.ConnectionIllegalStateException, 2) // next 2 calls will raise connection exception
        // Set statuses that simulator will return
        val statuses = Map(
          StatusCode.ERROR_STATUS -> 2L,
          StatusCode.NTP_ESTIMATE_ERROR -> 0L,
          StatusCode.NTP_RESTART_COUNTER -> 0L,
          StatusCode.NTP_STATUS -> 9L)
        setStatuses(statuses)
        // Send test message
        jaiCameraMonitor ! CheckStatusRequest
        // Check expected result
        val expectedMsg1 = SystemSensorMessage(
          sensor = "Camera",
          timeStamp = 4796,
          systemId = "",
          event = "CameraFtp",
          sendingComponent = "Sitebuffer",
          data = Map("cameraId" -> cameraId, "details" -> "SERIAL232"))
        val expectedMsg2 = SystemSensorMessage(
          sensor = "Camera",
          timeStamp = 4796,
          systemId = "",
          event = "CameraNtpSync",
          sendingComponent = "Sitebuffer",
          data = Map("cameraId" -> cameraId, "details" -> " NTP_STATUS-STA_FLL"))
        systemSensorMessageSenderProbe.expectMsg(5 seconds, expectedMsg1)
        systemSensorMessageSenderProbe.expectMsg(5 seconds, expectedMsg2)
      }
    }

    "there are problems connecting to the camera (even after maximum retries)" must {
      "in the end send a CameraConnection SystemSensorMessage" in {
        currentTime = 4796
        // Create camera
        createJaiCameraMonitor()
        setExceptionMode(TestJaiConnectionExceptionType.ConnectionIllegalStateException, 4) // larger than maximum retries!
        // Set statuses that simulator will return
        val statuses = Map(
          StatusCode.ERROR_STATUS -> 2L,
          StatusCode.NTP_ESTIMATE_ERROR -> 0L,
          StatusCode.NTP_RESTART_COUNTER -> 0L,
          StatusCode.NTP_STATUS -> 9L)
        setStatuses(statuses)
        // Send test message
        jaiCameraMonitor ! CheckStatusRequest
        // Check expected result
        val expectedMsg = SystemSensorMessage(
          sensor = "Camera",
          timeStamp = 4796,
          systemId = "",
          event = "CameraConnection",
          sendingComponent = "Sitebuffer",
          data = Map("cameraId" -> cameraId, "details" -> "All 3 attempts to connect to camera camera-0 failed"))
        systemSensorMessageSenderProbe.expectMsg(5 seconds, expectedMsg)
      }
    }

    "asking for the camera status returns a (not connection related) IllegalStateException" must {
      "immediately send an InterfaceError SystemSensorMessage" in {
        currentTime = 4796
        // Create camera
        createJaiCameraMonitor()
        setExceptionMode(TestJaiConnectionExceptionType.OtherIllegalStateException, 1) // One is enough!
        // Set statuses that simulator will return
        val statuses = Map(
          StatusCode.ERROR_STATUS -> 2L,
          StatusCode.NTP_ESTIMATE_ERROR -> 0L,
          StatusCode.NTP_RESTART_COUNTER -> 0L,
          StatusCode.NTP_STATUS -> 9L)
        setStatuses(statuses)
        // Send test message
        jaiCameraMonitor ! CheckStatusRequest
        // Check expected result
        val expectedMsg = SystemSensorMessage(
          sensor = "Camera",
          timeStamp = 4796,
          systemId = "",
          event = "CameraInterfaceError",
          sendingComponent = "Sitebuffer",
          data = Map("cameraId" -> cameraId, "details" -> "Another error message"))
        systemSensorMessageSenderProbe.expectMsg(5 seconds, expectedMsg)
      }
    }

    "asking for the camera status returns an Exception" must {
      "immediately send an InterfaceError SystemSensorMessage" in {
        currentTime = 4796
        // Create camera
        createJaiCameraMonitor()
        setExceptionMode(TestJaiConnectionExceptionType.OtherException, 1) // One is enough!
        // Set statuses that simulator will return
        val statuses = Map(
          StatusCode.ERROR_STATUS -> 2L,
          StatusCode.NTP_ESTIMATE_ERROR -> 0L,
          StatusCode.NTP_RESTART_COUNTER -> 0L,
          StatusCode.NTP_STATUS -> 9L)
        setStatuses(statuses)
        // Send test message
        jaiCameraMonitor ! CheckStatusRequest
        // Check expected result
        val expectedMsg = SystemSensorMessage(
          sensor = "Camera",
          timeStamp = 4796,
          systemId = "",
          event = "CameraInterfaceError",
          sendingComponent = "Sitebuffer",
          data = Map("cameraId" -> cameraId, "details" -> "Any error message"))
        systemSensorMessageSenderProbe.expectMsg(5 seconds, expectedMsg)
      }
    }

    "multiple requests within the given timeoutDuration" must {
      "process only one CameraStatus and send a SystemSensorMessage for each error contained" in {
        currentTime = 4796
        // Create camera
        createJaiCameraMonitor()
        setExceptionMode(TestJaiConnectionExceptionType.ConnectionIllegalStateException, 0)
        // Set statuses that simulator will return
        val statuses = Map(
          StatusCode.ERROR_STATUS -> 2L,
          StatusCode.NTP_ESTIMATE_ERROR -> 0L,
          StatusCode.NTP_RESTART_COUNTER -> 0L,
          StatusCode.NTP_STATUS -> 9L)
        setStatuses(statuses)
        // Send test message
        jaiCameraMonitor ! CheckStatusRequest
        jaiCameraMonitor ! CheckStatusRequest
        // Check expected result
        val expectedMsg1 = SystemSensorMessage(
          sensor = "Camera",
          timeStamp = 4796,
          systemId = "",
          event = "CameraFtp",
          sendingComponent = "Sitebuffer",
          data = Map("cameraId" -> cameraId, "details" -> "SERIAL232"))
        val expectedMsg2 = SystemSensorMessage(
          sensor = "Camera",
          timeStamp = 4796,
          systemId = "",
          event = "CameraNtpSync",
          sendingComponent = "Sitebuffer",
          data = Map("cameraId" -> cameraId, "details" -> " NTP_STATUS-STA_FLL"))
        systemSensorMessageSenderProbe.expectMsg(5 seconds, expectedMsg1)
        systemSensorMessageSenderProbe.expectMsg(5 seconds, expectedMsg2)
        systemSensorMessageSenderProbe.expectNoMsg(10 seconds)
      }
    }

  }

  "A JaiCameraMonitor actor, receiving a DisturbanceEvent," when {

    "DisturbanceState.DETECTED and alarmDelay = 0" must {
      "immediately send a corresponding SystemSensorMessage" in {
        currentTime = 4796
        // Create camera
        val config = defaultCameraMonitorConfig.copy(alarmDelay = 0.millis)

        createJaiCameraMonitor(aCameraMonitorConfig = config)
        // Send test message
        val timestamp = new Date(5678L)
        val event = new DisturbanceEvent(
          laneId = "dummy",
          timestamp = timestamp,
          eventType = CameraError(eventType = DisturbanceTypes.CAMERA_COMM, details = Some("IO_BOARD")),
          state = DisturbanceState.DETECTED,
          reason = "This will be ignored")
        jaiCameraMonitor ! event
        // Check expected result
        val expectedMsg = SystemSensorMessage(
          sensor = "Camera",
          timeStamp = 5678,
          systemId = "",
          event = "CameraFtp",
          sendingComponent = "Sitebuffer",
          data = Map("cameraId" -> cameraId, "details" -> "IO_BOARD"))
        systemSensorMessageSenderProbe.expectMsg(1 seconds, expectedMsg)
      }
    }

    "DisturbanceState.SOLVED (without earlier DETECTED) and alarmDelay = 0" must {
      "do nothing" in {
        currentTime = 4796
        // Create camera
        val config = defaultCameraMonitorConfig.copy(alarmDelay = 0.millis)

        createJaiCameraMonitor(aCameraMonitorConfig = config)
        // Send test message
        val timestamp = new Date(5678L)
        val event = new DisturbanceEvent(
          laneId = "dummy",
          timestamp = timestamp,
          eventType = CameraError(eventType = DisturbanceTypes.CAMERA_COMM, details = Some("IO_BOARD")),
          state = DisturbanceState.SOLVED,
          reason = "This will be ignored")
        jaiCameraMonitor ! event
        // Check expected result
        systemSensorMessageSenderProbe.expectNoMsg(10 seconds)
      }
    }

    "DisturbanceState.DETECTED and event type in question is not solved within alarmDelay milliseconds" must {
      "send a corresponding SystemSensorMessage" in {
        currentTime = 4796
        // Create camera
        val config = defaultCameraMonitorConfig.copy(alarmDelay = 1.second)

        createJaiCameraMonitor(aCameraMonitorConfig = config)
        // Send test message
        val timestamp = new Date(5678L)
        val event = new DisturbanceEvent(
          laneId = "dummy",
          timestamp = timestamp,
          eventType = CameraError(eventType = DisturbanceTypes.CAMERA_COMM, details = Some("IO_BOARD")),
          state = DisturbanceState.DETECTED,
          reason = "This will be ignored")
        jaiCameraMonitor ! event
        // Check expected result
        val expectedMsg = SystemSensorMessage(
          sensor = "Camera",
          timeStamp = 5678,
          systemId = "",
          event = "CameraFtp",
          sendingComponent = "Sitebuffer",
          data = Map("cameraId" -> cameraId, "details" -> "IO_BOARD"))
        systemSensorMessageSenderProbe.expectMsg(2 seconds, expectedMsg)
      }
    }

    "DisturbanceState.DETECTED and event type in question is solved within alarmDelay milliseconds" must {
      "do nothing" in {
        currentTime = 4796
        // Create camera
        val config = defaultCameraMonitorConfig.copy(alarmDelay = 1.second)

        createJaiCameraMonitor(aCameraMonitorConfig = config)
        // Send test message
        val timestamp = new Date(5678L)
        val detectedEvent = new DisturbanceEvent(
          laneId = "dummy",
          timestamp = timestamp,
          eventType = CameraError(eventType = DisturbanceTypes.CAMERA_COMM, details = Some("IO_BOARD")),
          state = DisturbanceState.DETECTED,
          reason = "This will be ignored")
        jaiCameraMonitor ! detectedEvent
        val solvedEvent = new DisturbanceEvent(
          laneId = "dummy",
          timestamp = timestamp,
          eventType = CameraError(eventType = DisturbanceTypes.CAMERA_COMM, details = Some("IO_BOARD")),
          state = DisturbanceState.SOLVED,
          reason = "This will be ignored")
        jaiCameraMonitor ! solvedEvent
        // Check expected result
        systemSensorMessageSenderProbe.expectNoMsg(10 seconds)
      }
    }

    "DisturbanceState.DETECTED and event type in question is solved beyond alarmDelay milliseconds" must {
      "send a corresponding SystemSensorMessage" in {
        currentTime = 4796
        // Create camera
        val config = defaultCameraMonitorConfig.copy(alarmDelay = 1.second)

        createJaiCameraMonitor(aCameraMonitorConfig = config)
        // Send test message
        val timestamp1 = new Date(5678L)
        val detectedEvent = new DisturbanceEvent(
          laneId = "dummy",
          timestamp = timestamp1,
          eventType = CameraError(eventType = DisturbanceTypes.CAMERA_COMM, details = Some("IO_BOARD")),
          state = DisturbanceState.DETECTED,
          reason = "This will be ignored")
        jaiCameraMonitor ! detectedEvent
        Thread.sleep(2000)
        val timestamp2 = new Date(6677L)
        val solvedEvent = new DisturbanceEvent(
          laneId = "dummy",
          timestamp = timestamp2,
          eventType = CameraError(eventType = DisturbanceTypes.CAMERA_COMM, details = Some("IO_BOARD")),
          state = DisturbanceState.SOLVED,
          reason = "This will be ignored")
        jaiCameraMonitor ! solvedEvent
        // Check expected result
        val expectedMsg = SystemSensorMessage(
          sensor = "Camera",
          timeStamp = 5678,
          systemId = "",
          event = "CameraFtp",
          sendingComponent = "Sitebuffer",
          data = Map("cameraId" -> cameraId, "details" -> "IO_BOARD"))
        systemSensorMessageSenderProbe.expectMsg(2 seconds, expectedMsg)
      }
    }

  }

  /**
   * Special test version of the JaiConnectionProvider. This one simulates exceptions thrown whilst calling jaiConnectionSendCameraStatus.
   * The number of consecutive calls that will throw an exception is determined by the <exceptionCounter>
   * <exceptionType> determines the type of Exception thrown.
   *
   * Use the setExceptionMode method to change these settings.
   */
  trait TestJaiConnectionProvider extends JaiConnection {
    var jaiConnection: JaiCameraConnection = _

    def createJaiConnection: Either[String, String]

    def jaiConnectionStart: Either[String, String] = {
      jaiConnection.start()
      Right("Done")
    }

    def jaiConnectionShutdown: Either[String, String] = {
      jaiConnection.shutdown()
      Right("Done")
    }

    def jaiConnectionSendCameraStatus: Either[String, Boolean] = {
      if (exceptionCounter == 0) {
        jaiConnection.sendCameraStatus()
        Right(true)
      } else {
        exceptionCounter -= 1
        exceptionType match {
          case TestJaiConnectionExceptionType.ConnectionIllegalStateException ⇒
            println("jaiConnectionSendCameraStatus throws connection related IllegalStateException")
            Right(false)
          case TestJaiConnectionExceptionType.OtherIllegalStateException ⇒
            println("jaiConnectionSendCameraStatus throws IllegalStateException")
            Left("Another error message")
          case TestJaiConnectionExceptionType.OtherException ⇒
            println("jaiConnectionSendCameraStatus throws Exception")
            Left("Any error message")
        }
      }
    }

  }

  /**
   * Defines which kind of exception the jaiConnectionSendCameraStatus will throw
   */
  object TestJaiConnectionExceptionType extends Enumeration {
    val ConnectionIllegalStateException = Value("ConnectionIllegalStateException")
    val OtherIllegalStateException = Value("OtherIllegalStateException")
    val OtherException = Value("OtherException")
  }

}

object JaiCameraMonitorTest {
  val testSystem = {
    val customConfig = ConfigFactory.parseString("""akka.actor.creation-timeout = 30s""") // Trying to avoid the occasional TimeoutException
    val defaultConfig = ConfigFactory.defaultReference

    ActorSystem("JaiCameraMonitorTest", customConfig.withFallback(defaultConfig))
  }
}