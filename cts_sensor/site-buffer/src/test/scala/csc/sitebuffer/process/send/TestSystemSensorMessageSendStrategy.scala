package csc.sitebuffer.process.send

/**
 * Uitproberen Sender - Camel Producer - Camel Consumer strategie, voor het verzenden van SystemSensorMessages
 * van Sitebuffer naar server.
 */

import akka.actor.{ Actor, ActorRef, ActorSystem, Props }
import akka.camel._
import csc.akka.CamelClient
import scala.concurrent.duration._
import scala.util.{ Failure, Success }

case class TestSystemSensorMessage(text: String) {
  def toJson: String = "{text: %s}".format(text)
}

object SendSystemSensorMessageStrategy extends App with CamelClient {
  val system = ActorSystem("SendSystemSensorMessageStrategy")
  implicit val ec = system.dispatcher
  val tcpConsumer = system.actorOf(Props[TcpConsumer], "TcpConsumer")
  activationFutureFor(tcpConsumer, 10 seconds) onComplete {
    case Failure(_) ⇒ println("Error, failed to activate")
    case Success(tcpConsumerActorRef) ⇒
      val tcpProducer = system.actorOf(Props[TcpProducer], "TcpProducer")
      val sender = system.actorOf(Props(new Sender(tcpProducer)), "Sender")

      // Hier gaat 'ie
      sender ! TestSystemSensorMessage("Sensor") // Happy flow
  }
}

class TcpConsumer extends Actor with Consumer {
  def endpointUri = "mina:tcp://localhost:6223?textline=true&sync=true"

  def receive = {
    case message: CamelMessage ⇒
      val body = message.bodyAs[String]
      println("Consumer received - body: " + body)
      message.headers.foreach((header: (String, Any)) ⇒
        println("Consumer received - header[%s]: %s [type = %s]".format(header._1, header._2, header._2.getClass)))

      body match {
        case "{text: Sensor}" ⇒
          println("Yes, aangekomen op de server!")
          sender ! Ack
        case _ ⇒
          throw new Exception("CamelMessage with unknown body")
        //sender ! Failure(new Throwable("CamelMessage with unknown body"))
      }
  }

  override def preRestart(reason: Throwable, message: Option[Any]) {
    println("Consumer preRestart")
    sender ! Failure(reason)
  }
}

case object TcpProducerSucces
case object TcpProducerFailure

class TcpProducer extends Actor with Producer {
  def endpointUri = "mina:tcp://localhost:6223?textline=true&sync=true"

  //  override def transformOutgoingMessage(msg: Any) = msg match {
  //    case msg: String ⇒ "Bless %s!".format(msg)
  //  }

  override def transformResponse(msg: Any): Any = msg match {
    case message: CamelMessage ⇒ message.body match {
      case "Ack" ⇒
        println("Producer received response: Ack")
        println("         and transforms this to TcpProducerSucces")
        message.headers.foreach((header: (String, Any)) ⇒
          println("Producer received - header[%s]: %s".format(header._1, header._2)))
        TcpProducerSucces
      case msg: Any ⇒
        println("Producer received CamelMessage - response: " + msg.toString)
        println("Producer received - body: " + message.bodyAs[String])
        message.headers.foreach((header: (String, Any)) ⇒
          println("Producer received - header[%s]: %s".format(header._1, header._2)))
        TcpProducerFailure
    }
    case other ⇒
      println("Producer received %s".format(other.toString()))
      TcpProducerFailure
  }
}

class Sender(producer: ActorRef) extends Actor {
  def receive = {
    case message: TestSystemSensorMessage ⇒
      println("Sender received TestSystemSensorMessage: " + message.toString)
      println("       and will send TestSystemSensorMessage.toJson to Producer")
      producer ! CamelMessage(message.toJson, Map(CamelMessage.MessageExchangeId -> "12345"))
    case message @ TcpProducerSucces ⇒
      println("Sender received TcpProducerSucces")
    case message @ TcpProducerFailure ⇒
      println("Sender received TcpProducerFailure")
  }
}

