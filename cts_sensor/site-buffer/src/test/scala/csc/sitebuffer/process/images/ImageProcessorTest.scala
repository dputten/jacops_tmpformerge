/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.images

import java.io.File

import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import scala.concurrent.duration._
import csc.sitebuffer.FileTestUtils
import csc.sitebuffer.process.images.ImageDeleter.DeleteImage
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }
import testframe.Resources

/**
 * Test van ImageProcessor
 */
class ImageProcessorTest extends TestKit(ActorSystem("ImageProcessorTest")) with WordSpecLike with MustMatchers with BeforeAndAfterEach with BeforeAndAfterAll {
  import FileTestUtils._

  val testProbe: TestProbe = TestProbe()
  var imageProcessor: TestActorRef[ImageProcessor] = _

  val resourceDir = new File(Resources.getResourceDirPath().getAbsolutePath)
  val imageProcessorResourceDir = new File(resourceDir, "csc/sitebuffer/process/images/imageProcessor")
  val testBaseDir = new File(resourceDir, "out/imageProcessor")
  if (!testBaseDir.exists()) {
    testBaseDir.mkdirs()
  } else {
    clearDirectory(testBaseDir)
  }

  override def beforeAll() {
    super.beforeAll()
  }

  override def afterAll() {
    system.shutdown()

    clearDirectory(testBaseDir)
    testBaseDir.delete()
  }

  override def afterEach() {
    clearDirectory(testBaseDir)
  }

  "An ImageProcessor actor" when {

    "receiving a image path for a valid jpeg" must {
      "copy that image to the target directory with a new name (containing its sha) and send a DeleteImage to recipients" in {
        // Prepare input
        val inputDir = new File(testBaseDir, "input")
        inputDir.mkdir
        val testFile = copyTestResource("valid.jpg", new File(inputDir, "02120130628155503.347.jpg"))
        // Create test actor
        imageProcessor = TestActorRef(new ImageProcessor(
          directoryPrefixForOutgoingImages = "pre",
          recipients = Set(testProbe.ref)))
        // Send test message
        val imagePath = testFile.getCanonicalPath
        imageProcessor ! NewImage(imagePath, 12345678L)
        // Check expected results
        testProbe.expectMsg(500 millis, DeleteImage(imagePath))
        val outputDir = new File(testBaseDir, "preinput")
        outputDir.exists() must be(true)
        val outputFile = new File(outputDir, "02120130628155503.347-731fd6059075df227b892db80149d320.jpg")
        outputFile.exists() must be(true)
      }
    }

    "receiving a image path for a invalid jpeg (wrong start bytes)" must {
      "send a DeleteImage to recipients" in {
        // Prepare input
        val inputDir = new File(testBaseDir, "input")
        inputDir.mkdir
        val testFile = copyTestResource("wrongStartBytes.jpg", new File(inputDir, "02120130628155503.347.jpg"))
        // Create test actor
        imageProcessor = TestActorRef(new ImageProcessor(
          directoryPrefixForOutgoingImages = "pre",
          recipients = Set(testProbe.ref)))
        // Send test message
        val imagePath = testFile.getCanonicalPath
        imageProcessor ! NewImage(imagePath, 12345678L)
        // Check expected results
        testProbe.expectMsg(500 millis, DeleteImage(imagePath))
        val outputDir = new File(testBaseDir, "preinput")
        outputDir.exists() must be(false)
      }
    }

    "receiving a image path for a invalid jpeg (wrong end bytes)" must {
      "send a DeleteImage to recipients" in {
        // Prepare input
        val inputDir = new File(testBaseDir, "input")
        inputDir.mkdir
        val testFile = copyTestResource("wrongEndBytes.jpg", new File(inputDir, "02120130628155503.347.jpg"))
        // Create test actor
        imageProcessor = TestActorRef(new ImageProcessor(
          directoryPrefixForOutgoingImages = "pre",
          recipients = Set(testProbe.ref)))
        // Send test message
        val imagePath = testFile.getCanonicalPath
        imageProcessor ! NewImage(imagePath, 12345678L)
        // Check expected results
        testProbe.expectMsg(500 millis, DeleteImage(imagePath))
        val outputDir = new File(testBaseDir, "preinput")
        outputDir.exists() must be(false)
      }
    }

  }

  def copyTestResource(resourceName: String, toFilePath: File): File = {
    copyFile(new File(imageProcessorResourceDir, resourceName), toFilePath)
  }

}

