/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.system

import java.io.{ File, PrintWriter }

import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }
import testframe.Resources

import scala.io.Source

/**
 * Test van TimestampFileStorage trait.
 */
class TimestampFileStorageTest extends WordSpec with MustMatchers with BeforeAndAfterEach with BeforeAndAfterAll {

  var mockCurrentTime: Long = _
  val dummyCurrentTime = () ⇒ mockCurrentTime

  val resourceDir = new File(Resources.getResourceDirPath().getAbsolutePath)
  val testDirectory = new File(resourceDir, "timestamp-file-storage-test")

  val timestampFileName = "sitebufferTimestamp.txt"
  val timestampFile = new File(testDirectory, timestampFileName)

  override def beforeAll() {
  }

  override def afterAll() {
    deleteDirectory(testDirectory)
  }

  override def beforeEach() {
    if (testDirectory.exists()) {
      clearDirectory(testDirectory)
    }
  }

  def createTestDirectory(): Unit = {
    if (!testDirectory.exists()) {
      testDirectory.mkdir()
    }
  }

  "Calling storeTimestamp on an object with the TimestampFileStorage trait" when {
    "the configured directory does not exist" must {
      "return Right(Done) and create directory and a timestamp file containing correct timestamp" in {
        testDirectory.exists() must be(false)
        val testObject = new TimestampFileStorage {
          val timestampDirectory = testDirectory.getAbsolutePath
        }
        // Call method to be tested
        val newTimestamp = 22334455L
        val result = testObject.storeTimestamp(newTimestamp)
        // Check for expected result
        result must be(Right("Done"))
        filesCount(testDirectory) must be(1)
        timestampInTimestampFile must be(newTimestamp)
      }
    }

    "the configured directory is empty" must {
      "return Right(Done) and create a timestamp file containing correct timestamp" in {
        createTestDirectory()
        filesCount(testDirectory) must be(0)
        val testObject = new TimestampFileStorage {
          val timestampDirectory = testDirectory.getAbsolutePath
        }
        // Call method to be tested
        val newTimestamp = 22334455L
        val result = testObject.storeTimestamp(newTimestamp)
        // Check for expected result
        result must be(Right("Done"))
        filesCount(testDirectory) must be(1)
        timestampInTimestampFile must be(newTimestamp)
      }
    }

    "the configured directory already contains a timestamp file" must {
      "return Right(Done) and update timestamp file containing correct timestamp" in {
        createTestDirectory()
        createTimestampFile(12345678L)
        filesCount(testDirectory) must be(1)
        val testObject = new TimestampFileStorage {
          val timestampDirectory = testDirectory.getAbsolutePath
        }
        // Call method to be tested
        val newTimestamp = 22334455L
        val result = testObject.storeTimestamp(newTimestamp)
        // Check for expected result
        result must be(Right("Done"))
        filesCount(testDirectory) must be(1)
        timestampInTimestampFile must be(newTimestamp)
      }
    }

    "the configured directory (with extra '/' at the end) already contains a timestamp file" must {
      "return Right(Done) and update timestamp file containing correct timestamp" in {
        createTestDirectory()
        createTimestampFile(12345678L)
        filesCount(testDirectory) must be(1)
        val testObject = new TimestampFileStorage {
          val timestampDirectory = testDirectory.getAbsolutePath + "/"
        }
        // Call method to be tested
        val newTimestamp = 22334455L
        val result = testObject.storeTimestamp(newTimestamp)
        // Check for expected result
        result must be(Right("Done"))
        filesCount(testDirectory) must be(1)
        timestampInTimestampFile must be(newTimestamp)
      }
    }
  }

  "Calling storeTimestamp multiple times on an object with the TimestampFileStorage trait" when {

    "the configured directory is empty" must {
      "each time return Right(Done) and create/update timestamp file containing correct timestamp" in {
        createTestDirectory()
        filesCount(testDirectory) must be(0)
        val testObject = new TimestampFileStorage {
          val timestampDirectory = testDirectory.getAbsolutePath
        }
        // Call method to be tested
        val newTimestamp1 = 22334455L
        val result1 = testObject.storeTimestamp(newTimestamp1)
        // Check for expected result
        result1 must be(Right("Done"))
        filesCount(testDirectory) must be(1)
        timestampInTimestampFile must be(newTimestamp1)

        // Call method to be tested
        val newTimestamp2 = 33445566L
        val result2 = testObject.storeTimestamp(newTimestamp2)
        // Check for expected result
        result2 must be(Right("Done"))
        filesCount(testDirectory) must be(1)
        timestampInTimestampFile must be(newTimestamp2)

        // Call method to be tested
        val newTimestamp3 = 44556677L
        val result3 = testObject.storeTimestamp(newTimestamp3)
        // Check for expected result
        result3 must be(Right("Done"))
        filesCount(testDirectory) must be(1)
        timestampInTimestampFile must be(newTimestamp3)
      }
    }

  }

  "Calling getLatestTimestamp on an object with the TimestampFileStorage trait" when {
    "the configured directory is empty" must {
      "return Right(None)" in {
        createTestDirectory()
        filesCount(testDirectory) must be(0)
        val testObject = new TimestampFileStorage {
          val timestampDirectory = testDirectory.getAbsolutePath
        }
        // Call method to be tested
        val result = testObject.getLatestTimestamp
        result must be(Right(None))
      }
    }

    "the configured directory contains an empty timestamp file" must {
      "return Left(<error message>)" in {
        createTestDirectory()
        createEmptyTimestampFile()
        filesCount(testDirectory) must be(1)
        val testObject = new TimestampFileStorage {
          val timestampDirectory = testDirectory.getAbsolutePath
        }
        // Call method to be tested
        val result = testObject.getLatestTimestamp
        result must be(Left("Timestamp file [%s] was empty".format(timestampFile.getAbsolutePath)))
      }
    }

    "the configured directory contains a correct timestamp file" must {
      "return Right(Some(timestamp))" in {
        createTestDirectory()
        createTimestampFile(12345678L)
        filesCount(testDirectory) must be(1)
        val testObject = new TimestampFileStorage {
          val timestampDirectory = testDirectory.getAbsolutePath
        }
        // Call method to be tested
        val result = testObject.getLatestTimestamp
        result must be(Right(Some(12345678L)))
      }
    }

    "the configured directory (with extra '/' at the end) contains a correct timestamp file" must {
      "return Right(Some(timestamp))" in {
        createTestDirectory()
        createTimestampFile(12345678L)
        filesCount(testDirectory) must be(1)
        val testObject = new TimestampFileStorage {
          val timestampDirectory = testDirectory.getAbsolutePath + "/"
        }
        // Call method to be tested
        val result = testObject.getLatestTimestamp
        result must be(Right(Some(12345678L)))
      }
    }

    "the configured directory contains a timestamp file with more than one line" must {
      "return Left(<error message>)" in {
        createTestDirectory()
        create2LinesTimestampFile()
        filesCount(testDirectory) must be(1)
        val testObject = new TimestampFileStorage {
          val timestampDirectory = testDirectory.getAbsolutePath
        }
        // Call method to be tested
        val result = testObject.getLatestTimestamp
        result must be(Left("Timestamp file [%s] contains more than one line".format(timestampFile.getAbsolutePath)))
      }
    }

    "the configured directory contains a timestamp file with an incorrect timestamp" must {
      "return Left(<error message>)" in {
        createTestDirectory()
        createWrongTimestampFile()
        filesCount(testDirectory) must be(1)
        val testObject = new TimestampFileStorage {
          val timestampDirectory = testDirectory.getAbsolutePath
        }
        // Call method to be tested
        val result = testObject.getLatestTimestamp
        result must be(Left("Error reading timestamp from timestamp file [%s]: [java.lang.NumberFormatException: For input string: \"rubbish\"]".format(timestampFile.getAbsolutePath)))
      }
    }

  }

  def createTimestampFile(timestamp: Long) {
    val file = new File(testDirectory, timestampFileName)
    val writer = new PrintWriter(file)
    try {
      writer.write(timestamp.toString)
    } finally {
      writer.close()
    }
  }

  def createEmptyTimestampFile() {
    val file = new File(testDirectory, timestampFileName)
    val writer = new PrintWriter(file)
    writer.close()
  }

  def create2LinesTimestampFile() {
    val file = new File(testDirectory, timestampFileName)
    val writer = new PrintWriter(file)
    try {
      writer.write("12345678\r\n")
      writer.write("87654321")
    } finally {
      writer.close()
    }
  }

  def createWrongTimestampFile() {
    val file = new File(testDirectory, timestampFileName)
    val writer = new PrintWriter(file)
    try {
      writer.write("rubbish")
    } finally {
      writer.close()
    }
  }

  def timestampInTimestampFile: Long = {
    val file = new File(testDirectory, timestampFileName)
    if (file.exists()) {
      val lines = Source.fromFile(file).getLines().toList
      lines.size match {
        case 0 ⇒ -2L
        case 1 ⇒ lines.head.toLong
        case _ ⇒ -3L
      }
    } else {
      -1L
    }

  }

  def filesCount(dir: File): Int = {
    dir.listFiles.size
  }

  def clearDirectory(dir: File) {
    dir.listFiles.foreach { f ⇒ deleteFile(f) }
  }

  def deleteDirectory(dir: File) {
    deleteFile(dir)
  }

  def deleteFile(file: File) {
    if (file.isDirectory)
      file.listFiles.foreach { f ⇒ deleteFile(f) }
    file.delete
  }

}
