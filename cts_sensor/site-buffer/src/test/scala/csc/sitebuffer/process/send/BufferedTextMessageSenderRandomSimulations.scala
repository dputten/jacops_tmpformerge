/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.send

import java.util.Date

import akka.actor.{ Actor, ActorRef, ActorSystem, Props }
import akka.testkit.{ TestFSMRef, TestKit, TestProbe }
import scala.concurrent.duration._
import csc.sitebuffer.process.send.BufferedTextMessageSender.{ Idle, ToDo }
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, WordSpec }

import scala.collection.immutable
import scala.util.Random

/**
 * Run random simulations van BufferedTextMessageSender
 */
class BufferedTextMessageSenderRandomSimulations extends TestKit(ActorSystem("BufferedTextMessageSenderRandomSimulations")) with WordSpecLike with MustMatchers
  with BeforeAndAfterAll {

  implicit val executor = system.dispatcher

  /**
   * Test version of the InOutTextSender trait.
   * Will send a text to the configured receiver.
   */
  trait TestInOutTextSender extends InOutTextSender { self: Actor ⇒
    def receiver: ActorRef

    def sendText(text: String) = {
      receiver ! text
    }
  }

  val numberOfMessages = 20 // Number of TextMessages that will be sent
  val successRate = 60 // Percentage of message the mockReceiver will return Success for.
  val maxDelayBetweenMessagesMs = 1000 // Maximum delay between 2 successive TextMessages
  val minProcessingTimeMs = 20 // Minimum time needed by mockReceiver to "process message"
  val maxProcessingTimeMs = 300 // Maximum time needed by mockReceiver to "process message"

  override def beforeAll() {
    super.beforeAll()
  }

  override def afterAll() {
    system.shutdown
  }

  implicit def intWithTimes(n: Int) = new {
    def times(f: Int ⇒ Unit) = 1 to n foreach { i ⇒ f(i) }
  }

  def createTestSender(testReceiver: ActorRef, delay: Int = 1000): TestFSMRef[BufferedTextMessageSender.State, BufferedTextMessageSender.Data, BufferedTextMessageSender] = {
    TestFSMRef(new BufferedTextMessageSender(retryDelayMs = delay) with TestInOutTextSender {
      val receiver = testReceiver
    })
  }

  "A BufferedTextMessageSender actor" when {

    "running a simulation with %d messages".format(numberOfMessages) must {
      "afterwards end up in Idle state and empty data" in {
        val finishReceiver = TestProbe()
        val mockReceiver = system.actorOf(Props(new MockReceiver(numberOfMessages, successRate, minProcessingTimeMs, maxProcessingTimeMs, finishReceiver.ref)))
        val testSender = createTestSender(mockReceiver)
        val simulator = system.actorOf(Props(new Simulator(numberOfMessages, maxDelayBetweenMessagesMs, testSender)))

        // Start the simulator actor
        simulator ! Simulator.Send

        finishReceiver.expectMsg(70 seconds, MockReceiver.Finish)

        // check state after message has been processed
        testSender.stateName must be(Idle)
        testSender.stateData must be(ToDo(None, immutable.Queue()))
      }
    }
  }

}

class MockReceiver(numberOfMessages: Int, successRate: Int, minProcessingTimeMs: Int, maxProcessingTimeMs: Int, finishReceiver: ActorRef) extends Actor {
  import MockReceiver._

  implicit val executor = context.system.dispatcher
  val randomGenerator = new Random(new Date().getTime)
  var counter = 0

  def receive = {
    case s: String ⇒
      println("Receiver: received message [%s]".format(s))
      val reply = randomGenerator.nextInt(100) match {
        case x: Int if (x < successRate) ⇒
          counter += 1
          InOutTextSender.Success
        case _ ⇒ InOutTextSender.Failure
      }
      val delay = getRandomDuration
      println("Receiver: sending %s over %d ms".format(reply, delay))
      context.system.scheduler.scheduleOnce(delay millis, sender, reply)
      if (counter == numberOfMessages) {
        val delayForFinish = delay + 500 // Give actor enough time to
        println("Receiver: sending Finish over %d ms".format(delayForFinish))
        context.system.scheduler.scheduleOnce(delayForFinish millis, finishReceiver, Finish)
      }
  }

  def getRandomDuration: Int = {
    minProcessingTimeMs + randomGenerator.nextInt(maxProcessingTimeMs - minProcessingTimeMs)
  }

}

object MockReceiver {
  case object Finish
}

class Simulator(numberOfMessages: Int, maxDelayMs: Int, sender: ActorRef) extends Actor {
  import Simulator._

  implicit val executor = context.system.dispatcher
  val randomGenerator = new Random(new Date().getTime)
  var counter = 0

  def receive = {
    case Send ⇒
      counter += 1
      println("Simulator: sending message %d".format(counter))
      sender ! TextMessage("Message %d".format(counter))
      if (counter < numberOfMessages)
        context.system.scheduler.scheduleOnce(getRandomDuration millis, self, Send)
  }

  def getRandomDuration: Int = {
    randomGenerator.nextInt(maxDelayMs)
  }
}

object Simulator {
  case object Send
}
