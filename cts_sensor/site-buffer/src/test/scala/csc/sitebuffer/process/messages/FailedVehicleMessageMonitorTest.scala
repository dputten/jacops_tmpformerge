/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.messages

import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import scala.concurrent.duration._
import csc.sitebuffer.messages.{ Direction, VehicleMessage }
import csc.sitebuffer.process.SystemSensorMessage
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, WordSpec }

/**
 * Test for FailedVehicleMessageMonitor actor
 */
class FailedVehicleMessageMonitorTest extends TestKit(ActorSystem("FailedVehicleMessageMonitorTest")) with WordSpecLike with MustMatchers
  with BeforeAndAfterAll {

  var currentTime: Long = _
  val dummyCurrentTime = () ⇒ currentTime

  override def beforeAll() {
    super.beforeAll()
  }

  override def afterAll() {
    system.shutdown()
  }

  private def createVehicleMessage(offset: Int): VehicleMessage = {
    VehicleMessage(dateTime = 1360650493123L + offset, detector = 1, direction = Direction.Outgoing,
      speed = 23.0f, length = 50.0f, loop1RiseTime = 0L, loop1FallTime = 450L,
      loop2RiseTime = 400L, loop2FallTime = 875L, yellowTime2 = 0, redTime2 = 0,
      dateTime1 = 1360650493005L + offset, yellowTime1 = 0, redTime1 = 0, dateTime3 = 0L + offset,
      yellowTime3 = 0, redTime3 = 0, valid = true)
  }

  "The FailedVehicleMessageMonitor" when {
    val testProbe = TestProbe()
    val failedVehicleMessageMonitor = TestActorRef(new FailedVehicleMessageMonitor(getCurrentTime = dummyCurrentTime, systemSensorMessageSender = testProbe.ref))

    "receiving an invalid VehicleMessage" must {
      "send a SystemSensorMessage(Oscillator, SensorFailure) to the systemSensorMessageSender" in {
        currentTime = 676767
        // Send test message
        val message = createVehicleMessage(offset = 0)
        failedVehicleMessageMonitor ! message

        // Check for expected result
        val expectedMsg = SystemSensorMessage(
          sensor = "Oscillator",
          timeStamp = 676767L,
          systemId = "",
          event = "SensorFailure",
          sendingComponent = "Sitebuffer",
          data = Map())
        testProbe.expectMsg(500 millis, expectedMsg)
      }
    }

    "receiving an unknown message" must {
      "do nothing" in {
        // Send test message
        failedVehicleMessageMonitor ! "wat is dit?"
        testProbe.expectNoMsg(500 millis)
      }
    }

  }

}
