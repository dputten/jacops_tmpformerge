/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.send

import akka.actor.Status.Failure
import akka.actor.{ Actor, ActorRef, ActorSystem }
import akka.camel.{ Ack, CamelExtension, CamelMessage, Consumer }
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import akka.util.Timeout
import csc.akka.CamelClient
import scala.concurrent.Await
import scala.concurrent.duration._
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, WordSpec }

/**
 * Test van TextlineTcpSender trait.
 */
class TextlineTcpSenderTest
  extends TestKit(ActorSystem("TextlineTcpSenderTest"))
  with WordSpecLike
  with MustMatchers
  with CamelClient
  with BeforeAndAfterEach
  with BeforeAndAfterAll {

  val testReceiverOfConsumerOutput: TestProbe = TestProbe()
  val testReceiverOfSenderResult: TestProbe = TestProbe()
  var testConsumer: TestActorRef[TestConsumer] = null

  override def beforeAll() {
    super.beforeAll()
  }

  override protected def beforeEach(): Unit = {
    testConsumer = TestActorRef(new TestConsumer(testReceiverOfConsumerOutput.ref))
    awaitActivation(testConsumer, 10 seconds)
  }

  override def afterAll() {
    testConsumer.stop()
    //awaitDeactivation(testConsumer, 10 seconds)
    system.shutdown()
  }

  private def createTextlineTcpSender(hostName: String, portNumber: Int): TestActorRef[TestInOutTextSenderActor with TextlineTcpSender] = {
    TestActorRef(new TestInOutTextSenderActor(testReceiverOfSenderResult.ref) with TextlineTcpSender {
      val host = hostName
      val port = portNumber
    })
  }

  "An actor with TextlineTcpSender trait" when {

    "calling sendText with a valid text" must {
      "send this text to the configured producer and receive Success back" in {
        val textlineTcpSender = createTextlineTcpSender("localhost", 6200)
        textlineTcpSender ! "{This is a valid text}"

        // test for expected result
        testReceiverOfSenderResult.expectMsg(2000 millis, InOutTextSender.Success)
        testReceiverOfConsumerOutput.expectMsg(1000 millis, "This is a valid text")
      }
    }

    "calling sendText with an invalid text" must {
      "send this text to the configured producer and receive Failure back" in {
        val textlineTcpSender = createTextlineTcpSender("localhost", 6200)
        textlineTcpSender ! "<This is an invalid text>"

        // test for expected result
        testReceiverOfSenderResult.expectMsg(2000 millis, InOutTextSender.Failure)
        testReceiverOfConsumerOutput.expectNoMsg(1000 millis)
      }
    }

    "calling sendText with a valid message and no corresponding Consumer present" must {
      "respond with Failure" in {
        val textlineTcpSender = createTextlineTcpSender("localhost", 6201)
        textlineTcpSender ! "{This is a valid text}"

        // test for expected result
        testReceiverOfSenderResult.expectMsg(2000 millis, InOutTextSender.Failure)
        testReceiverOfConsumerOutput.expectNoMsg(1000 millis)
      }
    }
  }

}

class TestInOutTextSenderActor(resultReceiver: ActorRef) extends Actor {
  self: InOutTextSender ⇒

  def receive = {
    case text: String ⇒
      sendText(text)
    case message: AnyRef ⇒
      resultReceiver ! message
  }
}

// This Consumer simulates the behavior of the real Consumer, that will run on the server.
//    - Ack to sender when valid message received.
//    - Failure to sender otherwise
class TestConsumer(validMessageReceiver: ActorRef) extends Actor with Consumer {
  def endpointUri = "mina:tcp://localhost:6200?textline=true&sync=true"

  val ValidMessage = """^\{(.*)\}$""".r // = Any string that begins with { and ends with }

  def receive = {
    case message: CamelMessage ⇒
      message.bodyAs[String] match {
        case ValidMessage(content) ⇒
          sender ! Ack
          validMessageReceiver ! content
        case _ ⇒
          throw new Exception("CamelMessage with unknown body")
      }
  }

  override def preRestart(reason: Throwable, message: Option[Any]) {
    sender ! Failure(reason)
  }
}
