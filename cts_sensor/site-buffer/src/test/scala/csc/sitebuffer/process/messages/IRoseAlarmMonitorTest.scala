/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.messages

import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import scala.concurrent.duration._
import csc.sitebuffer.messages.InputEventMessage
import csc.sitebuffer.process.SystemSensorMessage
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, WordSpec }

/**
 * Test van IRoseAlarmMonitor
 */
class IRoseAlarmMonitorTest extends TestKit(ActorSystem("IRoseAlarmMonitorTest")) with WordSpecLike with MustMatchers with BeforeAndAfterAll {

  override def beforeAll() {
    super.beforeAll()
  }

  override def afterAll() {
    system.shutdown
  }

  "The IRoseAlarmMonitor" when {
    val testProbe = TestProbe()
    val iRoseAlarmActorRef = TestActorRef(new IRoseAlarmMonitor(systemSensorMessageSender = testProbe.ref))

    "receiving an InputEventMessage, with event number = 0" must {
      "send an SystemSensorMessage(Temperature, OutOfRange) to the systemSensorMessageSender" in {
        // create message
        val inputEventMessage = InputEventMessage(dateTime = 123456789L, inputId = 8, eventNumber = 0, eventText = "Too cold")
        // construct expected message
        val expectedMsg = SystemSensorMessage(
          sensor = "Temperature",
          timeStamp = 123456789L,
          systemId = "",
          event = "OutOfRange",
          sendingComponent = "Sitebuffer",
          data = Map("details" -> "Too cold"))
        // send message
        iRoseAlarmActorRef ! inputEventMessage
        // test if expected message is received
        testProbe.expectMsg(500 millis, expectedMsg)
      }
    }

    "receiving an InputEventMessage, with event number = 1" must {
      "send an SystemSensorMessage(Temperature, OutOfRange) to the systemSensorMessageSender" in {
        // create message
        val inputEventMessage = InputEventMessage(dateTime = 123456789L, inputId = 8, eventNumber = 1, eventText = "Too warm")
        // construct expected message
        val expectedMsg = SystemSensorMessage(
          sensor = "Temperature",
          timeStamp = 123456789L,
          systemId = "",
          event = "OutOfRange",
          sendingComponent = "Sitebuffer",
          data = Map("details" -> "Too hot"))
        // send message
        iRoseAlarmActorRef ! inputEventMessage
        // test if expected message is received
        testProbe.expectMsg(500 millis, expectedMsg)
      }
    }

    "receiving an InputEventMessage, with event number = 2" must {
      "send an SystemSensorMessage(Humidity, OutOfRange) to the systemSensorMessageSender" in {
        // create message
        val inputEventMessage = InputEventMessage(dateTime = 123456789L, inputId = 8, eventNumber = 2, eventText = "Too humid")
        // construct expected message
        val expectedMsg = SystemSensorMessage(
          sensor = "Humidity",
          timeStamp = 123456789L,
          systemId = "",
          event = "OutOfRange",
          sendingComponent = "Sitebuffer",
          data = Map())
        // send message
        iRoseAlarmActorRef ! inputEventMessage
        // test if expected message is received
        testProbe.expectMsg(500 millis, expectedMsg)
      }
    }

    "receiving an InputEventMessage, with event number = 3" must {
      "send an SystemSensorMessage(Accelerometer, OutOfRange) to the systemSensorMessageSender" in {
        // create message
        val inputEventMessage = InputEventMessage(dateTime = 123456789L, inputId = 8, eventNumber = 3, eventText = "Leaning")
        // construct expected message
        val expectedMsg = SystemSensorMessage(
          sensor = "Accelerometer",
          timeStamp = 123456789L,
          systemId = "",
          event = "OutOfRange",
          sendingComponent = "Sitebuffer",
          data = Map())
        // send message
        iRoseAlarmActorRef ! inputEventMessage
        // test if expected message is received
        testProbe.expectMsg(500 millis, expectedMsg)
      }
    }

    "receiving an InputEventMessage, with event number = 4" must {
      "send an SystemSensorMessage(Oscillator, OutOfRange) to the systemSensorMessageSender" in {
        // create message
        val inputEventMessage = InputEventMessage(dateTime = 123456789L, inputId = 8, eventNumber = 4, eventText = "Something")
        // construct expected message
        val expectedMsg = SystemSensorMessage(
          sensor = "Oscillator",
          timeStamp = 123456789L,
          systemId = "",
          event = "OutOfRange",
          sendingComponent = "Sitebuffer",
          data = Map("details" -> "Oscillator A and B differ more than 5%"))
        // send message
        iRoseAlarmActorRef ! inputEventMessage
        // test if expected message is received
        testProbe.expectMsg(500 millis, expectedMsg)
      }
    }

    "receiving an InputEventMessage, with event number = 5" must {
      "send an SystemSensorMessage(Oscillator, OutOfRange) to the systemSensorMessageSender" in {
        // create message
        val inputEventMessage = InputEventMessage(dateTime = 123456789L, inputId = 8, eventNumber = 5, eventText = "Something")
        // construct expected message
        val expectedMsg = SystemSensorMessage(
          sensor = "Oscillator",
          timeStamp = 123456789L,
          systemId = "",
          event = "OutOfRange",
          sendingComponent = "Sitebuffer",
          data = Map("details" -> "Frequency step > 1% in oscillator A, not in B"))
        // send message
        iRoseAlarmActorRef ! inputEventMessage
        // test if expected message is received
        testProbe.expectMsg(500 millis, expectedMsg)
      }
    }

    "receiving an InputEventMessage, with event number = 6" must {
      "send an SystemSensorMessage(Oscillator, OutOfRange) to the systemSensorMessageSender" in {
        // create message
        val inputEventMessage = InputEventMessage(dateTime = 123456789L, inputId = 8, eventNumber = 6, eventText = "Something")
        // construct expected message
        val expectedMsg = SystemSensorMessage(
          sensor = "Oscillator",
          timeStamp = 123456789L,
          systemId = "",
          event = "OutOfRange",
          sendingComponent = "Sitebuffer",
          data = Map("details" -> "Frequency step > 1% in oscillator B, not in A"))
        // send message
        iRoseAlarmActorRef ! inputEventMessage
        // test if expected message is received
        testProbe.expectMsg(500 millis, expectedMsg)
      }
    }

    "receiving an InputEventMessage, with event number = 7" must {
      "ignore this message" in {
        // create message
        val inputEventMessage = InputEventMessage(dateTime = 123456789L, inputId = 8, eventNumber = 7, eventText = "Invalid eventNumber")
        // send message
        iRoseAlarmActorRef ! inputEventMessage
        // test if expected message is received
        testProbe.expectNoMsg(1000 millis)
      }
    }

    "receiving a message other than InputEventMessage" must {
      "ignore this message" in {
        // create message
        val inputEventMessage = "Some kind of non InputEventMessage"
        // send message
        iRoseAlarmActorRef ! inputEventMessage
        // test if expected message is received
        testProbe.expectNoMsg(1000 millis)
      }
    }

  }
}
