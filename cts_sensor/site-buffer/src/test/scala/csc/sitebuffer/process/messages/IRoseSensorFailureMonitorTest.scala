/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.messages

import java.io.File

import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import scala.concurrent.duration._
import csc.sitebuffer.config._
import csc.sitebuffer.messages.{ Accelerometer, SensorMessage }
import csc.sitebuffer.process.SystemSensorMessage
import csc.sitebuffer.process.SystemSensorMessage.Sensor
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, WordSpec }

/**
 * Test van IRoseSensorFailureMonitor
 */
class IRoseSensorFailureMonitorTest extends TestKit(ActorSystem("IRoseSensorFailureMonitorTest")) with WordSpecLike with MustMatchers with BeforeAndAfterAll {

  var datetime = 100000000L

  override def beforeAll() {
    super.beforeAll()
  }

  override def afterAll() {
    system.shutdown()
  }

  "The IRoseSensorFailureMonitor (config: CountForAlarm = 4)" when {
    val config = SiteBufferConfiguration(
      iRoseEndpoint = new HostPortConfig(host = "", port = 0),
      vrEndpoint = new DirectoryConfig(""),
      anprEndpoint = new DirectoryConfig(""),
      systemSensorMessageEndpoint = new HostPortConfig(host = "localhost", port = 6000),
      throttling = Throttle(vrThrottling = FileThrottleConfig(new File("inputDir"), new File("outputDir"), "[0-9]+", "", ".*\\.json", 5, 1000),
        imageThrottling = FileThrottleConfig(new File("imgInputDir"), new File("imgOutputDir"), "TA[0-9]", "PA", ".*\\.jpg", 5, 8000)),
      consecutiveFailedSensorReadingsCountForAlarm = 4,
      isAliveCheck = IsAliveCheck(isAliveCheckInitialDelayMs = 15000,
        isAliveCheckFrequencyMs = 20000),
      clockOutOfSyncMarginMs = 500,
      systemSensorMessageRetryDelayMs = 2000,
      repeatingSystemSensorMessagesFilterWindowMs = 6000,
      minimalSpeedForProcessingInvalidVehicleMessageKmh = 20,
      loopFailure = new LoopFailureConfig(maxAllowedMessages = 0, testWindowMs = 0, iRoseRebootScriptPath = ""),
      cabinetDoorConfig = new CabinetDoorConfig(inputNumber = 0, openEventNumber = 0, closedEventNumber = 0),
      cameraConfigs = Map[String, CameraConfig](),
      cameraMonitorConfig = new CameraMonitorConfig(enabled = true, checkFrequency = 60000.millis, alarmDelay = 100.millis, maxRetries = 3, timeoutDuration = 100.millis),
      ntpFailure = new NtpFailureConfig(maxAllowedMessages = 0, testWindowMs = 0),
      imageProcessing = new ImageProcessingConfig(newImagesEndpoint = new HostPortConfig(host = "", port = 0), directoryPrefixForOutgoingImages = "O", maxDeleteRetries = 0, minDelayBeforeRetryMs = 0),
      wasDown = new WasDownMonitorConfig(enabled = true, timestampDirectory = "", timestampUpdateFrequency = 1.millis, minimalDeviationForWasDown = 3.millis),
      inputRecording = new InputRecordingConfig(enabled = true, baseDirectory = "", imageFilenamesOnly = false),
      imageFilter = new ImageFilterConfig(true, "jpg", 500.millis),
      loopState = new LoopStateConfig(500.millis, "output"),
      calibration = new CalibrationConfig(telnetUser = "root", telnetPwd = "pwd", telnetPort = 23,
        sshHost = "192.168.1.10", sshPort = 22, sshUser = "root", sshPwd = "pwd", sshTimeout = 30000,
        NTPmaxDiff_ms = 10,
        ftpRootDir = "/data",
        calibrationImageDir = "calibration", speedThreshold = 300,
        centralHost = "20.133.70.197", centralport = 12300, msgRetryDelayMs = 15000,
        consumerHost = "0.0.0.0", consumerPort = 12350, retriggerTime = 20.seconds, maxProcessTime = 5.minutes, maxMessageLength = 1000,
        jpgQuality = 100))

    val testProbe = TestProbe()
    val iRoseSensorFailureRef = TestActorRef(new IRoseSensorFailureMonitor(config = config, systemSensorMessageSender = testProbe.ref))

    IRoseSensorFailureMonitor.monitoredSensors.foreach(sensor ⇒
      "receiving 3 consecutive failed readings for the %s sensor".format(sensor) must {
        "do nothing" in {
          // create message
          val failedMessage1 = createSensorMessageWithFailures(Seq(sensor))
          val failedMessage2 = createSensorMessageWithFailures(Seq(sensor))
          val failedMessage3 = createSensorMessageWithFailures(Seq(sensor))
          // send message
          iRoseSensorFailureRef ! failedMessage1
          iRoseSensorFailureRef ! failedMessage2
          iRoseSensorFailureRef ! failedMessage3
          // test for expected result
          testProbe.expectNoMsg(1000 millis)
        }
      })

    IRoseSensorFailureMonitor.monitoredSensors.foreach(sensor ⇒
      "receiving 4 consecutive failed readings for the %s sensor".format(sensor) must {
        "send a SystemSensorMessage(%s, SensorFailure) to the systemSensorMessageSender".format(sensor) in {
          // create message
          val failedMessage1 = createSensorMessageWithFailures(Seq(sensor))
          val failedMessage2 = createSensorMessageWithFailures(Seq(sensor))
          val failedMessage3 = createSensorMessageWithFailures(Seq(sensor))
          val failedMessage4 = createSensorMessageWithFailures(Seq(sensor))
          // construct expected message
          val expectedMsg = SystemSensorMessage(
            sensor = sensor,
            timeStamp = failedMessage4.dateTime,
            systemId = "",
            event = "SensorFailure",
            sendingComponent = "Sitebuffer",
            data = Map())
          // send message
          iRoseSensorFailureRef ! failedMessage1
          iRoseSensorFailureRef ! failedMessage2
          iRoseSensorFailureRef ! failedMessage3
          iRoseSensorFailureRef ! failedMessage4
          // test for expected result
          testProbe.expectMsg(500 millis, expectedMsg)
        }
      })

    IRoseSensorFailureMonitor.monitoredSensors.foreach(sensor ⇒
      "receiving (3 failed, 1 good, 3 failed) readings for the %s sensor".format(sensor) must {
        "do nothing" in {
          // create message
          val failedMessage1 = createSensorMessageWithFailures(Seq(sensor))
          val failedMessage2 = createSensorMessageWithFailures(Seq(sensor))
          val failedMessage3 = createSensorMessageWithFailures(Seq(sensor))
          val goodMessage = createSensorMessageWithFailures(Seq())
          val failedMessage4 = createSensorMessageWithFailures(Seq(sensor))
          val failedMessage5 = createSensorMessageWithFailures(Seq(sensor))
          val failedMessage6 = createSensorMessageWithFailures(Seq(sensor))
          // send message
          iRoseSensorFailureRef ! failedMessage1
          iRoseSensorFailureRef ! failedMessage2
          iRoseSensorFailureRef ! failedMessage3
          iRoseSensorFailureRef ! goodMessage
          iRoseSensorFailureRef ! failedMessage4
          iRoseSensorFailureRef ! failedMessage5
          iRoseSensorFailureRef ! failedMessage6
          // test for expected result
          testProbe.expectNoMsg(1000 millis)
        }
      })

    IRoseSensorFailureMonitor.monitoredSensors.foreach(sensor ⇒
      "receiving (4 failed, 1 good, 4 failed) readings for the %s sensor".format(sensor) must {
        "send 2 SystemSensorMessage(%s, SensorFailure) to the systemSensorMessageSender".format(sensor) in {
          // create message
          val failedMessage1 = createSensorMessageWithFailures(Seq(sensor))
          val failedMessage2 = createSensorMessageWithFailures(Seq(sensor))
          val failedMessage3 = createSensorMessageWithFailures(Seq(sensor))
          val failedMessage4 = createSensorMessageWithFailures(Seq(sensor))
          val goodMessage = createSensorMessageWithFailures(Seq())
          val failedMessage5 = createSensorMessageWithFailures(Seq(sensor))
          val failedMessage6 = createSensorMessageWithFailures(Seq(sensor))
          val failedMessage7 = createSensorMessageWithFailures(Seq(sensor))
          val failedMessage8 = createSensorMessageWithFailures(Seq(sensor))
          // construct expected message
          val expectedMsg1 = SystemSensorMessage(
            sensor = sensor,
            timeStamp = failedMessage4.dateTime,
            systemId = "",
            event = "SensorFailure",
            sendingComponent = "Sitebuffer",
            data = Map())
          val expectedMsg2 = SystemSensorMessage(
            sensor = sensor,
            timeStamp = failedMessage8.dateTime,
            systemId = "",
            event = "SensorFailure",
            sendingComponent = "Sitebuffer",
            data = Map())
          // send message
          iRoseSensorFailureRef ! failedMessage1
          iRoseSensorFailureRef ! failedMessage2
          iRoseSensorFailureRef ! failedMessage3
          iRoseSensorFailureRef ! failedMessage4
          iRoseSensorFailureRef ! goodMessage
          iRoseSensorFailureRef ! failedMessage5
          iRoseSensorFailureRef ! failedMessage6
          iRoseSensorFailureRef ! failedMessage7
          iRoseSensorFailureRef ! failedMessage8
          // test for expected result
          testProbe.expectMsg(500 millis, expectedMsg1)
          testProbe.expectMsg(500 millis, expectedMsg2)
        }
      })

    IRoseSensorFailureMonitor.monitoredSensors.foreach(sensor ⇒
      "receiving 8 consecutive failed readings for the %s sensor".format(sensor) must {
        "send 2 SystemSensorMessage(%s, SensorFailure) to the systemSensorMessageSender".format(sensor) in {
          // create message
          val failedMessage1 = createSensorMessageWithFailures(Seq(sensor))
          val failedMessage2 = createSensorMessageWithFailures(Seq(sensor))
          val failedMessage3 = createSensorMessageWithFailures(Seq(sensor))
          val failedMessage4 = createSensorMessageWithFailures(Seq(sensor))
          val failedMessage5 = createSensorMessageWithFailures(Seq(sensor))
          val failedMessage6 = createSensorMessageWithFailures(Seq(sensor))
          val failedMessage7 = createSensorMessageWithFailures(Seq(sensor))
          val failedMessage8 = createSensorMessageWithFailures(Seq(sensor))
          // construct expected message
          val expectedMsg1 = SystemSensorMessage(
            sensor = sensor,
            timeStamp = failedMessage4.dateTime,
            systemId = "",
            event = "SensorFailure",
            sendingComponent = "Sitebuffer",
            data = Map())
          val expectedMsg2 = SystemSensorMessage(
            sensor = sensor,
            timeStamp = failedMessage8.dateTime,
            systemId = "",
            event = "SensorFailure",
            sendingComponent = "Sitebuffer",
            data = Map())
          // send message
          iRoseSensorFailureRef ! failedMessage1
          iRoseSensorFailureRef ! failedMessage2
          iRoseSensorFailureRef ! failedMessage3
          iRoseSensorFailureRef ! failedMessage4
          iRoseSensorFailureRef ! failedMessage5
          iRoseSensorFailureRef ! failedMessage6
          iRoseSensorFailureRef ! failedMessage7
          iRoseSensorFailureRef ! failedMessage8
          // test for expected result
          testProbe.expectMsg(500 millis, expectedMsg1)
          testProbe.expectMsg(500 millis, expectedMsg2)
        }
      })

    "receiving 4 consecutive failed readings for the all sensors" must {
      "send one SystemSensorMessage(<sensor>, SensorFailure) for each <sensor> to the systemSensorMessageSender" in {
        // create message
        val failedMessage1 = createSensorMessageWithFailures(Seq(Sensor.Temperature, Sensor.Humidity, Sensor.Accelerometer))
        val failedMessage2 = createSensorMessageWithFailures(Seq(Sensor.Temperature, Sensor.Humidity, Sensor.Accelerometer))
        val failedMessage3 = createSensorMessageWithFailures(Seq(Sensor.Temperature, Sensor.Humidity, Sensor.Accelerometer))
        val failedMessage4 = createSensorMessageWithFailures(Seq(Sensor.Temperature, Sensor.Humidity, Sensor.Accelerometer))
        // construct expected message
        val expectedMsg1 = SystemSensorMessage(
          sensor = Sensor.Temperature,
          timeStamp = failedMessage4.dateTime,
          systemId = "",
          event = "SensorFailure",
          sendingComponent = "Sitebuffer",
          data = Map())
        val expectedMsg2 = SystemSensorMessage(
          sensor = Sensor.Humidity,
          timeStamp = failedMessage4.dateTime,
          systemId = "",
          event = "SensorFailure",
          sendingComponent = "Sitebuffer",
          data = Map())
        val expectedMsg3 = SystemSensorMessage(
          sensor = Sensor.Accelerometer,
          timeStamp = failedMessage4.dateTime,
          systemId = "",
          event = "SensorFailure",
          sendingComponent = "Sitebuffer",
          data = Map())
        // send message
        iRoseSensorFailureRef ! failedMessage1
        iRoseSensorFailureRef ! failedMessage2
        iRoseSensorFailureRef ! failedMessage3
        iRoseSensorFailureRef ! failedMessage4
        // test for expected result
        val receivedMessages = testProbe.receiveN(3, 500 millis)
        receivedMessages.contains(expectedMsg1) must be(true)
        receivedMessages.contains(expectedMsg2) must be(true)
        receivedMessages.contains(expectedMsg3) must be(true)
      }
    }

    Seq("x", "y", "z").foreach(axis ⇒
      "receiving 4 consecutive failed readings (%s-axis only) of the Accelerometer sensor".format(axis) must {
        "send a SystemSensorMessage(Accelerometer, SensorFailure) to the systemSensorMessageSender" in {
          // create message
          val failedMessage1 = createSensorMessageWithAccelerometerFailure(Seq(axis))
          val failedMessage2 = createSensorMessageWithAccelerometerFailure(Seq(axis))
          val failedMessage3 = createSensorMessageWithAccelerometerFailure(Seq(axis))
          val failedMessage4 = createSensorMessageWithAccelerometerFailure(Seq(axis))
          // construct expected message
          val expectedMsg = SystemSensorMessage(
            sensor = Sensor.Accelerometer,
            timeStamp = failedMessage4.dateTime,
            systemId = "",
            event = "SensorFailure",
            sendingComponent = "Sitebuffer",
            data = Map())
          // send message
          iRoseSensorFailureRef ! failedMessage1
          iRoseSensorFailureRef ! failedMessage2
          iRoseSensorFailureRef ! failedMessage3
          iRoseSensorFailureRef ! failedMessage4
          // test for expected result
          testProbe.expectMsg(500 millis, expectedMsg)
        }
      })

    "receiving 4 consecutive failed readings (different axis) of the Accelerometer sensor" must {
      "send a SystemSensorMessage(Accelerometer, SensorFailure) to the systemSensorMessageSender" in {
        // create message
        val failedMessage1 = createSensorMessageWithAccelerometerFailure(Seq("x"))
        val failedMessage2 = createSensorMessageWithAccelerometerFailure(Seq("y"))
        val failedMessage3 = createSensorMessageWithAccelerometerFailure(Seq("z"))
        val failedMessage4 = createSensorMessageWithAccelerometerFailure(Seq("x"))
        // construct expected message
        val expectedMsg = SystemSensorMessage(
          sensor = Sensor.Accelerometer,
          timeStamp = failedMessage4.dateTime,
          systemId = "",
          event = "SensorFailure",
          sendingComponent = "Sitebuffer",
          data = Map())
        // send message
        iRoseSensorFailureRef ! failedMessage1
        iRoseSensorFailureRef ! failedMessage2
        iRoseSensorFailureRef ! failedMessage3
        iRoseSensorFailureRef ! failedMessage4
        // test for expected result
        testProbe.expectMsg(500 millis, expectedMsg)
      }
    }

    "receiving a message other than SensorMessage" must {
      "ignore this message" in {
        // create message
        val message = "Some kind of non SensorMessage"
        // send message
        iRoseSensorFailureRef ! message
        // test if expected message is received
        testProbe.expectNoMsg(1000 millis)
      }
    }

  }

  def createSensorMessageWithFailures(sensors: Seq[String]): SensorMessage = {
    def nextDatetime: Long = {
      datetime += 100
      datetime
    }
    def getTemperature: Float = if (sensors.contains(Sensor.Temperature)) 999.999F else 34
    def getHumidity: Float = if (sensors.contains(Sensor.Humidity)) 999.999F else 66
    def getAccelerometer: Accelerometer = if (sensors.contains(Sensor.Accelerometer)) Accelerometer(999.999F, 999.999F, 999.999F) else Accelerometer(1.5F, 2.5F, 3.5F)

    SensorMessage(dateTime = nextDatetime, temperature = getTemperature, humidity = getHumidity, acceleration = getAccelerometer)
  }

  def createSensorMessageWithAccelerometerFailure(axis: Seq[String]): SensorMessage = {
    def nextDatetime: Long = {
      datetime += 100
      datetime
    }
    def getXAxis: Float = if (axis.contains("x")) 999.999F else 1
    def getYAxis: Float = if (axis.contains("y")) 999.999F else 2
    def getZAxis: Float = if (axis.contains("z")) 999.999F else 3
    def getAccelerometer: Accelerometer = Accelerometer(getXAxis, getYAxis, getZAxis)

    SensorMessage(dateTime = nextDatetime, temperature = 34, humidity = 66, acceleration = getAccelerometer)
  }
}