/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.recording

import java.io.File

import akka.actor.ActorSystem
import akka.testkit.TestKit
import csc.sitebuffer.FileTestUtils
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }
import testframe.Resources

/**
 * Test van IRoseStringToDirectoryWriterCreator trait.
 */
class IRoseStringToDirectoryWriterCreatorTest extends TestKit(ActorSystem("IRoseStringToDirectoryWriterCreatorTest")) with WordSpecLike with MustMatchers
  with BeforeAndAfterEach with BeforeAndAfterAll {
  import FileTestUtils._
  import RecordingTestUtils._

  var testCreator: IRoseStringToDirectoryWriterCreator = _

  val resourceDir = new File(Resources.getResourceDirPath().getAbsolutePath)
  val testBaseDir = new File(resourceDir, "out/iRoseStringToDirectoryWriterCreator")
  if (!testBaseDir.exists()) {
    testBaseDir.mkdirs()
  } else {
    clearDirectory(testBaseDir)
  }

  override def beforeAll() {
    super.beforeAll()
  }

  override def afterAll() {
    system.shutdown()

    clearDirectory(testBaseDir)
    testBaseDir.delete()
  }

  override def afterEach() {
    clearDirectory(testBaseDir)
  }

  def createTestCreator(directory: File): IRoseStringToDirectoryWriterCreator = new IRoseStringToDirectoryWriterCreator {
    def getBaseDirectory: File = {
      directory
    }
  }

  "An IRoseStringToDirectoryWriterCreator" when {
    "createWriter is called" must {
      "must create a correctly working IRoseStringToDirectoryWriter" in {
        testCreator = createTestCreator(testBaseDir)
        val recordingId = "recordingId"
        val outputDir = new File(testBaseDir, "%s/messages".format(recordingId))
        outputDir.exists() must be(false)
        val writer = testCreator.createWriter(recordingId, system)
        Thread.sleep(50)
        outputDir.exists() must be(true)
        outputDir.listFiles().length must be(0)

        case class TestMessage(iRoseMessage: String, timestamp: Long, filename: String)
        val testMessages = Seq[TestMessage](
          TestMessage(vehicleMessage1, 1234567800, "1234567800-1-1.txt"),
          TestMessage(alarmInputEventMessage1, 1234567801, "1234567801-2.txt"),
          TestMessage(singleLoopMessage1, 1234567802, "1234567802-4.txt"),
          TestMessage(vehicleMessage2, 1234567803, "1234567803-1-2.txt"),
          TestMessage(inputEventMessage1, 1234567804, "1234567804-2.txt"),
          TestMessage(alarmInputEventMessage2, 1234567805, "1234567805-2.txt"),
          TestMessage(sensorMessage1, 1234567806, "1234567806-0.txt"),
          TestMessage(vehicleMessage3, 1234567807, "1234567807-1-3.txt"),
          TestMessage(inputEventMessage2, 1234567808, "1234567808-2.txt"),
          TestMessage(sensorMessage2, 1234567809, "1234567809-0.txt"),
          TestMessage(singleLoopMessage2, 1234567810, "1234567810-4.txt"))

        testMessages.zipWithIndex.foreach {
          case (testMessage, index) ⇒
            // Create IRoseString
            val iRoseString = createIRoseString(testMessage.iRoseMessage, testMessage.timestamp)
            writer ! iRoseString
            Thread.sleep(50)
            // Check expected results
            outputDir.listFiles().length must be(index + 1)
            getCreatedFile(directory = outputDir, filename = testMessage.filename) match {
              case Some(file) ⇒
                getContent(file) must be(iRoseString.string)

              case None ⇒
                fail("recorded file was not created")
            }
        }

      }
    }

  }

}
