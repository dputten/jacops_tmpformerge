/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.messages

import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import scala.concurrent.duration._
import csc.sitebuffer.messages.{ Accelerometer, SensorMessage }
import csc.sitebuffer.process.SystemSensorMessage
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, WordSpec }

/**
 * Test van IRoseClockMonitor
 */
class IRoseClockMonitorTest extends TestKit(ActorSystem("IRoseClockMonitorTest")) with WordSpecLike with MustMatchers with BeforeAndAfterAll {

  var testProbe: TestProbe = _
  var iRoseIsAliveRef: TestActorRef[IRoseClockMonitor] = _

  var currentTime: Long = _
  val dummyCurrentTime = () ⇒ currentTime

  override def beforeAll() {
    super.beforeAll()
  }

  override def afterAll() {
    system.shutdown
  }

  "The IRoseClockMonitor (config: outOfSyncMarginMs = 1000ms)" when {
    val testProbe = TestProbe()
    val iRoseClockMonitorRef = TestActorRef(new IRoseClockMonitor(outOfSyncMarginMs = 1000, getCurrentTime = dummyCurrentTime, systemSensorMessageSender = testProbe.ref))

    "receiving a SensorMessage with a timestamp 999ms in the past (= within the margin)" must {
      "do nothing" in {
        // create message
        val sensorMessage = createSensorMessage(2000L)
        currentTime = 2999
        // send message
        iRoseClockMonitorRef ! sensorMessage
        // test for expected result
        testProbe.expectNoMsg(1000 millis)
      }
    }

    "receiving a SensorMessage with a timestamp 1000ms in the past (= on the margin border)" must {
      "do nothing" in {
        // create message
        val sensorMessage = createSensorMessage(2000L)
        currentTime = 3000
        // send message
        iRoseClockMonitorRef ! sensorMessage
        // test for expected result
        testProbe.expectNoMsg(1000 millis)
      }
    }

    "receiving a SensorMessage with a timestamp 1001ms in the past (= outside the margin)" must {
      "send a SystemSensorMessage(Clock, Deviation) to the systemSensorMessageSender" in {
        // create message
        val sensorMessage = createSensorMessage(2000L)
        currentTime = 3001
        val expectedMsg = SystemSensorMessage(
          sensor = "Clock",
          timeStamp = 3001,
          systemId = "",
          event = "Deviation",
          sendingComponent = "Sitebuffer",
          data = Map())
        // send message
        iRoseClockMonitorRef ! sensorMessage
        // test for expected result
        testProbe.expectMsg(500 millis, expectedMsg)
      }
    }

    "receiving a SensorMessage with a timestamp 1ms in the future (= outside the margin)" must {
      "send a SystemSensorMessage(Clock, Deviation) to the systemSensorMessageSender" in {
        // create message
        val sensorMessage = createSensorMessage(2001L)
        currentTime = 2000
        val expectedMsg = SystemSensorMessage(
          sensor = "Clock",
          timeStamp = 2000,
          systemId = "",
          event = "Deviation",
          sendingComponent = "Sitebuffer",
          data = Map())
        // send message
        iRoseClockMonitorRef ! sensorMessage
        // test for expected result
        testProbe.expectMsg(500 millis, expectedMsg)
      }
    }
  }

  def createSensorMessage(dateTime: Long): SensorMessage = {
    SensorMessage(dateTime = dateTime, temperature = 12, humidity = 23, acceleration = Accelerometer(1, 2, 3))
  }

}
