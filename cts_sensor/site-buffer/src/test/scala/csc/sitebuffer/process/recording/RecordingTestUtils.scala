/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.recording

import java.io.File

import csc.sitebuffer.process.irose.IRoseString
import org.apache.commons.io.FileUtils

/**
 * Utilities for testing recording code.
 */
object RecordingTestUtils {

  def getCreatedFile(directory: File, filename: String): Option[File] = try {
    new File(directory, filename) match {
      case file ⇒
        if (file.exists())
          Some(file)
        else
          None
    }
  } catch {
    case e: Exception ⇒
      None
  }

  def getContent(file: File): String = try {
    FileUtils.readFileToString(file)
  } catch {
    case e: Exception ⇒
      ""
  }

  def createIRoseString(message: String, timestamp: Long): IRoseString = {
    IRoseString(
      string = createMessageWithCrc(message),
      timestamp = timestamp)
  }

  def createMessageWithCrc(message: String): String = {
    def calcCrc(message: String): Int = {
      message.foldLeft(0)((accu, ch) ⇒ accu ^ ch)
    }
    message + "%02X".format(calcCrc(message))
  }

  def vehicleMessage1: String = {
    createMessageWithCrc("130212;072813,123;1;1;023;500;00000;00400;00450;00875;0000,0;0000,0;130212;072813,005;0000,0;0000,0;700101;010000,000;0000,0;0000,0;")
  }

  def vehicleMessage2: String = {
    createMessageWithCrc("130212;072813,125;1;2;023;500;00000;00400;00450;00888;0000,0;0000,0;130212;072813,005;0000,0;0000,0;700101;010000,000;0000,0;0000,0;0;")
  }

  def vehicleMessage3: String = {
    createMessageWithCrc("130212;072813,123;1;3;023;500;00000;00400;00450;00875;0000,0;0000,0;130212;072813,005;0000,0;0000,0;700101;010000,000;0000,0;0000,0;0;seri;1;10;22;")
  }

  def alarmInputEventMessage1: String = {
    createMessageWithCrc("130212;072813,123;2;8;3;something;")
  }

  def alarmInputEventMessage2: String = {
    createMessageWithCrc("130212;072813,155;2;8;2;something else;")
  }

  def inputEventMessage1: String = {
    createMessageWithCrc("130212;072813,123;2;3;3;something;")
  }

  def inputEventMessage2: String = {
    createMessageWithCrc("130212;072813,155;2;3;1;something else;")
  }

  def sensorMessage1: String = {
    createMessageWithCrc("130212;072813,123;0;010,500;060,000;0003,500;-004,600;005,700;")
  }

  def sensorMessage2: String = {
    createMessageWithCrc("130212;072813,155;0;010,550;060,000;0033,500;-004,600;005,700;")
  }

  def singleLoopMessage1: String = {
    createMessageWithCrc("130212;072813,123;4;2;000;000;00000;00007;00132;00000;")
  }

  def singleLoopMessage2: String = {
    createMessageWithCrc("130212;072813,155;4;2;000;000;00000;00017;00532;00000;")
  }

}
