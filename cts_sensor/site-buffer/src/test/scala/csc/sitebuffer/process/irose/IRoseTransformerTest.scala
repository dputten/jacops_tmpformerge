package csc.sitebuffer.process.irose

/**
 * Copyright (C) 2012,2013 CSC <http://www.csc.com>.
 * User: jwluiten
 * Date: 2/5/13
 */
import csc.sitebuffer.messages._
import org.scalatest.{ MustMatchers, WordSpec }

class IRoseTransformerTest extends WordSpec with MustMatchers {

  "IRoseTransformer" must {
    "transform bogus string into Left(UnknownMessage)" in {
      val iRoseString = "bogus"
      IRoseTransformer.transform(iRoseString) match {
        case Left(msg) ⇒ msg must be(UnknownMessage("bogus"))
        case _         ⇒ fail("Message [%s] not transformed correctly".format(iRoseString))
      }
    }
  }

  "IRoseTransformer" must {
    "transform invalid message with date-time and valid checksum into Left(UnknownMessage)" in {
      val iRoseString = "120101;111111,123;bogus;48"
      IRoseTransformer.transform(iRoseString) match {
        case Left(msg) ⇒ msg must be(UnknownMessage("120101;111111,123;bogus;48"))
        case _         ⇒ fail("Message [%s] not transformed correctly".format(iRoseString))
      }
    }
  }

  "IRoseTransformer" must {
    "transform valid vehicle message (with valid = '0') into Right(VehicleMessage)" in {
      val iRoseString = createMessageWithCrc("130212;072813,123;1;1;023;500;00000;00400;00450;00875;0000,0;0000,0;" + "" +
        "130212;072813,005;0000,0;0000,0;700101;010000,000;0000,0;0000,0;0;")
      IRoseTransformer.transform(iRoseString) match {
        case Right(msg) ⇒
          //msg must be(VehicleMessage(dateTime = 1360650493123L, detector = 1, direction = Direction.Outgoing,
          msg must be(VehicleMessage(dateTime = 1360654093123L, detector = 1, direction = Direction.Outgoing,
            speed = 23.0f, length = 50.0f, loop1RiseTime = 0L, loop1FallTime = 450L,
            loop2RiseTime = 400L, loop2FallTime = 875L, yellowTime2 = 0, redTime2 = 0,
            dateTime1 = 1360654093005L, yellowTime1 = 0, redTime1 = 0, dateTime3 = 3600000L,
            yellowTime3 = 0, redTime3 = 0, valid = true))
        case _ ⇒ fail("Message [%s] not transformed correctly".format(iRoseString))
      }
    }
    "transform valid vehicle message (with valid = '1') into Right(VehicleMessage)" in {
      val iRoseString = createMessageWithCrc("130212;072813,123;1;1;023;500;00000;00400;00450;00875;0000,0;0000,0;" + "" +
        "130212;072813,005;0000,0;0000,0;700101;010000,000;0000,0;0000,0;1;")
      IRoseTransformer.transform(iRoseString) match {
        case Right(msg) ⇒
          //msg must be(VehicleMessage(dateTime = 1360650493123L, detector = 1, direction = Direction.Outgoing,
          msg must be(VehicleMessage(dateTime = 1360654093123L, detector = 1, direction = Direction.Outgoing,
            speed = 23.0f, length = 50.0f, loop1RiseTime = 0L, loop1FallTime = 450L,
            loop2RiseTime = 400L, loop2FallTime = 875L, yellowTime2 = 0, redTime2 = 0,
            dateTime1 = 1360654093005L, yellowTime1 = 0, redTime1 = 0, dateTime3 = 3600000L,
            yellowTime3 = 0, redTime3 = 0, valid = false))
        case _ ⇒ fail("Message [%s] not transformed correctly".format(iRoseString))
      }
    }
  }

  // TODO: This tests the old format. Remove once new format has been introduced.
  "IRoseTransformer" must {
    "transform valid old vehicle message (without valid field) into Right(VehicleMessage)" in {
      val iRoseString = createMessageWithCrc("130212;072813,123;1;1;023;500;00000;00400;00450;00875;0000,0;0000,0;" + "" +
        "130212;072813,005;0000,0;0000,0;700101;010000,000;0000,0;0000,0;")
      IRoseTransformer.transform(iRoseString) match {
        case Right(msg) ⇒
          //msg must be(VehicleMessage(dateTime = 1360650493123L, detector = 1, direction = Direction.Outgoing,
          msg must be(VehicleMessage(dateTime = 1360654093123L, detector = 1, direction = Direction.Outgoing,
            speed = 23.0f, length = 50.0f, loop1RiseTime = 0L, loop1FallTime = 450L,
            loop2RiseTime = 400L, loop2FallTime = 875L, yellowTime2 = 0, redTime2 = 0,
            dateTime1 = 1360654093005L, yellowTime1 = 0, redTime1 = 0, dateTime3 = 3600000L,
            yellowTime3 = 0, redTime3 = 0, valid = true))
        case _ ⇒ fail("Message [%s] not transformed correctly".format(iRoseString))
      }
    }
  }

  "IRoseTransformer" must {
    "transform valid sensor message into Right(SensorMessage)" in {
      val iRoseString = "130212;074307,005;0;001,234;002,345;0007,373;-005,579;-005,081;37"
      IRoseTransformer.transform(iRoseString) match {
        case Right(msg) ⇒ msg must be(SensorMessage(dateTime = 1360654987005L, temperature = 1.234f, humidity = 2.345f, acceleration = Accelerometer(x = 7.373f, y = -5.579f, z = -5.081f)))
        case _          ⇒ fail("Message [%s] not transformed correctly".format(iRoseString))
      }
    }
  }

  "IRoseTransformer" must {
    List(0, 1, 2, 3, 8).foreach(validInput ⇒
      "transform a valid input event message with input = '%d' into Right(InputEvent)".format(validInput) in {
        val iRoseString = createMessageWithCrc("130212;074256,002;2;%d;3;input 2 off;".format(validInput))
        println("CRC:" + iRoseString)
        IRoseTransformer.transform(iRoseString) match {
          case Right(msg) ⇒ msg must be(InputEventMessage(dateTime = 1360654976002L, inputId = validInput, eventNumber = 3, eventText = "input 2 off"))
          case _          ⇒ fail("Message [%s] not transformed correctly".format(iRoseString))
        }
      })
    List(4, 5, 6, 7, 9).foreach(validInput ⇒
      "transform a invalid input event message (input = '%d') into Left(InvalidInputEventMessage)".format(validInput) in {
        val iRoseString = createMessageWithCrc("130212;074256,002;2;%d;3;input 2 off;".format(validInput))
        IRoseTransformer.transform(iRoseString) match {
          case Left(msg) ⇒ msg must be(InvalidInputEventMessage("Error [requirement failed: inputId not in range [0,1,2,3,8]] processing message [%s]".format(iRoseString)))
          case _         ⇒ fail("Message [%s] not transformed correctly".format(iRoseString))
        }
      })
  }

  "IRoseTransformer" must {
    "transform valid single loop message into Right(SingleLoopMessage)" in {
      val iRoseString = "130212;074259,002;4;2;000;000;00000;00520;00111;00000;10"
      IRoseTransformer.transform(iRoseString) match {
        case Right(msg) ⇒ msg must be(SingleLoopMessage(dateTime = 1360654979002L, loopId = 2, lastScanCycle = 520, coverTime = 111))
        case _          ⇒ fail("Message [%s] not transformed correctly".format(iRoseString))
      }
    }
  }

  "IRoseTransformer" must {
    "transform a correct InputEventMessage" in {
      val epoch = (Date("140719;115651,185")getTime)
      val iRoseString = createMessageWithCrc("140719;115651,185;2;8;1;input 2 off;")
      val x = IRoseTransformer.transform(iRoseString)

      println(x)
      IRoseTransformer.transform(iRoseString) match {
        case Right(msg) ⇒ msg must be(InputEventMessage(dateTime = epoch, inputId = 8, eventNumber = 1, eventText = "input 2 off"))
        case _          ⇒ fail("Message [%s] not transformed correctly".format("fpi"))
      }
    }
  }

  "IRoseTransformer" must {
    "transform a correct Alarm InputEventMessage into Right(InputEventMessage)" in {
      val epoch = (Date("140719;115651,185")getTime)
      val iRoseString = createMessageWithCrc("140719;115651,185;2;8;1;")

      IRoseTransformer.transform(iRoseString) match {
        case Right(msg) ⇒ msg must be(InputEventMessage(dateTime = epoch, inputId = 8, eventNumber = 1, eventText = ""))
        case _          ⇒ fail("Message [%s] not transformed correctly".format("fpi"))
      }
    }
  }

  "IRoseTransformer" must {
    "prove that the old code could not handle alarms. No match with current regex." in {
      val iRoseString = createMessageWithCrc("140719;115651,185;2;8;1;")

      iRoseString match {
        case IRoseTransformer.input_event_re(date_time, input, event, text) ⇒
          fail("this should not happen")
        case _ ⇒ Left(UnknownMessage(iRoseString))
      }
    }
  }

  "IRoseTransformer" must {
    "prove that the new code could can handle alarms." in {
      val iRoseString = createMessageWithCrc("140719;115651,185;2;8;1;")
      iRoseString match {
        case IRoseTransformer.input_event_alarm(date_time, input, event) ⇒
          println("success")
        case _ ⇒
          fail("this should not happen")
      }
    }
  }

  /**
   * NOT funny! DateFormat.parse rolls over when month > 12
   * "IRoseTransformer" must {
   * "return None for invalid date" in {
   * val someMessage: Option[IRoseMessage] = IRoseTransformer.transform("691301;010101,111;0;")
   * someMessage match {
   * case Some(msg) ⇒ assert(false)
   * case None      ⇒ assert(true)
   * }
   * }
   * }
   */

  /*
  "IRoseTransformer" must {
    "transform proper string into Sensor" in {
      val someMessage: IRoseMessage = IRoseTransformer.transform("691201;010101,111;0;")
      someMessage.isInstanceOf[SensorMessage] must be(true)
    }
  }
*/

  /*
  "IRoseTransformer" must {
    "transform proper string into VehicleMessage" in {
      val someMessage: Option[IRoseMessage] =
        IRoseTransformer.transform("691201;010101,111;1;0;150;035;12345;23456;34567;45678")
      someMessage match {
        case Some(msg) ⇒ {
          msg match {
            case m: VehicleMessage ⇒ {
              m.direction must be(Direction.Outgoing)
              m.speed must be(150.0f)
              m.length must be(3.5f)
              m.loop1RiseTime must be(12345)
              m.loop2RiseTime must be(23456)
              m.loop1FallTime must be(34567)
              m.loop2FallTime must be(45678)
            }
            case _ ⇒ assert(false)
          }
        }
        case _ ⇒ assert(false)
      }
    }
  }
*/
  /*
  "IRoseTransformer" must {
    "transform proper string with negative speed into VehicleMessage" in {
      val someMessage: Option[IRoseMessage] =
        IRoseTransformer.transform("691201;010101,111;1;0;-150;035;10000;20000;30000;40000")
      someMessage match {
        case Some(msg) ⇒ {
          msg match {
            case m: VehicleMessage ⇒ {
              m.detector must be(0)
              m.direction must be(Direction.Incoming)
              m.speed must be(150.0f)
              m.length must be(3.5f)
              m.loop1RiseTime must be(10000)
              m.loop2RiseTime must be(20000)
              m.loop1FallTime must be(30000)
              m.loop2FallTime must be(40000)
            }
            case _ ⇒ assert(false)
          }
        }
        case _ ⇒ assert(false)
      }
    }
  }
  */

  def createMessageWithCrc(message: String): String = {
    def calcCrc(message: String): Int = {
      message.foldLeft(0)((accu, ch) ⇒ accu ^ ch)
    }
    message + "%02X".format(calcCrc(message))
  }
}

