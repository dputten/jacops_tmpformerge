/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.messages

import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import scala.concurrent.duration._
import csc.akka.logging.DirectLogging
import csc.akka.process.{ ExecuteScript, ExecuteScriptResult }
import csc.sitebuffer.messages.SingleLoopMessage
import csc.sitebuffer.process.SystemSensorMessage
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }

/**
 * Test van IRoseLoopFailureMonitor
 */
class IRoseLoopFailureMonitorTest extends TestKit(ActorSystem("IRoseLoopFailureMonitorTest")) with WordSpecLike with MustMatchers with DirectLogging
  with BeforeAndAfterEach with BeforeAndAfterAll {

  var datetime = 100000000L

  var mockCurrentTime: Long = _
  val dummyCurrentTime = () ⇒ mockCurrentTime

  var testProbe: TestProbe = _
  var testProbeExecuteScriptActor: TestProbe = TestProbe()
  var iRoseLoopFailureActor: TestActorRef[IRoseLoopFailureMonitor] = _

  override def beforeAll() {
    super.beforeAll()
  }

  override def afterAll() {
    system.shutdown()
  }

  override def beforeEach() {
    log.debug("beforeEach")
    testProbe = TestProbe()
    testProbeExecuteScriptActor = TestProbe()
    iRoseLoopFailureActor = TestActorRef(
      new IRoseLoopFailureMonitor(maxAllowedMessages = 3, testWindowMs = 100, iRoseRebootScriptPath = "testpath", getCurrentTime = dummyCurrentTime, systemSensorMessageSender = testProbe.ref, executeScriptActor = testProbeExecuteScriptActor.ref))
  }

  def sendMessage(loopId: Int, currentTime: Long) {
    mockCurrentTime = currentTime
    iRoseLoopFailureActor ! createSingleLoopMessage(loopId)
  }

  def sendMessageExecuteScriptResult() {
    iRoseLoopFailureActor ! new ExecuteScriptResult(script = new ExecuteScript("", Seq()), resultCode = Right(0), stdOut = Seq(), stdError = Seq())
  }

  "The IRoseLoopFailureMonitor (maxAllowedMessages = 3, sampleDurationMs = 100)" when {

    "receiving 3 SingleLoop messages for the same loop in exactly 99 seconds" must {
      "do nothing" in {
        // send message
        sendMessage(loopId = 1, currentTime = 1)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendMessage(loopId = 1, currentTime = 50)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendMessage(loopId = 1, currentTime = 99)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
      }
    }

    "receiving 3 SingleLoop messages for the same loop in exactly 100 seconds" must {
      "do nothing" in {
        // send message
        sendMessage(loopId = 1, currentTime = 1)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendMessage(loopId = 1, currentTime = 50)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendMessage(loopId = 1, currentTime = 100)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
      }
    }

    "receiving 4 SingleLoop messages for the same loop in exactly 101 seconds" must {
      "do nothing" in {
        // send message
        sendMessage(loopId = 1, currentTime = 1)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendMessage(loopId = 1, currentTime = 50)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendMessage(loopId = 1, currentTime = 100)
        // test for expected result
        testProbe.expectNoMsg(500 millis)

        // send message
        sendMessage(loopId = 1, currentTime = 101)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
      }
    }

    "receiving 3 SingleLoop messages for the same loop in exactly 101 seconds" must {
      "do nothing" in {
        // send message
        sendMessage(loopId = 1, currentTime = 1)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendMessage(loopId = 1, currentTime = 50)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendMessage(loopId = 1, currentTime = 101)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
      }
    }

    "receiving 4 SingleLoop messages for the same loop in exactly 99 seconds" must {
      "send a SystemSensorMessage(DetectionLoop, SensorFailure) to the systemSensorMessageSender " +
        "and generate a irose reboot" in {
          val expectedMsgForExecuteScriptActor = new ExecuteScript("testpath", Seq())
          val expectedMsg = SystemSensorMessage(
            sensor = "DetectionLoop",
            timeStamp = 99,
            systemId = "",
            event = "SensorFailure",
            sendingComponent = "Sitebuffer",
            data = Map("loopId" -> "1"))
          // send message
          sendMessage(loopId = 1, currentTime = 1)
          // test for expected result
          testProbe.expectNoMsg(500 millis)
          // send message
          sendMessage(loopId = 1, currentTime = 50)
          // test for expected result
          testProbe.expectNoMsg(500 millis)
          // send message
          sendMessage(loopId = 1, currentTime = 70)
          // test for expected result
          testProbe.expectNoMsg(500 millis)
          // send message
          sendMessage(loopId = 1, currentTime = 99)
          // test for expected result
          testProbe.expectMsg(500 millis, expectedMsg)
          testProbeExecuteScriptActor.expectMsg(500 millis, expectedMsgForExecuteScriptActor)

          // send message
          sendMessage(loopId = 1, currentTime = 70)
          // test for expected result - should produce a log.info - 'iRose is rebooting, count is temporarily disabled.'
          testProbe.expectNoMsg(500 millis)

          sendMessageExecuteScriptResult

          // send message
          sendMessage(loopId = 1, currentTime = 1)
          // test for expected result
          testProbe.expectNoMsg(500 millis)

        }
    }

    "receiving 4 SingleLoop messages for the same loop in exactly 100 seconds" must {
      "send a SystemSensorMessage(DetectionLoop, SensorFailure) to the systemSensorMessageSender " +
        "and generate a irose reboot" in {
          val expectedMsgForExecuteScriptActor = new ExecuteScript("testpath", Seq())
          val expectedMsg = SystemSensorMessage(
            sensor = "DetectionLoop",
            timeStamp = 100,
            systemId = "",
            event = "SensorFailure",
            sendingComponent = "Sitebuffer",
            data = Map("loopId" -> "1"))

          // send message
          sendMessage(loopId = 1, currentTime = 1)
          // test for expected result
          testProbe.expectNoMsg(500 millis)
          // send message
          sendMessage(loopId = 1, currentTime = 50)
          // test for expected result
          testProbe.expectNoMsg(500 millis)
          // send message
          sendMessage(loopId = 1, currentTime = 70)
          // test for expected result
          testProbe.expectNoMsg(500 millis)
          // send message
          sendMessage(loopId = 1, currentTime = 100)
          // test for expected result
          testProbe.expectMsg(500 millis, expectedMsg)

          testProbeExecuteScriptActor.expectMsg(500 millis, expectedMsgForExecuteScriptActor)

          // send message
          sendMessage(loopId = 1, currentTime = 105)
          // test for expected result - should produce a log.info - 'iRose is rebooting, count is temporarily disabled.'
          testProbe.expectNoMsg(500 millis)

          sendMessageExecuteScriptResult

          // send message
          sendMessage(loopId = 1, currentTime = 1)
          // test for expected result
          testProbe.expectNoMsg(500 millis)
        }
    }

    "receiving 4 SingleLoop messages for different loops in exactly 100 seconds" must {
      "do nothing" in {
        // send message
        sendMessage(loopId = 1, currentTime = 1)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendMessage(loopId = 2, currentTime = 50)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendMessage(loopId = 3, currentTime = 70)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendMessage(loopId = 4, currentTime = 100)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
      }
    }

    // Let op!! loopid2 begint te tellen op currentTime = 2
    // Het eerste loopid wat een irose reboot genereert zal alle counters voor de loopids resetten.
    // Want alle loopids zitten op dezelfde irose.
    // Daarom ten opzichte van de orginele test nog maar 1 SystemSensorMessage
    "receiving 4 SingleLoop messages each for 2 different loops in exactly 100 seconds" must {
      "send a 1-SystemSensorMessage(DetectionLoop, SensorFailure) to the systemSensorMessageSender and generate a irose reboot" in {
        val expectedMsgForExecuteScriptActor = new ExecuteScript("testpath", Seq())
        val expectedMsg1 = SystemSensorMessage(
          sensor = "DetectionLoop",
          timeStamp = 100,
          systemId = "",
          event = "SensorFailure",
          sendingComponent = "Sitebuffer",
          data = Map("loopId" -> "1"))

        // send message
        sendMessage(loopId = 1, currentTime = 1)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendMessage(loopId = 2, currentTime = 2)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendMessage(loopId = 1, currentTime = 50)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendMessage(loopId = 2, currentTime = 51)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendMessage(loopId = 1, currentTime = 70)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendMessage(loopId = 2, currentTime = 71)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendMessage(loopId = 1, currentTime = 100)
        // test for expected result
        testProbe.expectMsg(500 millis, expectedMsg1)

        // send message
        sendMessage(loopId = 2, currentTime = 101)
        testProbe.expectNoMsg(500 millis)

        testProbeExecuteScriptActor.expectMsg(500 millis, expectedMsgForExecuteScriptActor)

        // send message
        sendMessage(loopId = 1, currentTime = 105)
        // test for expected result - should produce a log.info - 'iRose is rebooting, count is temporarily disabled.'
        testProbe.expectNoMsg(500 millis)

        sendMessageExecuteScriptResult //geeft einde reboot irose aan.

        // send message
        sendMessage(loopId = 1, currentTime = 1)
        // test for expected result
        testProbe.expectNoMsg(500 millis)

      }
    }

    "receiving 7 SingleLoop messages for the same loop in exactly 100 seconds" must {
      "send 1 SystemSensorMessage(DetectionLoop, SensorFailure) to the systemSensorMessageSender and generate a irose reboot" in {
        val expectedMsg = SystemSensorMessage(
          sensor = "DetectionLoop",
          timeStamp = 40,
          systemId = "",
          event = "SensorFailure",
          sendingComponent = "Sitebuffer",
          data = Map("loopId" -> "1"))
        // send message
        sendMessage(loopId = 1, currentTime = 1)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendMessage(loopId = 1, currentTime = 20)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendMessage(loopId = 1, currentTime = 30)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendMessage(loopId = 1, currentTime = 40)
        // test for expected result - is nr4 within timewindow
        testProbe.expectMsg(500 millis, expectedMsg)
        // send message
        sendMessage(loopId = 1, currentTime = 60)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendMessage(loopId = 1, currentTime = 80)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendMessage(loopId = 1, currentTime = 100)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
      }
    }

    "receiving 8 SingleLoop messages for the same loop in exactly 100 seconds" must {
      "send 2 SystemSensorMessage(DetectionLoop, SensorFailure) to the systemSensorMessageSender and generate 2 irose reboots" in {
        val expectedMsgForExecuteScriptActor = new ExecuteScript("testpath", Seq())
        val expectedMsg1 = SystemSensorMessage(
          sensor = "DetectionLoop",
          timeStamp = 40,
          systemId = "",
          event = "SensorFailure",
          sendingComponent = "Sitebuffer",
          data = Map("loopId" -> "1"))
        val expectedMsg2 = SystemSensorMessage(
          sensor = "DetectionLoop",
          timeStamp = 100,
          systemId = "",
          event = "SensorFailure",
          sendingComponent = "Sitebuffer",
          data = Map("loopId" -> "1"))
        // send message
        sendMessage(loopId = 1, currentTime = 1)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendMessage(loopId = 1, currentTime = 20)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendMessage(loopId = 1, currentTime = 30)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendMessage(loopId = 1, currentTime = 40)
        // test for expected result
        testProbe.expectMsg(500 millis, expectedMsg1)

        testProbeExecuteScriptActor.expectMsg(500 millis, expectedMsgForExecuteScriptActor)
        sendMessageExecuteScriptResult //geeft einde reboot irose aan.

        // send message
        sendMessage(loopId = 1, currentTime = 60)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendMessage(loopId = 1, currentTime = 80)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendMessage(loopId = 1, currentTime = 90)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendMessage(loopId = 1, currentTime = 100)
        // test for expected result
        testProbe.expectMsg(500 millis, expectedMsg2)

        testProbeExecuteScriptActor.expectMsg(500 millis, expectedMsgForExecuteScriptActor)
        sendMessageExecuteScriptResult //geeft einde reboot irose aan.
      }
    }

  }

  def createSingleLoopMessage(loopId: Int): SingleLoopMessage = {
    def nextDatetime: Long = {
      datetime += 10
      datetime
    }
    SingleLoopMessage(nextDatetime, loopId, 10, 20)
  }

}
