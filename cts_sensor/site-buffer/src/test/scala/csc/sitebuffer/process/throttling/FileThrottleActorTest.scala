package csc.sitebuffer.process.throttling

import java.io.File

import akka.actor.{ ActorSystem, Props }
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import csc.sitebuffer.config.FileThrottleConfig
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }
import csc.sitebuffer.FileTestUtils._
import testframe.Resources

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 4/16/14.
 */
class FileThrottleActorTest extends TestKit(ActorSystem("FileThrottleActorTest")) with WordSpecLike with MustMatchers
  with BeforeAndAfterEach with BeforeAndAfterAll {

  val resourceDir = new File(Resources.getResourceDirPath().getAbsolutePath)
  val throttleResourceDir = new File(resourceDir, "csc/sitebuffer/process/throttling")

  val testBaseDir = new File(resourceDir, "throttleTest")
  if (!testBaseDir.exists()) {
    testBaseDir.mkdirs()
  } else {
    clearDirectory(testBaseDir)
  }
  val testOutputDir = new File(testBaseDir, "output")
  val testInputDir = new File(testBaseDir, "input")

  val testProbe = TestProbe()
  val conf = FileThrottleConfig(testInputDir, testOutputDir, "TA[0-9]", "PA", "[0-9]{3}\\.json", 5, 1000)
  val throttleActor = TestActorRef(Props(new FileThrottleActor(conf)))
  testProbe watch (throttleActor)

  override def beforeAll() {
    super.beforeAll()
    clearDirectory(testBaseDir)
    prepareInputFiles()
  }

  override def afterAll() {
    //clearDirectory(testBaseDir)
    //testBaseDir.delete()
    system.shutdown()
    super.afterAll()
  }

  override def beforeEach() {
    prepareInputFiles()
  }

  override def afterEach() {
    //clearDirectory(testBaseDir)
  }

  private def prepareInputFiles(): Unit = {
    prepareInputDir(new File(testInputDir, "TA0"))
    prepareInputDir(new File(testInputDir, "TA1"))
    prepareInputDir(new File(testInputDir, "TA2"))
  }

  def copyTestResource(resourceName: String, toFilePath: File): File = {
    val fromFile = new File(throttleResourceDir, resourceName)
    copyFile(fromFile, toFilePath)
  }

  private def prepareInputDir(dir: File): Unit = {
    if (!dir.exists()) dir.mkdirs()
    copyTestResource("textFile.txt", new File(dir, "001.json"))
    copyTestResource("textFile.txt", new File(dir, "002.json"))
    copyTestResource("textFile.txt", new File(dir, "003.json"))
    copyTestResource("textFile.txt", new File(dir, "004.json"))
    copyTestResource("textFile.txt", new File(dir, "005.json"))
    copyTestResource("textFile.txt", new File(dir, "006.json"))
    copyTestResource("textFile.txt", new File(dir, "007.json"))
    copyTestResource("textFile.txt", new File(dir, "008.json"))
    copyTestResource("textFile.txt", new File(dir, "009.json"))
    copyTestResource("textFile.txt", new File(dir, "010.json"))
    copyTestResource("textFile.txt", new File(dir, "011.json"))
    copyTestResource("textFile.txt", new File(dir, "012.json"))
  }

  private def testOutput(dir: File, names: Seq[String]): Unit = {
    dir.exists must be(true)
    val files = dir.listFiles().sortBy({ f ⇒ f.getName })
    files.length must be(names.length)
    files.foreach({ f ⇒ names.contains(f.getName) must be(true) })
  }

  private def deleteFiles(dir: File, files: Seq[String]): Unit = {
    files.foreach({ name ⇒ deleteFile(new File(dir, name)) })
  }
  "A FileThrottleActor, receiving a PollCommand," when {

    "there are files to be moved" must {
      "move files to their destination" in {
        val output0 = new File(testOutputDir, "PA0")
        val output1 = new File(testOutputDir, "PA1")
        val output2 = new File(testOutputDir, "PA2")
        val names = Seq("001.json", "002.json", "003.json", "004.json", "005.json",
          "006.json", "007.json", "008.json", "009.json", "010.json", "011.json", "012.json")
        val names1 = names.dropRight(7)
        testOutputDir.exists must be(false)
        throttleActor ! PollCommand
        testOutput(output0, names1)
        testOutput(output1, names1)
        testOutput(output2, names1)

        val toDelete = names.dropRight(10)
        deleteFiles(output0, toDelete)
        deleteFiles(output1, toDelete)
        deleteFiles(output2, toDelete)

        val names2 = names.drop(2).dropRight(5)
        throttleActor ! PollCommand
        testOutput(output0, names2)
        testOutput(output1, names2)
        testOutput(output2, names2)
        clearDirectory(output0)
        clearDirectory(output1)
        clearDirectory(output2)

        val names3 = names.drop(7)
        throttleActor ! PollCommand
        testOutput(output0, names3)
        testOutput(output1, names3)
        testOutput(output2, names3)
      }
    }
  }

}
