/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.monitoring

import akka.actor.FSM.{ CurrentState, SubscribeTransitionCallBack, Transition }
import akka.actor.{ ActorSystem, Props }
import scala.concurrent.Future
import akka.testkit.{ TestKit, TestProbe }
import scala.concurrent.duration._
import csc.sitebuffer.messages.{ Direction, VehicleMessage }
import csc.sitebuffer.process.images.NewImage
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, WordSpec }

class CameraStateMonitorTest extends TestKit(ActorSystem("CameraStateMonitorTest"))
  with WordSpecLike with MustMatchers with BeforeAndAfterAll {

  implicit val executor = system.dispatcher

  override def afterAll() {
    super.afterAll()
    system.shutdown()
  }

  val startTime = System.currentTimeMillis()

  "CameraStateMonitor" must {
    "Stay in Normal when receiving Images" in {
      val probeMsg = TestProbe()
      val probeState = TestProbe()
      val testActor = system.actorOf(Props(
        new CameraStateMonitor(camera = "testCam",
          maxWaitTimeForImage = 1.second,
          maxErrorTimeForImage = 3.seconds,
          recipients = Set(probeMsg.ref))))
      testActor ! SubscribeTransitionCallBack(probeState.ref)

      probeState.expectMsg(CurrentState(testActor, Normal))
      testActor ! new NewImage(imagePath = "/data/images/A0/image1.jpg", timestamp = startTime)
      probeState.expectNoMsg(1.second)
      testActor ! new NewImage(imagePath = "/data/images/A0/image1.jpg", timestamp = startTime + 200)
      probeState.expectNoMsg(1.second)
      system.stop(testActor)
    }
    "Stay in Normal when receiving Image before vehicle" in {
      val probeMsg = TestProbe()
      val probeState = TestProbe()
      val testActor = system.actorOf(Props(
        new CameraStateMonitor(camera = "testCam",
          maxWaitTimeForImage = 1.second,
          maxErrorTimeForImage = 3.seconds,
          recipients = Set(probeMsg.ref))))
      testActor ! SubscribeTransitionCallBack(probeState.ref)

      probeState.expectMsg(CurrentState(testActor, Normal))
      testActor ! new NewImage(imagePath = "/data/images/A0/image1.jpg", timestamp = startTime)
      testActor ! new VehicleMessage(dateTime = startTime + 100, detector = 0, direction = Direction.Outgoing,
        speed = 40, length = 47, loop1RiseTime = 0, loop1FallTime = 50,
        loop2RiseTime = 100, loop2FallTime = 150, yellowTime2 = 0, redTime2 = 0,
        dateTime1 = startTime, yellowTime1 = 0, redTime1 = 0, dateTime3 = startTime + 200,
        yellowTime3 = 0, redTime3 = 0, valid = true,
        serialNr = "1234", offset1 = 0, offset2 = 0, offset3 = 0)

      probeState.expectNoMsg(1.second)
      system.stop(testActor)
    }
    "Stay in Normal when receiving invalid vehicle" in {
      val probeMsg = TestProbe()
      val probeState = TestProbe()
      val testActor = system.actorOf(Props(
        new CameraStateMonitor(camera = "testCam",
          maxWaitTimeForImage = 1.second,
          maxErrorTimeForImage = 3.seconds,
          recipients = Set(probeMsg.ref))))
      testActor ! SubscribeTransitionCallBack(probeState.ref)

      probeState.expectMsg(CurrentState(testActor, Normal))
      testActor ! new VehicleMessage(dateTime = startTime + 100, detector = 0, direction = Direction.Outgoing,
        speed = 40, length = 47, loop1RiseTime = 0, loop1FallTime = 50,
        loop2RiseTime = 100, loop2FallTime = 150, yellowTime2 = 0, redTime2 = 0,
        dateTime1 = startTime, yellowTime1 = 0, redTime1 = 0, dateTime3 = startTime + 200,
        yellowTime3 = 0, redTime3 = 0, valid = false,
        serialNr = "1234", offset1 = 0, offset2 = 0, offset3 = 0)

      probeState.expectNoMsg(1.second)
      system.stop(testActor)
    }
    "Move to expecting image and back to Normal when receiving vehicle before image" in {
      val probeMsg = TestProbe()
      val probeState = TestProbe()
      val testActor = system.actorOf(Props(
        new CameraStateMonitor(camera = "testCam",
          maxWaitTimeForImage = 1.second,
          maxErrorTimeForImage = 3.seconds,
          recipients = Set(probeMsg.ref))))
      testActor ! SubscribeTransitionCallBack(probeState.ref)
      probeState.expectMsg(CurrentState(testActor, Normal))

      testActor ! new VehicleMessage(dateTime = startTime + 100, detector = 0, direction = Direction.Outgoing,
        speed = 40, length = 47, loop1RiseTime = 0, loop1FallTime = 50,
        loop2RiseTime = 100, loop2FallTime = 150, yellowTime2 = 0, redTime2 = 0,
        dateTime1 = startTime, yellowTime1 = 0, redTime1 = 0, dateTime3 = startTime + 200,
        yellowTime3 = 0, redTime3 = 0, valid = true,
        serialNr = "1234", offset1 = 0, offset2 = 0, offset3 = 0)

      probeState.expectMsg(Transition(testActor, Normal, ExpectImage))
      testActor ! new NewImage(imagePath = "/data/images/A0/image1.jpg", timestamp = startTime)
      probeState.expectMsg(Transition(testActor, ExpectImage, Normal))
      system.stop(testActor)
    }
    "Move to all states" in {
      //Normal (VehicleMsg)-> ExpectImage (timeOut)-> SendError (vehicleMsg) -> Error (timeout)-> SendError (NewImage)-> Normal
      val probeMsg = TestProbe()
      val probeState = TestProbe()
      val testActor = system.actorOf(Props(
        new CameraStateMonitor(camera = "testCam",
          maxWaitTimeForImage = 1.second,
          maxErrorTimeForImage = 3.seconds,
          recipients = Set(probeMsg.ref))))
      testActor ! SubscribeTransitionCallBack(probeState.ref)
      probeState.expectMsg(CurrentState(testActor, Normal))

      testActor ! new VehicleMessage(dateTime = startTime + 100, detector = 0, direction = Direction.Outgoing,
        speed = 40, length = 47, loop1RiseTime = 0, loop1FallTime = 50,
        loop2RiseTime = 100, loop2FallTime = 150, yellowTime2 = 0, redTime2 = 0,
        dateTime1 = startTime, yellowTime1 = 0, redTime1 = 0, dateTime3 = startTime + 200,
        yellowTime3 = 0, redTime3 = 0, valid = true,
        serialNr = "1234", offset1 = 0, offset2 = 0, offset3 = 0)
      probeState.expectMsg(Transition(testActor, Normal, ExpectImage))
      probeState.expectMsg(Transition(testActor, ExpectImage, SendError))

      var recv = probeMsg.expectMsgType[CameraImageOutputError](100.millis)
      recv.camera must be("testCam")
      recv.nr must be(1)
      recv.startTime must be(0L)

      testActor ! new VehicleMessage(dateTime = startTime + 1100, detector = 0, direction = Direction.Outgoing,
        speed = 40, length = 47, loop1RiseTime = 0, loop1FallTime = 50,
        loop2RiseTime = 100, loop2FallTime = 150, yellowTime2 = 0, redTime2 = 0,
        dateTime1 = startTime + 1000, yellowTime1 = 0, redTime1 = 0, dateTime3 = startTime + 1200,
        yellowTime3 = 0, redTime3 = 0, valid = true,
        serialNr = "1234", offset1 = 0, offset2 = 0, offset3 = 0)

      probeState.expectMsg(Transition(testActor, SendError, Error))

      probeState.expectMsg(4.seconds, Transition(testActor, Error, SendError))

      recv = probeMsg.expectMsgType[CameraImageOutputError](100.millis)
      recv.camera must be("testCam")
      recv.nr must be(2)
      recv.startTime must be(0L)

      testActor ! new NewImage(imagePath = "/data/images/A0/image1.jpg", timestamp = startTime + 1000)
      probeState.expectMsg(Transition(testActor, SendError, Normal))
      probeMsg.expectMsg(new CameraImageOutputFixed(camera = "testCam", startTime = 0L, endTime = startTime + 1000, nr = 2))
      system.stop(testActor)
    }
    "Ignore old Images" in {
      //Normal (VehicleMsg)-> ExpectImage (timeOut)-> SendError -> Error (timeout)-> SendError -> Error (NewImage)-> Normal
      val probeMsg = TestProbe()
      val probeState = TestProbe()
      val testActor = system.actorOf(Props(
        new CameraStateMonitor(camera = "testCam",
          maxWaitTimeForImage = 3.second,
          maxErrorTimeForImage = 3.seconds,
          recipients = Set(probeMsg.ref))))
      testActor ! SubscribeTransitionCallBack(probeState.ref)
      probeState.expectMsg(CurrentState(testActor, Normal))

      testActor ! new NewImage(imagePath = "/data/images/A0/image1.jpg", timestamp = startTime - 1000)

      testActor ! new VehicleMessage(dateTime = startTime + 100, detector = 0, direction = Direction.Outgoing,
        speed = 40, length = 47, loop1RiseTime = 0, loop1FallTime = 50,
        loop2RiseTime = 100, loop2FallTime = 150, yellowTime2 = 0, redTime2 = 0,
        dateTime1 = startTime, yellowTime1 = 0, redTime1 = 0, dateTime3 = startTime + 200,
        yellowTime3 = 0, redTime3 = 0, valid = true,
        serialNr = "1234", offset1 = 0, offset2 = 0, offset3 = 0)
      probeState.expectMsg(Transition(testActor, Normal, ExpectImage))
      testActor ! new NewImage(imagePath = "/data/images/A0/image1.jpg", timestamp = startTime - 500)
      probeState.expectNoMsg(1.second)
      testActor ! new NewImage(imagePath = "/data/images/A0/image1.jpg", timestamp = startTime - 1)
      probeState.expectNoMsg(1.second)
      testActor ! new NewImage(imagePath = "/data/images/A0/image1.jpg", timestamp = startTime)
      probeState.expectMsg(Transition(testActor, ExpectImage, Normal))
      system.stop(testActor)
    }
    "Move from error to normal" in {
      //Normal (VehicleMsg)-> ExpectImage (timeOut)-> SendError (vehicleMsg) -> Error (NewImage)-> Normal
      val probeMsg = TestProbe()
      val probeState = TestProbe()
      val testActor = system.actorOf(Props(
        new CameraStateMonitor(camera = "testCam",
          maxWaitTimeForImage = 1.second,
          maxErrorTimeForImage = 3.seconds,
          recipients = Set(probeMsg.ref))))
      testActor ! SubscribeTransitionCallBack(probeState.ref)
      probeState.expectMsg(CurrentState(testActor, Normal))

      testActor ! new VehicleMessage(dateTime = startTime + 100, detector = 0, direction = Direction.Outgoing,
        speed = 40, length = 47, loop1RiseTime = 0, loop1FallTime = 50,
        loop2RiseTime = 100, loop2FallTime = 150, yellowTime2 = 0, redTime2 = 0,
        dateTime1 = startTime, yellowTime1 = 0, redTime1 = 0, dateTime3 = startTime + 200,
        yellowTime3 = 0, redTime3 = 0, valid = true,
        serialNr = "1234", offset1 = 0, offset2 = 0, offset3 = 0)
      probeState.expectMsg(Transition(testActor, Normal, ExpectImage))
      probeState.expectMsg(Transition(testActor, ExpectImage, SendError))

      val recv = probeMsg.expectMsgType[CameraImageOutputError](100.millis)
      recv.camera must be("testCam")
      recv.nr must be(1)
      recv.startTime must be(0L)

      testActor ! new VehicleMessage(dateTime = startTime + 1100, detector = 0, direction = Direction.Outgoing,
        speed = 40, length = 47, loop1RiseTime = 0, loop1FallTime = 50,
        loop2RiseTime = 100, loop2FallTime = 150, yellowTime2 = 0, redTime2 = 0,
        dateTime1 = startTime + 1000, yellowTime1 = 0, redTime1 = 0, dateTime3 = startTime + 1200,
        yellowTime3 = 0, redTime3 = 0, valid = true,
        serialNr = "1234", offset1 = 0, offset2 = 0, offset3 = 0)

      probeState.expectMsg(Transition(testActor, SendError, Error))

      testActor ! new NewImage(imagePath = "/data/images/A0/image1.jpg", timestamp = startTime + 1000)
      probeState.expectMsg(Transition(testActor, Error, Normal))
      probeMsg.expectMsg(new CameraImageOutputFixed(camera = "testCam", startTime = 0L, endTime = startTime + 1000, nr = 1))
      system.stop(testActor)
    }
    "Move from SendError to normal" in {
      //Normal (VehicleMsg)-> ExpectImage (timeOut)-> SendError (NewImage)-> Normal
      val probeMsg = TestProbe()
      val probeState = TestProbe()
      val testActor = system.actorOf(Props(
        new CameraStateMonitor(camera = "testCam",
          maxWaitTimeForImage = 1.second,
          maxErrorTimeForImage = 3.seconds,
          recipients = Set(probeMsg.ref))))
      testActor ! SubscribeTransitionCallBack(probeState.ref)
      probeState.expectMsg(CurrentState(testActor, Normal))

      testActor ! new VehicleMessage(dateTime = startTime + 100, detector = 0, direction = Direction.Outgoing,
        speed = 40, length = 47, loop1RiseTime = 0, loop1FallTime = 50,
        loop2RiseTime = 100, loop2FallTime = 150, yellowTime2 = 0, redTime2 = 0,
        dateTime1 = startTime, yellowTime1 = 0, redTime1 = 0, dateTime3 = startTime + 200,
        yellowTime3 = 0, redTime3 = 0, valid = true,
        serialNr = "1234", offset1 = 0, offset2 = 0, offset3 = 0)
      probeState.expectMsg(Transition(testActor, Normal, ExpectImage))
      probeState.expectMsg(Transition(testActor, ExpectImage, SendError))

      val recv = probeMsg.expectMsgType[CameraImageOutputError](100.millis)
      recv.camera must be("testCam")
      recv.nr must be(1)
      recv.startTime must be(0L)

      testActor ! new NewImage(imagePath = "/data/images/A0/image1.jpg", timestamp = startTime + 1000)
      probeState.expectMsg(Transition(testActor, SendError, Normal))
      probeMsg.expectMsg(new CameraImageOutputFixed(camera = "testCam", startTime = 0L, endTime = startTime + 1000, nr = 1))
      system.stop(testActor)
    }
    "Keep track of start failure" in {
      //Normal (VehicleMsg)-> ExpectImage (timeOut)-> SendError (NewImage)-> Normal
      val probeMsg = TestProbe()
      val probeState = TestProbe()
      val testActor = system.actorOf(Props(
        new CameraStateMonitor(camera = "testCam",
          maxWaitTimeForImage = 1.second,
          maxErrorTimeForImage = 3.seconds,
          recipients = Set(probeMsg.ref))))
      testActor ! SubscribeTransitionCallBack(probeState.ref)
      probeState.expectMsg(CurrentState(testActor, Normal))

      testActor ! new NewImage(imagePath = "/data/images/A0/image1.jpg", timestamp = startTime - 1000)

      testActor ! new VehicleMessage(dateTime = startTime + 100, detector = 0, direction = Direction.Outgoing,
        speed = 40, length = 47, loop1RiseTime = 0, loop1FallTime = 50,
        loop2RiseTime = 100, loop2FallTime = 150, yellowTime2 = 0, redTime2 = 0,
        dateTime1 = startTime, yellowTime1 = 0, redTime1 = 0, dateTime3 = startTime + 200,
        yellowTime3 = 0, redTime3 = 0, valid = true,
        serialNr = "1234", offset1 = 0, offset2 = 0, offset3 = 0)
      probeState.expectMsg(Transition(testActor, Normal, ExpectImage))
      probeState.expectMsg(Transition(testActor, ExpectImage, SendError))

      val recv = probeMsg.expectMsgType[CameraImageOutputError](100.millis)
      recv.camera must be("testCam")
      recv.nr must be(1)
      recv.startTime must be(startTime - 1000)

      testActor ! new NewImage(imagePath = "/data/images/A0/image1.jpg", timestamp = startTime + 1000)
      probeState.expectMsg(Transition(testActor, SendError, Normal))
      probeMsg.expectMsg(new CameraImageOutputFixed(camera = "testCam", startTime = startTime - 1000, endTime = startTime + 1000, nr = 1))
      system.stop(testActor)
    }
    "Move from Expect Image to SendError states while receiving vehicle messages" in {
      //Normal (VehicleMsg)-> ExpectImage (timeOut)-> SendError
      val probeMsg = TestProbe()
      val probeState = TestProbe()
      val testActor = system.actorOf(Props(
        new CameraStateMonitor(camera = "testCam",
          maxWaitTimeForImage = 1.second,
          maxErrorTimeForImage = 3.seconds,
          recipients = Set(probeMsg.ref))))
      testActor ! SubscribeTransitionCallBack(probeState.ref)
      probeState.expectMsg(CurrentState(testActor, Normal))

      testActor ! new VehicleMessage(dateTime = startTime + 100, detector = 0, direction = Direction.Outgoing,
        speed = 40, length = 47, loop1RiseTime = 0, loop1FallTime = 50,
        loop2RiseTime = 100, loop2FallTime = 150, yellowTime2 = 0, redTime2 = 0,
        dateTime1 = startTime, yellowTime1 = 0, redTime1 = 0, dateTime3 = startTime + 200,
        yellowTime3 = 0, redTime3 = 0, valid = true,
        serialNr = "1234", offset1 = 0, offset2 = 0, offset3 = 0)
      probeState.expectMsg(Transition(testActor, Normal, ExpectImage))
      Future {
        for (i ← 1 until 10) {
          val startTime2 = startTime + 100L
          testActor ! new VehicleMessage(dateTime = startTime2 + (i * 100), detector = 0, direction = Direction.Outgoing,
            speed = 40, length = 47, loop1RiseTime = 0, loop1FallTime = 50,
            loop2RiseTime = 100, loop2FallTime = 150, yellowTime2 = 0, redTime2 = 0,
            dateTime1 = startTime2, yellowTime1 = 0, redTime1 = 0, dateTime3 = startTime2 + 200,
            yellowTime3 = 0, redTime3 = 0, valid = true,
            serialNr = "1234", offset1 = 0, offset2 = 0, offset3 = 0)
          Thread.sleep(500)
        }
      }
      probeState.expectMsg(Transition(testActor, ExpectImage, SendError))

      var recv = probeMsg.expectMsgType[CameraImageOutputError](100.millis)
      recv.camera must be("testCam")
      recv.nr must be(1)
      recv.startTime must be(0L)

      system.stop(testActor)
    }
    "Move from error to SendError states while receiving vehicle messages" in {
      //Normal (VehicleMsg)-> ExpectImage (timeOut)-> SendError (vehicleMsg) -> Error (timeout)-> SendError (NewImage)-> Normal
      val probeMsg = TestProbe()
      val probeState = TestProbe()
      val testActor = system.actorOf(Props(
        new CameraStateMonitor(camera = "testCam",
          maxWaitTimeForImage = 1.second,
          maxErrorTimeForImage = 3.seconds,
          recipients = Set(probeMsg.ref))))
      testActor ! SubscribeTransitionCallBack(probeState.ref)
      probeState.expectMsg(CurrentState(testActor, Normal))

      testActor ! new VehicleMessage(dateTime = startTime + 100, detector = 0, direction = Direction.Outgoing,
        speed = 40, length = 47, loop1RiseTime = 0, loop1FallTime = 50,
        loop2RiseTime = 100, loop2FallTime = 150, yellowTime2 = 0, redTime2 = 0,
        dateTime1 = startTime, yellowTime1 = 0, redTime1 = 0, dateTime3 = startTime + 200,
        yellowTime3 = 0, redTime3 = 0, valid = true,
        serialNr = "1234", offset1 = 0, offset2 = 0, offset3 = 0)
      probeState.expectMsg(Transition(testActor, Normal, ExpectImage))
      probeState.expectMsg(Transition(testActor, ExpectImage, SendError))

      var recv = probeMsg.expectMsgType[CameraImageOutputError](100.millis)
      recv.camera must be("testCam")
      recv.nr must be(1)
      recv.startTime must be(0L)

      testActor ! new VehicleMessage(dateTime = startTime + 1100, detector = 0, direction = Direction.Outgoing,
        speed = 40, length = 47, loop1RiseTime = 0, loop1FallTime = 50,
        loop2RiseTime = 100, loop2FallTime = 150, yellowTime2 = 0, redTime2 = 0,
        dateTime1 = startTime + 1000, yellowTime1 = 0, redTime1 = 0, dateTime3 = startTime + 1200,
        yellowTime3 = 0, redTime3 = 0, valid = true,
        serialNr = "1234", offset1 = 0, offset2 = 0, offset3 = 0)

      probeState.expectMsg(Transition(testActor, SendError, Error))

      Future {
        for (i ← 1 until 10) {
          val startTime2 = startTime + 100L
          testActor ! new VehicleMessage(dateTime = startTime2 + (i * 100), detector = 0, direction = Direction.Outgoing,
            speed = 40, length = 47, loop1RiseTime = 0, loop1FallTime = 50,
            loop2RiseTime = 100, loop2FallTime = 150, yellowTime2 = 0, redTime2 = 0,
            dateTime1 = startTime2, yellowTime1 = 0, redTime1 = 0, dateTime3 = startTime2 + 200,
            yellowTime3 = 0, redTime3 = 0, valid = true,
            serialNr = "1234", offset1 = 0, offset2 = 0, offset3 = 0)
          Thread.sleep(500)
        }
      }

      probeState.expectMsg(4.seconds, Transition(testActor, Error, SendError))

      recv = probeMsg.expectMsgType[CameraImageOutputError](100.millis)
      recv.camera must be("testCam")
      recv.nr must be(2)
      recv.startTime must be(0L)

      testActor ! new NewImage(imagePath = "/data/images/A0/image1.jpg", timestamp = startTime + 1000)
      probeState.expectMsg(Transition(testActor, SendError, Normal))
      probeMsg.expectMsg(new CameraImageOutputFixed(camera = "testCam", startTime = 0L, endTime = startTime + 1000, nr = 2))
      system.stop(testActor)
    }

  }

}