/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.send

import akka.actor.{ Actor, ActorRef, ActorSystem }
import akka.testkit.{ TestFSMRef, TestKit, TestProbe }
import scala.concurrent.duration._
import csc.sitebuffer.process.send.BufferedTextMessageSender.{ ToDo, _ }
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, WordSpec }

import scala.collection.immutable

/**
 * Test van BufferedTextMessageSender
 */
class BufferedTextMessageSenderTest extends TestKit(ActorSystem("BufferedTextMessageSenderTest")) with WordSpecLike with MustMatchers
  with BeforeAndAfterAll {

  implicit val executor = system.dispatcher

  /**
   * Test version of the InOutTextSender trait.
   * Will send a text to the configured receiver.
   */
  trait TestInOutTextSender extends InOutTextSender { self: Actor ⇒
    def receiver: ActorRef

    def sendText(text: String) = {
      receiver ! text
    }
  }

  override def beforeAll() {
    super.beforeAll()
  }

  override def afterAll() {
    system.shutdown
  }

  def createTestSender(testReceiver: ActorRef, delay: Int = 1000): TestFSMRef[BufferedTextMessageSender.State, BufferedTextMessageSender.Data, BufferedTextMessageSender] = {
    TestFSMRef(new BufferedTextMessageSender(retryDelayMs = delay) with TestInOutTextSender {
      val receiver = testReceiver
    })
  }

  "A BufferedTextMessageSender actor" when {

    "created" must {
      "start in Idle state and empty data" in {
        val testSender = createTestSender(TestProbe().ref)
        testSender.stateName must be(Idle)
        testSender.stateData must be(ToDo(None, immutable.Queue()))
      }
    }

    "receiving a first TextMessage, which is successfully sent" must {
      "send message and be back in Idle after Success reply" in {
        val mockReceiver = TestProbe()
        val testSender = createTestSender(mockReceiver.ref)
        // create message
        val msg = TextMessage("First text")
        //       ---[message]---> sender
        testSender ! msg
        // check expected result:
        mockReceiver.expectMsg(msg.text)
        //  sender <--[Success]---- receiver
        mockReceiver.send(testSender, InOutTextSender.Success)
        // check state after message has been processed, give actor time to reach expected state
        awaitCond((testSender.stateName == Idle), 500 millis, 100 millis)
        testSender.stateName must be(Idle)
        testSender.stateData must be(ToDo(None, immutable.Queue()))
      }
    }

    "receiving a burst of 3 TextMessage's (all will be successfully sent)" must {
      "send all messages and be back in Idle afterwards" in {
        val mockReceiver = TestProbe()
        val testSender = createTestSender(mockReceiver.ref)
        // create messages
        val msg1 = TextMessage("1 text")
        val msg2 = TextMessage("2 text")
        val msg3 = TextMessage("3 text")
        //       ---[messages]---> sender
        testSender ! msg1
        testSender ! msg2
        testSender ! msg3
        // check expected result:
        mockReceiver.expectMsg(msg1.text)
        //  sender <--[Success]---- receiver
        mockReceiver.send(testSender, InOutTextSender.Success)
        mockReceiver.expectMsg(msg2.text)
        //  sender <--[Success]---- receiver
        mockReceiver.send(testSender, InOutTextSender.Success)
        mockReceiver.expectMsg(msg3.text)
        //  sender <--[Success]---- receiver
        mockReceiver.send(testSender, InOutTextSender.Success)
        // check state after message has been processed, give actor time to reach expected state
        awaitCond((testSender.stateName == Idle), 500 millis, 100 millis)
        testSender.stateName must be(Idle)
        testSender.stateData must be(ToDo(None, immutable.Queue()))
      }
    }

    "receiving 3 separate TextMessage's (all will be successfully sent)" must {
      "send message and be back in Idle after each message" in {
        val mockReceiver = TestProbe()
        val testSender = createTestSender(mockReceiver.ref)
        // create messages
        val msg1 = TextMessage("1 text")
        val msg2 = TextMessage("2 text")
        val msg3 = TextMessage("3 text")
        //       ---[message]---> sender
        testSender ! msg1
        // check expected result:
        mockReceiver.expectMsg(msg1.text)
        //  sender <--[Success]---- receiver
        mockReceiver.send(testSender, InOutTextSender.Success)
        // check state after first message has been processed, give actor time to reach expected state
        awaitCond((testSender.stateName == Idle), 500 millis, 100 millis)
        testSender.stateName must be(Idle)
        testSender.stateData must be(ToDo(None, immutable.Queue()))
        //       ---[message]---> sender
        testSender ! msg2
        // check state after second message received
        mockReceiver.expectMsg(msg2.text)
        //  sender <--[Success]---- receiver
        mockReceiver.send(testSender, InOutTextSender.Success)
        // check state after second message has been processed, give actor time to reach expected state
        awaitCond((testSender.stateName == Idle), 500 millis, 100 millis)
        testSender.stateName must be(Idle)
        testSender.stateData must be(ToDo(None, immutable.Queue()))
        //       ---[message]---> sender
        testSender ! msg3
        // check state after third message received
        mockReceiver.expectMsg(msg3.text)
        //  sender <--[Success]---- receiver
        mockReceiver.send(testSender, InOutTextSender.Success)
        // check state after third message has been processed, give actor time to reach expected state
        awaitCond((testSender.stateName == Idle), 500 millis, 100 millis)
        testSender.stateName must be(Idle)
        testSender.stateData must be(ToDo(None, immutable.Queue()))
      }
    }

    "receiving a first TextMessage, which is not successfully sent" must {
      "message will be resent, until this succeeds" in {
        val mockReceiver = TestProbe()
        val testSender = createTestSender(testReceiver = mockReceiver.ref)
        // create message
        val message = TextMessage("First text")
        //       ---[message]---> sender
        testSender ! message
        // check expected result:
        mockReceiver.expectMsg(message.text)
        //  sender <--[Failure]---- receiver
        mockReceiver.send(testSender, InOutTextSender.Failure)
        mockReceiver.expectMsg(message.text)
        //  sender <--[Failure]---- receiver
        mockReceiver.send(testSender, InOutTextSender.Failure)
        mockReceiver.expectMsg(message.text)
        //  sender <--[Success]---- receiver
        mockReceiver.send(testSender, InOutTextSender.Success)
        // check state after message has been processed, give actor time to reach expected state
        awaitCond((testSender.stateName == Idle), 500 millis, 100 millis)
        testSender.stateName must be(Idle)
        testSender.stateData must be(ToDo(None, immutable.Queue()))
      }
    }

    "receiving TextMessage's whilst in Sending state" must {
      "add these message to the queue and process them later" in {
        val mockReceiver = TestProbe()
        val testSender = createTestSender(mockReceiver.ref)
        // create messages
        val msg1 = TextMessage("1 text")
        val msg2 = TextMessage("2 text")
        val msg3 = TextMessage("3 text")
        //       ---[message]---> sender
        testSender ! msg1
        // check state after first message received
        mockReceiver.expectMsg(msg1.text)

        // State = Sending, the real test starts here.
        //       ---[messages]---> sender
        testSender ! msg2
        testSender ! msg3
        // check state after third message received
        testSender.stateData must be(ToDo(Some(msg1), immutable.Queue(msg2, msg3)))
        testSender.stateName must be(Sending)

        // Start emptying queue.
        //  sender <--[Success]---- receiver
        mockReceiver.send(testSender, InOutTextSender.Success)
        mockReceiver.expectMsg(msg2.text)
        mockReceiver.send(testSender, InOutTextSender.Success)
        mockReceiver.expectMsg(msg3.text)
        mockReceiver.send(testSender, InOutTextSender.Success)
        // check state after message has been processed, give actor time to reach expected state
        awaitCond((testSender.stateName == Idle), 500 millis, 100 millis)
        testSender.stateName must be(Idle)
        testSender.stateData must be(ToDo(None, immutable.Queue()))
      }
    }

    "receiving TextMessage's whilst in WaitingForRetry state" must {
      "add these message to the queue and process them later" in {
        val mockReceiver = TestProbe()
        val testSender = createTestSender(testReceiver = mockReceiver.ref, delay = 3000) // Longer delay to ensure msg2 and msg3 are send during WaitingForRetry state
        // create messages
        val msg1 = TextMessage("1 text")
        val msg2 = TextMessage("2 text")
        val msg3 = TextMessage("3 text")
        //       ---[message]---> sender
        testSender ! msg1
        // check state after first message received
        mockReceiver.expectMsg(msg1.text)
        testSender.stateName must be(Sending)
        testSender.stateData must be(ToDo(Some(msg1), immutable.Queue()))
        //  sender <--[Failure]---- receiver
        mockReceiver.send(testSender, InOutTextSender.Failure)
        // check state after first failed delivery
        testSender.stateName must be(WaitingForRetry)
        testSender.stateData must be(ToDo(Some(msg1), immutable.Queue()))

        // State = WaitingForRetry, the real test starts here.
        //       ---[messages]---> sender
        testSender ! msg2
        testSender ! msg3
        testSender.stateName must be(WaitingForRetry)

        mockReceiver.expectMsg(5000 millis, msg1.text)
        testSender.stateData must be(ToDo(Some(msg1), immutable.Queue(msg2, msg3)))

        // Start emptying queue.
        //  sender <--[Success]---- receiver
        mockReceiver.send(testSender, InOutTextSender.Success)
        mockReceiver.expectMsg(msg2.text)
        mockReceiver.send(testSender, InOutTextSender.Success)
        mockReceiver.expectMsg(msg3.text)
        mockReceiver.send(testSender, InOutTextSender.Success)
        // check state after message has been processed, give actor time to reach expected state
        awaitCond((testSender.stateName == Idle), 500 millis, 100 millis)
        testSender.stateName must be(Idle)
        testSender.stateData must be(ToDo(None, immutable.Queue()))
      }
    }

    "receiving an unknown message" must {
      "do nothing" in {
        val testSender = createTestSender(TestProbe().ref)
        testSender.stateName must be(Idle)
        testSender.stateData must be(ToDo(None, immutable.Queue()))

        testSender ! "Unknown message"
        testSender.stateName must be(Idle)
        testSender.stateData must be(ToDo(None, immutable.Queue()))
      }
    }

  }

}
