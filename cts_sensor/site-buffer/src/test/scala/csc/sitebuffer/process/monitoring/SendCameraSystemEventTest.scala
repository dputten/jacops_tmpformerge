/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.monitoring

import akka.actor.{ ActorSystem, Props }
import akka.testkit.{ TestKit, TestProbe }
import scala.concurrent.duration._
import csc.sitebuffer.process.SystemSensorMessage
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, WordSpec }

class SendCameraSystemEventTest extends TestKit(ActorSystem("SendCameraSystemEventTest"))
  with WordSpecLike with MustMatchers with BeforeAndAfterAll {

  override def afterAll() {
    super.afterAll()
    system.shutdown()
  }
  val startTime = System.currentTimeMillis()

  "SendCameraSystemEventTest" must {
    "Send error only after waitPeriod" in {
      val probeMsg = TestProbe()
      val testActor = system.actorOf(Props(
        new SendCameraSystemEvent(waitForFixTime = 30.minutes.toMillis,
          systemSensorMessage = probeMsg.ref)))

      testActor ! CameraImageOutputError(
        camera = "cam1",
        startTime = startTime,
        currentTime = startTime,
        nr = 1)
      probeMsg.expectNoMsg(100.millis)
      testActor ! CameraImageOutputError(
        camera = "cam1",
        startTime = startTime,
        currentTime = startTime + 29.minutes.toMillis,
        nr = 2)
      probeMsg.expectNoMsg(100.millis)
      testActor ! CameraImageOutputError(
        camera = "cam1",
        startTime = startTime,
        currentTime = startTime + 30.minutes.toMillis,
        nr = 3)
      val expected = SystemSensorMessage(
        sensor = SystemSensorMessage.Sensor.Camera,
        timeStamp = startTime,
        systemId = "",
        event = SystemSensorMessage.EventType.CameraUploadCorrectionFailed,
        sendingComponent = "Sitebuffer",
        data = Map("CameraId" -> "cam1"))
      probeMsg.expectMsg(expected)
    }
    "Send CameraUploadSolved after CameraUploadFailed" in {
      val probeMsg = TestProbe()
      val testActor = system.actorOf(Props(
        new SendCameraSystemEvent(waitForFixTime = 30.minutes.toMillis,
          systemSensorMessage = probeMsg.ref)))

      testActor ! CameraImageOutputError(
        camera = "cam1",
        startTime = startTime,
        currentTime = startTime + 30.minutes.toMillis,
        nr = 1)
      val expected = SystemSensorMessage(
        sensor = SystemSensorMessage.Sensor.Camera,
        timeStamp = startTime,
        systemId = "",
        event = SystemSensorMessage.EventType.CameraUploadCorrectionFailed,
        sendingComponent = "Sitebuffer",
        data = Map("CameraId" -> "cam1"))
      probeMsg.expectMsg(expected)

      testActor ! CameraImageOutputFixed(
        camera = "cam1",
        startTime = startTime,
        endTime = startTime + 31.minutes.toMillis,
        nr = 2)
      val expectedFix = SystemSensorMessage(
        sensor = SystemSensorMessage.Sensor.Camera,
        timeStamp = startTime + 31.minutes.toMillis,
        systemId = "",
        event = SystemSensorMessage.EventType.CameraUploadSolved,
        sendingComponent = "Sitebuffer",
        data = Map("CameraId" -> "cam1"))
      probeMsg.expectMsg(expectedFix)
    }
    "Send CameraUploadFailed when the fix is within waiting period" in {
      val probeMsg = TestProbe()
      val testActor = system.actorOf(Props(
        new SendCameraSystemEvent(waitForFixTime = 30.minutes.toMillis,
          systemSensorMessage = probeMsg.ref)))

      testActor ! CameraImageOutputError(
        camera = "cam1",
        startTime = startTime,
        currentTime = startTime,
        nr = 1)

      testActor ! CameraImageOutputFixed(
        camera = "cam1",
        startTime = startTime,
        endTime = startTime + 15.minutes.toMillis,
        nr = 2)
      val expected = SystemSensorMessage(
        sensor = SystemSensorMessage.Sensor.Camera,
        timeStamp = startTime + 15.minutes.toMillis,
        systemId = "",
        event = SystemSensorMessage.EventType.CameraUploadFailed,
        sendingComponent = "Sitebuffer",
        data = Map("downStartTime" -> startTime.toString,
          "CameraId" -> "cam1"))
      probeMsg.expectMsg(expected)
    }
    "Send CameraUploadFailed when the fix is after waiting period but before errorMsg" in {
      val probeMsg = TestProbe()
      val testActor = system.actorOf(Props(
        new SendCameraSystemEvent(waitForFixTime = 30.minutes.toMillis,
          systemSensorMessage = probeMsg.ref)))

      testActor ! CameraImageOutputError(
        camera = "cam1",
        startTime = startTime,
        currentTime = startTime,
        nr = 1)

      testActor ! CameraImageOutputFixed(
        camera = "cam1",
        startTime = startTime,
        endTime = startTime + 35.minutes.toMillis,
        nr = 2)
      val expected = SystemSensorMessage(
        sensor = SystemSensorMessage.Sensor.Camera,
        timeStamp = startTime + 35.minutes.toMillis,
        systemId = "",
        event = SystemSensorMessage.EventType.CameraUploadFailed,
        sendingComponent = "Sitebuffer",
        data = Map("downStartTime" -> startTime.toString,
          "CameraId" -> "cam1"))
      probeMsg.expectMsg(expected)
    }
  }
}