/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.send

import csc.sitebuffer.process.SystemSensorMessage
import org.scalatest.WordSpec
import org.scalatest._

/**
 * Test van SystemSensorMessageSerialization.
 */
class SystemSensorMessageSerializationTest extends WordSpec with MustMatchers {

  def createCompleteSensorMessage: SystemSensorMessage = {
    SystemSensorMessage(
      sensor = "TestSensor",
      timeStamp = 12345678L,
      systemId = "TestSystemId",
      event = "This is a test message",
      sendingComponent = "TestComponent",
      data = Map[String, String]("one" -> "test 1", "two" -> "test 2", "three" -> "test 3"))
  }

  def createMinimalSensorMessage: SystemSensorMessage = {
    SystemSensorMessage(
      sensor = "",
      timeStamp = 0L,
      systemId = "",
      event = "",
      sendingComponent = "",
      data = Map[String, String]())
  }

  "SensorMessageSerialization" must {
    "serialize and deserialize a complete SensorMessage object" in {
      val sensorMessage = createCompleteSensorMessage
      SystemSensorMessageSerialization.serializeToJSON(sensorMessage) match {
        case Left(_) ⇒ fail("Unexpected error during serialization")
        case Right(json) ⇒
          SystemSensorMessageSerialization.deserializeFromJSON(json) match {
            case Left(_) ⇒ fail("Unexpected error during deserialization")
            case Right(sensorMessageClone) ⇒
              sensorMessageClone must be(sensorMessage)
          }
      }
    }

    "serialize and deserialize a minimal SensorMessage object" in {
      val sensorMessage = createMinimalSensorMessage
      SystemSensorMessageSerialization.serializeToJSON(sensorMessage) match {
        case Left(_) ⇒ fail("Unexpected error during serialization")
        case Right(json) ⇒
          SystemSensorMessageSerialization.deserializeFromJSON(json) match {
            case Left(_) ⇒ fail("Unexpected error during deserialization")
            case Right(sensorMessageClone) ⇒
              sensorMessageClone must be(sensorMessage)
          }
      }
    }

    "return a Left(error) if deserialization of a json failes" in {
      SystemSensorMessageSerialization.deserializeFromJSON("onzin") match {
        case Left(e)  ⇒ e.startsWith("SystemSensorMessageSerialization: Error during deserialization:") must be(true)
        case Right(_) ⇒ fail("Deserialize should have returned a Left()")
      }

    }
  }
}
