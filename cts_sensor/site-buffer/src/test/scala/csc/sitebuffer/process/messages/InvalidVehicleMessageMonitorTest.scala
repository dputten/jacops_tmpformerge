/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.messages

import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import scala.concurrent.duration._
import csc.sitebuffer.messages.{ Direction, VehicleMessage }
import csc.sitebuffer.process.SystemSensorMessage
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, WordSpec }

/**
 * Test for InvalidVehicleMessageMonitor actor
 */
class InvalidVehicleMessageMonitorTest extends TestKit(ActorSystem("InvalidVehicleMessageMonitorTest")) with WordSpecLike with MustMatchers
  with BeforeAndAfterAll {

  override def beforeAll() {
    super.beforeAll()
  }

  override def afterAll() {
    system.shutdown()
  }

  private def createVehicleMessage(offset: Int, speed: Float, valid: Boolean): VehicleMessage = {
    VehicleMessage(dateTime = 1360650493123L + offset, detector = 1, direction = Direction.Outgoing,
      speed = speed, length = 50.0f, loop1RiseTime = 0L, loop1FallTime = 450L,
      loop2RiseTime = 400L, loop2FallTime = 875L, yellowTime2 = 0, redTime2 = 0,
      dateTime1 = 1360650493005L + offset, yellowTime1 = 0, redTime1 = 0, dateTime3 = 0L + offset,
      yellowTime3 = 0, redTime3 = 0, valid = valid)
  }

  "The InvalidVehicleMessageMonitor (minimalSpeedForProcessingInvalidVehicleMessageKmh = 20)" when {
    val testProbe = TestProbe()
    val invalidVehicleMessageMonitor = TestActorRef(new InvalidVehicleMessageMonitor(
      minimalSpeedForProcessingInvalidVehicleMessageKmh = 20,
      systemSensorMessageSender = testProbe.ref))

    "receiving an invalid VehicleMessage with speed 19.9" must {
      "do nothing" in {
        // Send test message
        val message = createVehicleMessage(offset = 0, speed = 19.9f, valid = false)
        invalidVehicleMessageMonitor ! message
        testProbe.expectNoMsg(500 millis)
      }
    }

    "receiving an invalid VehicleMessage with speed 20.0" must {
      "send a SystemSensorMessage(Speed, OutOfRange) to the systemSensorMessageSender" in {
        // Send test message
        val message = createVehicleMessage(offset = 0, speed = 20.0f, valid = false)
        invalidVehicleMessageMonitor ! message

        // Check for expected result
        val expectedMsg = SystemSensorMessage(
          sensor = "Speed",
          timeStamp = 1360650493123L,
          systemId = "",
          event = "OutOfRange",
          sendingComponent = "Sitebuffer",
          data = Map())
        testProbe.expectMsg(500 millis, expectedMsg)
      }
    }

    "receiving an invalid VehicleMessage with speed 20.1" must {
      "send a SystemSensorMessage(Speed, OutOfRange) to the systemSensorMessageSender" in {
        // Send test message
        val message = createVehicleMessage(offset = 0, speed = 20.1f, valid = false)
        invalidVehicleMessageMonitor ! message

        // Check for expected result
        val expectedMsg = SystemSensorMessage(
          sensor = "Speed",
          timeStamp = 1360650493123L,
          systemId = "",
          event = "OutOfRange",
          sendingComponent = "Sitebuffer",
          data = Map())
        testProbe.expectMsg(500 millis, expectedMsg)
      }
    }

    "receiving a valid VehicleMessage" must {
      "do nothing" in {
        // Send test message
        val message = createVehicleMessage(offset = 0, speed = 15f, valid = true)
        invalidVehicleMessageMonitor ! message
        testProbe.expectNoMsg(500 millis)
      }
    }

    "receiving an unknown message" must {
      "do nothing" in {
        // Send test message
        invalidVehicleMessageMonitor ! "wat is dit?"
        testProbe.expectNoMsg(500 millis)
      }
    }

  }

}
