/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.images

import java.text.SimpleDateFormat
import java.util.Date

import akka.actor.{ ActorSystem, Props }
import akka.testkit.{ TestKit, TestProbe }
import scala.concurrent.duration._
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, WordSpec }

class ImageFilterTest extends TestKit(ActorSystem("ImageFilterTest"))
  with WordSpecLike with MustMatchers with BeforeAndAfterAll {
  private val timestampFormat = new SimpleDateFormat("yyyyMMddHHmmss.SSS")
  timestampFormat.setTimeZone(java.util.TimeZone.getTimeZone(("UTC")))

  override def afterAll() {
    super.afterAll()
    system.shutdown()
  }

  "ImageFilter" must {
    "pass not delayed messages" in {
      val probeNormalFlow = TestProbe()
      val probeCleanup = TestProbe()
      val testActor = system.actorOf(Props(new ImageFilter("jpg", 1.second, Seq(probeCleanup.ref), Seq(probeNormalFlow.ref))))
      val startTime = new Date()
      val msg = NewImage(imagePath = "/data/image/0/20%s.jpg".format(timestampFormat.format(startTime)), timestamp = startTime.getTime + 999)
      testActor ! msg
      probeNormalFlow.expectMsg(msg)
      probeCleanup.expectNoMsg(200.millis)
      system.stop(testActor)
    }
    "filter delayed messages" in {
      val probeNormalFlow = TestProbe()
      val probeCleanup = TestProbe()
      val testActor = system.actorOf(Props(new ImageFilter("jpg", 1.second, Seq(probeCleanup.ref), Seq(probeNormalFlow.ref))))
      val startTime = new Date()
      val msg = NewImage(imagePath = "/data/image/0/20%s.jpg".format(timestampFormat.format(startTime)), timestamp = startTime.getTime + 1.second.toMillis)
      testActor ! msg
      probeCleanup.expectMsg(msg)
      probeNormalFlow.expectNoMsg(200.millis)
      system.stop(testActor)
    }
  }
}