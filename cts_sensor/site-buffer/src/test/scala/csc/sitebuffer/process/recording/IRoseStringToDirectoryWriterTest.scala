/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.recording

import java.io.File

import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit }
import csc.sitebuffer.FileTestUtils
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }
import testframe.Resources

/**
 * Test van IRoseStringToDirectoryWriter actor.
 */
class IRoseStringToDirectoryWriterTest extends TestKit(ActorSystem("IRoseStringToDirectoryWriterTest")) with WordSpecLike with MustMatchers
  with BeforeAndAfterEach with BeforeAndAfterAll {
  import FileTestUtils._
  import RecordingTestUtils._

  var testWriter: TestActorRef[IRoseStringToDirectoryWriter] = _

  val resourceDir = new File(Resources.getResourceDirPath().getAbsolutePath)
  val testBaseDir = new File(resourceDir, "out/iRoseStringToDirectoryWriter")
  if (!testBaseDir.exists()) {
    testBaseDir.mkdirs()
  } else {
    clearDirectory(testBaseDir)
  }

  val testOutputDir = new File(testBaseDir, "output")

  override def beforeAll() {
    super.beforeAll()
  }

  override def afterAll() {
    system.shutdown()

    clearDirectory(testBaseDir)
    testBaseDir.delete()
  }

  override def afterEach() {
    clearDirectory(testBaseDir)
  }

  def createTestActor(directory: File): TestActorRef[IRoseStringToDirectoryWriter] = {
    TestActorRef(new IRoseStringToDirectoryWriter(directory = directory))
  }

  "An IRoseStringToDirectoryWriter actor" when {
    "being created and directory does already exist" must {
      "do nothing" in {
        // Make sure directory does exist beforehand
        testOutputDir.mkdirs()
        testOutputDir.exists() must be(true)
        // Create test actor
        testWriter = createTestActor(directory = testOutputDir)
        // Check expected results
        testOutputDir.exists() must be(true)
      }
    }

    "being created and directory does not exist" must {
      "create the directory passed in the constructor" in {
        // Make sure directory does not exist beforehand
        testOutputDir.exists() must be(false)
        // Create test actor
        testWriter = createTestActor(directory = testOutputDir)
        // Check expected results
        testOutputDir.exists() must be(true)
      }
    }

    "receiving IRoseString's" must {
      "write the content of each IRoseString to a file in the directory" in {
        // Create test actor
        testWriter = createTestActor(directory = testOutputDir)
        testOutputDir.listFiles().length must be(0)

        case class TestMessage(iRoseMessage: String, timestamp: Long, filename: String)
        val testMessages = Seq[TestMessage](
          TestMessage(vehicleMessage1, 1234567800, "1234567800-1-1.txt"),
          TestMessage(alarmInputEventMessage1, 1234567801, "1234567801-2.txt"),
          TestMessage(singleLoopMessage1, 1234567802, "1234567802-4.txt"),
          TestMessage(vehicleMessage2, 1234567803, "1234567803-1-2.txt"),
          TestMessage(inputEventMessage1, 1234567804, "1234567804-2.txt"),
          TestMessage(alarmInputEventMessage2, 1234567805, "1234567805-2.txt"),
          TestMessage(sensorMessage1, 1234567806, "1234567806-0.txt"),
          TestMessage(vehicleMessage3, 1234567807, "1234567807-1-3.txt"),
          TestMessage(inputEventMessage2, 1234567808, "1234567808-2.txt"),
          TestMessage(sensorMessage2, 1234567809, "1234567809-0.txt"),
          TestMessage(singleLoopMessage2, 1234567810, "1234567810-4.txt"))

        info("filename is determined by content of IRoseString")
        testMessages.zipWithIndex.foreach {
          case (testMessage, index) ⇒
            // Create IRoseString
            val iRoseString = createIRoseString(testMessage.iRoseMessage, testMessage.timestamp)
            testWriter ! iRoseString
            Thread.sleep(50)
            // Check expected results
            testOutputDir.listFiles().length must be(index + 1)
            getCreatedFile(directory = testOutputDir, filename = testMessage.filename) match {
              case Some(file) ⇒
                getContent(file) must be(iRoseString.string)

              case None ⇒
                fail("recorded file was not created")
            }
        }

      }
    }

  }

}
