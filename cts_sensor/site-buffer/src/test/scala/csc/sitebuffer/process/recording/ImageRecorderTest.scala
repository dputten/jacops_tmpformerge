/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.recording

import java.io.File

import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import csc.sitebuffer.FileTestUtils
import csc.sitebuffer.process.images.NewImage
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }
import testframe.Resources

/**
 * Test van ImageRecorder
 */
class ImageRecorderTest extends TestKit(ActorSystem("ImageRecorderTest")) with WordSpecLike with MustMatchers
  with BeforeAndAfterEach with BeforeAndAfterAll {
  import FileTestUtils._

  val testProbe: TestProbe = TestProbe()
  var testRecorder: TestActorRef[ImageRecorder] = _

  val resourceDir = new File(Resources.getResourceDirPath().getAbsolutePath)
  val imageRecorderResourceDir = new File(resourceDir, "csc/sitebuffer/process/recording/imageRecorder")
  val testBaseDir = new File(resourceDir, "out/imageRecorder")
  val testInputDir = new File(testBaseDir, "input")
  val testInputDir1 = new File(testInputDir, "PA1")
  val testInputDir2 = new File(testInputDir, "PA2")
  val testOutputDir = new File(testBaseDir, "output")
  if (!testInputDir1.exists()) {
    testInputDir1.mkdirs()
  } else {
    clearDirectory(testInputDir1)
  }
  if (!testInputDir2.exists()) {
    testInputDir2.mkdirs()
  } else {
    clearDirectory(testInputDir2)
  }
  if (!testOutputDir.exists()) {
    testOutputDir.mkdirs()
  } else {
    clearDirectory(testOutputDir)
  }

  val testInputFiles: Array[File] = Array(
    copyTestResource("image.jpg", new File(testInputDir1, "02120130628155503.001.jpg")),
    copyTestResource("image.jpg", new File(testInputDir2, "02120130628155503.002.jpg")),
    copyTestResource("image.jpg", new File(testInputDir1, "02120130628155503.003.jpg")),
    copyTestResource("image.jpg", new File(testInputDir2, "02120130628155503.004.jpg")),
    copyTestResource("image.jpg", new File(testInputDir1, "02120130628155503.005.jpg")),
    copyTestResource("image.jpg", new File(testInputDir2, "02120130628155503.006.jpg")),
    copyTestResource("image.jpg", new File(testInputDir1, "02120130628155503.007.jpg")),
    copyTestResource("image.jpg", new File(testInputDir2, "02120130628155503.008.jpg")),
    copyTestResource("image.jpg", new File(testInputDir1, "02120130628155503.009.jpg")),
    copyTestResource("image.jpg", new File(testInputDir2, "02120130628155503.010.jpg")),
    copyTestResource("image.jpg", new File(testInputDir1, "02120130628155503.011.jpg")),
    copyTestResource("image.jpg", new File(testInputDir2, "02120130628155503.012.jpg")))

  override def beforeAll() {
    super.beforeAll()
  }

  override def afterAll() {
    system.shutdown()

    clearDirectory(testBaseDir)
    testBaseDir.delete()
  }

  override def afterEach() {
    clearDirectory(testOutputDir)
  }

  def createTestActor(directory: File, imageFilenamesOnly: Boolean): TestActorRef[ImageRecorder] = {
    TestActorRef(new ImageRecorder(baseDirectory = testOutputDir, imageFilenamesOnly = imageFilenamesOnly, recipients = Set(testProbe.ref)))
  }

  def sendStartRecordingInput(recordingId: String): Unit = {
    system.eventStream.publish(StartRecordingInput(recordingId))
  }

  def sendStopRecordingInput(): Unit = {
    system.eventStream.publish(StopRecordingInput)
  }

  Seq(false, true).foreach { imageFilenamesOnly ⇒

    "An ImageRecorder actor, (imageFilenamesOnly = %s)".format(imageFilenamesOnly) when {
      "receiving an NewImage without having received a StartRecordingInput event first" must {
        "do nothing" in {
          // Make sure we start from scratch
          testOutputDir.listFiles().length must be(0)
          // Create actor
          testRecorder = createTestActor(testBaseDir, imageFilenamesOnly)
          // Send test message
          testRecorder ! NewImage(testInputFiles(0).getCanonicalPath, 12345678L)
          // Check expected results
          testOutputDir.listFiles().length must be(0)
        }
      }

      "receiving a StartRecordingInput event whilst waitingForRecording" must {
        "start recording all incoming NewImage's until a StopRecordingInput is received" in {
          // Make sure we start from scratch
          testOutputDir.listFiles().length must be(0)
          // Create actor
          testRecorder = createTestActor(testBaseDir, imageFilenamesOnly)
          // Publish StartRecordingInput event
          val recordingId1 = "recording-1"
          val recording1Dir = new File(testOutputDir, "%s/images".format(recordingId1))
          recording1Dir.exists() must be(false)
          sendStartRecordingInput(recordingId1)
          Thread.sleep(50)
          recording1Dir.exists() must be(true)
          recording1Dir.listFiles().length must be(0)
          // Send 4 test messages
          testRecorder ! NewImage(testInputFiles(0).getCanonicalPath, 12345678000L)
          testRecorder ! NewImage(testInputFiles(1).getCanonicalPath, 12345678001L)
          testRecorder ! NewImage(testInputFiles(2).getCanonicalPath, 12345678002L)
          testRecorder ! NewImage(testInputFiles(3).getCanonicalPath, 12345678003L)
          Thread.sleep(100)
          // Check expected results
          val testRecording1OutputDir1 = new File(recording1Dir, "PA1")
          val testRecording1OutputDir2 = new File(recording1Dir, "PA2")
          info("NewImage's are recorded to a subdirectory of the recording base directory (name = recordingId from StartRecordingInput event)")
          info("Name of subdirectory determined by name of directory where original is located")
          testRecording1OutputDir1.listFiles().length must be(2)
          testRecording1OutputDir2.listFiles().length must be(2)
          checkTotalSize(imageFilenamesOnly, testRecording1OutputDir1.listFiles())
          checkTotalSize(imageFilenamesOnly, testRecording1OutputDir2.listFiles())
          if (imageFilenamesOnly) {
            info("If imageFilenamesOnly = true, then the recorded images will have length 0")
          } else {
            info("If imageFilenamesOnly = false, then the complete image is recorded")
          }
          // Publish StopRecordingInput event
          sendStopRecordingInput()
          // Send 4 test messages
          testRecorder ! NewImage(testInputFiles(4).getCanonicalPath, 12345678005L)
          testRecorder ! NewImage(testInputFiles(5).getCanonicalPath, 12345678006L)
          testRecorder ! NewImage(testInputFiles(6).getCanonicalPath, 12345678007L)
          testRecorder ! NewImage(testInputFiles(7).getCanonicalPath, 12345678008L)
          Thread.sleep(100)
          // Check expected results
          info("NewImage's received after StopRecordingInput event are not recorded")
          info("So a StopRecordingInput event ends a recording session")
          testRecording1OutputDir1.listFiles().length must be(2)
          testRecording1OutputDir2.listFiles().length must be(2)
          checkTotalSize(imageFilenamesOnly, testRecording1OutputDir1.listFiles())
          checkTotalSize(imageFilenamesOnly, testRecording1OutputDir2.listFiles())
        }
      }

      "receiving a StartRecordingInput event whilst recording" must {
        "continue recording incoming NewImage's to the directory of existing recording" in {
          // Make sure we start from scratch
          testOutputDir.listFiles().length must be(0)
          // Create actor
          testRecorder = createTestActor(testBaseDir, imageFilenamesOnly)
          // Publish StartRecordingInput event
          val recordingId1 = "recording-1"
          val recording1Dir = new File(testOutputDir, "%s/images".format(recordingId1))
          recording1Dir.exists() must be(false)
          sendStartRecordingInput(recordingId1)
          Thread.sleep(50)
          recording1Dir.exists() must be(true)
          recording1Dir.listFiles().length must be(0)
          // Send 4 test messages
          testRecorder ! NewImage(testInputFiles(0).getCanonicalPath, 12345678001L)
          testRecorder ! NewImage(testInputFiles(1).getCanonicalPath, 12345678002L)
          testRecorder ! NewImage(testInputFiles(2).getCanonicalPath, 12345678003L)
          testRecorder ! NewImage(testInputFiles(3).getCanonicalPath, 12345678004L)
          Thread.sleep(100)
          // Check expected results
          val testRecording1OutputDir1 = new File(recording1Dir, "PA1")
          val testRecording1OutputDir2 = new File(recording1Dir, "PA2")
          testRecording1OutputDir1.listFiles().length must be(2)
          testRecording1OutputDir2.listFiles().length must be(2)
          checkTotalSize(imageFilenamesOnly, testRecording1OutputDir1.listFiles())
          checkTotalSize(imageFilenamesOnly, testRecording1OutputDir2.listFiles())
          // Publish another StartRecordingInput event
          val recordingId2 = "recording-2"
          val recording2Dir = new File(testOutputDir, "%s/images".format(recordingId2))
          recording2Dir.exists() must be(false)
          sendStartRecordingInput(recordingId2)
          // Send 4 test messages
          testRecorder ! NewImage(testInputFiles(4).getCanonicalPath, 12345678005L)
          testRecorder ! NewImage(testInputFiles(5).getCanonicalPath, 12345678006L)
          testRecorder ! NewImage(testInputFiles(6).getCanonicalPath, 12345678007L)
          testRecorder ! NewImage(testInputFiles(7).getCanonicalPath, 12345678008L)
          Thread.sleep(100)
          // Check expected results
          info("So the second StartRecordingInput event is ignored")
          info("So current recording must be stopped before next recording can start")
          testRecording1OutputDir1.listFiles().length must be(4)
          testRecording1OutputDir2.listFiles().length must be(4)
          checkTotalSize(imageFilenamesOnly, testRecording1OutputDir1.listFiles())
          checkTotalSize(imageFilenamesOnly, testRecording1OutputDir2.listFiles())
        }
      }

      "receiving multiple StartRecordingInput/StopRecordingInput events with different recordingId's" must {
        "record incoming NewImage's that fall within a recording session" in {
          // Make sure we start from scratch
          testOutputDir.listFiles().length must be(0)
          // Prepare input
          // Create actor
          testRecorder = createTestActor(testBaseDir, imageFilenamesOnly)
          // Publish StartRecordingInput event
          val recordingId1 = "recording-1"
          val recording1Dir = new File(testOutputDir, "%s/images".format(recordingId1))
          recording1Dir.exists() must be(false)
          sendStartRecordingInput(recordingId1)
          Thread.sleep(50)
          recording1Dir.exists() must be(true)
          recording1Dir.listFiles().length must be(0)
          // Send 4 test messages
          testRecorder ! NewImage(testInputFiles(0).getCanonicalPath, 12345678001L)
          testRecorder ! NewImage(testInputFiles(1).getCanonicalPath, 12345678002L)
          testRecorder ! NewImage(testInputFiles(2).getCanonicalPath, 12345678003L)
          testRecorder ! NewImage(testInputFiles(3).getCanonicalPath, 12345678004L)
          Thread.sleep(100)
          // Check expected results
          val testRecording1OutputDir1 = new File(recording1Dir, "PA1")
          val testRecording1OutputDir2 = new File(recording1Dir, "PA2")
          testRecording1OutputDir1.listFiles().length must be(2)
          testRecording1OutputDir2.listFiles().length must be(2)
          checkTotalSize(imageFilenamesOnly, testRecording1OutputDir1.listFiles())
          checkTotalSize(imageFilenamesOnly, testRecording1OutputDir2.listFiles())
          // Publish StopRecordingInput event
          sendStopRecordingInput()
          // Send 4 test messages
          testRecorder ! NewImage(testInputFiles(4).getCanonicalPath, 12345678005L)
          testRecorder ! NewImage(testInputFiles(5).getCanonicalPath, 12345678006L)
          testRecorder ! NewImage(testInputFiles(6).getCanonicalPath, 12345678007L)
          testRecorder ! NewImage(testInputFiles(7).getCanonicalPath, 12345678008L)
          Thread.sleep(100)
          // Check expected results
          testRecording1OutputDir1.listFiles().length must be(2)
          testRecording1OutputDir2.listFiles().length must be(2)
          checkTotalSize(imageFilenamesOnly, testRecording1OutputDir1.listFiles())
          checkTotalSize(imageFilenamesOnly, testRecording1OutputDir2.listFiles())

          // Publish another StartRecordingInput event
          val recordingId2 = "recording-2"
          val recording2Dir = new File(testOutputDir, "%s/images".format(recordingId2))
          recording2Dir.exists() must be(false)
          sendStartRecordingInput(recordingId2)
          Thread.sleep(50)
          recording2Dir.exists() must be(true)
          recording2Dir.listFiles().length must be(0)
          // Send 4 test messages
          testRecorder ! NewImage(testInputFiles(8).getCanonicalPath, 12345678009L)
          testRecorder ! NewImage(testInputFiles(9).getCanonicalPath, 12345678010L)
          testRecorder ! NewImage(testInputFiles(10).getCanonicalPath, 12345678011L)
          testRecorder ! NewImage(testInputFiles(11).getCanonicalPath, 12345678012L)
          Thread.sleep(100)

          // Check expected results
          info("So there is one directory per recordingId")
          val testRecording2OutputDir1 = new File(recording1Dir, "PA1")
          val testRecording2OutputDir2 = new File(recording1Dir, "PA2")
          testRecording1OutputDir1.listFiles().length must be(2)
          testRecording1OutputDir2.listFiles().length must be(2)
          testRecording2OutputDir1.listFiles().length must be(2)
          testRecording2OutputDir2.listFiles().length must be(2)
          checkTotalSize(imageFilenamesOnly, testRecording1OutputDir1.listFiles())
          checkTotalSize(imageFilenamesOnly, testRecording1OutputDir2.listFiles())
          checkTotalSize(imageFilenamesOnly, testRecording2OutputDir1.listFiles())
          checkTotalSize(imageFilenamesOnly, testRecording2OutputDir2.listFiles())
        }
      }

      "receiving multiple StartRecordingInput/StopRecordingInput events with the same recordingId's" must {
        "record incoming IRoseString's that fall within a recording session" in {
          // Make sure we start from scratch
          testOutputDir.listFiles().length must be(0)
          // Create actor
          testRecorder = createTestActor(testBaseDir, imageFilenamesOnly)
          // Publish StartRecordingInput event
          val recordingId1 = "recording-1"
          val recording1Dir = new File(testOutputDir, "%s/images".format(recordingId1))
          recording1Dir.exists() must be(false)
          sendStartRecordingInput(recordingId1)
          Thread.sleep(50)
          recording1Dir.exists() must be(true)
          recording1Dir.listFiles().length must be(0)
          // Send 4 test messages
          testRecorder ! NewImage(testInputFiles(0).getCanonicalPath, 12345678001L)
          testRecorder ! NewImage(testInputFiles(1).getCanonicalPath, 12345678002L)
          testRecorder ! NewImage(testInputFiles(2).getCanonicalPath, 12345678003L)
          testRecorder ! NewImage(testInputFiles(3).getCanonicalPath, 12345678004L)
          Thread.sleep(100)
          // Check expected results
          val testRecording1OutputDir1 = new File(recording1Dir, "PA1")
          val testRecording1OutputDir2 = new File(recording1Dir, "PA2")
          testRecording1OutputDir1.listFiles().length must be(2)
          testRecording1OutputDir2.listFiles().length must be(2)
          checkTotalSize(imageFilenamesOnly, testRecording1OutputDir1.listFiles())
          checkTotalSize(imageFilenamesOnly, testRecording1OutputDir2.listFiles())
          // Publish StopRecordingInput event
          sendStopRecordingInput()
          // Send 4 test messages
          testRecorder ! NewImage(testInputFiles(4).getCanonicalPath, 12345678005L)
          testRecorder ! NewImage(testInputFiles(5).getCanonicalPath, 12345678006L)
          testRecorder ! NewImage(testInputFiles(6).getCanonicalPath, 12345678007L)
          testRecorder ! NewImage(testInputFiles(7).getCanonicalPath, 12345678008L)
          Thread.sleep(100)
          // Check expected results
          testRecording1OutputDir1.listFiles().length must be(2)
          testRecording1OutputDir2.listFiles().length must be(2)
          checkTotalSize(imageFilenamesOnly, testRecording1OutputDir1.listFiles())
          checkTotalSize(imageFilenamesOnly, testRecording1OutputDir2.listFiles())

          // Publish another StartRecordingInput event
          sendStartRecordingInput(recordingId1)
          Thread.sleep(50)
          // Send 4 test messages
          testRecorder ! NewImage(testInputFiles(8).getCanonicalPath, 12345678009L)
          testRecorder ! NewImage(testInputFiles(9).getCanonicalPath, 12345678010L)
          testRecorder ! NewImage(testInputFiles(10).getCanonicalPath, 12345678011L)
          testRecorder ! NewImage(testInputFiles(11).getCanonicalPath, 12345678012L)
          Thread.sleep(100)
          // Check expected results
          info("So multiple recording sessions with the same recordingId are possible (all written to one directory)")
          testRecording1OutputDir1.listFiles().length must be(4)
          testRecording1OutputDir2.listFiles().length must be(4)
          checkTotalSize(imageFilenamesOnly, testRecording1OutputDir1.listFiles())
          checkTotalSize(imageFilenamesOnly, testRecording1OutputDir2.listFiles())
        }
      }
    }

  }

  def copyTestResource(resourceName: String, toFilePath: File): File = {
    copyFile(new File(imageRecorderResourceDir, resourceName), toFilePath)
  }

  def checkTotalSize(imageFilenamesOnly: Boolean, files: Seq[File]): Unit = {
    (imageFilenamesOnly, totalSize(files)) match {
      case (false, totalSize) ⇒
        (totalSize > 0) must be(true)
      case (true, totalSize) ⇒
        totalSize must be(0)
    }
  }

  def totalSize(files: Seq[File]) = files.foldLeft(0L) { (acc, cur) ⇒ acc + cur.length() }

}
