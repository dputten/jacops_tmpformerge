/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.messages

import java.util.Date

import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import scala.concurrent.duration._
import csc.sitebuffer.messages.{ Accelerometer, IsAliveCheck, SensorMessage }
import csc.sitebuffer.process.SystemSensorMessage
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }

/**
 * Test van IRoseIsAliveMonitor
 */
class IRoseIsAliveMonitorTest extends TestKit(ActorSystem("IRoseIsAliveMonitorTest")) with WordSpecLike with MustMatchers with BeforeAndAfterEach with BeforeAndAfterAll {

  var testProbe: TestProbe = _
  var iRoseIsAliveRef: TestActorRef[IRoseIsAliveMonitor] = _

  override def beforeAll() {
    super.beforeAll()
  }

  override def afterAll() {
    system.shutdown
  }

  override def beforeEach() {
    testProbe = TestProbe()
    iRoseIsAliveRef = TestActorRef(new IRoseIsAliveMonitor(systemSensorMessageSender = testProbe.ref))
  }

  implicit def intWithTimes(n: Int) = new {
    def times(f: ⇒ Unit) = 1 to n foreach { _ ⇒ f }
  }

  "The IRoseIsAliveMonitor" when {

    "first receiving a IsAliveCheck message" must {
      "do nothing (isAliveCheck passes)" in {
        // create message
        val isAliveCheck = IsAliveCheck()
        // send message
        iRoseIsAliveRef ! isAliveCheck
        // test for expected result
        testProbe.expectNoMsg(1000 millis)
      }
    }

    "first receiving 2 IsAliveCheck messages" must {
      "send one SystemSensorMessage(IsAlive, OutOfRange) to the systemSensorMessageSender" in {
        // create message
        val isAliveCheck = IsAliveCheck()
        // construct expected message
        val expectedMsg = SystemSensorMessage(
          sensor = "IsAlive",
          timeStamp = new Date().getTime,
          systemId = "",
          event = "OutOfRange",
          sendingComponent = "Sitebuffer",
          data = Map())
        // send message
        iRoseIsAliveRef ! isAliveCheck
        iRoseIsAliveRef ! isAliveCheck
        // test for expected result
        val receivedMessages = testProbe.receiveN(1, 1000 millis)
        receivedMessages.head match {
          case message: SystemSensorMessage ⇒ checkEqualExceptTimestamp(message, expectedMsg)
          case _                            ⇒ fail("Wrong message received")
        }
      }
    }

    "first receiving several SensorMessages followed by one IsAliveCheck message" must {
      "do nothing (isAliveCheck passes)" in {
        // send messages
        5.times {
          iRoseIsAliveRef ! createSensorMessage
        }
        iRoseIsAliveRef ! IsAliveCheck()
        // test for expected result
        testProbe.expectNoMsg(1000 millis)
      }
    }

    "first receiving several SensorMessages followed by 2 IsAliveCheck messages" must {
      "send one SystemSensorMessage(IsAlive, OutOfRange) to the systemSensorMessageSender" in {
        // create message
        val isAliveCheck = IsAliveCheck()
        // construct expected message
        val expectedMsg = SystemSensorMessage(
          sensor = "IsAlive",
          timeStamp = new Date().getTime,
          systemId = "",
          event = "OutOfRange",
          sendingComponent = "Sitebuffer",
          data = Map())
        // send messages
        5.times {
          iRoseIsAliveRef ! createSensorMessage
        }
        2.times {
          iRoseIsAliveRef ! isAliveCheck
        }
        // test for expected result
        val receivedMessages = testProbe.receiveN(1, 1000 millis)
        receivedMessages.head match {
          case message: SystemSensorMessage ⇒ checkEqualExceptTimestamp(message, expectedMsg)
          case _                            ⇒ fail("Wrong message received")
        }
      }
    }

    "first receiving several SensorMessages followed by 3 IsAliveCheck messages" must {
      "send one SystemSensorMessage(IsAlive, OutOfRange) to the systemSensorMessageSender" in {
        // create message
        val isAliveCheck = IsAliveCheck()
        // construct expected message
        val expectedMsg = SystemSensorMessage(
          sensor = "IsAlive",
          timeStamp = new Date().getTime,
          systemId = "",
          event = "OutOfRange",
          sendingComponent = "Sitebuffer",
          data = Map())
        // send messages
        5.times {
          iRoseIsAliveRef ! createSensorMessage
        }
        3.times {
          iRoseIsAliveRef ! isAliveCheck
        }
        // test for expected result
        val receivedMessages = testProbe.receiveN(1, 1000 millis)
        receivedMessages.head match {
          case message: SystemSensorMessage ⇒ checkEqualExceptTimestamp(message, expectedMsg)
          case _                            ⇒ fail("Wrong message received")
        }
      }
    }

    "first receiving several SensorMessages followed by 4 IsAliveCheck messages" must {
      "send 2 SystemSensorMessage(IsAlive, OutOfRange) to the systemSensorMessageSender" in {
        // create message
        val isAliveCheck = IsAliveCheck()
        // construct expected message
        val expectedMsg = SystemSensorMessage(
          sensor = "IsAlive",
          timeStamp = new Date().getTime,
          systemId = "",
          event = "OutOfRange",
          sendingComponent = "Sitebuffer",
          data = Map())
        // send messages
        5.times {
          iRoseIsAliveRef ! createSensorMessage
        }
        4.times {
          iRoseIsAliveRef ! isAliveCheck
        }
        // test for expected result
        val receivedMessages = testProbe.receiveN(2, 1000 millis)
        receivedMessages.foreach { message ⇒
          message match {
            case message: SystemSensorMessage ⇒ checkEqualExceptTimestamp(message, expectedMsg)
            case _                            ⇒ fail("Wrong message received")
          }
        }
      }
    }

    "receiving alternate SensorMessage and IsAliveCheck messages" must {
      "do nothing (all isAliveCheck's pass)" in {
        // create message
        val isAliveCheck = IsAliveCheck()
        // send messages
        10.times {
          iRoseIsAliveRef ! createSensorMessage
          iRoseIsAliveRef ! isAliveCheck
        }
        // test for expected result
        testProbe.expectNoMsg(2000 millis)
      }
    }

  }

  def checkEqualExceptTimestamp(receivedMessage: SystemSensorMessage, expectedMessage: SystemSensorMessage) {
    val receivedMessageWithExpectedDatetime = receivedMessage.copy(timeStamp = expectedMessage.timeStamp)
    receivedMessageWithExpectedDatetime must be(expectedMessage)
    (receivedMessage.timeStamp - expectedMessage.timeStamp < 200) must be(true)
  }

  def createSensorMessage: SensorMessage = {
    SensorMessage(dateTime = new Date().getTime, temperature = 12, humidity = 23, acceleration = Accelerometer(1, 2, 3))
  }

}
