package csc.sitebuffer.process.irose

import akka.actor.ActorSystem
import akka.testkit.TestKit
import csc.akka.logging.DirectLogging
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import org.scalatest._

import scala.collection.mutable.ListBuffer

/**
 * Created by jeijlders on 5/6/14.
 */
class IRoseCusStatParserTest extends TestKit(ActorSystem("IRoseCusStatParserTest")) with WordSpecLike with MustMatchers
  with DirectLogging with BeforeAndAfterAll {
  override def afterAll() {
    try {
      system.shutdown
    } catch {
      case e: Exception ⇒ log.error("COULD NOT SHUTDOWN ACTOR REGISTRY".format(e))
    }
  }

  "IRoseCusStatParserTest" when {

    "parsing een cust stat bericht zonder fouten" must {
      "helemaal niets retourneren" in {
        val str = "loop 0 Last value: 1578703 Frequency: 79179Hz in/out/retry: 126/0/0 base: 1578691 1577152 1577918 1578502 A:1579149, B:1578244 dev: 0,057% Debug: 1 0 0 0 0 0 loop 1 Last value: 1583517 Frequency: 78939Hz in/out/retry: 126/0/0 base: 1583499 1581956 1582724 1583309 A:1584018, B:1582981 dev: 0,065% Debug: 1 0 0 0 0 0 loop 2 Last value: 1597160 Frequency: 78262Hz in/out/retry: 232/0/0 base: 1597193 1595636 1596411 1597002 A:1597704, B:1596696 dev: 0,063% Debug: 0 0 0 0 0 0 "
        val iroseCusStatParser: IRoseCusStatParser = new IRoseCusStatParser()
        val result = iroseCusStatParser.getIRoseLoopErrors(str)
        result.length must be(0)
      }
    }

    "parsing een cust stat bericht met 8 loops waarvan 2 met een error" must {
      "een lijst retourneren met 2-iroseLoopErrors voor lus 5 en 6" in {
        val str = "loop 0 Last value: 1578703 Frequency: 79179Hz in/out/retry: 126/0/0 base: 1578691 1577152 1577918 1578502 A:1579149, B:1578244 dev: 0,057% Debug: 1 0 0 0 0 0 loop 1 Last value: 1583517 Frequency: 78939Hz in/out/retry: 126/0/0 base: 1583499 1581956 1582724 1583309 A:1584018, B:1582981 dev: 0,065% Debug: 1 0 0 0 0 0 loop 2 Last value: 1597160 Frequency: 78262Hz in/out/retry: 232/0/0 base: 1597193 1595636 1596411 1597002 A:1597704, B:1596696 dev: 0,063% Debug: 0 0 0 0 0 0 loop 3 Last value: 1570258 Frequency: 79605Hz in/out/retry: 232/0/0 base: 1570251 1568721 1569482 1570063 A:1570949, B:1569554 dev: 0,088%  Debug: 0 0 0 0 0 0  loop 4: error (state = 5, lastvalue = 0, wait 0, nSubState 0  Last value: 0 Frequency: 0Hz in/out/retry: 0/0/1  base: 0 0 0 0 A: 0, B: 0 dev: 0,000%  loop 5: error (state = 5, lastvalue = 0, wait 0, nSubState 0  Last value: 0 Frequency: 0Hz in/out/retry: 0/0/1  base: 0 0 0 0 A: 0, B: 0 dev: 0,000%  loop 6: error (state = 1, lastvalue = 0, wait 0, nSubState 0  Last value: 0 Frequency: 0Hz in/out/retry: 0/0/1  base: 0 0 0 0 A: 0, B: 0 dev: 0,000%  loop 7: error (state = 6, lastvalue = 0, wait 0, nSubState 0  Last value: 0 Frequency: 0Hz in/out/retry: 0/0/1  base: 0 0 0 0 A: 0, B: 0 dev: 0,000%"
        val iroseCusStatParser: IRoseCusStatParser = new IRoseCusStatParser()
        val result = iroseCusStatParser.getIRoseLoopErrors(str)

        result.length must be(2)
        result.contains(new IRoseLoopError("4", IRoseCusStatParser.error5Description)) must be(true)
        result.contains(new IRoseLoopError("5", IRoseCusStatParser.error5Description)) must be(true)

      }
    }

    "parsing een cust stat bericht met twee loops waarvan 1 met een error" must {
      "twee retourneren voor de loops en 1 error" in {
        val str = "loop 3 Last value: 1570258 Frequency: 79605Hz in/out/retry: 232/0/0 base: 1570251 1568721 1569482 1570063 A:1570949, B:1569554 dev: 0,088% Debug: 0 0 0 0 0 0 loop 4: error (state = 5), lastvalue = 0, wait 0, nSubState 0"
        val iroseCusStatParser: IRoseCusStatParser = new IRoseCusStatParser()
        val loops: ListBuffer[String] = iroseCusStatParser.extractStringsWithIndexes(str, iroseCusStatParser.getIndexesOfArgument(str, "loop"))

        loops.length must be(2)
      }
    }

    "parsing een cust stat bericht met 8 loops waarvan 4 met een error waarvan 1 met state=4 en 1 met state=5" must {
      "8 retourneren voor de loops en 4 errors en twee voor luslos" in {
        val achtLoopsWaarvanVierError = "loop 0 Last value: 1578703 Frequency: 79179Hz in/out/retry: 126/0/0 base: 1578691 1577152 1577918 1578502 A:1579149, B:1578244 dev: 0,057% Debug: 1 0 0 0 0 0 loop 1 Last value: 1583517 Frequency: 78939Hz in/out/retry: 126/0/0 base: 1583499 1581956 1582724 1583309 A:1584018, B:1582981 dev: 0,065% Debug: 1 0 0 0 0 0 loop 2 Last value: 1597160 Frequency: 78262Hz in/out/retry: 232/0/0 base: 1597193 1595636 1596411 1597002 A:1597704, B:1596696 dev: 0,063% Debug: 0 0 0 0 0 0 loop 3 Last value: 1570258 Frequency: 79605Hz in/out/retry: 232/0/0 base: 1570251 1568721 1569482 1570063 A:1570949, B:1569554 dev: 0,088%  Debug: 0 0 0 0 0 0  loop 4: error (state = 5, lastvalue = 0, wait 0, nSubState 0  Last value: 0 Frequency: 0Hz in/out/retry: 0/0/1  base: 0 0 0 0 A: 0, B: 0 dev: 0,000%  loop 5: error (state = 5, lastvalue = 0, wait 0, nSubState 0  Last value: 0 Frequency: 0Hz in/out/retry: 0/0/1  base: 0 0 0 0 A: 0, B: 0 dev: 0,000%  loop 6: error (state = 1, lastvalue = 0, wait 0, nSubState 0  Last value: 0 Frequency: 0Hz in/out/retry: 0/0/1  base: 0 0 0 0 A: 0, B: 0 dev: 0,000%  loop 7: error (state = 6, lastvalue = 0, wait 0, nSubState 0  Last value: 0 Frequency: 0Hz in/out/retry: 0/0/1  base: 0 0 0 0 A: 0, B: 0 dev: 0,000%"
        val iroseCusStatParser: IRoseCusStatParser = new IRoseCusStatParser()
        val loops: ListBuffer[String] = iroseCusStatParser.extractStringsWithIndexes(achtLoopsWaarvanVierError, iroseCusStatParser.getIndexesOfArgument(achtLoopsWaarvanVierError, "loop"))
        val loopsMetErrorsEnGeenLusAangesloten: ListBuffer[String] = iroseCusStatParser.getLoopsWithoutLoopConnected(loops)
        val iroseLoopErrors: ListBuffer[Option[IRoseLoopError]] = new ListBuffer[Option[IRoseLoopError]]()

        loopsMetErrorsEnGeenLusAangesloten.foreach(loop ⇒ iroseLoopErrors += iroseCusStatParser.getIRoseLoopError(loop))

        loops.length must be(8)
        loopsMetErrorsEnGeenLusAangesloten.length must be(2)
        iroseLoopErrors.flatten.length must be(2)
      }
    }

    "parsing een cust stat bericht zonder loops" must {
      "moet geen loops retourneren" in {
        val str = "Last value: 1570258 Frequency: 79605Hz in/out/retry: 232/0/0 base: 1570251 1568721 1569482 1570063 A:1570949, B:1569554 dev: 0,088% Debug: 0 0 0 0 0 0  error (state = 5), lastvalue = 0, wait 0, nSubState 0"

        val iroseCusStatParser: IRoseCusStatParser = new IRoseCusStatParser()
        val loops = iroseCusStatParser.extractStringsWithIndexes(str, iroseCusStatParser.getIndexesOfArgument(str, "loop"))

        loops.length must be(0)
      }
    }
  }
}
