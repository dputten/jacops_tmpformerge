/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.send

import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import scala.concurrent.duration._
import csc.sitebuffer.process.SystemSensorMessage
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }

/**
 * Test van SystemSensorMessageFilter
 */
class SystemSensorMessageFilterTest extends TestKit(ActorSystem("IRoseLoopFailureMonitorTest")) with WordSpecLike with MustMatchers
  with BeforeAndAfterEach with BeforeAndAfterAll {

  var mockCurrentTime: Long = _
  val dummyCurrentTime = () ⇒ mockCurrentTime

  var testProbe: TestProbe = _
  var systemSensorMessageFilterActor: TestActorRef[SystemSensorMessageFilter] = _

  override def beforeAll() {
    super.beforeAll()
  }

  override def afterAll() {
    system.shutdown()
  }

  override def beforeEach() {
    testProbe = TestProbe()
    systemSensorMessageFilterActor = TestActorRef(
      new SystemSensorMessageFilter(filterWindowMs = 100, getCurrentTime = dummyCurrentTime, nextActor = testProbe.ref))
  }

  def sendMessage(message: SystemSensorMessage, currentTime: Long) {
    mockCurrentTime = currentTime
    systemSensorMessageFilterActor ! message
  }

  "The SystemSensorMessageFilter (filterWindowMs = 100)" when {

    "receiving 2 SystemSensorMessage messages of the same type exactly 99ms apart" must {
      "only pass on the first message" in {
        // create and send message
        val message1 = SystemSensorMessage(sensor = "sensor", timeStamp = 12340001L, systemId = "systemId", event = "Failure", sendingComponent = "Component", data = Map())
        sendMessage(message1, 1)
        // test for expected result
        testProbe.expectMsg(500 millis, message1)
        // create message
        val message2 = SystemSensorMessage(sensor = "sensor", timeStamp = 12340099L, systemId = "systemId", event = "Failure", sendingComponent = "Component", data = Map())
        sendMessage(message2, 99)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
      }
    }

    "receiving 2 SystemSensorMessage messages of the same type exactly 100ms apart" must {
      "only pass on the first message" in {
        // create and send message
        val message1 = SystemSensorMessage(sensor = "sensor", timeStamp = 12340001L, systemId = "systemId", event = "Failure", sendingComponent = "Component", data = Map())
        sendMessage(message1, 1)
        // test for expected result
        testProbe.expectMsg(500 millis, message1)
        // create message
        val message2 = SystemSensorMessage(sensor = "sensor", timeStamp = 12340100L, systemId = "systemId", event = "Failure", sendingComponent = "Component", data = Map())
        sendMessage(message2, 100)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
      }
    }

    "receiving 2 SystemSensorMessage messages of the same type exactly 101ms apart" must {
      "pass on both messages" in {
        // create and send message
        val message1 = SystemSensorMessage(sensor = "sensor", timeStamp = 12340001L, systemId = "systemId", event = "Failure", sendingComponent = "Component", data = Map())
        sendMessage(message1, 1)
        // test for expected result
        testProbe.expectMsg(500 millis, message1)
        // create message
        val message2 = SystemSensorMessage(sensor = "sensor", timeStamp = 12340101L, systemId = "systemId", event = "Failure", sendingComponent = "Component", data = Map())
        sendMessage(message2, 101)
        // test for expected result
        testProbe.expectMsg(500 millis, message2)
      }
    }

    "receiving 5 SystemSensorMessage messages of the same type within a 99ms window" must {
      "only pass on the first message" in {
        // create and send message
        val message1 = SystemSensorMessage(sensor = "sensor", timeStamp = 12340001L, systemId = "systemId", event = "Failure", sendingComponent = "Component", data = Map())
        sendMessage(message1, 1)
        // test for expected result
        testProbe.expectMsg(500 millis, message1)
        // create and send message
        val message2 = SystemSensorMessage(sensor = "sensor", timeStamp = 12340011L, systemId = "systemId", event = "Failure", sendingComponent = "Component", data = Map())
        sendMessage(message2, 11)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // create and send message
        val message3 = SystemSensorMessage(sensor = "sensor", timeStamp = 12340041L, systemId = "systemId", event = "Failure", sendingComponent = "Component", data = Map())
        sendMessage(message3, 41)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // create and send message
        val message4 = SystemSensorMessage(sensor = "sensor", timeStamp = 12340071L, systemId = "systemId", event = "Failure", sendingComponent = "Component", data = Map())
        sendMessage(message4, 71)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // create message
        val message5 = SystemSensorMessage(sensor = "sensor", timeStamp = 12340099L, systemId = "systemId", event = "Failure", sendingComponent = "Component", data = Map())
        sendMessage(message5, 99)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
      }
    }

    "receiving 5 SystemSensorMessage messages of the same type within a 100ms window" must {
      "only pass on the first message" in {
        // create and send message
        val message1 = SystemSensorMessage(sensor = "sensor", timeStamp = 12340001L, systemId = "systemId", event = "Failure", sendingComponent = "Component", data = Map())
        sendMessage(message1, 1)
        // test for expected result
        testProbe.expectMsg(500 millis, message1)
        // create and send message
        val message2 = SystemSensorMessage(sensor = "sensor", timeStamp = 12340011L, systemId = "systemId", event = "Failure", sendingComponent = "Component", data = Map())
        sendMessage(message2, 11)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // create and send message
        val message3 = SystemSensorMessage(sensor = "sensor", timeStamp = 12340041L, systemId = "systemId", event = "Failure", sendingComponent = "Component", data = Map())
        sendMessage(message3, 41)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // create and send message
        val message4 = SystemSensorMessage(sensor = "sensor", timeStamp = 12340071L, systemId = "systemId", event = "Failure", sendingComponent = "Component", data = Map())
        sendMessage(message4, 71)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // create message
        val message5 = SystemSensorMessage(sensor = "sensor", timeStamp = 12340100L, systemId = "systemId", event = "Failure", sendingComponent = "Component", data = Map())
        sendMessage(message5, 100)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
      }
    }

    "receiving 5 SystemSensorMessage messages of the same type within a 101ms window" must {
      "only pass on the first and the last message" in {
        // create and send message
        val message1 = SystemSensorMessage(sensor = "sensor", timeStamp = 12340001L, systemId = "systemId", event = "Failure", sendingComponent = "Component", data = Map())
        sendMessage(message1, 1)
        // test for expected result
        testProbe.expectMsg(500 millis, message1)
        // create and send message
        val message2 = SystemSensorMessage(sensor = "sensor", timeStamp = 12340011L, systemId = "systemId", event = "Failure", sendingComponent = "Component", data = Map())
        sendMessage(message2, 11)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // create and send message
        val message3 = SystemSensorMessage(sensor = "sensor", timeStamp = 12340041L, systemId = "systemId", event = "Failure", sendingComponent = "Component", data = Map())
        sendMessage(message3, 41)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // create and send message
        val message4 = SystemSensorMessage(sensor = "sensor", timeStamp = 12340071L, systemId = "systemId", event = "Failure", sendingComponent = "Component", data = Map())
        sendMessage(message4, 71)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // create message
        val message5 = SystemSensorMessage(sensor = "sensor", timeStamp = 12340101L, systemId = "systemId", event = "Failure", sendingComponent = "Component", data = Map())
        sendMessage(message5, 101)
        // test for expected result
        testProbe.expectMsg(500 millis, message5)
      }
    }

    "receiving 2 SystemSensorMessage messages with different sensors within filterWindow" must {
      "pass on both messages (different sensors means different type of messages)" in {
        // create and send message
        val message1 = SystemSensorMessage(sensor = "sensor1", timeStamp = 12340001L, systemId = "systemId", event = "Failure", sendingComponent = "Component", data = Map())
        sendMessage(message1, 1)
        // test for expected result
        testProbe.expectMsg(500 millis, message1)
        // create message
        val message2 = SystemSensorMessage(sensor = "sensor2", timeStamp = 12340010L, systemId = "systemId", event = "Failure", sendingComponent = "Component", data = Map())
        sendMessage(message2, 10)
        // test for expected result
        testProbe.expectMsg(500 millis, message2)
      }
    }

    "receiving 2 SystemSensorMessage messages with different messages within filterWindow" must {
      "pass on both messages (different messages means different type of messages)" in {
        // create and send message
        val message1 = SystemSensorMessage(sensor = "sensor", timeStamp = 12340001L, systemId = "systemId", event = "Failure", sendingComponent = "Component", data = Map())
        sendMessage(message1, 1)
        // test for expected result
        testProbe.expectMsg(500 millis, message1)
        // create message
        val message2 = SystemSensorMessage(sensor = "sensor", timeStamp = 12340010L, systemId = "systemId", event = "OutOfRange", sendingComponent = "Component", data = Map())
        sendMessage(message2, 10)
        // test for expected result
        testProbe.expectMsg(500 millis, message2)
      }
    }

    "receiving 2 SystemSensorMessage messages with different components within filterWindow" must {
      "pass on both messages (different components means different type of messages)" in {
        // create and send message
        val message1 = SystemSensorMessage(sensor = "sensor", timeStamp = 12340001L, systemId = "systemId", event = "Failure", sendingComponent = "Component1", data = Map())
        sendMessage(message1, 1)
        // test for expected result
        testProbe.expectMsg(500 millis, message1)
        // create message
        val message2 = SystemSensorMessage(sensor = "sensor", timeStamp = 12340010L, systemId = "systemId", event = "Failure", sendingComponent = "Component2", data = Map())
        sendMessage(message2, 10)
        // test for expected result
        testProbe.expectMsg(500 millis, message2)
      }
    }

    "receiving 2 SystemSensorMessage messages with different data within filterWindow" must {
      "pass on both messages (different data means different type of messages)" in {
        // create and send message
        val message1 = SystemSensorMessage(sensor = "sensor", timeStamp = 12340001L, systemId = "systemId", event = "Failure", sendingComponent = "Component", data = Map("1" -> "One"))
        sendMessage(message1, 1)
        // test for expected result
        testProbe.expectMsg(500 millis, message1)
        // create message
        val message2 = SystemSensorMessage(sensor = "sensor", timeStamp = 12340010L, systemId = "systemId", event = "Failure", sendingComponent = "Component", data = Map("1" -> "Een"))
        sendMessage(message2, 10)
        // test for expected result
        testProbe.expectMsg(500 millis, message2)
      }
    }

    "receiving one SystemSensorMessage of the same type every 20ms for 300ms in total" must {
      "pass on every fifth message" in {
        (0 to 300 by 20).foreach { timestamp ⇒
          // create and send message
          val message = SystemSensorMessage(sensor = "sensor", timeStamp = 12340000L + timestamp, systemId = "systemId", event = "Failure", sendingComponent = "Component", data = Map())
          sendMessage(message, timestamp)
          // test for expected result
          if (timestamp % 100 == 0) {
            testProbe.expectMsg(500 millis, message)
          } else {
            testProbe.expectNoMsg(500 millis)
          }
        }
      }
    }

    "receiving multiple SystemSensorMessages of different types in a 500ms" must {
      "correctly filter the messages (simulation test)" in {
        def sendingMessageExpected(messageType: Int, timestamp: Int): Boolean = messageType match {
          case 1 ⇒ (timestamp % 20 == 0) // Send message 1 every 20 ms
          case 2 ⇒ (timestamp % 40 == 0) // Send message 2 every 40 ms
          case _ ⇒ (timestamp % 160 == 0) // Send message 3 every 160 ms
        }
        (0 to 500 by 20).foreach { timestamp ⇒
          if (sendingMessageExpected(1, timestamp)) {
            // create and send messages
            val message = SystemSensorMessage(sensor = "sensor1", timeStamp = 12340000L + timestamp, systemId = "systemId", event = "Failure", sendingComponent = "Component", data = Map())
            sendMessage(message, timestamp)
            // test for expected result
            if (Seq(0, 100, 200, 300, 400, 500).contains(timestamp)) {
              testProbe.expectMsg(500 millis, message)
            } else {
              testProbe.expectNoMsg(500 millis)
            }
          }
          if (sendingMessageExpected(2, timestamp)) {
            // create and send messages
            val message = SystemSensorMessage(sensor = "sensor2", timeStamp = 12340001L + timestamp, systemId = "systemId", event = "Failure", sendingComponent = "Component", data = Map())
            sendMessage(message, timestamp)
            // test for expected result
            if (Seq(0, 120, 240, 360, 480).contains(timestamp)) {
              testProbe.expectMsg(500 millis, message)
            } else {
              testProbe.expectNoMsg(500 millis)
            }
          }
          if (sendingMessageExpected(3, timestamp)) {
            // create and send messages
            val message = SystemSensorMessage(sensor = "sensor3", timeStamp = 12340002L + timestamp, systemId = "systemId", event = "Failure", sendingComponent = "Component", data = Map())
            sendMessage(message, timestamp)
            // test for expected result
            if (Seq(0, 160, 320, 480).contains(timestamp)) {
              testProbe.expectMsg(500 millis, message)
            } else {
              testProbe.expectNoMsg(500 millis)
            }
          }
        }
      }
    }

  }

}
