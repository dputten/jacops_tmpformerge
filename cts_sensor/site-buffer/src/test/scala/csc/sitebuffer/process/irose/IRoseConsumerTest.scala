/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.irose

import akka.actor.{ Actor, ActorRef, ActorSystem }
import akka.camel.Producer
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import scala.concurrent.duration._
import org.scalatest._

/**
 * Test van IRoseConsumer actor.
 */
class IRoseConsumerTest extends TestKit(ActorSystem("IRoseConsumerTest")) with WordSpecLike with MustMatchers with BeforeAndAfterAll {

  var currentTime: Long = _
  val dummyCurrentTime = () ⇒ currentTime

  val iRoseProducer = TestActorRef(new TestIRoseProducer())
  val iRoseMessageRecipients = Set(TestProbe(), TestProbe())

  val iRoseConsumerActor = createIRoseConsumer(
    iRoseStringRecipients = iRoseMessageRecipients.map(_.ref))

  override def beforeAll() {
    super.beforeAll()
  }

  override def afterAll() {
    iRoseProducer.stop()
    system.shutdown()
  }

  def createIRoseConsumer(iRoseStringRecipients: Set[ActorRef]): TestActorRef[IRoseConsumer] = {
    TestActorRef(
      new IRoseConsumer(
        iRoseEndpointHost = "localhost",
        iRoseEndpointPort = 12321,
        getCurrentTime = dummyCurrentTime,
        recipients = iRoseStringRecipients))
  }

  "An IRoseConsumer actor" when {

    "receiving a vehicle message string" must {
      "send an IRoseString object, containing the string, to all recipients" in {
        currentTime = 123456100
        val iRoseMessage = vehicleMessage1
        iRoseProducer ! iRoseMessage
        val expectedMessage = IRoseString(string = iRoseMessage, timestamp = currentTime)
        expectMessage(expectedMessage, iRoseMessageRecipients)
      }
    }

    "receiving an input message string with inputId = 8" must {
      "send an IRoseString object, containing the string, to all recipients" in {
        currentTime = 123456101
        val iRoseMessage = alarmInputEventMessage1
        iRoseProducer ! iRoseMessage
        val expectedMessage = IRoseString(string = iRoseMessage, timestamp = currentTime)
        expectMessage(expectedMessage, iRoseMessageRecipients)
      }
    }

    "receiving an input message string (inputId <> 8)" must {
      "send an IRoseString object, containing the string, to all recipients" in {
        currentTime = 123456102
        val iRoseMessage = inputEventMessage1
        iRoseProducer ! iRoseMessage
        val expectedMessage = IRoseString(string = iRoseMessage, timestamp = currentTime)
        expectMessage(expectedMessage, iRoseMessageRecipients)
      }
    }

    "receiving a sensor message string" must {
      "send an IRoseString object, containing the string, to all recipients" in {
        currentTime = 123456103
        val iRoseMessage = sensorMessage1
        iRoseProducer ! iRoseMessage
        val expectedMessage = IRoseString(string = iRoseMessage, timestamp = currentTime)
        expectMessage(expectedMessage, iRoseMessageRecipients)
      }
    }

    "receiving a single loop message string" must {
      "send an IRoseString object, containing the string, to all recipients" in {
        currentTime = 123456104
        val iRoseMessage = singleLoopMessage1
        iRoseProducer ! iRoseMessage
        val expectedMessage = IRoseString(string = iRoseMessage, timestamp = currentTime)
        expectMessage(expectedMessage, iRoseMessageRecipients)
      }
    }

    "receiving an invalid message string" must {
      "send an IRoseString object, containing the string, to all recipients" in {
        currentTime = 123456105
        val invalidMessage = createMessageWithCrc("130212;072813,123;this;is;an;invalid;message;")
        iRoseProducer ! invalidMessage
        val expectedMessage = IRoseString(string = invalidMessage, timestamp = currentTime)
        expectMessage(expectedMessage, iRoseMessageRecipients)
      }
    }

    "receiving multiple message strings of different types" must {
      "send all corresponding IRoseMessage's to all recipients" in {
        currentTime = 123456200
        var message = vehicleMessage1
        iRoseProducer ! message
        var expectedMessage = IRoseString(string = message, timestamp = currentTime)
        expectMessage(expectedMessage, iRoseMessageRecipients)

        currentTime = 123456201
        message = sensorMessage1
        iRoseProducer ! message
        expectedMessage = IRoseString(string = message, timestamp = currentTime)
        expectMessage(expectedMessage, iRoseMessageRecipients)

        currentTime = 123456202
        message = inputEventMessage1
        iRoseProducer ! message
        expectedMessage = IRoseString(string = message, timestamp = currentTime)
        expectMessage(expectedMessage, iRoseMessageRecipients)

        currentTime = 123456203
        message = singleLoopMessage1
        iRoseProducer ! message
        expectedMessage = IRoseString(string = message, timestamp = currentTime)
        expectMessage(expectedMessage, iRoseMessageRecipients)

        currentTime = 123456204
        message = inputEventMessage2
        iRoseProducer ! message
        expectedMessage = IRoseString(string = message, timestamp = currentTime)
        expectMessage(expectedMessage, iRoseMessageRecipients)

        currentTime = 123456205
        message = alarmInputEventMessage1
        iRoseProducer ! message
        expectedMessage = IRoseString(string = message, timestamp = currentTime)
        expectMessage(expectedMessage, iRoseMessageRecipients)

        currentTime = 123456206
        message = sensorMessage2
        iRoseProducer ! message
        expectedMessage = IRoseString(string = message, timestamp = currentTime)
        expectMessage(expectedMessage, iRoseMessageRecipients)

        currentTime = 123456207
        message = alarmInputEventMessage2
        iRoseProducer ! message
        expectedMessage = IRoseString(string = message, timestamp = currentTime)
        expectMessage(expectedMessage, iRoseMessageRecipients)

        currentTime = 123456208
        message = singleLoopMessage2
        iRoseProducer ! message
        expectedMessage = IRoseString(string = message, timestamp = currentTime)
        expectMessage(expectedMessage, iRoseMessageRecipients)

        currentTime = 123456209
        message = vehicleMessage2
        iRoseProducer ! message
        expectedMessage = IRoseString(string = message, timestamp = currentTime)
        expectMessage(expectedMessage, iRoseMessageRecipients)

        currentTime = 123456210
        message = vehicleMessage3
        iRoseProducer ! message
        expectedMessage = IRoseString(string = message, timestamp = currentTime)
        expectMessage(expectedMessage, iRoseMessageRecipients)
      }
    }

  }

  def expectMessage(message: IRoseString, recipients: Set[TestProbe]) {
    recipients.foreach { recipient ⇒
      recipient.expectMsg(500 millis, message)
    }
  }

  def expectNoMessage(recipients: Set[TestProbe]) {
    recipients.foreach { recipient ⇒
      recipient.expectNoMsg(500 millis)
    }
  }

  def createMessageWithCrc(message: String): String = {
    def calcCrc(message: String): Int = {
      message.foldLeft(0)((accu, ch) ⇒ accu ^ ch)
    }
    message + "%02X".format(calcCrc(message))
  }

  def vehicleMessage1: String = {
    createMessageWithCrc("130212;072813,123;1;1;023;500;00000;00400;00450;00875;0000,0;0000,0;130212;072813,005;0000,0;0000,0;700101;010000,000;0000,0;0000,0;")
  }

  def vehicleMessage2: String = {
    createMessageWithCrc("130212;072813,125;1;1;023;500;00000;00400;00450;00888;0000,0;0000,0;130212;072813,005;0000,0;0000,0;700101;010000,000;0000,0;0000,0;0;")
  }

  def vehicleMessage3: String = {
    createMessageWithCrc("130212;072813,123;1;1;023;500;00000;00400;00450;00875;0000,0;0000,0;130212;072813,005;0000,0;0000,0;700101;010000,000;0000,0;0000,0;0;seri;1;10;22;")
  }

  def alarmInputEventMessage1: String = {
    createMessageWithCrc("130212;072813,123;2;8;3;something;")
  }

  def alarmInputEventMessage2: String = {
    createMessageWithCrc("130212;072813,155;2;8;2;something else;")
  }

  def inputEventMessage1: String = {
    createMessageWithCrc("130212;072813,123;2;3;3;something;")
  }

  def inputEventMessage2: String = {
    createMessageWithCrc("130212;072813,155;2;3;1;something else;")
  }

  def sensorMessage1: String = {
    createMessageWithCrc("130212;072813,123;0;010,500;060,000;0003,500;-004,600;005,700;")
  }

  def sensorMessage2: String = {
    createMessageWithCrc("130212;072813,155;0;010,550;060,000;0033,500;-004,600;005,700;")
  }

  def singleLoopMessage1: String = {
    createMessageWithCrc("130212;072813,123;4;2;000;000;00000;00007;00132;00000;")
  }

  def singleLoopMessage2: String = {
    createMessageWithCrc("130212;072813,155;4;2;000;000;00000;00017;00532;00000;")
  }

  class TestIRoseProducer extends Actor with Producer {
    def endpointUri = "mina:tcp://localhost:12321?textline=true&sync=false"
  }
}
