/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.system

import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import scala.concurrent.duration.Duration
import scala.concurrent.duration._
import csc.sitebuffer.process.SystemSensorMessage
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, WordSpec }

/**
 * Test van WasDownMonitor
 */
class WasDownMonitorTest extends TestKit(ActorSystem("WasDownMonitorTest")) with WordSpecLike with MustMatchers with BeforeAndAfterAll {
  var testProbe: TestProbe = TestProbe()
  var testMonitor: TestActorRef[WasDownMonitor with TimestampStorage] = _

  var currentTime: Long = _
  val dummyCurrentTime = () ⇒ currentTime

  var errorModeEnabled: Boolean = false
  var errorMessage: String = _
  var savedTimestamp: Option[Long] = _
  /**
   * Test version of the InOutTextSender trait.
   * Will send a text to the configured receiver.
   */
  trait TestTimestampStorage extends TimestampStorage {

    def storeTimestamp(timestamp: Long): Either[String, String] = {
      if (errorModeEnabled) {
        Left(errorMessage)
      } else {
        savedTimestamp = Some(timestamp)
        Right("Done")
      }
    }

    def getLatestTimestamp: Either[String, Option[Long]] = {
      if (errorModeEnabled) {
        Left(errorMessage)
      } else {
        Right(savedTimestamp)
      }
    }

  }

  override def beforeAll() {
    super.beforeAll()
  }

  override def afterAll() {
    system.shutdown()
  }

  def createTestMonitor(timestampUpdateFrequency: FiniteDuration, minimalDeviationForWasDown: Duration): TestActorRef[WasDownMonitor with TimestampStorage] = {
    TestActorRef(new WasDownMonitor(
      timestampUpdateFrequency = timestampUpdateFrequency,
      minimalDeviationForWasDown = minimalDeviationForWasDown,
      getCurrentTime = dummyCurrentTime,
      systemSensorMessageSender = testProbe.ref) with TestTimestampStorage)
  }

  "The WasDownMonitor (minimalDeviationForWasDown = 100 ms) on startup" when {

    "no timestamp exists in storage" must {
      "do nothing" in {
        // Set up test
        currentTime = 10001000L
        errorModeEnabled = false
        savedTimestamp = None
        // Created tested actor
        testMonitor = createTestMonitor(timestampUpdateFrequency = 1.hour, minimalDeviationForWasDown = 100.millis)
        // Check for expected result
        testProbe.expectNoMsg(5 seconds)
      }
    }

    "a valid timestamp exists in storage and deviates 99ms from current time" must {
      "do nothing" in {
        // Set up test
        currentTime = 10001099L
        errorModeEnabled = false
        savedTimestamp = Some(10001000L)
        // Created tested actor
        testMonitor = createTestMonitor(timestampUpdateFrequency = 1.hour, minimalDeviationForWasDown = 100.millis)
        // Check for expected result
        testProbe.expectNoMsg(5 seconds)
      }
    }

    "a valid timestamp exists in storage and deviates 100ms from current time" must {
      "do nothing" in {
        // Set up test
        currentTime = 10001100L
        errorModeEnabled = false
        savedTimestamp = Some(10001000L)
        // Created tested actor
        testMonitor = createTestMonitor(timestampUpdateFrequency = 1.hour, minimalDeviationForWasDown = 100.millis)
        // Check for expected result
        testProbe.expectNoMsg(5 seconds)
      }
    }

    "a valid timestamp exists in storage and deviates 101ms from current time" must {
      "send a SystemSensorMessage(WasDown, OutOfRange) to the systemSensorMessageSender" in {
        // Set up test
        currentTime = 10001101L
        errorModeEnabled = false
        savedTimestamp = Some(10001000L)
        // Created tested actor
        testMonitor = createTestMonitor(timestampUpdateFrequency = 1.hour, minimalDeviationForWasDown = 100.millis)
        // Check for expected result
        val expectedMsg = SystemSensorMessage(
          sensor = "WasDown",
          timeStamp = 10001101,
          systemId = "",
          event = "OutOfRange",
          sendingComponent = "Sitebuffer",
          data = Map("downStartTime" -> "10001000"))
        testProbe.expectMsg(5 seconds, expectedMsg)
      }
    }

    "there was a problem reading a timestamp from storage" must {
      "do nothing" in {
        // Set up test
        currentTime = 10001100L
        errorModeEnabled = true
        errorMessage = "Oeps reading timestamp"
        savedTimestamp = Some(10001000L)
        // Created tested actor
        testMonitor = createTestMonitor(timestampUpdateFrequency = 3.seconds, minimalDeviationForWasDown = 10.seconds)
        // Check for expected result
        testProbe.expectNoMsg(5 seconds)
      }
    }

  }

  "The WasDownMonitor (minimalDeviationForWasDown = 100 ms)" when {

    "after the timestamp check has been performed" must {
      "regularly update the timestamp in storage" in {
        currentTime = 10001000L
        errorModeEnabled = false
        savedTimestamp = Some(10000999L)
        // Created tested actor
        testMonitor = createTestMonitor(timestampUpdateFrequency = 3.seconds, minimalDeviationForWasDown = 10.seconds)
        Thread.sleep(3000)
        currentTime = 10001010L
        Thread.sleep(3000)
        savedTimestamp must be(Some(10001010L))
        currentTime = 10001020L
        Thread.sleep(3000)
        savedTimestamp must be(Some(10001020L))
        currentTime = 10001030L
        Thread.sleep(3000)
        savedTimestamp must be(Some(10001030L))
      }
    }

    "after the timestamp check has been performed and a SystemSensorMessage has been send" must {
      "regularly update the timestamp in storage" in {
        // Set up test
        currentTime = 10001000L
        errorModeEnabled = false
        savedTimestamp = Some(10000990L)
        // Created tested actor
        testMonitor = createTestMonitor(timestampUpdateFrequency = 1.hour, minimalDeviationForWasDown = 9.millis)
        // Check for expected result
        val expectedMsg = SystemSensorMessage(
          sensor = "WasDown",
          timeStamp = 10001000,
          systemId = "",
          event = "OutOfRange",
          sendingComponent = "Sitebuffer",
          data = Map("downStartTime" -> "10000990"))
        testProbe.expectMsg(5 seconds, expectedMsg)
        Thread.sleep(3000)
        currentTime = 10001010L
        Thread.sleep(3000)
        savedTimestamp must be(Some(10001010L))
        currentTime = 10001020L
        Thread.sleep(3000)
        savedTimestamp must be(Some(10001020L))
        currentTime = 10001030L
        Thread.sleep(3000)
        savedTimestamp must be(Some(10001030L))
      }
    }

  }

}
