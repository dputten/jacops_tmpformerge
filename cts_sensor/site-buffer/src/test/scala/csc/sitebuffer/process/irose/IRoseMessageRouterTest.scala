/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.irose

import akka.actor.{ ActorRef, ActorSystem }
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import scala.concurrent.duration._
import csc.sitebuffer.messages.{ Accelerometer, _ }
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, WordSpec }

/**
 * Test van IRoseMessageRouter
 */
class IRoseMessageRouterTest extends TestKit(ActorSystem("IRoseMessageRouterTest")) with WordSpecLike with MustMatchers with BeforeAndAfterAll {

  val vehicleMessageRecipients = Set(TestProbe(), TestProbe())
  val alarmEventRecipients = Set(TestProbe(), TestProbe())
  val inputEventMessageRecipients = Set(TestProbe(), TestProbe())
  val sensorMessageRecipients = Set(TestProbe(), TestProbe())
  val singleLoopMessageRecipients = Set(TestProbe(), TestProbe())

  val iRoseMessageRouter = createIRoseMessageRouter(
    vehicleMessageRecipients = vehicleMessageRecipients.map(_.ref),
    alarmEventRecipients = alarmEventRecipients.map(_.ref),
    inputEventMessageRecipients = inputEventMessageRecipients.map(_.ref),
    sensorMessageRecipients = sensorMessageRecipients.map(_.ref),
    singleLoopMessageRecipients = singleLoopMessageRecipients.map(_.ref))

  def createIRoseMessageRouter(vehicleMessageRecipients: Set[ActorRef],
                               alarmEventRecipients: Set[ActorRef],
                               inputEventMessageRecipients: Set[ActorRef],
                               sensorMessageRecipients: Set[ActorRef],
                               singleLoopMessageRecipients: Set[ActorRef]): TestActorRef[IRoseMessageRouter] = {
    TestActorRef(
      new IRoseMessageRouter(
        vehicleMessageRecipients = vehicleMessageRecipients,
        alarmEventRecipients = alarmEventRecipients,
        inputEventMessageRecipients = inputEventMessageRecipients,
        sensorMessageRecipients = sensorMessageRecipients,
        singleLoopMessageRecipients = singleLoopMessageRecipients))
  }

  override def beforeAll() {
    super.beforeAll()
  }

  override def afterAll() {
    system.shutdown()
  }

  "An IRoseMessageRouter actor" when {

    "receiving a VehicleMessage" must {
      "send this VehicleMessage to all vehicleMessageRecipients" in {
        val vehicleMessage = vehicleMessage1
        iRoseMessageRouter ! vehicleMessage
        expectMessage(vehicleMessage, vehicleMessageRecipients)
      }
    }

    "receiving an InputEventMessage with inputId = 8" must {
      "send this InputEventMessage to all alarmEventRecipients" in {
        val inputMessage = alarmInputEventMessage1
        iRoseMessageRouter ! inputMessage
        expectMessage(inputMessage, alarmEventRecipients)
      }
    }

    "receiving an InputEventMessage (inputId <> 8)" must {
      "send this InputEventMessage to all inputEventMessageRecipients" in {
        val inputMessage = inputEventMessage1
        iRoseMessageRouter ! inputMessage
        expectMessage(inputMessage, inputEventMessageRecipients)
      }
    }

    "receiving a SensorMessage" must {
      "send this SensorMessage to all sensorMessageRecipients" in {
        val sensorMessage = sensorMessage1
        iRoseMessageRouter ! sensorMessage
        expectMessage(sensorMessage, sensorMessageRecipients)
      }
    }

    "receiving a SingleLoopMessage" must {
      "send this SingleLoopMessage to all singleLoopMessageRecipients" in {
        val singleLoopMessage = singleLoopMessage1
        iRoseMessageRouter ! singleLoopMessage
        expectMessage(singleLoopMessage, singleLoopMessageRecipients)
      }
    }

    "receiving an invalid message" must {
      "do nothing" in {
        iRoseMessageRouter ! "This is an invalid message"
        // This is probably not the correct way to determine that nothing happens.
        expectNoMessage(vehicleMessageRecipients)
        expectNoMessage(alarmEventRecipients)
        expectNoMessage(inputEventMessageRecipients)
        expectNoMessage(sensorMessageRecipients)
        expectNoMessage(singleLoopMessageRecipients)
      }
    }

    "receiving multiple IRoseMessage's of different types" must {
      "send all these IRoseMessage's to the corresponding recipients" in {
        val vehicleMessageA = vehicleMessage1
        iRoseMessageRouter ! vehicleMessageA
        expectMessage(vehicleMessageA, vehicleMessageRecipients)

        val sensorMessageA = sensorMessage1
        iRoseMessageRouter ! sensorMessageA
        expectMessage(sensorMessageA, sensorMessageRecipients)

        val inputMessageA = inputEventMessage1
        iRoseMessageRouter ! inputMessageA
        expectMessage(inputMessageA, inputEventMessageRecipients)

        val singleLoopMessageA = singleLoopMessage1
        iRoseMessageRouter ! singleLoopMessageA
        expectMessage(singleLoopMessageA, singleLoopMessageRecipients)

        val inputMessageB = inputEventMessage2
        iRoseMessageRouter ! inputMessageB
        expectMessage(inputMessageB, inputEventMessageRecipients)

        val alarmMessageA = alarmInputEventMessage1
        iRoseMessageRouter ! alarmMessageA
        expectMessage(alarmMessageA, alarmEventRecipients)

        val sensorMessageB = sensorMessage2
        iRoseMessageRouter ! sensorMessageB
        expectMessage(sensorMessageB, sensorMessageRecipients)

        val alarmMessageB = alarmInputEventMessage2
        iRoseMessageRouter ! alarmMessageB
        expectMessage(alarmMessageB, alarmEventRecipients)

        val singleLoopMessageB = singleLoopMessage2
        iRoseMessageRouter ! singleLoopMessageB
        expectMessage(singleLoopMessageB, singleLoopMessageRecipients)

        val vehicleMessageB = vehicleMessage2
        iRoseMessageRouter ! vehicleMessageB
        expectMessage(vehicleMessageB, vehicleMessageRecipients)
      }
    }

  }

  def expectMessage(message: IRoseMessage, recipients: Set[TestProbe]) {
    recipients.foreach { recipient ⇒
      recipient.expectMsg(500 millis, message)
    }
  }

  def expectNoMessage(recipients: Set[TestProbe]) {
    recipients.foreach { recipient ⇒
      recipient.expectNoMsg(500 millis)
    }
  }

  def vehicleMessage1: VehicleMessage = {
    VehicleMessage(dateTime = 1360650493123L, detector = 1, direction = Direction.Outgoing,
      speed = 23.0f, length = 50.0f, loop1RiseTime = 0L, loop1FallTime = 450L,
      loop2RiseTime = 400L, loop2FallTime = 875L, yellowTime2 = 0, redTime2 = 0,
      dateTime1 = 1360650493005L, yellowTime1 = 0, redTime1 = 0, dateTime3 = 0L,
      yellowTime3 = 0, redTime3 = 0, valid = true)
  }

  def vehicleMessage2: VehicleMessage = {
    VehicleMessage(dateTime = 1360650493125L, detector = 1, direction = Direction.Outgoing,
      speed = 23.0f, length = 50.0f, loop1RiseTime = 0L, loop1FallTime = 450L,
      loop2RiseTime = 400L, loop2FallTime = 888L, yellowTime2 = 0, redTime2 = 0,
      dateTime1 = 1360650493005L, yellowTime1 = 0, redTime1 = 0, dateTime3 = 0L,
      yellowTime3 = 0, redTime3 = 0, valid = true)
  }

  def alarmInputEventMessage1: InputEventMessage = {
    new InputEventMessage(dateTime = 1360650493123L, inputId = 8, eventNumber = 3, eventText = "something")
  }

  def alarmInputEventMessage2: InputEventMessage = {
    new InputEventMessage(dateTime = 1360650493155L, inputId = 8, eventNumber = 2, eventText = "something else")
  }

  def inputEventMessage1: InputEventMessage = {
    new InputEventMessage(dateTime = 1360650493123L, inputId = 3, eventNumber = 3, eventText = "something")
  }

  def inputEventMessage2: InputEventMessage = {
    new InputEventMessage(dateTime = 1360650493155L, inputId = 3, eventNumber = 1, eventText = "something else")
  }

  def sensorMessage1: SensorMessage = {
    new SensorMessage(dateTime = 1360650493123L, temperature = 10.5f, humidity = 60.0f, acceleration = Accelerometer(x = 3.5f, y = -4.6f, z = 5.7f))
  }

  def sensorMessage2: SensorMessage = {
    new SensorMessage(dateTime = 1360650493155L, temperature = 10.55f, humidity = 60.0f, acceleration = Accelerometer(x = 33.5f, y = -4.6f, z = 5.7f))
  }

  def singleLoopMessage1: SingleLoopMessage = {
    new SingleLoopMessage(dateTime = 1360650493123L, loopId = 2, lastScanCycle = 7, coverTime = 132)
  }

  def singleLoopMessage2: SingleLoopMessage = {
    new SingleLoopMessage(dateTime = 1360650493155L, loopId = 2, lastScanCycle = 17, coverTime = 532)
  }

}
