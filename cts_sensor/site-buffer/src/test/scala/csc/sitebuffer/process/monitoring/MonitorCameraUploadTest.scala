/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.monitoring

import java.io.File

import akka.actor.{ ActorSystem, Props }
import akka.testkit.{ TestKit, TestProbe }
import scala.concurrent.duration._
import csc.sitebuffer.config.CameraConfig
import csc.sitebuffer.messages.{ Direction, VehicleMessage }
import csc.sitebuffer.process.SystemSensorMessage
import csc.sitebuffer.process.images.NewImage
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, WordSpec }

class MonitorCameraUploadTest extends TestKit(ActorSystem("CameraStateMonitorTest"))
  with WordSpecLike with MustMatchers with BeforeAndAfterAll {

  override def afterAll() {
    super.afterAll()
    system.shutdown()
  }

  val startTime = System.currentTimeMillis()

  val config1 = CameraConfig(
    cameraId = "camera1", cameraHost = "10.0.0.1", cameraPort = 1400,
    maxTriggerRetries = 5, timeDisconnectedMs = 1000,
    rebootScript = "site-buffer/src/test/resources/monitoring/camera1Reboot.sh", detectors = Seq(1, 0), imageDir = "A0",
    maxWaitForImage = 2000, maxWaitForReboot = 600000, waitForFixedMsg = 1800000)
  val config2 = CameraConfig(
    cameraId = "camera2", cameraHost = "10.0.0.1", cameraPort = 1400,
    maxTriggerRetries = 5, timeDisconnectedMs = 1000,
    rebootScript = "site-buffer/src/test/resources/monitoring/camera2Reboot.sh", detectors = Seq(2), imageDir = "A1",
    maxWaitForImage = 2000, maxWaitForReboot = 600000, waitForFixedMsg = 1800000)

  "CameraStateMonitor" must {
    "reboot camera when using one camera" in {
      val rebootFile = new File("site-buffer/target/camera1.txt")
      if (rebootFile.exists())
        rebootFile.delete()

      val probeMsg = TestProbe()
      val testActor = system.actorOf(Props(new MonitorCameraUpload(config = Seq(config1), systemSensorMessage = probeMsg.ref)))

      testActor ! new NewImage(imagePath = "/data/images/A0/image1.jpg", timestamp = startTime - 1000)

      testActor ! new VehicleMessage(dateTime = startTime + 100, detector = 0, direction = Direction.Outgoing,
        speed = 40, length = 47, loop1RiseTime = 0, loop1FallTime = 50,
        loop2RiseTime = 100, loop2FallTime = 150, yellowTime2 = 0, redTime2 = 0,
        dateTime1 = startTime, yellowTime1 = 0, redTime1 = 0, dateTime3 = startTime + 200,
        yellowTime3 = 0, redTime3 = 0, valid = true,
        serialNr = "1234", offset1 = 0, offset2 = 0, offset3 = 0)

      //wait for a while
      val start = System.currentTimeMillis()
      while ((start + 4.seconds.toMillis > System.currentTimeMillis()) && !rebootFile.exists()) {
        Thread.sleep(1000)
      }
      rebootFile.exists() must be(true)
      testActor ! new NewImage(imagePath = "/data/images/A0/image1.jpg", timestamp = startTime + 1000)

      val expectedMsg = SystemSensorMessage(
        sensor = SystemSensorMessage.Sensor.Camera,
        timeStamp = startTime + 1000,
        systemId = "",
        event = SystemSensorMessage.EventType.CameraUploadFailed,
        sendingComponent = "Sitebuffer",
        data = Map("downStartTime" -> (startTime - 1000).toString,
          "CameraId" -> "camera1"))

      probeMsg.expectMsg(expectedMsg)

      system.stop(testActor)
    }
    "reboot cameras when using two cameras" in {
      val rebootFile = new File("site-buffer/target/camera1.txt")
      if (rebootFile.exists())
        rebootFile.delete()

      val rebootFile2 = new File("site-buffer/target/camera2.txt")
      if (rebootFile2.exists())
        rebootFile2.delete()

      val probeMsg = TestProbe()
      val testActor = system.actorOf(Props(new MonitorCameraUpload(config = Seq(config1, config2), systemSensorMessage = probeMsg.ref)))

      testActor ! new NewImage(imagePath = "/data/images/A0/image1.jpg", timestamp = startTime - 1000)
      testActor ! new NewImage(imagePath = "/data/images/A1/image1.jpg", timestamp = startTime - 1000)

      testActor ! new VehicleMessage(dateTime = startTime + 100, detector = 0, direction = Direction.Outgoing,
        speed = 40, length = 47, loop1RiseTime = 0, loop1FallTime = 50,
        loop2RiseTime = 100, loop2FallTime = 150, yellowTime2 = 0, redTime2 = 0,
        dateTime1 = startTime, yellowTime1 = 0, redTime1 = 0, dateTime3 = startTime + 200,
        yellowTime3 = 0, redTime3 = 0, valid = true,
        serialNr = "1234", offset1 = 0, offset2 = 0, offset3 = 0)

      //wait for a while
      val start = System.currentTimeMillis()
      while ((start + 5.seconds.toMillis > System.currentTimeMillis()) && !rebootFile.exists()) {
        Thread.sleep(1000)
      }
      rebootFile.exists() must be(true)
      testActor ! new NewImage(imagePath = "/data/images/A0/image1.jpg", timestamp = startTime + 1000)

      val expectedMsg = SystemSensorMessage(
        sensor = SystemSensorMessage.Sensor.Camera,
        timeStamp = startTime + 1000,
        systemId = "",
        event = SystemSensorMessage.EventType.CameraUploadFailed,
        sendingComponent = "Sitebuffer",
        data = Map("downStartTime" -> (startTime - 1000).toString,
          "CameraId" -> "camera1"))

      probeMsg.expectMsg(expectedMsg)

      testActor ! new VehicleMessage(dateTime = startTime + 100, detector = 2, direction = Direction.Outgoing,
        speed = 40, length = 47, loop1RiseTime = 0, loop1FallTime = 50,
        loop2RiseTime = 100, loop2FallTime = 150, yellowTime2 = 0, redTime2 = 0,
        dateTime1 = startTime, yellowTime1 = 0, redTime1 = 0, dateTime3 = startTime + 200,
        yellowTime3 = 0, redTime3 = 0, valid = true,
        serialNr = "1234", offset1 = 0, offset2 = 0, offset3 = 0)

      //wait for a while
      val start2 = System.currentTimeMillis()
      while ((start2 + 4.seconds.toMillis > System.currentTimeMillis()) && !rebootFile2.exists()) {
        Thread.sleep(1000)
      }
      rebootFile2.exists() must be(true)
      testActor ! new NewImage(imagePath = "/data/images/A1/image1.jpg", timestamp = startTime + 1000)

      val expectedMsg2 = SystemSensorMessage(
        sensor = SystemSensorMessage.Sensor.Camera,
        timeStamp = startTime + 1000,
        systemId = "",
        event = SystemSensorMessage.EventType.CameraUploadFailed,
        sendingComponent = "Sitebuffer",
        data = Map("downStartTime" -> (startTime - 1000).toString,
          "CameraId" -> "camera2"))

      probeMsg.expectMsg(expectedMsg2)

      system.stop(testActor)
    }

  }
}