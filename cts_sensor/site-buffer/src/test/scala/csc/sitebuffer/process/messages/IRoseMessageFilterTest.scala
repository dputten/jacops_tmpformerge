/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.messages

import akka.actor.{ ActorRef, ActorSystem }
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import scala.concurrent.duration._
import csc.sitebuffer.messages.{ Accelerometer, _ }
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }

/**
 * Test for IRoseMessageFilter actor
 */
class IRoseMessageFilterTest extends TestKit(ActorSystem("IRoseMessageFilterTest")) with WordSpecLike with MustMatchers
  with BeforeAndAfterAll with BeforeAndAfterEach {

  var datetime1970 = 100000000L
  var datetimeNormal = 31536000001L

  val trueRecipient1 = TestProbe()
  val trueRecipient2 = TestProbe()
  val falseRecipient1 = TestProbe()
  val falseRecipient2 = TestProbe()

  var messageTypeCounter = 0
  def nextMessageTypeCount: Int = {
    messageTypeCounter = (messageTypeCounter + 1) % 4
    messageTypeCounter
  }

  override def beforeAll() {
    super.beforeAll()
  }

  override def afterAll() {
    system.shutdown()
  }

  private def createIRoseMessageFilter(filterCondition: (IRoseMessage) ⇒ Boolean, trueRecipients: Set[ActorRef], falseRecipients: Set[ActorRef]): TestActorRef[IRoseMessageFilter] = {
    TestActorRef(
      new IRoseMessageFilter(filterCondition = filterCondition, trueRecipients = trueRecipients, falseRecipients = falseRecipients))
  }

  "The VehicleMessageFilter, configured with an always true filterCondition" when {

    "receiving 3 VehicleMessages" must {
      "pass all messages on to all trueRecipients and none to the falseRecipients" in {
        val alwaysTrue = (message: IRoseMessage) ⇒ true
        val vehicleMessageFilter = createIRoseMessageFilter(
          filterCondition = alwaysTrue, trueRecipients = Set(trueRecipient1.ref, trueRecipient2.ref), falseRecipients = Set(falseRecipient1.ref, falseRecipient2.ref))

        Set((100, true), (200, true), (300, false), (400, false), (500, true), (600, false)).foreach {
          case (offset, is1970) ⇒
            val message = createNextIRoseMessage(offset = offset, is1970 = is1970)
            vehicleMessageFilter ! message

            expectMessage(Set(trueRecipient1, trueRecipient2), message)
            expectNoMessage(Set(falseRecipient1, falseRecipient2), message)
        }
      }
    }

  }

  "The VehicleMessageFilter, configured with an always false filterCondition" when {

    "receiving 3 VehicleMessages" must {
      "pass all messages on to all falseRecipients and none to the trueRecipients" in {
        val alwaysFalse = (message: IRoseMessage) ⇒ false
        val vehicleMessageFilter = createIRoseMessageFilter(
          filterCondition = alwaysFalse, trueRecipients = Set(trueRecipient1.ref, trueRecipient2.ref), falseRecipients = Set(falseRecipient1.ref, falseRecipient2.ref))

        Set((100, true), (200, true), (300, false), (400, false), (500, true), (600, false)).foreach {
          case (offset, is1970) ⇒
            val message = createNextIRoseMessage(offset = offset, is1970 = is1970)
            vehicleMessageFilter ! message

            expectMessage(Set(falseRecipient1, falseRecipient2), message)
            expectNoMessage(Set(trueRecipient1, trueRecipient2), message)
        }
      }
    }

  }

  "The VehicleMessageFilter, configured with a filterCondition that returns whether message has a 1970 timestamp" when {

    "receiving several VehicleMessages" must {
      "pass all '1970' messages to all trueRecipients and all other messages to the falseRecipients" in {
        val firstDayOf1971 = 31536000000L
        def in1970(epochTime: Long): Boolean = epochTime < firstDayOf1971

        val checkIn1970 = (message: IRoseMessage) ⇒ in1970(message.dateTime)
        val vehicleMessageFilter = createIRoseMessageFilter(
          filterCondition = checkIn1970, trueRecipients = Set(trueRecipient1.ref, trueRecipient2.ref), falseRecipients = Set(falseRecipient1.ref, falseRecipient2.ref))

        Set((100, true), (200, true), (300, false), (400, false), (500, true), (600, false)).foreach {
          case (offset, is1970) ⇒
            val message = createNextIRoseMessage(offset = offset, is1970 = is1970)
            vehicleMessageFilter ! message

            if (is1970) {
              expectMessage(Set(trueRecipient1, trueRecipient2), message)
              expectNoMessage(Set(falseRecipient1, falseRecipient2), message)
            } else {
              expectMessage(Set(falseRecipient1, falseRecipient2), message)
              expectNoMessage(Set(trueRecipient1, trueRecipient2), message)
            }
        }
      }
    }

  }

  private def expectMessage(probes: Set[TestProbe], message: IRoseMessage) {
    probes.foreach { probe ⇒
      probe.expectMsg(500 millis, message)
    }
  }

  private def expectNoMessage(probes: Set[TestProbe], message: IRoseMessage) {
    probes.foreach { probe ⇒
      probe.expectNoMsg(500 millis)
    }
  }

  def createNextIRoseMessage(offset: Int, is1970: Boolean): IRoseMessage = {
    if (is1970) {
      createIRoseMessage(datetime1970 + offset, nextMessageTypeCount)
    } else {
      createIRoseMessage(datetimeNormal + offset, nextMessageTypeCount)
    }
  }

  def createIRoseMessage(datetime: Long, nextMessageType: Int): IRoseMessage = {
    nextMessageType match {
      case 0 ⇒ VehicleMessage(dateTime = datetime, detector = 1, direction = Direction.Outgoing,
        speed = 23.0f, length = 50.0f, loop1RiseTime = 0L, loop1FallTime = 450L,
        loop2RiseTime = 400L, loop2FallTime = 875L, yellowTime2 = 0, redTime2 = 0,
        dateTime1 = 1360650493005L, yellowTime1 = 0, redTime1 = 0, dateTime3 = 0L,
        yellowTime3 = 0, redTime3 = 0, valid = true)
      case 1 ⇒ InputEventMessage(dateTime = datetime, inputId = 8, eventNumber = 3, eventText = "something")
      case 2 ⇒ SensorMessage(dateTime = datetime, temperature = 10.5f, humidity = 60.0f, acceleration = Accelerometer(x = 3.5f, y = -4.6f, z = 5.7f))
      case 3 ⇒ SingleLoopMessage(datetime, 1, 10, 20)
    }
  }

}
