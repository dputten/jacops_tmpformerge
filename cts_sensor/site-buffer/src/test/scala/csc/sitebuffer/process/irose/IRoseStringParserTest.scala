/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.irose

import akka.actor.{ ActorRef, ActorSystem }
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import scala.concurrent.duration._
import csc.akka.logging.DirectLogging
import csc.sitebuffer.messages.{ Accelerometer, _ }
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import org.scalatest._

/**
 * Test van IRoseStringParser actor.
 */
class IRoseStringParserTest extends TestKit(ActorSystem("IRoseStringParserTest")) with WordSpecLike with MustMatchers with DirectLogging with BeforeAndAfterAll {
  override def afterAll() {
    try {
      system.shutdown
    } catch {
      case e: Exception ⇒ log.error("COULD NOT SHUTDOWN ACTOR REGISTRY".format(e))
    }
  }

  val iRoseMessageRecipients = Set(TestProbe(), TestProbe())

  val iRoseStringParserActor = createIRoseStringParser(
    iRoseMessageRecipients = iRoseMessageRecipients.map(_.ref))

  def createIRoseStringParser(iRoseMessageRecipients: Set[ActorRef]): TestActorRef[IRoseStringParser] = {
    TestActorRef(
      new IRoseStringParser(
        transform = IRoseTransformer.transform,
        recipients = iRoseMessageRecipients))
  }

  "An IRoseStringParser actor" when { // TODO: teksten !!!

    "receiving a IRoseString containing a vehicle message string" must {
      "send the corresponding VehicleMessage to all recipients" in {
        val (vehicleMessage, expectedMessage) = vehicleMessageTuple1
        iRoseStringParserActor ! vehicleMessage
        expectMessage(expectedMessage, iRoseMessageRecipients)
      }
    }

    "receiving a IRoseString containing an input message string with inputId = 8" must {
      "send the corresponding InputEventMessage to all recipients" in {
        val (inputMessage, expectedMessage) = alarmInputEventMessageTuple1
        iRoseStringParserActor ! inputMessage
        expectMessage(expectedMessage, iRoseMessageRecipients)
      }
    }

    "receiving a IRoseString containing an input message string (inputId <> 8)" must {
      "send the corresponding InputEventMessage to all recipients" in {
        val (inputMessage, expectedMessage) = inputEventMessageTuple1
        iRoseStringParserActor ! inputMessage
        expectMessage(expectedMessage, iRoseMessageRecipients)
      }
    }

    "receiving a IRoseString containing a sensor message string" must {
      "send the corresponding SensorMessage to all recipients" in {
        val (sensorMessage, expectedMessage) = sensorMessageTuple1
        iRoseStringParserActor ! sensorMessage
        expectMessage(expectedMessage, iRoseMessageRecipients)
      }
    }

    "receiving a IRoseString containing a single loop message string" must {
      "send the corresponding SingleLoopMessage to all recipients" in {
        val (singleLoopMessage, expectedMessage) = singleLoopMessageTuple1
        iRoseStringParserActor ! singleLoopMessage
        expectMessage(expectedMessage, iRoseMessageRecipients)
      }
    }

    "receiving a IRoseString containing an invalid message string" must {
      "do nothing" in {
        iRoseStringParserActor ! createMessageWithCrc("130212;072813,123;this;is;an;invalid;message;")
        // This is probably not the correct way to determine that nothing happens.
        expectNoMessage(iRoseMessageRecipients)
      }
    }

    "receiving multiple IRoseString's containing strings of different types" must {
      "send all corresponding IRoseMessage's to all recipients" in {
        val (vehicleMessage1, expectedVehicleMessage1) = vehicleMessageTuple1
        iRoseStringParserActor ! vehicleMessage1
        expectMessage(expectedVehicleMessage1, iRoseMessageRecipients)

        val (sensorMessage1, expectedSensorMessage1) = sensorMessageTuple1
        iRoseStringParserActor ! sensorMessage1
        expectMessage(expectedSensorMessage1, iRoseMessageRecipients)

        val (inputMessage1, expectedInputMessage1) = inputEventMessageTuple1
        iRoseStringParserActor ! inputMessage1
        expectMessage(expectedInputMessage1, iRoseMessageRecipients)

        val (singleLoopMessage1, expectedSingleLoopMessage1) = singleLoopMessageTuple1
        iRoseStringParserActor ! singleLoopMessage1
        expectMessage(expectedSingleLoopMessage1, iRoseMessageRecipients)

        val (inputMessage2, expectedInputMessage2) = inputEventMessageTuple2
        iRoseStringParserActor ! inputMessage2
        expectMessage(expectedInputMessage2, iRoseMessageRecipients)

        val (alarmMessage1, expectedAlarmMessage1) = alarmInputEventMessageTuple1
        iRoseStringParserActor ! alarmMessage1
        expectMessage(expectedAlarmMessage1, iRoseMessageRecipients)

        val (sensorMessage2, expectedSensorMessage2) = sensorMessageTuple2
        iRoseStringParserActor ! sensorMessage2
        expectMessage(expectedSensorMessage2, iRoseMessageRecipients)

        val (alarmMessage2, expectedAlarmMessage2) = alarmInputEventMessageTuple2
        iRoseStringParserActor ! alarmMessage2
        expectMessage(expectedAlarmMessage2, iRoseMessageRecipients)

        val (singleLoopMessage2, expectedSingleLoopMessage2) = singleLoopMessageTuple2
        iRoseStringParserActor ! singleLoopMessage2
        expectMessage(expectedSingleLoopMessage2, iRoseMessageRecipients)

        val (vehicleMessage2, expectedVehicleMessage2) = vehicleMessageTuple2
        iRoseStringParserActor ! vehicleMessage2
        expectMessage(expectedVehicleMessage2, iRoseMessageRecipients)

        val (vehicleMessage3, expectedVehicleMessage3) = vehicleMessageTuple3
        iRoseStringParserActor ! vehicleMessage3
        expectMessage(expectedVehicleMessage3, iRoseMessageRecipients)

        val (vehicleMessage4, expectedVehicleMessage4) = vehicleMessageTuple4
        iRoseStringParserActor ! vehicleMessage4
        expectMessage(expectedVehicleMessage4, iRoseMessageRecipients)
      }
    }

  }

  def expectMessage(message: IRoseMessage, recipients: Set[TestProbe]) {
    recipients.foreach { recipient ⇒
      recipient.expectMsg(500 millis, message)
    }
  }

  def expectNoMessage(recipients: Set[TestProbe]) {
    recipients.foreach { recipient ⇒
      recipient.expectNoMsg(500 millis)
    }
  }

  def createMessageWithCrc(message: String): String = {
    def calcCrc(message: String): Int = {
      message.foldLeft(0)((accu, ch) ⇒ accu ^ ch)
    }
    message + "%02X".format(calcCrc(message))
  }

  def createIRoseString(string: String): IRoseString = {
    IRoseString(string = string, timestamp = 12345678)
  }

  def vehicleMessageTuple1: (IRoseString, VehicleMessage) = {
    (
      createIRoseString(createMessageWithCrc("130212;072813,123;1;1;023;500;00000;00400;00450;00875;0000,0;0000,0;130212;072813,005;0000,0;0000,0;700101;010000,000;0000,0;0000,0;")),
      VehicleMessage(dateTime = 1360654093123L, detector = 1, direction = Direction.Outgoing,
        speed = 23.0f, length = 50.0f, loop1RiseTime = 0L, loop1FallTime = 450L,
        loop2RiseTime = 400L, loop2FallTime = 875L, yellowTime2 = 0, redTime2 = 0,
        dateTime1 = 1360654093005L, yellowTime1 = 0, redTime1 = 0, dateTime3 = 3600000L,
        yellowTime3 = 0, redTime3 = 0, valid = true))
  }

  def vehicleMessageTuple2: (IRoseString, VehicleMessage) = {
    (
      createIRoseString(createMessageWithCrc("130212;072813,125;1;1;023;500;00000;00400;00450;00888;0000,0;0000,0;130212;072813,005;0000,0;0000,0;700101;010000,000;0000,0;0000,0;0;")),
      VehicleMessage(dateTime = 1360654093125L, detector = 1, direction = Direction.Outgoing,
        speed = 23.0f, length = 50.0f, loop1RiseTime = 0L, loop1FallTime = 450L,
        loop2RiseTime = 400L, loop2FallTime = 888L, yellowTime2 = 0, redTime2 = 0,
        dateTime1 = 1360654093005L, yellowTime1 = 0, redTime1 = 0, dateTime3 = 3600000L,
        yellowTime3 = 0, redTime3 = 0, valid = true))
  }
  def vehicleMessageTuple3: (IRoseString, VehicleMessage) = {
    (
      createIRoseString(createMessageWithCrc("130212;072813,123;1;1;023;500;00000;00400;00450;00875;0000,0;0000,0;130212;072813,005;0000,0;0000,0;700101;010000,000;0000,0;0000,0;0;seri;1;10;22;")),
      VehicleMessage(dateTime = 1360654093123L, detector = 1, direction = Direction.Outgoing,
        speed = 23.0f, length = 50.0f, loop1RiseTime = 0L, loop1FallTime = 450L,
        loop2RiseTime = 400L, loop2FallTime = 875L, yellowTime2 = 0, redTime2 = 0,
        dateTime1 = 1360654093005L, yellowTime1 = 0, redTime1 = 0, dateTime3 = 3600000L,
        yellowTime3 = 0, redTime3 = 0, valid = true,
        serialNr = "seri", offset1 = 1, offset2 = 10, offset3 = 22))
  }
  def vehicleMessageTuple4: (IRoseString, VehicleMessage) = {
    (
      createIRoseString(createMessageWithCrc("130212;072813,123;1;1;023;500;00000;00400;00450;00875;0000,0;0000,0;130212;072813,005;0000,0;0000,0;700101;010000,000;0000,0;0000,0;0;seri;1;10;22;0000017;")),
      VehicleMessage(dateTime = 1360654093123L, detector = 1, direction = Direction.Outgoing,
        speed = 23.0f, length = 50.0f, loop1RiseTime = 0L, loop1FallTime = 450L,
        loop2RiseTime = 400L, loop2FallTime = 875L, yellowTime2 = 0, redTime2 = 0,
        dateTime1 = 1360654093005L, yellowTime1 = 0, redTime1 = 0, dateTime3 = 3600000L,
        yellowTime3 = 0, redTime3 = 0, valid = true,
        serialNr = "seri", offset1 = 1, offset2 = 10, offset3 = 22))
  }

  def alarmInputEventMessageTuple1: (IRoseString, InputEventMessage) = {
    (
      createIRoseString(createMessageWithCrc("130212;072813,123;2;8;3;something;")),
      new InputEventMessage(dateTime = 1360654093123L, inputId = 8, eventNumber = 3, eventText = "something"))
  }

  def alarmInputEventMessageTuple2: (IRoseString, InputEventMessage) = {
    (
      createIRoseString(createMessageWithCrc("130212;072813,155;2;8;2;something else;")),
      new InputEventMessage(dateTime = 1360654093155L, inputId = 8, eventNumber = 2, eventText = "something else"))
  }

  def inputEventMessageTuple1: (IRoseString, InputEventMessage) = {
    (
      createIRoseString(createMessageWithCrc("130212;072813,123;2;3;3;something;")),
      new InputEventMessage(dateTime = 1360654093123L, inputId = 3, eventNumber = 3, eventText = "something"))
  }

  def inputEventMessageTuple2: (IRoseString, InputEventMessage) = {
    (
      createIRoseString(createMessageWithCrc("130212;072813,155;2;3;1;something else;")),
      new InputEventMessage(dateTime = 1360654093155L, inputId = 3, eventNumber = 1, eventText = "something else"))
  }

  def sensorMessageTuple1: (IRoseString, SensorMessage) = {
    (
      createIRoseString(createMessageWithCrc("130212;072813,123;0;010,500;060,000;0003,500;-004,600;005,700;")),
      new SensorMessage(dateTime = 1360654093123L, temperature = 10.5f, humidity = 60.0f, acceleration = Accelerometer(x = 3.5f, y = -4.6f, z = 5.7f)))
  }

  def sensorMessageTuple2: (IRoseString, SensorMessage) = {
    (
      createIRoseString(createMessageWithCrc("130212;072813,155;0;010,550;060,000;0033,500;-004,600;005,700;")),
      new SensorMessage(dateTime = 1360654093155L, temperature = 10.55f, humidity = 60.0f, acceleration = Accelerometer(x = 33.5f, y = -4.6f, z = 5.7f)))
  }

  def singleLoopMessageTuple1: (IRoseString, SingleLoopMessage) = {
    (
      createIRoseString(createMessageWithCrc("130212;072813,123;4;2;000;000;00000;00007;00132;00000;")),
      new SingleLoopMessage(dateTime = 1360654093123L, loopId = 2, lastScanCycle = 7, coverTime = 132))
  }

  def singleLoopMessageTuple2: (IRoseString, SingleLoopMessage) = {
    (
      createIRoseString(createMessageWithCrc("130212;072813,155;4;2;000;000;00000;00017;00532;00000;")),
      new SingleLoopMessage(dateTime = 1360654093155L, loopId = 2, lastScanCycle = 17, coverTime = 532))
  }

}
