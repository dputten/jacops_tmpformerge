/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.recording

import java.io.File

import akka.actor._
import akka.testkit.{ TestActorRef, TestKit }
import csc.sitebuffer.FileTestUtils
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }
import testframe.Resources

/**
 * Test of the IRoseStringRecorder actor.
 */
class IRoseStringRecorderTest extends TestKit(ActorSystem("IRoseStringRecorderTest")) with WordSpecLike with MustMatchers
  with BeforeAndAfterEach with BeforeAndAfterAll {
  import FileTestUtils._
  import RecordingTestUtils._

  var testRecorder: TestActorRef[IRoseStringRecorder] = _

  var testCameHere = false

  val resourceDir = new File(Resources.getResourceDirPath().getAbsolutePath)
  val testBaseDir = new File(resourceDir, "out/iRoseStringRecorder")
  if (!testBaseDir.exists()) {
    testBaseDir.mkdirs()
  } else {
    clearDirectory(testBaseDir)
  }

  override def beforeAll() {
    super.beforeAll()
  }

  override def afterAll() {
    system.shutdown()

    clearDirectory(testBaseDir)
    testBaseDir.delete()
  }

  override def afterEach() {
    clearDirectory(testBaseDir)
  }

  def createTestActor(directory: File): TestActorRef[IRoseStringRecorder] = {
    TestActorRef(new IRoseStringRecorder with IRoseStringToDirectoryWriterCreator {
      def getBaseDirectory: File = {
        directory
      }
    })
  }

  def createTestActorWithInitException(directory: File): TestActorRef[IRoseStringRecorder] = {
    TestActorRef(new IRoseStringRecorder with IRoseStringToDirectoryWriterWithInitExceptionCreator)
  }

  def sendStartRecordingInput(recordingId: String): Unit = {
    system.eventStream.publish(StartRecordingInput(recordingId))
  }

  def sendStopRecordingInput(): Unit = {
    system.eventStream.publish(StopRecordingInput)
  }

  "An IRoseStringRecorder actor" when {
    "receiving an IRoseString without having received a StartRecordingInput event first" must {
      "do nothing" in {
        // Make sure we start from scratch
        testBaseDir.listFiles().length must be(0)
        // Create actor
        testRecorder = createTestActor(testBaseDir)
        // Send test message
        testRecorder ! createIRoseString(message = vehicleMessage1, timestamp = 12345678L)
        // Check expected results
        testBaseDir.listFiles().length must be(0)
      }
    }

    "receiving a StartRecordingInput event whilst waitingForRecording" must {
      "start recording all incoming IRoseString's until a StopRecordingInput is received" in {
        // Make sure we start from scratch
        testBaseDir.listFiles().length must be(0)
        // Create actor
        testRecorder = createTestActor(testBaseDir)
        // Publish StartRecordingInput event
        val recordingId1 = "recording-1"
        val recording1Dir = new File(testBaseDir, "%s/messages".format(recordingId1))
        recording1Dir.exists() must be(false)
        sendStartRecordingInput(recordingId1)
        Thread.sleep(50)
        recording1Dir.exists() must be(true)
        recording1Dir.listFiles().length must be(0)
        // Send 3 test messages
        testRecorder ! createIRoseString(message = vehicleMessage1, timestamp = 1234567800L)
        testRecorder ! createIRoseString(message = alarmInputEventMessage1, timestamp = 1234567801L)
        testRecorder ! createIRoseString(message = sensorMessage1, timestamp = 1234567802L)
        Thread.sleep(100)
        // Check expected results
        info("IRoseString's are recorded to a directory (name = recordingId from StartRecordingInput event)")
        recording1Dir.listFiles().length must be(3)
        // Publish StopRecordingInput event
        sendStopRecordingInput()
        // Send 3 test messages
        testRecorder ! createIRoseString(message = vehicleMessage2, timestamp = 1234567820L)
        testRecorder ! createIRoseString(message = alarmInputEventMessage2, timestamp = 1234567821L)
        testRecorder ! createIRoseString(message = sensorMessage2, timestamp = 1234567822L)
        Thread.sleep(100)
        // Check expected results
        info("IRoseString's received after StopRecordingInput event are not recorded")
        info("So a StopRecordingInput event ends a recording session")
        recording1Dir.listFiles().length must be(3)
      }
    }

    "receiving a StartRecordingInput event whilst recording" must {
      "continue recording incoming IRoseString's to the directory of existing recording" in {
        // Make sure we start from scratch
        testBaseDir.listFiles().length must be(0)
        // Create actor
        testRecorder = createTestActor(testBaseDir)
        // Publish StartRecordingInput event
        val recordingId1 = "recording-1"
        val recording1Dir = new File(testBaseDir, "%s/messages".format(recordingId1))
        recording1Dir.exists() must be(false)
        sendStartRecordingInput(recordingId1)
        Thread.sleep(50)
        recording1Dir.exists() must be(true)
        recording1Dir.listFiles().length must be(0)
        // Send 3 test messages
        testRecorder ! createIRoseString(message = vehicleMessage1, timestamp = 1234567800L)
        testRecorder ! createIRoseString(message = alarmInputEventMessage1, timestamp = 1234567801L)
        testRecorder ! createIRoseString(message = sensorMessage1, timestamp = 1234567802L)
        Thread.sleep(100)
        // Check expected results
        recording1Dir.listFiles().length must be(3)
        // Publish another StartRecordingInput event
        val recordingId2 = "recording-2"
        val recording2Dir = new File(testBaseDir, "%s/messages".format(recordingId2))
        recording2Dir.exists() must be(false)
        sendStartRecordingInput(recordingId2)
        // Send 3 test messages
        testRecorder ! createIRoseString(message = vehicleMessage2, timestamp = 1234567820L)
        testRecorder ! createIRoseString(message = alarmInputEventMessage2, timestamp = 1234567821L)
        testRecorder ! createIRoseString(message = sensorMessage2, timestamp = 1234567822L)
        Thread.sleep(100)
        // Check expected results
        info("So the second StartRecordingInput event is ignored")
        info("So current recording must be stopped before next recording can start")
        recording1Dir.listFiles().length must be(6)
        recording2Dir.exists() must be(false)
      }
    }

    "receiving multiple StartRecordingInput/StopRecordingInput events with different recordingId's" must {
      "record incoming IRoseString's that fall within a recording session" in {
        // Make sure we start from scratch
        testBaseDir.listFiles().length must be(0)
        // Create actor
        testRecorder = createTestActor(testBaseDir)
        // Publish StartRecordingInput event
        val recordingId1 = "recording-1"
        val recording1Dir = new File(testBaseDir, "%s/messages".format(recordingId1))
        recording1Dir.exists() must be(false)
        sendStartRecordingInput(recordingId1)
        Thread.sleep(50)
        recording1Dir.exists() must be(true)
        recording1Dir.listFiles().length must be(0)
        // Send 3 test messages
        testRecorder ! createIRoseString(message = vehicleMessage1, timestamp = 1234567800L)
        testRecorder ! createIRoseString(message = alarmInputEventMessage1, timestamp = 1234567801L)
        testRecorder ! createIRoseString(message = sensorMessage1, timestamp = 1234567802L)
        Thread.sleep(100)
        // Check expected results
        recording1Dir.listFiles().length must be(3)
        // Publish StopRecordingInput event
        sendStopRecordingInput()
        // Send 3 test messages
        testRecorder ! createIRoseString(message = vehicleMessage2, timestamp = 1234567820L)
        testRecorder ! createIRoseString(message = alarmInputEventMessage2, timestamp = 1234567821L)
        testRecorder ! createIRoseString(message = sensorMessage2, timestamp = 1234567822L)
        Thread.sleep(100)
        recording1Dir.listFiles().length must be(3)

        // Publish another StartRecordingInput event
        val recordingId2 = "recording-2"
        val recording2Dir = new File(testBaseDir, "%s/messages".format(recordingId2))
        recording2Dir.exists() must be(false)
        sendStartRecordingInput(recordingId2)
        Thread.sleep(50)
        recording2Dir.exists() must be(true)
        recording2Dir.listFiles().length must be(0)
        // Send 3 test messages
        testRecorder ! createIRoseString(message = vehicleMessage2, timestamp = 1234567840L)
        testRecorder ! createIRoseString(message = alarmInputEventMessage2, timestamp = 1234567841L)
        testRecorder ! createIRoseString(message = sensorMessage2, timestamp = 1234567842L)
        Thread.sleep(100)
        // Check expected results
        info("So there is one directory per recordingId")
        recording1Dir.listFiles().length must be(3)
        recording2Dir.listFiles().length must be(3)
      }
    }

    "receiving multiple StartRecordingInput/StopRecordingInput events with the same recordingId's" must {
      "record incoming IRoseString's that fall within a recording session" in {
        // Make sure we start from scratch
        testBaseDir.listFiles().length must be(0)
        // Create actor
        testRecorder = createTestActor(testBaseDir)
        // Publish StartRecordingInput event
        val recordingId1 = "recording-1"
        val recording1Dir = new File(testBaseDir, "%s/messages".format(recordingId1))
        recording1Dir.exists() must be(false)
        sendStartRecordingInput(recordingId1)
        Thread.sleep(50)
        recording1Dir.exists() must be(true)
        recording1Dir.listFiles().length must be(0)
        // Send 3 test messages
        testRecorder ! createIRoseString(message = vehicleMessage1, timestamp = 1234567800L)
        testRecorder ! createIRoseString(message = alarmInputEventMessage1, timestamp = 1234567801L)
        testRecorder ! createIRoseString(message = sensorMessage1, timestamp = 1234567802L)
        Thread.sleep(100)
        // Check expected results
        recording1Dir.listFiles().length must be(3)
        // Publish StopRecordingInput event
        sendStopRecordingInput()
        // Send 3 test messages
        testRecorder ! createIRoseString(message = vehicleMessage2, timestamp = 1234567820L)
        testRecorder ! createIRoseString(message = alarmInputEventMessage2, timestamp = 1234567821L)
        testRecorder ! createIRoseString(message = sensorMessage2, timestamp = 1234567822L)
        Thread.sleep(100)
        recording1Dir.listFiles().length must be(3)

        // Publish another StartRecordingInput event
        sendStartRecordingInput(recordingId1)
        Thread.sleep(50)
        // Send 3 test messages
        testRecorder ! createIRoseString(message = vehicleMessage2, timestamp = 1234567840L)
        testRecorder ! createIRoseString(message = alarmInputEventMessage2, timestamp = 1234567841L)
        testRecorder ! createIRoseString(message = sensorMessage2, timestamp = 1234567842L)
        Thread.sleep(100)
        // Check expected results
        info("So multiple recording sessions with the same recordingId are possible (all written to one directory)")
        recording1Dir.listFiles().length must be(6)
      }
    }

    "creation of the writer for a recording session fails" must {
      "stay in waitingForRecording" in {
        // Make sure we start from scratch
        testBaseDir.listFiles().length must be(0)
        testCameHere = false
        // Create actor
        testRecorder = createTestActorWithInitException(testBaseDir)
        // Publish StartRecordingInput event
        val recordingId1 = "recording-1"
        sendStartRecordingInput(recordingId1)
        Thread.sleep(50)
        // Send 3 test messages
        testRecorder ! createIRoseString(message = vehicleMessage1, timestamp = 1234567800L)
        testRecorder ! createIRoseString(message = alarmInputEventMessage1, timestamp = 1234567801L)
        testRecorder ! createIRoseString(message = sensorMessage1, timestamp = 1234567802L)
        Thread.sleep(100)
        // Check expected results
        testCameHere must be(false)
        testRecorder.underlyingActor.writer must be(None)
      }
    }

  }

  trait IRoseStringToDirectoryWriterWithInitExceptionCreator extends IRoseStringWriterCreator {

    def createWriter(recordingId: String, actorRefFactory: ActorRefFactory): ActorRef = {

      actorRefFactory.actorOf(Props(
        new IRoseStringToDirectoryWriterWithInitException()),
        "IRoseStringToDirectoryWriterWithInitException")
    }

    def recordingDirectoryName(recordingId: String): String = {
      recordingId.trim
    }

  }

  class IRoseStringToDirectoryWriterWithInitException() extends Actor {

    throw new Exception("InitException")

    def receive = {
      case _ ⇒ testCameHere = true
    }
  }

}
