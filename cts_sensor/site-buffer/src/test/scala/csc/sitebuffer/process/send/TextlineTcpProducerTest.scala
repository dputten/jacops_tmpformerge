/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.send

import akka.actor.Status.Failure
import akka.actor.{ Actor, ActorRef, ActorSystem, Props }
import akka.camel.{ Ack, CamelExtension, CamelMessage, Consumer }
import akka.pattern.ask
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import akka.util.Timeout
import csc.akka.CamelClient

import scala.concurrent.Await
import scala.concurrent.duration._
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }

import scala.util.Success

/**
 * Test van TextlineTcpProducer
 */
class TextlineTcpProducerTest
  extends TestKit(ActorSystem("TextlineTcpProducerTest"))
  with WordSpecLike
  with MustMatchers
  with CamelClient
  with BeforeAndAfterEach
  with BeforeAndAfterAll {

  val testReceiverOfValidMessage: TestProbe = TestProbe()
  val testConsumerRef = system.actorOf(Props(new TextlineTcpProducerTestConsumer(testReceiverOfValidMessage.ref)))

  var systemSensorMessageProducerRef: TestActorRef[TextlineTcpProducer] = _

  override def beforeAll() {
    super.beforeAll()
    val camel = CamelExtension(system)
    val future = camel.activationFutureFor(testConsumerRef)(
      timeout = 10 seconds,
      executor = system.dispatcher)
    Await.ready(future, 10.seconds)
  }

  override def afterAll() {
    system.stop(testConsumerRef)
    awaitDeactivation(testConsumerRef, 10 seconds)
    system.shutdown()
  }

  override def afterEach() {
    systemSensorMessageProducerRef.stop()
  }

  "A TextlineTcpProducer actor" when {

    "it has been created" must {
      "contain the correct endpointUri" in {
        systemSensorMessageProducerRef = TestActorRef(new TextlineTcpProducer(host = "localhost", port = 6200))
        systemSensorMessageProducerRef.underlyingActor.endpointUri must be("mina:tcp://localhost:6200?textline=true&sync=true")
      }
    }

    "receiving a valid message" must {
      "send this to the Consumer and (after receiving an Ack reply) respond with Success" in {
        systemSensorMessageProducerRef = TestActorRef(new TextlineTcpProducer(host = "localhost", port = 6200))
        // send message
        implicit val timeout = Timeout(20000 millis)
        val reply = systemSensorMessageProducerRef ? "{This is a valid message}"
        // test for expected result
        Await.ready(reply, 10.seconds)
        reply.value.get must be(Success(InOutTextSender.Success))
        testReceiverOfValidMessage.expectMsg(1000 millis, "This is a valid message")
      }
    }

    "receiving an invalid message" must {
      "send this to the Consumer and (after receiving a Failure reply) respond with Failure" in {
        systemSensorMessageProducerRef = TestActorRef(new TextlineTcpProducer(host = "localhost", port = 6200))
        // send message
        implicit val timeout = Timeout(2000 millis)
        val reply = systemSensorMessageProducerRef ? "<This is an invalid message>"
        // test for expected result
        Await.ready(reply, 10.seconds)
        reply.value.get must be(Success(InOutTextSender.Failure))
        testReceiverOfValidMessage.expectNoMsg(1000 millis)
      }
    }

    "receiving a valid message and no corresponding Consumer present" must {
      "respond with Failure" in {
        systemSensorMessageProducerRef = TestActorRef(new TextlineTcpProducer(host = "localhost", port = 6201))
        // send message
        implicit val timeout = Timeout(5000 millis)
        val reply = systemSensorMessageProducerRef ? "{This is a valid message}"
        // test for expected result
        Await.ready(reply, 10.seconds)
        reply.value.get must be(Success(InOutTextSender.Failure))
      }
    }
  }

}

// This Consumer simulates the behavior of the real Consumer, that will run on the server.
//    - Ack to sender when valid message received.
//    - Failure to sender otherwise
class TextlineTcpProducerTestConsumer(validMessageReceiver: ActorRef) extends Actor with Consumer {
  def endpointUri = "mina:tcp://localhost:6200?textline=true&sync=true"

  val ValidMessage = """^\{(.*)\}$""".r // = Any string that begins with { and ends with }

  def receive = {
    case message: CamelMessage ⇒
      message.bodyAs[String] match {
        case ValidMessage(content) ⇒
          sender ! Ack
          validMessageReceiver ! content
        case _ ⇒
          throw new Exception("CamelMessage with unknown body")
      }
  }

  override def preRestart(reason: Throwable, message: Option[Any]) {
    sender ! Failure(reason)
  }
}

