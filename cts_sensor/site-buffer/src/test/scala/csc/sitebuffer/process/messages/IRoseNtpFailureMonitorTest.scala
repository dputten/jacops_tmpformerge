/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.messages

import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import scala.concurrent.duration._
import csc.sitebuffer.messages._
import csc.sitebuffer.process.SystemSensorMessage
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }

/**
 * Test van IRoseNtpFailureMonitor
 */
class IRoseNtpFailureMonitorTest extends TestKit(ActorSystem("IRoseNtpFailureMonitorTest")) with WordSpecLike with MustMatchers
  with BeforeAndAfterEach with BeforeAndAfterAll {

  var datetime1970 = 100000000L
  var datetimeNormal = 31536000001L

  var mockCurrentTime: Long = _
  val dummyCurrentTime = () ⇒ mockCurrentTime

  var testProbe: TestProbe = _
  var iRoseLoopFailureActor: TestActorRef[IRoseNtpFailureMonitor] = _

  var messageTypeCounter = 0
  def nextMessageTypeCount: Int = {
    messageTypeCounter = (messageTypeCounter + 1) % 4
    messageTypeCounter
  }

  override def beforeAll() {
    super.beforeAll()
  }

  override def afterAll() {
    system.shutdown()
  }

  override def beforeEach() {
    testProbe = TestProbe()
    iRoseLoopFailureActor = TestActorRef(
      new IRoseNtpFailureMonitor(maxAllowedMessages = 3, testWindowMs = 100, getCurrentTime = dummyCurrentTime, systemSensorMessageSender = testProbe.ref))
  }

  def send1970Message(currentTime: Long) {
    mockCurrentTime = currentTime
    iRoseLoopFailureActor ! createNext1970IRoseMessage
  }

  def sendNormalMessage(currentTime: Long) {
    mockCurrentTime = currentTime
    iRoseLoopFailureActor ! createNextNormalIRoseMessage
  }

  "The IRoseNtpFailureMonitor (maxAllowedMessages = 3, sampleDurationMs = 100)" when {

    "receiving 3 '1970' IRoseMessages in exactly 99 seconds" must {
      "do nothing" in {
        // send message
        send1970Message(currentTime = 1)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        send1970Message(currentTime = 50)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        send1970Message(currentTime = 99)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
      }
    }

    "receiving 3 '1970' IRoseMessages in exactly 100 seconds" must {
      "do nothing" in {
        // send message
        send1970Message(currentTime = 1)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        send1970Message(currentTime = 50)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        send1970Message(currentTime = 100)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
      }
    }

    "receiving 3 '1970' IRoseMessages in exactly 101 seconds" must {
      "do nothing" in {
        // send message
        send1970Message(currentTime = 1)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        send1970Message(currentTime = 50)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        send1970Message(currentTime = 101)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
      }
    }

    "receiving 4 '1970' IRoseMessages in exactly 99 seconds" must {
      "send a SystemSensorMessage(Ntp, SensorFailure) to the systemSensorMessageSender" in {
        val expectedMsg = SystemSensorMessage(
          sensor = "Ntp",
          timeStamp = 99,
          systemId = "",
          event = "SensorFailure",
          sendingComponent = "Sitebuffer",
          data = Map())
        // send message
        send1970Message(currentTime = 1)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        send1970Message(currentTime = 50)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        send1970Message(currentTime = 70)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        send1970Message(currentTime = 99)
        // test for expected result
        testProbe.expectMsg(500 millis, expectedMsg)
      }
    }

    "receiving 4 '1970' IRoseMessages and 4 normal IRoseMessages in exactly 99 seconds" must {
      "send one SystemSensorMessage(Ntp, SensorFailure) to the systemSensorMessageSender" in {
        val expectedMsg = SystemSensorMessage(
          sensor = "Ntp",
          timeStamp = 99,
          systemId = "",
          event = "SensorFailure",
          sendingComponent = "Sitebuffer",
          data = Map())
        // send message
        send1970Message(currentTime = 1)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendNormalMessage(currentTime = 20)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendNormalMessage(currentTime = 30)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        send1970Message(currentTime = 50)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendNormalMessage(currentTime = 60)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        send1970Message(currentTime = 70)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendNormalMessage(currentTime = 80)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        send1970Message(currentTime = 99)
        // test for expected result
        testProbe.expectMsg(500 millis, expectedMsg)
      }
    }

    "receiving 4 '1970' IRoseMessages in exactly 100 seconds" must {
      "send a SystemSensorMessage(Ntp, SensorFailure) to the systemSensorMessageSender" in {
        val expectedMsg = SystemSensorMessage(
          sensor = "Ntp",
          timeStamp = 100,
          systemId = "",
          event = "SensorFailure",
          sendingComponent = "Sitebuffer",
          data = Map())
        // send message
        send1970Message(currentTime = 1)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        send1970Message(currentTime = 50)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        send1970Message(currentTime = 70)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        send1970Message(currentTime = 100)
        // test for expected result
        testProbe.expectMsg(500 millis, expectedMsg)
      }
    }

    "receiving 4 '1970' IRoseMessages in exactly 101 seconds" must {
      "do nothing" in {
        // send message
        send1970Message(currentTime = 1)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        send1970Message(currentTime = 50)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        send1970Message(currentTime = 70)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        send1970Message(currentTime = 101)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
      }
    }

    "receiving 7 '1970' IRoseMessages in exactly 100 seconds" must {
      "send 1 SystemSensorMessage(Ntp, SensorFailure) to the systemSensorMessageSender" in {
        val expectedMsg = SystemSensorMessage(
          sensor = "Ntp",
          timeStamp = 40,
          systemId = "",
          event = "SensorFailure",
          sendingComponent = "Sitebuffer",
          data = Map())
        // send message
        send1970Message(currentTime = 1)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        send1970Message(currentTime = 20)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        send1970Message(currentTime = 30)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendNormalMessage(currentTime = 35)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        send1970Message(currentTime = 40)
        // test for expected result
        testProbe.expectMsg(500 millis, expectedMsg)
        // send message
        send1970Message(currentTime = 60)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        send1970Message(currentTime = 80)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendNormalMessage(currentTime = 85)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        send1970Message(currentTime = 100)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
      }
    }

    "receiving 8 '1970' IRoseMessages in exactly 100 seconds" must {
      "send 2 SystemSensorMessage(Ntp, SensorFailure) to the systemSensorMessageSender" in {
        val expectedMsg1 = SystemSensorMessage(
          sensor = "Ntp",
          timeStamp = 40,
          systemId = "",
          event = "SensorFailure",
          sendingComponent = "Sitebuffer",
          data = Map())
        val expectedMsg2 = SystemSensorMessage(
          sensor = "Ntp",
          timeStamp = 100,
          systemId = "",
          event = "SensorFailure",
          sendingComponent = "Sitebuffer",
          data = Map())
        // send message
        send1970Message(currentTime = 1)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        send1970Message(currentTime = 20)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendNormalMessage(currentTime = 23)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendNormalMessage(currentTime = 27)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        send1970Message(currentTime = 30)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        send1970Message(currentTime = 40)
        // test for expected result
        testProbe.expectMsg(500 millis, expectedMsg1)
        // send message
        send1970Message(currentTime = 60)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        send1970Message(currentTime = 80)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        sendNormalMessage(currentTime = 85)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        send1970Message(currentTime = 90)
        // test for expected result
        testProbe.expectNoMsg(500 millis)
        // send message
        send1970Message(currentTime = 100)
        // test for expected result
        testProbe.expectMsg(500 millis, expectedMsg2)
      }
    }

  }

  def createNext1970IRoseMessage: IRoseMessage = {
    def nextDatetime: Long = {
      datetime1970 += 10
      datetime1970
    }
    createIRoseMessage(nextDatetime, nextMessageTypeCount)
  }

  def createNextNormalIRoseMessage: IRoseMessage = {
    def nextDatetime: Long = {
      datetimeNormal += 10
      datetimeNormal
    }
    createIRoseMessage(nextDatetime, nextMessageTypeCount)
  }

  def createIRoseMessage(datetime: Long, nextMessageType: Int): IRoseMessage = {
    nextMessageType match {
      case 0 ⇒ VehicleMessage(dateTime = datetime, detector = 1, direction = Direction.Outgoing,
        speed = 23.0f, length = 50.0f, loop1RiseTime = 0L, loop1FallTime = 450L,
        loop2RiseTime = 400L, loop2FallTime = 875L, yellowTime2 = 0, redTime2 = 0,
        dateTime1 = 1360650493005L, yellowTime1 = 0, redTime1 = 0, dateTime3 = 0L,
        yellowTime3 = 0, redTime3 = 0, valid = true)
      case 1 ⇒ InputEventMessage(dateTime = datetime, inputId = 8, eventNumber = 3, eventText = "something")
      case 2 ⇒ SensorMessage(dateTime = datetime, temperature = 10.5f, humidity = 60.0f, acceleration = Accelerometer(x = 3.5f, y = -4.6f, z = 5.7f))
      case 3 ⇒ SingleLoopMessage(datetime, 1, 10, 20)
    }
  }

}
