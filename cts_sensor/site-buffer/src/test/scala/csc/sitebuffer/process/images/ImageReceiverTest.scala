/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.images

import akka.actor.{ Actor, ActorSystem }
import akka.camel.{ CamelExtension, Producer }
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import csc.akka.CamelClient

import scala.concurrent.duration._
import org.scalatest._

/**
 * Test van ImageReceiver
 */
class ImageReceiverTest
  extends TestKit(ActorSystem("ImageReceiverTest"))
  with WordSpecLike
  with MustMatchers
  with CamelClient
  with BeforeAndAfterAll {

  var currentTime: Long = _
  val dummyCurrentTime = () ⇒ currentTime

  val imagePathProducer = TestActorRef(new TestImagePathProducer())

  val testProbe: TestProbe = TestProbe()
  var imageReceiver: TestActorRef[ImageReceiver] = _

  override def beforeAll() {
    super.beforeAll()
  }

  override def afterAll() {
    imagePathProducer.stop()
    system.shutdown()
  }

  "An ImageReceiver actor" when {
    // Create test actor
    imageReceiver = TestActorRef(new ImageReceiver(
      imagesEndpointHost = "localhost",
      imagesEndpointPort = 12321,
      getCurrentTime = dummyCurrentTime,
      recipients = Set(testProbe.ref)))

    awaitActivation(imagePathProducer, 1.second)

    "receiving an image path (over TCP)" must {
      "send a NewImage (containing that image path) to recipients" in {
        currentTime = 12345678L
        // Send test message
        imagePathProducer ! "/share/dir1/dir2/dir3"
        // Check expected results
        testProbe.expectMsg(500 millis, NewImage("/share/dir1/dir2/dir3", 12345678L))
      }
    }

  }

}

class TestImagePathProducer extends Actor with Producer {
  def endpointUri = "mina:tcp://localhost:12321?textline=true&sync=false"
}