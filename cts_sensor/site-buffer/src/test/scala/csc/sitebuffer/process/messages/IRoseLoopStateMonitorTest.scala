package csc.sitebuffer.process.messages

import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import csc.akka.process.{ ExecuteScript, ExecuteScriptResult }
import csc.sitebuffer.process.SystemSensorMessage
import csc.sitebuffer.process.SystemSensorMessage.{ EventType, Sensor }
import csc.sitebuffer.process.irose.{ IRoseCusStatParser, IRoseLoopError }
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }

import scala.collection.immutable

/**
 * Created by jeijlders on 5/12/14.
 */
class IRoseLoopStateMonitorTest extends TestKit(ActorSystem("IRoseLoopStateMonitorTest"))
  with WordSpecLike with MustMatchers with BeforeAndAfterAll with BeforeAndAfterEach {

  var executeScriptActor: TestProbe = TestProbe()
  var systemSensorMessageSender: TestProbe = TestProbe()
  var mockCurrentTime: Long = _
  val dummyCurrentTime = () ⇒ mockCurrentTime

  override def afterAll() {
    super.afterAll()
    system.stop(testActor)
    system.shutdown()
  }

  "IRoseLoopStateMonitor " when {
    "receiving tick " must {
      "send ExecuteScript to executeScriptActor" in {
        println("---------------------------START TEST(send ExecuteScript to executeScriptActor)")
        val iRoseLoopStateMonitor = TestActorRef(new IRoseLoopStateMonitor(new IRoseCusStatParser(), executeScriptActor.ref, systemSensorMessageSender.ref, "site-buffer/src/test/resources/monitoring/cusStatusIrose.sh", dummyCurrentTime))
        iRoseLoopStateMonitor ! "tick"

        executeScriptActor.expectMsg(new ExecuteScript("site-buffer/src/test/resources/monitoring/cusStatusIrose.sh", Seq()))
      }
    }

    "receiving ExecuteScriptResult and no previous results" must {
      "do nothing" in {
        println("---------------------------START TEST(receiving ExecuteScriptResult and no previous results)")
        val str = "loop 0 Last value: 1578703 Frequency: 79179Hz in/out/retry: 126/0/0 base: 1578691 1577152 1577918 1578502 A:1579149, B:1578244 dev: 0,057% Debug: 1 0 0 0 0 0 loop 1 Last value: 1583517 Frequency: 78939Hz in/out/retry: 126/0/0 base: 1583499 1581956 1582724 1583309 A:1584018, B:1582981 dev: 0,065% Debug: 1 0 0 0 0 0 loop 2 Last value: 1597160 Frequency: 78262Hz in/out/retry: 232/0/0 base: 1597193 1595636 1596411 1597002 A:1597704, B:1596696 dev: 0,063% Debug: 0 0 0 0 0 0 loop 3 Last value: 1570258 Frequency: 79605Hz in/out/retry: 232/0/0 base: 1570251 1568721 1569482 1570063 A:1570949, B:1569554 dev: 0,088%  Debug: 0 0 0 0 0 0  loop 4: error (state = 5, lastvalue = 0, wait 0, nSubState 0  Last value: 0 Frequency: 0Hz in/out/retry: 0/0/1  base: 0 0 0 0 A: 0, B: 0 dev: 0,000%  loop 5: error (state = 5, lastvalue = 0, wait 0, nSubState 0  Last value: 0 Frequency: 0Hz in/out/retry: 0/0/1  base: 0 0 0 0 A: 0, B: 0 dev: 0,000%  loop 6: error (state = 1, lastvalue = 0, wait 0, nSubState 0  Last value: 0 Frequency: 0Hz in/out/retry: 0/0/1  base: 0 0 0 0 A: 0, B: 0 dev: 0,000%  loop 7: error (state = 6, lastvalue = 0, wait 0, nSubState 0  Last value: 0 Frequency: 0Hz in/out/retry: 0/0/1  base: 0 0 0 0 A: 0, B: 0 dev: 0,000%"
        val executeScriptResult = new ExecuteScriptResult(script = new ExecuteScript("", Seq()), resultCode = Right(1), stdOut = Seq(str), stdError = Seq())
        val iRoseLoopStateMonitor = TestActorRef(new IRoseLoopStateMonitor(new IRoseCusStatParser(), executeScriptActor.ref, systemSensorMessageSender.ref, "site-buffer/src/test/resources/monitoring/cusStatusIrose.sh", dummyCurrentTime))
        iRoseLoopStateMonitor ! executeScriptResult
        systemSensorMessageSender.expectNoMsg()
      }
    }

    "receiving ExecuteScriptResult and  returns a throwable" must {
      "throwable should be logged" in {
        println("---------------------------START TEST(receiving ExecuteScriptResult and  returns a throwable)")
        val str = "loop 0 Last value: 1578703 Frequency: 79179Hz in/out/retry: 126/0/0 base: 1578691 1577152 1577918 1578502 A:1579149, B:1578244 dev: 0,057% Debug: 1 0 0 0 0 0 loop 1 Last value: 1583517 Frequency: 78939Hz in/out/retry: 126/0/0 base: 1583499 1581956 1582724 1583309 A:1584018, B:1582981 dev: 0,065% Debug: 1 0 0 0 0 0 loop 2 Last value: 1597160 Frequency: 78262Hz in/out/retry: 232/0/0 base: 1597193 1595636 1596411 1597002 A:1597704, B:1596696 dev: 0,063% Debug: 0 0 0 0 0 0 loop 3 Last value: 1570258 Frequency: 79605Hz in/out/retry: 232/0/0 base: 1570251 1568721 1569482 1570063 A:1570949, B:1569554 dev: 0,088%  Debug: 0 0 0 0 0 0  loop 4: error (state = 5, lastvalue = 0, wait 0, nSubState 0  Last value: 0 Frequency: 0Hz in/out/retry: 0/0/1  base: 0 0 0 0 A: 0, B: 0 dev: 0,000%  loop 5: error (state = 5, lastvalue = 0, wait 0, nSubState 0  Last value: 0 Frequency: 0Hz in/out/retry: 0/0/1  base: 0 0 0 0 A: 0, B: 0 dev: 0,000%  loop 6: error (state = 1, lastvalue = 0, wait 0, nSubState 0  Last value: 0 Frequency: 0Hz in/out/retry: 0/0/1  base: 0 0 0 0 A: 0, B: 0 dev: 0,000%  loop 7: error (state = 6, lastvalue = 0, wait 0, nSubState 0  Last value: 0 Frequency: 0Hz in/out/retry: 0/0/1  base: 0 0 0 0 A: 0, B: 0 dev: 0,000%"
        val executeScriptResult = new ExecuteScriptResult(script = new ExecuteScript("", Seq()), resultCode = Left(new Exception), stdOut = Seq(str), stdError = Seq())
        val iRoseLoopStateMonitor = TestActorRef(new IRoseLoopStateMonitor(new IRoseCusStatParser(), executeScriptActor.ref, systemSensorMessageSender.ref, "site-buffer/src/test/resources/monitoring/cusStatusIrose.sh", dummyCurrentTime))
        iRoseLoopStateMonitor ! executeScriptResult
      }
    }

    "receiving two ExecuteScriptResults with one overlapping looperror" must {
      "send one systemSensorMessage" in {
        println("---------------------------START TEST(receiving two ExecuteScriptResults with one overlapping looperror)")
        val str1 = "loop 0 Last value: 1578703 Frequency: 79179Hz in/out/retry: 126/0/0 base: 1578691 1577152 1577918 1578502 A:1579149, B:1578244 dev: 0,057% Debug: 1 0 0 0 0 0 loop 1 Last value: 1583517 Frequency: 78939Hz in/out/retry: 126/0/0 base: 1583499 1581956 1582724 1583309 A:1584018, B:1582981 dev: 0,065% Debug: 1 0 0 0 0 0 loop 2 Last value: 1597160 Frequency: 78262Hz in/out/retry: 232/0/0 base: 1597193 1595636 1596411 1597002 A:1597704, B:1596696 dev: 0,063% Debug: 0 0 0 0 0 0 loop 3 Last value: 1570258 Frequency: 79605Hz in/out/retry: 232/0/0 base: 1570251 1568721 1569482 1570063 A:1570949, B:1569554 dev: 0,088%  Debug: 0 0 0 0 0 0  loop 4: error (state = 5, lastvalue = 0, wait 0, nSubState 0  Last value: 0 Frequency: 0Hz in/out/retry: 0/0/1  base: 0 0 0 0 A: 0, B: 0 dev: 0,000%  loop 5: error (state = 5, lastvalue = 0, wait 0, nSubState 0  Last value: 0 Frequency: 0Hz in/out/retry: 0/0/1  base: 0 0 0 0 A: 0, B: 0 dev: 0,000%  loop 6: error (state = 1, lastvalue = 0, wait 0, nSubState 0  Last value: 0 Frequency: 0Hz in/out/retry: 0/0/1  base: 0 0 0 0 A: 0, B: 0 dev: 0,000%  loop 7: error (state = 6, lastvalue = 0, wait 0, nSubState 0  Last value: 0 Frequency: 0Hz in/out/retry: 0/0/1  base: 0 0 0 0 A: 0, B: 0 dev: 0,000%"
        val str2 = "loop 0 Last value: 1578703 Frequency: 79179Hz in/out/retry: 126/0/0 base: 1578691 1577152 1577918 1578502 A:1579149, B:1578244 dev: 0,057% Debug: 1 0 0 0 0 0 loop 1 Last value: 1583517 Frequency: 78939Hz in/out/retry: 126/0/0 base: 1583499 1581956 1582724 1583309 A:1584018, B:1582981 dev: 0,065% Debug: 1 0 0 0 0 0 loop 2 Last value: 1597160 Frequency: 78262Hz in/out/retry: 232/0/0 base: 1597193 1595636 1596411 1597002 A:1597704, B:1596696 dev: 0,063% Debug: 0 0 0 0 0 0 loop 3 Last value: 1570258 Frequency: 79605Hz in/out/retry: 232/0/0 base: 1570251 1568721 1569482 1570063 A:1570949, B:1569554 dev: 0,088%  Debug: 0 0 0 0 0 0  loop 4 Last value: 1578703 Frequency: 79179Hz in/out/retry: 126/0/0 base: 1578691 1577152 1577918 1578502 A:1579149, B:1578244 dev: 0,057% Debug: 1 0 0 0 0 0 loop 1 Last value: 1583517 Frequency: 78939Hz in/out/retry: 126/0/0 base: 1583499 1581956 1582724 1583309 A:1584018, B:1582981 dev: 0,065% Debug: 1 0 0 0 0 0  loop 5: error (state = 5, lastvalue = 0, wait 0, nSubState 0  Last value: 0 Frequency: 0Hz in/out/retry: 0/0/1  base: 0 0 0 0 A: 0, B: 0 dev: 0,000%  loop 6: error (state = 1, lastvalue = 0, wait 0, nSubState 0  Last value: 0 Frequency: 0Hz in/out/retry: 0/0/1  base: 0 0 0 0 A: 0, B: 0 dev: 0,000%  loop 7: error (state = 6, lastvalue = 0, wait 0, nSubState 0  Last value: 0 Frequency: 0Hz in/out/retry: 0/0/1  base: 0 0 0 0 A: 0, B: 0 dev: 0,000%"
        val executeScriptResult1 = new ExecuteScriptResult(script = new ExecuteScript("", Seq()), resultCode = Right(0), stdOut = Seq(str1), stdError = Seq())
        val executeScriptResult2 = new ExecuteScriptResult(script = new ExecuteScript("", Seq()), resultCode = Right(0), stdOut = Seq(str2), stdError = Seq())
        val iRoseLoopStateMonitor = TestActorRef(new IRoseLoopStateMonitor(new IRoseCusStatParser(), executeScriptActor.ref, systemSensorMessageSender.ref, "site-buffer/src/test/resources/monitoring/cusStatusIrose.sh", dummyCurrentTime))
        iRoseLoopStateMonitor ! executeScriptResult1
        iRoseLoopStateMonitor ! executeScriptResult2

        systemSensorMessageSender.expectMsg(createSystemSensorMessage(new IRoseLoopError("5", "state= 5 - IROSE2_STATE_ERR_LOWFREQ")))
      }
    }

    "receiving two ExecuteScriptResults with two overlapping looperrors" must {
      "send two systemSensorMessages" in {
        println("---------------------------START TEST(receiving two ExecuteScriptResults with two overlapping looperrors)")
        val str1 = "loop 0 Last value: 1578703 Frequency: 79179Hz in/out/retry: 126/0/0 base: 1578691 1577152 1577918 1578502 A:1579149, B:1578244 dev: 0,057% Debug: 1 0 0 0 0 0 loop 1 Last value: 1583517 Frequency: 78939Hz in/out/retry: 126/0/0 base: 1583499 1581956 1582724 1583309 A:1584018, B:1582981 dev: 0,065% Debug: 1 0 0 0 0 0 loop 2 Last value: 1597160 Frequency: 78262Hz in/out/retry: 232/0/0 base: 1597193 1595636 1596411 1597002 A:1597704, B:1596696 dev: 0,063% Debug: 0 0 0 0 0 0 loop 3 Last value: 1570258 Frequency: 79605Hz in/out/retry: 232/0/0 base: 1570251 1568721 1569482 1570063 A:1570949, B:1569554 dev: 0,088%  Debug: 0 0 0 0 0 0  loop 4: error (state = 5, lastvalue = 0, wait 0, nSubState 0  Last value: 0 Frequency: 0Hz in/out/retry: 0/0/1  base: 0 0 0 0 A: 0, B: 0 dev: 0,000%  loop 5: error (state = 4, lastvalue = 0, wait 0, nSubState 0  Last value: 0 Frequency: 0Hz in/out/retry: 0/0/1  base: 0 0 0 0 A: 0, B: 0 dev: 0,000%  loop 6: error (state = 1, lastvalue = 0, wait 0, nSubState 0  Last value: 0 Frequency: 0Hz in/out/retry: 0/0/1  base: 0 0 0 0 A: 0, B: 0 dev: 0,000%  loop 7: error (state = 6, lastvalue = 0, wait 0, nSubState 0  Last value: 0 Frequency: 0Hz in/out/retry: 0/0/1  base: 0 0 0 0 A: 0, B: 0 dev: 0,000%"
        val str2 = "loop 0 Last value: 1578703 Frequency: 79179Hz in/out/retry: 126/0/0 base: 1578691 1577152 1577918 1578502 A:1579149, B:1578244 dev: 0,057% Debug: 1 0 0 0 0 0 loop 1 Last value: 1583517 Frequency: 78939Hz in/out/retry: 126/0/0 base: 1583499 1581956 1582724 1583309 A:1584018, B:1582981 dev: 0,065% Debug: 1 0 0 0 0 0 loop 2 Last value: 1597160 Frequency: 78262Hz in/out/retry: 232/0/0 base: 1597193 1595636 1596411 1597002 A:1597704, B:1596696 dev: 0,063% Debug: 0 0 0 0 0 0 loop 3 Last value: 1570258 Frequency: 79605Hz in/out/retry: 232/0/0 base: 1570251 1568721 1569482 1570063 A:1570949, B:1569554 dev: 0,088%  Debug: 0 0 0 0 0 0  loop 4: error (state = 5, lastvalue = 0, wait 0, nSubState 0  Last value: 0 Frequency: 0Hz in/out/retry: 0/0/1  base: 0 0 0 0 A: 0, B: 0 dev: 0,000%  loop 5: error (state = 4, lastvalue = 0, wait 0, nSubState 0  Last value: 0 Frequency: 0Hz in/out/retry: 0/0/1  base: 0 0 0 0 A: 0, B: 0 dev: 0,000%  loop 6: error (state = 1, lastvalue = 0, wait 0, nSubState 0  Last value: 0 Frequency: 0Hz in/out/retry: 0/0/1  base: 0 0 0 0 A: 0, B: 0 dev: 0,000%  loop 7: error (state = 6, lastvalue = 0, wait 0, nSubState 0  Last value: 0 Frequency: 0Hz in/out/retry: 0/0/1  base: 0 0 0 0 A: 0, B: 0 dev: 0,000%"
        val executeScriptResult1 = new ExecuteScriptResult(script = new ExecuteScript("", Seq()), resultCode = Right(0), stdOut = Seq(str1), stdError = Seq())
        val executeScriptResult2 = new ExecuteScriptResult(script = new ExecuteScript("", Seq()), resultCode = Right(0), stdOut = Seq(str2), stdError = Seq())
        val iRoseLoopStateMonitor = TestActorRef(new IRoseLoopStateMonitor(new IRoseCusStatParser(), executeScriptActor.ref, systemSensorMessageSender.ref, "site-buffer/src/test/resources/monitoring/cusStatusIrose.sh", dummyCurrentTime))
        iRoseLoopStateMonitor ! executeScriptResult1
        iRoseLoopStateMonitor ! executeScriptResult2

        systemSensorMessageSender.expectMsg(createSystemSensorMessage(new IRoseLoopError("4", "state= 5 - IROSE2_STATE_ERR_LOWFREQ")))
        systemSensorMessageSender.expectMsg(createSystemSensorMessage(new IRoseLoopError("5", "state= 4 - IROSE2_STATE_ERR_HIGHFREQ")))
      }
    }

    def createSystemSensorMessage(iroseLoopError: IRoseLoopError): SystemSensorMessage = {
      SystemSensorMessage(Sensor.DetectionLoop, mockCurrentTime, "", EventType.DetectionLoopState, "Sitebuffer", immutable.Map("loopId" -> iroseLoopError.loopNr, "state" -> iroseLoopError.state))
    }
  }
}