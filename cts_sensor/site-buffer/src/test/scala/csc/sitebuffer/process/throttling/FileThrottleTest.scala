package csc.sitebuffer.process.throttling

import java.io.File

import csc.akka.logging.DirectLogging
import csc.sitebuffer.FileTestUtils
import csc.sitebuffer.FileTestUtils._
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }
import testframe.Resources

/**
 * Copyright (C) 2012-2014 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 4/7/14.
 */

class FileThrottleTest extends WordSpec with MustMatchers with BeforeAndAfterAll with BeforeAndAfterEach with DirectLogging {

  val resourceDir = new File(Resources.getResourceDirPath().getAbsolutePath)
  val testBaseDir = new File(resourceDir, "throttleTest")
  if (!testBaseDir.exists()) {
    testBaseDir.mkdirs()
  } else {
    clearDirectory(testBaseDir)
  }
  val testOutputDir = new File(testBaseDir, "output")
  val testInputDir = new File(testBaseDir, "input")
  val throttleResourceDir = new File(resourceDir, "csc/sitebuffer/process/throttling")

  override def beforeAll() {
    super.beforeAll()
  }

  override def afterAll() {
    clearDirectory(testBaseDir)
    testBaseDir.delete()
  }

  override def beforeEach() {
    new File(testBaseDir, "input")
  }

  override def afterEach() {
    clearDirectory(testBaseDir)
  }

  "A FileThrottler object" when {
    "being created and input directory does not exist" must {
      "throw an IllegalArgumentException" in {
        intercept[IllegalArgumentException] {
          new FileThrottler(testInputDir, testOutputDir, 5, { s ⇒ s }, { aFile ⇒ true })
        }
      }
    }

    "being created and output directory does not exist" must {
      "create the directory passed in the constructor" in {
        // Make sure directory does not exist beforehand
        testOutputDir.exists() must be(false)
        testInputDir.mkdirs()
        // Create FileThrottler
        new FileThrottler(testInputDir, testOutputDir, 5, { s ⇒ s }, { aFile ⇒ true })
        // Check expected results
        testOutputDir.exists() must be(true)
      }
    }
    "when poll method is called" must {
      "move files to output directory until threshold is reached" in {
        // Make sure directory does not exist beforehand
        testInputDir.mkdirs()
        testInputDir.exists must be(true)
        val inputFIles = testInputFiles
        // Create FileThrottler

        val throttle = new FileThrottler(testInputDir, testOutputDir, 3,
          { s: Seq[File] ⇒ s.sortBy(f ⇒ f.getName) },
          { aFile ⇒ true })
        testOutputDir.exists must be(true)

        // Start throttling files.
        throttle.poll
        val outputFiles: Array[File] = testOutputDir.listFiles()
        outputFiles.length must be(3)
        testInputDir.listFiles.length must be(9)
        deleteFile(outputFiles(0))
        testOutputDir.listFiles.length must be(2)
        throttle.poll
        testOutputDir.listFiles.length must be(3)
        testInputDir.listFiles.length must be(8)

        clearDirectory(testOutputDir)
        throttle.poll
        testOutputDir.listFiles.length must be(3)
        testInputDir.listFiles.length must be(5)

        clearDirectory(testOutputDir)
        throttle.poll
        testOutputDir.listFiles.length must be(3)
        testInputDir.listFiles.length must be(2)

        clearDirectory(testOutputDir)
        throttle.poll
        testOutputDir.listFiles.length must be(2)
        testInputDir.listFiles.length must be(0)
      }
    }
  }

  def copyTestResource(resourceName: String, toFilePath: File): File = {
    copyFile(new File(throttleResourceDir, resourceName), toFilePath)
  }

  def testInputFiles: Array[File] = Array(
    copyTestResource("textFile.txt", new File(testInputDir, "001.txt")),
    copyTestResource("textFile.txt", new File(testInputDir, "002.txt")),
    copyTestResource("textFile.txt", new File(testInputDir, "003.txt")),
    copyTestResource("textFile.txt", new File(testInputDir, "004.txt")),
    copyTestResource("textFile.txt", new File(testInputDir, "005.txt")),
    copyTestResource("textFile.txt", new File(testInputDir, "006.txt")),
    copyTestResource("textFile.txt", new File(testInputDir, "007.txt")),
    copyTestResource("textFile.txt", new File(testInputDir, "008.txt")),
    copyTestResource("textFile.txt", new File(testInputDir, "009.txt")),
    copyTestResource("textFile.txt", new File(testInputDir, "010.txt")),
    copyTestResource("textFile.txt", new File(testInputDir, "011.txt")),
    copyTestResource("textFile.txt", new File(testInputDir, "012.txt")))

}