/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.messages

import akka.testkit.{ TestActorRef, TestProbe, TestKit }
import akka.actor.{ ActorRef, ActorSystem }
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }
import org.scalatest._
import csc.sitebuffer.messages.{ Direction, VehicleMessage }
import scala.concurrent.duration._

/**
 * Test for VehicleMessageFilter actor
 */
class VehicleMessageFilterTest extends TestKit(ActorSystem("VehicleMessageFilterTest")) with WordSpecLike with MustMatchers
  with BeforeAndAfterAll with BeforeAndAfterEach {

  val trueRecipient1 = TestProbe()
  val trueRecipient2 = TestProbe()
  val falseRecipient1 = TestProbe()
  val falseRecipient2 = TestProbe()

  override def beforeAll() {
    super.beforeAll()
  }

  override def afterAll() {
    system.shutdown()
  }

  private def createVehicleMessageFilter(filterCondition: (VehicleMessage) ⇒ Boolean, trueRecipients: Set[ActorRef], falseRecipients: Set[ActorRef]): TestActorRef[VehicleMessageFilter] = {
    TestActorRef(
      new VehicleMessageFilter(filterCondition = filterCondition, trueRecipients = trueRecipients, falseRecipients = falseRecipients))
  }

  private def createVehicleMessage(offset: Int, valid: Boolean = true): VehicleMessage = {
    VehicleMessage(dateTime = 1360650493123L + offset, detector = 1, direction = Direction.Outgoing,
      speed = 23.0f, length = 50.0f, loop1RiseTime = 0L, loop1FallTime = 450L,
      loop2RiseTime = 400L, loop2FallTime = 875L, yellowTime2 = 0, redTime2 = 0,
      dateTime1 = 1360650493005L + offset, yellowTime1 = 0, redTime1 = 0, dateTime3 = 0L + offset,
      yellowTime3 = 0, redTime3 = 0, valid = valid)
  }

  "The VehicleMessageFilter, configured with an always true filterCondition" when {

    "receiving 3 VehicleMessages" must {
      "pass all messages on to all trueRecipients and none to the falseRecipients" in {
        val alwaysTrue = (message: VehicleMessage) ⇒ true
        val vehicleMessageFilter = createVehicleMessageFilter(
          filterCondition = alwaysTrue, trueRecipients = Set(trueRecipient1.ref, trueRecipient2.ref), falseRecipients = Set(falseRecipient1.ref, falseRecipient2.ref))

        Set(100, 200, 300).foreach { offset ⇒
          val message = createVehicleMessage(offset)
          vehicleMessageFilter ! message

          expectMessage(Set(trueRecipient1, trueRecipient2), message)
          expectNoMessage(Set(falseRecipient1, falseRecipient2), message)
        }
      }
    }

  }

  "The VehicleMessageFilter, configured with an always false filterCondition" when {

    "receiving 3 VehicleMessages" must {
      "pass all messages on to all falseRecipients and none to the trueRecipients" in {
        val alwaysFalse = (message: VehicleMessage) ⇒ false
        val vehicleMessageFilter = createVehicleMessageFilter(
          filterCondition = alwaysFalse, trueRecipients = Set(trueRecipient1.ref, trueRecipient2.ref), falseRecipients = Set(falseRecipient1.ref, falseRecipient2.ref))

        Set(100, 200, 300).foreach { offset ⇒
          val message = createVehicleMessage(offset)
          vehicleMessageFilter ! message

          expectMessage(Set(falseRecipient1, falseRecipient2), message)
          expectNoMessage(Set(trueRecipient1, trueRecipient2), message)
        }
      }
    }

  }

  "The VehicleMessageFilter, configured with a filterCondition that returns the valid field" when {

    "receiving several VehicleMessages" must {
      "pass all messages with valid=true to all trueRecipients and all messages with valid=false to the falseRecipients" in {
        val checkValid = (message: VehicleMessage) ⇒ message.valid
        val vehicleMessageFilter = createVehicleMessageFilter(
          filterCondition = checkValid, trueRecipients = Set(trueRecipient1.ref, trueRecipient2.ref), falseRecipients = Set(falseRecipient1.ref, falseRecipient2.ref))

        Set((100, true), (200, true), (300, false), (400, false), (500, true), (600, false)).foreach {
          case (offset, valid) ⇒
            val message = createVehicleMessage(offset = offset, valid = valid)
            vehicleMessageFilter ! message

            if (valid) {
              expectMessage(Set(trueRecipient1, trueRecipient2), message)
              expectNoMessage(Set(falseRecipient1, falseRecipient2), message)
            } else {
              expectMessage(Set(falseRecipient1, falseRecipient2), message)
              expectNoMessage(Set(trueRecipient1, trueRecipient2), message)
            }
        }
      }
    }

  }

  private def expectMessage(probes: Set[TestProbe], message: VehicleMessage) {
    probes.foreach { probe ⇒
      probe.expectMsg(500 millis, message)
    }
  }

  private def expectNoMessage(probes: Set[TestProbe], message: VehicleMessage) {
    probes.foreach { probe ⇒
      probe.expectNoMsg(500 millis)
    }
  }

}
