/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.messages

import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import scala.concurrent.duration._
import csc.sitebuffer.config.CabinetDoorConfig
import csc.sitebuffer.messages.InputEventMessage
import csc.sitebuffer.process.SystemSensorMessage
import org.scalatest._
import org.scalatest.{ BeforeAndAfterAll, WordSpec }

/**
 * Test van IRoseDigitalInputMonitor
 */
class IRoseDigitalInputMonitorTest extends TestKit(ActorSystem("IRoseDigitalInputMonitorTest")) with WordSpecLike with MustMatchers with BeforeAndAfterAll {

  override def beforeAll() {
    super.beforeAll()
  }

  override def afterAll() {
    system.shutdown
  }

  "The IRoseDigitalInputMonitor" when {
    val config = CabinetDoorConfig(inputNumber = 0, openEventNumber = 4, closedEventNumber = 3)

    val testProbe = TestProbe()
    val iRoseDigitalInputActorRef = TestActorRef(new IRoseDigitalInputMonitor(cabinetDoorConfig = config, systemSensorMessageSender = testProbe.ref))

    "receiving an InputEventMessage, containing an 'Cabinet door open' event" must {
      "send an SystemSensorMessage(Door, Open) to the systemSensorMessageSender" in {
        // create message
        val inputEventMessage = InputEventMessage(dateTime = 123456789L, inputId = 0, eventNumber = 4, eventText = "Cabinet door open")
        // construct expected message
        val expectedMsg = SystemSensorMessage(
          sensor = "Door",
          timeStamp = 123456789L,
          systemId = "",
          event = "Open",
          sendingComponent = "Sitebuffer",
          data = Map())
        // send message
        iRoseDigitalInputActorRef ! inputEventMessage
        // test if expected message is received
        testProbe.expectMsg(500 millis, expectedMsg)
      }
    }

    "receiving an InputEventMessage, containing an 'Cabinet door closed' event" must {
      "send an SystemSensorMessage(Door, Closed) to the systemSensorMessageSender" in {
        // create message
        val inputEventMessage = InputEventMessage(dateTime = 123456789L, inputId = 0, eventNumber = 3, eventText = "Cabinet door closed")
        // construct expected message
        val expectedMsg = SystemSensorMessage(
          sensor = "Door",
          timeStamp = 123456789L,
          systemId = "",
          event = "Closed",
          sendingComponent = "Sitebuffer",
          data = Map())
        // send message
        iRoseDigitalInputActorRef ! inputEventMessage
        // test if expected message is received
        testProbe.expectMsg(500 millis, expectedMsg)
      }
    }

    "receiving an InputEventMessage, for the cabinet door input, but with an unknown event number" must {
      "ignore this message" in {
        // create message
        val inputEventMessage = InputEventMessage(dateTime = 123456789L, inputId = 0, eventNumber = 5, eventText = "Unknown event number")
        // send message
        iRoseDigitalInputActorRef ! inputEventMessage
        // test if expected message is received
        testProbe.expectNoMsg(1000 millis)
      }
    }

    "receiving an InputEventMessage, for another input" must {
      "ignore this message" in {
        // create message
        val inputEventMessage = InputEventMessage(dateTime = 123456789L, inputId = 1, eventNumber = 3, eventText = "Not the cabinet door input")
        // send message
        iRoseDigitalInputActorRef ! inputEventMessage
        // test if expected message is received
        testProbe.expectNoMsg(1000 millis)
      }
    }

    "receiving an InputEventMessage, with inputId = 8" must {
      "ignore this message" in {
        // create message
        val inputEventMessage = InputEventMessage(dateTime = 123456789L, inputId = 8, eventNumber = 4, eventText = "This is an alarm!")
        // send message
        iRoseDigitalInputActorRef ! inputEventMessage
        // test if expected message is received
        testProbe.expectNoMsg(1000 millis)
      }
    }

    "receiving a message other than InputEventMessage" must {
      "ignore this message" in {
        // create message
        val inputEventMessage = "Some kind of non InputEventMessage"
        // send message
        iRoseDigitalInputActorRef ! inputEventMessage
        // test if expected message is received
        testProbe.expectNoMsg(1000 millis)
      }
    }

  }
}
