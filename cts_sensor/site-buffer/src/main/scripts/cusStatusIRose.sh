#!/bin/bash

login=root
passwd=admin
logdir=$(dirname $0)/../logs
logFile="${logdir}/IroseCusStatus.log"
ipIrose="192.168.1.10"

[ "$DISPLAY" ] || export DISPLAY=dummydisplay:0
export SSH_ASKPASS=$(mktemp)
echo echo $passwd>${SSH_ASKPASS}
chmod 700 ${SSH_ASKPASS}
rm -f ${HOME}/.ssh/known_hosts
date >> ${logFile}
echo "Cus status Irose ${ipIrose}" >> ${logFile}
setsid ssh  -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -T ${login}@${ipIrose} cus status
rm -f ${SSH_ASKPASS}