/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sitebuffer.config

import com.typesafe.config.{ Config, ConfigFactory }
import csc.akka.config.ConfigUtil._
import scala.concurrent.duration._
import java.io.File

/**
 * Configuration for host-port combinations.
 * @param host The configured host. (IP or hostname)
 * @param port The configured port number.
 */
case class HostPortConfig(host: String, port: Int) {

  def this(configPath: String, config: Config) = {
    this(
      config.getReplacedString(configPath + ".host", ""),
      config.getReplacedInteger(configPath + ".port", 0))
  }
}

/**
 * Configuration for a directory
 * @param directory Path string of the configured directory.
 */
case class DirectoryConfig(directory: String) {

  def this(configPath: String, config: Config) = {
    this(
      config.getReplacedString(configPath + ".directory", ""))
  }
}

case class FileThrottleConfig(inputDirectory: File, outputDirectory: File,
                              subDirectoryMask: String, newDirectoryPrefix: String, fileMask: String,
                              threshold: Int, interval_ms: Int) {
  def this(configPath: String, config: Config) = {
    this(
      new File(config.getReplacedString(configPath + ".inputPath")),
      new File(config.getReplacedString(configPath + ".outputPath")),
      config.getReplacedString(configPath + ".subDirectoryMask"),
      config.getReplacedString(configPath + ".newDirectoryPrefix", ""),
      config.getReplacedString(configPath + ".fileMask"),
      config.getReplacedInteger(configPath + ".threshold"),
      config.getReplacedInteger(configPath + ".interval_ms"))
  }
}
/**
 * Configuration for de loop failure events.
 * @param checkFrequencyMs Frequency in which iroses are checked.
 * @param cusStatusIRosePath path to the script.
 */
case class LoopStateConfig(checkFrequencyMs: FiniteDuration, cusStatusIRosePath: String) {

  def this(configPath: String, config: Config) = {
    this(
      config.getReplacedInteger(configPath + ".checkFrequencyMs", 0) millis,
      config.getReplacedString(configPath + ".cusStatusIRosePath", ""))
  }
}

/**
 * Configuration for de loop state events.
 * @param maxAllowedMessages Maximum number of single loop messages allowed (without triggering a sensor event) within given test window.
 * @param testWindowMs Test window used for determining loop failure.
 */
case class LoopFailureConfig(maxAllowedMessages: Int, testWindowMs: Int, iRoseRebootScriptPath: String) {

  def this(configPath: String, config: Config) = {
    this(
      config.getReplacedInteger(configPath + ".maxAllowedMessages", 0),
      config.getReplacedInteger(configPath + ".testWindowMs", 0),
      config.getReplacedString(configPath + ".iRoseRebootScriptPath", ""))
  }
}

/**
 * Configuration for de ntp failure events.
 * @param maxAllowedMessages Maximum number of "1970" messages allowed (without triggering a sensor event) within given test window.
 * @param testWindowMs Test window used for determining ntp failure.
 */
case class NtpFailureConfig(maxAllowedMessages: Int, testWindowMs: Int) {

  def this(configPath: String, config: Config) = {
    this(
      config.getReplacedInteger(configPath + ".maxAllowedMessages", 0),
      config.getReplacedInteger(configPath + ".testWindowMs", 0))
  }
}

/**
 * Configuration for cabinet door events.
 * @param inputNumber Number of the General Purpose I/O used
 * @param openEventNumber Event number for a door open event
 * @param closedEventNumber Event number for a door closed event
 */
case class CabinetDoorConfig(inputNumber: Int, openEventNumber: Int, closedEventNumber: Int) {

  def this(configPath: String, config: Config) = {
    this(
      config.getReplacedInteger(configPath + ".inputNumber", 0),
      config.getReplacedInteger(configPath + ".openEventNumber", 0),
      config.getReplacedInteger(configPath + ".closedEventNumber", 0))
  }
}

/**
 * Configuration for cameras.
 * @param cameraId The id of a camera
 * @param cameraHost The host address used to access the camera
 * @param cameraPort The port used to access the camera
 * @param maxTriggerRetries Maximum number of retries for accessing the camera
 * @param timeDisconnectedMs Geen idee
 * @param rebootScript The script used to reboot the camera
 * @param detectors The detectors linked to this camera
 * @param imageDir
 */
case class CameraConfig(cameraId: String, cameraHost: String, cameraPort: Int,
                        maxTriggerRetries: Int, timeDisconnectedMs: Int,
                        rebootScript: String, detectors: Seq[Int], imageDir: String,
                        maxWaitForImage: Long, maxWaitForReboot: Long, waitForFixedMsg: Long) {

  def this(configPath: String, config: Config) = {

    this(
      config.getReplacedString(configPath + ".cameraId", {
        val index = configPath.lastIndexOf(".")
        if (index < 0) "" else configPath.substring(index + 1)
      }),
      config.getReplacedString(configPath + ".cameraHost", ""),
      config.getReplacedInteger(configPath + ".cameraPort", 0),
      config.getReplacedInteger(configPath + ".maxTriggerRetries", 0),
      config.getReplacedInteger(configPath + ".timeDisconnectedMs", 0),
      config.getReplacedString(configPath + ".rebootScript", ""),
      {
        val list = config.getReplacedString(configPath + ".detectors", "")
        list.split(",").foldLeft(Seq[Int]()) {
          case (list, arg) ⇒ try {
            list :+ arg.toInt
          } catch {
            case ex: Exception ⇒ list
          }
        }
      },
      config.getReplacedString(configPath + ".imageDir", ""),
      config.getReplacedLong(configPath + ".maxWaitForImage", 1.minute.toMillis),
      config.getReplacedLong(configPath + ".maxWaitForReboot", 10.minutes.toMillis),
      config.getReplacedLong(configPath + ".waitForFixedMsg", 30.minutes.toMillis))
  }
}

/**
 * Configuration for camera monitor.
 * @param enabled Is the CameraMonitor enabled?
 * @param checkFrequency Frequency in which cameras are checked.
 * @param alarmDelay Delay after alarm has been received and rechecking if alarm still exist. Only then is alarm raised.
 * @param maxRetries Maximum number of retries for connecting to the camera.
 * @param timeoutDuration Timeout duration before retrying to connect to the camera.
 */
case class CameraMonitorConfig(enabled: Boolean, checkFrequency: FiniteDuration, alarmDelay: FiniteDuration, maxRetries: Int, timeoutDuration: FiniteDuration) {

  def this(configPath: String, config: Config) = {
    this(
      config.getReplacedString(configPath + ".enabled", "false") == "true",
      config.getReplacedInteger(configPath + ".checkFrequencyMs", 0) millis,
      config.getReplacedInteger(configPath + ".alarmDelayMs", 0) millis,
      config.getReplacedInteger(configPath + ".maxRetries", 0),
      config.getReplacedInteger(configPath + ".timeoutDurationMs", 0) millis)
  }
}

/**
 * Configuration for WasDownMonitor.
 * @param enabled Is wasDownMonitor enabled?
 * @param timestampDirectory Directory which wasDownMonitor will use (will be created automatically)
 * @param timestampUpdateFrequency Frequency in which timestamp will be updated
 * @param minimalDeviationForWasDown Minimal deviation relative to last saved timestamp needed to trigger a WasDown event
 */
case class WasDownMonitorConfig(enabled: Boolean, timestampDirectory: String, timestampUpdateFrequency: FiniteDuration, minimalDeviationForWasDown: Duration) {

  def this(configPath: String, config: Config) = {
    this(
      config.getReplacedString(configPath + ".enabled", "false") == "true",
      config.getReplacedString(configPath + ".timestampDirectory", ""),
      config.getReplacedInteger(configPath + ".timestampUpdateFrequencyInSeconds", 0) seconds,
      config.getReplacedInteger(configPath + ".minimalDeviationForWasDownInSeconds", 0) seconds)
  }
}

/**
 * Configuration for processing of incoming images.
 * @param newImagesEndpoint Host/port of endpoint for receiving full path names of all new images
 * @param directoryPrefixForOutgoingImages Prefix that will be added to name of camera directory in order to get outgoing directory
 * @param maxDeleteRetries # Maximum number of retries for deleting an image.
 * @param minDelayBeforeRetryMs # Minimal delay (in millis) before retry to delete an image.
 */
case class ImageProcessingConfig(newImagesEndpoint: HostPortConfig, directoryPrefixForOutgoingImages: String, maxDeleteRetries: Int, minDelayBeforeRetryMs: Int) {

  def this(configPath: String, config: Config) = {
    this(
      new HostPortConfig(configPath + ".newImagesEndpoint", config),
      config.getReplacedString(configPath + ".directoryPrefixForOutgoingImages", "O"),
      config.getReplacedInteger(configPath + ".maxDeleteRetries", 0),
      config.getReplacedInteger(configPath + ".minDelayBeforeRetryMs", 0))
  }
}

/**
 * Configuration for recording of input
 * @param enabled Is inputRecording enabled?
 * @param baseDirectory Base directory where recordings are stored (will be created automatically)
 */
case class InputRecordingConfig(enabled: Boolean, baseDirectory: String, imageFilenamesOnly: Boolean) {

  def this(configPath: String, config: Config) = {
    this(
      config.getReplacedString(configPath + ".enabled", "false") == "true",
      config.getReplacedString(configPath + ".baseDirectory", ""),
      config.getReplacedString(configPath + ".imageFilenamesOnly", "false") == "true")
  }
}

case class ImageFilterConfig(enable: Boolean, imageExtension: String, maxDelayTime: Duration) {

  def this(configPath: String, config: Config) = {
    this(
      config.getReplacedString(configPath + ".enabled", "false") == "true",
      config.getReplacedString(configPath + ".imageExtension", "jpg"),
      config.getReplacedLong(configPath + ".maxDelayTime", 1000L).millis)
  }
}

case class CalibrationConfig(telnetUser: String, telnetPwd: String, telnetPort: Int,
                             sshHost: String, sshPort: Int, sshUser: String, sshPwd: String, sshTimeout: Int,
                             NTPmaxDiff_ms: Long,
                             ftpRootDir: String,
                             calibrationImageDir: String,
                             speedThreshold: Int,
                             centralHost: String,
                             centralport: Int,
                             msgRetryDelayMs: Int,
                             consumerHost: String,
                             consumerPort: Int,
                             retriggerTime: FiniteDuration,
                             maxProcessTime: Duration,
                             maxMessageLength: Int,
                             jpgQuality: Int) {
  def this(configPath: String, config: Config) = {
    this(
      config.getReplacedString(configPath + ".camera.telnetUser", "root"),
      config.getReplacedString(configPath + ".camera.telnetPwd", "JAIPULNiX"),
      config.getReplacedInteger(configPath + ".camera.telnetPort", 23),
      config.getReplacedString(configPath + ".iRose.sshHost", "192.168.1.10"),
      config.getReplacedInteger(configPath + ".iRose.sshPort", 22),
      config.getReplacedString(configPath + ".iRose.sshUser", "root"),
      config.getReplacedString(configPath + ".iRose.sshPwd", "admin"),
      config.getReplacedInteger(configPath + ".iRose.sshTimeout", 30000),
      config.getReplacedLong(configPath + ".NTPmaxDiff_ms", 10),
      config.getReplacedString(configPath + ".ftpRootDir", "/data"),
      config.getReplacedString(configPath + ".calibrationImageDir", "calibration"),
      config.getReplacedInteger(configPath + ".speedThreshold", 300),
      config.getReplacedString(configPath + ".response.centralHost", "20.133.70.197"),
      config.getReplacedInteger(configPath + ".response.centralport", 12350),
      config.getReplacedInteger(configPath + ".response.msgRetryDelayMs", 15000),
      config.getReplacedString(configPath + ".request.host", "0.0.0.0"),
      config.getReplacedInteger(configPath + ".request.port", 12300),
      config.getReplacedInteger(configPath + ".retriggerTimeMs", 20000).millis,
      config.getReplacedInteger(configPath + ".maxProcessTimeMs", 300000).millis,
      config.getReplacedInteger(configPath + ".maxMessageLength", 1000),
      config.getReplacedInteger(configPath + ".jpgQuality", 100))
  }
}

/**
 * Class holding the configuration for the SiteBuffer
 * @param iRoseEndpoint Host/port where iRose messages are received. (camel mina tcp endpoint)
 * @param vrEndpoint Directory to which vehicle messages are written for vehicle registration processing (camel file endpoint)
 * @param anprEndpoint Directory to which vehicle messages are written for anpr processing. (camel file endpoint)
 * @param systemSensorMessageEndpoint Host/port to which SystemSensorMessages are send. (camel mina tcp endpoint)
 * @param consecutiveFailedSensorReadingsCountForAlarm Number of consecutive failed sensor readings needed to trigger a SystemSensorMessage.
 * @param isAliveCheck IsAliveCheck message configuration.
 * @param clockOutOfSyncMarginMs Margin (in milliseconds) the clock is allowed to be out of sync before a SystemSensorMessage created.
 * @param systemSensorMessageRetryDelayMs Delay (in milliseconds) between a SystemSensorMessage send failure and it being resend.
 * @param repeatingSystemSensorMessagesFilterWindowMs Window (in milliseconds) in which a specific SystemSensorMessage can only be send once.
 * @param minimalSpeedForProcessingInvalidVehicleMessageKmh Minimal speed (in km/h) required to process an invalid message.
 * @param loopFailure Configuration for de loop failure events.
 * @param cabinetDoorConfig Configuration for cabinet door events.
 * @param cameraConfigs Configuration of all the cameras.
 * @param cameraMonitorConfig Configuration of the camera monitor.
 * @param ntpFailure Configuration of the NTP failure monitor.
 * @param wasDown Configuration of the "was down" monitor.
 * @param imageProcessing Configuration for processing of incoming images.
 * @param inputRecording Configuration for recording of input.
 */
case class SiteBufferConfiguration(
  iRoseEndpoint: HostPortConfig,
  vrEndpoint: DirectoryConfig,
  anprEndpoint: DirectoryConfig,
  systemSensorMessageEndpoint: HostPortConfig,
  throttling: Throttle,
  consecutiveFailedSensorReadingsCountForAlarm: Int,
  isAliveCheck: IsAliveCheck,
  clockOutOfSyncMarginMs: Int,
  systemSensorMessageRetryDelayMs: Int,
  repeatingSystemSensorMessagesFilterWindowMs: Int,
  minimalSpeedForProcessingInvalidVehicleMessageKmh: Int,
  loopFailure: LoopFailureConfig,
  cabinetDoorConfig: CabinetDoorConfig,
  cameraConfigs: Map[String, CameraConfig],
  cameraMonitorConfig: CameraMonitorConfig,
  ntpFailure: NtpFailureConfig,
  wasDown: WasDownMonitorConfig,
  imageProcessing: ImageProcessingConfig,
  inputRecording: InputRecordingConfig,
  imageFilter: ImageFilterConfig,
  loopState: LoopStateConfig,
  calibration: CalibrationConfig) {

  def this(configPath: String, config: Config) = {
    this(
      new HostPortConfig(configPath + ".iRoseEndpoint", config),
      new DirectoryConfig(configPath + ".vrEndpoint", config),
      new DirectoryConfig(configPath + ".anprEndpoint", config),
      new HostPortConfig(configPath + ".systemSensorMessageEndpoint", config),
      new Throttle(configPath, config),
      config.getReplacedInteger(configPath + ".consecutiveFailedSensorReadingsCountForAlarm", 0),
      new IsAliveCheck(configPath, config),
      config.getReplacedInteger(configPath + ".clockOutOfSyncMarginMs", 500),
      config.getReplacedInteger(configPath + ".systemSensorMessageRetryDelayMs", 2000),
      config.getReplacedInteger(configPath + ".repeatingSystemSensorMessagesFilterWindowMs", 60000),
      config.getReplacedInteger(configPath + ".minimalSpeedForProcessingInvalidVehicleMessageKmh", 20),
      new LoopFailureConfig(configPath + ".loopFailure", config),
      new CabinetDoorConfig(configPath + ".cabinetDoor", config),
      SiteBufferConfiguration.getCameraConfigs(configPath + ".cameras", config),
      new CameraMonitorConfig(configPath + ".cameraMonitor", config),
      new NtpFailureConfig(configPath + ".ntpFailure", config),
      new WasDownMonitorConfig(configPath + ".wasDownMonitor", config),
      new ImageProcessingConfig(configPath + ".imageProcessing", config),
      new InputRecordingConfig(configPath + ".inputRecording", config),
      new ImageFilterConfig(configPath + ".imageFilter", config),
      new LoopStateConfig(configPath + ".loopState", config),
      new CalibrationConfig(configPath + ".calibration", config))
  }

}

object SiteBufferConfiguration {
  def apply() = new SiteBufferConfiguration("site-buffer", ConfigFactory.load())
  def apply(configPath: String) = {
    val config = ConfigFactory.load()
    if (!config.hasPath(configPath))
      throw new Exception("Path %s does not exist in configuration".format(configPath))
    new SiteBufferConfiguration(configPath, config)
  }

  def getCameraConfigs(configPath: String, config: Config): Map[String, CameraConfig] = {
    val cameraIdList = config.getConfig(configPath).getKeySet()
    cameraIdList.map { cameraId ⇒ (cameraId, new CameraConfig(configPath + "." + cameraId, config)) }.toMap
  }
}

/**
 * Throttle parameters
 * @param vrThrottling
 * @param imageThrottling
 */
case class Throttle(vrThrottling: FileThrottleConfig, imageThrottling: FileThrottleConfig) {
  def this(configPath: String, config: Config) = {
    this(
      new FileThrottleConfig(configPath + ".vrThrottling", config),
      new FileThrottleConfig(configPath + ".imageThrottling", config))
  }
}

/**
 * IsAlive check parameters
 * @param isAliveCheckInitialDelayMs Initial delay (in milliseconds) before first IsAliveCheck message is triggered.
 * @param isAliveCheckFrequencyMs Frequency (in milliseconds) for IsAliveCheck messages.
 */
case class IsAliveCheck(isAliveCheckInitialDelayMs: Int, isAliveCheckFrequencyMs: Int) {
  def this(configPath: String, config: Config) = {
    this(
      config.getReplacedInteger(configPath + ".isAliveCheckInitialDelayMs", 15000),
      config.getReplacedInteger(configPath + ".isAliveCheckFrequencyMs", 20000))
  }
}
