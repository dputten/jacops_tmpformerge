/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.common

import java.security.MessageDigest
import sun.misc.BASE64Encoder
import java.math.BigInteger
import java.io.{ InputStream, ByteArrayInputStream, FileInputStream }

/**
 * Support object for Signing, encoding and creating digests
 */
object Signing {

  def digest(data: Array[Byte], algorithm: String): String = {
    Signing.digest(new ByteArrayInputStream(data), algorithm)
  }

  def digest(fileName: String, algorithm: String): String = {
    Signing.digest(new FileInputStream(fileName), algorithm)
  }

  /**
   * Create a hash code for given file
   * @param input input stream
   * @param algorithm the algorithm used to create the hashcode
   */
  def digest(input: InputStream, algorithm: String = "SHA-256"): String = {
    val md = MessageDigest.getInstance(algorithm);
    try {
      val buffer = new Array[Byte](1024)
      Stream.continually(input.read(buffer))
        .takeWhile(_ != -1)
        .foreach(md.update(buffer, 0, _))
      var number = new BigInteger(1, md.digest)
      number.toString(16)
    } finally {
      input.close()
    }
  }

  /**
   * Encode a string using base64
   */
  def encode(value: String) = (new BASE64Encoder).encode(value.getBytes("UTF-8"))
}