/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.startup

import akka.actor._
import csc.akka.logging.DirectLogging
import csc.sitebuffer.config._
import scala.concurrent.duration._
import csc.sitebuffer.messages.{ IRoseMessage, VehicleMessage }
import csc.sitebuffer.process.images.{ ImageFilter, ImageReceiver, ImageProcessor, ImageDeleter }
import csc.sitebuffer.process.camera.{ JaiConnectionProvider, JaiCameraMonitor }
import csc.sitebuffer.process.messages._
import csc.sitebuffer.process.system.{ WasDownMonitor, TimestampFileStorage }
import csc.sitebuffer.process.send._
import csc.sitebuffer.process.irose._
import csc.sitebuffer.process.recording.{ ImageRecorder, IRoseStringToDirectoryWriterCreator, IRoseStringRecorder }
import java.io.File
import csc.sitebuffer.process.monitoring.MonitorCameraUpload
import csc.sitebuffer.process.throttling.FileThrottleActor
import csc.akka.process.ExecuteScriptActor
import csc.sitebuffer.calibration._
import csc.sitebuffer.calibration.ssh.SSHCommandsImpl
import akka.routing.RoundRobinRouter
import csc.sitebuffer.process.send.TextMessage
import csc.sitebuffer.process.recording.StartRecordingInput
import csc.sitebuffer.messages.IsAliveCheck

import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Helper object to start the SiteBuffer process
 */
object StartupSiteBuffer extends DirectLogging {
  /**
   * Create a graph of actors that comprise the site-buffer.
   *
   * The general idea is that the SiteBuffer receives all messages from the iRose and processes them. All messages
   * enter the system by the IRoseConsumer, which listens to a mina-tcp endpoint. The IRoseConsumer passes the messages
   * on as IRoseString's (includes local timestamp) to the IRoseStringParser. The messages from the iRose can
   * be divided in the following types:
   * - vehicle messages
   * - alarm messages (which are InputEvent messages with input = 8)
   * - normal input event messages (which are InputEvent messages with input <> 8)
   * - sensor messages
   * - single loop messages
   * The IRoseStringParser transforms IRoseString's to different kinds of IRoseMessage's and sends these to the
   * IRoseMessageRouter.
   *
   * For each type of IRoseMessage, the IRoseMessageRouter contains a set of recipients. The actor will route the respective
   * messages to their recipients (who will do "their thing").
   *
   * There are also actors for processing images (received from the cameras). These are copied to another directory
   * and given a name that contains its checksum. The original image is deleted.
   */
  def startSiteBuffer(config: SiteBufferConfiguration, actorSystem: ActorSystem, getCurrentTime: () ⇒ Long) {
    val anprEndpoint = config.anprEndpoint

    log.info("Starting sitebuffer")

    /*
     * Start process Image pipeline
     */

    // Construct the ImageDeleter actor that will delete all processed images.
    val imageDeleter = actorSystem.actorOf(Props(
      ImageDeleter(maxDeleteRetries = config.imageProcessing.maxDeleteRetries, minDelayBeforeRetryMs = config.imageProcessing.minDelayBeforeRetryMs)),
      "ImageDeleter")

    // Construct the ImageProcessor actor that will copy all valid jpeg images to another directory with an extended name (that contains a checksum).
    // For each processed image the actor will send a DeleteImage message to the ImageDeleter.
    val imageProcessor = actorSystem.actorOf(Props(
      new ImageProcessor(directoryPrefixForOutgoingImages = config.imageProcessing.directoryPrefixForOutgoingImages,
        recipients = Set(imageDeleter))),
      "ImageProcessor")

    val processRef = if (config.imageFilter.enable) {
      actorSystem.actorOf(Props(
        new ImageFilter(imageExtension = config.imageFilter.imageExtension,
          maxDelayTime = config.imageFilter.maxDelayTime,
          skipProcessing = Seq(imageDeleter),
          recipients = Seq(imageProcessor))),
        "ImageFilter")
    } else {
      imageProcessor
    }

    // Construct the ImageRecorder actor that will, if activated, record each incoming NewImage to disk.
    // Each incoming NewImage message will be passed on to the ImageProcessor.
    val imageRecorder = actorSystem.actorOf(Props(
      new ImageRecorder(
        baseDirectory = new File(config.inputRecording.baseDirectory),
        imageFilenamesOnly = config.inputRecording.imageFilenamesOnly,
        recipients = Set(processRef))),
      "ImageRecorder")

    /*
     * Start SystemSensor pipeline
     */
    // Construct the SystemSensorMessageSender actor. At creation, this actor receives a send function, that sends string text to
    // a BufferedTextMessageSender actor, configured with a TextlineTcpSender.
    val textOverTcpSender = actorSystem.actorOf(Props(
      new BufferedTextMessageSender(config.systemSensorMessageRetryDelayMs) with TextlineTcpSender {
        val host = config.systemSensorMessageEndpoint.host
        val port = config.systemSensorMessageEndpoint.port
      }),
      "BufferedTextMessageSender")
    val sendTextToTextlineTcpSender = (text: String) ⇒ textOverTcpSender ! TextMessage(text)
    val systemSensorMessageSender = actorSystem.actorOf(Props(
      new SystemSensorMessageSender(send = sendTextToTextlineTcpSender)),
      "SystemSensorMessageSender")

    // Create a SystemSensorMessageFilter actor, that will filter repeating messages within the given filterWindowMs.
    // Messages are passed on to the SystemSensorMessageSender.
    val systemSensorMessageFilter = actorSystem.actorOf(Props(
      new SystemSensorMessageFilter(
        filterWindowMs = config.repeatingSystemSensorMessagesFilterWindowMs,
        getCurrentTime = getCurrentTime,
        nextActor = systemSensorMessageSender)),
      "SystemSensorMessageFilter")

    /*
     * Create all the different monitor actors, which monitor if certain SystemSensorMessages should be generated.
     */

    // This actor creates a SystemSensorMessage for failed ftp uploads of the camera
    val monitorCameraUpload = actorSystem.actorOf(Props(
      new MonitorCameraUpload(config.cameraConfigs.values.toSeq, systemSensorMessageFilter)))

    // Generated SystemSensorMessages are passed on to the SystemSensorMessageFilter actor.
    // (except for the IRoseAlarmMonitor actor, which sends its messages direct to SystemSensorMessageSender)
    val alarmMonitor = actorSystem.actorOf(Props(
      new IRoseAlarmMonitor(systemSensorMessageSender)),
      "IRoseAlarmMonitor")

    val digitalInputMonitor = actorSystem.actorOf(Props(
      new IRoseDigitalInputMonitor(
        cabinetDoorConfig = config.cabinetDoorConfig,
        systemSensorMessageSender = systemSensorMessageFilter)),
      "IRoseDigitalInputMonitor")

    val sensorFailureMonitor = actorSystem.actorOf(Props(
      new IRoseSensorFailureMonitor(config, systemSensorMessageFilter)),
      "IRoseSensorFailureMonitor")

    val clockMonitor = actorSystem.actorOf(Props(
      new IRoseClockMonitor(
        outOfSyncMarginMs = config.clockOutOfSyncMarginMs,
        getCurrentTime = getCurrentTime,
        systemSensorMessageSender = systemSensorMessageFilter)),
      "IRoseClockMonitor")

    val isAliveMonitor = actorSystem.actorOf(Props(
      new IRoseIsAliveMonitor(systemSensorMessageFilter)),
      "IRoseIsAliveMonitor")

    val executeScriptActor = actorSystem.actorOf(Props[ExecuteScriptActor])
    val loopFailureMonitor = actorSystem.actorOf(Props(
      new IRoseLoopFailureMonitor(
        maxAllowedMessages = config.loopFailure.maxAllowedMessages,
        testWindowMs = config.loopFailure.testWindowMs,
        iRoseRebootScriptPath = config.loopFailure.iRoseRebootScriptPath,
        getCurrentTime = getCurrentTime,
        systemSensorMessageSender = systemSensorMessageFilter,
        executeScriptActor = executeScriptActor)),
      "IRoseLoopFailureMonitor")

    val iRoseLoopStateMonitor = actorSystem.actorOf(Props(
      new IRoseLoopStateMonitor(
        iroseCusStatParser = new IRoseCusStatParser,
        executeScriptActor = executeScriptActor,
        systemSensorMessageSender = systemSensorMessageFilter,
        cusStatusIRosePath = config.loopState.cusStatusIRosePath,
        getCurrentTime = getCurrentTime)),
      "IRoseLoopStateMonitor")

    actorSystem.scheduler.schedule(config.loopState.checkFrequencyMs, config.loopState.checkFrequencyMs, iRoseLoopStateMonitor, "tick")

    // Create a CameraMonitor for each configured camera.
    // A CameraMonitor receives messages from a camera and sends created SystemSensorMessages to the SystemSensorMessageFilter actor.
    if (config.cameraMonitorConfig.enabled) {
      config.cameraConfigs.map {
        case (aCameraId, aCameraConfig) ⇒
          actorSystem.actorOf(Props(
            new JaiCameraMonitor(
              cameraId = aCameraId,
              cameraMonitorConfig = config.cameraMonitorConfig,
              getCurrentTime = getCurrentTime,
              systemSensorMessageSender = systemSensorMessageFilter) with JaiConnectionProvider {
              val cameraId = aCameraId
              val cameraConfig = aCameraConfig
            }), "JaiCameraMonitor-%s".format(aCameraId))
      }.toSet
    }

    // This actor writes VehicleMessages as JSON to a directory (for Vehicle Registration)
    val vrProducerActor = actorSystem.actorOf(Props(
      new JSONProducerActor(config.vrEndpoint.directory)),
      "VR-ProducerActor")

    // This actor creates a SystemSensorMessage for invalid VehicleMessages
    val invalidVehicleMessageMonitor = actorSystem.actorOf(Props(
      new InvalidVehicleMessageMonitor(
        minimalSpeedForProcessingInvalidVehicleMessageKmh = config.minimalSpeedForProcessingInvalidVehicleMessageKmh,
        systemSensorMessageSender = systemSensorMessageSender)),
      "InvalidVehicleMessageMonitor")

    // This actor creates a SystemSensorMessage for failed VehicleMessages
    val failedVehicleMessageMonitor = actorSystem.actorOf(Props(
      new FailedVehicleMessageMonitor(getCurrentTime = getCurrentTime, systemSensorMessageSender = systemSensorMessageSender)),
      "FailedVehicleMessageMonitor")

    /*
      This filter actor sends ALL  VehicleMessages to vrProducerActor
      See FC-455: due to calibration messages not being identified correctly, we now pass all vehiclemessages
      and in the preselector we extract the real calibration messages
     */
    val passThroughAllVehicleMessages = (message: VehicleMessage) ⇒ true // necessary to not break the VehicleMessageFilter class AND we want all (valid & inValid) passages, see FC-455
    val invalidVehicleMessageFilterActor = actorSystem.actorOf(Props(
      new VehicleMessageFilter(filterCondition = passThroughAllVehicleMessages, trueRecipients = Set(vrProducerActor), falseRecipients = Set(invalidVehicleMessageMonitor))),
      "invalidVehicleMessageFilter")

    // This filter actor sends good VehicleMessages to invalidVehicleMessageFilterActor and failed ones to failedVehicleMessageMonitor
    val vehicleMessageIsGood = (message: VehicleMessage) ⇒ message.speed > 0.0
    val failedVehicleMessageFilterActor = actorSystem.actorOf(Props(
      new VehicleMessageFilter(filterCondition = vehicleMessageIsGood, trueRecipients = Set(invalidVehicleMessageFilterActor), falseRecipients = Set(failedVehicleMessageMonitor))),
      "failedVehicleMessageFilter")

    // Create the recipients of the vehicle messages.
    val hasANPR = config.anprEndpoint.directory.length() > 0

    val vehicleMessageRecipients = if (hasANPR) {
      log.info("ANPR enabled to endpoint:" + anprEndpoint)
      // VehicleMessages used for both the VehicleRegistration and ANPR
      Set[ActorRef](
        failedVehicleMessageFilterActor,
        actorSystem.actorOf(Props(new JSONProducerActor(config.anprEndpoint.directory)), "ANPR-ProducerActor"))
    } else {
      log.info("ANPR disabled")
      // VehicleMessages only used for VehicleRegistration
      Set[ActorRef](
        failedVehicleMessageFilterActor)
    }

    val calibrateVehicleTap = actorSystem.actorOf(
      Props(
        new CalibrationRegistrationTap(config.calibration.speedThreshold,
          recipients = vehicleMessageRecipients)), "vehicleTap")

    // Create the IRoseMessageRouter, passing all the different sets of recipients.
    val iRoseMessageRouter = actorSystem.actorOf(
      Props(
        new IRoseMessageRouter(
          vehicleMessageRecipients = Set(calibrateVehicleTap, monitorCameraUpload),
          alarmEventRecipients = Set(alarmMonitor),
          inputEventMessageRecipients = Set(digitalInputMonitor),
          sensorMessageRecipients = Set(sensorFailureMonitor, isAliveMonitor, clockMonitor),
          singleLoopMessageRecipients = Set(loopFailureMonitor))),
      "IRoseMessageRouter")

    // This actor creates a SystemSensorMessage for '1970' IRoseMessages
    val iRoseNtpFailureMonitor = actorSystem.actorOf(Props(
      new IRoseNtpFailureMonitor(
        maxAllowedMessages = config.ntpFailure.maxAllowedMessages,
        testWindowMs = config.ntpFailure.testWindowMs,
        getCurrentTime = getCurrentTime,
        systemSensorMessageSender = systemSensorMessageFilter)),
      "IRoseNtpFailureMonitor")

    // This filter actor sends IRoseMessages from 1970 to iRoseNtpFailureMonitor and all other ones to iRoseMessageRouter
    val firstDayOf1971 = 31536000000L
    def in1970(epochTime: Long): Boolean = epochTime < firstDayOf1971

    val iRoseMessageIn1970 = (message: IRoseMessage) ⇒ in1970(message.dateTime)
    val in1970IRoseMessageFilterActor = actorSystem.actorOf(Props(
      new IRoseMessageFilter(
        filterCondition = iRoseMessageIn1970,
        trueRecipients = Set(iRoseNtpFailureMonitor),
        falseRecipients = Set(iRoseMessageRouter))),
      "in1970IRoseMessageFilter")

    // This actor transforms an incoming IRoseString to one of the different IRoseMessage's.
    val iRoseStringParser = actorSystem.actorOf(Props(
      new IRoseStringParser(
        transform = IRoseTransformer.transform,
        recipients = Set(in1970IRoseMessageFilterActor))),
      "IRoseStringParser")

    // This actor will, if activated, record each incoming IRoseString to disk.
    val iRoseStringRecorder = actorSystem.actorOf(Props(
      new IRoseStringRecorder with IRoseStringToDirectoryWriterCreator {
        def getBaseDirectory: File = {
          new File(config.inputRecording.baseDirectory)
        }
      }),
      "IRoseStringRecorder")

    // Give iRoseStringRecorder time to start so it is ready to receive the StartRecordingInput next.
    Thread.sleep(1000)

    // Both IRoseStringRecorder and ImagesRecorder have been created.
    // Check if recording should start. (Currently recording can only start at startup, based on setting in config.)
    if (config.inputRecording.enabled) {
      actorSystem.eventStream.publish(StartRecordingInput(recordingId = "%d".format(getCurrentTime())))
    }

    //=============
    // With the construction of the next 2 actors, the SiteBuffer will start handling input: iRose messages and images.
    //=============

    val calibrateImageTap = actorSystem.actorOf(
      Props(new CalibrationImagesTap(recipients = Set(imageRecorder))), "imageTap")

    // Construct the ImageReceiver that will receive image paths over TCP for all new images. For each path the actor will send a NewImage message
    // to the ImageProcessor.
    // (These are sent by a ftplet)
    val imageReceiver = actorSystem.actorOf(Props(
      new ImageReceiver(imagesEndpointHost = config.imageProcessing.newImagesEndpoint.host,
        imagesEndpointPort = config.imageProcessing.newImagesEndpoint.port,
        getCurrentTime = getCurrentTime,
        recipients = Set(calibrateImageTap, monitorCameraUpload))),
      "ImageReceiver")

    // Create the IRoseConsumer, which listens to a port for incoming messages from the iRose.
    // Each message is timestamped, put into an IRoseString and send to all recipients.
    val iRoseStringsRecipients = Set(iRoseStringParser, iRoseStringRecorder)
    actorSystem.actorOf(
      Props(
        new IRoseConsumer(
          iRoseEndpointHost = config.iRoseEndpoint.host,
          iRoseEndpointPort = config.iRoseEndpoint.port,
          getCurrentTime = getCurrentTime,
          recipients = iRoseStringsRecipients)),
      "IRoseConsumer")

    // This actor checks if the SiteBuffer was down (for too long). Afterwards it will regularly update a timestamp file.
    actorSystem.actorOf(Props(
      new WasDownMonitor(
        timestampUpdateFrequency = config.wasDown.timestampUpdateFrequency,
        minimalDeviationForWasDown = config.wasDown.minimalDeviationForWasDown,
        getCurrentTime = getCurrentTime,
        systemSensorMessageSender = systemSensorMessageSender) with TimestampFileStorage {
        val timestampDirectory = config.wasDown.timestampDirectory
      }),
      "WasDownMonitor")

    // Create fileThrottler for vehicleMessages
    actorSystem.actorOf(Props(
      new FileThrottleActor(config.throttling.vrThrottling)))

    // Create fileThrottler for vehicleImages
    actorSystem.actorOf(Props(
      new FileThrottleActor(config.throttling.imageThrottling)))

    // Schedule the IsAliveCheck message for the isAliveMonitor
    actorSystem.scheduler.schedule(config.isAliveCheck.isAliveCheckInitialDelayMs millis, config.isAliveCheck.isAliveCheckFrequencyMs millis, isAliveMonitor, IsAliveCheck())

    /*
     * Create Gantry calibration
     */
    val calibrationTextOverTcpSender = actorSystem.actorOf(Props(
      new BufferedTextMessageSender(config.calibration.msgRetryDelayMs) with TextlineTcpSender {
        val host = config.calibration.centralHost
        val port = config.calibration.centralport
      }),
      "CalibrationMessageSender")

    val calibrationResponseSender = actorSystem.actorOf(Props(
      new CalibrationCreateTextResult(config.calibration.maxMessageLength, Set(calibrationTextOverTcpSender))), "CalibrationCreateText")
    val cameraDif = actorSystem.actorOf(Props(
      new CameraTimeDifference()).withRouter(RoundRobinRouter(nrOfInstances = config.cameraConfigs.size + 2)), "CameraTimeDifference")
    val calibrate = actorSystem.actorOf(Props(
      new GantryCalibration(calibrationCfg = config.calibration,
        cameraConfigs = config.cameraConfigs,
        imageTap = calibrateImageTap,
        registrationTap = calibrateVehicleTap,
        cameraTimeDiff = cameraDif,
        commands = new SSHCommandsImpl(),
        resultSender = calibrationResponseSender)), "GantryCalibration")
    actorSystem.actorOf(Props(
      new CalibrationConsumer(
        calibrationEndpointHost = config.calibration.consumerHost,
        calibrationEndpointPort = config.calibration.consumerPort,
        recipients = Set(calibrate))), "CalibrateConsumer")

    log.info("SiteBuffer started")
  }
}

