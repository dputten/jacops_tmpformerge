/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.startup

import csc.akka.logging.DirectLogging
import akka.kernel.Bootable
import csc.sitebuffer.config.SiteBufferConfiguration
import akka.actor.ActorSystem
import java.util.Date

/**
 * Boot class Starts Sitebuffer
 */
private[startup] class Boot extends Bootable with DirectLogging {
  val actorSystem = ActorSystem("SiteBuffer")

  def startup = {
    log.info("Starting SiteBuffer")
    StartupSiteBuffer.startSiteBuffer(config = SiteBufferConfiguration(), actorSystem = actorSystem, getCurrentTime = () ⇒ new Date().getTime)
  }

  def shutdown = {
    (new ShutdownHook(actorSystem)) run ()
  }
}
