/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.calibration.ssh

import fr.janalyse.ssh.{ NoPassword, SSHPassword, SSH, SSHOptions }
import csc.sitebuffer.calibration.ComponentCertificate
import csc.sitebuffer.process.irose.IRoseLoopError

class SSHCommandsImpl extends SSHCommands {
  private var sshConnection: Option[SSH] = None

  def connect(host: String, port: Int, user: String, pwd: String, timeout: Int) {
    if (!sshConnection.isEmpty) {
      disconnect()
    }

    val options = SSHOptions(host, user, SSHPassword(Some(pwd)), NoPassword, None, port, None, timeout)
    //val ssh = new SSH(SSHOptions(username = user, password = pwd, port = port, timeout = timeout, noneCipher = false)(host = host))
    val ssh = new SSH(options)

    sshConnection = Option(ssh)
  }
  def disconnect() {
    sshConnection.foreach(_.close)
    sshConnection = None
  }

  def getIRoseCertificates: CommandResult[List[ComponentCertificate]] = {
    sshConnection match {
      case Some(ssh) ⇒ CommandCertificates.getCertificates(ssh)
      case None      ⇒ throw new IllegalStateException("Not SSH connection")
    }
  }

  def getIRoseLoopErrors: CommandResult[List[IRoseLoopError]] = {
    sshConnection match {
      case Some(ssh) ⇒ CommandLoopStatus.getLoopErrors(ssh)
      case None      ⇒ throw new IllegalStateException("Not SSH connection")
    }
  }

  def getIRoseTimeDifference: CommandResult[TimeDifference] = {
    sshConnection match {
      case Some(ssh) ⇒ CommandTimeDifference.getIRoseTimeDifference(ssh)
      case None      ⇒ throw new IllegalStateException("Not SSH connection")
    }
  }

  def calibrateLane(detectorId: Int): CommandResult[Boolean] = {
    sshConnection match {
      case Some(ssh) ⇒ CommandCalibrateLane.calibrateLane(ssh, detectorId)
      case None      ⇒ throw new IllegalStateException("Not SSH connection")
    }
  }
}