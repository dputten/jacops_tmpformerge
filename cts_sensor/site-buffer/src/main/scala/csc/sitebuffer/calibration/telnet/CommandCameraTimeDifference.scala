/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.calibration.telnet

import org.apache.commons.net.telnet.TelnetClient
import java.io._
import collection.mutable.ListBuffer
import csc.sitebuffer.calibration.ssh.TimeDifference
import csc.sitebuffer.calibration.ssh.CommandResult
import org.slf4j.LoggerFactory
import java.nio.CharBuffer

object CommandCameraTimeDifference {
  val log = LoggerFactory.getLogger(getClass)
  val promt = "root@TS(C)-5000EN"

  def retrieveTimeDifference(host: String, port: Int = 23, user: String, pwd: String): CommandResult[TimeDifference] = {
    log.info("Telnet: Connect to %s:%d".format(host, port))
    val telnet = new TelnetClient()
    try {
      telnet.connect(host, port)
      val inputReader = new InputStreamReader(telnet.getInputStream)
      val writer = new OutputStreamWriter(telnet.getOutputStream)
      Thread.sleep(1000)
      val connectOutput = readAll(inputReader)
      //check connection
      checkConnectOutput(connectOutput) match {
        case Some(error) ⇒ {
          CommandResult[TimeDifference](List(error), TimeDifference(0L, 0L))
        }
        case None ⇒ {
          writer.write(user + "\n")
          writer.flush()
          Thread.sleep(1000)
          writer.write(pwd + "\n")
          writer.flush()
          Thread.sleep(1000)
          try {
            readUntilPromt(inputReader)
          } catch {
            case ex: IOException if (ex.getMessage == "Login incorrect") ⇒ {
              //try to login again
              log.warn("failed to login: retry")
              writer.write(user + "\n")
              writer.flush()
              writer.write(pwd + "\n")
              writer.flush()
              Thread.sleep(1000)
              readUntilPromt(inputReader)
            }
          }
          //don't know what to check
          writer.write("ntpq -p\n")
          writer.flush()
          Thread.sleep(1000)
          val ntpqOutput = readUntilPromt(inputReader)
          parseNtpqOutput(ntpqOutput)
        }
      }
    } catch {
      case ex: Exception ⇒ {
        val msg = "Failed to get time difference of camera %s".format(host)
        log.error(msg, ex)
        CommandResult[TimeDifference](List(msg), TimeDifference(0L, 0L))
      }
    } finally {
      telnet.disconnect()
    }
  }

  def readUntilPromt(reader: Reader): Seq[String] = {
    val buffer = CharBuffer.allocate(1024)
    val stringBuffer = new StringBuffer()
    var nrRead = 0
    var done = false
    do {
      do {
        nrRead = reader.read(buffer)
        if (nrRead > 0) {
          val readStr = new String(buffer.array(), 0, nrRead).replace("\r", "")
          stringBuffer.append(readStr)
        } else {
          Thread.sleep(1000)
        }
        buffer.clear()
      } while (nrRead >= 1024)
      //Do we have all output
      val recvStr = stringBuffer.toString
      if (recvStr.contains(promt)) {
        done = true
      }
      if (recvStr.contains("Login incorrect")) {
        throw new IOException("Login incorrect")
      }
      if (recvStr.contains("Login timed out")) {
        throw new IOException("Login timed out")
      }
    } while (!done)
    stringBuffer.toString.split("\n")
  }

  def readAll(reader: Reader): Seq[String] = {
    val buffer = CharBuffer.allocate(1024)
    val stringBuffer = new StringBuffer()
    var nrRead = 0
    do {
      nrRead = reader.read(buffer)
      val readStr = new String(buffer.array(), 0, nrRead).replace("\r", "")
      stringBuffer.append(readStr)
      buffer.clear()
    } while (nrRead >= 1024)

    val output = stringBuffer.toString.split("\n").toSeq
    output
  }

  def parseNtpqOutput(lines: Seq[String]): CommandResult[TimeDifference] = {
    log.info("Start parsing [%s]".format(lines))
    /* output example
remote           refid      st t when poll reach   delay   offset  jitter
==============================================================================
*10.109.22.14    .PPSa.           1 u  511 1024  377    0.197   -0.623   0.213
*/
    val reg = """(.)(\S+)\s+(\S+)\s+(\d+)\s+\S\s+[\S]+\s+\d+\s+([0-7]+)\s+[-\d.]+\s+([-\d.])+\s+[-\d.]+""".r
    val header = """\s+remote[\s]+refid[\s]+st[\s]+t[\s]+when[\s]+poll[\s]+reach[\s]+delay[\s]+offset[\s]+jitter""".r
    val headerLine = """=+""".r

    val results = lines.map(line ⇒ {
      line match {
        case reg(tallyCode, remote, refId, stratum, reach, offset) ⇒ {
          val offsetValue = (offset.toFloat).round
          Right(tallyCode, offsetValue)
        }
        case header()                   ⇒ Left("") //nothing to do
        case headerLine()               ⇒ Left("") //nothing to do
        case l if (l.contains(promt))   ⇒ Left("") //nothing to do
        case "ntpq -p"                  ⇒ Left("") //nothing to do
        case l if (l.contains("Login")) ⇒ Left("") //nothing to do
        case other                      ⇒ Left("Parsing Failed for line [%s]".format(line))
      }
    })
    val split = results.groupBy(_ match {
      case Left("") ⇒ "empty"
      case Left(_)  ⇒ "errors"
      case Right(_) ⇒ "timeDiff"
    })

    var errors = split.getOrElse("errors", List()).map(_.left.get)
    val time = split.getOrElse("timeDiff", List()).map(_.right.get)
    if (time.isEmpty) {
      log.error("Telnet-parse: no offset found")
      errors = errors :+ "Couldn't find time offset"
    }
    val result = time.find(_._1 == "*").orElse(time.find(_._1 == "+")).orElse(time.find(_._1 != "")).orElse(time.lastOption)
    val timeDiff = result.map { case (tally, offset) ⇒ TimeDifference(System.currentTimeMillis(), offset) }.getOrElse(TimeDifference(0L, 0L))

    CommandResult[TimeDifference](errors.toList, timeDiff)
  }

  def checkConnectOutput(connectOutput: Seq[String]): Option[String] = {
    connectOutput.lastOption match {
      case Some(line) ⇒ if (line.contains("login")) {
        None
      } else {
        log.error("Unecpected output [%s]".format(connectOutput.mkString("\n")))
        Some("Unecpected last line [%s]".format(line))
      }
      case None ⇒ Some("No output while connecting")
    }

  }

}

/* output telnet:

[ctes@ctes-eg100-shoebox ~]$ telnet 192.168.2.20
Trying 192.168.2.20...
Connected to 192.168.2.20.
Escape character is '^]'.

 _____                    _____           _         _
|  _  |___ ___ ___ ___   |  _  |___ ___  |_|___ ___| |_
|     |  _| .'| . | . |  |   __|  _| . | | | -_|  _|  _|
|__|__|_| |__,|_  |___|  |__|  |_| |___|_| |___|___|_|
              |___|                    |___|

Arago Project http://arago-project.org TS(C)-5000EN

Arago 2010.11 TS(C)-5000EN

login: root
Password:
root@TS(C)-5000EN:~# ntpq -p
     remote           refid      st t when poll reach   delay   offset  jitter
==============================================================================
*192.168.2.2     20.133.70.196    4 u    6   32  377    0.173   -0.064   0.011


  echo open 192.168.2.20
  sleep 2
  echo "root"
  sleep 1
  echo "JAIPULNiX"
  sleep 1
  echo "ntpq -p"
  sleep 20
  ) | telnet | sed  '/^root@/,/^$/!d' | log "CAMERA"

 */ 