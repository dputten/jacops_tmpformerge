/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.calibration

case class ComponentCertificate(name: String, checksum: String)
case class SerialNumber(serialNumber: String,
                        serialNumberPrefix: String = "",
                        serialNumberLength: Int = 5)

case class GantryCalibrationRequest(systemId: String, gantryId: String, time: Long,
                                    reportingOfficerCode: String,
                                    mustCertificates: Seq[ComponentCertificate],
                                    serialNr: SerialNumber)

case class GantryCalibrationResponse(systemId: String, gantryId: String, time: Long,
                                     reportingOfficerCode: String,
                                     serialNr: String,
                                     activeCertificates: Seq[ComponentCertificate],
                                     images: Seq[CalibrationImage],
                                     results: Seq[StepResult])

case class CalibrationImage(name: String, imageId: String, checksum: String, image: Seq[Int]) {
  override def toString = """{name: %s, imageId: %s, checksum: %s, image: ... }""".format(name, imageId, checksum)
}

case class StepResult(stepName: String, success: Boolean, detail: Option[String])
object StepResult {
  val STEP_Certificates = "certificates"
  val STEP_Loops = "Loops"
  val STEP_VehicleRecords = "vehicle"
  val STEP_Camera = "camera"
  val STEP_NTP = "NTP"

  val allSteps = Seq(STEP_Certificates, STEP_Loops, STEP_VehicleRecords, STEP_Camera, STEP_NTP)
}