/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.calibration

import akka.actor.{ ActorRef, ActorLogging, Actor }
import akka.event.LoggingReceive
import net.liftweb.json.{ DefaultFormats, Serialization }
import csc.sitebuffer.process.send.TextMessage
import csc.akka.remote.wire.Protocol

class CalibrationCreateTextResult(maxMessageLength: Int, recipients: Set[ActorRef], implicit val formats: DefaultFormats = DefaultFormats) extends Actor with ActorLogging with Protocol {
  override def preRestart(reason: Throwable, message: Option[Any]) {
    log.error(reason, "Restart while processing %s".format(message.toString))
    super.preRestart(reason, message)
  }

  def receive = LoggingReceive {
    case resp: AnyRef ⇒ {
      val json = Serialization.write(resp)
      log.info("send message: " + resp.toString)
      val msgLines = makeFragments(json.getBytes, maxMessageLength)
      log.info("send %s lines".format(msgLines.size))
      for (line ← msgLines) {
        val msg = TextMessage(line)
        recipients.foreach(_ ! msg)
      }
    }
  }

}