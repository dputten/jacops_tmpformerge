/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.calibration.checks

import org.slf4j.LoggerFactory
import csc.sitebuffer.calibration.ssh.SSHCommands
import csc.sitebuffer.calibration.GetRegistrationResponse

object CalibrationTrigger {
  val log = LoggerFactory.getLogger(getClass)

  def triggerLoops(commands: SSHCommands, detectors: Seq[Int]) {
    log.info("Start Trigger iRose Calibration")

    for (detector ← detectors) {
      val result = commands.calibrateLane(detector)
      if (!result.errors.isEmpty) {
        //something went wrong
        log.warn("Failed to trigger lane %d: %s".format(detector, result.errors.mkString(", ")))
      }
    }
    log.info("Finished Trigger iRose Calibration")
  }

  def retriggerLoopsCheck(commands: SSHCommands, detectors: Seq[Int], vehicles: Seq[GetRegistrationResponse],
                          host: String, port: Int, user: String, pwd: String, timeout: Int) {
    val nrDetectors = detectors.size
    val regDone = vehicles.size == nrDetectors
    log.debug("Registrations %d => %s".format(vehicles.size, if (regDone) "Ready" else "Waiting"))
    if (!regDone) {
      try {
        commands.connect(host, port, user, pwd, timeout)
        detectors.foreach(currentDetector ⇒ {
          if (!vehicles.exists(_.detector == currentDetector)) {
            log.info("RetriggerRegistration for detector %d".format(currentDetector))
            val result = commands.calibrateLane(currentDetector)
            if (!result.errors.isEmpty) {
              //something went wrong
              log.warn("Failed to trigger lane %d: %s".format(currentDetector, result.errors.mkString(", ")))
            }
          }
        })
      } finally {
        commands.disconnect()
      }
    }
  }

}