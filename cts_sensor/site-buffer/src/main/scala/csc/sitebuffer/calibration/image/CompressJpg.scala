/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.calibration.image

import java.io.{ ByteArrayOutputStream, FileInputStream, File }
import javax.imageio.{ IIOImage, ImageWriteParam, ImageIO }
import collection.JavaConverters._

object CompressJpg {

  def readAndCompress(jpg: File, compression: Int = 100): Array[Byte] = {
    val input = new FileInputStream(jpg)
    val image = ImageIO.read(input)

    // Find a jpeg writer
    val writer = {
      val writerList = ImageIO.getImageWritersByFormatName("jpg").asScala
      if (writerList.isEmpty) {
        throw new IllegalArgumentException("Couldn't find writer for imageType jpg")
      } else {
        writerList.next()
      }
    }

    // Prepare output file
    val outputStream = new ByteArrayOutputStream()
    val ios = ImageIO.createImageOutputStream(outputStream)
    try {
      writer.setOutput(ios)

      // Set the compression quality
      val iwparam = writer.getDefaultWriteParam()
      iwparam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT)
      iwparam.setCompressionQuality(compression.toFloat / 100)
      // Write the image
      writer.write(null, new IIOImage(image, null, null), iwparam)
    } finally {
      // Cleanup
      try {
        ios.flush()
      } finally {
        try {
          writer.dispose()
        } finally {
          ios.close()
        }
      }
    }
    outputStream.toByteArray
  }
}