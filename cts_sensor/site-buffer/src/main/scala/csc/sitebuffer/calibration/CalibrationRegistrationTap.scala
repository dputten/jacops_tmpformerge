/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.calibration

import csc.sitebuffer.messages.VehicleMessage
import akka.actor.{ ActorLogging, Actor, ActorRef }
import akka.event.LoggingReceive

case class GetRegistrationRequest(refId: String, detector: Int)
case class GetRegistrationResponse(refId: String, detector: Int, registration: VehicleMessage)

/**
 * This actor passes allVehicleMessage except for the calibration messages
 * And when a request are waiting the VehicleMessage is send to the requestor
 * @param calibrationSpeedThreshold the speed threshold when the message is a calibration
 * @param recipients the next process step in processing the VehicleMessage
 */
class CalibrationRegistrationTap(calibrationSpeedThreshold: Float, recipients: Set[ActorRef]) extends Actor with ActorLogging {

  var requests = Seq[(ActorRef, GetRegistrationRequest)]()

  override def preRestart(reason: Throwable, message: Option[Any]) {
    log.error(reason, "Restart while processing %s".format(message.toString))
    super.preRestart(reason, message)
  }

  def receive = LoggingReceive {
    case req: GetRegistrationRequest ⇒ {
      log.info("%s: Received request for detector %d".format(req.refId, req.detector))
      requests = requests :+ (sender, req)
    }
    case registration: VehicleMessage ⇒ {
      if (!requests.isEmpty) {
        processVehicleMessage(registration)
      }
      if (isCalibration(registration)) {
        log.info("Received calibration for detector %d".format(registration.detector))
      } else {
        sendToRecipients(registration)
      }
    }
  }

  private def sendToRecipients(message: Any) {
    for (recipient ← recipients) {
      recipient ! message
    }
  }

  private def processVehicleMessage(msg: VehicleMessage) {
    val found = requests.filter(_._2.detector == msg.detector)
    found.foreach {
      case (requestor, req) ⇒ {
        log.info("%s: Received message for detector %d".format(req.refId, req.detector))
        requestor ! GetRegistrationResponse(req.refId, req.detector, msg)
        requests = requests.filterNot(_._2.detector == msg.detector)
      }
    }
  }

  private def isCalibration(msg: VehicleMessage): Boolean = {
    !msg.valid && msg.speed > calibrationSpeedThreshold
  }

}