/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.calibration.ssh

import csc.sitebuffer.process.irose.{ IRoseLoopError, IRoseCusStatParser }
import fr.janalyse.ssh.SSH

object CommandLoopStatus {
  val iroseCusStatParser = new IRoseCusStatParser

  def getLoopErrors(sshConnection: SSH): CommandResult[List[IRoseLoopError]] = {
    val result = sshConnection.execOnceAndTrim("cus status")
    if (result == null) {
      CommandResult[List[IRoseLoopError]](List("Could not execute command: cus status"), List())
    } else {
      val errors = iroseCusStatParser.getIRoseLoopErrors(result)
      CommandResult(List(), errors.toList)
    }
  }

}