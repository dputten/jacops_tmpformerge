/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.calibration.checks

import csc.sitebuffer.calibration.{ CalibrationState, StepResult, ComponentCertificate }
import org.slf4j.LoggerFactory
import csc.sitebuffer.calibration.ssh.SSHCommands

object CertificateCheck {
  val log = LoggerFactory.getLogger(getClass)

  def check(state: CalibrationState, commands: SSHCommands, mustCertificates: Seq[ComponentCertificate]): CalibrationState = {
    log.info("start Certificate check")
    val result = commands.getIRoseCertificates

    val activeCertList = result.result
    val errorComponents = activeCertList.foldLeft(Seq[String]()) {
      case (errorComp, current) ⇒ {
        mustCertificates.find(_.name == current.name) match {
          case Some(must) ⇒ {
            //check certificate
            if (must.checksum == current.checksum) {
              //it OK
              errorComp
            } else {
              //check failed
              errorComp :+ current.name
            }
          }
          case None ⇒ {
            //certificat not part of mustList so skip
            errorComp
          }
        }
      }
    }
    if (errorComponents.isEmpty && result.errors.isEmpty) {
      //test succeeded
      //create stepResult
      val step = StepResult(StepResult.STEP_Certificates, true, None)
      //update state
      log.info("Finished Certificate check successful")
      state.copy(stepResults = state.stepResults :+ step, certificates = activeCertList)
    } else {
      //test failed
      var errorMsg = ""
      if (!result.errors.isEmpty) {
        errorMsg = "Errors getting Certificates: [" + result.errors.mkString(", ") + "] "
        log.error(errorMsg)
      }
      if (!errorComponents.isEmpty) {
        val msg = "Failed components: [" + errorComponents.mkString(", ") + "]"
        errorMsg += msg
        log.error(msg)
      }

      val step = StepResult(StepResult.STEP_Certificates, false, Some(errorMsg))
      //update state
      log.info("Finished Certificate check with errors")
      state.copy(stepResults = state.stepResults :+ step, certificates = activeCertList)
    }
  }

}