/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.calibration.ssh

import org.slf4j.LoggerFactory
import fr.janalyse.ssh.SSH
import java.util.logging.SimpleFormatter
import java.text.{ ParseException, SimpleDateFormat }

case class TimeDifference(measureTime: Long, timeDifference: Long)

object CommandTimeDifference {
  private val log = LoggerFactory.getLogger(getClass)
  private val timeParser = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy")

  //Thu May 28 14:17:26 UTC 2015: -0.000145 (mode -j)
  private val NTP_record_re = ("""(.*):\s+([0-9\-.]+)\s+\(.*\)\Z""").r

  def getIRoseTimeDifference(sshConnection: SSH): CommandResult[TimeDifference] = {
    val command = "tail -5  /var/log/sntplog"
    val result = sshConnection.execOnceAndTrim(command)
    if (result == null) {
      CommandResult(List("Could not execute command [%s]".format(command)), TimeDifference(0L, 0L))
    } else {
      val lines = result.split("\n").toList
      val parseResult = parseRawRecords(lines)
      val (errors, diff) = tupleWithErrorsAndTimeDiff(parseResult)
      CommandResult(errors, diff.lastOption.getOrElse(TimeDifference(0L, 0L)))
    }
  }

  /**
   *
   * @param rawNTPLog, result from iRose
   * @return List of Left(String) if the string could not be parsed or Right(timediff in msec)
   */
  def parseRawRecords(rawNTPLog: List[String]): List[Either[String, TimeDifference]] = {

    rawNTPLog.map(aString ⇒ {
      aString match {
        case NTP_record_re(measurementTime, timeDiff) ⇒ {
          try {
            val diffMs = (timeDiff.toFloat * 1000).round
            val time = timeParser.parse(measurementTime)
            Right(TimeDifference(time.getTime, diffMs))
          } catch {
            case ex: ParseException ⇒ {
              val msg = "Failed to parse date in line: %s".format(aString)
              Left(msg)
            }
            case ex: NumberFormatException ⇒ {
              val msg = "Failed to create Ms of value %s".format(timeDiff)
              Left(msg)
            }
            case ex: Exception ⇒ {
              val msg = "%s while processing line: %s".format(ex.getMessage, aString)
              Left(msg)
            }
          }
        }
        case _ ⇒
          Left(aString)
      }
    })

  }

  /**
   * Split the list with errors and certificates in a tuple (List[String], List[ComponentCertificate])
   * @param xs list containing errors and certificates
   * @return
   */
  private def tupleWithErrorsAndTimeDiff(xs: List[Either[String, TimeDifference]]): (List[String], List[TimeDifference]) = {
    val split = xs.groupBy(_ match {
      case Left(_)  ⇒ "errors"
      case Right(_) ⇒ "timeDiff"
    })

    (split.getOrElse("errors", List()).map(_.left.get), split.getOrElse("timeDiff", List()).map(_.right.get))
  }

}

/*

 echo admin |$SSHASK ssh  -o "StrictHostKeyChecking no" root@192.168.1.10 tail -10  /var/log/sntplog

# tail -10  /var/log/sntplog
Thu May 28 14:17:26 UTC 2015: -0.000145 (mode -j)
Thu May 28 14:17:56 UTC 2015: -0.000245 (mode -j)
Thu May 28 14:18:26 UTC 2015: -0.000202 (mode -j)
Thu May 28 14:18:57 UTC 2015: -0.000258 (mode -j)
Thu May 28 14:19:27 UTC 2015: -0.000003 (mode -j)
Thu May 28 14:19:57 UTC 2015: -0.000352 (mode -j)
Thu May 28 14:20:27 UTC 2015: -0.000229 (mode -j)
Thu May 28 14:20:57 UTC 2015: -0.000219 (mode -j)
Thu May 28 14:21:27 UTC 2015: -0.000235 (mode -j)
Thu May 28 14:21:57 UTC 2015: -0.000240 (mode -j)


getClockDiffIroseCamera ()
{
  # time im milliseconds
  (
  offsetCamera=$(cat $NTPLOG | grep CAMERA | grep '192.168.2.2' | awk '{print $12}')
  offsetIrose=$(echo "scale=3; ($(cat $NTPLOG | grep IROSE | tail -1 | awk '{print $10}')*1000)/1"|bc)
  offsetShoebox=$(cat $NTPLOG | grep "SHOEBOX \*" | awk '{print $12}')
  echo -n "Shoebox ($offsetShoebox) - Irose ($offsetIrose) - Camera ($offsetCamera) (ms): "
  echo ${offsetCamera} - ${offsetIrose}|bc | sed 's/-//'
  ) | log "CLOCKDIFF"
}



[05/24/15 00:00:01]   SHOEBOX remote           refid      st t when poll reach   delay   offset  jitter
[05/24/15 00:00:01]   SHOEBOX ==============================================================================
[05/24/15 00:00:01]   SHOEBOX +eg100appram001  10.4.12.10       3 u    3  128  377   42.453  -10.680 224.586
[05/24/15 00:00:01]   SHOEBOX *eg100appram002  10.4.12.10       3 u   68  128  377   68.814    2.840 139.420
[05/24/15 00:00:01]   SHOEBOX 192.168.1.200   .STEP.          16 u    -  128    0    0.000    0.000   0.000
[05/24/15 00:00:01]   SHOEBOX LOCAL(0)        .LOCL.          10 l   62   64  377    0.000    0.000   0.000
[05/24/15 00:00:05]     IROSE Sat May 23 21:55:24 UTC 2015: -0.000253 (mode -j)
[05/24/15 00:00:05]     IROSE Sat May 23 21:55:54 UTC 2015: -0.000128 (mode -j)
[05/24/15 00:00:05]     IROSE Sat May 23 21:56:24 UTC 2015: 0.000324 (mode -j)
[05/24/15 00:00:05]     IROSE Sat May 23 21:56:54 UTC 2015: 0.000303 (mode -j)
[05/24/15 00:00:05]     IROSE Sat May 23 21:57:25 UTC 2015: 0.000258 (mode -j)
[05/24/15 00:00:05]     IROSE Sat May 23 21:57:55 UTC 2015: 0.000263 (mode -j)
[05/24/15 00:00:05]     IROSE Sat May 23 21:58:25 UTC 2015: 0.000242 (mode -j)
[05/24/15 00:00:05]     IROSE Sat May 23 21:58:55 UTC 2015: 0.000110 (mode -j)
[05/24/15 00:00:05]     IROSE Sat May 23 21:59:25 UTC 2015: 0.000271 (mode -j)
[05/24/15 00:00:05]     IROSE Sat May 23 21:59:55 UTC 2015: 0.000162 (mode -j)
[05/24/15 00:00:25]    CAMERA root@TS(C)-5000EN:~# ntpq -p
[05/24/15 00:00:25]    CAMERA remote           refid      st t when poll reach   delay   offset  jitter
[05/24/15 00:00:25]    CAMERA ==============================================================================
[05/24/15 00:00:25]    CAMERA *192.168.2.2     20.133.70.197    4 u   26   32  377    0.168    1.668   0.264
[05/24/15 00:00:25] CLOCKDIFF Shoebox (2.840) - Irose (.162) - Camera (1.668) (ms): 1.506

*/
