/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.calibration.ssh

import csc.sitebuffer.calibration.ComponentCertificate
import csc.sitebuffer.process.irose.IRoseLoopError

trait SSHCommands {

  def connect(host: String, port: Int, user: String, pwd: String, timeout: Int)
  def disconnect()

  def getIRoseCertificates: CommandResult[List[ComponentCertificate]]

  def getIRoseLoopErrors: CommandResult[List[IRoseLoopError]]

  def getIRoseTimeDifference: CommandResult[TimeDifference]

  def calibrateLane(detectorId: Int): CommandResult[Boolean]
}