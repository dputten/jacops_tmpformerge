/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.calibration

import ssh.TimeDifference
import akka.actor.{ ActorLogging, Actor }
import akka.event.LoggingReceive
import telnet.CommandCameraTimeDifference

case class CameraTimeDifferenceRequest(refId: String, host: String, port: Int, user: String, pwd: String)
case class CameraTimeDifferenceResponse(refId: String, host: String, port: Int, succeed: Boolean, timeDif: TimeDifference, errors: List[String])

/**
 * connect to the camera using telnet
 * run  "ntpq -p"
 * collect the timeDifference
 */
class CameraTimeDifference extends Actor with ActorLogging {
  override def preRestart(reason: Throwable, message: Option[Any]) {
    log.error(reason, "Restart while processing %s".format(message.toString))
    super.preRestart(reason, message)
  }

  def receive = LoggingReceive {
    case request: CameraTimeDifferenceRequest ⇒ {
      val result = CommandCameraTimeDifference.retrieveTimeDifference(request.host, request.port, request.user, request.pwd)
      sender ! CameraTimeDifferenceResponse(request.refId, request.host, request.port, result.errors.isEmpty, result.result, result.errors)
      log.info("CameraTimeDifference: send response: success=" + result.errors.isEmpty)
    }
  }

}

