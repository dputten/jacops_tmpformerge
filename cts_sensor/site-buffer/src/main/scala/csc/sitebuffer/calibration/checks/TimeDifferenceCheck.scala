/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.calibration.checks

import csc.sitebuffer.calibration.{ CameraTimeDifferenceResponse, CameraTimeDifferenceRequest, CalibrationState, StepResult }
import org.slf4j.LoggerFactory
import csc.sitebuffer.calibration.ssh.SSHCommands
import csc.sitebuffer.config.CameraConfig
import akka.actor.ActorRef

object TimeDifferenceCheck {
  val log = LoggerFactory.getLogger(getClass)

  def retrieveIroseTimeDifference(state: CalibrationState, commands: SSHCommands): CalibrationState = {
    log.info("Start Get iRose Time difference")

    val result = commands.getIRoseTimeDifference
    if (result.errors.isEmpty) {
      log.info("Finished Get iRose Time difference successful")
      state.copy(iRoseTimeDiff = Some(result.result))
    } else {
      //something went wrong
      val msg = "Failed to get the iRose Time difference : %s".format(result.errors.mkString(", "))
      log.error(msg)
      val step = StepResult(StepResult.STEP_NTP, false, Some(msg))
      //update state
      log.info("Finished Get iRose Time difference with errors")
      state.copy(stepResults = state.stepResults :+ step)
    }
  }

  def retriggerCameraCheck(cameraTimeDiffActor: ActorRef,
                           cameras: Seq[CameraConfig],
                           cameraTimeDiff: Seq[CameraTimeDifferenceResponse],
                           telnetPort: Int, telnetUser: String, telnetPwd: String) {
    val nrCameras = cameras.size
    val (succeeded, failed) = cameraTimeDiff.partition(_.succeed)
    val timeDiffDone = succeeded.size == nrCameras
    log.debug("TimeDifference succeeded %d failed %d => %s".format(succeeded.size, failed.size, if (timeDiffDone) "Ready" else "Waiting"))
    if (!timeDiffDone) {
      cameras.foreach {
        case current ⇒ {
          if (!succeeded.exists(_.refId == current.imageDir)) {
            log.info("Retrigger time difference for camera %s (%s)".format(current.imageDir, current.cameraHost))
            cameraTimeDiffActor ! CameraTimeDifferenceRequest(refId = current.imageDir, host = current.cameraHost,
              port = telnetPort,
              user = telnetUser,
              pwd = telnetPwd)
          }
        }
      }
    }
  }
}