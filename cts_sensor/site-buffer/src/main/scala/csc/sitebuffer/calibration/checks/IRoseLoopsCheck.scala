/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.calibration.checks

import csc.sitebuffer.calibration.{ CalibrationState, StepResult }
import csc.sitebuffer.calibration.ssh.SSHCommands
import org.slf4j.LoggerFactory

object IRoseLoopsCheck {
  val log = LoggerFactory.getLogger(getClass)

  def check(state: CalibrationState, commands: SSHCommands): CalibrationState = {
    log.info("Start IRose Loop check")

    val result = commands.getIRoseLoopErrors
    if (result.result.isEmpty && result.errors.isEmpty) {
      //test succeeded
      //create stepResult
      val step = StepResult(StepResult.STEP_Loops, true, None)
      //update state
      log.info("Finished IRose Loop check successful")
      state.copy(stepResults = state.stepResults :+ step)
    } else {
      //test failed
      var errorMsg = ""
      if (!result.errors.isEmpty) {
        errorMsg = "Errors getting Loop errors: [" + result.errors.mkString(", ") + "] "
        log.error(errorMsg)
      }
      if (!result.result.isEmpty) {
        val toString = result.result.map(err ⇒ "detector=%s state=%s".format(err.loopNr, err.state))
        val msg = "Failed loops: [" + toString.mkString(", ") + "]"
        errorMsg += msg
        log.error(msg)
      }

      val step = StepResult(StepResult.STEP_Loops, false, Some(errorMsg))
      //update state
      log.info("Finished IRose Loop check with errors")
      state.copy(stepResults = state.stepResults :+ step, iRoseLoopErrors = result.result)
    }
  }

}