/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.calibration.checks

import csc.sitebuffer.calibration._
import csc.sitebuffer.calibration.SerialNumber
import csc.sitebuffer.calibration.GetRegistrationResponse
import csc.sitebuffer.calibration.GantryCalibrationResponse
import image.CompressJpg
import scala.Some
import csc.sitebuffer.calibration.StoreImageResponse
import java.io.File
import csc.sitebuffer.common.Signing
import org.slf4j.LoggerFactory
import csc.sitebuffer.config.CameraConfig
import org.apache.commons.io.FileUtils

object CalibrationResponse {
  val log = LoggerFactory.getLogger(getClass)

  def createImageResult(state: CalibrationState, cameras: Seq[CameraConfig]): CalibrationState = {
    val nrCameras = cameras.size
    val imagesDone = state.images.size == nrCameras
    val detail = if (imagesDone) {
      //check if all files exists
      val errors = state.images.foldLeft(Seq[String]()) {
        case (errors, image) ⇒ {
          val file = new File(image.targetDir, image.fileName)
          if (file.exists()) {
            errors
          } else {
            log.error("File %s for %s not found".format(file.getAbsolutePath, image.cameraDir))
            errors :+ image.cameraDir
          }
        }
      }
      if (errors.isEmpty) {
        log.info("CheckImages was successful")
        None
      } else {
        val msg = "Missing physical calibration images for [%s]".format(errors.mkString(", "))
        Some(msg)
      }
    } else {
      val missingImages = cameras.foldLeft(Seq[String]()) {
        case (missing, current) ⇒ {
          if (state.images.exists(_.cameraDir == current.imageDir)) {
            missing
          } else {
            missing :+ current.imageDir
          }
        }
      }
      val msg = "Missing calibration images: [%s]".format(missingImages.mkString(", "))
      log.error("CheckImages: " + msg)
      Some(msg)
    }
    val step = StepResult(StepResult.STEP_Camera, detail.isEmpty, detail)
    if (detail.isDefined) {
      //delete all files
      state.images.foreach(image ⇒ {
        val file = new File(image.targetDir, image.fileName)
        if (file.exists()) {
          FileUtils.deleteQuietly(file)
        }
      })
      state.copy(stepResults = state.stepResults :+ step, images = Seq())
    } else {
      state.copy(stepResults = state.stepResults :+ step)
    }
  }

  def createVehicleResult(state: CalibrationState, cameras: Seq[CameraConfig]): CalibrationState = {
    val detectors = cameras.flatMap(_.detectors)
    val vehiclesDone = state.vehicles.size == detectors.size
    val detail = if (vehiclesDone) {
      log.info("CheckVehicles was successful")
      None
    } else {
      val missingDetectors = detectors.foldLeft(Seq[String]()) {
        case (missing, currentDetector) ⇒ {
          if (state.vehicles.exists(_.detector == currentDetector)) {
            missing
          } else {
            missing :+ currentDetector.toString
          }
        }
      }
      val msg = "Missing vehicle registrations for detectors: [%s]".format(missingDetectors.mkString(", "))
      log.error("CheckVehicles: " + msg)
      Some(msg)
    }
    val step = StepResult(StepResult.STEP_VehicleRecords, vehiclesDone, detail)
    state.copy(stepResults = state.stepResults :+ step)
  }

  def createNTPResult(state: CalibrationState, cameras: Seq[CameraConfig], NTPmaxDiff_ms: Long): CalibrationState = {
    val failedRequests = state.cameraTimeDiff.filter(_.succeed == false)
    val nrCameras = cameras.size
    var timeDiffDone = state.cameraTimeDiff.size == nrCameras
    val step = (timeDiffDone, state.iRoseTimeDiff, failedRequests.isEmpty) match {
      case (_, _, false) ⇒ {
        val msg = failedRequests.map(_.refId).mkString(", ")
        log.error("NTPCheck: Failed to retrieve Camera NTP information: [%s]".format(msg))
        //no Camera Info => NTP check fails
        StepResult(StepResult.STEP_NTP, false, Some("Failed to retrieve Camera NTP information: [%s]".format(msg)))
      }
      case (_, None, _) ⇒ {
        log.error("NTPCheck: Failed to retrieve iRose NTP information")
        //no Irose Info => NTP check fails
        StepResult(StepResult.STEP_NTP, false, Some("Failed to retrieve iRose NTP information"))
      }
      case (false, _, _) ⇒ {
        //missing camera info => NTP check fails
        val missingImages = cameras.foldLeft(Seq[String]()) {
          case (missing, current) ⇒ {
            if (state.cameraTimeDiff.exists(_.refId == current.imageDir)) {
              missing
            } else {
              missing :+ current.imageDir
            }
          }
        }
        val msg = "Missing time difference camera: [%s]".format(missingImages.mkString(", "))
        log.error("NTPCheck: " + msg)
        StepResult(StepResult.STEP_NTP, false, Some(msg))
      }
      case (true, Some(iRoseTime), true) ⇒ {
        //calculate time difference between camera and iRose
        val timeDiff = state.cameraTimeDiff.map(camera ⇒ {
          (camera.refId, camera.timeDif.timeDifference - iRoseTime.timeDifference)
        })
        val ntpErrors = timeDiff.foldLeft(Seq[String]()) {
          case (errors, (id, time)) ⇒ if (math.abs(time) > NTPmaxDiff_ms) {
            log.error("NTPCheck failed for camera %s: diff= %d > %d".format(id, time, NTPmaxDiff_ms))
            errors :+ id
          } else {
            errors
          }
        }
        val detail = if (ntpErrors.isEmpty) {
          log.info("NTPCheck was successful")
          None
        } else {
          val msg = "NTPCheck failed for camera = [%s]".format(ntpErrors.mkString(", "))
          log.error(msg)
          Some(msg)
        }
        StepResult(StepResult.STEP_NTP, ntpErrors.isEmpty, detail)
      }
    }
    state.copy(stepResults = state.stepResults :+ step)
  }
  def checkCalibrationFinished(state: CalibrationState, cameras: Seq[CameraConfig],
                               NTPmaxDiff_ms: Long, calibrationImageDir: String,
                               jpgQuality: Int): Option[GantryCalibrationResponse] = {
    val nrCameras = cameras.size
    val nrDetectors = cameras.flatMap(_.detectors).size
    val imagesDone = state.images.size == nrCameras
    log.debug("Store Images %d => %s".format(state.images.size, if (imagesDone) "Ready" else "Waiting"))
    val (succeeded, failed) = state.cameraTimeDiff.partition(_.succeed)
    val timeDiffDone = succeeded.size == nrCameras
    log.debug("TimeDifference succeeded %d failed %d => %s".format(succeeded.size, failed.size, if (timeDiffDone) "Ready" else "Waiting"))
    val regDone = state.vehicles.size == nrDetectors
    log.debug("Registrations %d => %s".format(state.vehicles.size, if (regDone) "Ready" else "Waiting"))

    if (imagesDone && timeDiffDone && regDone) {
      Some(createCalibrationResult(state, cameras, NTPmaxDiff_ms, calibrationImageDir, jpgQuality))
    } else {
      log.info("Calibration isn't ready yet!")
      None
    }
  }

  def createCalibrationResult(state: CalibrationState,
                              cameras: Seq[CameraConfig],
                              NTPmaxDiff_ms: Long,
                              calibrationImageDir: String,
                              jpgQuality: Int): GantryCalibrationResponse = {
    log.info("Create Calibration result")
    var updatedState = createImageResult(state, cameras)
    updatedState = createVehicleResult(updatedState, cameras)
    updatedState = createNTPResult(updatedState, cameras, NTPmaxDiff_ms)

    //create Result
    val stepResults = getStepResults(updatedState.stepResults)
    val serialNr = getSerialNr(updatedState.vehicles, updatedState.request.serialNr)

    //if the result is failed we don't need the images
    //so drop the images to speedup sending the response
    val checkSucceeded = !stepResults.exists(_.success == false)
    val images = if (checkSucceeded) {
      getDownloadImages(updatedState.request.systemId, updatedState.request.gantryId, updatedState.images, calibrationImageDir, jpgQuality)
    } else {
      Seq()
    }
    val result = GantryCalibrationResponse(
      systemId = updatedState.request.systemId,
      gantryId = updatedState.request.gantryId,
      time = updatedState.request.time,
      reportingOfficerCode = updatedState.request.reportingOfficerCode,
      serialNr = serialNr,
      activeCertificates = updatedState.certificates,
      images = images,
      results = stepResults)

    //delete images
    updatedState.images.map(image ⇒ {
      val file = new File(image.targetDir, image.fileName)
      FileUtils.deleteQuietly(file)
    })

    log.info("Created Calibration result %s".format(result))
    result
  }

  def getStepResults(stepResults: Seq[StepResult]): Seq[StepResult] = {
    //make sure that all steps are present
    StepResult.allSteps.map(step ⇒ {
      stepResults.find(_.stepName == step) match {
        case Some(stepInfo) ⇒ stepInfo
        case None           ⇒ StepResult(step, false, Some("Step not finished"))
      }
    })
  }
  def getSerialNr(vehicles: Seq[GetRegistrationResponse], serialNrInfo: SerialNumber): String = {
    vehicles.headOption match {
      case None ⇒ {
        log.error("Could not create serial number due to missing vehicle registrations")
        serialNrInfo.serialNumber
      }
      case Some(reg) ⇒ {
        val serialNumberFormatString = if (reg.registration.serialNr.length < serialNrInfo.serialNumberLength)
          "%s" + "0" * (serialNrInfo.serialNumberLength - reg.registration.serialNr.length) + "%s"
        else
          "%s%s"
        serialNumberFormatString.format(serialNrInfo.serialNumberPrefix, reg.registration.serialNr)
      }
    }
  }

  def getDownloadImages(systemId: String, gantryId: String, images: Seq[StoreImageResponse], calibrationImageDir: String, jpgQuality: Int): Seq[CalibrationImage] = {

    images.map(image ⇒ {
      val file = new File(image.targetDir, image.fileName)
      val bytes = if (jpgQuality > 0 && jpgQuality < 100) {
        //do compressing
        CompressJpg.readAndCompress(file, jpgQuality)
      } else {
        //just read original
        FileUtils.readFileToByteArray(file)
      }
      val checksum = Signing.digest(bytes, "MD5")
      CalibrationImage(
        name = image.fileName,
        imageId = "%s-%s-%s".format(systemId, gantryId, image.cameraDir),
        checksum = checksum,
        image = bytes.map(_.toInt))
    })
  }

}