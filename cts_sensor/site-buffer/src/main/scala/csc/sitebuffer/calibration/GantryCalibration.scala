/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.calibration

import akka.actor.{ ActorRef, ActorLogging, Actor }
import akka.event.LoggingReceive
import checks._
import ssh.{ TimeDifference, SSHCommands }
import java.util.Date
import java.text.SimpleDateFormat
import java.io.File
import csc.sitebuffer.process.irose.IRoseLoopError
import csc.sitebuffer.config.{ CalibrationConfig, CameraConfig }

import scala.concurrent.ExecutionContext

case class CalibrationState(request: GantryCalibrationRequest, timeReceived: Date,
                            iRoseTimeDiff: Option[TimeDifference] = None,
                            certificates: Seq[ComponentCertificate] = Seq(),
                            iRoseLoopErrors: Seq[IRoseLoopError] = Seq(),
                            images: Seq[StoreImageResponse] = Seq(),
                            vehicles: Seq[GetRegistrationResponse] = Seq(),
                            cameraTimeDiff: Seq[CameraTimeDifferenceResponse] = Seq(),
                            stepResults: Seq[StepResult] = Seq())
object CalibrationTimeout

/**
 * This class is responsible for doing the actual calibration
 * Activate the CalibrationImage tap
 * Activate the CalibrationRegistration tap
 * Activate the CameraTimeDifference for all camera's
 * Connect to iRose
 * get the certificates or checksum
 * Check if the loops are working
 * Trigger the iRose Calibration script
 * Get the time difference
 * Disconnect from iRose
 * wait for response of CalibrationRegistration tap
 * wait for response of CalibrationImage tap
 * wait for response CameraTimeDifference
 * create calibration response
 *
 * @param imageTap  Actor responsible to get calibration images
 * @param registrationTap Actor responsible to get vehicle registrations
 * @param cameraTimeDiff Actor responsible to get the camera time difference
 * @param commands Interface to get iRose information
 * @param resultSender Actor responsible for sending the Calibration result back to the central calibration
 */
class GantryCalibration(calibrationCfg: CalibrationConfig,
                        cameraConfigs: Map[String, CameraConfig],
                        imageTap: ActorRef,
                        registrationTap: ActorRef,
                        cameraTimeDiff: ActorRef,
                        commands: SSHCommands,
                        resultSender: ActorRef) extends Actor with ActorLogging {

  var calibrationState: Option[CalibrationState] = None
  val dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS z")
  implicit val executor: ExecutionContext = context.dispatcher

  override def preRestart(reason: Throwable, message: Option[Any]) {
    log.error(reason, "Restart while processing %s".format(message.toString))
    super.preRestart(reason, message)
  }

  def receive = LoggingReceive {
    case req: GantryCalibrationRequest ⇒ {
      //check if we still are processing a calibration
      calibrationState match {
        case Some(state) ⇒ {
          //already processing a calibration
          if (state.request.time == req.time) {
            log.info("Received Resend calibration request for time %s(%d)".format(dateFormat.format(new Date(req.time)), req.time))
            retriggerCalibration()
          } else {
            log.warning("Received new calibration %s while processing the previous %s".format(dateFormat.format(new Date(req.time)), dateFormat.format(new Date(state.request.time))))
            val response = CalibrationResponse.createCalibrationResult(state, cameraConfigs.values.toSeq, calibrationCfg.NTPmaxDiff_ms, calibrationCfg.calibrationImageDir, calibrationCfg.jpgQuality)
            resultSender ! response
            log.info("Start calibration for time %s(%d)".format(dateFormat.format(new Date(req.time)), req.time))
            calibrationState = Some(CalibrationState(req, new Date()))
          }
        }
        case None ⇒ {
          log.info("Start calibration for time %s(%d)".format(dateFormat.format(new Date(req.time)), req.time))
          calibrationState = Some(CalibrationState(req, new Date()))
        }
      }

      //start calibration
      // Activate the CalibrationImage tap
      val targetDir = new File(calibrationCfg.ftpRootDir, calibrationCfg.calibrationImageDir)
      cameraConfigs.foreach {
        case (id, cfg) ⇒ {
          imageTap ! StoreImageRequest(refId = id, cameraDir = cfg.imageDir, targetDir = targetDir.getAbsolutePath)
        }
      }
      // Activate the CalibrationRegistration tap
      val laneDetectors = cameraConfigs.values.flatMap(_.detectors)
      laneDetectors.foreach(detector ⇒ {
        registrationTap ! GetRegistrationRequest(refId = detector.toString, detector = detector)
      })

      // Activate the CameraTimeDifference for all camera's
      cameraConfigs.foreach {
        case (id, cfg) ⇒ {
          cameraTimeDiff ! CameraTimeDifferenceRequest(refId = cfg.imageDir, host = cfg.cameraHost,
            port = calibrationCfg.telnetPort,
            user = calibrationCfg.telnetUser,
            pwd = calibrationCfg.telnetPwd)
        }
      }
      try {
        // Connect to iRose
        commands.connect(calibrationCfg.sshHost, calibrationCfg.sshPort,
          calibrationCfg.sshUser, calibrationCfg.sshPwd, calibrationCfg.sshTimeout)
        for (state ← calibrationState) {
          // Trigger the iRose Calibration script
          CalibrationTrigger.triggerLoops(commands, laneDetectors.toSeq)

          // get the certificates or checksum
          var newState = CertificateCheck.check(state, commands, req.mustCertificates)
          calibrationState = Some(newState)
          // Check if the loops are working
          newState = IRoseLoopsCheck.check(newState, commands)
          calibrationState = Some(newState)
          // Get the time difference
          newState = TimeDifferenceCheck.retrieveIroseTimeDifference(newState, commands)
          calibrationState = Some(newState)
          if (hasCalibrationErrors) {
            val response = CalibrationResponse.createCalibrationResult(newState, cameraConfigs.values.toSeq, calibrationCfg.NTPmaxDiff_ms, calibrationCfg.calibrationImageDir, calibrationCfg.jpgQuality)
            resultSender ! response
            calibrationState = None
          } else {
            //wait for responses
            log.info("Wait for responses")
          }
        }
      } finally {
        // Disconnect from iRose
        commands.disconnect()
      }
      context.system.scheduler.scheduleOnce(calibrationCfg.retriggerTime, self, CalibrationTimeout)
    }
    case storedImage: StoreImageResponse ⇒ {
      for (state ← calibrationState) {
        val alreadyReceived = state.images.exists(_.cameraDir == storedImage.cameraDir)
        if (alreadyReceived) {
          log.info("Received a double image for camera %s".format(storedImage.cameraDir))
        } else {
          log.info("Received an image for camera %s".format(storedImage.cameraDir))
          //add to state
          val updatedState = state.copy(images = state.images :+ storedImage)
          calibrationState = Some(updatedState)
          //check if calibration is complete
          checkCalibrationFinished(updatedState)
        }
      }
    }
    case registration: GetRegistrationResponse ⇒ {
      for (state ← calibrationState) {
        val alreadyReceived = state.vehicles.exists(_.detector == registration.detector)
        if (alreadyReceived) {
          log.info("Received a double registration for detectot %d".format(registration.detector))
        } else {
          log.info("Received a registration for detectot %d".format(registration.detector))
          //add to state
          val updatedState = state.copy(vehicles = state.vehicles :+ registration)
          calibrationState = Some(updatedState)
          //check if calibration is complete
          checkCalibrationFinished(updatedState)
        }
      }
    }
    case cameraTimeDiff: CameraTimeDifferenceResponse ⇒ {
      for (state ← calibrationState) {
        //update cameraTimeDiff only when there wasn't a response for the camera or a failed response
        val alreadyReceived = state.cameraTimeDiff.exists(diff ⇒ diff.host == cameraTimeDiff.host && diff.succeed)
        if (alreadyReceived) {
          log.info("Received a double timeDifference for camera %s".format(cameraTimeDiff.refId))
        } else {
          log.info("Received an timeDifference for camera %s".format(cameraTimeDiff.refId))
          //add to state
          //filter old version, which (if exists) is a failed response
          val newList = state.cameraTimeDiff.filter(_.host != cameraTimeDiff.host) :+ cameraTimeDiff
          val updatedState = state.copy(cameraTimeDiff = newList)
          calibrationState = Some(updatedState)
          //check if calibration is complete
          checkCalibrationFinished(updatedState)
        }
      }
    }
    case CalibrationTimeout ⇒ {
      for (state ← calibrationState) {
        if (state.timeReceived.getTime + calibrationCfg.maxProcessTime.toMillis < System.currentTimeMillis()) {
          log.info("Calibration max processing time reached. create response")
          val response = CalibrationResponse.createCalibrationResult(state, cameraConfigs.values.toSeq, calibrationCfg.NTPmaxDiff_ms, calibrationCfg.calibrationImageDir, calibrationCfg.jpgQuality)
          resultSender ! response
          calibrationState = None
        } else {
          log.info("CalibrationTimeout")
          retriggerCalibration()
          context.system.scheduler.scheduleOnce(calibrationCfg.retriggerTime, self, CalibrationTimeout)
        }
      }
    }
  }

  def hasCalibrationErrors: Boolean = {
    calibrationState.map(state ⇒ {
      state.stepResults.exists(!_.success)
    }).getOrElse(false)
  }

  def checkCalibrationFinished(state: CalibrationState) {
    val response = CalibrationResponse.checkCalibrationFinished(state, cameraConfigs.values.toSeq, calibrationCfg.NTPmaxDiff_ms, calibrationCfg.calibrationImageDir, calibrationCfg.jpgQuality)
    response.foreach(resp ⇒ {
      resultSender ! resp
      calibrationState = None
    })
  }

  def retriggerCalibration() {
    log.info("RetriggerCalibration")
    //retrigger waiting step when possible
    for (state ← calibrationState) {
      val detectors = cameraConfigs.values.flatMap(_.detectors).toSeq
      //check where we waiting for

      CalibrationTrigger.retriggerLoopsCheck(commands,
        detectors,
        state.vehicles, calibrationCfg.sshHost, calibrationCfg.sshPort,
        calibrationCfg.sshUser, calibrationCfg.sshPwd, calibrationCfg.sshTimeout)

      TimeDifferenceCheck.retriggerCameraCheck(cameraTimeDiffActor = cameraTimeDiff,
        cameras = cameraConfigs.values.toSeq,
        cameraTimeDiff = state.cameraTimeDiff,
        telnetPort = calibrationCfg.telnetPort,
        telnetUser = calibrationCfg.telnetUser,
        telnetPwd = calibrationCfg.telnetPwd)
      //just check other steps, but we can't do anything about it
      val imagesDone = state.images.size == cameraConfigs.size
      val regDone = state.vehicles.size == detectors.size
      log.debug("Store Images %d => %s".format(state.images.size, if (imagesDone) "Ready" else "Waiting"))
      //if we don't have vehicleRegistrations it is again triggered in previous step
      //when we have all vehicleRegistrations => probably camera issues
      if (regDone && !imagesDone) {
        log.error("Expecting camara failure. produce response")
        val response = CalibrationResponse.createCalibrationResult(state, cameraConfigs.values.toSeq, calibrationCfg.NTPmaxDiff_ms, calibrationCfg.calibrationImageDir, calibrationCfg.jpgQuality)
        resultSender ! response
        calibrationState = None
      }
    }
  }
}

