/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.calibration.ssh

case class CommandResult[T](errors: List[String], result: T)