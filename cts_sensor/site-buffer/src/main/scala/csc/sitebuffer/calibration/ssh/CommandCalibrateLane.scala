/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.calibration.ssh

import org.slf4j.LoggerFactory
import fr.janalyse.ssh.SSH

object CommandCalibrateLane {
  var returnMessage: String = """Calibratietest"""
  private val log = LoggerFactory.getLogger(getClass)

  def calibrateLane(sshConnection: SSH, detectorId: Int): CommandResult[Boolean] = {
    val command = "calibratie %s".format(detectorId)
    val result = sshConnection.execOnceAndTrim(command)
    if (result == null) {
      CommandResult(List("Could not execute command [%s]".format(command)), false)
    } else {
      if (result.startsWith(returnMessage)) {
        CommandResult(List(), true)
      } else {
        val errorMessage = "Bericht van iRose ontvangen: " + result
        log.error(errorMessage)
        log.error("Message expected from iRose: " + returnMessage)
        CommandResult(List(errorMessage), false)
      }

    }
  }

}