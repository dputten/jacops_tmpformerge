/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.calibration

import akka.actor.{ ActorLogging, Actor, ActorRef }
import akka.camel.{ Ack, CamelMessage, Consumer }
import akka.event.LoggingReceive
import net.liftweb.json.{ DefaultFormats, Serialization }
import csc.akka.remote.wire.Protocol

/**
 * Receive calibration requests
 * @param calibrationEndpointHost interface where to listen
 * @param calibrationEndpointPort port number where to listen
 * @param recipients set of actors processing the received message
 */
class CalibrationConsumer(
  calibrationEndpointHost: String,
  calibrationEndpointPort: Int,
  recipients: Set[ActorRef]) extends Actor with ActorLogging with Consumer with Protocol {

  private def calibrationEndpointTemplate = "mina:tcp://%s:%d?textline=true&sync=true"
  def endpointUri = calibrationEndpointTemplate.format(calibrationEndpointHost, calibrationEndpointPort)
  implicit val formats = DefaultFormats

  override def preRestart(reason: Throwable, message: Option[Any]) {
    log.error(reason, "Restart while processing %s".format(message.toString))
    super.preRestart(reason, message)
  }

  def receive = LoggingReceive {
    case msg: CamelMessage ⇒ {
      val str = msg.bodyAs[String]
      log.info("Received msg: %s....." format (str.substring(0, 50)))
      sender ! Ack
      if (str.trim.isEmpty) {
        log.info("Received an empty line")
      } else {
        addFragment(str) foreach (messageData ⇒ {
          try {
            val msg = Serialization.read[GantryCalibrationRequest](new String(messageData))
            sendToRecipients(msg)
          } catch {
            case ex: Exception ⇒ {
              log.error(ex, "Failed to DeSerialize %s".format(str))
            }
          }
        })
      }
    }
    case msg: Any ⇒
      unknownMessageReceived(msg.toString)
  }

  def sendToRecipients(message: Any) {
    for (recipient ← recipients) {
      recipient ! message
    }
  }

  def unknownMessageReceived(message: String) {
    log.error("Unknown message received: [%s]".format(message))
  }
}