/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.calibration.ssh

import csc.sitebuffer.calibration.ComponentCertificate
import fr.janalyse.ssh.SSH

object CommandCertificates {
  private val hash = "([0-9a-f]{128})"
  private val component = "(daemon_irose2|iRose2\\.ko|iRose2\\.rbf|irose2\\-site\\-dependant\\.conf|irose2\\-site\\-independant\\.conf)"
  private val certificate_re = (hash + "\\s*.*" + component + "\\s*.*").r

  /**
   * Return the list with errors and certificates in a tuple (List[String], List[ComponentCertificate])
   * @param sshConnection
   */
  def getCertificates(sshConnection: SSH): CommandResult[List[ComponentCertificate]] = {
    val result = sshConnection.execOnceAndTrim("get-crc.sh")
    if (result == null) {
      CommandResult[List[ComponentCertificate]](List("Could not execute command get-crc.sh"), List())
    } else {
      val lines = result.split("\n").toList
      val parseResult = parseRawCertificates(lines)
      val (errors, cert) = tupleWithErrorsAndCertificates(parseResult)
      CommandResult(errors, cert)
    }
  }

  /**
   *
   * @param rawCertificates, result from retrieveRawCertificates
   * @return List of Left(String) if the string could not be parsed or Right(IRoseCertificate)
   */
  def parseRawCertificates(rawCertificates: List[String]): List[Either[String, ComponentCertificate]] = {

    val maybeCertificates: List[Either[String, ComponentCertificate]] = rawCertificates.map(aString ⇒ {
      aString match {
        case certificate_re(hash, component) ⇒
          Right(ComponentCertificate(component, hash))
        case _ ⇒
          Left(aString)
      }
    })
    maybeCertificates
  }

  /**
   * Split the list with errors and certificates in a tuple (List[String], List[ComponentCertificate])
   * @param xs list containing errors and certificates
   * @return
   */
  private def tupleWithErrorsAndCertificates(xs: List[Either[String, ComponentCertificate]]): (List[String], List[ComponentCertificate]) = {
    val split = xs.groupBy(_ match {
      case Left(_)  ⇒ "errors"
      case Right(_) ⇒ "certificates"
    })

    (split.getOrElse("errors", List()).map(_.left.get), split.getOrElse("certificates", List()).map(_.right.get))
  }

}