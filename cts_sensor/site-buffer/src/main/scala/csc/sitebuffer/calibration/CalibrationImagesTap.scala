/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.calibration

import akka.event.LoggingReceive
import csc.sitebuffer.process.images.NewImage
import akka.actor.{ ActorLogging, Actor, ActorRef }
import java.io.File
import org.apache.commons.io.FileUtils

case class StoreImageRequest(refId: String, cameraDir: String, targetDir: String)
case class StoreImageResponse(refId: String, cameraDir: String, targetDir: String, fileName: String)

/**
 * Pass all NewImages to the next stage and wait for Requests.
 * When there is a request check if the image is a match with the request
 * And when it is a match copy the file to the target directory
 * @param recipients the next step in processing the image
 */
class CalibrationImagesTap(recipients: Set[ActorRef]) extends Actor with ActorLogging {
  var requests = Seq[(ActorRef, StoreImageRequest)]()

  override def preRestart(reason: Throwable, message: Option[Any]) {
    log.error(reason, "Restart while processing %s".format(message.toString))
    super.preRestart(reason, message)
  }

  def receive = LoggingReceive {
    case req: StoreImageRequest ⇒ {
      log.info("%s: Received request for camera %s".format(req.refId, req.cameraDir))
      requests = requests :+ (sender, req)
    }
    case newImage: NewImage ⇒ {
      if (!requests.isEmpty) {
        processImage(newImage)
      }
      sendToRecipients(newImage)
    }
  }

  private def sendToRecipients(message: Any) {
    for (recipient ← recipients) {
      recipient ! message
    }
  }

  def processImage(newImage: NewImage) {
    val file = new File(newImage.imagePath)
    val cameraDir = getCameraDirectory(file)
    log.debug("Check Received file %s => cameraDir %s".format(newImage.imagePath, cameraDir))
    val found = requests.filter(_._2.cameraDir == cameraDir)
    found.zipWithIndex.foreach {
      case ((requestor, req), index) ⇒ {
        //copy file
        val targetFile = new File(req.targetDir, "%d_%s".format(index, file.getName))
        log.info("%s: Received file %s copying to %s".format(req.refId, newImage.imagePath, targetFile.getAbsolutePath))
        FileUtils.copyFile(file, targetFile)
        //send response
        requestor ! StoreImageResponse(req.refId, req.cameraDir, req.targetDir, targetFile.getName)
        //remove from requests
        requests = requests.filterNot(_._2.cameraDir == cameraDir)
      }
    }
  }

  def getCameraDirectory(imagePath: File): String = {
    imagePath.getParentFile.getName
  }
}