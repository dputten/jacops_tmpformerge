/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.send

import akka.actor.{ ActorLogging, Actor }
import akka.camel.{ CamelMessage, Producer }
import akka.actor.Status.Failure

/**
 * Actor that receives messages and sends them on to the configured Camel endpoint.
 */
class TextlineTcpProducer(host: String, port: Int) extends Actor with Producer with ActorLogging {
  def endpointUriTemplate = "mina:tcp://%s:%d?textline=true&sync=true"
  def endpointUri = endpointUriTemplate.format(host, port)

  override protected def transformResponse(msg: Any): Any = msg match {
    case message: CamelMessage ⇒ message.body match {
      case "Ack" ⇒
        InOutTextSender.Success
      case msg: Any ⇒
        log.error("Unknown CamelMessage received: %s".format(message.bodyAs[String]))
        InOutTextSender.Failure
    }
    case message: Failure ⇒
      log.error("Failure received: %s".format(message.toString()))
      InOutTextSender.Failure
  }
}
