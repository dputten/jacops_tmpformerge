/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.recording

/**
 * Messages used on the event stream.
 */
sealed trait RecordEventMessage

case class StartRecordingInput(recordingId: String) extends RecordEventMessage
case object StopRecordingInput extends RecordEventMessage

