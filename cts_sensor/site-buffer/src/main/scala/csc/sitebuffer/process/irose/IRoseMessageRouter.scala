/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.irose

import akka.actor.{ ActorLogging, Actor, ActorRef }
import csc.sitebuffer.messages._
import akka.event.LoggingReceive

/**
 * Actor receiving IRoseMessages.
 * Depending on the message type, they will be routed to their recipients.
 *
 * @param vehicleMessageRecipients actor that will receive all VehicleMessages
 * @param alarmEventRecipients actor that will receive all alarm messages (= InputEventMessage with inputId = 8)
 * @param inputEventMessageRecipients actor that will receive all InputEventMessages
 * @param sensorMessageRecipients actor that will receive all SensorMessages
 * @param singleLoopMessageRecipients actor that will receive all SingleLoopMessages
 */
class IRoseMessageRouter(
  vehicleMessageRecipients: Set[ActorRef],
  alarmEventRecipients: Set[ActorRef],
  inputEventMessageRecipients: Set[ActorRef],
  sensorMessageRecipients: Set[ActorRef],
  singleLoopMessageRecipients: Set[ActorRef]) extends Actor with ActorLogging {

  def receive = LoggingReceive {
    case vehicleMessage: VehicleMessage ⇒
      sendToRecipients(vehicleMessage, vehicleMessageRecipients)
    case inputMessage: InputEventMessage if isAlarm(inputMessage) ⇒
      sendToRecipients(inputMessage, alarmEventRecipients)
    case inputMessage: InputEventMessage ⇒
      sendToRecipients(inputMessage, inputEventMessageRecipients)
    case sensorMessage: SensorMessage ⇒
      sendToRecipients(sensorMessage, sensorMessageRecipients)
    case singleLoopMessage: SingleLoopMessage ⇒
      sendToRecipients(singleLoopMessage, singleLoopMessageRecipients)
    case anyRef: AnyRef ⇒ {
      log.error("Received unknown message: [%s]".format(anyRef))
    }
  }

  def sendToRecipients(message: Any, recipients: Set[ActorRef]) {
    for (recipient ← recipients) {
      recipient ! message
    }
  }

  private def isAlarm(inputMessage: InputEventMessage): Boolean = {
    inputMessage.inputId == 8
  }
}
