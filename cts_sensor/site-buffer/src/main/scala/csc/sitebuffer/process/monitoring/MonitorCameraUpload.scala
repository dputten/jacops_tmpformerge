/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.monitoring

import csc.sitebuffer.process.images.NewImage
import csc.sitebuffer.messages.VehicleMessage
import akka.actor.{ Props, ActorRef, Actor }
import csc.sitebuffer.config.CameraConfig
import scala.concurrent.duration._
import scala.tools.nsc.io.File
import csc.akka.process.ExecuteScriptActor

class MonitorCameraUpload(config: Seq[CameraConfig], systemSensorMessage: ActorRef) extends Actor {
  private var cameraStates = Map[String, ActorRef]()

  override def preStart() {
    super.preStart()
    //start Reboot Actor and systemLogger
    val exec = context.actorOf(Props[ExecuteScriptActor])
    val reboot = context.actorOf(Props(new RebootCamera(config, Seq(exec))))
    //start per Camera State checker
    cameraStates = config.map(cfg ⇒ {
      val logger: ActorRef = context.actorOf(Props(
        new SendCameraSystemEvent(waitForFixTime = cfg.waitForFixedMsg,
          systemSensorMessage = systemSensorMessage)))
      val ref = context.actorOf(Props(
        new CameraStateMonitor(camera = cfg.cameraId,
          maxWaitTimeForImage = cfg.maxWaitForImage.millis,
          maxErrorTimeForImage = cfg.maxWaitForReboot.millis,
          recipients = Set(reboot, logger))))
      (cfg.cameraId, ref)
    }).toMap
  }

  def receive = {
    case image: NewImage ⇒ {
      //find CameraState
      val file = File(image.imagePath)
      val cameraCfg = config.find(cfg ⇒ file.parent.endsWith(cfg.imageDir))
      val ref = cameraCfg.flatMap(cfg ⇒ {
        cameraStates.get(cfg.cameraId)
      })
      //send msg to CameraState
      ref.foreach(_ ! image)
    }
    case vehicle: VehicleMessage ⇒ {
      //find CameraState
      val cameraCfg = config.find(cfg ⇒ cfg.detectors.contains(vehicle.detector))
      val ref = cameraCfg.flatMap(cfg ⇒ {
        cameraStates.get(cfg.cameraId)
      })
      //send msg to CameraState
      ref.foreach(_ ! vehicle)
    }
  }
}