package csc.sitebuffer.process.messages

import akka.actor.{ ActorRef, ActorLogging, Actor }
import scala.concurrent.duration._
import csc.sitebuffer.process.irose.{ IRoseLoopError, IRoseCusStatParser }
import akka.event.LoggingReceive
import scala.collection.mutable.ListBuffer
import csc.sitebuffer.process.SystemSensorMessage
import csc.sitebuffer.process.SystemSensorMessage.{ EventType, Sensor }
import scala.collection.immutable
import csc.akka.process.{ ExecuteScriptResult, ExecuteScript }

/**
 * Created by jeijlders on 5/8/14.
 */
class IRoseLoopStateMonitor(iroseCusStatParser: IRoseCusStatParser,
                            executeScriptActor: ActorRef,
                            systemSensorMessageSender: ActorRef,
                            cusStatusIRosePath: String,
                            getCurrentTime: () ⇒ Long) extends Actor with ActorLogging {
  var previousErrors = new ListBuffer[IRoseLoopError]

  def receive = LoggingReceive {
    case "tick" ⇒
      log.debug("IRoseLoopStateMonitor entering start")
      executeScriptActor ! new ExecuteScript(cusStatusIRosePath, Seq())
    case result: ExecuteScriptResult ⇒ {
      result.resultCode match {
        case Right(returnCode) if (returnCode == 0) ⇒ {

          log.info("Cus stat IRose successful. Executed script [%s].".format(result.script.script))
          val errors = iroseCusStatParser.getIRoseLoopErrors(result.stdOut.mkString)
          log.debug("result.stdOut.mkString()" + result.stdOut.mkString)
          errors.foreach(error ⇒ {
            log.debug(error.toString)
            if (previousErrors.contains(error)) {
              log.info("loop " + error.loopNr + " is still disconected.")
              log.info("SENDING TO systemSensorMessageSender " + createSystemSensorMessage(error))
              systemSensorMessageSender ! createSystemSensorMessage(error)
            }
          })
          previousErrors = errors
        }
        case Right(returnCode) ⇒ log.error("Cus stat IRose failed. Failure in script [%s] received returncode [%d]".format(result.script.script, returnCode))
        case Left(throwable)   ⇒ log.error(throwable, "Cus stat IRose failed. Failure while executing script [%s]".format(result.script.script))
      }
    }
    case _ ⇒
  }

  def createSystemSensorMessage(iroseLoopError: IRoseLoopError): SystemSensorMessage = {
    log.debug("IRoseLoopStateMonitor entering createSystemSensorMessage")
    val now = getCurrentTime()
    SystemSensorMessage(Sensor.DetectionLoop, now, "", EventType.DetectionLoopState, "Sitebuffer", immutable.Map("loopId" -> iroseLoopError.loopNr, "state" -> iroseLoopError.state))
  }
}
