/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.recording

import akka.actor.{ Props, ActorRef, ActorRefFactory }
import java.io.File

/**
 * This trait represents the ability to create a writer of IRoseString messages.
 * The writer for given recordingId is created using given ActorRefFactory.
 */
trait IRoseStringToDirectoryWriterCreator extends IRoseStringWriterCreator {

  def getBaseDirectory: File

  def createWriter(recordingId: String, actorRefFactory: ActorRefFactory): ActorRef = {
    val recordingDirectory = new File(getBaseDirectory, recordingDirectoryName(recordingId))
    val recordingMessagesDirectory = new File(recordingDirectory, "messages")

    actorRefFactory.actorOf(Props(
      new IRoseStringToDirectoryWriter(recordingMessagesDirectory)),
      "IRoseStringToDirectoryWriter-%s".format(recordingId))
  }

  def recordingDirectoryName(recordingId: String): String = {
    recordingId.trim
  }

}
