/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.recording

import akka.actor.{ ActorLogging, Actor }
import java.io.File
import org.apache.commons.io.FileUtils
import csc.sitebuffer.process.irose.IRoseString

/**
 * Actor that writes IRoseStrings to files in given directory.
 * The name of a file is determined by the content of the corresponding IRoseString.
 */
class IRoseStringToDirectoryWriter(directory: File) extends Actor with ActorLogging {

  // Next line might throw an exception. Parent supervisor will receive ActorInitializationException and then
  // stop this actor. As a result parent will receive Terminated message.
  if (!directory.exists()) directory.mkdirs()

  def receive = {
    case msg: IRoseString ⇒
      writeIRoseStringToFile(getFilename(msg), msg)

    case msg: Any ⇒
      unknownMessageReceived(msg.toString)
  }

  def unknownMessageReceived(message: String) {
    log.error("Unknown message received: [%s]".format(message))
  }

  // In theory this filename might not be unique enough. In (current) practice it should be sufficient.
  def getFilename(msg: IRoseString): String = {
    (msg.typeChar, msg.detectorChar) match {
      case (Some(tChar), Some(dChar)) ⇒ "%d-%s-%s.txt".format(msg.timestamp, tChar, dChar)
      case (Some(tChar), None)        ⇒ "%d-%s.txt".format(msg.timestamp, tChar)
      case _                          ⇒ "%d.txt".format(msg.timestamp)
    }
  }

  def writeIRoseStringToFile(filename: String, iRoseString: IRoseString): Unit = try {
    FileUtils.writeStringToFile(new File(directory, filename), iRoseString.string)
  } catch {
    case e: Exception ⇒
      log.error("Exception trying to write IRoseString [%s] to file [%s]. Exception: %s".format(iRoseString, filename, e))
  }
}
