/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.camera

/**
 * Abstract trait representing a connection to a Jai camera.
 */
trait JaiConnection {

  /**
   * Create a connection to a Jai Camera.
   * Returns Right(okMessage) or Left(errorMessage)
   */
  def createJaiConnection: Either[String, String]

  /**
   * Start connection with Jai Camera.
   * Returns Right(okMessage) or Left(errorMessage)
   */
  def jaiConnectionStart: Either[String, String]

  /**
   * Shut down connection with Jai Camera.
   * Returns Right(okMessage) or Left(errorMessage)
   */
  def jaiConnectionShutdown: Either[String, String]

  /**
   * Send a status request to camera
   * Returns either:
   *    Right(true): Send request to camera without any trouble
   *    Right(false): Could not send request, because of connection problem. In this situation a retry is allowed.
   *    Left(errorMessage): Could not send request, because of a severe problem. No retry allowed.
   */
  def jaiConnectionSendCameraStatus: Either[String, Boolean]

  // def jaiConectionSendCamereTrigger()
  // def jaiConectionSendAllCameraStatus()
}
