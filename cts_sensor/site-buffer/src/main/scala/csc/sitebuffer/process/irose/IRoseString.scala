package csc.sitebuffer.process.irose

/**
 * Class representing an incoming iRose string
 */
case class IRoseString(string: String, timestamp: Long) {
  def typeChar: Option[Char] = try {
    Some(string.charAt(18))
  } catch {
    case e: Exception ⇒
      None
  }

  def detectorChar: Option[Char] = try {
    typeChar match {
      case Some('1') ⇒ // Only return detector for vehicle messages
        Some(string.charAt(20))

      case None ⇒
        None
    }
  } catch {
    case e: Exception ⇒
      None
  }
}
