/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.messages

import csc.sitebuffer.messages.IRoseMessage
import akka.actor.{ ActorLogging, Actor, ActorRef }
import akka.event.LoggingReceive

/**
 * Actor responsible for filtering IRoseMessages.
 * The filterCondition provides the filter test:
 *   If true, a message is passed to the trueRecipients
 *   If false, a message is passed to the falseRecipients
 */
// TODO (RV): Too much like VehicleMessageFilter. Common behavior should be in one place. No time for that now.
class IRoseMessageFilter(filterCondition: (IRoseMessage) ⇒ Boolean, trueRecipients: Set[ActorRef], falseRecipients: Set[ActorRef]) extends Actor with ActorLogging {

  def receive = LoggingReceive {
    case iRoseMessage: IRoseMessage ⇒
      if (filterCondition(iRoseMessage)) {
        sendToRecipients(iRoseMessage, trueRecipients)
      } else {
        sendToRecipients(iRoseMessage, falseRecipients)
      }

    case anyRef: AnyRef ⇒ {
      log.error("Received unknown message: [%s]".format(anyRef))
    }
  }

  def sendToRecipients(message: Any, recipients: Set[ActorRef]) {
    for (recipient ← recipients) {
      recipient ! message
    }
  }

}
