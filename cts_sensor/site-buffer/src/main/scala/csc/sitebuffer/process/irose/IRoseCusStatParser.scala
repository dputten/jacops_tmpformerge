package csc.sitebuffer.process.irose

import scala.collection.mutable.ListBuffer
import csc.akka.logging.DirectLogging

case class IRoseLoopError(loopNr: String, state: String)

object IRoseCusStatParser {
  val error4 = "error (state = 4"
  val error5 = "error (state = 5"
  val error4Description = "state= 4 - IROSE2_STATE_ERR_HIGHFREQ"
  val error5Description = "state= 5 - IROSE2_STATE_ERR_LOWFREQ"
}

class IRoseCusStatParser() extends DirectLogging {
  /**
   * Return a list of IRoseLoopErrors with the specific loop number and the specific IRose error based on the state
   */
  def getIRoseLoopErrors(cusStatresult: String): ListBuffer[IRoseLoopError] = {
    log.debug("cusStatresult=" + cusStatresult)

    val loops: ListBuffer[String] = extractStringsWithIndexes(cusStatresult, getIndexesOfArgument(cusStatresult, "loop"))
    val loopsMetErrorsEnGeenLusAangesloten: ListBuffer[String] = getLoopsWithoutLoopConnected(loops)
    val iroseLoopErrors: ListBuffer[Option[IRoseLoopError]] = new ListBuffer[Option[IRoseLoopError]]()

    loopsMetErrorsEnGeenLusAangesloten.foreach(loop ⇒ iroseLoopErrors += getIRoseLoopError(loop))

    log.debug("iroseLoopErrors.flatten=" + iroseLoopErrors.flatten)

    iroseLoopErrors.flatten
  }

  /**
   * Return a IRoseLoopError with the specific loop number and the specific IRose error based on the state
   */
  def getIRoseLoopError(loop: String): Option[IRoseLoopError] = {
    // find the first : this is where the loop part ends.
    val endIndexOfLoop: Int = loop.indexOf(':')
    val loopNummer = loop.substring(endIndexOfLoop - 1, endIndexOfLoop)
    val isStatus4 = loop.contains(IRoseCusStatParser.error4)
    val isStatus5 = loop.contains(IRoseCusStatParser.error5)

    if (isStatus4) { Some(new IRoseLoopError(loopNummer, IRoseCusStatParser.error4Description)) }
    else if (isStatus5) { Some(new IRoseLoopError(loopNummer, IRoseCusStatParser.error5Description)) }
    else None
  }

  /**
   * Filter the list to only get the items containing the status 4 and 5
   */
  def getLoopsWithoutLoopConnected(loops: ListBuffer[String]): ListBuffer[String] = {
    log.debug("loops=" + loops)

    loops.filter(loop ⇒ loop.contains(IRoseCusStatParser.error4) || loop.contains(IRoseCusStatParser.error5))
  }

  /**
   * Get the startindexes of the argument
   */
  def getIndexesOfArgument(stringToSearch: String, searchArgument: String): List[Int] = {
    log.debug("stringToSearch=" + stringToSearch)
    log.debug("searchArgument=" + searchArgument)

    val regex = ("\\b" + searchArgument + "\\b").r
    val loopIndexes: List[Int] = (for (m ← regex.findAllIn(stringToSearch) matchData) yield (m.start)).toList
    loopIndexes
  }

  /**
   * Get the string starting at the index until the next index
   */
  def extractStringsWithIndexes(stringToSearch: String, indexes: List[Int]): ListBuffer[String] = {
    log.debug("in extractStringsWithIndexes")
    log.debug("stringToSearch=" + stringToSearch)
    log.debug("indexes=" + indexes)

    var result: ListBuffer[String] = new ListBuffer[String]()

    for (i ← 0 to indexes.length - 1 if indexes.length > 0) {
      if (i != indexes.length - 1)
        result += stringToSearch.substring(indexes(i), indexes(i + 1))
      else
        result += stringToSearch.substring(indexes(i))
    }
    log.debug("result=" + indexes)
    result
  }
}
