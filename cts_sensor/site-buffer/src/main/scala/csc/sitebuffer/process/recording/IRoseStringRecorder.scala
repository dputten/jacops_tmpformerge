/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.recording

import akka.actor._
import akka.event.LoggingReceive
import scala.Some
import csc.sitebuffer.process.irose.IRoseString

/**
 * Actor that manages the recording of incoming IRoseString's.
 * Recording is started and stopped by incoming events on the eventStream:
 * - StartRecordingInput(directory): start writing incoming IRoseString's to given directory.
 * - StopRecordingInput: stop recording.
 * A child IRoseStringWriter is created for each recording session (and killed afterwards).
 */
class IRoseStringRecorder extends Actor with ActorLogging {
  self: IRoseStringWriterCreator ⇒

  import context._

  var writer: Option[ActorRef] = None

  override def preStart() = {
    context.system.eventStream.subscribe(context.self, classOf[RecordEventMessage])
  }

  override def postStop() {
    context.system.eventStream.unsubscribe(context.self)
  }

  def receive = waitingForRecording

  def waitingForRecording: Receive = LoggingReceive {
    case StartRecordingInput(recordingId) ⇒
      log.info("Starting to record iRose messages. RecordingId = %s".format(recordingId))
      writer = Some(createWriter(recordingId, context))
      become(recording)

    case StopRecordingInput ⇒
      log.error("Received unexpected StopRecordingInput message [no recording in progress]")

    case IRoseString ⇒
    // Do nothing
  }

  def recording: Receive = LoggingReceive {
    case StartRecordingInput(recordingId) ⇒
      log.error("Received unexpected StartRecordingInput message [recording is already in progress]")

    case StopRecordingInput ⇒
      log.info("Stopping the recording of iRose messages")
      writer.foreach(_ ! PoisonPill)
      writer = None
      become(waitingForRecording)

    // TODO: failing to create writer should be handled through supervision
    case msg: IRoseString ⇒
      if (writer.isDefined && !writer.get.isTerminated) {
        writer.foreach(_ ! msg)
      } else {
        writer = None
        become(waitingForRecording)
      }
  }

}
