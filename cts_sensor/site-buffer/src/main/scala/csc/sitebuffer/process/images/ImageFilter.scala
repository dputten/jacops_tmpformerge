/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.images

import akka.actor.{ ActorRef, Actor, ActorLogging }
import scala.concurrent.duration.Duration
import java.io.File
import java.text.SimpleDateFormat

/**
 * Work around function: Test if the receive time and the creation time differs too much.
 * Due to and bug in the camera, the image is of the time it was send and not the mentioned creation time.
 * So when it differs too much skip the processing step otherwise process the message as normal
 * @param imageExtension The image extension
 * @param maxDelayTime The max difference between the receive time and the creation time
 * @param skipProcessing The next step after processing (cleanup)
 * @param recipients The normal next step of processing
 */
class ImageFilter(imageExtension: String, maxDelayTime: Duration, skipProcessing: Seq[ActorRef], recipients: Seq[ActorRef]) extends Actor with ActorLogging {
  private val dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS")
  def receive = {
    case msg: NewImage ⇒ {
      val fileName = new File(msg.imagePath).getName
      val imageCreation = ImageNameParser.getTimeFromName(fileName, if (imageExtension.isEmpty) None else Some(imageExtension))
      if (msg.timestamp >= (imageCreation + maxDelayTime.toMillis)) {
        log.warning("Skipping image %s %d (%s)".format(msg.imagePath, msg.timestamp, dateFormat.format(msg.timestamp)))
        skipProcessing.foreach(_ ! msg)
      } else {
        recipients.foreach(_ ! msg)
      }
    }
  }

}