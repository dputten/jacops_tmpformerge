/**
 * Copyright (C) 2012,2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.messages

import akka.actor.{ ActorLogging, Actor, ActorRef }
import akka.event.LoggingReceive
import csc.sitebuffer.messages.InputEventMessage
import csc.sitebuffer.process.SystemSensorMessage.{ EventType, Sensor }
import csc.sitebuffer.config.CabinetDoorConfig
import csc.sitebuffer.process.SystemSensorMessage

/**
 * Class responsible for handling incoming InputEventMessages from iRose.
 * (Except alarms, contained in these messages. These are handled by an IRoseAlarmMonitor.)
 */
class IRoseDigitalInputMonitor(cabinetDoorConfig: CabinetDoorConfig, systemSensorMessageSender: ActorRef) extends Actor with ActorLogging {

  def receive = LoggingReceive {
    case inputMessage: InputEventMessage if inputMessage.inputId != 8 ⇒ processMessage(inputMessage)
    case anyRef: AnyRef ⇒ {
      log.error("Received unknown message: [%s]".format(anyRef))
    }
  }

  private def processMessage(inputMessage: InputEventMessage) {
    transformToSystemSensorMessage(inputMessage) match {
      case Some(systemSensorMessage) ⇒ systemSensorMessageSender ! systemSensorMessage
      case None                      ⇒ log.error("Could not process message: [%s]".format(inputMessage))
    }
  }

  private def transformToSystemSensorMessage(inputMessage: InputEventMessage): Option[SystemSensorMessage] = {
    def createSystemSensorMessage(sensor: String, event: String): SystemSensorMessage = {
      SystemSensorMessage(sensor, inputMessage.dateTime, "", event, "Sitebuffer", Map())
    }

    (inputMessage.inputId, inputMessage.eventNumber) match {
      case (cabinetDoorConfig.inputNumber, cabinetDoorConfig.openEventNumber) ⇒ Some(createSystemSensorMessage(Sensor.Door, EventType.Open))
      case (cabinetDoorConfig.inputNumber, cabinetDoorConfig.closedEventNumber) ⇒ Some(createSystemSensorMessage(Sensor.Door, EventType.Closed))
      case _ ⇒ None
    }
  }

}
