/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.messages

import akka.actor.{ ActorLogging, Actor, ActorRef }
import akka.event.LoggingReceive
import csc.sitebuffer.messages.SensorMessage
import csc.sitebuffer.process.SystemSensorMessage.{ EventType, Sensor }
import csc.sitebuffer.config.SiteBufferConfiguration
import csc.sitebuffer.process.SystemSensorMessage

/**
 * Actor responsible for monitoring the iRose sensors, based on the incoming SensorMessages.
 */
class IRoseSensorFailureMonitor(config: SiteBufferConfiguration, systemSensorMessageSender: ActorRef) extends Actor with ActorLogging {
  // A counter for each monitored sensor.
  private var counters: Map[String, FailureCounter] = Map()

  // Small class that counts consecutive failures based on a failureTest function.
  class FailureCounter(countForAlarm: Int, failureTest: IRoseSensorFailureMonitor.FailureTest) {
    private var counter = 0

    def nextReading(message: SensorMessage) {
      if (failureTest(message))
        counter += 1
      else
        reset() // only interested in consecutive failed readings
    }
    def reset() { counter = 0 }
    def alarmReached = (counter >= countForAlarm)
  }

  override def preStart() {
    // Create a counter for each sensor.
    counters = IRoseSensorFailureMonitor.monitoredSensors.map(sensor ⇒
      (sensor, new FailureCounter(config.consecutiveFailedSensorReadingsCountForAlarm, IRoseSensorFailureMonitor.getFailureTest(sensor).get))).toMap
  }

  def receive = LoggingReceive {
    case sensorMessage: SensorMessage ⇒ processMessage(sensorMessage)
    case anyRef: AnyRef ⇒ {
      log.error("Received unknown message: [%s]".format(anyRef))
    }
  }

  private def processMessage(message: SensorMessage) {
    for (sensor ← IRoseSensorFailureMonitor.monitoredSensors) processMessageForSensor(message, sensor)
  }

  private def processMessageForSensor(message: SensorMessage, sensor: String) {
    counters.get(sensor) match {
      case Some(counter) ⇒
        counter.nextReading(message)
        if (counter.alarmReached) {
          systemSensorMessageSender ! createSystemSensorMessage(message, sensor)
          counter.reset()
        }
      case None ⇒
        log.error("No counter present for sensor [%s]".format(sensor))
    }
  }

  private def createSystemSensorMessage(message: SensorMessage, sensor: String): SystemSensorMessage = {
    SystemSensorMessage(sensor, message.dateTime, "", EventType.SensorFailure, "Sitebuffer", Map())
  }
}

object IRoseSensorFailureMonitor {
  val monitoredSensors = Seq(Sensor.Temperature, Sensor.Humidity, Sensor.Accelerometer)

  type FailureTest = SensorMessage ⇒ Boolean

  // Return a failure test function for given sensor.
  def getFailureTest(sensor: String): Option[FailureTest] = {
    val failureValue = 999.999

    def isFailure(value: Float): Boolean = {
      value >= failureValue
    }
    sensor match {
      case Sensor.Temperature   ⇒ Some((message: SensorMessage) ⇒ isFailure(message.temperature))
      case Sensor.Humidity      ⇒ Some((message: SensorMessage) ⇒ isFailure(message.humidity))
      case Sensor.Accelerometer ⇒ Some((message: SensorMessage) ⇒ isFailure(message.acceleration.x) || isFailure(message.acceleration.y) || isFailure(message.acceleration.z))
      case _                    ⇒ None
    }
  }
}
