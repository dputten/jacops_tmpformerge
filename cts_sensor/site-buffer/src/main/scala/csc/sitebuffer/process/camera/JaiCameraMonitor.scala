/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.camera

import akka.actor.{ ActorRef, ActorLogging, Actor }
import akka.event.LoggingReceive
import csc.vehicle.jai._
import csc.sitebuffer.config.CameraMonitorConfig
import csc.vehicle.jai.DisturbanceEvent
import csc.vehicle.jai.CameraError
import java.util.Date
import csc.sitebuffer.process.SystemSensorMessage.{ EventType, Sensor }
import scala.collection.immutable
import csc.sitebuffer.process.SystemSensorMessage

/**
 * Actor responsible for monitoring one Jai camera.
 * When receiving a CheckStatusRequest, it will ask the camera for status data. Problems with connecting will result in retries.
 * The request should be answered by the camera with a CameraStatus message. Any errors contained will result in SystemSensorMessages being send.
 *
 * The camera may also report DisturbanceEvent's to this actor. These will also result in a SystemSensorMessages being send.
 * Depending on the cameraMonitorConfig.alarmDelay setting, the actor will wait to see if the disturbance still exists before sending
 * a SystemSensorMessage.
 */
class JaiCameraMonitor(cameraId: String,
                       cameraMonitorConfig: CameraMonitorConfig,
                       getCurrentTime: () ⇒ Long,
                       systemSensorMessageSender: ActorRef) extends Actor with ActorLogging {
  actor: JaiConnection ⇒

  import JaiCameraMonitor._

  implicit val executor = context.system.dispatcher

  private var detectedDisturbances = new collection.mutable.HashMap[CameraError, DisturbanceEvent]()

  private var statusRequestPending = false
  private var lastRequestId = ""

  val tick = context.system.scheduler.schedule(cameraMonitorConfig.checkFrequency, cameraMonitorConfig.checkFrequency, self, CheckStatusRequest)

  override def preStart() {
    doCreateJaiConnection()
    doJaiConnectionStart()
  }

  override def postStop() {
    tick.cancel()
    doJaiConnectionShutdown()
  }

  def receive = LoggingReceive {
    handleStatusRequests orElse handleJaiConnectionMessages orElse handleUnknownMessages
  }

  /**
   * This partial function handles the CheckStatusRequest:
   * - CheckStatusRequest: request new status data from the camera
   * - StatusTimeout: check if answer (= CameraStatus) has been received, if not retry (if maximum not yet reached)
   */
  def handleStatusRequests: Receive = {
    case CheckStatusRequest if !statusRequestPending ⇒ {
      // Case: no request pending -> handle incoming request
      log.info("Requesting status from camera %s".format(cameraId))
      askCameraForStatus match {
        case Right(notImportant) ⇒
          // Schedule first timeout
          activateNewRequestId
          scheduleStatusTimeout(0)
          statusRequestPending = true
        case Left(error) ⇒
          sendSystemSensorMessage(new CameraError(DisturbanceTypes.INTERFACE_ERROR, Some(error)))
          statusRequestPending = false
      }
    }

    case CheckStatusRequest ⇒ {
      // Case: already request pending -> ignore incoming request
      log.info("CheckStatusRequest received for camera %s, but previous request is still pending. Message ignored".format(cameraId))
    }

    case timeout: StatusTimeout if statusRequestPending && (timeout.requestId == lastRequestId) ⇒ {
      // Case: This timeout belongs to active request -> handle timeout
      if (maxRetriesReached(timeout.nrRetries)) {
        // This timeout belongs to the last retry
        sendSystemSensorMessage(new CameraError(DisturbanceTypes.INTERFACE_ERROR, Some("No reply from camera %s after %d attempts".format(cameraId, cameraMonitorConfig.maxRetries))))
        statusRequestPending = false
      } else {
        val newRetryNumber = timeout.nrRetries + 1
        val isLastRetry = maxRetriesReached(newRetryNumber)
        // Send request again
        log.info("Requesting status from camera %s (retry %d)".format(cameraId, newRetryNumber))
        askCameraForStatus match {
          case Right(true) ⇒
            scheduleStatusTimeout(newRetryNumber)
          case Right(false) ⇒
            if (isLastRetry) {
              sendSystemSensorMessage(new CameraError(DisturbanceTypes.CAMERA_CONNECTION, Some("All %d attempts to connect to camera %s failed".format(cameraMonitorConfig.maxRetries, cameraId))))
              statusRequestPending = false
            } else {
              scheduleStatusTimeout(newRetryNumber)
            }
          case Left(error) ⇒
            sendSystemSensorMessage(new CameraError(DisturbanceTypes.INTERFACE_ERROR, Some(error)))
            statusRequestPending = false
        }
      }
    }

    case timeout: StatusTimeout ⇒ {
      // Case: This timeout does not belong to an active request -> ignore
      log.info("StatusTimeout received for camera %s, but belongs to previously handled request. Message ignored".format(cameraId))
    }
  }

  /**
   * This partial function handles the messages from the camera:
   * - CameraStatus: Reply from the camera to a CheckStatusRequest.
   * - DisturbanceEvent: Camera reports a change (= Detected or Solved) regarding a disturbance.
   *                     Either send SystemSensorMessage or DelayedDisturbanceCheck.
   * - DelayedDisturbanceCheck: Check if the passed disturbance still exists. If so, send SystemSensorMessage.
   */
  def handleJaiConnectionMessages: Receive = {
    case status: CameraStatus ⇒ {
      // Case: Status update received from camera
      for (error ← status.errors) {
        sendSystemSensorMessage(error)
      }
      statusRequestPending = false
    }

    case disturbanceEvent: DisturbanceEvent ⇒ {
      // Case: Disturbance reported by camera
      disturbanceEvent.state match {
        case DisturbanceState.DETECTED ⇒ {
          log.info("Camera %s: Detected disturbanceEvent [%s]".format(cameraId, disturbanceEvent))
          if (cameraMonitorConfig.alarmDelay.toMillis == 0) {
            sendSystemSensorMessage(disturbanceEvent)
          } else {
            context.system.scheduler.scheduleOnce(cameraMonitorConfig.alarmDelay, self, new DelayedDisturbanceCheck(disturbanceEvent))
            addDetectedDisturbance(disturbanceEvent)
          }
        }
        case DisturbanceState.SOLVED ⇒ {
          log.info("Camera %s: Solved disturbanceEvent [%s]".format(cameraId, disturbanceEvent))
          removeDetectedDisturbance(disturbanceEvent)
        }
      }
    }

    case disturbanceCheck: DelayedDisturbanceCheck ⇒ {
      // Case: Request to check if given disturbance still exists
      val disturbanceEvent = disturbanceCheck.disturbanceEvent
      log.info("Camera %s: DelayedDisturbanceCheck received [%s]".format(cameraId, disturbanceEvent))
      // Send message if disturbance still detected
      if (isDetectedDisturbance(disturbanceEvent)) {
        removeDetectedDisturbance(disturbanceEvent)
        sendSystemSensorMessage(disturbanceEvent)
      }
    }
  }

  def handleUnknownMessages: Receive = {
    case anyRef: AnyRef ⇒ {
      log.error("Received unknown message: [%s]".format(anyRef))
    }
  }

  private def maxRetriesReached(value: Int): Boolean = {
    value >= cameraMonitorConfig.maxRetries
  }

  private def doCreateJaiConnection() {
    createJaiConnection match {
      case Right(okMessage)   ⇒ log.info("Starting Camera monitor %s with path %s".format(cameraId, self.path))
      case Left(errorMessage) ⇒ log.error("Error while starting Camera monitor %s with path [%s]: [%s]".format(cameraId, self.path, errorMessage))
    }
  }

  private def doJaiConnectionStart() {
    jaiConnectionStart match {
      case Right(okMessage)   ⇒ log.info("Starting Camera monitor %s with path %s".format(cameraId, self.path))
      case Left(errorMessage) ⇒ log.error("Error while starting Camera monitor %s with path [%s]: [%s]".format(cameraId, self.path, errorMessage))
    }
  }

  private def doJaiConnectionShutdown() {
    jaiConnectionShutdown match {
      case Right(okMessage)   ⇒ log.info("Stopping Camera monitor %s with path %s".format(cameraId, self.path))
      case Left(errorMessage) ⇒ log.error("Error while stopping Camera monitor %s with path [%s]: [%s]".format(cameraId, self.path, errorMessage))
    }
  }

  /**
   * This method will ask the camera for status data
   * @return One of the following:
   *         - Right(true): Send request to camera without any trouble
   *         - Right(false): Could not send request, because of connection problem. In this situation a retry is allowed.
   *         - Left(error): Could not send request, because of a severe problem. No retry allowed.
   */
  private def askCameraForStatus: Either[String, Boolean] = {
    val result = jaiConnectionSendCameraStatus
    result match {
      case Right(true)  ⇒ log.info("Send status request to camera %s with path %s".format(cameraId, self.path))
      case Right(false) ⇒ log.info("Camera %s: Failed to send status request because of connection problem".format(cameraId))
      case Left(error)  ⇒ log.error("Camera %s: Failed to send status request [%s]".format(cameraId, error))
    }
    result
  }

  private def scheduleStatusTimeout(attempt: Int) {
    context.system.scheduler.scheduleOnce(cameraMonitorConfig.timeoutDuration, self, new StatusTimeout(lastRequestId, attempt))
  }

  private def sendSystemSensorMessage(error: CameraError) {
    val eventType = getEventTypeForDisturbanceType(error.eventType)
    systemSensorMessageSender ! createSystemSensorMessage(eventType = eventType, details = error.details)
  }

  private def sendSystemSensorMessage(event: DisturbanceEvent) {
    // Major horrible nomenclature alert!
    val eventType = getEventTypeForDisturbanceType(event.eventType.eventType)
    systemSensorMessageSender ! createSystemSensorMessage(time = event.timestamp.getTime, eventType = eventType, details = event.eventType.details)
  }

  def createSystemSensorMessage(eventType: String, time: Long = now, details: Option[String] = None): SystemSensorMessage = {
    var data = immutable.Map("cameraId" -> cameraId)
    if (details.isDefined)
      data = data + ("details" -> details.get)
    SystemSensorMessage(Sensor.Camera, time, "", eventType, "Sitebuffer", data)
  }

  private def getEventTypeForDisturbanceType(disturbance: String): String = {
    disturbance match {
      case DisturbanceTypes.CAMERA_CONNECTION      ⇒ EventType.CameraConnection
      case DisturbanceTypes.CAMERA_NTP_SYNC        ⇒ EventType.CameraNtpSync
      case DisturbanceTypes.CAMERA_FTP             ⇒ EventType.CameraFtp
      case DisturbanceTypes.CAMERA_COMM            ⇒ EventType.CameraFtp
      case DisturbanceTypes.CAMERA_HOUSING         ⇒ EventType.CameraHousing
      case DisturbanceTypes.CAMERA_TRIGGER_RETRIES ⇒ EventType.CameraTriggerRetries
      case DisturbanceTypes.INTERFACE_ERROR        ⇒ EventType.CameraInterfaceError
      case DisturbanceTypes.CAMERA_IMAGE           ⇒ EventType.CameraImageLightSensor
      case _                                       ⇒ EventType.CameraUnknown
    }
  }

  private def now = getCurrentTime()

  private def activateNewRequestId: String = {
    lastRequestId = cameraId + "-" + new Date().getTime
    lastRequestId
  }

  private def isDetectedDisturbance(disturbanceEvent: DisturbanceEvent): Boolean = {
    detectedDisturbances.contains(disturbanceEvent.eventType)
  }

  private def addDetectedDisturbance(disturbanceEvent: DisturbanceEvent) {
    if (!isDetectedDisturbance(disturbanceEvent)) {
      detectedDisturbances += disturbanceEvent.eventType -> disturbanceEvent
      //println("AFTER ADD: [%s]".format(detectedDisturbances))
    }
  }

  private def removeDetectedDisturbance(disturbanceEvent: DisturbanceEvent) {
    if (isDetectedDisturbance(disturbanceEvent)) {
      detectedDisturbances.remove(disturbanceEvent.eventType)
      //println("AFTER REMOVE: [%s]".format(detectedDisturbances))
    }
  }

}

object JaiCameraMonitor {
  // Message to request the status of the camera being monitored
  case object CheckStatusRequest
  // Reply to the StatusRequest message. Contains the status of the camera
  case class StatusResponse(cameraId: String, errors: Seq[CameraError])
  // Message used for handling retries of StatusRequest
  case class StatusTimeout(requestId: String, nrRetries: Int)
  // Message to check if error still exists
  case class DelayedDisturbanceCheck(disturbanceEvent: DisturbanceEvent)
}