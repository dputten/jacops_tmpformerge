/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.send

import akka.actor._
import scala.collection.immutable
import scala.concurrent.duration._
import scala.Some

/**
 * The type of message handled by the BufferedTextMessageSender actor.
 * @param text The text to be sent
 */
case class TextMessage(text: String)

/**
 * This actor receives TextMessage messages and puts them in a queue.
 * The messages in the queue are send one after the other. If sending fails,
 * the actor will keep retrying sending the same message (after a delay) until it succeeds. Only then any next message will be sent.
 *
 * A InOutSender is used for sending messages. Each send should be replied to by either a Success of a Failed.
 */
class BufferedTextMessageSender(retryDelayMs: Int) extends Actor
  with LoggingFSM[BufferedTextMessageSender.State, BufferedTextMessageSender.Data] {
  self: InOutTextSender ⇒

  import BufferedTextMessageSender._
  import InOutTextSender._

  startWith(Idle, ToDo(None, immutable.Queue()))

  // Idle: no data to process, waiting for next TextMessage.
  when(Idle) {
    case Event(message: TextMessage, todo @ ToDo(_, q)) ⇒
      // Put message into queue en go to ReadyForNext.
      goto(ReadyForNext) using todo.copy(None, q.enqueue(message))
  }

  // ReadyForNext: Nothing is being send, but check if something is available in queue to be sent.
  when(ReadyForNext) {
    case Event(message: TextMessage, todo @ ToDo(_, q)) ⇒
      // Put message into queue.
      stay using todo.copy(queue = q.enqueue(message))
    case Event(Next, todo @ ToDo(_, q)) if !(q.isEmpty) ⇒
      // Get message out of queue and make it current. Go to Sending.
      val (next, newQueue) = q.dequeue
      goto(Sending) using ToDo(Some(next), newQueue)
    case Event(Next, todo) ⇒
      // No next message available. Do nothing.
      goto(Idle) using todo
  }

  // Sending: message has been send, waiting for reply.
  when(Sending) {
    case Event(message: TextMessage, todo @ ToDo(_, q)) ⇒
      // Put message into queue.
      stay using todo.copy(queue = q.enqueue(message))

    case Event(Success, ToDo(_, q)) ⇒
      // Empty current and go to ReadyForNext.
      goto(ReadyForNext) using ToDo(None, q)

    case Event(Failure, todo @ ToDo(_, _)) ⇒
      // Go to WaitingForRetry.
      goto(WaitingForRetry) using todo
  }

  // WaitingForRetry: sending message has failed, waiting to retry.
  when(WaitingForRetry) {
    case Event(message: TextMessage, todo @ ToDo(_, q)) ⇒
      // Put message into queue.
      stay using todo.copy(queue = q.enqueue(message))

    case Event(Retry, todo) ⇒
      // Go to Sending.
      goto(Sending) using todo
  }

  onTransition {
    case _ -> ReadyForNext ⇒
      // Schedule Next, so any available message in queue will be sent.
      setTimer(name = "next", msg = Next, timeout = 10 millis, repeat = false)
    case Sending -> WaitingForRetry ⇒
      // Schedule Retry, so current message will be resent after delay.
      setTimer(name = "retry", msg = Retry, timeout = retryDelayMs millis, repeat = false)
    case _ -> Sending ⇒
      // Send current. (Should be filled).
      nextStateData match {
        case ToDo(Some(current), q) ⇒ send(current)
        case ToDo(None, q)          ⇒ log.error("No TextMessage present to send!")
      }
  }

  whenUnhandled {
    case Event(e, s) ⇒
      log.warning("Received unhandled request [%s] in state [%s]/[%s]".format(e, stateName, s))
      stay()
  }

  initialize

  private def send(message: TextMessage) {
    // Send content of message
    sendText(message.text)
  }
}

object BufferedTextMessageSender {
  case object Retry
  case object Next

  sealed trait State
  case object Idle extends State
  case object ReadyForNext extends State
  case object Sending extends State
  case object WaitingForRetry extends State

  sealed trait Data
  case class ToDo(current: Option[TextMessage], queue: immutable.Queue[TextMessage]) extends Data
}
