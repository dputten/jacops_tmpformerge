/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.system

import akka.actor.{ Cancellable, ActorRef, ActorLogging, Actor }
import akka.event.LoggingReceive
import csc.sitebuffer.process.SystemSensorMessage.{ Sensor, EventType }
import scala.concurrent.duration._
import csc.sitebuffer.process.SystemSensorMessage

/**
 * Actor responsible for monitoring whether system has been down.
 * This is done as follows:
 * - On creation this actor sends a DoWasDownCheck message to itself.
 * - When receiving this message the actor will perform this check (see below) and produce a SystemSensorMessage if the check fails.
 * - After performing the check, the actor will start a schedule for the UpdateTimestamp message.
 * - On receiving an UpdateTimestamp message the actor will store the current time in some TimestampStorage
 *
 * The WasDown sensor message is send, if the current time differs too much from the latest timestamp in TimestampStorage.
 * The minimalDeviationForWasDownInSeconds can be configured in the config file, as is the timestampUpdateFrequencyInSeconds.
 *
 * IMPORTANT: minimalDeviationForWasDownInSeconds > timestampUpdateFrequencyInSeconds
 */
class WasDownMonitor(timestampUpdateFrequency: FiniteDuration,
                     minimalDeviationForWasDown: Duration,
                     getCurrentTime: () ⇒ Long,
                     systemSensorMessageSender: ActorRef) extends Actor with ActorLogging { actor: TimestampStorage ⇒
  import WasDownMonitor._

  implicit val executor = context.system.dispatcher

  var updateTimestampSchedule: Option[Cancellable] = None

  override def preStart() {
    context.system.scheduler.scheduleOnce(10.millis, self, DoWasDownCheck)
  }

  override def postStop() {
    for {
      cancellable ← updateTimestampSchedule
    } cancellable.cancel()
  }

  def receive = LoggingReceive {
    case DoWasDownCheck ⇒
      performWasDownCheck()
      scheduleUpdateTimestamp()
    case UpdateTimestamp ⇒
      updateTimestamp()
    case anyRef: AnyRef ⇒ {
      log.error("Received unknown message: [%s]".format(anyRef))
    }
  }

  private def performWasDownCheck() {
    val lastSavedTimestamp = getLastSavedTimestamp
    val now = getCurrentTime()
    if (now - lastSavedTimestamp > minimalDeviationForWasDown.toMillis) {
      systemSensorMessageSender ! createSystemSensorMessage(now, lastSavedTimestamp)
    }
  }

  private def scheduleUpdateTimestamp() {
    updateTimestampSchedule = Some(context.system.scheduler.schedule(timestampUpdateFrequency, timestampUpdateFrequency, self, UpdateTimestamp))
  }

  private def updateTimestamp() {
    storeTimestamp(getCurrentTime())
  }

  private def getLastSavedTimestamp: Long = {
    getLatestTimestamp match {
      case Right(Some(timestamp)) ⇒
        timestamp

      case Right(None) ⇒
        log.info("No stored timestamp found")
        getCurrentTime() // Any problems reading timestamp won't result in an sensor event.

      case Left(error) ⇒
        log.error("Error retrieving stored timestamp: %s".format(error))
        getCurrentTime() // Any problems reading timestamp won't result in an sensor event.
    }
  }

  private def createSystemSensorMessage(now: Long, from: Long): SystemSensorMessage = {
    SystemSensorMessage(Sensor.WasDown, now, "", EventType.OutOfRange, "Sitebuffer", Map("downStartTime" -> from.toString))
  }

}

object WasDownMonitor {
  case object DoWasDownCheck
  case object UpdateTimestamp
}
