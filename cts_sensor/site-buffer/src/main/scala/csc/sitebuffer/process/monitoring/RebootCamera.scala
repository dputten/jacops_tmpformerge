/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.monitoring

import akka.actor.{ ActorLogging, ActorRef, Actor }
import csc.sitebuffer.config.CameraConfig
import csc.akka.process.{ ExecuteScriptResult, ExecuteScript }

class RebootCamera(config: Seq[CameraConfig], recipients: Seq[ActorRef]) extends Actor with ActorLogging {
  val scriptMap = config.map(cfg ⇒ (cfg.cameraId, cfg.rebootScript)).toMap
  def receive = {
    case error: CameraImageOutputError ⇒ {
      scriptMap.get(error.camera).foreach(script ⇒ {
        val msg = new ExecuteScript(script, Seq())
        recipients.foreach(_ ! msg)
      })
    }
    case fixed: CameraImageOutputFixed ⇒ //do nothing
    case result: ExecuteScriptResult ⇒ {
      result.resultCode match {
        case Right(returnCode) if (returnCode == 0) ⇒ log.info("Executed script [%s] successful to reboot camera".format(result.script.script))
        case Right(returnCode)                      ⇒ log.error("Failure in script [%s] received returncode [%d]".format(result.script.script, returnCode))
        case Left(throwable)                        ⇒ log.error("Failure while executing script [%s]".format(result.script.script), throwable)
      }
    }

  }

}