package csc.sitebuffer.messages

import java.util.TimeZone
import java.text.SimpleDateFormat
/**
 * Copyright (C) 2012,2013 CSC <http://www.csc.com>.
 * User: jwluiten
 * Date: 2/4/13
 */

/*
  Conversions. Convenience methods used as a matter of style
 */
object Int {
  def apply(s: String): Int = s.toInt
}

object Float {
  def apply(s: String): Float = s.replace(',', '.').toFloat
}

class DateParser(parseFormat: String, timeZone: String = "UTC") {
  private val format = new SimpleDateFormat(parseFormat)
  format.setTimeZone(java.util.TimeZone.getTimeZone(timeZone))

  def stringToDate(timeStamp: String) = format.parse(timeStamp)
  def stringToEpoch(timeStamp: String) = stringToDate(timeStamp).getTime
}

object Date {
  private val dateParser = new DateParser("yyyyMMdd;HHmmss,SSS", "UTC")

  def apply(s: String): java.util.Date = {

    val yy = Int(s.substring(0, 2))
    val dateString = if (yy < 70) "20" + s else "19" + s
    dateParser.stringToDate(dateString)
  }
}

/*
  direction of vehicle
 */
object Direction extends Enumeration {
  val Incoming = Value(-1, "Incoming")
  val Outgoing = Value(1, "Outgoing")
}

/*
  MessageTypes used in messages sent by iRose over TCP/IP
 */
object IRoseMessageType extends Enumeration {
  val SensorType = Value(0)
  val VehiclePassageType = Value(1)
  val InputEventType = Value(2)
  val LoopPassageType = Value(4)
}

/*
  Accelerometer measuring the tilt of the installation
 */
case class Accelerometer(x: Float, y: Float, z: Float)

/*
 Error messages issued by IRoseTransformer#transform in case of
 the iRose loop detector sends a message not conforming to the
 protocol or a message with an invalid CRC
 */
sealed abstract class IRoseError
case class InvalidCRCMessage(text: String) extends IRoseError
case class UnknownMessage(text: String) extends IRoseError
case class InvalidInputEventMessage(text: String) extends IRoseError

/*
 Case classes for messages with a valid CRC that also comply with
 the protocol.
 */
sealed trait IRoseMessage {
  def dateTime: Long
}

case class SensorMessage(override val dateTime: Long, temperature: Float, humidity: Float,
                         acceleration: Accelerometer) extends IRoseMessage

object SensorMessage {
  def apply(dateTime: String, temperature: String, humidity: String, x: String, y: String, z: String) = {

    val acceleration = Accelerometer(Float(x), Float(y), Float(z))
    new SensorMessage(Date(dateTime).getTime, Float(temperature), Float(humidity), acceleration)
  }
}

case class InputEventMessage(override val dateTime: Long, inputId: Long, eventNumber: Int, eventText: String) extends IRoseMessage {
  require(List(0, 1, 2, 3, 8).contains(inputId), "inputId not in range [0,1,2,3,8]")
}

/*
  Input events, sent by the loop-detector as they occur
  (e.g. "Cabinet door closed")
 */
object InputEventMessage {
  def apply(dateTime: String, inputId: String, eventNumber: String, eventText: String) =
    new InputEventMessage(Date(dateTime)getTime, Int(inputId), Int(eventNumber), eventText)
}

/*
  Single Loop messages indicate a malfunction of one of the loops in a pair. A single loop
  does not provide the information needed to calculate speed of length.
 */
case class SingleLoopMessage(override val dateTime: Long, loopId: Int, lastScanCycle: Int, coverTime: Int) extends IRoseMessage

object SingleLoopMessage {
  def apply(dateTime: String, loopId: String, lastScan: String, coverTime: String) =
    new SingleLoopMessage(Date(dateTime).getTime, Int(loopId), Int(lastScan), Int(coverTime))
}

/*
  Message containing all information about a vehicle that passed a loop pair.
 */
case class VehicleMessage(override val dateTime: Long, detector: Int, direction: Direction.Value,
                          speed: Float, length: Float, loop1RiseTime: Long, loop1FallTime: Long,
                          loop2RiseTime: Long, loop2FallTime: Long, yellowTime2: Int, redTime2: Int,
                          dateTime1: Long, yellowTime1: Int, redTime1: Int, dateTime3: Long,
                          yellowTime3: Int, redTime3: Int, valid: Boolean,
                          serialNr: String, offset1: Int, offset2: Int, offset3: Int) extends IRoseMessage {
  require(detector >= 0 && detector < 8, "detector not in range [0..7]")

  def dateTime2 = dateTime // Convenience method
}

object VehicleMessage {
  /**
   * Convert YellowTime or RedTime from String to Long (milliseconds)
   * @param time Incoming time string
   * @return milliSeconds (Long)
   */
  private def float_str_to_ms(time: String): Int = {
    val time_re = "(\\d+),(\\d+)".r
    time match {
      /* guaranteed to match since the string was already parsed by the IRoseTransformer */
      case time_re(int, fract) ⇒
        1000 * Int(int) + 100 * Int(fract.substring(0, 1))
    }
  }

  /**
   * Transform valid field from incoming vehicle message to a Boolean.
   * This valid ("geldig") has a strange encoding: 0 means "valid", 1 means "not valid".
   * @param valid Incoming valid string. Either "0" or "1".
   * @return true (if valid = "0"), else false
   */
  private def string_to_valid(valid: String): Boolean = {
    (valid == "0")
  }

  def apply(dateTime: Long, detector: Int, direction: Direction.Value,
            speed: Float, length: Float, loop1RiseTime: Long, loop1FallTime: Long,
            loop2RiseTime: Long, loop2FallTime: Long, yellowTime2: Int, redTime2: Int,
            dateTime1: Long, yellowTime1: Int, redTime1: Int, dateTime3: Long,
            yellowTime3: Int, redTime3: Int, valid: Boolean) = {
    new VehicleMessage(dateTime, detector, direction, speed, length, loop1RiseTime, loop1FallTime,
      loop2RiseTime, loop2FallTime, yellowTime2, redTime2,
      dateTime1, yellowTime1, redTime1, dateTime3,
      yellowTime3, redTime3, valid, "DummyValue", 0, 0, 0)
  }

  def apply(dateTime: String, detector: String, speed: String, length: String, rise1: String, fall1: String,
            rise2: String, fall2: String, yellowTime2: String, redTime2: String,
            dateTime1: String, yellowTime1: String, redTime1: String, dateTime3: String,
            yellowTime3: String, redTime3: String, valid: String) = {
    val speed_f = Float(speed)
    val length_f = Float(length) / 10
    val direction = if (speed_f < 0) Direction.Incoming else Direction.Outgoing
    new VehicleMessage(Date(dateTime).getTime, Int(detector), direction, speed_f.abs, length_f, Int(rise1), Int(fall1),
      Int(rise2), Int(fall2), float_str_to_ms(yellowTime2), float_str_to_ms(redTime2),
      Date(dateTime1).getTime, float_str_to_ms(yellowTime1), float_str_to_ms(redTime1), Date(dateTime3).getTime,
      float_str_to_ms(yellowTime3), float_str_to_ms(redTime3), string_to_valid(valid), "DummyValue", 0, 0, 0)
  }

  def apply(dateTime: String, detector: String, speed: String, length: String, rise1: String, fall1: String,
            rise2: String, fall2: String, yellowTime2: String, redTime2: String,
            dateTime1: String, yellowTime1: String, redTime1: String, dateTime3: String,
            yellowTime3: String, redTime3: String, valid: String, serial: String, offset1: String, offset2: String, offset3: String) = {
    val speed_f = Float(speed)
    val length_f = Float(length) / 10
    val direction = if (speed_f < 0) Direction.Incoming else Direction.Outgoing
    new VehicleMessage(Date(dateTime).getTime, Int(detector), direction, speed_f.abs, length_f, Int(rise1), Int(fall1),
      Int(rise2), Int(fall2), float_str_to_ms(yellowTime2), float_str_to_ms(redTime2),
      Date(dateTime1).getTime, float_str_to_ms(yellowTime1), float_str_to_ms(redTime1), Date(dateTime3).getTime,
      float_str_to_ms(yellowTime3), float_str_to_ms(redTime3), string_to_valid(valid), serial, Int(offset1), Int(offset2), Int(offset3))
  }
}

/*
  Message used for triggering an IsAlive check.
 */
case class IsAliveCheck()

