/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.irose

import akka.actor.{ ActorRef, ActorLogging, Actor }
import akka.event.LoggingReceive
import csc.sitebuffer.messages.{ IRoseMessage, IRoseError }

/**
 * Actor that transforms incoming IRoseString's into the corresponding IRoseMessage's.
 */
class IRoseStringParser(
  transform: (String) ⇒ Either[IRoseError, IRoseMessage],
  recipients: Set[ActorRef]) extends Actor with ActorLogging {

  def receive = LoggingReceive {
    case msg: IRoseString ⇒ {
      transform(msg.string) match {
        case Right(iRoseMessage) ⇒ sendToRecipients(iRoseMessage, recipients)
        case Left(error)         ⇒ invalidMessageReceived(error)
      }
    }

    case msg: Any ⇒
      unknownMessageReceived(msg.toString)
  }

  def sendToRecipients(message: Any, recipients: Set[ActorRef]) {
    for (recipient ← recipients) {
      recipient ! message
    }
  }

  def invalidMessageReceived(message: IRoseError) {
    log.error("Invalid message received: [%s]".format(message))
  }

  def unknownMessageReceived(message: String) {
    log.error("Unknown message received: [%s]".format(message))
  }

}
