/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.send

import akka.actor.{ Props, Actor }

/**
 * This trait represents the ability of an Actor to send text messages over TCP.
 * Each call to sendText should return either a Success or Failure message.
 */
trait TextlineTcpSender extends InOutTextSender { actor: Actor ⇒
  def host: String
  def port: Int

  lazy val producerActor = context.actorOf(Props(new TextlineTcpProducer(host, port)), "TextlineTcpProducer")

  def sendText(text: String) {
    producerActor ! text
  }
}
