package csc.sitebuffer.process.messages

import akka.actor.{ ActorLogging, Actor, ActorRef }
import akka.event.LoggingReceive
import csc.sitebuffer.messages.VehicleMessage
import csc.sitebuffer.process.SystemSensorMessage.{ EventType, Sensor }
import csc.sitebuffer.process.SystemSensorMessage

/**
 * Actor responsible creating and sending SystemSensorMessages for each failed VehicleMessages.
 * This actor does NOT determine if a VehicleMessage is a failed VehicleMessage!
 */
class FailedVehicleMessageMonitor(getCurrentTime: () ⇒ Long, systemSensorMessageSender: ActorRef) extends Actor with ActorLogging {

  def receive = LoggingReceive {
    case failedVehicleMessage: VehicleMessage ⇒
      processMessage(failedVehicleMessage)

    case anyRef: AnyRef ⇒ {
      log.error("Received unknown message: [%s]".format(anyRef))
    }
  }

  private def processMessage(message: VehicleMessage) {
    systemSensorMessageSender ! createSystemSensorMessage(message)
  }

  def createSystemSensorMessage(message: VehicleMessage): SystemSensorMessage = {
    // Use now as timestamp because contents of message cannot be trusted
    SystemSensorMessage(Sensor.Oscillator, now, "", EventType.SensorFailure, "Sitebuffer", Map())
  }

  def now = getCurrentTime()
}
