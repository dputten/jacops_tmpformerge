package csc.sitebuffer.process.throttling

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 4/9/14.
 */

import akka.actor.{ Cancellable, ActorLogging, Actor }
import scala.concurrent.duration._
import csc.sitebuffer.config.FileThrottleConfig
import java.io.File

case class PollCommand()
case class PollDirectoryCommand(dir: File)

class FileThrottleActor(config: FileThrottleConfig) extends Actor with ActorLogging {

  implicit val executor = context.system.dispatcher

  protected var scheduledTask: Cancellable = _

  override def preStart() {
    // scheduling the task (with the 'self') should be the last statement in preStart()
    scheduledTask = context.system.scheduler.schedule(1000 millis, config.interval_ms millis, self, PollCommand)
  }
  override def postStop() {
    scheduledTask.cancel()
  }

  def receive = {
    case PollCommand               ⇒ poll
    case PollDirectoryCommand(dir) ⇒ pollDirectory(dir)
    case unknown                   ⇒ log.error("Received unknown message: {}", unknown)
  }

  protected def poll: Unit = {
    val inputDirectories = getInputDirectories
    inputDirectories.foreach({
      dir ⇒ self ! PollDirectoryCommand(dir)
    })
  }

  protected def pollDirectory(inputDir: File): Unit = {
    val baseName = inputDir.getName
    val outputDir: File = getOutputDir(inputDir) //new File(config.outputDirectory, baseName)
    val throttler = new FileThrottler(inputDir, outputDir, config.threshold,
      { s: Seq[File] ⇒ s.sortBy(f ⇒ f.getName) },
      { FileFilters.regexFilter(config.fileMask) })

    throttler.poll
  }

  protected def getInputDirectories: Seq[File] = {
    val availableFiles: Seq[File] = config.inputDirectory.listFiles().toSeq
    // get directories that match the mask
    availableFiles.filter({
      file ⇒ file.isDirectory && file.getName.matches(config.subDirectoryMask)
    })
  }

  protected def getOutputDir(inputDir: File): File = {
    val directoryName = inputDir.getName
    val newPrefix = config.newDirectoryPrefix
    require(directoryName.length > newPrefix.length)
    val outputDirectoryName = if (newPrefix.length > 0) {
      newPrefix + directoryName.drop(newPrefix.length)
    } else {
      directoryName
    }
    new File(config.outputDirectory, outputDirectoryName)
  }
}

