/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.monitoring

import akka.actor.{ ActorLogging, ActorRef, Actor }
import csc.sitebuffer.process.SystemSensorMessage

class SendCameraSystemEvent(waitForFixTime: Long, systemSensorMessage: ActorRef) extends Actor with ActorLogging {
  var lastErrorSend: Option[CameraImageOutputError] = None

  def receive = {
    case start: CameraImageOutputError ⇒ {
      log.error("Received a CameraImageOutputError: " + start)
      if (start.startTime + waitForFixTime <= start.currentTime) {
        systemSensorMessage ! SystemSensorMessage(
          sensor = SystemSensorMessage.Sensor.Camera,
          timeStamp = start.startTime,
          systemId = "",
          event = SystemSensorMessage.EventType.CameraUploadCorrectionFailed,
          sendingComponent = "Sitebuffer",
          data = Map("CameraId" -> start.camera))
        lastErrorSend = Some(start)
        log.info("Send CameraUploadError " + start)
      } else {
        log.info("Wait for CameraUploadFixed: %d < %d".format(start.currentTime - start.startTime, waitForFixTime))
      }
    }
    case end: CameraImageOutputFixed ⇒ {
      val lastErrorSendTime = lastErrorSend.map(_.startTime).getOrElse(0L)
      val msg = if (end.startTime == lastErrorSendTime) {
        // Error is already send
        // just send the fix message
        log.info("Send CameraUploadFixed " + end)
        SystemSensorMessage(sensor = SystemSensorMessage.Sensor.Camera,
          timeStamp = end.endTime,
          systemId = "",
          event = SystemSensorMessage.EventType.CameraUploadSolved,
          sendingComponent = "Sitebuffer",
          data = Map("CameraId" -> end.camera))
      } else {
        //send error with downStartTime
        log.info("Send CameraUploadError " + end)
        SystemSensorMessage(
          sensor = SystemSensorMessage.Sensor.Camera,
          timeStamp = end.endTime,
          systemId = "",
          event = SystemSensorMessage.EventType.CameraUploadFailed,
          sendingComponent = "Sitebuffer",
          data = Map("downStartTime" -> end.startTime.toString,
            "CameraId" -> end.camera))
      }
      systemSensorMessage ! msg
      lastErrorSend = None
    }
  }
}