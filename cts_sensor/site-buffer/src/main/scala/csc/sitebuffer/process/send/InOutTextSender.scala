/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.send

import akka.actor.Actor

/**
 * This trait represents the ability of an Actor to send text messages, using the sendText method.
 * Each call to sendText should return either a Success or Failure message.
 */
trait InOutTextSender { self: Actor ⇒
  def sendText(text: String)
}

object InOutTextSender {
  // Message returned to the sender
  case object Success
  case object Failure
}
