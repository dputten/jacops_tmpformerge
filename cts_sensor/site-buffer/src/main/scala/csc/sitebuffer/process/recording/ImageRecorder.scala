/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.recording

import akka.actor._
import akka.event.LoggingReceive
import csc.sitebuffer.process.images.NewImage
import java.io.{ FileInputStream, FileOutputStream, File }

/**
 * Actor that manages the recording of incoming NewImage's.
 * Recording is started and stopped by incoming events on the eventStream:
 * - StartRecordingInput(directory): start writing incoming NewImage's (somewhere under given base directory).
 * - StopRecordingInput: stop recording.
 *
 * If imageFilenamesOnly = true, only the names of images will be recorded. (empty files)
 */
class ImageRecorder(baseDirectory: File, imageFilenamesOnly: Boolean, recipients: Set[ActorRef]) extends Actor with ActorLogging {
  import context._

  var currentRecording: Option[Recording] = None

  override def preStart() = {
    context.system.eventStream.subscribe(context.self, classOf[RecordEventMessage])
  }

  override def postStop() {
    context.system.eventStream.unsubscribe(context.self)
  }

  def receive = waitingForRecording

  def waitingForRecording: Receive = LoggingReceive {
    case StartRecordingInput(recordingId) ⇒
      tryToStartRecording(recordingId) match {
        case Some(newRecording) ⇒
          currentRecording = Some(newRecording)
          log.info("Starting to record images. RecordingId = %s".format(recordingId))
          become(recording)

        case None ⇒
          currentRecording = None
          log.error("Could not start recording. RecordingId = %s".format(recordingId))
      }

    case StopRecordingInput ⇒
      log.error("Received unexpected StopRecordingInput message [no recording in progress]")

    case newImage: NewImage ⇒
      sendToRecipients(newImage)
  }

  def recording: Receive = LoggingReceive {
    case StartRecordingInput(recordingId) ⇒
      log.error("Received unexpected StartRecordingInput message [recording is already in progress]")

    case StopRecordingInput ⇒
      log.info("Stopping the recording of images")
      currentRecording = None
      become(waitingForRecording)

    case newImage: NewImage ⇒
      recordImage(newImage)
      sendToRecipients(newImage)
  }

  def recordImage(newImage: NewImage): Unit = {
    for {
      recording ← getMandatoryCurrentRecording
      imageFile ← makeFileForPath(newImage.imagePath)
      targetDirectory ← getTargetDirectory(imageFile, recording)
      newFile ← getTargetFile(newImage, targetDirectory, imageFile)
    } copyFile(imageFile, newFile)
  }

  def copyFile(fromFile: File, toFile: File): Unit = try {
    if (imageFilenamesOnly) {
      toFile.createNewFile()
    } else {
      new FileOutputStream(toFile).getChannel.transferFrom(new FileInputStream(fromFile).getChannel, 0, Long.MaxValue)
    }
  } catch {
    case e: Exception ⇒
      log.error("Error during copyFile(fromFile = %s, toFile = %s) [%s]".format(fromFile.getCanonicalPath, toFile.getCanonicalPath, e.getMessage))
  }

  // Simply returns currentRecording, logs an error if None.
  def getMandatoryCurrentRecording: Option[Recording] = {
    if (currentRecording.isEmpty) {
      log.error("Cannot record image, because there is no data for current recording.")
      None
    }
    currentRecording
  }

  def makeFileForPath(imagePath: String): Option[File] = try {
    Some(new File(imagePath))
  } catch {
    case e: Exception ⇒
      log.error("Image [%s] cannot be processed: could not create a file object. [%s]".format(imagePath, e.getMessage))
      None
  }

  /**
   * Determines, based on an image file and current recording, the directory this file should be copied to.
   * If image file is in directory dir1/dir2/dir3, then the target directory will be baseDir/recordingId/dir3,
   * The directory will be created if needed.
   * @param imageFile the file for which the target will be determined.
   * @param recording the current recording.
   * @return the target directory for given image file.
   */
  def getTargetDirectory(imageFile: File, recording: Recording): Option[File] = try {
    val currentDirectory = imageFile.getParentFile
    val targetDirectory = new File(recording.directory, currentDirectory.getName)
    if (!targetDirectory.exists()) {
      if (!targetDirectory.mkdirs()) {
        throw new Exception("Directory could not be created")
      }
    }
    Some(targetDirectory)
  } catch {
    case e: Exception ⇒
      log.error("Could not get/create target directory for [%s]. [%s]".format(imageFile.getCanonicalPath, e.getMessage))
      None
  }

  /**
   * Determine the new name of the imageFile in the targetDirectory. Will contain the timestamp from NewImage.
   * If the file name is 02020130712091244.324.jpg and the timestamp is 99999999, then the new name will be
   * 99999999-02020130712091244.324.jpg
   */
  def getTargetFile(newImage: NewImage, targetDirectory: File, imageFile: File): Option[File] = try {
    Some(new File(targetDirectory, "%d-%s".format(newImage.timestamp, imageFile.getName)))
  } catch {
    case e: Exception ⇒
      log.error("Could not create target file for [%s]. [%s]".format(imageFile.getCanonicalPath, e.getMessage))
      None
  }

  def sendToRecipients(message: Any) {
    for (recipient ← recipients) {
      recipient ! message
    }
  }

  /**
   * Will create the directory that will be used for this recording.
   * @param recordingId The id for this recording.
   * @return Recording instance for this recording.
   */
  def tryToStartRecording(recordingId: String): Option[Recording] = {
    val recordingDirectory = new File(baseDirectory, recordingDirectoryName(recordingId))
    val recordingMessagesDirectory = new File(recordingDirectory, "images")

    createDirectory(recordingMessagesDirectory) match {
      case Some(dir) ⇒
        Some(Recording(recordingId, dir))

      case None ⇒
        None
    }
  }

  def recordingDirectoryName(recordingId: String): String = {
    recordingId.trim
  }

  def createDirectory(dir: File): Option[File] = try {
    dir.mkdirs()
    Some(dir)
  } catch {
    case e: Exception ⇒
      log.error("Could not create directory %s. Error =  %s".format(dir.getCanonicalPath, e))
      None
  }

  case class Recording(recordingId: String, directory: File)
}
