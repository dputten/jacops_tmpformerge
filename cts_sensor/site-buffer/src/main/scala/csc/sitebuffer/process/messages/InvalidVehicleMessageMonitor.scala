/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.messages

import akka.actor.{ ActorRef, ActorLogging, Actor }
import akka.event.LoggingReceive
import csc.sitebuffer.messages.VehicleMessage
import csc.sitebuffer.process.SystemSensorMessage.{ EventType, Sensor }
import csc.sitebuffer.process.SystemSensorMessage

/**
 * Actor responsible creating and sending SystemSensorMessages for each invalid VehicleMessages.
 */
class InvalidVehicleMessageMonitor(minimalSpeedForProcessingInvalidVehicleMessageKmh: Int,
                                   systemSensorMessageSender: ActorRef) extends Actor with ActorLogging {
  val minimalSpeedAsFloat = minimalSpeedForProcessingInvalidVehicleMessageKmh.toFloat

  def receive = LoggingReceive {
    case vehicleMessage: VehicleMessage ⇒
      if (vehicleMessage.valid)
        log.debug("Received a valid VehicleMessage: [%s]".format(vehicleMessage))
      else
        processMessage(vehicleMessage)

    case anyRef: AnyRef ⇒ {
      log.error("Received unknown message: [%s]".format(anyRef))
    }
  }

  private def processMessage(message: VehicleMessage) {
    if (message.speed < minimalSpeedAsFloat) {
      // No further processing as SystemSensorMessage, just log.
      log.debug("Received an invalid VehicleMessage: [%s]. Speed to low for further processing.".format(message))
    } else {
      systemSensorMessageSender ! createSystemSensorMessage(message)
    }
  }

  def createSystemSensorMessage(message: VehicleMessage): SystemSensorMessage = {
    SystemSensorMessage(Sensor.Speed, message.dateTime2, "", EventType.OutOfRange, "Sitebuffer", Map())
  }

}
