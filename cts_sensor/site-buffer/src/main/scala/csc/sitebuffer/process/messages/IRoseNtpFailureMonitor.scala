/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.messages

import akka.actor.{ ActorLogging, Actor, ActorRef }
import scala.collection.mutable
import csc.sitebuffer.messages.IRoseMessage
import akka.event.LoggingReceive
import csc.sitebuffer.process.SystemSensorMessage.{ EventType, Sensor }
import csc.sitebuffer.process.SystemSensorMessage

/**
 * Class responsible for handling IRoseMessages.
 * If more than [maxAllowedMessages] messages are received within [testWindowMs] milliseconds with a 1970 date
 * a SystemSensorMessage will be created.
 */
class IRoseNtpFailureMonitor(maxAllowedMessages: Int, testWindowMs: Int, getCurrentTime: () ⇒ Long, systemSensorMessageSender: ActorRef) extends Actor with ActorLogging {
  // A failure monitor for each encountered loopId.
  private val monitor: FailureMonitor = FailureMonitor(maxAllowedMessages, testWindowMs)

  // Small class that monitors a
  case class FailureMonitor(maxAllowedMessages: Int, sampleDurationMs: Int) {
    private val data = mutable.Queue[Long]()

    def processNextReading(message: IRoseMessage): Boolean = {
      val now = getCurrentTime()
      data.enqueue(now)
      cleanup(now - sampleDurationMs)
      // Return whether failure situation has arisen
      data.size > maxAllowedMessages
    }
    private def cleanup(datetimeLimit: Long) {
      // Remove all entries older than datetimeLimit
      data.dequeueAll(_ <= datetimeLimit)
    }
    def reset { data.clear() }
  }

  def receive = LoggingReceive {
    case message: IRoseMessage ⇒
      if (in1970(message.dateTime)) {
        processMessage(message)
      }
    case anyRef: AnyRef ⇒ {
      log.error("Received unknown message: [%s]".format(anyRef))
    }
  }

  private def processMessage(message: IRoseMessage) {
    monitor.processNextReading(message) match {
      case true ⇒
        systemSensorMessageSender ! createSystemSensorMessage
        monitor.reset
      case false ⇒
    }
  }

  private val firstDayOf1971 = 31536000000L

  private def in1970(epochTime: Long): Boolean = epochTime < firstDayOf1971

  private def createSystemSensorMessage: SystemSensorMessage = {
    SystemSensorMessage(Sensor.Ntp, getCurrentTime(), "", EventType.SensorFailure, "Sitebuffer", Map())
  }

}
