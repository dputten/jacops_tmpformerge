/**
 * Copyright (C) 2012,2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.messages

import akka.actor.{ ActorRef, ActorLogging, Actor }
import csc.sitebuffer.messages._
import akka.event.LoggingReceive
import csc.sitebuffer.process.SystemSensorMessage.{ EventType, Sensor }
import csc.sitebuffer.process.SystemSensorMessage

/**
 * Class responsible for handling incoming alarms from iRose.
 * Alarms are InputEventMessages with inputId = 8.
 */
class IRoseAlarmMonitor(systemSensorMessageSender: ActorRef) extends Actor with ActorLogging {

  def receive = LoggingReceive {
    case inputMessage: InputEventMessage if inputMessage.inputId == 8 ⇒ processMessage(inputMessage)
    case anyRef: AnyRef ⇒ {
      log.error("Received unknown message: [%s]".format(anyRef))
    }
  }

  private def processMessage(inputMessage: InputEventMessage) {
    transformToSystemSensorMessage(inputMessage) match {
      case Some(systemSensorMessage) ⇒ systemSensorMessageSender ! systemSensorMessage
      case None                      ⇒ log.error("Could not process message: [%s]".format(inputMessage))
    }
  }

  private def transformToSystemSensorMessage(inputMessage: InputEventMessage): Option[SystemSensorMessage] = {
    def createSystemSensorMessage(sensor: String, event: String, data: Map[String, String] = Map()): SystemSensorMessage = {
      SystemSensorMessage(sensor, inputMessage.dateTime, "", event, "Sitebuffer", data)
    }

    inputMessage.eventNumber match {
      case 0 ⇒ Some(createSystemSensorMessage(Sensor.Temperature, EventType.OutOfRange, Map("details" -> "Too cold")))
      case 1 ⇒ Some(createSystemSensorMessage(Sensor.Temperature, EventType.OutOfRange, Map("details" -> "Too hot")))
      case 2 ⇒ Some(createSystemSensorMessage(Sensor.Humidity, EventType.OutOfRange)) // 2 = too humid
      case 3 ⇒ Some(createSystemSensorMessage(Sensor.Accelerometer, EventType.OutOfRange)) // 3 = leaning too much
      case 4 ⇒ Some(createSystemSensorMessage(Sensor.Oscillator, EventType.OutOfRange, Map("details" -> "Oscillator A and B differ more than 5%")))
      case 5 ⇒ Some(createSystemSensorMessage(Sensor.Oscillator, EventType.OutOfRange, Map("details" -> "Frequency step > 1% in oscillator A, not in B")))
      case 6 ⇒ Some(createSystemSensorMessage(Sensor.Oscillator, EventType.OutOfRange, Map("details" -> "Frequency step > 1% in oscillator B, not in A")))
      case _ ⇒ None
    }
  }

}
