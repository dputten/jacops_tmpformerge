/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.send

import akka.actor.{ ActorRef, ActorLogging, Actor }
import akka.event.LoggingReceive
import scala.collection.mutable
import csc.sitebuffer.process.SystemSensorMessage

/**
 * This actor receives SystemSensorMessage's and will only pass on those messages to nextActor that
 * have not already been send during the filterWindowMs.
 */
class SystemSensorMessageFilter(filterWindowMs: Int, getCurrentTime: () ⇒ Long, nextActor: ActorRef) extends Actor with ActorLogging {

  private val lastMessageTimes: mutable.Map[SystemSensorMessageFilterData, Long] = mutable.Map()

  def receive = LoggingReceive {
    case message: SystemSensorMessage ⇒ processMessage(message)
    case anyRef: AnyRef ⇒ {
      log.error("Received unknown message: [%s]".format(anyRef))
    }
  }

  private def processMessage(message: SystemSensorMessage) {
    val currentTime = getCurrentTime()
    val filterData = SystemSensorMessageFilterData(message)
    filterThisMessage(filterData, currentTime) match {
      case false ⇒
        // Send message and update lastMessageTime for this type of message
        nextActor ! message
        lastMessageTimes += (filterData -> currentTime)
      case true ⇒
    }
  }

  private def filterThisMessage(filterData: SystemSensorMessageFilterData, currentTime: Long): Boolean = {
    val lastTime = lastMessageTimes.getOrElse(filterData, -99999999L)
    // Filter this message if the last time a similar message occurred was within the filterWindowMs.
    (currentTime - lastTime) < filterWindowMs
  }

}

/**
 * Lite version of SystemSensorMessage containing only those fields that make up the type of a SystemSensorMessage.
 * If the value of any of these fields is different for 2 instances, then they have a different type. (And thus will have a separate entry in lastMessageTimes.)
 */
case class SystemSensorMessageFilterData(sensor: String, event: String, sendingComponent: String, data: Map[String, String])

object SystemSensorMessageFilterData {
  def apply(systemSensorMessage: SystemSensorMessage) = {
    new SystemSensorMessageFilterData(systemSensorMessage.sensor, systemSensorMessage.event, systemSensorMessage.sendingComponent, systemSensorMessage.data)
  }
}