/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process

/**
 * Class representing a message send to the system from or about a sensor.
 * @param sensor The sensor this message refers to
 * @param timeStamp Timestamp for this message
 * @param systemId Id of the system the sending component belongs to.
 * @param event Event this message refers to
 * @param sendingComponent Component that created this message.
 * @param data Any additional data.
 */
case class SystemSensorMessage(
  sensor: String,
  timeStamp: Long,
  systemId: String,
  event: String,
  sendingComponent: String,
  data: Map[String, String])

object SystemSensorMessage {
  object Sensor {
    val Door = "Door"
    val Temperature = "Temperature"
    val Humidity = "Humidity"
    val Accelerometer = "Accelerometer"
    val Camera = "Camera"
    val Clock = "Clock"
    val DetectionLoop = "DetectionLoop"
    val IsAlive = "IsAlive"
    val Speed = "Speed"
    val Oscillator = "Oscillator"
    val Ntp = "Ntp"
    val WasDown = "WasDown"
  }
  object EventType {
    val Open = "Open"
    val Closed = "Closed"
    val OutOfRange = "OutOfRange"
    val SensorFailure = "SensorFailure"
    val Deviation = "Deviation"
    val CameraConnection = "CameraConnection"
    val CameraNtpSync = "CameraNtpSync"
    val CameraFtp = "CameraFtp"
    val CameraComm = "CameraComm"
    val CameraHousing = "CameraHousing"
    val CameraTriggerRetries = "CameraTriggerRetries"
    val CameraInterfaceError = "CameraInterfaceError"
    val CameraImageLightSensor = "CameraImageLightSensor"
    val CameraUploadFailed = "CameraUploadFailed"
    val CameraUploadSolved = "CameraUploadSolved"
    val CameraUploadCorrectionFailed = "CameraUploadCorrectionFailed"
    val CameraUnknown = "CameraUnknown"
    val DetectionLoopState = "DetectionLoopState"
  }
}