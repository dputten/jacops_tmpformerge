/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.images

import util.matching.Regex
import java.text.{ SimpleDateFormat, ParseException }

/**
 * Object to parse the name of an image.
 * The expected filename format is:
 *    <camera_id><eejjmmdd><uummss.nnn>.<ext>  or
 *    <camera_id><eejjmmdd><uummss.nnn>
 */
object ImageNameParser {
  private val dateParseFormat = "yyyyMMddHHmmss.SSS"
  private val timestampParser = new DateParser(dateParseFormat)
  private val ImageFilename = new Regex("""(\w+)(\d{14}\.\d{3})[\.]?""")

  def getImageParts(fileName: String, ext: Option[String]): Option[ImageNameParts] = {
    val filenameWithoutExt = ext match {
      case Some(extension) ⇒
        fileName.dropRight(extension.length)
      case None ⇒
        fileName
    }
    filenameWithoutExt match {
      case ImageFilename(cameraId, dateString) ⇒
        Some(ImageNameParts(cameraId, dateString, ext))
      case _ ⇒
        None
    }
  }

  /**
   * Get the Time from the filename
   * @param fileName the filename
   * @return on success the time else 0
   */
  def getTimeFromName(fileName: String, ext: Option[String]): Long = {
    getImageParts(fileName, ext) match {
      case Some(imageNameParts) ⇒
        try {
          timestampParser.synchronized {
            timestampParser.stringToEpoch(imageNameParts.dateString)
          }
        } catch {
          case ex: ParseException ⇒ 0L
        }
      case None ⇒ 0L
    }
  }
}

/**
 * Converts strings conforming to the given parsefomat using the given timezone
 * @param parseFormat format used for parsing the timestamps
 * @param timeZone the timezone for the given timestamp in stringToDate and stringToEpoch
 */
class DateParser(parseFormat: String, timeZone: String = "UTC") {
  private val format = new SimpleDateFormat(parseFormat)
  format.setTimeZone(java.util.TimeZone.getTimeZone((timeZone)))

  def stringToDate(timeStamp: String) = format.parse(timeStamp)
  def stringToEpoch(timeStamp: String) = stringToDate(timeStamp).getTime
}

case class ImageNameParts(cameraId: String, dateString: String, ext: Option[String])