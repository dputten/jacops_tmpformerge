/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.monitoring

import akka.actor.{ ActorRef, LoggingFSM, Actor }
import csc.sitebuffer.process.images.NewImage
import csc.sitebuffer.messages.VehicleMessage
import scala.concurrent.duration.{ FiniteDuration, Duration }

sealed trait CameraState
case object Normal extends CameraState
case object ExpectImage extends CameraState
case object Error extends CameraState
case object SendError extends CameraState

case class CameraStateMonitorState(lastImageTime: Long, vehicle: Option[VehicleMessage] = None, nrErrorMsg: Long = 0L)

case class CameraImageOutputError(camera: String, startTime: Long, currentTime: Long, nr: Long)
case class CameraImageOutputFixed(camera: String, startTime: Long, endTime: Long, nr: Long)
case object TimeoutExpectImage
case object TimeoutError

class CameraStateMonitor(camera: String, maxWaitTimeForImage: FiniteDuration, maxErrorTimeForImage: FiniteDuration, recipients: Set[ActorRef]) extends Actor with LoggingFSM[CameraState, CameraStateMonitorState] {
  startWith(Normal, new CameraStateMonitorState(0L))

  when(Normal) {
    case Event(msg: NewImage, data: CameraStateMonitorState) ⇒ {
      stay() using updateImageTimeInState(msg.timestamp, data)
    }
    case Event(msg: VehicleMessage, data: CameraStateMonitorState) ⇒ {
      if (msg.dateTime1 <= data.lastImageTime || !msg.valid) {
        stay()
      } else {
        goto(ExpectImage) using data.copy(vehicle = Some(msg))
      }
    }
  }
  when(ExpectImage) {
    case Event(TimeoutExpectImage, data: CameraStateMonitorState) ⇒ {
      goto(SendError) using data.copy(nrErrorMsg = data.nrErrorMsg + 1)
    }
  }

  when(Error) {
    case Event(TimeoutError, data: CameraStateMonitorState) ⇒ {
      goto(SendError) using data.copy(nrErrorMsg = data.nrErrorMsg + 1)
    }
  }

  when(SendError) {
    case Event(msg: VehicleMessage, data: CameraStateMonitorState) ⇒ {
      goto(Error) using data.copy(vehicle = Some(msg))
    }
  }
  initialize

  //generic state changes
  whenUnhandled {
    case Event(msg: NewImage, data: CameraStateMonitorState) ⇒ {
      val vehicleTime = data.vehicle.map(_.dateTime1).getOrElse(0L)
      if (vehicleTime <= msg.timestamp) {
        goto(Normal) using new CameraStateMonitorState(math.max(msg.timestamp, data.lastImageTime))
      } else {
        stay() using updateImageTimeInState(msg.timestamp, data)
      }
    }
    case Event(msg: VehicleMessage, data: CameraStateMonitorState) ⇒ {
      //default we do nothing and still waiting for Images
      stay()
    }
  }

  onTransition {
    case _ -> ExpectImage ⇒ setTimer("ExpectImage", msg = TimeoutExpectImage, timeout = maxWaitTimeForImage, repeat = false)
    case _ -> SendError ⇒ {
      cancelTimer("ExpectImage")
      val errorMsg = new CameraImageOutputError(camera, nextStateData.lastImageTime, System.currentTimeMillis(), nextStateData.nrErrorMsg)
      recipients.foreach(_ ! errorMsg)
    }
    case ExpectImage -> _ ⇒ cancelTimer("ExpectImage")
    case SendError -> Normal ⇒ {
      //use the old state to get the number or error messages send
      val errorMsg = new CameraImageOutputFixed(camera, stateData.lastImageTime, nextStateData.lastImageTime, stateData.nrErrorMsg)
      recipients.foreach(_ ! errorMsg)
    }
    case _ -> Error ⇒ setTimer("Error", msg = TimeoutError, timeout = maxErrorTimeForImage, repeat = false)
    case Error -> Normal ⇒ {
      cancelTimer("Error")
      //use the old state to get the number or error messages send
      val errorMsg = new CameraImageOutputFixed(camera, stateData.lastImageTime, nextStateData.lastImageTime, stateData.nrErrorMsg)
      recipients.foreach(_ ! errorMsg)
    }
    case Error -> _ ⇒ cancelTimer("Error")
  }

  private def updateImageTimeInState(timestamp: Long, oldState: CameraStateMonitorState): CameraStateMonitorState = {
    if (timestamp > oldState.lastImageTime) {
      oldState.copy(lastImageTime = timestamp)
    } else {
      oldState
    }
  }
}