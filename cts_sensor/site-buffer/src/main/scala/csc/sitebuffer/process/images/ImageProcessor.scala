/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.images

import akka.actor.{ ActorLogging, Actor, ActorRef }
import akka.event.LoggingReceive
import java.io.{ FileInputStream, FileOutputStream, File }
import scala.util.matching.Regex
import csc.sitebuffer.common.Signing
import org.apache.commons.io.FileUtils

/**
 * Actor receives NewImages messages.
 * For each incoming image this actor will:
 * - Check if image is a valid jpeg image
 * - Calculate checksum for image
 * - Copy image to other directory with another name (containing checksum)
 * - Send DeleteImage message to next actor
 *
 * @param directoryPrefixForOutgoingImages Prefix that will be added to name of camera directory in order to get outgoing directory
 * @param recipients actor that will receive all created DeleteImage messages
 */
class ImageProcessor(directoryPrefixForOutgoingImages: String, recipients: Set[ActorRef]) extends Actor with ActorLogging {
  import ImageDeleter._

  def receive = LoggingReceive {
    case NewImage(imagePath, timestamp) ⇒ processImage(imagePath)
    case anyRef: AnyRef ⇒ {
      log.error("Received unknown message: [%s]".format(anyRef))
    }
  }

  def processImage(imagePath: String): Unit = {
    for (
      imageFile ← makeFileForPath(imagePath) if isValidJpeg(imageFile);
      _ = log.debug("imagePath: " + imagePath);
      _ = log.debug(imageFile.getAbsolutePath);
      sha ← calculateSha(imageFile);
      _ = log.debug(sha);
      targetDirectory ← getTargetDirectory(imageFile);
      _ = log.debug("targetDirectory: " + targetDirectory.getAbsolutePath);
      newFile ← getTargetFile(imageFile, sha, targetDirectory);
      _ = log.debug("newFile: " + newFile.getAbsolutePath)
    ) copyFile(imageFile, newFile)
    deleteFile(imagePath) // This will also delete invalid files
  }

  def makeFileForPath(imagePath: String): Option[File] = try {
    Some(new File(imagePath))
  } catch {
    case e: Exception ⇒
      log.error("Image [%s] cannot be processed: could not create a file object. [%s]".format(imagePath, e.getMessage))
      None
  }

  /**
   * Check if the given file is a valid jpeg image.
   * This is done by checking the magic numbers:
   * - first 2 bytes should be FF D8
   * - last 2 bytes should be FF D9
   * @param imageFile the file for which the jpeg check is performed.
   * @return imageFile is a valid jpeg image
   */
  def isValidJpeg(imageFile: File): Boolean = try {
    val jpegStartBytes = Seq(0xFF.toByte, 0xD8.toByte)
    val jpegEndBytes = Seq(0xFF.toByte, 0xD9.toByte)
    val byteArray = FileUtils.readFileToByteArray(imageFile)
    val isJpeg = byteArray.startsWith(jpegStartBytes) && byteArray.endsWith(jpegEndBytes)
    if (!isJpeg)
      log.error("File [%s] failed the jpeg check. Image will be discarded ".format(imageFile.getCanonicalPath))
    isJpeg
  } catch {
    case e: Exception ⇒
      log.error("Could not perform jpeg check for file %s. [%s]".format(imageFile.getCanonicalPath, e.getMessage))
      false
  }

  def calculateSha(imageFile: File): Option[String] = try {
    Some(Signing.digest(imageFile.getCanonicalPath, "MD5"))
  } catch {
    case e: Exception ⇒
      log.error("Sha for image [%s] could not be calculated. [%s]".format(imageFile.getCanonicalPath, e.getMessage))
      None
  }

  /**
   * Determines, based on an image file, the directory where this file should be moved to.
   * If image file is in directory dir1/dir2/dir3, then the target directory will be dir1/dir2/prefix+dir3,
   * where prefix is the given directoryPrefixForOutgoingImages.
   * The directory will be created if needed.
   * @param imageFile the file for which the target will be determined.
   * @return the target directory for given image file.
   */
  def getTargetDirectory(imageFile: File): Option[File] = try {
    val currentDirectory = imageFile.getParentFile
    val targetDirectory = new File(currentDirectory.getParentFile, "%s%s".format(directoryPrefixForOutgoingImages, currentDirectory.getName))
    if (!targetDirectory.exists()) {
      if (!targetDirectory.mkdirs()) {
        throw new Exception("Directory could not be created")
      }
    }
    Some(targetDirectory)
  } catch {
    case e: Exception ⇒
      log.error("Could not get/create target directory for [%s]. [%s]".format(imageFile.getCanonicalPath, e.getMessage))
      None
  }

  /**
   * Determine the new name of the imageFile in the targetDirectory. Will contain the sha.
   * If the file name is 02020130712091244.324.jpg and the sha is ab123456789abcdef, then the new name will be
   * 02020130712091244.324-ab123456789abcdef.jpg
   */
  def getTargetFile(imageFile: File, sha: String, targetDirectory: File): Option[File] = try {
    val ImagiFilename = new Regex("""(\d+.\d+).(\w+)""")
    imageFile.getName match {
      case ImagiFilename(name, extension) ⇒
        val newFilename = "%s-%s.%s".format(name, sha, extension)
        Some(new File(targetDirectory, newFilename))
      case _ ⇒
        throw new Exception("Filename [%s] cannot be parsed".format(imageFile.getName))
    }
  } catch {
    case e: Exception ⇒
      log.error("Could not get target file for [%s]. [%s]".format(imageFile.getCanonicalPath, e.getMessage))
      None
  }

  def copyFile(fromFile: File, toFile: File): Unit = {
    new FileOutputStream(toFile).getChannel.transferFrom(new FileInputStream(fromFile).getChannel, 0, Long.MaxValue)
  }

  /**
   * File deletion is performed by a separate actor.
   */
  def deleteFile(imagePath: String): Unit = {
    sendToRecipients(DeleteImage(imagePath), recipients)
  }

  def sendToRecipients(message: Any, recipients: Set[ActorRef]) {
    for (recipient ← recipients) {
      recipient ! message
    }
  }

}
