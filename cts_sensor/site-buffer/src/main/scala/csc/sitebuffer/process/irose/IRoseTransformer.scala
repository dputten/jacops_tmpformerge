package csc.sitebuffer.process.irose

import csc.sitebuffer.messages._
import scala.Some
import scala.Left
import csc.sitebuffer.messages.UnknownMessage
import scala.Right
import csc.sitebuffer.messages.InvalidCRCMessage

/**
 * Copyright (C) 2012,2013 CSC <http://www.csc.com>.
 * User: jwluiten
 * Date: 2/4/13
 */
object IRoseTransformer {
  private val sep = ";"

  /* strings for partial formats */
  private val date_time_fmt = "(\\d{6};\\d{6},\\d{3})" // yymmdd;hhmmss,SSS
  private val str = "([^" + sep + "]+)"
  private val u_int = "(\\d+)" // unsigned int
  private val s_int = "(-?\\d+)" // signed int
  private val sint_2 = "(\\d{1,2})" // signed int 1 or 2 digits
  private val u_float = "(\\d+,\\d+)"
  private val s_float = "(-?\\d+,\\d+)"
  private val zero_picx3 = "000"
  private val zero_picx5 = "00000"
  private val checksum = "([0-9A-Fa-f]{2})$"
  private val prologue = date_time_fmt + sep
  private val any = ".*"
  private val epilogue = sep + "..$"
  private val crc_re = ("(\\d{6};\\d{6},\\d{3}.*;)" + checksum).r

  /* Formats for complete messages. For an explanation of the messageformat see iRose v2.0 documentation */

  /* sensor message
                           dateTime   type        temperature     humidity        accel.x */
  private val sensor_re = (prologue + "0" + sep + s_float + sep + u_float + sep + s_float + sep +
    /* accel.y         accel.z */
    s_float + sep + s_float + epilogue).r

  /* vehicle message
                            dateTime  type        detector      speed */
  // TODO: This is the old format. Remove once new format has been introduced.
  private val vehicle_v1_re = (prologue + "1" + sep + u_int + sep + s_int + sep +
    /* length     rise1         rise2         fall1         fall2         yt2             rt2             dateTime1 */
    u_int + sep + u_int + sep + s_int + sep + u_int + sep + s_int + sep + s_float + sep + s_float + sep + date_time_fmt +
    /*    yt1             rt1             dateTime3             yt3             rt3     */
    sep + s_float + sep + s_float + sep + date_time_fmt + sep + s_float + sep + s_float + epilogue).r

  /*                        dateTime  type        detector      speed */
  // TODO: This is the old format. Remove once new format has been introduced.
  private val vehicle_v2_re = (prologue + "1" + sep + u_int + sep + s_int + sep +
    /* length     rise1         rise2         fall1         fall2         yt2             rt2             dateTime1 */
    u_int + sep + u_int + sep + s_int + sep + u_int + sep + s_int + sep + s_float + sep + s_float + sep + date_time_fmt +
    /*    yt1             rt1             dateTime3             yt3             rt3             valid */
    sep + s_float + sep + s_float + sep + date_time_fmt + sep + s_float + sep + s_float + sep + u_int + epilogue).r

  private val vehicle_v3_re = (prologue + "1" + sep + u_int + sep + s_int + sep +
    /* length     rise1         rise2         fall1         fall2         yt2             rt2             dateTime1 */
    u_int + sep + u_int + sep + s_int + sep + u_int + sep + s_int + sep + s_float + sep + s_float + sep + date_time_fmt +
    /*    yt1             rt1             dateTime3             yt3             rt3             valid */
    sep + s_float + sep + s_float + sep + date_time_fmt + sep + s_float + sep + s_float + sep + u_int + sep +
    /*  serial  offset1      offset2       offset3 */
    str + sep + u_int + sep + u_int + sep + u_int + any + epilogue).r

  /* input event message
                                dateTime   type        input         eventnumber */
  val input_event_re = (prologue + "2" + sep + u_int + sep + sint_2 +
    /*    eventtext */
    sep + "(.*)" + epilogue).r

  /* input event message
                               dateTime   type        input         eventnumber */
  val input_event_alarm = (prologue + "2" + sep + u_int + sep + sint_2 + epilogue).r

  /* single loop message
                             dateTime   type         */
  private val loop_msg_re = (prologue + "4" + sep + u_int + sep + zero_picx3 + sep + zero_picx3 + sep +
    zero_picx5 + sep + u_int + sep + u_int + sep + zero_picx5 + epilogue).r

  /**
   *
   * @param body the body of the message
   * @param crcExpected the crc to be validated
   * @return either a Left[IRoseError] indicating an invalid CRC or a Right[Byte] confirming the CRC
   */
  private def do_check_crc(body: String, crcExpected: String): Either[IRoseError, Int] = {
    // Calculate crc using foldLeft, XOR-ing all characters in the String.
    // Even more terse would actually make the expression look like an
    // indecent proposal: crcValue = (0 /: body) (_^_)
    //
    val crcValue = (0 /: body)((accu, ch) ⇒ accu ^ ch)

    // make hex-string and compare with the expected hex-string from iRose
    val crcString = "%02X".format(crcValue)
    if (crcString == crcExpected) {
      Right(crcValue)
    } else {
      Left(InvalidCRCMessage("body: %s, expected: %s, calculated: %s".format(body, crcExpected, crcString)))
    }
  }

  /**
   *
   * @param message complete message comprised of body and CRC
   * @return either a Left[IRoseError] indicating a parse error or an invalid CRC
   *         or a Right[Int] confirming CRC had the given correct value.
   */
  def check_crc(message: String): Either[IRoseError, Int] = {
    message match {
      case crc_re(body, crc) ⇒ do_check_crc(body, crc)
      case _                 ⇒ Left(UnknownMessage(message))
    }
  }

  /**
   *
   * @param message string as sent by the iRose loop detector
   * @return Either a Left[IRoseError] or a Right[IRoseMessage]
   */
  def transform(message: String): Either[IRoseError, IRoseMessage] = {
    // check wether message has correct CRC
    val crcResult = check_crc(message)
    crcResult match {
      // CRC matches. Now match the message with the regular expressions
      // and handle each message appropriately
      case Right(crcValue) ⇒ {
        message match {
          case sensor_re(date_time, temp, humidity, x, y, z) ⇒
            Right(SensorMessage(date_time, temp, humidity, x, y, z))

          case input_event_alarm(date_time, input, event) ⇒ {
            createInputEventMessage(date_time, input, event, "") match {
              case Right(inputEventMessage) ⇒ Right(inputEventMessage)
              case Left(errorMessage)       ⇒ Left(InvalidInputEventMessage("Error [%s] processing message [%s]".format(errorMessage, message)))
            }
          }

          case input_event_re(date_time, input, event, text) ⇒ {
            createInputEventMessage(date_time, input, event, text) match {
              case Right(inputEventMessage) ⇒ Right(inputEventMessage)
              case Left(errorMessage)       ⇒ Left(InvalidInputEventMessage("Error [%s] processing message [%s]".format(errorMessage, message)))
            }
          }

          // TODO: This is the old format. Remove once new format has been introduced.
          case vehicle_v3_re(date_time, detector, speed, length, rise1, rise2, fall1, fall2, yt2, rt2, dt1, yt1, rt1,
            dt3, yt3, rt3, valid, serial, offset1, offset2, offset3) ⇒
            Right(VehicleMessage(date_time, detector, speed, length, rise1, fall1, rise2, fall2, yt2, rt2,
              dt1, yt1, rt1, dt3, yt3, rt3, valid, serial, offset1, offset2, offset3))
          // TODO: This is the old format. Remove once new format has been introduced.
          case vehicle_v2_re(date_time, detector, speed, length, rise1, rise2, fall1, fall2, yt2, rt2, dt1, yt1, rt1,
            dt3, yt3, rt3, valid) ⇒
            Right(VehicleMessage(date_time, detector, speed, length, rise1, fall1, rise2, fall2, yt2, rt2,
              dt1, yt1, rt1, dt3, yt3, rt3, valid))

          // TODO: This is the old format. Remove once new format has been introduced.
          case vehicle_v1_re(date_time, detector, speed, length, rise1, rise2, fall1, fall2, yt2, rt2, dt1, yt1, rt1,
            dt3, yt3, rt3) ⇒
            Right(VehicleMessage(date_time, detector, speed, length, rise1, fall1, rise2, fall2, yt2, rt2,
              dt1, yt1, rt1, dt3, yt3, rt3, "0"))

          case loop_msg_re(date_time, loop_id, last_scan, cover_time) ⇒
            Right(SingleLoopMessage(date_time, loop_id, last_scan, cover_time))

          case _ ⇒ Left(UnknownMessage(message))
        }
      }
      // CRC of the message did not match the CRC we calculated. The
      // message was maimed in transfer. Return an error.
      case Left(iRoseError) ⇒ Left(iRoseError)
    }
  }

  def createInputEventMessage(date_time: String, input: String, event: String, eventText: String): Either[String, InputEventMessage] = {
    try {
      Right(InputEventMessage(date_time, input, event, eventText))
    } catch {
      case e: Exception ⇒
        Left(e.getMessage)
    }
  }
}
