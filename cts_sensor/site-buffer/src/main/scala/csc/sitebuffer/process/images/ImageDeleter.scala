/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.images

import akka.actor.{ ActorLogging, Actor }
import akka.event.LoggingReceive
import java.io.File
import scala.concurrent.duration._

/**
 * Actor responsible for deleting images.
 */
class ImageDeleter(maxDeleteRetries: Int, minDelayBeforeRetryMs: Int) extends Actor with ActorLogging { this: FileDelete ⇒
  import ImageDeleter._

  implicit val executor = context.system.dispatcher

  def receive = LoggingReceive {
    case image: NewImage ⇒
      deleteAttempt(image.imagePath, 1)
    case DeleteImage(imagePath) ⇒
      deleteAttempt(imagePath, 1)
    case DeleteImageNextAttempt(imagePath, attempts) ⇒
      deleteAttempt(imagePath, attempts)
    case anyRef: AnyRef ⇒ {
      log.error("Received unknown message: [%s]".format(anyRef))
    }
  }

  /**
   * Attempt to delete given file.
   * @param imagePath path to the file to be deleted.
   * @param attempt indicates which attempt this is.
   * @return if delete succeeded.
   */
  def deleteAttempt(imagePath: String, attempt: Int): Unit = {
    log.debug("deleteAttempt: imagePath: %s, attempt: %d".format(imagePath, attempt))
    getExistingFile(imagePath) match {
      case Some(file) ⇒
        if (!doDeleteFile(file)) {
          if (attempt < maxDeleteRetries) {
            val nextAttempt = DeleteImageNextAttempt(imagePath, attempt + 1)
            context.system.scheduler.scheduleOnce(minDelayBeforeRetryMs millis, self, nextAttempt)
          } else {
            log.error("Could not delete file [%s] in %d attempts".format(imagePath, maxDeleteRetries))
          }
        }

      case None ⇒
        log.warning("File [%s] does not exists. Delete not needed/possible".format(imagePath))
    }
  }

  def doDeleteFile(file: File): Boolean = deleteFile(file) match {
    case Right(deleted) ⇒
      deleted

    case Left(errorMessage) ⇒
      log.error(errorMessage)
      false
  }

  def getExistingFile(imagePath: String): Option[File] = {
    val file = new File(imagePath)
    if (file.exists()) {
      Some(file)
    } else {
      None
    }
  }

}

trait FileDelete {
  def deleteFile(file: File): Either[String, Boolean] = try {
    Right(file.delete())
  } catch {
    case e: Exception ⇒
      Left("Error trying to delete file [%s]: [%s]".format(file.getCanonicalPath, e.getMessage))
  }
}

object ImageDeleter {
  case class DeleteImage(imagePath: String)
  case class DeleteImageNextAttempt(imagePath: String, attempts: Int)

  def apply(maxDeleteRetries: Int, minDelayBeforeRetryMs: Int): ImageDeleter = {
    new ImageDeleter(maxDeleteRetries, minDelayBeforeRetryMs) with FileDelete
  }
}
