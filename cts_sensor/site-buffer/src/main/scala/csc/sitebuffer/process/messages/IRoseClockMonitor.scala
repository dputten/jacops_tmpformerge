/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.messages

import akka.actor.{ ActorLogging, Actor, ActorRef }
import akka.event.LoggingReceive
import csc.sitebuffer.messages.SensorMessage
import csc.sitebuffer.process.SystemSensorMessage.{ EventType, Sensor }
import csc.sitebuffer.process.SystemSensorMessage

/**
 * Actor responsible for monitoring the clock in the iRose, based on the incoming SensorMessages.
 */
class IRoseClockMonitor(outOfSyncMarginMs: Int, getCurrentTime: () ⇒ Long, systemSensorMessageSender: ActorRef) extends Actor with ActorLogging {

  def receive = LoggingReceive {
    case sensorMessage: SensorMessage ⇒ processSensorMessage(sensorMessage)
    case anyRef: AnyRef ⇒ {
      log.error("Received unknown message: [%s]".format(anyRef))
    }
  }

  private def processSensorMessage(sensorMessage: SensorMessage) {
    if (datetimeOutOfRange(sensorMessage)) {
      systemSensorMessageSender ! newSystemSensorMessage
    }
  }

  private def datetimeOutOfRange(sensorMessage: SensorMessage): Boolean = {
    val timeDifference = now - sensorMessage.dateTime
    (timeDifference < 0) || (timeDifference > outOfSyncMarginMs)
  }

  private def now: Long = getCurrentTime()

  private def newSystemSensorMessage: SystemSensorMessage = {
    SystemSensorMessage(Sensor.Clock, now, "", EventType.Deviation, "Sitebuffer", Map())
  }

}
