package csc.sitebuffer.process.throttling

import java.io.File
import scala.annotation.tailrec
import csc.akka.logging.DirectLogging

/**
 * Copyright (C) 2012-2014 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 4/2/14.
 */

object FileFilters {
  /*
    Given a Regex return a function that accepts a File and
    answers true if the file's name matches the regex, false otherwise
   */
  def regexFilter(regexPattern: String): (File) ⇒ Boolean = { f ⇒
    f.getName.matches(regexPattern)
  }
}

class FileThrottler(inputDir: File,
                    outputDir: File,
                    threshold: Int,
                    sortInputFiles: (Seq[File]) ⇒ Seq[File],
                    inputFileFilter: (File) ⇒ Boolean) extends DirectLogging {

  require(inputDir.exists(), "Input directory must exist.")
  if (!outputDir.exists()) {
    outputDir.mkdirs()
  }

  // Files in output directory.
  protected def outputFiles: Seq[File] = outputDir.listFiles().toSeq

  // Files in input directory
  protected def inputFiles: Seq[File] = getInputFiles()

  protected def moveFile(sourceFile: File, destDir: File): Unit = {
    val baseName = sourceFile.getName
    val destFile = new File(destDir, baseName)
    if (!sourceFile.renameTo(destFile)) {
      log.error("Failed to rename file {} to {}", sourceFile, destFile)
    }
  }

  // While files in input directory and output directory is below threshold
  // move file from input directory to output directory
  def poll: Unit = {
    @tailrec
    def doPoll(files: Seq[File]): Unit = {
      if ((files != Nil) && (threshold > outputFiles.length)) {
        moveFile(files.head, outputDir)
        doPoll(files.tail)
      }
    }
    doPoll(inputFiles)
  }

  protected def getInputFiles(): Seq[File] = {
    val availableInputFiles: Seq[File] = inputDir.listFiles().toSeq
    availableInputFiles.filter({ file ⇒ file.isFile && inputFileFilter(file) }).
      sortWith({ (a, b) ⇒ a.getName < b.getName })
  }
}
