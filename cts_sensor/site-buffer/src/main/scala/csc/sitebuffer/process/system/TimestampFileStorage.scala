/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.system

import java.io.{ PrintWriter, File }
import scala.io.Source

/**
 * This trait represents the ability store and retrieve timestamps from file system.
 */
trait TimestampFileStorage extends TimestampStorage {
  val timestampFileName = "sitebufferTimestamp.txt"
  def timestampDirectory: String

  def storeTimestamp(timestamp: Long): Either[String, String] = {
    val fileName = fullPathName
    try {
      val file = new File(fileName)
      if (!file.getParentFile.exists()) {
        file.getParentFile.mkdirs()
      }
      val writer = new PrintWriter(file)
      writer.write(timestamp.toString)
      writer.close()
      Right("Done")
    } catch {
      case e: Exception ⇒
        Left("Could not write new timestamp to %s file. Exception [%s]".format(fileName, e.getMessage))
    }
  }

  def getLatestTimestamp: Either[String, Option[Long]] = {
    val fileName = fullPathName
    if (fileExists(fileName)) {
      extractTimestamp(new File(fileName))
    } else {
      Right(None)
    }
  }

  private def fullPathName: String = {
    val template = if (timestampDirectory.trim.last == '/') "%s%s" else "%s/%s"
    template.format(timestampDirectory.trim, timestampFileName)
  }

  private def fileExists(filePath: String): Boolean = {
    new File(filePath).exists()
  }

  private def extractTimestamp(file: File): Either[String, Option[Long]] = {
    val lines = Source.fromFile(file).getLines().toList
    lines.size match {
      case 0 ⇒
        Left("Timestamp file [%s] was empty".format(file.getAbsolutePath))

      case 1 ⇒ try {
        Right(Some(lines.head.toLong))
      } catch {
        case e: Exception ⇒
          Left("Error reading timestamp from timestamp file [%s]: [%s]".format(file.getAbsolutePath, e))
      }

      case _ ⇒
        Left("Timestamp file [%s] contains more than one line".format(file.getAbsolutePath))
    }
  }

}
