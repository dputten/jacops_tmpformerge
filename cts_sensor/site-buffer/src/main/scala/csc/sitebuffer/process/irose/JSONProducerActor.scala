/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.irose

/**
 * Actor responsible for serializing all incoming IRoseMessages and send them to the configured endpoint.
 */

import akka.actor.{ ActorLogging, Actor }
import akka.camel.{ Producer, CamelMessage }
import csc.sitebuffer.messages._
import csc.json.lift.EnumerationSerializer
import net.liftweb.json._
import org.apache.camel.Exchange
import java.util.UUID
import java.security.MessageDigest
import java.math.BigInteger

class JSONProducerActor(jsonEndpointDirectory: String) extends Actor with Producer with ActorLogging {
  private def endpointUriTemplate = "file://%s"
  def endpointUri = endpointUriTemplate.format(jsonEndpointDirectory)

  // BEWARE: oneway = false results in this actor also sending the transformed message
  // back to the originating actor.
  override def oneway: Boolean = true

  /**
   * Transform the message before it is send to the endpoint.
   * @param msg the message received
   * @return CamelMessage containing the message after transformation
   */
  override protected def transformOutgoingMessage(msg: Any): CamelMessage = {
    // Add serializer for Direction enum
    val formats = DefaultFormats + new EnumerationSerializer(Direction)

    msg match {
      // For the time being we're only interested in messages from iRose
      //case iRoseMsg: IRoseMessage ⇒ {
      case iRoseMsg: VehicleMessage ⇒ {
        // Serialize case class to JSON. For IRoseMessage case class see Messages.scala
        //val jsonString = Serialization.write[IRoseMessage](iRoseMsg)(formats)
        val jsonString = Serialization.write[VehicleMessage](iRoseMsg)(formats)
        // and wrap in false CamelMessage for transfer to the endpoint
        log.debug("Creating CamelMessage containing JSON: %s".format(jsonString))
        val checksum = createChecksum(jsonString)

        val fileName = "%s/%d-%s.json".format(iRoseMsg.detector.toString, iRoseMsg.dateTime, UUID.randomUUID().toString)

        log.debug("path to store new vehicle message [%s]".format(fileName))

        CamelMessage(jsonString + checksum, Map(Exchange.FILE_NAME -> fileName))
      }
      // Ignore any other messages
      case _ ⇒ {
        CamelMessage(None, Map.empty)
      }
    }
  }

  /**
   * Create a MD5 checksum of the given string
   * @param json The json content
   * @return
   */
  private def createChecksum(json: String): String = {
    val md = MessageDigest.getInstance("MD5")
    md.update(json.getBytes())
    var number = new BigInteger(1, md.digest)
    number.toString(16)
  }
}