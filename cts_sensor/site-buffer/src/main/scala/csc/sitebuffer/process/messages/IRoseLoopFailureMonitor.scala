/**
 * Copyright (C) 2012,2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.messages

import akka.actor._
import akka.actor.{ ActorLogging, Actor, ActorRef }
import akka.event.LoggingReceive
import csc.sitebuffer.messages.SingleLoopMessage
import csc.sitebuffer.process.SystemSensorMessage.{ EventType, Sensor }
import scala.collection.mutable
import scala.collection.immutable
import csc.sitebuffer.process.SystemSensorMessage
import csc.akka.process.{ ExecuteScript, ExecuteScriptResult }

/**
 * Class responsible for handling incoming SingleLoopMessages from iRose.
 * If more than [maxAllowedMessages] messages are received within [testWindowMs] milliseconds for one loop
 * a SystemSensorMessage will be created.
 */
class IRoseLoopFailureMonitor(maxAllowedMessages: Int, testWindowMs: Int, iRoseRebootScriptPath: String, getCurrentTime: () ⇒ Long, systemSensorMessageSender: ActorRef, executeScriptActor: ActorRef) extends Actor with ActorLogging {
  // A failure monitor for each encountered loopId.
  private val monitors: mutable.Map[Int, FailureMonitor] = mutable.Map()

  // Small class that monitors the count of SingleLoopMessages.
  case class FailureMonitor(maxAllowedMessages: Int, sampleDurationMs: Int) {
    private val data = mutable.Queue[Long]()

    def processNextReading(message: SingleLoopMessage): Boolean = {
      val now = getCurrentTime()
      data.enqueue(now)
      cleanup(now - sampleDurationMs)
      // Return whether failure situation has arisen
      data.size > maxAllowedMessages
    }
    private def cleanup(datetimeLimit: Long) {
      // Remove all entries older than datetimeLimit which results in the current count.
      data.dequeueAll(_ <= datetimeLimit)
    }
    def reset { data.clear() }
  }

  def receive = LoggingReceive {
    case message: SingleLoopMessage ⇒ processMessage(message)
    case anyRef: AnyRef ⇒ {
      log.error("Received unknown message: [%s]".format(anyRef))
    }
  }

  def processMessageWithoutCounting: Receive = LoggingReceive {
    case result: ExecuteScriptResult ⇒ {
      result.resultCode match {
        case Right(returnCode) if (returnCode == 0) ⇒ log.info("Reboot IRose succesful. Executed script [%s].".format(result.script.script))
        case Right(returnCode)                      ⇒ log.error("Reboot IRose failed. Failure in script [%s] received returncode [%d]".format(result.script.script, returnCode))
        case Left(throwable)                        ⇒ log.error(throwable, "Reboot IRose failed. Failure while executing script [%s]".format(result.script.script))
      }
      context.unbecome()
    }
    // Alle andere berichten (waaronder SingleLoopMessage) tijdelijk negeren. Pas weer wat mee doen
    // na de unbecome (de reboot dus)
    case _ ⇒
  }

  private def processMessage(message: SingleLoopMessage) {
    getMonitor(message.loopId).processNextReading(message) match {
      case true ⇒
        systemSensorMessageSender ! createSystemSensorMessage(message.loopId)

        val msg = new ExecuteScript(iRoseRebootScriptPath, Seq())
        executeScriptActor ! msg

        // Aangezien een reboot voor alles gebeurt ook alle loops resetten
        monitors.foreach((e: (Int, FailureMonitor)) ⇒ e._2.reset)
        context.become(processMessageWithoutCounting)
        log.info("Reboot of iRose started. There are more than [maxAllowedMessages]:%d messages received within [testWindowMs]:%d milliseconds for one loop.".format(maxAllowedMessages, testWindowMs))
        log.info("iRose is rebooting, count is temporarily disabled.")
      case false ⇒
    }
  }

  private def getMonitor(loopId: Int): FailureMonitor = {
    monitors.get(loopId) match {
      case Some(monitor) ⇒
        monitor
      case None ⇒
        val emptyMonitor = FailureMonitor(maxAllowedMessages, testWindowMs)
        monitors += loopId -> emptyMonitor
        emptyMonitor
    }
  }

  def createSystemSensorMessage(loopId: Int): SystemSensorMessage = {
    SystemSensorMessage(Sensor.DetectionLoop, getCurrentTime(), "", EventType.SensorFailure, "Sitebuffer", immutable.Map("loopId" -> ("%d".format(loopId))))
  }
}
