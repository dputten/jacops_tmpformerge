/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.send

import akka.actor.{ ActorLogging, Actor }
import akka.event.LoggingReceive
import csc.sitebuffer.process.SystemSensorMessage

/**
 * This actor receives SystemSensorMessage's, transforms them to JSON and sends them on using the passed send function.
 */
class SystemSensorMessageSender(val send: (String) ⇒ Unit) extends Actor with ActorLogging {

  def receive = LoggingReceive {
    case message: SystemSensorMessage ⇒ processMessage(message)
    case anyRef: AnyRef ⇒ {
      log.error("Received unknown message: [%s]".format(anyRef))
    }
  }

  private def processMessage(message: SystemSensorMessage) {
    toJSON(message) match {
      case Right(json) ⇒ send(json)
      case Left(error) ⇒ log.error("Could not process message: [%s]".format(error))
    }
  }

  private def toJSON(message: SystemSensorMessage): Either[String, String] =
    SystemSensorMessageSerialization.serializeToJSON(message)

}
