/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.send

import net.liftweb.json.{ DefaultFormats, Serialization }
import csc.sitebuffer.process.SystemSensorMessage

/**
 * Serialize SystemSensorMessage.
 */
object SystemSensorMessageSerialization {
  implicit val formats = DefaultFormats

  def serializeToJSON(sensorMessage: SystemSensorMessage): Either[String, String] = {
    try {
      Right(Serialization.write(sensorMessage))
    } catch {
      case ex: Exception ⇒ Left("SystemSensorMessageSerialization: Error during serialization: %s".format(ex.getMessage))
    }
  }

  def deserializeFromJSON(jsonString: String): Either[String, SystemSensorMessage] = {
    try {
      Right(Serialization.read[SystemSensorMessage](jsonString))
    } catch {
      case ex: Exception ⇒ Left("SystemSensorMessageSerialization: Error during deserialization: %s".format(ex.getMessage))
    }
  }
}
