/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.messages

import akka.actor.{ ActorLogging, Actor, ActorRef }
import akka.event.LoggingReceive
import csc.sitebuffer.messages.{ IsAliveCheck, SensorMessage }
import csc.sitebuffer.process.SystemSensorMessage.{ EventType, Sensor }
import java.util.Date
import csc.sitebuffer.process.SystemSensorMessage

/**
 * Actor responsible for monitoring if iRose is still alive.
 * This is done using the following messages:
 * - incoming SensorMessage's from the iRose (every x seconds; this is configured in the iRose config).
 * - incoming IsAliveCheck messages, that are send by the scheduler every y seconds. (configured in Sitebuffer config.)
 *
 * If no SensorMessage was received between two IsAliveCheck messages, a SystemSensorMessage will be created.
 *
 * IMPORTANT: y > x
 */
class IRoseIsAliveMonitor(systemSensorMessageSender: ActorRef) extends Actor with ActorLogging {

  var lastMessageWasIsAliveCheck = false

  def receive = LoggingReceive {
    case sensorMessage: SensorMessage ⇒ processSensorMessage
    case isAliveCheck: IsAliveCheck   ⇒ processIsAliveCheck
    case anyRef: AnyRef ⇒ {
      log.error("Received unknown message: [%s]".format(anyRef))
    }
  }

  private def processSensorMessage {
    reset
  }

  private def processIsAliveCheck {
    if (lastMessageWasIsAliveCheck) {
      systemSensorMessageSender ! newSystemSensorMessage
      reset // So next check will pass
    } else {
      lastMessageWasIsAliveCheck = true
    }
  }

  private def newSystemSensorMessage: SystemSensorMessage = {
    SystemSensorMessage(Sensor.IsAlive, new Date().getTime, "", EventType.OutOfRange, "Sitebuffer", Map())
  }

  private def reset { lastMessageWasIsAliveCheck = false }

}
