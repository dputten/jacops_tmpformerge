/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.images

import akka.actor.{ ActorLogging, Actor, ActorRef }
import akka.camel.{ CamelMessage, Consumer }
import akka.event.LoggingReceive
import java.io.File

/**
 * Actor receiving messages (containing full path names of images) over TCP/IP using Camel.
 * The main function is to convert CamelMessages to "Sitebuffer messages" and pass them on.
 *
 * @param imagesEndpointHost The host of the Camel endpoint where this actor will listen for messages
 * @param imagesEndpointPort The port of the Camel endpoint where this actor will listen for messages
 * @param getCurrentTime Function that returns the current time
 * @param recipients actor that will receive all created NewImage messages
 */
class ImageReceiver(imagesEndpointHost: String,
                    imagesEndpointPort: Int,
                    getCurrentTime: () ⇒ Long,
                    recipients: Set[ActorRef]) extends Actor with ActorLogging with Consumer {

  private def imagesEndpointTemplate = "mina:tcp://%s:%d?textline=true&sync=false"
  def endpointUri = imagesEndpointTemplate.format(imagesEndpointHost, imagesEndpointPort)

  def receive = LoggingReceive {
    case message: CamelMessage ⇒ {
      log.info("Received msg:" + message.bodyAs[String])
      extractImagePath(message) match {
        case Right(imagePath) ⇒ sendToRecipients(NewImage(imagePath = imagePath, timestamp = getCurrentTime()), recipients)
        case Left(error)      ⇒ invalidMessageReceived(error)
      }
    }
  }

  def extractImagePath(message: CamelMessage): Either[String, String] = try {
    Right(new File(message.bodyAs[String]).getCanonicalPath)
  } catch {
    case e: Exception ⇒
      Left("Image path could not be extracted: %s".format(e.getMessage))
  }

  def sendToRecipients(message: Any, recipients: Set[ActorRef]) {
    for (recipient ← recipients) {
      recipient ! message
    }
  }

  def invalidMessageReceived(message: String) {
    log.error("Invalid message received: [%s]".format(message))
  }

}
