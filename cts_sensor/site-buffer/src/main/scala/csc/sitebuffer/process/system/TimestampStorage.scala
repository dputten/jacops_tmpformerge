/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.system

/**
 * This trait represents the ability store and retrieve timestamps.
 */
trait TimestampStorage {
  def storeTimestamp(timestamp: Long): Either[String, String]
  def getLatestTimestamp: Either[String, Option[Long]]
}
