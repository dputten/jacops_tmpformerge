/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.messages

import akka.actor.{ ActorRef, ActorLogging, Actor }
import akka.event.LoggingReceive
import csc.sitebuffer.messages.VehicleMessage

/**
 * Actor responsible for filtering VehicleMessages.
 * The filterCondition provides the filter test:
 *   If true, a message is passed to the trueRecipients
 *   If false, a message is passed to the falseRecipients
 */
class VehicleMessageFilter(filterCondition: (VehicleMessage) ⇒ Boolean, trueRecipients: Set[ActorRef], falseRecipients: Set[ActorRef]) extends Actor with ActorLogging {

  def receive = LoggingReceive {
    case vehicleMessage: VehicleMessage ⇒
      if (filterCondition(vehicleMessage)) {
        sendToRecipients(vehicleMessage, trueRecipients)
      } else {
        sendToRecipients(vehicleMessage, falseRecipients)
      }

    case anyRef: AnyRef ⇒ {
      log.error("Received unknown message: [%s]".format(anyRef))
    }
  }

  def sendToRecipients(message: Any, recipients: Set[ActorRef]) {
    for (recipient ← recipients) {
      recipient ! message
    }
  }

}
