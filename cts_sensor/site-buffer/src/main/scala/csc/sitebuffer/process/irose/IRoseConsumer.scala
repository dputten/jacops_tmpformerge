/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.irose

import akka.actor.{ ActorLogging, Actor, ActorRef }
import akka.camel.{ CamelMessage, Consumer }
import akka.event.LoggingReceive

/**
 * Actor receiving messages from iRose over TCP/IP using Camel.
 * The messages received are put into an IRoseString, timestamped, and send to the recipients.
 *
 * @param iRoseEndpointHost The host of the Camel endpoint where this actor will listen for messages
 * @param iRoseEndpointPort The port of the Camel endpoint where this actor will listen for messages
 * @param getCurrentTime Function that returns the current time
 * @param recipients actor that will receive all created IRoseString
 */
class IRoseConsumer(
  iRoseEndpointHost: String,
  iRoseEndpointPort: Int,
  getCurrentTime: () ⇒ Long,
  recipients: Set[ActorRef]) extends Actor with ActorLogging with Consumer {

  private def iRoseEndpointTemplate = "mina:tcp://%s:%d?textline=true&sync=false"
  def endpointUri = iRoseEndpointTemplate.format(iRoseEndpointHost, iRoseEndpointPort)

  def receive = LoggingReceive {
    case msg: CamelMessage ⇒ {
      val str = msg.bodyAs[String]
      log.info("Received msg:" + str)
      sendToRecipients(IRoseString(string = str, timestamp = getCurrentTime()), recipients)
    }

    case msg: Any ⇒
      unknownMessageReceived(msg.toString)
  }

  def sendToRecipients(message: Any, recipients: Set[ActorRef]) {
    for (recipient ← recipients) {
      recipient ! message
    }
  }

  def unknownMessageReceived(message: String) {
    log.error("Unknown message received: [%s]".format(message))
  }

}
