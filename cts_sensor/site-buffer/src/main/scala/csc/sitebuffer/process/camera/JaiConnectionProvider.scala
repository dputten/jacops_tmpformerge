/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.camera

import csc.vehicle.jai.JaiCameraConnection
import csc.sitebuffer.config.CameraConfig
import akka.actor.Actor

/**
 * Trait representing a connection to a Jai camera.
 */
trait JaiConnectionProvider extends JaiConnection { actor: Actor ⇒

  def cameraConfig: CameraConfig // Should be set at creation
  def cameraId: String // Should be set at creation

  var jaiConnection: Option[JaiCameraConnection] = None

  /**
   * Create a connection to a Jai Camera.
   * Returns Right(okMessage) or Left(errorMessage)
   */
  def createJaiConnection: Either[String, String] = {
    jaiConnection match {
      case None ⇒
        createJaiCameraConnection(cameraId = cameraId, cameraConfig = cameraConfig) match {
          case Right(connection) ⇒
            jaiConnection = Some(connection)
            Right("Done")
          case Left(error) ⇒
            jaiConnection = None
            Left(error)
        }
      case Some(connection) ⇒
        Left("Could not create JaiCameraConnection, because it already exists")
    }
  }

  def createJaiCameraConnection(cameraId: String, cameraConfig: CameraConfig): Either[String, JaiCameraConnection] = {
    try {
      val connection = new JaiCameraConnection(
        laneId = cameraId,
        host = cameraConfig.cameraHost,
        port = cameraConfig.cameraPort,
        maxTriggerRetries = cameraConfig.maxTriggerRetries,
        timeDisconnected = cameraConfig.timeDisconnectedMs,
        cameraTriggerId = 0xFF,
        recipients = Set(self))
      Right(connection)
    } catch {
      case ex: Exception ⇒ Left("Could not create JaiCameraConnection: [%s]".format(ex))
    }
  }

  /**
   * Start connection with Jai Camera.
   * Returns Right(okMessage) or Left(errorMessage)
   */
  def jaiConnectionStart: Either[String, String] = {
    jaiConnection match {
      case Some(connection) ⇒ doJaiConnectionStart(connection)
      case None             ⇒ Left("Could not start Jai connection: no jaiConnection established")
    }
  }

  private def doJaiConnectionStart(connection: JaiCameraConnection): Either[String, String] = {
    try {
      connection.start()
      Right("Done")
    } catch {
      case ex: Exception ⇒ Left(ex.toString)
    }
  }

  /**
   * Shut down connection with Jai Camera.
   * Returns Right(okMessage) or Left(errorMessage)
   */
  def jaiConnectionShutdown: Either[String, String] = {
    jaiConnection match {
      case Some(connection) ⇒ doJaiConnectionShutdown(connection)
      case None             ⇒ Left("Could not shut down Jai connection: no jaiConnection established")
    }
  }

  private def doJaiConnectionShutdown(connection: JaiCameraConnection): Either[String, String] = {
    try {
      connection.shutdown()
      Right("Done")
    } catch {
      case ex: Exception ⇒ Left(ex.toString)
    }
  }

  /**
   * Send a status request to camera
   * Returns either:
   *    Right(true): Send request to camera without any trouble
   *    Right(false): Could not send request, because of connection problem. In this situation a retry is allowed.
   *    Left(errorMessage): Could not send request, because of a severe problem. No retry allowed.
   */
  def jaiConnectionSendCameraStatus: Either[String, Boolean] = {
    jaiConnection match {
      case Some(connection) ⇒ doJaiConnectionSendCameraStatus(connection)
      case None             ⇒ Left("Could not send status request over Jai connection: no jaiConnection established")
    }
  }

  private def doJaiConnectionSendCameraStatus(connection: JaiCameraConnection): Either[String, Boolean] = {
    try {
      connection.sendCameraStatus()
      Right(true)
    } catch {
      case ex: IllegalStateException ⇒ {
        if (ex.getMessage.contains("[DISCONNECTED]") || ex.getMessage.contains("[CONNECT_ERROR]")) {
          Right(false)
        } else
          Left(ex.toString)
      }
      case ex: Exception ⇒ Left(ex.toString)
    }
  }

}
