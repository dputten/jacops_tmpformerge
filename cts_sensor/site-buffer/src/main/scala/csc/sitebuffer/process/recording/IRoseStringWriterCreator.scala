/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.recording

import akka.actor.{ ActorRefFactory, ActorRef }

/**
 * This trait represents the ability to create a writer of IRoseString messages.
 * The writer for given recordingId is created using given ActorRefFactory.
 */
trait IRoseStringWriterCreator {
  // Returned actor must be able to handle IRoseString messages
  def createWriter(recordingId: String, actorRefFactory: ActorRefFactory): ActorRef
}
