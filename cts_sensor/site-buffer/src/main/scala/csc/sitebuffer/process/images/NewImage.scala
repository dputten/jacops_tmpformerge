/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sitebuffer.process.images

/**
 * Message created for each new image.
 */
case class NewImage(imagePath: String, timestamp: Long)
