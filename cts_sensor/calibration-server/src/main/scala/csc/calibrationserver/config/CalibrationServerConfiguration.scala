/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.calibrationserver.config

import com.typesafe.config.{ Config, ConfigFactory }
import csc.akka.logging.DirectLogging
import csc.akka.config.ConfigUtil._
import csc.amqp.{ AmqpConfiguration, ServicesConfiguration }
import csc.gantry.config._
import csc.systemevents._
import scala.concurrent.duration._
import csc.curator.utils.{ CuratorToolsImpl, Curator }
import org.apache.curator.framework.CuratorFrameworkFactory
import org.apache.curator.retry.RetryUntilElapsed
import net.liftweb.json.DefaultFormats
import csc.json.lift.EnumerationSerializer
import csc.vehicle.message.VehicleImageType
import java.util.concurrent.TimeUnit

//case class AmqpConfiguration(actorName: String, amqpUri: String, reconnectDelay: FiniteDuration)
//case class ServicesConfiguration(consumerQueue: String, producerEndpoint: String, producerRouteKey: String, serversCount: Int)

/**
 * The configuration used for the self test functionality
 * @param zkServers   Zookeeper servers connect strings
 * @param zkBaseSleepTimeMs Base sleep time for back-off Algorithms when connection to zookeeper
 * @param zkMaxRetries  Maximum of reconnect to zookeeper
 * @param monitorTimeout Timeout used of the responses from Monitor loggers
 * @param tableResults  HBase Table name of the selftest results
 * @param tableImages HBase Table name of the selfTest images
 * @param pollTime The poll/sleep time of the SelfTestLeader
 *
 */
case class SelfTestConfiguration(
  zkServers: String,
  zkBaseSleepTimeMs: Int,
  zkMaxRetries: Int,
  monitorTimeout: Int,
  tableResults: String,
  tableImages: String,
  pollTime: Int,
  userPath: String,
  workingDirectory: Option[String])

/**
 * Configuration of the gantry monitor application
 * @param selfTest The selftest configuration
 * @param responseTooOldAfter The time a response is too old to use
 * @param gantryRetryDuration the time between receiving an error response and the next restry
 * @param responseTimeoutDuration the time a response is expected
 * @param calibrationTimeOut the maximum time a calibration may take
 */
case class CalibrationServerConfiguration(
  selfTest: Option[SelfTestConfiguration],
  responseTooOldAfter: Duration,
  gantryRetryDuration: FiniteDuration,
  responseTimeoutDuration: FiniteDuration,
  calibrationTimeOut: FiniteDuration)

/**
 * Reading the configuration file and create the VehicleRegistration Configuration classes
 */
class Configuration(cur: Option[Curator]) extends DirectLogging {
  val config = ConfigFactory.load()
  private val rootConfig = "calibration-server"
  var curator: Option[Curator] = cur
  val amqpConfiguration = createAmqpConfig(config)
  val amqpCalibrationServicesConfiguration = createAmqpCalibrationServicesConfig(config)
  val serverConfiguration = createSystemMonitor(config)
  val laneConfiguration = createLaneConfig(config)
  val systemEventStore = createSystemEventStore(config)

  private def getCurator(servers: String): Curator = {
    log.info("Using zookeeper for Configuration")
    curator.getOrElse {
      val retryPolicy = new RetryUntilElapsed(3.days.toMillis.toInt, 5000)
      val client = CuratorFrameworkFactory.newClient(servers, retryPolicy)

      client.start()
      val formats = DefaultFormats + new EnumerationSerializer(VehicleImageType, ServiceType, ZkCaseFileType)
      val impl = new CuratorToolsImpl(Some(client), log, formats)
      curator = Some(impl)

      impl
    }
  }

  /**
   * Create the amqp-services configuration structure
   * @param config
   * @return CalibrationConfiguration the Calibration configuration
   */
  private def createAmqpCalibrationServicesConfig(config: Config): ServicesConfiguration = {
    val consumerQueue = config.getReplacedString(rootConfig + ".calibrationServices.consumer-queue")
    val producerEndpoint = config.getReplacedString(rootConfig + ".calibrationServices.producer-endpoint")
    val producerRouteKey = config.getReplacedString(rootConfig + ".calibrationServices.producer-route-key")
    val serversCount = config.getReplacedInteger(rootConfig + ".calibrationServices.servers-count")
    new ServicesConfiguration(consumerQueue, producerEndpoint, producerRouteKey, serversCount)
  }

  /**
   * Create the amqp configuration structure
   * @param config
   * @return CalibrationConfiguration the Calibration configuration
   */
  private def createAmqpConfig(config: Config): AmqpConfiguration = {
    val actorName = config.getReplacedString(rootConfig + ".amqp-connection.actor-name")
    val amqpUri = config.getReplacedString(rootConfig + ".amqp-connection.amqp-uri")
    val reconnectDelay = Duration.create(config.getReplacedMilliseconds(rootConfig + ".amqp-connection.reconnect-delay"), TimeUnit.MILLISECONDS)
    new AmqpConfiguration(actorName, amqpUri, reconnectDelay)
  }

  /**
   * Create the LaneConfiguration implementation
   * @param config
   * @return The LaneConfiguration implementation which has to be used
   */
  private def createLaneConfig(config: Config): LaneConfiguration = {
    val service = config.getReplacedString(rootConfig + ".laneConfig.zookeeper.service")
    if (service == "on") {
      log.info("Using zookeeper for LaneConfiguration")
      val servers = config.getReplacedString(rootConfig + ".laneConfig.zookeeper.servers")
      val labelPath = {
        val path = config.getReplacedString(rootConfig + ".laneConfig.zookeeper.labelPath", "")
        if (path.isEmpty) {
          None
        } else {
          Some(path)
        }
      }

      new ZookeeperLaneConfiguration(getCurator(servers), "/ctes/systems", labelPath)

    } else {
      val fileService = config.getReplacedString(rootConfig + ".laneConfig.file.service")
      if (fileService == "on") {
        val file = config.getReplacedString(rootConfig + ".laneConfig.file.fileName")
        log.info("Using jsonFile %s for LaneConfiguration".format(file))
        new FileLaneConfiguration(file, None)
      } else {
        log.info("Using Empty LaneConfiguration")
        new EmptyLaneConfiguration
      }
    }
  }

  /**
   * Create the SystemEventStore implementation
   * @param config
   * @return The SystemEventStore implementation which has to be used
   */
  def createSystemEventStore(config: Config): SystemEventStore = {
    val service = config.getReplacedString(rootConfig + ".systemEvent.zookeeper.service")
    val fileService = config.getReplacedString(rootConfig + ".systemEvent.file.service")

    if (service == "on") {
      log.info("Using zookeeper for SystemEventsStore")
      val servers = config.getReplacedString(rootConfig + ".systemEvent.zookeeper.servers")
      new ZookeeperSystemEventStore(getCurator(servers))
    } else if (fileService == "on") {
      val file = config.getReplacedString(rootConfig + ".systemEvent.file.fileName")
      log.info("Using jsonFile %s for SystemEventsStore".format(file))
      new FileSystemEventStore(file)
    } else {
      log.info("Using Empty SystemEventsStore")
      new EmptySystemEventStore
    }
  }

  /**
   * Create the gantry configuration structure
   * @param config
   * @return GantryConfiguration the gantry configuration
   */
  private def createSystemMonitor(config: Config): CalibrationServerConfiguration = {

    val selfTestService = config.getReplacedString(rootConfig + ".selftest.service")
    val selfCfg = if (selfTestService == "on") {
      Some(new SelfTestConfiguration(
        zkServers = config.getReplacedString(rootConfig + ".selftest.zookeeper.servers"),
        zkBaseSleepTimeMs = config.getReplacedInteger(rootConfig + ".selftest.zookeeper.baseSleep"),
        zkMaxRetries = config.getReplacedInteger(rootConfig + ".selftest.zookeeper.maxRetries"),
        monitorTimeout = config.getReplacedInteger(rootConfig + ".selftest.monitorTimeout"),
        tableResults = config.getReplacedString(rootConfig + ".selftest.resultTableName"),
        tableImages = config.getReplacedString(rootConfig + ".selftest.imageTableName"),
        pollTime = config.getReplacedInteger(rootConfig + ".selftest.sleepLeader"),
        userPath = "/ctes/users/",
        workingDirectory = None))
    } else {
      None
    }
    val responseTooOldAfter = config.getReplacedInteger(rootConfig + ".selftest.timers.responseTooOldAfter").millis
    val responseTimeoutDuration = config.getReplacedInteger(rootConfig + ".selftest.timers.responseTimeout").millis
    val gantryRetryDuration = config.getReplacedInteger(rootConfig + ".selftest.timers.retryDuration").millis
    val calibrationTimeOut = config.getReplacedInteger(rootConfig + ".selftest.timers.calibrationTimeout").millis

    val cfg = new CalibrationServerConfiguration(selfTest = selfCfg,
      responseTooOldAfter = responseTooOldAfter,
      responseTimeoutDuration = responseTimeoutDuration,
      gantryRetryDuration = gantryRetryDuration,
      calibrationTimeOut = calibrationTimeOut)
    log.info("Found gantryserverConfiguration: " + cfg)
    cfg
  }

}
