/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.calibrationserver.selftest

import akka.actor._
import csc.akka.logging.DirectLogging
import csc.amqp.MQConnection
import csc.gantry.config.{ CertificateValidator, CorridorService, LaneConfiguration }

import scala.concurrent.duration._
import csc.curator.utils.Curator
import csc.calibrationserver.config.Configuration
import com.github.sstone.amqp.Amqp.ChannelParameters
import com.github.sstone.amqp.Amqp
import csc.calibrationserver.calibration.{ SelfTestStore, SelftestTriggerActor, ZookeeperRegisterSelfTestTrigger }

/**
 * One component to schedule a selftest for one system.
 * There are two triggers one is a node "trigger" placed in zookeeper in given path.
 * The other trigger is a day change.
 * @param client The zookeeper Curator
 * @param systemId the system Id
 * @param corridorId the corridorId
 * @param actorFact The ActorFactory this can be a ActorSystem of ActorContext
 * @param store The store where to store the selftest results
 * @param pollTime
 * @param userPath
 * @param laneConfig
 * @param serialNr
 * @param configuration
 * @param certificateValidator
 * @param calibrationProducer
 * @param responseTooOldAfter
 * @param gantryRetryDuration
 * @param responseTimeoutDuration
 * @param calibrationTimeOut
 */
class SelfTestComponent(client: Curator,
                        systemId: String,
                        corridorId: String,
                        actorFact: ActorRefFactory,
                        store: SelfTestStore,
                        pollTime: Int,
                        userPath: String,
                        laneConfig: LaneConfiguration,
                        serialNr: (String) ⇒ String,
                        configuration: Configuration,
                        certificateValidator: CertificateValidator,
                        calibrationProducer: ActorRef,
                        responseTooOldAfter: Duration,
                        gantryRetryDuration: FiniteDuration,
                        responseTimeoutDuration: FiniteDuration,
                        calibrationTimeOut: FiniteDuration,
                        rabbitMQConnection: MQConnection,
                        actorSystem: ActorSystem,
                        curator: Curator) extends DirectLogging {

  private val triggerPath = CorridorService.getCorridorCalibrationTriggerPath(systemId, corridorId)
  private val trigger = new ZookeeperRegisterSelfTestTrigger(client = client, triggerNodePath = triggerPath)

  // The class that will handle the actual calibration message for sending to the gantry-queue
  private val corridorCalibrationActor = actorFact.actorOf(Props(
    new CorridorCalibrationActor(
      curator = client,
      systemId = systemId,
      corridorId = corridorId,
      store = store,
      triggerRegister = trigger,
      laneConfig = laneConfig,
      serialNr = serialNr,
      calibrationProducer = calibrationProducer,
      certificateValidator = certificateValidator,
      responseTooOldAfter = responseTooOldAfter,
      gantryRetryDuration = gantryRetryDuration,
      responseTimeoutDuration = responseTimeoutDuration,
      calibrationTimeOut = calibrationTimeOut)))

  // This is the actor who watches zookeeper and will inform the corridorCalibrationActor for changes
  private val selftestTriggerActor = actorFact.actorOf(Props(
    new SelftestTriggerActor(
      client = client,
      systemId = systemId,
      corridorId = corridorId,
      selfTestActor = corridorCalibrationActor,
      userPath = userPath,
      pollTime = pollTime millis)))

  /**
   * CorridorCalibrationActor are created through this class per corridor.
   * A corridor can contain (entry,exit) multiple gantries
   * Calibration request messages are send to each single gantry by the CorridorCalibrationActor
   * CorridorCalibrationActor handles the response messages from all the gantries in a corridor.
   * In the code below a consumer is created for each gantry in the corridor and a reference to the calibrationactor is given
   * as a parameter because the CorridorCalibrationActor handles all the GantryCalibrationResponse messages for a all gantries in the corridor
   */
  val gantryIds = CorridorService.getCorridorGantryIds(curator, systemId, corridorId)
  val calibrationConsumers = gantryIds.map(gantryId ⇒ gantryId -> Some(actorFact.actorOf(Props(new CalibrationConsumer(corridorCalibrationActor))))).toMap
  val simpleConsumers = calibrationConsumers.map({
    case (gantryId, calibrationConsumer) ⇒
      rabbitMQConnection.createSimpleConsumer(createQueueName(gantryId), calibrationConsumer.get,
        Some(ChannelParameters(5)), autoack = false)
  })

  def stop() {
    actorFact.stop(selftestTriggerActor)
    actorFact.stop(corridorCalibrationActor)
    calibrationConsumers.foreach({ case (gantryId, calibrationConsumer) ⇒ actorFact.stop(calibrationConsumer.get) })
    simpleConsumers.foreach(simpleConsumer ⇒ actorFact.stop(simpleConsumer))
  }

  def createQueueName(gantryId: String): String = { "calibration" + gantryId }
  def getSystemId = systemId
  def getCorridorId = corridorId
}