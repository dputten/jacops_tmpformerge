/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.calibrationserver.selftest

import java.text.SimpleDateFormat
import java.util.Date

import scala.concurrent.duration._
import csc.akka.logging.DirectLogging
import csc.calibration._
import csc.calibrationserver.calibration.StartCalibration
import org.apache.commons.codec.digest.DigestUtils

case class GantryState(gantryId: String, response: Either[String, GantryCalibrationResponse])
case class CalibrationState(start: StartCalibration, gantries: Seq[GantryState])

class CalibrationFlow(tooOld: Duration) extends DirectLogging {
  val dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS")
  var currentCalibration = Seq[CalibrationState]()

  /**
   * Add the calibration to the state only when there are no other requests within 1 minute
   * @param start the new calibration request
   * @param gantryIds the gantries part of this request
   */
  def addNewCalibrationRequest(start: StartCalibration, gantryIds: Seq[String]) {
    val sameRequest = currentCalibration.find(current ⇒ {
      val diff = math.abs(start.time.getTime - current.start.time.getTime)
      diff < 1.minute.toMillis
    })
    if (sameRequest.isDefined) {
      log.warning("CalibrationFlow: Received Calibration request within 1 minute. Skip this request %s".format(start))
    } else {
      currentCalibration = currentCalibration :+ CalibrationState(start, gantryIds.map(GantryState(_, Left("No response"))))
    }
  }

  /**
   * Get all the finished calibration requests and remove them form the state
   * @return the finished calibrations
   */
  def popFinishedCalibrations(): Seq[CalibrationState] = {
    //check for calibrations which can be processed
    val (processable, unfinished) = currentCalibration.partition(state ⇒ {
      state.gantries.foldLeft(true) { case (response, gantry) ⇒ response && gantry.response.isRight }
    })
    currentCalibration = unfinished
    processable
  }

  /**
   * Get all the calibration requests which started before timeoutTime and remove them form the state
   * @return the timedOut calibrations
   */
  def popTimeOutCalibrations(timeoutTime: Long): Seq[CalibrationState] = {
    //check for calibrations which started before timeoutTime
    val (timedOut, pending) = currentCalibration.partition(_.start.time.getTime <= timeoutTime)
    currentCalibration = pending
    timedOut
  }

  /**
   * Update the calibration states which are waiting for this response.
   * When the response has errors it is not used
   * And a response can only be used when the creation of the response isn't too old
   * @param result
   * @return
   */
  def updateStateWithResponse(result: GantryCalibrationResponse): Boolean = {
    log.info("CalibrationFlow: Received respons for gantry %s".format(result.gantryId))
    var waitedFor = false

    val errors = getErrors(result)
    val updateValue = if (errors.size > 0) {
      val errorDetails = errors.map(step ⇒ step.stepName + ": " + step.detail.getOrElse(""))
      log.info("Received response for gantry %s with errors %s".format(result.gantryId, errorDetails))
      Left(errorDetails.mkString(", "))
    } else {
      Right(result)
    }

    val updatedState = currentCalibration.map { currentState ⇒
      {
        if (isResponsePartOfCalibration(currentState.start.time.getTime, result.sendTime)) {
          val gantries = currentState.gantries.map(gantryState ⇒ {
            //only update when there isn't a response yet
            if (gantryState.gantryId == result.gantryId && gantryState.response.isLeft) {
              waitedFor = true
              gantryState.copy(response = updateValue)
            } else {
              if (gantryState.gantryId == result.gantryId) {
                log.info("Received an double response from gantry %s with time %s".format(result.gantryId, dateFormat.format(new Date(result.time))))
              }
              gantryState
            }
          })
          currentState.copy(gantries = gantries)
        } else {
          currentState
        }
      }
    }

    currentCalibration = updatedState
    waitedFor
  }

  def waitForResponse(gantryId: String): Boolean = {
    val waitGantries = currentCalibration.flatMap(_.gantries).filter(grty ⇒ grty.gantryId == gantryId && grty.response.isLeft)
    waitGantries.size > 0
  }

  def isResponsePartOfCalibration(calibrationTime: Long, responseTime: Long): Boolean = {
    (calibrationTime <= responseTime + tooOld.toMillis)
  }

  /**
   * Check if the images has been correctly received
   * @param img the image to check
   * @return true when a image is incorrect received
   */
  def hasImageErrors(img: CameraImage): Boolean = {
    val image = img.image.map(_.toByte).toArray
    val checksum = DigestUtils.md5Hex(image)
    img.checksum != checksum
  }

  def getErrors(result: GantryCalibrationResponse): Seq[StepResult] = {
    val stepErrors = result.results.filter(_.success == false)
    val images = result.images.filter(img ⇒ hasImageErrors(img))

    if (images.isEmpty) {
      stepErrors
    } else {
      stepErrors :+ StepResult("image-validation", false, Some(images.map(_.imageId).mkString(", ")))
    }
  }
  def hasErrors(result: GantryCalibrationResponse): Boolean = {
    result.results.exists(_.success == false)
  }

}