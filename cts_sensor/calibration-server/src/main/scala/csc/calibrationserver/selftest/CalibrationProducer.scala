package csc.calibrationserver.selftest

import akka.actor.{ ActorLogging, ActorRef }
import csc.amqp.{ AmqpSerializers, PublisherActor }
import net.liftweb.json.DefaultFormats
import csc.calibration.GantryCalibrationRequest

/**
 * This class listens for GantryCalibrationResponse requests and send the request to the queue
 */
class CalibrationProducer(val producer: ActorRef, val producerEndpoint: String) extends PublisherActor
  with ActorLogging {
  override val ApplicationId = "calibration"
  override implicit val formats = DefaultFormats
  def responseName = "gantryCalibrationRequest"

  def producerRouteKey(request: AnyRef): String = {
    request match {
      case gantryCalibrationRequest: GantryCalibrationRequest ⇒ {
        gantryCalibrationRequest.systemId + "." + gantryCalibrationRequest.gantryId + "." + "GantryCalibrationRequest"
      }
      case other ⇒ request.getClass.getName
    }
  }

  override def preStart() {
    super.preStart()
    log.info("Started CalibrationProducer")
  }

  override def postStop() {
    super.postStop()
    log.info("Stopped CalibrationProducer")
  }

  override def preRestart(reason: Throwable, message: Option[Any]) {
    log.error(reason, "PreRestart CalibrationProducer while processing %s".format(message))
    super.preRestart(reason, message)
  }
}
