/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.calibrationserver.selftest

import java.util.Date

import akka.actor.{ Actor, ActorLogging, ActorRef }
import csc.gantry.config.{ CertificateValidator, CorridorService, LaneConfig, LaneConfiguration }
import csc.curator.utils.Curator
import java.text.SimpleDateFormat

import csc.calibration._
import csc.systemevents.SystemEvent
import csc.calibration.GantryCalibrationResponse
import csc.calibration.GantryCalibrationRequest
import csc.calibrationserver.calibration.{ RegisterSelfTestTrigger, SelfTestStore, SelftestImage, StartCalibration }
import csc.vehicle.message.Lane

import scala.concurrent.duration._

case class ResendGantryCalibration(systemId: String, gantryId: String, startCalibrationTime: Long)
case class NoResponseGantryCalibration(systemId: String, gantryId: String, startCalibrationTime: Long)
case class TimeoutCalibration(calibrationRequest: StartCalibration)

/**
 * This actor is triggered when a self-test has to be done.
 * Its functions is to trigger all tests and collect the results. When all results are received the self-test result is
 * generated. The result is one system event indication a success or failure of the test, a record in the self-test
 * store (in production this is hbase) with all the images and updates the trigger in the registry (zookeeper).
 * @param curator The zookeeper curator
 * @param systemId the system Id
 * @param corridorId The corridorId
 * @param store The Selftest Store where the results are stored
 * @param triggerRegister the trigger registry where the trigger can be set to processed
 * @param laneConfig the laneConfiguration object
 */
class CorridorCalibrationActor(curator: Curator, systemId: String, corridorId: String,
                               store: SelfTestStore, triggerRegister: RegisterSelfTestTrigger,
                               laneConfig: LaneConfiguration, serialNr: (String) ⇒ String,
                               calibrationProducer: ActorRef,
                               certificateValidator: CertificateValidator,
                               responseTooOldAfter: Duration,
                               gantryRetryDuration: FiniteDuration,
                               responseTimeoutDuration: FiniteDuration,
                               calibrationTimeOut: FiniteDuration) extends Actor with ActorLogging {

  implicit val executor = context.system.dispatcher

  val dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS")
  val calibrationState = new CalibrationFlow(responseTooOldAfter)

  override def preStart() {
    store.start()
    log.info("Start CorridorCalibrationActor for system: %s corridor: %s".format(systemId, corridorId))
  }
  override def postStop() {
    store.stop()
    log.info("Stop CorridorCalibrationActor for system: %s corridor: %s".format(systemId, corridorId))
  }

  override def preRestart(reason: Throwable, message: Option[Any]) {
    log.error(reason, "PreRestart CorridorCalibrationActor system: %s corridor: %s while processing %s".format(systemId, corridorId, message))
    super.preRestart(reason, message)
  }

  def receive = {
    case startCalibration: StartCalibration ⇒
      //start calibration
      log.info(sender.toString())
      context.system.eventStream.publish(SystemEvent("selftest", System.currentTimeMillis(), systemId, "SELFTEST-START", startCalibration.userId, startCalibration.reason, corridorId = Some(corridorId)))
      log.info("CorridorCalibrationActor: Start calibration system: %s corridor: %s".format(systemId, corridorId))

      val certErrors = certificateValidator.validateCertificatesForSystemId(systemId, startCalibration.time.getTime)
      if (certErrors.length > 0) {
        //failing certificates
        log.warning("CorridorCalibrationActor: Certificate test Failed: %s".format(certErrors))
        processCalibrationState(CalibrationState(startCalibration, Seq()), certErrors)
      } else {
        //OK
        log.info("CorridorCalibrationActor: Certificate test OK system: %s corridor: %s".format(systemId, corridorId))

        val gantryIds = CorridorService.getCorridorGantryIds(curator, systemId, corridorId)
        log.info("gantryId:" + gantryIds.mkString("--"))
        gantryIds.foreach(gantryId ⇒ {
          sendGantryRequest(systemId, gantryId, startCalibration.time.getTime)
        })
        calibrationState.addNewCalibrationRequest(startCalibration, gantryIds)
        context.system.scheduler.scheduleOnce(calibrationTimeOut, self, TimeoutCalibration(startCalibration))
      }

    case result: GantryCalibrationResponse ⇒ {
      log.info("CorridorCalibrationActor: Received respons for system: %s corridor: %s gantry %s".format(systemId, corridorId, result.gantryId))
      val imageErrors = result.images.exists(calibrationState.hasImageErrors(_))
      if (imageErrors) {
        //send directly a new request
        sendGantryRequest(result.systemId, result.gantryId, result.time)
        //only update state
        calibrationState.updateStateWithResponse(result)
      } else {
        val used = calibrationState.updateStateWithResponse(result)
        if (used) {
          //check for calibrations which can be processed
          val processable = calibrationState.popFinishedCalibrations()
          processable.foreach(state ⇒ {
            log.info("CorridorCalibrationActor: Create result system: %s corridor: %s".format(systemId, corridorId))
            processCalibrationState(state)
          })
        } else {
          if (calibrationState.hasErrors(result)) {
            //retry gantry request
            context.system.scheduler.scheduleOnce(gantryRetryDuration, self, ResendGantryCalibration(result.systemId, result.gantryId, result.time))
          } else {
            log.info("Received an unexpected response from system: %s corridor: %s gantry %s with time %s".format(systemId, corridorId, result.gantryId, dateFormat.format(new Date(result.time))))
          }
        }
      }
    }
    case TimeoutCalibration(timeout) ⇒ {
      log.debug("CorridorCalibrationActor: Received TimeoutCalibration system: %s corridor: %s".format(systemId, corridorId))
      //create calibration response anyway
      val timeoutCalibrations = calibrationState.popTimeOutCalibrations(timeout.time.getTime)
      timeoutCalibrations.foreach(state ⇒ {
        log.info("CorridorCalibrationActor: CalibrationTimeout requestTime=%s Create result system: %s corridor: %s".format(dateFormat.format(state.start.time), systemId, corridorId))
        processCalibrationState(state)
      })
    }
    case resend: ResendGantryCalibration ⇒ {
      sendGantryRequest(resend.systemId, resend.gantryId, resend.startCalibrationTime)
    }
    case resend: NoResponseGantryCalibration ⇒ {
      log.debug("CorridorCalibrationActor: Received NoResponseGantryCalibration for system: %s corridor: %s gantry %s".format(resend.gantryId, systemId, corridorId))
      if (calibrationState.waitForResponse(resend.gantryId)) {
        sendGantryRequest(resend.systemId, resend.gantryId, resend.startCalibrationTime)
      }
    }
  }

  /**
   * Send a gantry calibration request and schedule a response timeout
   * @param systemId the system ID
   * @param gantryId the gantry id
   * @param startCalibrationTime the start time of the calibration
   */
  private def sendGantryRequest(systemId: String, gantryId: String, startCalibrationTime: Long) {
    val laneIds = getActiveLanes(systemId, corridorId).filter(_.gantry == gantryId).map(lane ⇒ lane.laneId)

    calibrationProducer ! GantryCalibrationRequest(systemId, gantryId, startCalibrationTime, laneIds)
    log.info("Sending message for system: %s corridor: %s gantry %s".format(systemId, corridorId, gantryId.toString))
    val msg = NoResponseGantryCalibration(systemId, gantryId, startCalibrationTime)
    context.system.scheduler.scheduleOnce(responseTimeoutDuration, self, msg)
  }

  /**
   * create a result for a calibration result
   * @param state the calibration request
   */
  private def processCalibrationState(state: CalibrationState, foundErrors: Seq[String] = Seq()) {

    var errors = if (foundErrors.isEmpty) {
      state.gantries.foldLeft(Seq[String]()) {
        case (errorList, gantry) ⇒ gantry.response match {
          case Right(_)  ⇒ errorList
          case Left(str) ⇒ errorList :+ str
        }
      }
    } else {
      foundErrors
    }
    val resultImages = state.gantries.foldLeft(Seq[CameraImage]()) {
      case (imageList, gantry) ⇒ gantry.response match {
        case Right(img) ⇒ imageList ++ img.images
        case Left(str)  ⇒ imageList
      }
    }
    val images = resultImages.map(img ⇒ {
      val byteArray = img.image.map(_.toByte).toArray
      SelftestImage(img.imageId, byteArray)
    })

    //put result to the store (Hbase)
    try {
      store.store(systemId = state.start.systemId,
        corridorId = state.start.corridorId,
        nrCameras = images.size,
        time = state.start.time,
        images = images,
        errors = errors,
        reportingOfficerCode = state.start.reportingOfficerCode,
        serialNr = serialNr(systemId))
    } catch {
      case ex: Exception ⇒ {
        log.error(ex, "Failed to store result system: %s corridor: %s".format(systemId, corridorId))
        errors = errors :+ "Bewaren foto's gefaald"
      }
    }

    //create system event
    val sysReason = if (errors.isEmpty) {
      state.start.reason
    } else {
      "Calibratie is mislukt.\n" + errors.mkString("\n")
    }
    val endEvent = new SystemEvent(componentId = "selftest",
      timestamp = System.currentTimeMillis(),
      systemId = systemId,
      corridorId = Some(corridorId),
      eventType = "SELFTEST-END",
      userId = state.start.userId,
      reason = sysReason)
    context.system.eventStream.publish(endEvent)

    val event = new SystemEvent(componentId = "selftest",
      timestamp = state.start.time.getTime,
      systemId = systemId,
      corridorId = Some(corridorId),
      eventType = if (errors.isEmpty) "SELFTEST-SUCCESS" else "SELFTEST-ERROR",
      userId = state.start.userId,
      reason = sysReason)
    context.system.eventStream.publish(event)

    if (state.start.reason != "MIDNIGHT") {
      //update Zookeeper trigger
      triggerRegister.processedTrigger()
    }
    if (errors.isEmpty) {
      log.info("CorridorCalibrationActor Selftest system: %s corridor: %s is successful completed. %s".format(systemId, corridorId, sysReason))
    } else {
      log.info("CorridorCalibrationActor Selftest system: %s corridor: %s has failed. %s".format(systemId, corridorId, sysReason))
    }
  }

  private def getLaneConfigs(systemId: String, corridorId: String): Seq[LaneConfig] = {
    val gantries = CorridorService.getCorridorGantryIds(curator, systemId, corridorId)
    gantries.flatMap(gantryId ⇒ laneConfig.getLanesForGantry(systemId, gantryId))
  }

  protected def getActiveLanes(systemId: String, corridorId: String): Seq[Lane] = {
    //get all corridor lanes
    val lanes = getLaneConfigs(systemId, corridorId)

    val lanesWithAlerts = getLanesWithAlerts(systemId, corridorId)

    lanes.flatMap(lane ⇒ {
      if (!lanesWithAlerts.contains(lane.lane.laneId)) {
        //add only when there are no alerts
        Some(lane.lane)
      } else {
        log.warning("system: %s corridor: %s Lane [%s] is turned off due to alerts".format(systemId, corridorId, lane.lane.laneId))
        None
      }
    })
  }

  def getLanesWithAlerts(systemId: String, corridorId: String): Seq[String] = {
    //get all alerts
    val alerts = CorridorService.getAllAlerts(curator, systemId, corridorId)
    val lanesWithAlerts = alerts.map(alert ⇒ {
      val id = alert.getLaneId()
      log.debug("system: %s corridor: %s Convert path=%s to laneId=%s".format(systemId, corridorId, alert.path, id))
      id
    })
    lanesWithAlerts
  }
}
