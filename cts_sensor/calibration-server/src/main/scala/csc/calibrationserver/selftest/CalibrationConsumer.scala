/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.calibrationserver.selftest

import akka.actor._
import com.github.sstone.amqp.Amqp.{ Reject, Ack, Delivery }
import csc.amqp.AmqpSerializers
import net.liftweb.json.DefaultFormats
import csc.calibration.GantryCalibrationResponse

/**
 * This class listens to the queue for calibration requests and send the request to the Calibration
 */
class CalibrationConsumer(calibrationActor: ActorRef)
  extends Actor with ActorLogging with AmqpSerializers {
  implicit val system = context.system
  override implicit val formats = DefaultFormats
  override val ApplicationId = "CalibrationConsumer"

  object GantryCalibrationResponseExtractor extends MessageExtractor[GantryCalibrationResponse]

  def receive = {
    case delivery @ GantryCalibrationResponseExtractor(message) ⇒ {
      val consumer = sender
      log.info("Received Delivery")
      calibrationActor ! message
      consumer ! Ack(delivery.envelope.getDeliveryTag)
    }

    case delivery: Delivery ⇒ {
      log.info("Received Delivery")
      val consumer = sender
      log.error("Dropping message with tag {} - cannot determine message type", delivery.envelope.getDeliveryTag)
      consumer ! Reject(delivery.envelope.getDeliveryTag, false)
    }
  }

  override def preStart() {
    super.preStart()
    log.info("Started CalibrationConsumer")
  }

  override def postStop() {
    super.postStop()
    log.info("Stopped CalibrationConsumer")
  }
  override def preRestart(reason: Throwable, message: Option[Any]) {
    log.error(reason, "PreRestart CalibrationConsumer while processing %s".format(message))
    super.preRestart(reason, message)
  }

}