/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.calibrationserver.startup

import csc.akka.logging.DirectLogging
import akka.kernel.Bootable
import akka.actor.{ ActorRef, ActorSystem, Props }
import csc.amqp.MQConnection
import csc.calibrationserver.Update

import scala.concurrent.duration._
import net.liftweb.json.DefaultFormats
import csc.amqp.JsonSerializers
import csc.calibrationserver.config.{ Configuration, SelfTestConfiguration }
import csc.curator.utils.Curator
import csc.gantry.config.{ CertificateValidator, CertificateValidatorZookeeper }
import csc.calibrationserver.calibration.{ HBaseSelfTestStore, SelfTestStore }

/**
 * Boot class Starts Sensor monitor
 */
class Boot extends Bootable with JsonSerializers with DirectLogging {
  implicit val system = ActorSystem("CalibrationServer")
  var startupCalibrationServer: Option[ActorRef] = None
  var curator: Curator = _
  var selfTestStore: SelfTestStore = _
  var certificateValidator: CertificateValidator = _

  override implicit val formats = DefaultFormats

  implicit val executor = system.dispatcher

  def getAmqpConnection(config: Configuration): MQConnection = {
    val connection = MQConnection.create(config.amqpConfiguration.actorName, config.amqpConfiguration.amqpUri,
      config.amqpConfiguration.reconnectDelay)
    log.info("Amqp connection created")
    connection
  }

  def startup = {
    log.info("Starting Calibration Server - Boot")

    val localCurator = if (curator == null) None else Some(curator)
    val configuration = new Configuration(localCurator)
    if (curator == null) {
      curator = configuration.curator.get
    }

    val amqpConnection = getAmqpConnection(configuration)
    val amqpProducer = amqpConnection.producer
    val monitorConfiguration = configuration.serverConfiguration

    if (certificateValidator == null) certificateValidator = new CertificateValidatorZookeeper(curator)

    def createSelfTestStore(selfCfg: SelfTestConfiguration): SelfTestStore = {
      if (selfTestStore == null)
        new HBaseSelfTestStore(quorum = selfCfg.zkServers, tableNameResults = selfCfg.tableResults, tableNameImages = selfCfg.tableImages)
      else
        getSelfTestStore
    }

    def getSelfTestStore(): SelfTestStore = {
      selfTestStore
    }

    startupCalibrationServer = Some(system.actorOf(Props(
      new StartupCalibrationServer(Some(curator),
        monitorConfiguration, configuration.laneConfiguration,
        configuration.systemEventStore, createSelfTestStore, configuration, amqpProducer, certificateValidator, amqpConnection))))
    system.scheduler.schedule(1.minutes, 1.minutes, startupCalibrationServer.get, Update)

    log.info("Calibration Server is started")
  }

  def shutdown = {
    startupCalibrationServer.foreach(x ⇒ system.stop(x))
    system.shutdown()
  }
}
