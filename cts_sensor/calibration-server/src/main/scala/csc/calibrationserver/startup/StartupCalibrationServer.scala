/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.calibrationserver.startup

import akka.actor._
import csc.amqp.MQConnection
import csc.calibrationserver.Update
import csc.calibrationserver.calibration.SelfTestStore
import csc.calibrationserver.config.{ CalibrationServerConfiguration, SelfTestConfiguration, _ }
import csc.calibrationserver.selftest.{ CalibrationProducer, SelfTestComponent }
import csc.curator.utils.Curator
import csc.gantry.config.{ CertificateValidator, LaneConfiguration }
import csc.systemevents.{ SystemEventListener, SystemEventStore }

/**
 * Helper to start the complete Sensor Monitor
 */
class StartupCalibrationServer(curatorOpt: Option[Curator],
                               calibrationServerConfiguration: CalibrationServerConfiguration,
                               laneConfig: LaneConfiguration,
                               systemEventStore: SystemEventStore,
                               createSelfTestStore: (SelfTestConfiguration) ⇒ SelfTestStore,
                               configuration: Configuration,
                               producer: ActorRef,
                               certificateValidator: CertificateValidator,
                               rabbitMQConnection: MQConnection) extends Actor with ActorLogging {

  log.info("StartupCalibrationServer calibrationServerConfiguration: {}", calibrationServerConfiguration)
  log.info("StartupCalibrationServer laneConfig: {}", laneConfig)
  log.info("StartupCalibrationServer config: {}", configuration)

  private var selfTestStoreOpt: Option[SelfTestStore] = None
  protected var selftestCompList = Seq[SelfTestComponent]()
  implicit val system = context.system

  def receive = {
    case Update ⇒
      update()
  }

  override def preStart() {
    super.preStart()
    log.info("StartupCalibrationServer preStart")
    init()
  }

  override def postStop() {
    stop()
    super.postStop()
  }

  /**
   * Creating actors and start the image server.
   * returns when the VehicleRegistration is up and running
   */
  def init() {
    context.actorOf(Props(new SystemEventListener(systemEventStore)))

    selfTestStoreOpt = calibrationServerConfiguration.selfTest.map(createSelfTestStore(_))
    selfTestStoreOpt.foreach(_.start())
    update()
    log.info("StartupCalibrationServer done")
  }

  def update() {

    log.debug(calibrationServerConfiguration.selfTest.toString)

    for (
      curator ← curatorOpt;
      selfTestStore ← selfTestStoreOpt
    ) {
      log.info("StartupCalibrationServer updating corridors")

      val systemCorridorIds: Seq[(String, String)] = getCorridorsForSystem

      log.info("startMonitor found the corridors= " + systemCorridorIds)

      val newCorridors: Seq[(String, String)] = getNewCorridors(systemCorridorIds)

      startNewCorridors(newCorridors, curator, selfTestStore)

      stopOldCorridors(systemCorridorIds)

      log.info("Selftest is updated")
    }
  }

  def getNewCorridors(systemCorridorIds: Seq[(String, String)]): Seq[(String, String)] = {
    val newCorridors = systemCorridorIds.filter {
      case (systemId, corridorId) ⇒
        findSelfTestComponent(systemId, corridorId).isEmpty
    }
    newCorridors
  }

  def stopOldCorridors(systemCorridorIds: Seq[(String, String)]) {
    //stop old corridors
    val (current, deleted) = selftestCompList.partition(com ⇒ systemCorridorIds.contains((com.getSystemId, com.getCorridorId)))
    selftestCompList = current
    deleted.foreach(comp ⇒ {
      log.info("Stop selftest for [%s:%s]".format(comp.getSystemId, comp.getCorridorId))
      comp.stop()
    })
  }

  def getCorridorsForSystem: Seq[(String, String)] = {
    //get all corridors
    val systemIds = laneConfig.getAllSystemIds()
    log.info("startMonitor found the systems= " + systemIds)
    val systemCorridorIds = systemIds.flatMap(systemId ⇒ {
      laneConfig.getCorridorsConfig(systemId).map(_.corridors.map(cor ⇒ (systemId, cor.id)))
    }).flatten
    systemCorridorIds
  }

  def startNewCorridors(newCorridors: Seq[(String, String)], curator: Curator, selfTestStore: SelfTestStore) {
    newCorridors.foreach {
      case (systemId, corridorId) ⇒
        log.info("Start Selftest for system, corridor-> [%s:%s]".format(systemId, corridorId))

        selftestCompList = selftestCompList :+ new SelfTestComponent(
          client = curator,
          systemId = systemId,
          corridorId = corridorId,
          actorFact = context,
          store = selfTestStore,
          pollTime = 10000,
          userPath = "/ctes/users/",
          laneConfig = laneConfig,
          serialNr = getSerialNumber,
          configuration = configuration,
          certificateValidator = certificateValidator,
          calibrationProducer = createCalibrationProducer,
          responseTooOldAfter = configuration.serverConfiguration.responseTooOldAfter,
          gantryRetryDuration = configuration.serverConfiguration.gantryRetryDuration,
          responseTimeoutDuration = configuration.serverConfiguration.responseTimeoutDuration,
          calibrationTimeOut = configuration.serverConfiguration.calibrationTimeOut,
          rabbitMQConnection, system, curator)
    }
  }

  // Todo: Naar startup. er is er maar 1 nodig.
  def createCalibrationProducer: ActorRef = {
    context.actorOf(Props(new CalibrationProducer(producer, configuration.amqpCalibrationServicesConfiguration.producerEndpoint)))
  }

  def findSelfTestComponent(systemId: String, corridorId: String): Option[SelfTestComponent] = {
    selftestCompList.find(comp ⇒ comp.getCorridorId == corridorId && comp.getSystemId == systemId)
  }

  def getSerialNumber(systemId: String): String = {
    val systemCfgOpt = laneConfig.getSystemConfig(systemId)
    systemCfgOpt.map(_.serialNumber.serialNumber).getOrElse("")
  }

  def stop() = {
    for (selfTest ← selftestCompList) {
      selfTest.stop()
    }
    selftestCompList = Seq()

    //Stop the shared components used by the different SelfTestComponents
    selfTestStoreOpt.foreach(store ⇒ store.stop())
    selfTestStoreOpt = None
  }
}

