/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.calibrationserver.jai

import java.util.Date
import csc.vehicle.message.Lane

/**
 * Message to request to get the status of the camera.
 */
//case class JaiStatusRequest()

/**
 * Message to request to get an image from the camera
 */
//case class JaiImageRequest()

/**
 * Successfull Response to request to get an image from the camera
 */
case class JaiImageResponse(lane: Lane, image: Array[Byte], time: Option[Date])

/**
 * Error Response to request to get an image from the camera
 */
//case class JaiImageError(lane: Lane, error: Seq[CameraError])

/**
 * Response to request to get the status of the camera
 */
//case class JaiStatusResponse(lane: Lane, errors: Seq[CameraError])
