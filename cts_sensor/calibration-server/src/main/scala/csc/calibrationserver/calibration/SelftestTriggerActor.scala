/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.calibrationserver.calibration

import java.util.Calendar

import akka.actor.{ Actor, ActorLogging, ActorRef, Cancellable }
import csc.curator.utils.Curator

import scala.concurrent.duration._

/**
 * Actor to manage the selftest triggers
 * @param client the zookeeper curator
 * @param systemId The system Id
 * @param corridorId The corridor Id
 * @param selfTestActor the selftest actor
 * @param userPath zookeeper path to the users
 * @param pollTime polltime of the midnight check
 */
class SelftestTriggerActor(client: Curator, systemId: String, corridorId: String, selfTestActor: ActorRef, userPath: String, pollTime: FiniteDuration) extends Actor with ActorLogging {

  implicit val executor = context.system.dispatcher

  private val watch = new WatchSelfTestTrigger(client, systemId, corridorId, selfTestActor, userPath)
  private var lastDayTrigger = Calendar.getInstance().get(Calendar.DAY_OF_YEAR)
  private var schedule: Option[Cancellable] = None

  override def preStart() {
    super.preStart()

    watch.start()
    //use a continue schedule to make sure never to lose a Tick message
    val cancel = context.system.scheduler.schedule(modifySleepTime(pollTime), pollTime, self, Tick)
    schedule = Some(cancel)
    log.info("Started SelftestTriggerActor for system [%s:%s]".format(systemId, corridorId))
  }

  override def postStop() {
    super.postStop()
    watch.stop()
    schedule.foreach(_.cancel())
    log.info("Stopped SelftestTriggerActor for system %s ".format(systemId))
  }

  def receive = {
    case Tick ⇒ {
      log.info("Tick")
      //check day change
      val cal = Calendar.getInstance()
      val day = cal.get(Calendar.DAY_OF_YEAR)
      if (day != lastDayTrigger) {
        lastDayTrigger = day
        cal.set(Calendar.HOUR_OF_DAY, 0)
        cal.set(Calendar.MINUTE, 0)
        cal.set(Calendar.SECOND, 0)
        cal.set(Calendar.MILLISECOND, 0)
        watch.midnightTrigger(cal.getTime)
      }
    }
  }

  private def modifySleepTime(sleepTime: FiniteDuration): FiniteDuration = {
    val endOfDay = Calendar.getInstance()
    endOfDay.set(Calendar.HOUR_OF_DAY, 24)
    endOfDay.set(Calendar.MINUTE, 0)
    endOfDay.set(Calendar.SECOND, 0)
    endOfDay.set(Calendar.MILLISECOND, 0)
    val endOfDayTime = endOfDay.getTimeInMillis
    val timeLeftInDay = endOfDayTime - System.currentTimeMillis()
    if (timeLeftInDay < sleepTime.toMillis) {
      math.max(timeLeftInDay, 1000).millis //minimum delay is one second
    } else {
      sleepTime
    }
  }
}

object Tick