/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.calibrationserver.calibration

import java.util.Date

import net.liftweb.json.{ DefaultFormats, Serialization }
import org.apache.hadoop.hbase._
import org.apache.hadoop.hbase.client.{ HBaseAdmin, HTable, Put }
import org.apache.hadoop.hbase.util.Bytes
import org.slf4j.LoggerFactory

import scala.collection.mutable.ListBuffer

/**
 * The interface of the SelfTest store used to be able to test without hbase
 */
trait SelfTestStore {
  /**
   * Store a self test result into the store
   * @param systemId the systemId
   * @param nrCameras the number of cameras tested
   * @param time the time of the self test
   * @param images the list of images
   * @param errors the list of errors
   */
  def store(systemId: String, corridorId: String, nrCameras: Int, time: Date, images: Seq[SelftestImage], errors: Seq[String],
            reportingOfficerCode: String, serialNr: String)

  /**
   * stop the store
   */
  def stop()

  /**
   * start the store
   */
  def start()
}

/**
 * Key class for storing and retrieving objects in the data store
 * @param time The timestamp of the object
 * @param id The id of the object
 */
case class KeyWithTimeAndId(time: Long, id: String) {
  def createKey(): Array[Byte] = {
    //Bytes.toBytes(time) ++ id.getBytes
    Bytes.toBytes("%s-%s".format(time.toString, id))
  }
}

/**
 * The class stored as json object
 * @param systemId  The systemId of the tests
 * @param corridorId The corridorId of the test
 * @param time  The time of the test
 * @param success The result of the test
 * @param nrCamerasTested The number of cameras tested
 * @param images The list of images
 * @param errors The list of found errors
 */
case class SelfTestResult(systemId: String,
                          corridorId: String,
                          time: Long,
                          success: Boolean,
                          reportingOfficerCode: String,
                          nrCamerasTested: Int,
                          images: Seq[KeyWithTimeAndId],
                          errors: Seq[String],
                          serialNr: String)

case class SelftestImage(id: String, image: Array[Byte])

/**
 * The Hbase implementation of the SelfTestStore
 * HTable isn't threadSave so this should be dealt with
 * @param quorum  The zookeeper quorum used to configure hbase
 * @param tableNameResults The table name where the json results hase to be stored
 * @param tableNameImages The table name where the images are stored
 */
class HBaseSelfTestStore(quorum: String, tableNameResults: String, tableNameImages: String) extends SelfTestStore {
  val log = LoggerFactory.getLogger(getClass)

  private val columnFamily = "cf".getBytes()
  private val attributeResult = "R".getBytes()
  private val attributeImage = "I".getBytes()

  private val config = HBaseConfiguration.create()
  config.set("hbase.zookeeper.quorum", quorum)
  private var resultTable: Option[HTable] = None
  private var imageTable: Option[HTable] = None
  val rowKeyDistributerResults = RowKeyDistributerManager.getDistributer(tableNameResults)
  val rowKeyDistributerImages = RowKeyDistributerManager.getDistributer(tableNameImages)

  /**
   * Start the Hbase connection
   */
  def start() {
    val adminHBase = new HBaseAdmin(config)

    try {
      if (resultTable == None) {
        if (!adminHBase.tableExists(tableNameResults)) {
          log.info("Table Resulttable does not exists")
          val tableName = TableName.valueOf(tableNameResults)
          val desc = new HTableDescriptor(tableName)
          desc.addFamily(new HColumnDescriptor(columnFamily))
          try {
            adminHBase.createTable(desc)
            log.info("Created Resulttable")
          } catch {
            case ex: TableExistsException ⇒ {
            } //someone beat use in creating the table, which isn't a problem
            case ex: Exception ⇒ {
              log.error("Failed to create table " + tableNameResults, ex)
            }
          }
        }
        resultTable = Some(new HTable(config, tableNameResults))
      }
      if (imageTable == None) {
        if (!adminHBase.tableExists(tableNameImages)) {
          log.info("Table imageTable does not exists")
          val tName = TableName.valueOf(tableNameImages)
          val desc = new HTableDescriptor(tName)
          val meta = new HColumnDescriptor(columnFamily)
          desc.addFamily(meta)
          try {
            adminHBase.createTable(desc)
            log.info("Created imageTable")
          } catch {
            case ex: TableExistsException ⇒ {
            } //someone beat use in creating the table, which isn't a problem
            case ex: Exception ⇒ {
              log.error("Failed to create table " + tableNameResults, ex)
            }
          }
        }
        imageTable = Some(new HTable(config, tableNameImages))
      }
    } finally {
      adminHBase.close()
    }
  }

  /**
   * Stop the hbase connection
   */
  def stop() {
    resultTable.foreach { table ⇒
      table.synchronized {
        try {
          table.flushCommits()
          table.close()
        } finally {
          resultTable = None
        }
      }
    }

    imageTable.foreach { table ⇒
      table.synchronized {
        try {
          table.flushCommits()
          table.close()
        } finally {
          imageTable = None
        }
      }
    }
  }

  /**
   * Create the key used for storing the self test result
   * @param systemId the system Id
   * @param time the time of the self test
   * @return the result key
   */
  private def createResultKey(systemId: String, time: Date, corridorId: String): Array[Byte] = {
    val orig = Bytes.toBytes("%s-%s-%s".format(systemId, corridorId, time.getTime.toString))
    rowKeyDistributerResults.getDistributedKey(orig)
  }

  /**
   * Store a self test result into hbase
   * @param systemId the systemId
   * @param nrCameras the number of cameras tested
   * @param time the time of the self test
   * @param images the list of images
   * @param errors the list of errors
   */
  def store(systemId: String, corridorId: String, nrCameras: Int, time: Date, images: Seq[SelftestImage], errors: Seq[String],
            reportingOfficerCode: String, serialNr: String) {
    //store all images
    val listImageKeys = new ListBuffer[KeyWithTimeAndId]
    imageTable.foreach(table ⇒ {
      table.synchronized {
        for (image ← images) {
          val keyOrg = new KeyWithTimeAndId(time.getTime, "%s-%s".format(corridorId, image.id))
          val key = rowKeyDistributerImages.getDistributedKey(keyOrg.createKey())
          val put = new Put(key)
          put.add(columnFamily, attributeImage, time.getTime, image.image)
          table.put(put)
          listImageKeys += keyOrg
        }
        table.flushCommits()
      }
    })

    //store result
    resultTable.foreach(table ⇒ {
      val key = createResultKey(systemId, time, corridorId)
      val result = SelfTestResult(
        systemId = systemId,
        corridorId = corridorId,
        reportingOfficerCode = reportingOfficerCode,
        nrCamerasTested = nrCameras,
        time = time.getTime,
        success = errors.isEmpty,
        images = listImageKeys.toSeq,
        errors = errors,
        serialNr = serialNr)
      val json = Serialization.write(result)(DefaultFormats)
      table.synchronized {
        val put = new Put(key)
        put.add(columnFamily, attributeResult, time.getTime, json.getBytes)
        table.put(put)
        table.flushCommits()
      }
    })
  }
}
