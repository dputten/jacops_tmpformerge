/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.calibrationserver.calibration

import csc.akka.logging.DirectLogging
import csc.curator.utils.Curator

/**
 * Interface for indicating that the selftest is finished. Used to be able to test without zookeeper
 */
trait RegisterSelfTestTrigger {
  def processedTrigger()
}

/**
 * The zookeeper implementation of the RegisterSelfTestTrigger.
 * When the selftest trigger is processed it removes the node.
 * @param client the zookeeper curator
 * @param triggerNodePath the path of the trigger node
 */
class ZookeeperRegisterSelfTestTrigger(client: Curator, triggerNodePath: String) extends RegisterSelfTestTrigger with DirectLogging {
  def processedTrigger() {
    log.info("Deleting self test trigger in zookeeper with path %s".format(triggerNodePath))
    if (client.exists(triggerNodePath)) {
      client.delete(triggerNodePath)
    } else {
      log.info("Trigger node path %s does not exist".format(triggerNodePath))
    }
  }
}