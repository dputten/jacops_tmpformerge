/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.calibrationserver.calibration

import com.sematext.hbase.wd.RowKeyDistributorByHashPrefix.OneByteSimpleHash
import com.sematext.hbase.wd.{ AbstractRowKeyDistributor, RowKeyDistributorByHashPrefix }
import csc.curator.utils.Curator

class NoDistributionOfRowKey extends AbstractRowKeyDistributor {
  def getDistributedKey(p1: Array[Byte]) = p1

  def getOriginalKey(p1: Array[Byte]) = p1

  def getAllDistributedKeys(p1: Array[Byte]) = Array(p1)

  def getParamsToStore = ""

  def init(p1: String) {}
}

object RowKeyDistributerManager {
  val defaultNrBuckets = 10
  private var listConfiguration = Map[String, Int]()
  private var listRowKeys = Map[String, AbstractRowKeyDistributor]()

  def init(curator: Curator) {
    if (listRowKeys.isEmpty) {
      val path = "/ctes/configuration/hbaseRowKeys"
      listConfiguration = curator.get[Map[String, Int]](path).getOrElse(Map())
    }
  }

  def getDistributer(tableName: String): AbstractRowKeyDistributor = {
    listRowKeys.get(tableName).getOrElse {
      val nrBuckets = listConfiguration.get(tableName).getOrElse(defaultNrBuckets)
      val dist = if (nrBuckets <= 0) {
        new NoDistributionOfRowKey()
      } else {
        new RowKeyDistributorByHashPrefix(new OneByteSimpleHash(nrBuckets))
      }
      listRowKeys += tableName -> dist
      dist
    }
  }
  def clear() {
    listConfiguration = Map()
    listRowKeys = Map()
  }
}