/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.calibrationserver.calibration

import java.util.Date
import java.util.concurrent.atomic.AtomicReference

import akka.actor.ActorRef
import csc.akka.logging.DirectLogging
import csc.curator.utils.Curator
import csc.gantry.config.{ CorridorService, TriggerData, ZookeeperUser }
import org.apache.curator.framework.CuratorFramework
import org.apache.curator.framework.recipes.cache.{ PathChildrenCache, PathChildrenCacheEvent, PathChildrenCacheListener }

/**
 * This class watch the zookeeper trigger Node. When created it sends the trigger to the SelftestTrigger
 * @param client the zookeeper curator
 * @param systemId The system iD
 * @param selfTestActor the selftest actor
 */
class WatchSelfTestTrigger(client: Curator, systemId: String, corridorId: String, selfTestActor: ActorRef, userPath: String) extends PathChildrenCacheListener with DirectLogging {
  private val parentPath = CorridorService.getCorridorCalibrationPath(systemId, corridorId)
  private val selftestPath = CorridorService.getCorridorCalibrationTriggerPath(systemId, corridorId)
  private val cache = new AtomicReference[Option[PathChildrenCache]](None)

  val userParentPath = if (userPath.charAt(userPath.length() - 1) == '/') {
    userPath
  } else {
    userPath + "/"
  }

  /**
   * Starts the watch for changes on the self test Trigger
   */
  def start() {
    if (client.curator.isEmpty) {
      throw new IllegalStateException("Curator has an empty CuratorFramework")
    }
    if (cache.compareAndSet(None, Some(new PathChildrenCache(client.curator.get, parentPath, false)))) {
      cache.get.foreach(pathCache ⇒ {
        pathCache.start()
        //add listener
        pathCache.getListenable().addListener(this)
        val triggerData = getTriggerData(pathCache)
        if (!triggerData.processed) {
          // First delete the triggerdata and the add it again so the 'childEvent' will fire.
          CorridorService.deleteClassificationTriggerData(client, systemId, corridorId)
          CorridorService.putClassificationTriggerData(client, systemId, corridorId, triggerData)

          log.info("Added TriggerData for [%s] again in start.".format(systemId))
          //selfTestActor ! new StartCalibration(new Date(), triggerData.userId, triggerData.reportingOfficerCode, triggerData.reason)
        }
        log.info("WatchSelfTestTrigger started " + systemId)
      })
    }

    Thread.sleep(5000)
  }

  def midnightTrigger(time: Date) {
    //check state
    log.info("MidnightTrigger [%s] check state.".format(systemId))
    try {
      CorridorService.getCorridorState(client, systemId, corridorId) match {
        case Some(stateData) ⇒ {
          if (stateData.state.startsWith("Enforce")) {
            getUserData(stateData.userId) match {
              case Some(user) ⇒ {
                selfTestActor ! new StartCalibration(time, user.id, user.reportingOfficerId, "MIDNIGHT", systemId, corridorId)
                log.info("MidnightTrigger [%s] created trigger".format(systemId))
              }
              case None ⇒ log.error("Could not find reportingOfficerId for user %s".format(stateData.userId))
            }

          } else {
            log.info("MidnightTrigger [%s] skipping selftest state=%s".format(systemId, stateData.state))
          }
        }
        case None ⇒ log.warning("Could not find state") //done
      }
    } catch {
      case t: Throwable ⇒ log.error("Could not create MidnightTrigger [%s]: %s".format(systemId, t.getMessage)) //done
    }
  }

  /**
   * Stops the watch for changes on the self test Trigger
   */
  def stop() {
    cache.getAndSet(None).foreach(pathCache ⇒ {
      pathCache.getListenable().removeListener(this)
      pathCache.close()
      log.info("WatchSelfTestTrigger stopped " + systemId)
    })
  }

  /**
   * One of the children has been changed
   * @param client the CuratorFramework
   * @param event the indication what has changed
   */
  def childEvent(client: CuratorFramework, event: PathChildrenCacheEvent) {
    if (event == null) {
      log.warning("WatchSelfTestTrigger[%s] event is null".format(systemId))
    } else if (event.getData == null) {
      log.warning("WatchSelfTestTrigger[%s] event.getData is null".format(systemId))
    } else {
      log.info("WatchSelfTestTrigger[%s]".format(systemId))
      if (event.getData.getPath == selftestPath.toString() && event.getType == PathChildrenCacheEvent.Type.CHILD_ADDED) {
        cache.get.foreach(pathCache ⇒ {
          val data = getTriggerData(pathCache)
          if (!data.processed) {
            log.info("sending StartCalibration[%s] in childEvent".format(systemId))
            selfTestActor ! new StartCalibration(new Date(), data.userId, data.reportingOfficerCode, data.reason, systemId, corridorId)
          }
        })
      }
    }
  }

  /**
   * Get the data from zookeeper.
   * Using the CuratorFramework, because couldn't get it to work using the cache
   * @param pathCache
   * @return
   */
  private def getTriggerData(pathCache: PathChildrenCache): TriggerData = {
    val getData = CorridorService.getClassificationTriggerData(client, systemId, corridorId)
    getData.getOrElse(TriggerData(0, "", "", true, ""))
  }
  /**
   * Get the data from zookeeper.
   * Using the CuratorFramework, because couldn't get it to work using the cache
   * @return
   */
  private def getUserData(Id: String): Option[ZookeeperUser] = {
    client.get[ZookeeperUser](userParentPath + Id)
  }
}

case class StartCalibration(time: Date, userId: String, reportingOfficerCode: String, reason: String, systemId: String, corridorId: String) {
  def isMidNight: Boolean = {
    reason == "MIDNIGHT"
  }
}
