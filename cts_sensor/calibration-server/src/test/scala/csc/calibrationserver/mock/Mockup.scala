/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.calibrationserver.mock

import java.util.concurrent.{ CyclicBarrier, TimeUnit }
import java.util.Date

import csc.calibrationserver.calibration.{ RegisterSelfTestTrigger, SelfTestStore, SelftestImage }

class RegisterSelfTestTriggerMock(barrier: CyclicBarrier) extends RegisterSelfTestTrigger {
  private var nrCalled = 0
  def processedTrigger() {
    nrCalled += 1
    barrier.await(30, TimeUnit.SECONDS)
  }
  def getNrCalled(): Int = {
    nrCalled
  }
}

class SelfTestStoreMock(barrier: CyclicBarrier) extends SelfTestStore {
  private var nrCalled = 0

  def store(systemId: String, corridorId: String, nrCameras: Int, time: Date, images: Seq[SelftestImage],
            errors: Seq[String], reportingOfficerCode: String, serialNr: String) {
    nrCalled += 1
    barrier.await(30, TimeUnit.SECONDS)
  }

  def stop() {}

  def start() {}

  def getNrCalled(): Int = {
    nrCalled
  }
}

class SelfTestStoreMockEmpty() extends SelfTestStore {
  private var nrCalled = 0

  def store(systemId: String, corridorId: String, nrCameras: Int, time: Date, images: Seq[SelftestImage],
            errors: Seq[String], reportingOfficerCode: String, serialNr: String) {
    nrCalled += 1
  }

  def stop() {}

  def start() {}

  def getNrCalled(): Int = {
    nrCalled
  }
}
