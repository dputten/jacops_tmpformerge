package csc.calibrationserver.mock

import csc.gantry.config._
import csc.gantry.config.CorridorsConfig
import csc.gantry.config.RadarConfig
import scala.Some
import csc.gantry.config.PIRConfig
import csc.gantry.config.CorridorConfig
import csc.gantry.config.PirRadarCameraSensorConfigImpl
import csc.vehicle.message.Lane
import csc.gantry.config.CameraConfig
import csc.curator.utils.Curator

class LaneConfigurationMock extends DefaultLaneConfiguration {
  val corridorId = "Cor1"
  val lane1 = new Lane(laneId = "A2-40HM-Lane1",
    name = "Lane1",
    gantry = "40HMA",
    system = "A2",
    sensorGPS_longitude = 1,
    sensorGPS_latitude = 1)
  val lane2 = new Lane(laneId = "A2-40HM-Lane2",
    name = "Lane2",
    gantry = "40HMB",
    system = "A2",
    sensorGPS_longitude = 21,
    sensorGPS_latitude = 2)
  val camera = new CameraConfig(relayURI = "Dummy", cameraHost = "localhost", cameraPort = 5000, maxTriggerRetries = 5, timeDisconnected = 1000)
  val radar = new RadarConfig(radarURI = "dymmy",
    measurementAngle = 33.5,
    height = 5.3)
  val pir = new PIRConfig(pirHost = "Dummy", pirPort = 6000, pirAddress = 1, refreshPeriod = 1000, vrHost = "Dummy", vrPort = 6001)
  val cfg = new PirRadarCameraSensorConfigImpl(
    lane = lane1,
    camera = camera,
    imageMask = None,
    recognizeOptions = Map(),
    radar = radar,
    pir = pir)

  private val _lanes = List(cfg)

  override def lanes: List[LaneConfig] = _lanes

  override def getCorridorsConfig(system: String): Option[CorridorsConfig] = {
    Some(CorridorsConfig(List(CorridorConfig(id = corridorId, name = corridorId, serviceType = "SectionControl",
      info = ZkCorridorInfo(corridorId = 2,
        locationCode = "2",
        reportId = "",
        reportHtId = "",
        reportPerformance = true,
        locationLine1 = "")))))
  }

}
