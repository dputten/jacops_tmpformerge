package csc.calibrationserver.mock

import csc.gantry.config.CertificateValidator
import csc.curator.utils.Curator

class CertificateValidatorMock extends CertificateValidator {
  override def validateCertificatesForSystemId(systemId: String, currentDate: Long): List[String] = { Nil }
}
