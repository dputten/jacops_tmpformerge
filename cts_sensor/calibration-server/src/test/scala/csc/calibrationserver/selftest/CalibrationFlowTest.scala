/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.calibrationserver.selftest

import java.util.Date

import scala.concurrent.duration._
import csc.calibration.{ GantryCalibrationResponse, StepResult }
import csc.calibrationserver.calibration.StartCalibration
import org.scalatest.WordSpec
import org.scalatest._

class CalibrationFlowTest extends WordSpec with MustMatchers {
  val now = System.currentTimeMillis()
  "CalibrationFlowTest" must {
    "add multiple request" in {
      val flow = new CalibrationFlow(0.minutes)
      val req1 = StartCalibration(time = new Date(now), userId = "user1", reportingOfficerCode = "AA1234", reason = "StandBy", systemId = "A4", corridorId = "S1")
      flow.addNewCalibrationRequest(req1, Seq())
      val req2 = StartCalibration(time = new Date(now + 2.minute.toMillis), userId = "user2", reportingOfficerCode = "BB1234", reason = "StandBy", systemId = "A4", corridorId = "S1")
      flow.addNewCalibrationRequest(req2, Seq())

      val stats = flow.popTimeOutCalibrations(now + 3.minutes.toMillis)
      stats.size must be(2)
      stats.head.start must be(req1)
      stats.last.start must be(req2)
    }
    "timeout must return only the old request" in {
      val flow = new CalibrationFlow(0.minutes)
      val req1 = StartCalibration(time = new Date(now), userId = "user1", reportingOfficerCode = "AA1234", reason = "StandBy", systemId = "A4", corridorId = "S1")
      flow.addNewCalibrationRequest(req1, Seq())
      val req2 = StartCalibration(time = new Date(now + 2.minute.toMillis), userId = "user2", reportingOfficerCode = "BB1234", reason = "StandBy", systemId = "A4", corridorId = "S1")
      flow.addNewCalibrationRequest(req2, Seq())

      val stats = flow.popTimeOutCalibrations(now + 2.minutes.toMillis - 1)
      stats.size must be(1)
      stats.head.start must be(req1)
    }
    "filter request with same time" in {
      val flow = new CalibrationFlow(0.minutes)
      val req1 = StartCalibration(time = new Date(now), userId = "user1", reportingOfficerCode = "AA1234", reason = "StandBy", systemId = "A4", corridorId = "S1")
      flow.addNewCalibrationRequest(req1, Seq())
      val req2 = StartCalibration(time = new Date(now), userId = "user2", reportingOfficerCode = "BB1234", reason = "StandBy", systemId = "A4", corridorId = "S1")
      flow.addNewCalibrationRequest(req2, Seq())

      val stats = flow.popTimeOutCalibrations(now + 1)
      stats.size must be(1)
      stats.head.start must be(req1)
    }
    "filter request with earlier time" in {
      val flow = new CalibrationFlow(0.minutes)
      val req1 = StartCalibration(time = new Date(now), userId = "user1", reportingOfficerCode = "AA1234", reason = "StandBy", systemId = "A4", corridorId = "S1")
      flow.addNewCalibrationRequest(req1, Seq())
      val req2 = StartCalibration(time = new Date(now + 1 - 1.minute.toMillis), userId = "user2", reportingOfficerCode = "BB1234", reason = "StandBy", systemId = "A4", corridorId = "S1")
      flow.addNewCalibrationRequest(req2, Seq())

      val stats = flow.popTimeOutCalibrations(now + 1)
      stats.size must be(1)
      stats.head.start must be(req1)
    }
    "filter request with later time" in {
      val flow = new CalibrationFlow(0.minutes)
      val req1 = StartCalibration(time = new Date(now), userId = "user1", reportingOfficerCode = "AA1234", reason = "StandBy", systemId = "A4", corridorId = "S1")
      flow.addNewCalibrationRequest(req1, Seq())
      val req2 = StartCalibration(time = new Date(now + 1.minute.toMillis - 1), userId = "user2", reportingOfficerCode = "BB1234", reason = "StandBy", systemId = "A4", corridorId = "S1")
      flow.addNewCalibrationRequest(req2, Seq())

      val stats = flow.popTimeOutCalibrations(now + 2.minutes.toMillis)
      stats.size must be(1)
      stats.head.start must be(req1)
    }
    "Update one request with successful response" in {
      val flow = new CalibrationFlow(0.minutes)
      val req1 = StartCalibration(time = new Date(now), userId = "user1", reportingOfficerCode = "AA1234", reason = "StandBy", systemId = "A4", corridorId = "S1")
      flow.addNewCalibrationRequest(req1, Seq("E1", "X1"))
      val response = GantryCalibrationResponse("A4", "E1", req1.time.getTime, now, Seq(), Seq(StepResult("step1", true, None)))
      flow.updateStateWithResponse(response) must be(true)

      val stats = flow.popTimeOutCalibrations(now + 2.minutes.toMillis)
      stats.size must be(1)
      stats.head.start must be(req1)
      val gantries = stats.head.gantries
      gantries.size must be(2)
      gantries.head.response must be(Right(response))
      gantries.last.response.isLeft must be(true)
    }
    "Update one request with failed response" in {
      val flow = new CalibrationFlow(0.minutes)
      val req1 = StartCalibration(time = new Date(now), userId = "user1", reportingOfficerCode = "AA1234", reason = "StandBy", systemId = "A4", corridorId = "S1")
      flow.addNewCalibrationRequest(req1, Seq("E1", "X1"))
      val response = GantryCalibrationResponse("A4", "E1", req1.time.getTime, now, Seq(), Seq(StepResult("step1", false, Some("failed"))))
      flow.updateStateWithResponse(response) must be(true)

      val stats = flow.popTimeOutCalibrations(now + 2.minutes.toMillis)
      stats.size must be(1)
      stats.head.start must be(req1)
      val gantries = stats.head.gantries
      gantries.size must be(2)
      gantries.head.response must be(Left("step1: failed"))
      gantries.last.response.isLeft must be(true)
    }
    "Update multiple requests" in {
      val flow = new CalibrationFlow(0.minutes)
      val req1 = StartCalibration(time = new Date(now), userId = "user1", reportingOfficerCode = "AA1234", reason = "StandBy", systemId = "A4", corridorId = "S1")
      flow.addNewCalibrationRequest(req1, Seq("E1", "X1"))
      val req2 = StartCalibration(time = new Date(now + 2.minute.toMillis), userId = "user2", reportingOfficerCode = "BB1234", reason = "StandBy", systemId = "A4", corridorId = "S1")
      flow.addNewCalibrationRequest(req2, Seq("E1", "X1"))

      val response = GantryCalibrationResponse("A4", "E1", req1.time.getTime, req2.time.getTime, Seq(), Seq(StepResult("step1", true, None)))
      flow.updateStateWithResponse(response) must be(true)

      val stats = flow.popTimeOutCalibrations(now + 2.minutes.toMillis)
      stats.size must be(2)
      stats.head.start must be(req1)
      var gantries = stats.head.gantries
      gantries.size must be(2)
      gantries.head.response must be(Right(response))
      gantries.last.response.isLeft must be(true)

      gantries = stats.last.gantries
      gantries.size must be(2)
      gantries.head.response must be(Right(response))
      gantries.last.response.isLeft must be(true)
    }
    "Not update when old response" in {
      val flow = new CalibrationFlow(0.minutes)
      val req1 = StartCalibration(time = new Date(now), userId = "user1", reportingOfficerCode = "AA1234", reason = "StandBy", systemId = "A4", corridorId = "S1")
      flow.addNewCalibrationRequest(req1, Seq("E1", "X1"))
      val req2 = StartCalibration(time = new Date(now + 2.minute.toMillis), userId = "user2", reportingOfficerCode = "BB1234", reason = "StandBy", systemId = "A4", corridorId = "S1")
      flow.addNewCalibrationRequest(req2, Seq("E1", "X1"))

      val response = GantryCalibrationResponse("A4", "E1", req1.time.getTime, req1.time.getTime, Seq(), Seq(StepResult("step1", true, None)))
      flow.updateStateWithResponse(response) must be(true)

      val stats = flow.popTimeOutCalibrations(now + 2.minutes.toMillis)
      stats.size must be(2)
      stats.head.start must be(req1)
      var gantries = stats.head.gantries
      gantries.size must be(2)
      gantries.head.response must be(Right(response))
      gantries.last.response.isLeft must be(true)

      gantries = stats.last.gantries
      gantries.size must be(2)
      gantries.head.response.isLeft must be(true)
      gantries.last.response.isLeft must be(true)
    }
    "Not update when not existing gantry" in {
      val flow = new CalibrationFlow(0.minutes)
      val req1 = StartCalibration(time = new Date(now), userId = "user1", reportingOfficerCode = "AA1234", reason = "StandBy", systemId = "A4", corridorId = "S1")
      flow.addNewCalibrationRequest(req1, Seq("E1", "X1"))

      val response = GantryCalibrationResponse("A4", "E3", req1.time.getTime, req1.time.getTime, Seq(), Seq(StepResult("step1", true, None)))
      flow.updateStateWithResponse(response) must be(false)

      val stats = flow.popTimeOutCalibrations(now + 2.minutes.toMillis)
      stats.size must be(1)
      stats.head.start must be(req1)
      var gantries = stats.head.gantries
      gantries.size must be(2)
      gantries.head.response.isLeft must be(true)
      gantries.last.response.isLeft must be(true)
    }
    "return finished states" in {
      val flow = new CalibrationFlow(0.minutes)
      val req1 = StartCalibration(time = new Date(now), userId = "user1", reportingOfficerCode = "AA1234", reason = "StandBy", systemId = "A4", corridorId = "S1")
      flow.addNewCalibrationRequest(req1, Seq("E1", "X1"))
      val response = GantryCalibrationResponse("A4", "E1", req1.time.getTime, req1.time.getTime, Seq(), Seq(StepResult("step1", true, None)))
      flow.updateStateWithResponse(response) must be(true)
      val responseExit = GantryCalibrationResponse("A4", "X1", req1.time.getTime, req1.time.getTime, Seq(), Seq(StepResult("step1", true, None)))
      flow.updateStateWithResponse(responseExit) must be(true)

      val stats = flow.popFinishedCalibrations()

      stats.size must be(1)
      stats.head.start must be(req1)
      var gantries = stats.head.gantries
      gantries.size must be(2)
      gantries.head.response must be(Right(response))
      gantries.last.response must be(Right(responseExit))
    }
    "return multiple finished states" in {
      val flow = new CalibrationFlow(10.minutes)
      val req1 = StartCalibration(time = new Date(now), userId = "user1", reportingOfficerCode = "AA1234", reason = "StandBy", systemId = "A4", corridorId = "S1")
      flow.addNewCalibrationRequest(req1, Seq("E1", "X1"))

      val req2 = StartCalibration(time = new Date(now + 2.minutes.toMillis), userId = "user2", reportingOfficerCode = "BB1234", reason = "StandBy", systemId = "A4", corridorId = "S1")
      flow.addNewCalibrationRequest(req2, Seq("E1", "X1"))

      val response = GantryCalibrationResponse("A4", "E1", req1.time.getTime, req1.time.getTime, Seq(), Seq(StepResult("step1", true, None)))
      flow.updateStateWithResponse(response) must be(true)
      val responseExit = GantryCalibrationResponse("A4", "X1", req1.time.getTime, req1.time.getTime, Seq(), Seq(StepResult("step1", true, None)))
      flow.updateStateWithResponse(responseExit) must be(true)

      val stats = flow.popFinishedCalibrations()

      stats.size must be(2)
      stats.head.start must be(req1)
      var gantries = stats.head.gantries
      gantries.size must be(2)
      gantries.head.response must be(Right(response))
      gantries.last.response must be(Right(responseExit))
      stats.last.start must be(req2)
      gantries = stats.last.gantries
      gantries.size must be(2)
      gantries.head.response must be(Right(response))
      gantries.last.response must be(Right(responseExit))
    }
    "return nothing when there are no finished states" in {
      val flow = new CalibrationFlow(0.minutes)
      val req1 = StartCalibration(time = new Date(now), userId = "user1", reportingOfficerCode = "AA1234", reason = "StandBy", systemId = "A4", corridorId = "S1")
      flow.addNewCalibrationRequest(req1, Seq("E1", "X1"))

      flow.popFinishedCalibrations() must be(Seq())
      val response = GantryCalibrationResponse("A4", "E1", req1.time.getTime, req1.time.getTime, Seq(), Seq(StepResult("step1", true, None)))
      flow.updateStateWithResponse(response) must be(true)
      flow.popFinishedCalibrations() must be(Seq())
    }
    "correctly report waiting for gantry when no response has received" in {
      val flow = new CalibrationFlow(10.minutes)
      val req1 = StartCalibration(time = new Date(now), userId = "user1", reportingOfficerCode = "AA1234", reason = "StandBy", systemId = "A4", corridorId = "S1")
      flow.addNewCalibrationRequest(req1, Seq("E1", "X1"))
      flow.waitForResponse("E1") must be(true)
    }
    "correctly report waiting for gantry when response has received" in {
      val flow = new CalibrationFlow(10.minutes)
      val req1 = StartCalibration(time = new Date(now), userId = "user1", reportingOfficerCode = "AA1234", reason = "StandBy", systemId = "A4", corridorId = "S1")
      flow.addNewCalibrationRequest(req1, Seq("E1", "X1"))

      val response = GantryCalibrationResponse("A4", "E1", req1.time.getTime, req1.time.getTime, Seq(), Seq(StepResult("step1", true, None)))
      flow.updateStateWithResponse(response) must be(true)

      flow.waitForResponse("E1") must be(false)
    }
    "correctly report waiting for gantry with multiple requests when response has received" in {
      val flow = new CalibrationFlow(10.minutes)
      val req1 = StartCalibration(time = new Date(now), userId = "user1", reportingOfficerCode = "AA1234", reason = "StandBy", systemId = "A4", corridorId = "S1")
      flow.addNewCalibrationRequest(req1, Seq("E1", "X1"))

      val req2 = StartCalibration(time = new Date(now + 2.minutes.toMillis), userId = "user2", reportingOfficerCode = "BB1234", reason = "StandBy", systemId = "A4", corridorId = "S1")
      flow.addNewCalibrationRequest(req2, Seq("E1", "X1"))

      val response = GantryCalibrationResponse("A4", "E1", req1.time.getTime, req1.time.getTime, Seq(), Seq(StepResult("step1", true, None)))
      flow.updateStateWithResponse(response) must be(true)

      flow.waitForResponse("E1") must be(false)
    }
    "correctly report waiting for gantry with multiple requests when no response has received" in {
      val flow = new CalibrationFlow(0.minutes)
      val req1 = StartCalibration(time = new Date(now), userId = "user1", reportingOfficerCode = "AA1234", reason = "StandBy", systemId = "A4", corridorId = "S1")
      flow.addNewCalibrationRequest(req1, Seq("E1", "X1"))

      val req2 = StartCalibration(time = new Date(now + 2.minutes.toMillis), userId = "user2", reportingOfficerCode = "BB1234", reason = "StandBy", systemId = "A4", corridorId = "S1")
      flow.addNewCalibrationRequest(req2, Seq("E1", "X1"))

      flow.waitForResponse("E1") must be(true)
    }
    "correctly report waiting for gantry with multiple requests when some response has received" in {
      val flow = new CalibrationFlow(0.minutes)
      val req1 = StartCalibration(time = new Date(now), userId = "user1", reportingOfficerCode = "AA1234", reason = "StandBy", systemId = "A4", corridorId = "S1")
      flow.addNewCalibrationRequest(req1, Seq("E1", "X1"))

      val req2 = StartCalibration(time = new Date(now + 2.minutes.toMillis), userId = "user2", reportingOfficerCode = "BB1234", reason = "StandBy", systemId = "A4", corridorId = "S1")
      flow.addNewCalibrationRequest(req2, Seq("E1", "X1"))

      val response = GantryCalibrationResponse("A4", "E1", req1.time.getTime, req1.time.getTime, Seq(), Seq(StepResult("step1", true, None)))
      flow.updateStateWithResponse(response) must be(true)

      flow.waitForResponse("E1") must be(true)
    }
  }

}