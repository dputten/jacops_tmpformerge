/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.calibrationserver.selftest

import java.util.Date
import java.util.concurrent.{ CyclicBarrier, TimeUnit }

import akka.actor.{ ActorSystem, Props }
import akka.testkit.{ TestKit, TestProbe }

import scala.concurrent.duration._
import csc.akka.logging.DirectLogging
import csc.calibration._
import csc.calibrationserver.calibration.StartCalibration
import csc.calibrationserver.mock._
import csc.curator.utils.xml.{ CuratorToolsImplXml, XmlReader }
import csc.gantry.config._
import csc.json.lift.EnumerationSerializer
import csc.vehicle.message.VehicleImageType
import net.liftweb.json.DefaultFormats
import org.apache.commons.codec.digest.DigestUtils
import org.scalatest._
import testframe.Resources

class CorridorCalibrationActorTest extends TestKit(ActorSystem("CorridorCalibrationActorTest")) with WordSpecLike with MustMatchers with BeforeAndAfterAll with DirectLogging {
  val formats = DefaultFormats + new EnumerationSerializer(VehicleImageType, ServiceType, ZkCaseFileType)

  override def afterAll() {
    super.afterAll()
    system.shutdown
  }

  val startTime = System.currentTimeMillis()

  "CorridorCalibrationActor" must {
    "pass happy flow- receive request send gantry requests wait for response and create calibration result" in {
      val zookeeperDataFile = Resources.getResourcePath("zookeeper_basic.xml")
      val curator = new CuratorToolsImplXml(new XmlReader(zookeeperDataFile, log), log, formats)

      val producer = TestProbe()
      val triggerBar = new CyclicBarrier(2)
      val storeBar = new CyclicBarrier(2)
      val config = new ZookeeperLaneConfiguration(curator, "/ctes/systems", None)

      val actor = system.actorOf(Props(new CorridorCalibrationActor(
        curator = curator,
        systemId = "N62",
        corridorId = "S1",
        store = new SelfTestStoreMock(storeBar),
        triggerRegister = new RegisterSelfTestTriggerMock(triggerBar),
        laneConfig = config,
        serialNr = (systemId: String) ⇒ systemId + "-123",
        calibrationProducer = producer.ref,
        certificateValidator = new CertificateValidatorMock,
        responseTooOldAfter = 0.millis,
        gantryRetryDuration = 500.millis,
        responseTimeoutDuration = 2.seconds,
        calibrationTimeOut = 1.hour)))
      //send calibration request
      actor ! StartCalibration(new Date(startTime), "userID", "AA1234", "Reason", "A2", "S1")
      //expect the gantry requests
      val req1 = producer.expectMsgType[GantryCalibrationRequest](2000.millis)
      val req2 = producer.expectMsgType[GantryCalibrationRequest](2000.millis)
      req1.laneIds must be(List("N62-E1-E1R1"))
      req1.gantryId must be("E1")
      req2.laneIds must be(List("N62-X1-X1R1"))
      req2.gantryId must be("X1")
      val sendGantryTime = startTime + 1.second.toMillis
      //send gantry responses
      val image1 = "image1".getBytes
      val checksum = DigestUtils.md5Hex(image1)
      val imageList1 = Seq(CameraImage(name = "E1R1", imageId = req1.laneIds.head, checksum = checksum, image = image1.map(_.toInt)))
      actor ! GantryCalibrationResponse(req1.systemId, req1.gantryId, req1.time, sendGantryTime, imageList1, Seq(StepResult("Camera", true, None)))

      val image2 = "image2".getBytes
      val checksum2 = DigestUtils.md5Hex(image2)
      val imageList2 = Seq(CameraImage(name = "E1R1", imageId = req1.laneIds.head, checksum = checksum2, image = image2.map(_.toInt)))
      actor ! GantryCalibrationResponse(req2.systemId, req2.gantryId, req2.time, sendGantryTime, imageList2, Seq(StepResult("Camera", true, None)))
      //Expect Calibration response
      storeBar.await(2, TimeUnit.SECONDS)
      triggerBar.await(1, TimeUnit.SECONDS)
      system.stop(actor)
    }
    "retrigger gantryRequests when no response is received" in {
      val zookeeperDataFile = Resources.getResourcePath("zookeeper_basic.xml")
      val curator = new CuratorToolsImplXml(new XmlReader(zookeeperDataFile, log), log, formats)

      val producer = TestProbe()
      val triggerBar = new CyclicBarrier(2)
      val storeBar = new CyclicBarrier(2)
      val config = new ZookeeperLaneConfiguration(curator, "/ctes/systems", None)

      val actor = system.actorOf(Props(new CorridorCalibrationActor(
        curator = curator,
        systemId = "N62",
        corridorId = "S1",
        store = new SelfTestStoreMock(storeBar),
        triggerRegister = new RegisterSelfTestTriggerMock(triggerBar),
        laneConfig = config,
        serialNr = (systemId: String) ⇒ systemId + "-123",
        calibrationProducer = producer.ref,
        certificateValidator = new CertificateValidatorMock,
        responseTooOldAfter = 0.millis,
        gantryRetryDuration = 100.millis, //fast retry
        responseTimeoutDuration = 500.millis, //small timeout
        calibrationTimeOut = 1.hour)))
      //send calibration request
      actor ! StartCalibration(new Date(startTime), "userID", "AA1234", "Reason", "A2", "S1")
      //expect the gantry requests
      val req1 = producer.expectMsgType[GantryCalibrationRequest](2000.millis)
      val req2 = producer.expectMsgType[GantryCalibrationRequest](2000.millis)
      req1.laneIds must be(List("N62-E1-E1R1"))
      req1.gantryId must be("E1")
      req2.laneIds must be(List("N62-X1-X1R1"))
      req2.gantryId must be("X1")
      val sendGantryTime = startTime + 1.second.toMillis
      //send only one gantry responses
      val image1 = "image1".getBytes
      val checksum = DigestUtils.md5Hex(image1)
      val imageList1 = Seq(CameraImage(name = "E1R1", imageId = req1.laneIds.head, checksum = checksum, image = image1.map(_.toInt)))
      actor ! GantryCalibrationResponse(req1.systemId, req1.gantryId, req1.time, sendGantryTime, imageList1, Seq(StepResult("Camera", true, None)))

      val req2Resend = producer.expectMsgType[GantryCalibrationRequest](2000.millis)
      req2Resend.laneIds must be(List("N62-X1-X1R1"))
      req2Resend.gantryId must be("X1")

      system.stop(actor)
    }
    "retrigger gantryRequests when incorrect checksum is received" in {
      val zookeeperDataFile = Resources.getResourcePath("zookeeper_basic.xml")
      val curator = new CuratorToolsImplXml(new XmlReader(zookeeperDataFile, log), log, formats)

      val producer = TestProbe()
      val triggerBar = new CyclicBarrier(2)
      val storeBar = new CyclicBarrier(2)
      val config = new ZookeeperLaneConfiguration(curator, "/ctes/systems", None)

      val actor = system.actorOf(Props(new CorridorCalibrationActor(
        curator = curator,
        systemId = "N62",
        corridorId = "S1",
        store = new SelfTestStoreMock(storeBar),
        triggerRegister = new RegisterSelfTestTriggerMock(triggerBar),
        laneConfig = config,
        serialNr = (systemId: String) ⇒ systemId + "-123",
        calibrationProducer = producer.ref,
        certificateValidator = new CertificateValidatorMock,
        responseTooOldAfter = 0.millis,
        gantryRetryDuration = 5000.millis, //long delay should not be used in this situation
        responseTimeoutDuration = 2.seconds,
        calibrationTimeOut = 1.hour)))
      //send calibration request
      actor ! StartCalibration(new Date(startTime), "userID", "AA1234", "Reason", "A2", "S1")
      //expect the gantry requests
      val req1 = producer.expectMsgType[GantryCalibrationRequest](2000.millis)
      val req2 = producer.expectMsgType[GantryCalibrationRequest](2000.millis)
      req1.laneIds must be(List("N62-E1-E1R1"))
      req1.gantryId must be("E1")
      req2.laneIds must be(List("N62-X1-X1R1"))
      req2.gantryId must be("X1")
      val sendGantryTime = startTime + 1.second.toMillis
      //send gantry responses
      val image1 = "image1".getBytes
      //incorrect Checksum
      val checksum = "1234567890"
      val imageList1 = Seq(CameraImage(name = "E1R1", imageId = req1.laneIds.head, checksum = checksum, image = image1.map(_.toInt)))
      actor ! GantryCalibrationResponse(req1.systemId, req1.gantryId, req1.time, sendGantryTime, imageList1, Seq(StepResult("Camera", true, None)))

      //expect resend gantry request
      val req1Resend = producer.expectMsgType[GantryCalibrationRequest](2000.millis)
      req1Resend.laneIds must be(List("N62-E1-E1R1"))
      req1Resend.gantryId must be("E1")
      system.stop(actor)
    }
    "retrigger gantryRequests when an error response is received" in {
      val zookeeperDataFile = Resources.getResourcePath("zookeeper_basic.xml")
      val curator = new CuratorToolsImplXml(new XmlReader(zookeeperDataFile, log), log, formats)

      val producer = TestProbe()
      val triggerBar = new CyclicBarrier(2)
      val storeBar = new CyclicBarrier(2)
      val config = new ZookeeperLaneConfiguration(curator, "/ctes/systems", None)

      val actor = system.actorOf(Props(new CorridorCalibrationActor(
        curator = curator,
        systemId = "N62",
        corridorId = "S1",
        store = new SelfTestStoreMock(storeBar),
        triggerRegister = new RegisterSelfTestTriggerMock(triggerBar),
        laneConfig = config,
        serialNr = (systemId: String) ⇒ systemId + "-123",
        calibrationProducer = producer.ref,
        certificateValidator = new CertificateValidatorMock,
        responseTooOldAfter = 0.millis,
        gantryRetryDuration = 100.millis, //fast retry
        responseTimeoutDuration = 2.seconds,
        calibrationTimeOut = 1.hour)))
      //send calibration request
      actor ! StartCalibration(new Date(startTime), "userID", "AA1234", "Reason", "A2", "S1")
      //expect the gantry requests
      val req1 = producer.expectMsgType[GantryCalibrationRequest](2000.millis)
      val req2 = producer.expectMsgType[GantryCalibrationRequest](2000.millis)
      req1.laneIds must be(List("N62-E1-E1R1"))
      req1.gantryId must be("E1")
      req2.laneIds must be(List("N62-X1-X1R1"))
      req2.gantryId must be("X1")
      val sendGantryTime = startTime + 1.second.toMillis
      //send gantry responses
      val image1 = "image1".getBytes
      val checksum = DigestUtils.md5Hex(image1)
      val imageList1 = Seq(CameraImage(name = "E1R1", imageId = req1.laneIds.head, checksum = checksum, image = image1.map(_.toInt)))
      actor ! GantryCalibrationResponse(req1.systemId, req1.gantryId, req1.time, sendGantryTime, imageList1, Seq(StepResult("Camera", true, None)))

      val image2 = "image2".getBytes
      val checksum2 = DigestUtils.md5Hex(image2)
      val imageList2 = Seq(CameraImage(name = "E1R1", imageId = req1.laneIds.head, checksum = checksum2, image = image2.map(_.toInt)))
      actor ! GantryCalibrationResponse(req2.systemId, req2.gantryId, req2.time, sendGantryTime, imageList2, Seq(StepResult("Camera", false, Some("oeps"))))
      //expect resend of request 1
      val req2Resend = producer.expectMsgType[GantryCalibrationRequest](3000.millis)
      req2Resend.laneIds must be(List("N62-X1-X1R1"))
      req2Resend.gantryId must be("X1")

      system.stop(actor)
    }
    "send a response after the final timeout" in {
      val zookeeperDataFile = Resources.getResourcePath("zookeeper_basic.xml")
      val curator = new CuratorToolsImplXml(new XmlReader(zookeeperDataFile, log), log, formats)

      val producer = TestProbe()
      val triggerBar = new CyclicBarrier(2)
      val storeBar = new CyclicBarrier(2)
      val config = new ZookeeperLaneConfiguration(curator, "/ctes/systems", None)

      val actor = system.actorOf(Props(new CorridorCalibrationActor(
        curator = curator,
        systemId = "N62",
        corridorId = "S1",
        store = new SelfTestStoreMock(storeBar),
        triggerRegister = new RegisterSelfTestTriggerMock(triggerBar),
        laneConfig = config,
        serialNr = (systemId: String) ⇒ systemId + "-123",
        calibrationProducer = producer.ref,
        certificateValidator = new CertificateValidatorMock,
        responseTooOldAfter = 0.millis,
        gantryRetryDuration = 500.millis,
        responseTimeoutDuration = 2.seconds,
        calibrationTimeOut = 1.second) //fast timeout
        ))
      //send calibration request
      actor ! StartCalibration(new Date(startTime), "userID", "AA1234", "Reason", "A2", "S1")
      //expect the gantry requests
      val req1 = producer.expectMsgType[GantryCalibrationRequest](2000.millis)
      val req2 = producer.expectMsgType[GantryCalibrationRequest](2000.millis)
      req1.laneIds must be(List("N62-E1-E1R1"))
      req1.gantryId must be("E1")
      req2.laneIds must be(List("N62-X1-X1R1"))
      req2.gantryId must be("X1")
      //Expect Calibration response
      storeBar.await(2, TimeUnit.SECONDS)
      triggerBar.await(1, TimeUnit.SECONDS)
      system.stop(actor)
    }
  }
}