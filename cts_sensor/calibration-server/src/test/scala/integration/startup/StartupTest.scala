/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package integration.startup

import akka.testkit.TestKit
import csc.amqp.MQConnection
import org.scalatest._
import akka.actor.{ ActorRef, ActorSystem }
import com.github.sstone.amqp.Amqp
import net.liftweb.json.DefaultFormats
import csc.akka.logging.DirectLogging
import csc.gantry.config._
import csc.calibrationserver.mock._
import csc.gantry.config.ZkCorridorInfo
import csc.gantry.config.ZkSection
import csc.gantry.config.ZkPSHTMIdentification
import csc.gantry.config.ZkCorridor
import csc.gantry.config.TriggerData
import csc.gantry.config.ZkDirection
import csc.vehicle.message.VehicleImageType
import csc.curator.CuratorTestServer
import csc.curator.utils.{ Curator, CuratorToolsImpl }
import csc.json.lift.EnumerationSerializer
import csc.calibrationserver.config.Configuration

/**
 * Test the complete SelfTestComponent. Start all the needed components and add a selftest trigger.
 * The selftest should be called and the trigger removed.
 */
class StartupTest extends TestKit(ActorSystem("StartupTest")) with WordSpecLike with MustMatchers with CuratorTestServer with DirectLogging with BeforeAndAfterAll {
  implicit val formats = DefaultFormats
  val imageTableName = "image"
  val resultTableName = "result"
  var startupMonitorOpt: Option[ActorRef] = None
  var curator: Curator = _

  def getCorridorConfigPath(systemId: String, corridorId: String) = "/ctes/systems/%s/corridors/%s/config".format(systemId, corridorId)
  def getCorridorStatePath(systemId: String, corridorId: String) = "/ctes/systems/%s/corridors/%s/control/errors/alerts/current".format(systemId, corridorId)
  def getCorridorCalibrationPath(systemId: String, corridorId: String) = "/ctes/systems/%s/corridors/%s/control/calibration".format(systemId, corridorId)
  def getCorridorCalibrationTriggerPath(systemId: String, corridorId: String) = getCorridorCalibrationPath(systemId, corridorId) + "/trigger"

  def getClassificationTriggerData(curator: Curator, systemId: String, corridorId: String): Option[TriggerData] = {
    val path = getCorridorCalibrationTriggerPath(systemId, corridorId)
    curator.get[TriggerData](path)
  }

  def putClassificationTriggerData(curator: Curator, systemId: String, corridorId: String, triggerData: TriggerData) = {
    curator.put(getCorridorCalibrationTriggerPath(systemId, corridorId), triggerData)
  }

  override def beforeEach() {
    super.beforeEach()
    val formats = DefaultFormats + new EnumerationSerializer(VehicleImageType, ServiceType, ZkCaseFileType)
    curator = new CuratorToolsImpl(clientScope, log, formats)
  }

  def getAmqpConnection(config: Configuration): MQConnection = {
    val connection = MQConnection.create(config.amqpConfiguration.actorName, config.amqpConfiguration.amqpUri,
      config.amqpConfiguration.reconnectDelay)
    log.info("Amqp connection created")
    connection
  }

  override def afterAll() {
    super.afterAll()
    system.shutdown
  }

  "The boot" must {
    "Walk through the start process" ignore {

      val laneConfig = new LaneConfigurationMock()

      createZookeeperCorridor(curator = curator, systemId = laneConfig.lane1.system, corridorId = laneConfig.corridorId,
        gantryId = laneConfig.lane1.gantry, endGantryId = laneConfig.lane2.gantry)

      val store = new SelfTestStoreMockEmpty()

      putClassificationTriggerData(curator, laneConfig.lane1.system, laneConfig.corridorId, new TriggerData(System.currentTimeMillis(),
        "userId", "VerbCode", false, "START"))

      val boot = new csc.calibrationserver.startup.Boot()
      boot.curator = curator

      boot.selfTestStore = store
      boot.certificateValidator = new CertificateValidatorMock()
      boot.startup()

      Thread.sleep(7000)
      boot.shutdown
    }
  }

  def createZookeeperCorridor(curator: Curator, systemId: String, corridorId: String, gantryId: String, endGantryId: String) {
    val section = ZkSection(id = "S1",
      systemId = systemId,
      name = "S1",
      length = 0,
      startGantryId = gantryId,
      endGantryId = endGantryId,
      matrixBoards = Seq())
    CorridorService.createSection(curator, systemId, section)

    val corridor = ZkCorridor(id = corridorId,
      name = corridorId,
      roadType = 0,
      serviceType = ServiceType.SectionControl,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = false,
      dynamaxMqId = "",
      approvedSpeeds = Seq(),
      pshtm = ZkPSHTMIdentification(useYear = false,
        useMonth = true,
        useDay = false,
        useReportingOfficerId = false,
        useNumberOfTheDay = true,
        useLocationCode = false,
        useHHMCode = false,
        useTypeViolation = false,
        useFixedCharacter = false,
        fixedCharacter = "T"),
      info = ZkCorridorInfo(corridorId = 2,
        locationCode = "2",
        reportId = "",
        reportHtId = "",
        reportPerformance = true,
        locationLine1 = ""),
      startSectionId = section.id,
      endSectionId = section.id,
      allSectionIds = Seq(section.id),
      direction = ZkDirection(directionFrom = "",
        directionTo = ""),
      radarCode = 4,
      roadCode = 0,
      dutyType = "",
      deploymentCode = "",
      codeText = "")
    CorridorService.createCorridor(curator, systemId, corridor)
  }
}

