/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.gantry.config

import csc.akka.logging.DirectLogging
import csc.curator.CuratorTestServer
import csc.curator.utils.{ Curator, CuratorToolsImpl }
import csc.json.lift.EnumerationSerializer
import csc.vehicle.message.VehicleImageType
import net.liftweb.json.DefaultFormats
import org.scalatest.WordSpec
import org.scalatest._

class CorridorServiceTest extends WordSpec with MustMatchers with CuratorTestServer with DirectLogging {
  var curator: Curator = _

  override def beforeEach() {
    super.beforeEach()

    val formats = DefaultFormats + new EnumerationSerializer(VehicleImageType, ServiceType, ZkCaseFileType)
    curator = new CuratorToolsImpl(clientScope, log, formats)

    createCorridors()
  }
  val systemId = "A2"
  val corridorId1 = "Cor1"
  val corridorId2 = "Cor2"
  val corridorId3 = "Cor3"
  val gantryId1 = "Gantry1"
  val gantryId2 = "Gantry2"
  val gantryId3 = "Gantry3"
  val gantryId4 = "Gantry4"

  "The CorridorServiceTest" must {

    "get gantries of a corridor with one section" in {
      val gantries = CorridorService.getCorridorGantryIds(curator, systemId, corridorId1)
      gantries.size must be(2)
      gantries(0) must be(gantryId1)
      gantries(1) must be(gantryId2)
    }
    "get gantries of a corridor with two sections" in {
      val gantries = CorridorService.getCorridorGantryIds(curator, systemId, corridorId2)
      gantries.size must be(2)
      gantries(0) must be(gantryId1)
      gantries(1) must be(gantryId4)
    }
    "get gantry of a corridor with one gantry" in {
      val gantries = CorridorService.getCorridorGantryIds(curator, systemId, corridorId3)
      gantries.size must be(1)
      gantries(0) must be(gantryId1)
    }
  }

  def createCorridors() {

    val section = ZkSection(id = "S1",
      systemId = systemId,
      name = "S1",
      length = 0,
      startGantryId = gantryId1,
      endGantryId = gantryId2,
      matrixBoards = Seq())
    CorridorService.createSection(curator, systemId, section)

    val section2 = ZkSection(id = "S2",
      systemId = systemId,
      name = "S2",
      length = 0,
      startGantryId = gantryId3,
      endGantryId = gantryId4,
      matrixBoards = Seq())
    CorridorService.createSection(curator, systemId, section2)

    val section3 = ZkSection(id = "S3",
      systemId = systemId,
      name = "S3",
      length = 0,
      startGantryId = gantryId1,
      endGantryId = gantryId1,
      matrixBoards = Seq())
    CorridorService.createSection(curator, systemId, section3)

    val corridor = ZkCorridor(id = corridorId1,
      name = corridorId1,
      roadType = 0,
      serviceType = ServiceType.SectionControl,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = false,
      dynamaxMqId = "",
      approvedSpeeds = Seq(),
      pshtm = ZkPSHTMIdentification(useYear = false,
        useMonth = true,
        useDay = false,
        useReportingOfficerId = false,
        useNumberOfTheDay = true,
        useLocationCode = false,
        useHHMCode = false,
        useTypeViolation = false,
        useFixedCharacter = false,
        fixedCharacter = "T"),
      info = ZkCorridorInfo(corridorId = 2,
        locationCode = "2",
        reportId = "",
        reportHtId = "",
        reportPerformance = true,
        locationLine1 = ""),
      startSectionId = section.id,
      endSectionId = section.id,
      allSectionIds = Seq(section.id),
      direction = ZkDirection(directionFrom = "",
        directionTo = ""),
      radarCode = 4,
      roadCode = 0,
      dutyType = "",
      deploymentCode = "",
      codeText = "")
    CorridorService.createCorridor(curator, systemId, corridor)

    val corridor2 = corridor.copy(
      id = corridorId2,
      name = corridorId2,
      startSectionId = section.id,
      endSectionId = section2.id,
      allSectionIds = Seq(section.id, section2.id))
    CorridorService.createCorridor(curator, systemId, corridor2)

    val corridor3 = corridor.copy(
      id = corridorId3,
      name = corridorId3,
      startSectionId = section3.id,
      endSectionId = section3.id,
      allSectionIds = Seq(section3.id))
    CorridorService.createCorridor(curator, systemId, corridor3)
  }
}