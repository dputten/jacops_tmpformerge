/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.gantry.config

import csc.akka.logging.DirectLogging
import csc.config.Path
import csc.curator.CuratorTestServer
import csc.curator.utils.{ Curator, CuratorToolsImpl }
import csc.vehicle.message.Lane
import org.apache.commons.lang.NotImplementedException
import org.scalatest.WordSpec
import org.scalatest._
import csc.gantry.config.ZooKeeperLaneConfigurationTest._

/**
 * Test the Zookeeper implementation of the LaneConfiguration
 */
class ZooKeeperLaneConfigurationTest extends WordSpec with MustMatchers with CuratorTestServer with DirectLogging {
  var curator: Curator = _
  var config: ZookeeperLaneConfiguration = _

  override def beforeEach() {
    super.beforeEach()

    curator = new CuratorToolsImpl(clientScope, log)
    config = new ZookeeperLaneConfiguration(curator, "/ctes/systems/", None)
  }

  override def afterEach() {
    config.shutdown()
    super.afterEach()
  }

  "The ZooKeeperLaneConfiguration" must {

    "get lane data from empty zookeeper" in {
      val laneConfigList = config.getLanesForRoute("A2")
      laneConfigList must have size (0)
    }

    "get pir lane data from zookeeper" in {
      //add configuration
      //create Lane config
      val laneCfg = createLanePir()
      //store Lane config
      storeLane(laneCfg)
      //start test

      val laneConfigList = config.getLanesForRoute("A2")

      deleteLane(laneCfg)
      laneConfigList must be(List(laneCfg))
    }
    "get irose lane data from zookeeper" in {
      //add configuration
      //create Lane config
      val laneCfg = createLaneIRose()
      //store Lane config
      storeLane(laneCfg)
      //start test

      val laneConfigList = config.getLanesForRoute("A4")

      deleteLane(laneCfg)
      laneConfigList must be(List(laneCfg))
    }

    "get pir lane data (with ImageMask) from zookeeper" in {
      //add configuration
      //create Lane config
      val laneCfg = createLanePir4WithImageMask()
      //store Lane config
      storeLane(laneCfg)
      //start test

      val laneConfigList = config.getLanesForRoute("A2")

      deleteLane(laneCfg)
      laneConfigList must be(List(laneCfg))
    }

    "get multiple lane data from zookeeper" in {
      //add configuration
      //create Lane config
      val laneCfgPir = createLanePir()
      storeLane(laneCfgPir)
      val laneCfgPir2 = createLanePir2()
      storeLane(laneCfgPir2)
      val laneCfgPir3 = createLanePir3()
      storeLane(laneCfgPir3)
      //start test
      val laneConfigList = config.getLanesForRoute("A2")
      deleteLane(laneCfgPir)
      deleteLane(laneCfgPir2)
      deleteLane(laneCfgPir3)
      laneConfigList must have size (2)
      laneConfigList must contain(laneCfgPir)
      laneConfigList must contain(laneCfgPir2)
    }

    "get all lane data from zookeeper" in {
      //add configuration
      //create Lane config
      val laneCfgPir = createLanePir()
      storeLane(laneCfgPir)
      val laneCfgPir2 = createLanePir2()
      storeLane(laneCfgPir2)
      val laneCfgPir3 = createLanePir3()
      storeLane(laneCfgPir3)
      //start test
      val laneConfigList = config.getLanes()
      deleteLane(laneCfgPir)
      deleteLane(laneCfgPir2)
      deleteLane(laneCfgPir3)
      laneConfigList must have size (3)
      laneConfigList must contain(laneCfgPir)
      laneConfigList must contain(laneCfgPir2)
      laneConfigList must contain(laneCfgPir3)
    }

    "get corridor data from zookeeper" in {
      val redLightCorridor = CorridorConfig(
        id = "1",
        name = "RedLight",
        serviceType = "RedLight",
        info = ZkCorridorInfo(corridorId = 1,
          locationCode = "3",
          reportId = "",
          reportHtId = "",
          reportPerformance = true,
          locationLine1 = "Location X",
          locationLine2 = Some("Secret")))
      val speedCorridor = CorridorConfig(
        id = "2",
        name = "Speed",
        serviceType = "SpeedFixed",
        info = ZkCorridorInfo(corridorId = 2,
          locationCode = "3",
          reportId = "",
          reportHtId = "",
          reportPerformance = true,
          locationLine1 = "Location Y",
          locationLine2 = Some("Secret")))
      storeCorridors("3215", Seq(redLightCorridor, speedCorridor))
      //start test
      config.getCorridorsConfig("3215") match {
        case Some(corridorsConfig) ⇒
          corridorsConfig.corridors must have size 2
          corridorsConfig.corridors must contain(redLightCorridor)
          corridorsConfig.corridors must contain(speedCorridor)
          corridorsConfig.getRedLightCorridorConfig must be(Some(redLightCorridor))
          corridorsConfig.getSpeedFixedCorridorConfig must be(Some(speedCorridor))
        case None ⇒
          fail("getCorridorsConfig should not return None")
      }
    }

    "get system data from zookeeper" in {
      val system = ZookeeperSystemConfig(id = "A2",
        name = "A2",
        violationPrefixId = "",
        orderNumber = 1,
        location = ZkSystemLocation(
          description = "A2 desc",
          region = "Amsterdam",
          roadNumber = "A2",
          roadPart = "xx",
          systemLocation = "Amsterdam",
          viewingDirection = None),
        maxSpeed = 100,
        compressionFactor = 80,
        retentionTimes = ZkSystemRetentionTimes(
          nrDaysKeepTrafficData = 7,
          nrDaysKeepViolations = 5,
          nrDaysRemindCaseFiles = 2),
        pardonTimes = ZkSystemPardonTimes(),
        mtmRouteId = "",
        serialNumber = SerialNumber("1234", "PCB", 5))
      curator.put("/ctes/systems/A2/config", system)
      val activeCert = ActiveCertificate(System.currentTimeMillis(), ComponentCertificate("Configuration", "ABCDEF1234567890"))
      CertificateService.putJarCertificate(curator, "A2", activeCert)
      val activeCert2 = ActiveCertificate(System.currentTimeMillis(), ComponentCertificate("PleaseIgnore", "ABCDEF1234567890"))
      CertificateService.putJarCertificate(curator, "A2", activeCert2)

      val instType = new MeasurementMethodType(typeDesignation = "",
        category = "A",
        unitSpeed = "km/h",
        unitRedLight = "s",
        unitLength = "m",
        restrictiveConditions = "",
        displayRange = "20-250 km/h",
        temperatureRange = "temperatuurbereik -10 to 50 graden Celsius",
        permissibleError = "toelatbare fout 3%",
        typeCertificate = new TypeCertificate("TP1234", 0, List(ComponentCertificate("Configuration", "ABCDEF1234567890"))))

      curator.put(CertificateService.getCertificateMeasurementMethodTypePath("cert1"), instType)
      val inst = MeasurementMethodInstallation(typeId = "cert1", timeFrom = 100000)
      curator.put(CertificateService.getInstallCertificatePath("A2") + "/cert1", inst)

      //start test
      val cfg = config.getSystemConfig("A2").get
      cfg.activeChecksums must be(Seq(activeCert, activeCert2))
      cfg.typeCertificates must be(Seq(MeasurementMethodInstallationType(inst.timeFrom, instType)))
      cfg.compressionFactor must be(system.compressionFactor)
      cfg.description must be(system.location.description)
      cfg.id must be(system.id)
      cfg.maxSpeed must be(system.maxSpeed)
      cfg.name must be(system.name)
      cfg.nrDaysKeepTrafficData must be(system.retentionTimes.nrDaysKeepTrafficData)
      cfg.nrDaysKeepViolations must be(system.retentionTimes.nrDaysKeepViolations)
      cfg.region must be(system.location.region)
      cfg.roadNumber must be(system.location.roadNumber)
      cfg.roadPart must be(system.location.roadPart)
      cfg.serialNumber must be(system.serialNumber)
      cfg.activeSoftwareSeal must be("d149274109b50d5147c09d6fc7e80c71")
    }

  }

  def storeLane(laneCfg: LaneConfig) {
    val pathGantry = Path("/ctes/systems") / laneCfg.lane.system / "gantries" / laneCfg.lane.gantry
    if (!curator.exists(pathGantry)) {
      curator.createEmptyPath(pathGantry)

      //insert gantry
      val zkGantry = new ZooKeeperGantry(
        id = laneCfg.lane.gantry,
        systemId = laneCfg.lane.system,
        name = laneCfg.lane.gantry,
        longitude = laneCfg.lane.sensorGPS_longitude,
        latitude = laneCfg.lane.sensorGPS_latitude)
      curator.put(pathGantry / "config", zkGantry)
    }

    val path = pathGantry / "lanes" / laneCfg.lane.name
    curator.createEmptyPath(path)

    laneCfg match {
      case cfgPir: PirRadarCameraSensorConfig ⇒ {
        //create Pir config
        val zkCfg = new ZooKeeperPirConfig(
          id = cfgPir.lane.name,
          name = cfgPir.lane.name,
          bpsLaneId = laneCfg.lane.bpsLaneId,
          camera = cfgPir.camera,
          radar = cfgPir.radar,
          pir = cfgPir.pir)
        curator.put(path / "config", zkCfg)
        // create imageMask

        if (cfgPir.imageMask.isDefined) {
          val ckImageMask = new ZooKeeperImageMaskConfig(
            vertices = cfgPir.imageMask.get.vertices map { vertex ⇒ new ZooKeeperImageMaskVertex(x = vertex.x, y = vertex.y) })
          curator.put(path / "imageMask", ckImageMask)
        }
      }
      case cfg: IRoseSensorConfig ⇒ {
        //create iRose config
        case class IRoseConfig(ftpHost: String, ftpPort: Int, ftpUser: String, ftpPwd: String,
                               imageDir: String, registrationDir: String,
                               matchWindow: Long, imageExtension: String, registrationExtension: String,
                               getImageMaxRetries: Int,
                               reconnectTimeout: Long,
                               pollerFrequency: Long,
                               cleanupTime: Long)

        case class SensorIRoseConfig(id: String, name: String, seqName: Option[String], seqNumber: Option[Int], camera: CameraConfig, iRose: IRoseConfig)

        val zkCfg = new SensorIRoseConfig(
          id = cfg.lane.name,
          name = cfg.lane.name,
          seqName = cfg.seqConfig.seqName,
          seqNumber = cfg.seqConfig.seqNumber,
          camera = cfg.camera,
          iRose = new IRoseConfig(ftpHost = "localhost", ftpPort = 12345, ftpUser = "me", ftpPwd = "secret",
            imageDir = "imageDir", registrationDir = "regDir",
            matchWindow = 1000, imageExtension = ".jpg", registrationExtension = ".json",
            getImageMaxRetries = 5,
            reconnectTimeout = 1000,
            pollerFrequency = 1000,
            cleanupTime = 1000))
        curator.set(path, new ZookeeperNodeAttributes("iRose"), 0)
        curator.put(path / "config", zkCfg)
        // create imageMask

        if (cfg.imageMask.isDefined) {
          val ckImageMask = new ZooKeeperImageMaskConfig(
            vertices = cfg.imageMask.get.vertices map { vertex ⇒ new ZooKeeperImageMaskVertex(x = vertex.x, y = vertex.y) })
          curator.put(path / "imageMask", ckImageMask)
        }

      }
      case _ ⇒ {
        //other configurations aren't supported yet
        throw new NotImplementedException("Unsuported LaneConfig " + laneCfg.getClass.getName)
      }
    }
  }
  def deleteLane(laneCfg: LaneConfig) {
    val path = Path("/ctes/systems") / laneCfg.lane.system / "gantries" / laneCfg.lane.gantry / "lanes" / laneCfg.lane.name / "config"
    if (curator.exists(path))
      curator.delete(path)
    val pathIM = Path("/ctes/systems") / laneCfg.lane.system / "gantries" / laneCfg.lane.gantry / "lanes" / laneCfg.lane.name / "imageMask"
    if (curator.exists(pathIM))
      curator.delete(pathIM)
    //try to clean path
    try {
      //lanes
      var tmpPath = path.parent
      curator.delete(tmpPath)
      //the gantry
      tmpPath = tmpPath.parent
      val children = curator.getChildren(tmpPath)
      if (children.size == 1) {
        curator.delete(tmpPath / "config")
        curator.delete(tmpPath)
        //ganties
        tmpPath = tmpPath.parent
        curator.delete(tmpPath)
        //the system
        tmpPath = tmpPath.parent
        curator.delete(tmpPath)
        //systems
        tmpPath = tmpPath.parent
        curator.delete(tmpPath)
        //ctes
        tmpPath = tmpPath.parent
        curator.delete(tmpPath)
      }
    } catch {
      case _: Throwable ⇒ //ignore all
    }
  }

  def storeCorridors(systemId: String, corridorConfigs: Seq[CorridorConfig]): Unit = {
    val pathCorridors = Path("/ctes/systems") / systemId / "corridors"
    if (!curator.exists(pathCorridors)) {
      curator.createEmptyPath(pathCorridors)
      corridorConfigs.foreach { corridorConfig ⇒
        val pathCorridor = pathCorridors / corridorConfig.id
        curator.createEmptyPath(pathCorridor)
        curator.put(pathCorridor / "config", corridorConfig)
      }

    }
  }
}

object ZooKeeperLaneConfigurationTest {

  def createLanePir(): LaneConfig = {
    val lane = new Lane(
      laneId = "A2-44.5HM-Lane1",
      name = "Lane1",
      gantry = "44.5HM",
      system = "A2",
      sensorGPS_latitude = 0,
      sensorGPS_longitude = 0)
    val radar = new RadarConfig(
      radarURI = "mina:tcp://localhost:11000?textline=true",
      measurementAngle = 35.5, //Angle of the radar (degrees) between ground and the radar
      height = 5.3, //height of the radar (meters)
      surfaceReflection = 0.8)
    val pir = new PIRConfig("localhost", 1000, pirAddress = 2, refreshPeriod = 1000, vrHost = "localhost", vrPort = 1100)
    new PirRadarCameraSensorConfigImpl(
      lane = lane,
      camera = new CameraConfig("L4", cameraHost = "localhost", cameraPort = 1400, maxTriggerRetries = 10, timeDisconnected = 20000),
      imageMask = None,
      recognizeOptions = Map(),
      radar = radar,
      pir = pir)
  }
  def createLanePir2(): LaneConfig = {
    val lane = new Lane(
      laneId = "A2-44.5HM-Lane2",
      name = "Lane2",
      gantry = "44.5HM",
      system = "A2",
      sensorGPS_latitude = 0,
      sensorGPS_longitude = 0)
    val radar = new RadarConfig(
      radarURI = "mina:tcp://localhost:11000?textline=true",
      measurementAngle = 35.5, //Angle of the radar (degrees) between ground and the radar
      height = 5.3, //height of the radar (meters)
      surfaceReflection = 0.8)
    val pir = new PIRConfig("localhost", 1000, pirAddress = 2, refreshPeriod = 1000, vrHost = "localhost", vrPort = 1100)
    new PirRadarCameraSensorConfigImpl(
      lane = lane,
      camera = new CameraConfig("L4", cameraHost = "localhost", cameraPort = 1400, maxTriggerRetries = 10, timeDisconnected = 20000),
      imageMask = None,
      recognizeOptions = Map(),
      radar = radar,
      pir = pir)
  }

  def createLanePir3(): LaneConfig = {
    val lane = new Lane(
      laneId = "A4-44.5HM-Lane1",
      name = "Lane1",
      gantry = "44.5HM",
      system = "A4",
      sensorGPS_latitude = 0,
      sensorGPS_longitude = 0)
    val radar = new RadarConfig(
      radarURI = "mina:tcp://localhost:11000?textline=true",
      measurementAngle = 35.5, //Angle of the radar (degrees) between ground and the radar
      height = 5.3, //height of the radar (meters)
      surfaceReflection = 0.8)
    val pir = new PIRConfig("localhost", 1000, pirAddress = 2, refreshPeriod = 1000, vrHost = "localhost", vrPort = 1100)
    new PirRadarCameraSensorConfigImpl(
      lane = lane,
      camera = new CameraConfig("L4", cameraHost = "localhost", cameraPort = 1400, maxTriggerRetries = 10, timeDisconnected = 20000),
      imageMask = None,
      recognizeOptions = Map(),
      radar = radar,
      pir = pir)
  }

  def createLanePir4WithImageMask(): LaneConfig = {
    val lane = new Lane(
      laneId = "A2-44.5HM-Lane1",
      name = "Lane1",
      gantry = "44.5HM",
      system = "A2",
      sensorGPS_latitude = 0,
      sensorGPS_longitude = 0)
    val radar = new RadarConfig(
      radarURI = "mina:tcp://localhost:11000?textline=true",
      measurementAngle = 35.5, //Angle of the radar (degrees) between ground and the radar
      height = 5.3, //height of the radar (meters)
      surfaceReflection = 0.8)
    val pir = new PIRConfig("localhost", 1000, pirAddress = 2, refreshPeriod = 1000, vrHost = "localhost", vrPort = 1100)
    val imageMask = new ImageMaskImpl(List(new ImageMaskVertex(10, 10), new ImageMaskVertex(20, 10), new ImageMaskVertex(20, 20), new ImageMaskVertex(10, 20)))
    new PirRadarCameraSensorConfigImpl(
      lane = lane,
      camera = new CameraConfig("L4", cameraHost = "localhost", cameraPort = 1400, maxTriggerRetries = 10, timeDisconnected = 20000),
      imageMask = Some(imageMask),
      recognizeOptions = Map(),
      radar = radar,
      pir = pir)
  }

  def createLaneIRose(): LaneConfig = {
    val lane = new Lane(
      laneId = "A4-44.5HM-Lane3",
      name = "Lane3",
      gantry = "44.5HM",
      system = "A4",
      sensorGPS_latitude = 0,
      sensorGPS_longitude = 0)
    new IRoseSensorConfigImpl(
      lane = lane,
      camera = new CameraConfig("L43", cameraHost = "localhost", cameraPort = 1400, maxTriggerRetries = 10, timeDisconnected = 20000),
      imageMask = None,
      recognizeOptions = Map(),
      seqConfig = new LaneSeqConfig(Some(5), Some("PCB")))
  }

}