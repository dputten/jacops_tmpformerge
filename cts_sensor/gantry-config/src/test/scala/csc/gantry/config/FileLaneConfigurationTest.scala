/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.gantry.config

import csc.vehicle.message.Lane
import net.liftweb.json.Serialization
import org.scalatest.WordSpec
import org.scalatest._

import scala.collection.mutable.ListBuffer

class FileLaneConfigurationTest extends WordSpec with MustMatchers {
  "The FileLaneConfiguration" must {

    "get lane data from empty config" in {
      val fileName = "gantry-config/src/test/resources/csc/gantry/config/empty.json"
      val config = new FileLaneConfiguration(fileName, None, None)
      val laneConfigList = config.getLanesForRoute("A2")
      laneConfigList must have size (0)

      config.getMultiLaneCameras must be(Nil)
    }

    "get lane and camera data from filled config" in {
      val fileName = "gantry-config/src/test/resources/csc/gantry/config/filled.json"
      val cameraFilename = Some("gantry-config/src/test/resources/csc/gantry/config/cameras.json")
      val config = new FileLaneConfiguration(fileName, None, cameraFilename)
      val laneConfigList = config.getLanesForRoute("A2")
      laneConfigList must have size (2)
      for (cfg ← laneConfigList) {
        cfg.lane.system must be("A2")
        if (cfg.lane.laneId == "200") {
          cfg.lane.isIncoming must be(true)
        } else {
          cfg.lane.isIncoming must be(false)
        }
      }

      val cameras = config.getMultiLaneCameras
      cameras.size must be(1)
      val cam1 = cameras(0)
      cam1.rightLaneId must be(Some("X1R1"))
      cam1.leftLaneId must be(Some("X1R2"))
      cam1.config.relayURI must be("/data/images/raw/X1CAM1")
    }
  }

}

object createJson {
  implicit val formats = FileLaneConfiguration.formats

  def createfile() {
    val buffer = new ListBuffer[LaneConfig]()
    buffer += new RadarCameraSensorConfigImpl(
      lane = new Lane(laneId = "100",
        name = "Lane1",
        gantry = "22.4 HM",
        system = "A2",
        sensorGPS_latitude = 0,
        sensorGPS_longitude = 0),
      camera = new CameraConfig(relayURI = "224_1", cameraHost = "localhost", cameraPort = 1400, maxTriggerRetries = 10, timeDisconnected = 20000),
      imageMask = None,
      recognizeOptions = Map(),
      radar = new RadarConfig(radarURI = "URI:blabla",
        measurementAngle = 33.5, //Angle of the radar (degrees) between ground and the radar
        height = 5.3))
    buffer += new RadarCameraSensorConfigImpl(
      lane = new Lane(laneId = "200",
        name = "Lane1",
        gantry = "44.4 HM",
        system = "A2",
        sensorGPS_latitude = 0,
        sensorGPS_longitude = 0),
      camera = new CameraConfig(relayURI = "444_1", cameraHost = "localhost", cameraPort = 1400, maxTriggerRetries = 10, timeDisconnected = 20000),
      imageMask = None,
      recognizeOptions = Map(),
      radar = new RadarConfig(radarURI = "URI:blabla",
        measurementAngle = 33.5, //Angle of the radar (degrees) between ground and the radar
        height = 5.3))
    buffer += new RadarCameraSensorConfigImpl(
      lane = new Lane(laneId = "100",
        name = "Lane1",
        gantry = "22.4 HM",
        system = "A4",
        sensorGPS_latitude = 0,
        sensorGPS_longitude = 0),
      camera = new CameraConfig(relayURI = "224_1", cameraHost = "localhost", cameraPort = 1400, maxTriggerRetries = 10, timeDisconnected = 20000),
      imageMask = None,
      recognizeOptions = Map(),
      radar = new RadarConfig(radarURI = "URI:blabla",
        measurementAngle = 33.5, //Angle of the radar (degrees) between ground and the radar
        height = 5.3))
    buffer += new HonacSensorConfigImpl(
      lane = new Lane(laneId = "200",
        name = "Lane2",
        gantry = "44.4 HM",
        system = "A2",
        sensorGPS_latitude = 0,
        sensorGPS_longitude = 0),
      camera = new CameraConfig(relayURI = "224_2", cameraHost = "localhost", cameraPort = 1400, maxTriggerRetries = 10, timeDisconnected = 20000),
      imageMask = None,
      recognizeOptions = Map())

    buffer += new DoubleRadarCameraSensorConfigImpl(
      lane = new Lane(laneId = "200",
        name = "Lane3",
        gantry = "66.4 HM",
        system = "A2",
        sensorGPS_latitude = 0,
        sensorGPS_longitude = 0),
      camera = new CameraConfig(relayURI = "664_3", cameraHost = "localhost", cameraPort = 1400, maxTriggerRetries = 10, timeDisconnected = 20000),
      imageMask = None,
      recognizeOptions = Map(),
      radar = new RadarConfig(radarURI = "URI:blabla",
        measurementAngle = 33.5, //Angle of the radar (degrees) between ground and the radar
        height = 5.3),
      radarLength = new RadarConfig(radarURI = "URI:blabla2",
        measurementAngle = 45.0, //Angle of the radar (degrees) between ground and the radar
        height = 5.3))

    buffer += new PirRadarCameraSensorConfigImpl(
      lane = new Lane(laneId = "200",
        name = "Lane4",
        gantry = "66.4 HM",
        system = "A2",
        sensorGPS_latitude = 0,
        sensorGPS_longitude = 0),
      camera = new CameraConfig(relayURI = "664_4", cameraHost = "localhost", cameraPort = 1400, maxTriggerRetries = 10, timeDisconnected = 20000),
      imageMask = None,
      recognizeOptions = Map(),
      radar = new RadarConfig(radarURI = "URI:blabla",
        measurementAngle = 33.5, //Angle of the radar (degrees) between ground and the radar
        height = 5.3),
      pir = new PIRConfig(pirHost = "localhost", pirPort = 9999, pirAddress = 2, refreshPeriod = 1000, vrHost = "localhost", vrPort = 1100))
    buffer += new PirCameraSensorConfigImpl(
      lane = new Lane(laneId = "200",
        name = "Lane4",
        gantry = "66.4 HM",
        system = "A2",
        sensorGPS_latitude = 0,
        sensorGPS_longitude = 0),
      camera = new CameraConfig(relayURI = "664_4", cameraHost = "localhost", cameraPort = 1400, maxTriggerRetries = 10, timeDisconnected = 20000),
      imageMask = None,
      recognizeOptions = Map(),
      pir = new PIRConfig(pirHost = "localhost", pirPort = 9999, pirAddress = 2, refreshPeriod = 1000, vrHost = "localhost", vrPort = 1100))

    val json = Serialization.write(buffer.toList)
    System.out.println("<<" + json + ">>")
  }
}