/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.gantry.config

import org.slf4j.LoggerFactory
import csc.curator.utils.Curator

object CorridorService {
  private val log = LoggerFactory.getLogger(this.getClass)

  def getCorridorConfigPath(systemId: String, corridorId: String) = "/ctes/systems/%s/corridors/%s/config".format(systemId, corridorId)
  def getCorridorStatePath(systemId: String, corridorId: String) = "/ctes/systems/%s/corridors/%s/control/state/current".format(systemId, corridorId)
  def getCorridorCalibrationPath(systemId: String, corridorId: String) = "/ctes/systems/%s/corridors/%s/control/calibration".format(systemId, corridorId)
  def getCorridorCalibrationTriggerPath(systemId: String, corridorId: String) = getCorridorCalibrationPath(systemId, corridorId) + "/trigger"

  def getSectionPath(systemId: String, sectionId: String) = "/ctes/systems/%s/sections/%s/config".format(systemId, sectionId)

  def getCorridorConfig(curator: Curator, systemId: String, corridorId: String): Option[ZkCorridor] = {
    val configPath = getCorridorConfigPath(systemId, corridorId)
    curator.get[ZkCorridor](configPath)
  }

  def getAllAlerts(curator: Curator, systemId: String, corridorId: String): Seq[ZkSystemAlert] = {
    val keyPrefix = getCorridorStatePath(systemId, corridorId)
    val alerts = curator.getChildren(keyPrefix)
    alerts.flatMap(alertPath ⇒ curator.get[ZkSystemAlert](alertPath))
  }

  def getCorridorGantryIds(curator: Curator, systemId: String, corridorId: String): Seq[String] = {
    val corridor = getCorridorConfig(curator, systemId, corridorId)
    corridor.map(cor ⇒ getCorridorGantryIds(curator, systemId, cor)).getOrElse(Seq())
  }

  def getCorridorGantryIds(curator: Curator, systemId: String, corridor: ZkCorridor): Seq[String] = {
    val startSection = curator.get[ZkSection](getSectionPath(systemId, corridor.startSectionId))
    val endSection = if (corridor.startSectionId == corridor.endSectionId) {
      startSection
    } else {
      curator.get[ZkSection](getSectionPath(systemId, corridor.endSectionId))
    }

    val gantries = startSection.map(_.startGantryId) ++ endSection.map(_.endGantryId)
    gantries.toSeq.distinct
  }

  def getCorridorState(curator: Curator, systemId: String, corridorId: String): Option[ZookeeperSystemState] = {
    val path = getCorridorStatePath(systemId, corridorId)
    curator.get[ZookeeperSystemState](path)
  }

  def getClassificationTriggerData(curator: Curator, systemId: String, corridorId: String): Option[TriggerData] = {
    val path = getCorridorCalibrationTriggerPath(systemId, corridorId)
    curator.get[TriggerData](path)
  }

  def putClassificationTriggerData(curator: Curator, systemId: String, corridorId: String, triggerData: TriggerData) = {
    curator.put(getCorridorCalibrationTriggerPath(systemId, corridorId), triggerData)
  }

  def deleteClassificationTriggerData(curator: Curator, systemId: String, corridorId: String) = {
    curator.delete(getCorridorCalibrationTriggerPath(systemId, corridorId))
  }

  def createCorridor(curator: Curator, systemId: String, corridor: ZkCorridor) {
    val configPath = CorridorService.getCorridorConfigPath(systemId, corridor.id)
    curator.put(configPath, corridor)
  }

  def createSection(curator: Curator, systemId: String, section: ZkSection) {
    curator.put(CorridorService.getSectionPath(systemId, section.id), section)
  }
}