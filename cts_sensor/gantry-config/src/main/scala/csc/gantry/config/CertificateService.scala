/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.gantry.config

import csc.config.Path
import java.util.Date
import csc.curator.utils.{ Versioned, Curator }
import csc.common.Signing
//import csc.sectioncontrol.messages.certificates.ComponentCertificate
//import csc.gantry.config.ComponentCertificate

import org.slf4j.LoggerFactory

object CertificateService {
  private val log = LoggerFactory.getLogger(this.getClass.getName)
  /**
   * Return the path where to store the generated certificates.
   * Children must be instances of ActiveCertificate class
   */
  def getActiveCertificatesPath(systemId: String) = "/ctes/systems/%s/activeCertificates".format(systemId)

  /**
   * Return the path where to look for the provided install certificate.
   * The node must be an instance of TypeCertificate class
   */
  def getInstallCertificatePath(systemId: String) = "/ctes/systems/%s/certificate/measurementMethod".format(systemId)

  /**
   * Return the path where to look for the provided Location certificate
   */
  def getLocationCertificatePath(systemId: String) = "/ctes/systems/%s/certificate/location".format(systemId)

  def getCertificateMeasurementMethodTypePath(key: String) = "/ctes/configuration/certificate/measurementMethodType/%s".format(key)

  def getAllInstalledMeasurementMethodTypes(curator: Curator, systemId: String): Seq[MeasurementMethodInstallationType] = {
    val path = Path(getInstallCertificatePath(systemId))
    val children = curator.getChildren(path)

    val install = children.flatMap(curator.get[MeasurementMethodInstallation](_))
    install.sortBy(_.timeFrom)

    install.flatMap(inst ⇒ {
      val path = Path(getCertificateMeasurementMethodTypePath(inst.typeId))
      curator.get[MeasurementMethodType](path).map(new MeasurementMethodInstallationType(inst.timeFrom, _))
    })
  }

  def getMeasurementMethodType(curator: Curator, systemId: String, time: Long): Option[MeasurementMethodType] = {
    val install = getMeasurementMethodInstall(curator, systemId, time)
    install.flatMap(install ⇒ {
      val path = Path(getCertificateMeasurementMethodTypePath(install.typeId))
      curator.get[MeasurementMethodType](path)
    })
  }

  def getMeasurementMethodInstall(curator: Curator, systemId: String, time: Long): Option[MeasurementMethodInstallation] = {
    val path = Path(getInstallCertificatePath(systemId))
    val children = curator.getChildren(path)

    val install = children.flatMap(curator.get[MeasurementMethodInstallation](_))
    install.sortBy(_.timeFrom).reverse.find(_.timeFrom <= time)
  }

  def getLocationCertificate(curator: Curator, systemId: String, time: Long): Option[LocationCertificate] = {
    val path = Path(getLocationCertificatePath(systemId))
    val children = curator.getChildren(path)

    val install = children.flatMap(curator.get[LocationCertificate](_))
    install.sortBy(_.inspectionDate).reverse.find(loc ⇒ loc.inspectionDate <= time && loc.validTime > time)
  }

  def getNMICertificate(cert: Seq[MeasurementMethodInstallationType], time: Date): String = {
    getNMICertificate(cert, time.getTime)
  }
  def getNMICertificate(cert: Seq[MeasurementMethodInstallationType], time: Long): String = {
    val currentCertificate = cert.sortBy(_.timeFrom).reverse.find(_.timeFrom <= time)
    currentCertificate.map(_.measurementType.typeCertificate.id).getOrElse("")
  }

  def getCurrentCertificate(cert: Seq[MeasurementMethodInstallationType], time: Date): Option[MeasurementMethodInstallationType] = {
    getCurrentCertificate(cert, time.getTime)
  }
  def getCurrentCertificate(cert: Seq[MeasurementMethodInstallationType], time: Long): Option[MeasurementMethodInstallationType] = {
    cert.sortBy(_.timeFrom).reverse.find(_.timeFrom <= time)
  }

  def getComponentCertificates(curator: Curator, systemId: String, time: Long): Either[String, List[ComponentCertificate]] = {
    //get typecertificate components
    val typeCert = getMeasurementMethodType(curator, systemId, time)
    //get location components
    val location = getLocationCertificate(curator, systemId, time)
    //join lists
    (typeCert.map(_.typeCertificate.components), location.map(_.components)) match {
      case (Some(l1), Some(l2)) ⇒ Right(l1 ++ l2)
      case (None, Some(_))      ⇒ Left("Type certificate")
      case (Some(_), None)      ⇒ Left("Location certificate")
      case (None, None)         ⇒ Left("Type and location certificate")
    }
  }
  def getActiveJarCertificate(curator: Curator, systemId: String, componentName: String): Option[ActiveCertificate] = {
    val path = Path(getActiveCertificatesPath(systemId)) / componentName
    curator.get[ActiveCertificate](path.toString())
  }

  def getAllActiveJarCertificates(curator: Curator, systemId: String): Seq[ActiveCertificate] = {
    val path = Path(getActiveCertificatesPath(systemId))
    val componentNames = curator.getChildNames(path)
    componentNames.flatMap(name ⇒ getActiveJarCertificate(curator, systemId, name))
  }

  def putJarCertificate(curator: Curator, systemId: String, certificate: ActiveCertificate) {
    val path = Path(getActiveCertificatesPath(systemId)) / certificate.componentCertificate.name
    curator.getVersioned[ActiveCertificate](path.toString()) match {
      case Some(Versioned(_, v)) ⇒ {
        curator.set(path.toString(), certificate, v)
      }
      case None ⇒
        if (!curator.exists(path.parent.toString())) {
          curator.createEmptyPath(path.parent.toString())
        }
        curator.put(path.toString(), certificate)
    }
  }

  def removeOldJarVersionsCertificates(curator: Curator, systemId: String, compName: String) {
    val pos = compName.indexOf("_")
    if (pos > 0) {
      val baseName = compName.substring(0, pos)
      val path = getActiveCertificatesPath(systemId)
      val moduleNames = curator.getChildNames(path)

      val oldVersions = moduleNames.filter(name ⇒ name != compName && name.startsWith(baseName))

      oldVersions.foreach(name ⇒ curator.delete(path + "/" + name))
    }
  }

  /*
  def getActiveSoftwareCertificate(curator: Curator, systemId: String): ComponentCertificate = {
    val activeComponentCerts = getAllActiveJarCertificates(curator, systemId).map(_.componentCertificate)
    ComponentCertificate("Softwarezegel", calculateSoftwareSeal(activeComponentCerts))
  }
*/

  def getActiveSoftwareCertificate(curator: Curator, systemId: String, time: Long = System.currentTimeMillis()): ComponentCertificate = {
    log.info("Calculating active software seal")
    val seal = getMeasurementMethodType(curator, systemId, time) match {
      case None ⇒ ""
      case Some(measurementMethodType) ⇒ {
        val activeCertificates = measurementMethodType.typeCertificate.components map { (component) ⇒
          log.info("software seal using component <" + component.name + ">")
          getActiveJarCertificate(curator, systemId, component.name)
        }
        calculateSoftwareSeal(activeCertificates.flatten.map(_.componentCertificate))
      }
    }

    log.info("Active software seal=<" + seal + ">")
    ComponentCertificate("Softwarezegel", seal)
  }

  def calculateSoftwareSeal(certs: Seq[ComponentCertificate]): String = {
    val appendedChecksums = certs.sortBy(_.name).foldLeft("") { (accu: String, cert: ComponentCertificate) ⇒
      accu + cert.checksum
    }
    Signing.calculateHash(Some(appendedChecksums.getBytes("UTF-8")))
  }
}