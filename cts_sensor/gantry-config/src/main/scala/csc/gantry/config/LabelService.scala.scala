/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.gantry.config

import csc.config.Path
import csc.curator.utils.Curator

/**
 * Class representing labels
 *
 * @param nodePath The path of the node which has the label
 * @param nodeType The type of the Node
 * @param labelName The name of the label
 */
case class Label(nodePath: String, nodeType: String, labelName: String) {
  def getNodeName(): String = {
    Path(nodePath).nodes.last.name
  }
}

case class ZkLabels(labels: String)

/**
 * Service to get labels.
 */
object LabelService {
  def getDefaultPath = Path("/ctes/labels")

  /**
   * Definitions of the label types supported at this moment
   */
  object LabelTypes {
    val system = "System"
    val gantry = "Gantry"
    val lane = "Lane"
    val corridor = "Corridor"
    val section = "Section"
  }

  /**
   * Get all existing labels
   * @param curator The Curator used to connect to Zookeeper
   * @return Seq of labels
   */
  def getAllLabels(curator: Curator): Seq[Label] = {
    curator.get[List[Label]](getDefaultPath).getOrElse(Seq())
  }

  /**
   * Get all labels for the specified nodetype
   * @param nodeType The node type
   * @param curator The Curator used to connect to Zookeeper
   * @return Seq of labels
   */
  def getLabelsByType(nodeType: String, curator: Curator): Seq[Label] = {
    getAllLabels(curator).filter(_.nodeType == nodeType)
  }

  /**
   * Get all labels for given node
   * @param nodePath The path of the node
   * @param curator The Curator used to connect to Zookeeper
   * @return Seq of labels
   */
  def getLabelsByNode(nodePath: String, curator: Curator): Seq[Label] = {
    getAllLabels(curator).filter(_.nodePath == nodePath)
  }

  /**
   * Get the labels for the specified labelName
   * @param label the labelName
   * @param curator The Curator used to connect to Zookeeper
   * @return Seq of labels
   */
  def getLabels(label: String, curator: Curator): Seq[Label] = {
    getAllLabels(curator).filter(_.labelName == label)
  }

  /**
   * get the label nodes for the specified label and node type
   * @param label The labelName
   * @param nodeType the node type
   * @param curator The Curator used to connect to Zookeeper
   * @return Seq of labels
   */
  def getLabelsByLabelAndType(label: String, nodeType: String, curator: Curator): Seq[Label] = {
    getAllLabels(curator).filter(l ⇒ l.nodeType == nodeType && l.labelName == label)
  }
}