/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.gantry.config

/*
* Is used as an storage object that contains everything of a Corridor
* @param id of corridor
* @param name of corridor
* @param serviceType service type of corridor
* @param pardonTimeEsa
* @param pardonTimeTechnical
* @param pshtm
* @param info
* @param startSectionId
* @param endSectionId
* @param allSectionIds
*/
case class ZkCorridor(id: String,
                      name: String,
                      roadType: Int,
                      serviceType: ServiceType.Value,
                      caseFileType: ZkCaseFileType.Value = ZkCaseFileType.HHMVS40,
                      isInsideUrbanArea: Boolean,
                      dynamaxMqId: String,
                      approvedSpeeds: Seq[Int],
                      pshtm: ZkPSHTMIdentification,
                      info: ZkCorridorInfo,
                      startSectionId: String,
                      endSectionId: String,
                      allSectionIds: Seq[String],
                      direction: ZkDirection,
                      radarCode: Int,
                      roadCode: Int,
                      dutyType: String,
                      deploymentCode: String,
                      codeText: String)

case class ZkDirection(directionFrom: String,
                       directionTo: String)

//service type of corridor
object ServiceType extends Enumeration {
  val RedLight, SpeedFixed, SpeedMobile, SectionControl, ANPR, TargetGroup = Value
}

object ZkCaseFileType extends Enumeration {
  val TCVS33 /* old - IRS TC-VS versie 3.3 */ = Value
  val HHMVS14 /* new - IRS HHM-VS 1.4 */ = Value
  val HHMVS40 /* new - IRS HHM-VS 4.0 */ = Value
  val HHMVS20 = Value
}

/*
* Is used as an storage object that contains everything of a Corridor
* @param corridorId of corridor
* @param locationCode
* @param reportId
* @param reportHtId
* @param locationLine1
* @param locationLine2
*/
case class ZkCorridorInfo(corridorId: Int,
                          locationCode: String,
                          reportId: String,
                          reportHtId: String,
                          reportPerformance: Boolean,
                          locationLine1: String,
                          locationLine2: Option[String] = None)

/*
* Is used as an storage object that represents what parts of a PSHTM number must be used
* @param useYear
* @param useMonth
* @param useDay
* @param useReportingOfficerId
* @param useNumberOfTheDay
* @param useLocationCode
* @param useTypeViolation
* @param useFixedCharacter
* @param fixedCharacter
*/
case class ZkPSHTMIdentification(useYear: Boolean,
                                 useMonth: Boolean,
                                 useDay: Boolean,
                                 useReportingOfficerId: Boolean,
                                 useNumberOfTheDay: Boolean,
                                 useLocationCode: Boolean,
                                 useHHMCode: Boolean,
                                 useTypeViolation: Boolean,
                                 useFixedCharacter: Boolean,
                                 fixedCharacter: String,
                                 elementOrderString: Option[String] = None)

/*
* Role where an user belongs to
* @param id
* @param systemId
* @param name
* @param length
* @param startGantryId
* @param endGantryId
* @param matrixBoards sequence of matrix board ids
*/
case class ZkSection(id: String,
                     systemId: String,
                     name: String,
                     length: Double,
                     startGantryId: String,
                     endGantryId: String,
                     matrixBoards: Seq[String])

/**
 * The class representing the Json class in Zookeeper representing the selftest trigger
 * @param modTime
 * @param userId
 * @param reportingOfficerCode
 * @param processed
 * @param reason
 */
case class TriggerData(modTime: Long, userId: String, reportingOfficerCode: String, processed: Boolean, reason: String)
