/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.gantry.config

import csc.config.Path
import csc.curator.utils.Curator

object RecognizeService {

  /**
   *  All paths used in this service
   */
  object Paths {
    val recognize = "recognize"

    /**
     * return the decoration path for this application
     */
    def getDefaultRecognizeOptionsPath() = Path("/ctes") / "configuration" / recognize

    /**
     * return the base path for this system
     */
    def getSystemPath(systemId: String) = Path("/ctes") / "systems" / systemId

    /**
     * return the decoration path for specified system
     */
    def getSystemRecognizeOptionsPath(systemId: String) = getSystemPath(systemId) / recognize

    /**
     * return the decoration path for specified lane
     */
    def getLaneRecognizeOptionsPath(systemId: String, gantryId: String, laneId: String) = getSystemPath(systemId) / "gantries" / gantryId / "lanes" / laneId / recognize

  }

  def getRecognizeOptions(curator: Curator, systemId: String, gantryId: String, laneId: String): Map[String, String] = {
    val default = Map[String, String]()
    val pathList = Seq(
      Paths.getDefaultRecognizeOptionsPath(),
      Paths.getSystemRecognizeOptionsPath(systemId),
      Paths.getLaneRecognizeOptionsPath(systemId, gantryId, laneId))
    val optMap = curator.get[Map[String, String]](pathList, default)
    optMap.getOrElse(Map())
  }

}