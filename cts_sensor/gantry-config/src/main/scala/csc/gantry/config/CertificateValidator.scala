package csc.gantry.config

import java.text.SimpleDateFormat
import java.util.Date
import csc.curator.utils.Curator

trait CertificateValidator {
  def validateCertificatesForSystemId(systemId: String,
                                      currentDate: Long): List[String]
}

/**
 * Copyright (C) 2012,2013 CSC <http://www.csc.com>.
 * User: jwluiten
 * Date: 7/10/13
 */
class CertificateValidatorZookeeper(curator: Curator) extends CertificateValidator {

  /**
   *
   * @param systemId Identification of the system for which certificates are to be validated
   * @param currentDate The current time
   * @return List[String] containing errors resulting from validation of certificates. Empty if all
   *         certificates validated successfully
   */
  def validateCertificatesForSystemId(systemId: String,
                                      currentDate: Long): List[String] = {

    val someComponents = CertificateService.getComponentCertificates(curator, systemId, currentDate)
    val errorList = someComponents match {
      case Right(components) ⇒ checkAllActiveCertificates(curator, systemId, components)
      case Left(msg)         ⇒ List(msg + " niet gevonden")
    }
    errorList ++ checkLocationCertificate(curator, systemId, currentDate)
  }

  private def checkAllActiveCertificates(curator: Curator, systemId: String, components: List[ComponentCertificate]): List[String] = {
    components.foldLeft(List[String]()) {
      (list, current) ⇒
        {
          val active: Option[ActiveCertificate] = CertificateService.getActiveJarCertificate(curator, systemId, current.name)
          if (active.isEmpty) {
            list :+ "Actief certificaat voor %s is niet gevonden".format(current.name)
          } else {
            val activeChecksum = active.map(_.componentCertificate.checksum).getOrElse("")
            if (current.checksum != activeChecksum) {
              list :+ "Actief certificaat voor %s is ongelijk aan inhoud van Type Certificaat (%s != %s)".format(current.name, current.checksum, activeChecksum)
            } else {
              //checksum is correct
              list
            }
          }
        }
    }
  }

  private def checkLocationCertificate(curator: Curator, systemId: String, currentDate: Long): Option[String] = {
    val locCert = CertificateService.getLocationCertificate(curator, systemId, currentDate)
    if (locCert.isEmpty) {
      val dateStr = (new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS")).format(new Date(currentDate))
      Some("Geen lokatie certificaat gevonden voor systeem %s met tijd %s".format(systemId, dateStr))
    } else {
      None
    }
  }

}
