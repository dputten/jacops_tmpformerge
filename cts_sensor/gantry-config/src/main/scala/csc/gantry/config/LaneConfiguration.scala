/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.gantry.config

import java.io.File

import csc.akka.logging.DirectLogging
import csc.config.Path
import csc.curator.utils.Curator
import csc.vehicle.message.Lane
import net.liftweb.json.{ DefaultFormats, Serialization, ShortTypeHints }
import org.apache.commons.io.FileUtils

/**
 * Interface of getting the Lane configuration.
 */
trait LaneConfiguration {
  /**
   * Get all systems ids
   * @return list of systemsIds.
   */
  def getAllSystemIds(): Seq[String]

  /**
   * Get all lane configuration items of the specified gantry
   * @param systemId the system of the gantry
   * @param gantryId the requested gantry
   * @return List of LaneConfig
   */
  def getLanesForGantry(systemId: String, gantryId: String): List[LaneConfig]
  /**
   * Get all lane configuration items of the specified route
   * @param route the requested route
   * @return List of LaneConfig
   */
  def getLanesForRoute(route: String): List[LaneConfig]

  /**
   * Get the configuration of all known lanes for all routes
   * @return List of all LaneConfig
   */
  def getLanes(): List[LaneConfig]

  /**
   * Get system configuration of the specified system
   * @return
   */
  def getSystemConfig(system: String): Option[SystemConfig]

  /**
   * Get corridor configuration of the specified system
   * @return
   */
  def getCorridorsConfig(system: String): Option[CorridorsConfig]

  def getMultiLaneCameras: List[MultiLaneCamera]

  def isMultiLaneSystem = getMultiLaneCameras.size > 0

  def getCameras: List[CameraConfig] = getMultiLaneCameras match {
    case Nil  ⇒ getLanes() map { _.camera }
    case list ⇒ list map { _.config }
  }

}

trait DefaultLaneConfiguration extends LaneConfiguration {

  def lanes: List[LaneConfig]

  def getLanes(): List[LaneConfig] = lanes

  def getAllSystemIds(): Seq[String] = lanes.map(_.lane.system).distinct

  def getLanesForGantry(systemId: String, gantryId: String): List[LaneConfig] =
    lanes.filter(cfg ⇒ cfg.lane.system == systemId && cfg.lane.gantry == gantryId)

  override def getLanesForRoute(route: String): List[LaneConfig] =
    lanes.filter(cfg ⇒ cfg.lane.system == route)

  def getActiveSystems(): List[String] = Nil

  def getSystemConfig(system: String): Option[SystemConfig] = None

  def getCorridorsConfig(system: String): Option[CorridorsConfig] = None

  def getMultiLaneCameras: List[MultiLaneCamera] = Nil

}

/**
 * Empty implementation of the LaneConfiguration
 */
class EmptyLaneConfiguration extends DefaultLaneConfiguration {
  val lanes = Nil
}

/**
 * Implementation of the LanConfiguration using ZooKeeper
 *
 * @param curator
 * @param path
 * @param labelPath
 */
class ZookeeperLaneConfiguration(curator: Curator, path: String, labelPath: Option[String]) extends LaneConfiguration with DirectLogging {
  val keyPrefix = Path(path)
  val keyGantry = "gantries"
  val keyLane = "lanes"
  val keyConfig = "config"
  val keyImageMask = "imageMask"
  val keyState = "state"
  val keyAlerts = "alerts"
  val keyCorridors = "corridors"
  val ENFORCE = "Enforce"

  def shutdown() {
  }

  /**
   * Get all systems ids
   * @return list of systemsIds.
   */
  def getAllSystemIds(): Seq[String] = {
    getSystems().map(_.nodes.last.name)
  }

  /**
   * Get for all known routes all known lanes from ZooKeeper
   * @return List of all LaneConfig
   */
  def getLanes(): List[LaneConfig] = {

    var lanes = List[LaneConfig]()
    if (curator.exists(keyPrefix)) {
      val routeList = getSystems()
      for (routePath ← routeList) {
        val route = routePath.nodes.last.name
        lanes ++= getLanesForRoute(route)
      }
    }
    lanes.toList
  }

  /**
   * Get all systems or when a labelPath is defined
   * return all the systems of the labels
   * @return list of paths to systems
   */
  def getSystems(): Seq[Path] = {
    labelPath match {
      case Some(path) ⇒ {
        val lableCfg = curator.get[ZkLabels](Path(path))
        val labels = lableCfg.map(_.labels).getOrElse("")
        val labelList = labels.split(",")

        val systemNames = labelList.flatMap(label ⇒ {
          val labels = LabelService.getLabelsByLabelAndType(label, LabelService.LabelTypes.system, curator)
          //select all lanes for the systems
          labels.map(lb ⇒ lb.getNodeName())
        })
        //create fullPaths
        systemNames.map(keyPrefix / _)
      }
      case None ⇒ curator.getChildren(keyPrefix)
    }
  }

  /**
   * Get the Lanes for the specified route from ZooKeeper
   * @param route the requested route
   * @return List of LaneConfig
   */
  def getLanesForRoute(route: String): List[LaneConfig] = {
    val routePath = keyPrefix / route / keyGantry
    var lanes = List[LaneConfig]()
    if (curator.exists(routePath)) {
      val gantryList = curator.getChildren(routePath).toList
      for (gantryPath ← gantryList) {
        val lanePath = gantryPath / keyLane
        val laneList = curator.getChildren(lanePath).toList
        for (laneRootPath ← laneList) {
          lanes = lanes ++ getLaneConfig(laneRootPath, route)
        }
      }
    }
    lanes.toList
  }

  def getLanesForGantry(systemId: String, gantryId: String): List[LaneConfig] = {
    val gantryPath = keyPrefix / systemId / keyGantry / gantryId
    var lanes = List[LaneConfig]()
    if (curator.exists(gantryPath)) {
      val lanePath = gantryPath / keyLane
      val laneList = curator.getChildren(lanePath).toList
      for (laneRootPath ← laneList) {
        lanes = lanes ++ getLaneConfig(laneRootPath, systemId)
      }
    }
    lanes.toList
  }

  /**
   * Create the lane configuration from specified path
   * At this moment only the PIR configuration is supported
   * @param lanePath /ctes/systems/<route>/gantries/<gantry>/lanes/<lane>
   * @return The Lane Configuration
   */
  def getLaneConfig(lanePath: Path, systemId: String): Option[LaneConfig] = {
    val node = curator.get[ZookeeperNodeAttributes](lanePath)
    val nodeType = node match {
      case Some(nodeAttr) ⇒ nodeAttr.nodeType
      case None           ⇒ "PirRadar"
    }
    nodeType match {
      case "PirRadar" ⇒ getPirLaneConfig(lanePath, systemId)
      case "iRose"    ⇒ getIRoseConfig(lanePath, systemId)
      case other      ⇒ None
    }
  }

  /**
   * Get a PirRadar Configuration
   * @param lanePath
   * @return
   */
  private def getPirLaneConfig(lanePath: Path, systemId: String): Option[LaneConfig] = {
    val pirCfg = curator.get[ZooKeeperPirConfig](lanePath / keyConfig)
    val pir = pirCfg match {
      case None ⇒ {
        log.warning("Node %s doesn't contain pir configuration.".format(lanePath / keyConfig))
        return None
      }
      case Some(cfg) ⇒ cfg
    }
    // Get Lane info
    val lane = getLaneInfo(lanePath, pir.name, pir.id, systemId, pir.bpsLaneId) match {
      case None    ⇒ return None
      case Some(l) ⇒ l
    }

    // Get ImageMask info
    val imageMask = getImageMask(lanePath)
    val laneId = lanePath.nodes.last.name
    val recognizeOptions = RecognizeService.getRecognizeOptions(curator, lane.system, lane.gantry, laneId)

    // Create (correct) LaneConfig's
    if (pir.radar.radarURI.indexOf(":") < 0) {
      //radar URI not set don't use radar
      Some(new PirCameraSensorConfigImpl(lane = lane, camera = pir.camera, imageMask = imageMask, recognizeOptions = recognizeOptions, pir = pir.pir))
    } else {
      Some(new PirRadarCameraSensorConfigImpl(lane = lane, camera = pir.camera, imageMask = imageMask, recognizeOptions = recognizeOptions, radar = pir.radar, pir = pir.pir))
    }
  }

  /**
   * Get the IRose Configuration
   * @param lanePath
   * @return
   */
  private def getIRoseConfig(lanePath: Path, systemId: String): Option[LaneConfig] = {
    val iRoseCfg = curator.get[ZooKeeperIRoseConfig](lanePath / keyConfig)
    val iRose = iRoseCfg match {
      case None ⇒ {
        log.warning("Node %s doesn't contain iRose configuration.".format(lanePath / keyConfig))
        return None
      }
      case Some(cfg) ⇒ cfg
    }

    val lane = getLaneInfo(lanePath, iRose.name, iRose.id, systemId, iRose.bpsLaneId) match {
      case None    ⇒ return None
      case Some(l) ⇒ l
    }
    // Get ImageMask info
    val imageMask = getImageMask(lanePath)
    val laneId = lanePath.nodes.last.name
    val recognizeOptions = RecognizeService.getRecognizeOptions(curator, lane.system, lane.gantry, laneId)

    val seqConfig = LaneSeqConfig(seqNumber = iRose.seqNumber, seqName = iRose.seqName)
    Some(new IRoseSensorConfigImpl(lane = lane, camera = iRose.camera, imageMask = imageMask, recognizeOptions = recognizeOptions, seqConfig = seqConfig))
  }
  /**
   * Get the lane Information
   * @param lanePath
   * @param laneName
   * @param laneId
   * @return
   */
  private def getLaneInfo(lanePath: Path, laneName: String, laneId: String, systemId: String, bpsLaneId: Option[String]): Option[Lane] = {
    // Get Lane info
    // ctes/systems/<route>/gantries/<gantry>/config
    val gantryConfig = lanePath.parent.parent / keyConfig
    val gantryCfg = curator.get[ZooKeeperGantry](gantryConfig)
    val gantry = gantryCfg match {
      case None ⇒ {
        log.warning("Node %s doesn't contain gantry configuration.".format(gantryConfig))
        return None
      }
      case Some(cfg) ⇒ cfg
    }
    val uniqueLaneId = systemId + "-" + gantry.id + "-" + laneId
    val lane = new Lane(laneId = uniqueLaneId,
      name = laneName,
      gantry = gantry.id,
      system = gantry.systemId,
      sensorGPS_longitude = gantry.longitude,
      sensorGPS_latitude = gantry.latitude,
      bpsLaneId = bpsLaneId)
    Some(lane)
  }

  /**
   * Get the image mask of a lane
   * @param lanePath
   * @return
   */
  private def getImageMask(lanePath: Path): Option[ImageMask] = {
    // ctes/systems/<route>/gantries/<gantry>/lanes/<lane>/imageMask
    val imageMaskCfg = curator.get[ZooKeeperImageMaskConfig](lanePath / keyImageMask)
    imageMaskCfg match {
      case None ⇒ {
        log.info("Node %s doesn't contain imageMask configuration.".format(lanePath / keyImageMask))
        None
      }
      case Some(cfg) ⇒ Some(createImageMask(cfg))
    }
  }

  private def createImageMask(config: ZooKeeperImageMaskConfig): ImageMask = {
    val vertices = config.vertices map { vertex ⇒ new ImageMaskVertex(vertex.x, vertex.y) }
    new ImageMaskImpl(vertices)
  }

  def getSystemConfig(system: String): Option[SystemConfig] = {
    val path = keyPrefix / system / keyConfig
    val systemConf = curator.get[ZookeeperSystemConfig](path)
    val typeCert = CertificateService.getAllInstalledMeasurementMethodTypes(curator, system)
    val checksums = CertificateService.getAllActiveJarCertificates(curator, system)

    systemConf.map(_.createSystemConfig(typeCert, checksums))
  }

  def getCorridorsConfig(system: String): Option[CorridorsConfig] = {
    val path = keyPrefix / system / keyCorridors
    if (curator.exists(path)) {
      val corridors = curator.getChildren(path).toList.flatMap { corridorPath ⇒
        curator.get[CorridorConfig](corridorPath / keyConfig)
      }
      Some(CorridorsConfig(corridors = corridors))
    } else {
      None
    }
  }

  def getMultiLaneCameras: List[MultiLaneCamera] = Nil //no multilane camera setup with zookeeper

}

case class ZkSystemAlert(path: String, message: String, timestamp: Long, configType: String, reductionFactor: Float, confirmed: Boolean) {
  val postFixCamera = "camera"
  def isCameraAlert: Boolean = {
    path.endsWith(postFixCamera)
  }
  def getLaneId(): String = {
    if (isCameraAlert) {
      val component = path.replace("/", "-")
      val postFixLength = postFixCamera.length() + 1
      if (component.length() > postFixLength) {
        component.substring(0, component.length() - postFixLength)
      } else ""
    } else ""
  }
}

/**
 * Zookeeper class to get the iRose configuration
 * zookeeper contains also a irose attribute but this isn't used here
 *
 * @param id The id of the node
 * @param name the name of the lane
 * @param camera the camera attributes
 */
case class ZooKeeperIRoseConfig(id: String, name: String, bpsLaneId: Option[String], seqNumber: Option[Int] = None, seqName: Option[String] = None, camera: CameraConfig)

/**
 * class to contain the node attributes
 * @param nodeType
 */
case class ZookeeperNodeAttributes(nodeType: String)
/**
 * Configuration of a lane in zookeeper. At this moment only Pir configuration is supported
 * @param id  filtered name
 * @param name  the name of this lane
 * @param camera the relay sensor configuration
 * @param radar the radar configuration
 * @param pir the pir configuration
 */
case class ZooKeeperPirConfig(id: String, name: String, bpsLaneId: Option[String], camera: CameraConfig, radar: RadarConfig, pir: PIRConfig)

/**
 * The configuration of a gantry in zookeeper
 * @param id  the filtered name
 * @param systemId gantry is part of this system
 * @param name  the name of the gantry
 * @param longitude the longitude of the location of the gantry
 * @param latitude the latitude of the location of the gantry
 */
case class ZooKeeperGantry(id: String, systemId: String, name: String, longitude: Double, latitude: Double)

/**
 * The configuration of an image mask in zookeeper
 * @param vertices A list of vertices, each represented by an (x,y) tuple.
 */
case class ZooKeeperImageMaskConfig(vertices: List[ZooKeeperImageMaskVertex])

/**
 * Represents one vertex entry in zookeeper
 * @param x x coordinate
 * @param y y coordinate
 */
case class ZooKeeperImageMaskVertex(x: Int, y: Int)

/**
 * The state of the system in zookeeper
 * @param state The current State
 * @param timestamp The time this state is set
 * @param userId  The user responsible for this state change
 * @param reason The raeson of the state change
 */
case class ZookeeperSystemState(state: String, timestamp: Long, userId: String, reason: String)

case class ZookeeperSystemConfig(id: String,
                                 name: String,
                                 location: ZkSystemLocation,
                                 violationPrefixId: String,
                                 orderNumber: Int,
                                 maxSpeed: Int,
                                 compressionFactor: Int,
                                 retentionTimes: ZkSystemRetentionTimes,
                                 pardonTimes: ZkSystemPardonTimes,
                                 esaProviderType: String = "NoProvider",
                                 mtmRouteId: String,
                                 minimumViolationTimeSeparation: Int = 0, //minutes
                                 serialNumber: SerialNumber) {

  def isDynamax: Boolean = esaProviderType == "Dynamax"

  def createSystemConfig(typeCert: Seq[MeasurementMethodInstallationType], checksums: Seq[ActiveCertificate]): SystemConfig = {
    SystemConfigImpl(id = id, name = name, description = location.description,
      maxSpeed = maxSpeed, compressionFactor = compressionFactor, nrDaysKeepTrafficData = retentionTimes.nrDaysKeepTrafficData,
      nrDaysKeepViolations = retentionTimes.nrDaysKeepViolations, region = location.region, roadNumber = location.roadNumber, roadPart = location.roadPart,
      serialNumber = serialNumber, typeCertificates = typeCert, activeChecksums = checksums)
  }
}

/**
 * PardonTimes related attributes of the system configuration.
 * @param pardonTimeEsa_secs
 * @param pardonTimeTechnical_secs
 */
case class ZkSystemPardonTimes(pardonTimeEsa_secs: Long = 0, pardonTimeTechnical_secs: Long = 0)

/**
 * RetentionTimes related attributes of the system configuration.
 * @param nrDaysKeepTrafficData
 * @param nrDaysKeepViolations
 * @param nrDaysRemindCaseFiles
 */
case class ZkSystemRetentionTimes(nrDaysKeepTrafficData: Int,
                                  nrDaysKeepViolations: Int,
                                  nrDaysRemindCaseFiles: Int)

/**
 * Location related attributes of the system configuration.
 * @param description
 * @param viewingDirection
 * @param region
 * @param roadNumber
 * @param roadPart
 */
case class ZkSystemLocation(description: String,
                            viewingDirection: Option[String],
                            region: String,
                            roadNumber: String,
                            roadPart: String,
                            systemLocation: String)

/*
* An user
* @param id
* @param reportingOfficerId
* @param phone
* @param ipAddress
* @param name
* @param username
* @param email
* @param passwordHash
* @param roleId
* @param sectionIds
*/
case class ZookeeperUser(id: String,
                         reportingOfficerId: String,
                         phone: String,
                         ipAddress: String,
                         name: String,
                         username: String,
                         email: String,
                         passwordHash: String,
                         roleId: String,
                         sectionIds: List[String])

/**
 * Json File implementation of the LaneConfiguration
 * @param inputFile The file containing the lane configuration in JSON format
 * @param systemCfg The system config used for all systems
 */
class FileLaneConfiguration(inputFile: String, systemCfg: Option[SystemConfig], cameraConfigFile: Option[String] = None)
  extends DefaultLaneConfiguration {
  implicit val formats = FileLaneConfiguration.formats
  val json = FileUtils.readFileToString(new File(inputFile))
  val cameraJson = cameraConfigFile map { p ⇒ FileUtils.readFileToString(new File(p)) }
  val configList = Serialization.read[List[LaneConfig]](json)
  val cameraList: List[MultiLaneCamera] = cameraJson map { j ⇒ Serialization.read[List[MultiLaneCamera]](j) } getOrElse (Nil)

  override def lanes: List[LaneConfig] = configList

  /**
   * Get system configuration of the specified system.
   * Using only one configuration for all systems
   * @return SystemConfig
   */
  override def getSystemConfig(system: String): Option[SystemConfig] = systemCfg

  /**
   * Get corridor configuration of the specified system
   * @return
   */
  override def getCorridorsConfig(system: String): Option[CorridorsConfig] = None

  override def getMultiLaneCameras: List[MultiLaneCamera] = cameraList

}

object FileLaneConfiguration {
  val formats = DefaultFormats.withHints(
    ShortTypeHints(List(
      classOf[HonacSensorConfigImpl],
      classOf[RadarCameraSensorConfigImpl],
      classOf[DoubleRadarCameraSensorConfigImpl],
      classOf[PirCameraSensorConfigImpl],
      classOf[PirRadarCameraSensorConfigImpl],
      classOf[CameraOnlyConfigImpl])))

  def toJson(config: LaneConfig) = Serialization.write(config)(formats)

}
