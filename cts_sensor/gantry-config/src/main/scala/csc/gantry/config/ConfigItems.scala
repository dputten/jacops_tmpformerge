/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.gantry.config

import scala.concurrent.duration._
import csc.base.SensorDevice

import csc.base.SensorDevice.SensorDevice
import csc.vehicle.message.TriggerArea
import csc.vehicle.message._
import csc.vehicle.message.TriggerArea._
import org.slf4j.LoggerFactory

/**
 * The Configuration of the ZooKeeper implementation of the LaneConfiguration
 * @param zkServers
 * @param sessionTimeout
 * @param connectionTimeout
 */
case class ZookeeperConfig(zkServers: String, sessionTimeout: Int, connectionTimeout: Int)

/**
 * Represent the position of a vertex of an image mask relative to the origin of that image. (Origin is top left corner).
 * @param x x coordinate of vertex (0 .. width-1)
 * @param y y coordinate of vertex (0 .. height-1)
 */
case class ImageMaskVertex(x: Int, y: Int)

/**
 * The Image Mask trait
 * Used to be able to switch between a static and a dynamic implementation
 */
trait ImageMask {
  def vertices: List[ImageMaskVertex]
}

/**
 * Represents an image mask.
 * Vertices define a polygon that will be used as a mask. (= Everything outside this polygon is removed.)
 * The polygon is defined as follows:
 * - For each 0 <= i < (vertices.size - 1): there is a side between vertices[i] and vertices[i+1].
 * - There is a side between vertices[vertices.size - 1] and vertices[0].
 * (The vertices may be given both clockwise and counterclockwise.)
 * @param vertices The vertices of an image mask
 */
case class ImageMaskImpl(vertices: List[ImageMaskVertex]) extends ImageMask

/**
 * Represents optional sequence number and name settings.
 * @param seqNumber sequence number
 * @param seqName sequence name
 */
case class LaneSeqConfig(seqNumber: Option[Int] = None, seqName: Option[String] = None)

/**
 * Base class used to be able to support multiple sensor configurations
 */
trait LaneConfig {
  /**
   * @return The lane information where this configuration is used
   */
  def lane: Lane
  /**
   * @return  The relaySensorData Configuration
   */
  def camera: CameraConfig
  /**
   * @return  The image mask configuration
   */
  def imageMask: Option[ImageMask]
  /**
   * @return The recognition options
   */
  def recognizeOptions: Map[String, String]

  /**
   * @return list of sensors available in this config
   */
  def sensorList: List[SensorDevice] = List(SensorDevice.Camera)

}

object LaneConfig {
  type LaneConfigProvider = Either[String ⇒ LaneConfig, LaneConfig]
}

/**
 * Camera Configuration needed for each Lane
 *
 * @param relayURI path where the data is places for this lane
 * @param cameraHost The host of the camera
 * @param cameraPort The ports of the camera
 * @param maxTriggerRetries The maximum number of retrying to send a camera trigger
 * @param timeDisconnected The disconnected time when a disconnected systemEvent is send
 */
case class CameraConfig(relayURI: String,
                        cameraHost: String,
                        cameraPort: Int,
                        maxTriggerRetries: Int,
                        timeDisconnected: Long) {

  def cameraId: String = relayURI.substring(relayURI.lastIndexOf('/') + 1)

}

case class MultiLaneCamera(config: CameraConfig, system: String, gantry: String, leftLaneId: Option[String], rightLaneId: Option[String]) {

  def laneIdFor(area: TriggerArea) = area match {
    case TriggerArea.LEFT  ⇒ leftLaneId
    case TriggerArea.RIGHT ⇒ rightLaneId
  }

  lazy val id = Camera(config.cameraId, gantry, system)

  def containsLane(laneId: String): Boolean = List(leftLaneId, rightLaneId).flatten.contains(laneId)

}

trait CameraOnlyConfig extends LaneConfig {
  //override def customClassifier: Boolean = true
}

case class Dimension(width: Int, height: Int)

case class CameraOnlyConfigImpl(lane: Lane,
                                camera: CameraConfig,
                                imageMask: Option[ImageMask],
                                recognizeOptions: Map[String, String]) extends CameraOnlyConfig

/**
 * Honac typed Lane configuration
 * @param maxDelayInMillisec maximum delay accepted
 */
case class HonacConfig(maxDelayInMillisec: Int)

//Classes used as a container with additional info for a sensor
/**
 * Configuration for a Radar sensor
 * @param radarURI  The URI to receive the radar messages
 * @param measurementAngle  The Angle of the radar
 * @param height  The heigth of the radar
 * @param surfaceReflection The reflection of the vehicles
 */
case class RadarConfig(
  radarURI: String,
  measurementAngle: Double, //Angle of the radar (degrees) between ground and the radar
  height: Double, //height of the radar (meters)
  surfaceReflection: Double = 1)

/**
 * The configuration needed for a PIR sensor
 * @param pirHost  Host of the Pir
 * @param pirPort  Port of the pir
 * @param pirAddress  Address of the pir range 0 to 255
 * @param refreshPeriod  The duration in msec of polling the vehicle data
 * @param vrHost  Host of the VehicleRegistration where to receive the pir registrations
 * @param vrPort  port of the VehicleRegistration where to receive the pir registrations
 */
case class PIRConfig(pirHost: String, pirPort: Int, pirAddress: Short, refreshPeriod: Long, vrHost: String, vrPort: Int)

/**
 * The configuration interface of a Lane with honac.
 * Used to be able to switch between a static and a dynamic implementation
 */
trait HonacSensorConfig extends LaneConfig

/**
 * The complete configuration of a Lane with honac
 * @param lane The lane information where this configuration is used
 * @param camera The relaySensorData Configuration
 * @param imageMask The image mask configuration
 */
case class HonacSensorConfigImpl(lane: Lane, camera: CameraConfig, imageMask: Option[ImageMask], recognizeOptions: Map[String, String]) extends HonacSensorConfig

/**
 * The configuration of a Lane with Camera and Radar
 * Used to be able to switch between a static and a dynamic implementation
 */
trait RadarCameraSensorConfig extends LaneConfig {
  /**
   * @return The radar configuration
   */
  def radar: RadarConfig

  override def sensorList: List[SensorDevice] = List(SensorDevice.Radar, SensorDevice.Camera)
}
/**
 * The complete configuration of a Lane with Camera and Radar
 * @param lane The lane information where this configuration is used
 * @param camera The relaySensorData Configuration
 * @param imageMask The image mask configuration
 * @param radar The radar configuration
 */
case class RadarCameraSensorConfigImpl(lane: Lane, camera: CameraConfig, imageMask: Option[ImageMask], recognizeOptions: Map[String, String], radar: RadarConfig) extends RadarCameraSensorConfig

/**
 * The configuration of a Lane with Camera and two separated Radars
 * Used to be able to switch between a static and a dynamic implementation
 */
trait DoubleRadarCameraSensorConfig extends LaneConfig {
  /**
   * @return The radar configuration
   */
  def radar: RadarConfig
  /**
   * @return The radar configuration used for the length
   */
  def radarLength: RadarConfig

  override def sensorList: List[SensorDevice] = List(SensorDevice.RadarLength, SensorDevice.Radar, SensorDevice.Camera)
}
/**
 * The complete configuration of a Lane with Camera and two separated Radars
 * @param lane The lane information where this configuration is used
 * @param camera The relaySensorData Configuration
 * @param imageMask The image mask configuration
 * @param radar The radar configuration
 * @param radarLength The radar configuration used for the length
 */
case class DoubleRadarCameraSensorConfigImpl(lane: Lane, camera: CameraConfig, imageMask: Option[ImageMask], recognizeOptions: Map[String, String], radar: RadarConfig, radarLength: RadarConfig) extends DoubleRadarCameraSensorConfig

/**
 * The configuration of a Lane with Camera, Radar and PIR
 * Used to be able to switch between a static and a dynamic implementation
 */
trait PirRadarCameraSensorConfig extends LaneConfig {
  /**
   * @return The radar configuration
   */
  def radar: RadarConfig
  /**
   * @return The pir configuration
   */
  def pir: PIRConfig

  override def sensorList: List[SensorDevice] = List(SensorDevice.PIR, SensorDevice.Radar, SensorDevice.Camera)
}

/**
 * The complete configuration of a Lane with Camera, Radar and PIR
 * @param lane The lane information where this configuration is used
 * @param camera The relaySensorData Configuration
 * @param radar The radar configuration
 * @param pir The PIR configuration
 */
case class PirRadarCameraSensorConfigImpl(lane: Lane, camera: CameraConfig, imageMask: Option[ImageMask], recognizeOptions: Map[String, String], radar: RadarConfig, pir: PIRConfig) extends PirRadarCameraSensorConfig

/**
 * The configuration of a Lane with Camera, Radar and PIR
 * Used to be able to switch between a static and a dynamic implementation
 */
trait PirCameraSensorConfig extends LaneConfig {
  /**
   * @return The PIR configuration
   */
  def pir: PIRConfig

  override def sensorList: List[SensorDevice] = List(SensorDevice.PIR, SensorDevice.Camera)
}

/**
 * The complete configuration of a Lane with Camera, Radar and PIR
 * @param lane The lane information where this configuration is used
 * @param camera The relaySensorData Configuration
 * @param pir The PIR configuration
 */
case class PirCameraSensorConfigImpl(lane: Lane, camera: CameraConfig, imageMask: Option[ImageMask], recognizeOptions: Map[String, String], pir: PIRConfig) extends PirCameraSensorConfig

/**
 * The configuration of a Lane with iRose
 * Used to be able to switch between a static and a dynamic implementation
 */
trait IRoseSensorConfig extends LaneConfig {
  /**
   * @return The relaySensorData configuration
   */
  def seqConfig: LaneSeqConfig
}

/**
 * The complete configuration of a Lane with iRose
 * @param lane The lane information where this configuration is used
 * @param camera The relaySensorData Configuration
 */
case class IRoseSensorConfigImpl(lane: Lane, camera: CameraConfig, imageMask: Option[ImageMask], recognizeOptions: Map[String, String], seqConfig: LaneSeqConfig) extends IRoseSensorConfig

trait SystemConfig {
  private val log = LoggerFactory.getLogger(this.getClass.getName)

  def id: String
  def name: String
  def description: String
  def maxSpeed: Int
  def compressionFactor: Int
  def nrDaysKeepTrafficData: Int
  def nrDaysKeepViolations: Int
  def region: String
  def roadNumber: String
  def roadPart: String
  def typeCertificates: Seq[MeasurementMethodInstallationType]
  def serialNumber: SerialNumber
  def activeChecksums: Seq[ActiveCertificate]

  def typeCertificate(time: Long): String = {
    CertificateService.getNMICertificate(typeCertificates, time)
  }

  def activeSoftwareSeal: String = {
    val now = System.currentTimeMillis()
    log.info("Calculating software seal")
    typeCertificates.sortBy(_.timeFrom).reverse.find(_.timeFrom <= now) match {
      case None ⇒ ""
      case Some(measurementMethodType) ⇒ {
        val activeCertificates = measurementMethodType.measurementType.typeCertificate.components map { (component) ⇒
          log.info("software seal using component <" + component.name + ">")
          activeChecksums.find(_.componentCertificate.name == component.name)
        }
        val seal = CertificateService.calculateSoftwareSeal(activeCertificates.flatten.map(_.componentCertificate))
        log.info("Software seal: <" + seal + ">")
        seal
      }
    }
  }
}

/**
 * The System configuration
 * @param id
 * @param name
 * @param description
 * @param serialNumber
 * @param maxSpeed
 * @param compressionFactor
 * @param nrDaysKeepTrafficData
 * @param nrDaysKeepViolations
 * @param region
 * @param roadNumber
 * @param roadPart
 * @param typeCertificates
 */
case class SystemConfigImpl(id: String,
                            name: String,
                            description: String,
                            maxSpeed: Int,
                            compressionFactor: Int,
                            nrDaysKeepTrafficData: Int,
                            nrDaysKeepViolations: Int,
                            region: String,
                            roadNumber: String,
                            roadPart: String,
                            typeCertificates: Seq[MeasurementMethodInstallationType] = Seq(),
                            serialNumber: SerialNumber,
                            activeChecksums: Seq[ActiveCertificate] = Seq()) extends SystemConfig

/**
 * SerialNumber info
 * @param serialNumber
 * @param serialNumberPrefix
 * @param serialNumberLength
 */
case class SerialNumber(serialNumber: String,
                        serialNumberPrefix: String = "",
                        serialNumberLength: Int = 5)

/**
 * Configs of all available corridors
 * @param corridors Configs of all available corridors
 */
case class CorridorsConfig(corridors: List[CorridorConfig]) {
  // Methods for retrieving specific corridor configurations
  def getSpeedFixedCorridorConfig: Option[CorridorConfig] = corridors.find(_.serviceType == "SpeedFixed")
  def getRedLightCorridorConfig: Option[CorridorConfig] = corridors.find(_.serviceType == "RedLight")
}

/**
 * Config of one corridor (only part of complete config)
 * @param id id of corridor
 * @param name name of corridor
 * @param serviceType serviceType of corridor, like "RedLight" or "FixedSpeed"
 * @param info info dato of corridor
 */
case class CorridorConfig(id: String, name: String, serviceType: String, info: ZkCorridorInfo)
