/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.curator

import org.apache.curator.test.TestingServer
import org.apache.curator.retry.RetryNTimes
import org.apache.curator.framework.{ CuratorFrameworkFactory, CuratorFramework }
import org.scalatest.{ BeforeAndAfterEach, Suite }

/**
 * Zookeeper Test Server using the Curator Framework
 * @author Raymond Roestenburg
 */
trait CuratorTestServer extends BeforeAndAfterEach {
  this: BeforeAndAfterEach with Suite ⇒
  var zkServers: String = _
  protected var testServerScope: Option[TestingServer] = _
  protected var clientScope: Option[CuratorFramework] = _

  override def beforeEach() {
    super.beforeEach()
    testServerScope = createTestServer
    zkServers = testServerScope.get.getConnectString
    clientScope = Some(CuratorFrameworkFactory.newClient(zkServers, new RetryNTimes(1, 1000)))
    clientScope.foreach(_.start())
  }

  def createTestServer: Some[TestingServer] = {
    Some(new TestingServer())
  }

  override def afterEach() {
    try {
      clientScope.foreach(_.close())
    } catch {
      // not interested if it is closed already
      case e: IllegalStateException ⇒
    } finally {
      clientScope = None
      try {
        testServerScope.foreach(_.close())
      } finally {
        testServerScope = None
        super.afterEach()
      }
    }
  }
}

