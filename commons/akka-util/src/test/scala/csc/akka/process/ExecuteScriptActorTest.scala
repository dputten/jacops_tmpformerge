/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.akka.process

import akka.testkit.{ TestProbe, TestKit }
import akka.actor.{ Props, ActorSystem }
import org.scalatest._
import scala.concurrent.duration._

class ExecuteScriptActorTest extends TestKit(ActorSystem("ExecuteScriptActorTest"))
  with WordSpecLike with MustMatchers with BeforeAndAfterAll {

  val actor = system.actorOf(Props[ExecuteScriptActor])

  override def afterAll() {
    super.afterAll()
    system.stop(testActor)
    system.shutdown()
  }

  "ExecuteScriptActor" must {
    "execute script without argument" in {
      val probe = TestProbe()
      val script = ExecuteScript("akka-util/src/test/resources/echoScript.sh", Seq())
      probe.send(actor, script)
      val result = probe.expectMsgType[ExecuteScriptResult](3.seconds)
      result.script must be(script)
      result.resultCode must be(Right(0))
      result.stdOut must be(Seq("akka-util/src/test/resources/echoScript.sh", ""))
      result.stdError must be(Seq())
    }
    "execute script with argument" in {
      val probe = TestProbe()
      val script = ExecuteScript("akka-util/src/test/resources/echoScript.sh", Seq("test"))
      probe.send(actor, script)
      val result = probe.expectMsgType[ExecuteScriptResult](3.seconds)
      result.script must be(script)
      result.resultCode must be(Right(0))
      result.stdOut must be(Seq("akka-util/src/test/resources/echoScript.sh", "test"))
      result.stdError must be(Seq())
    }
    "execute nonexisting script" in {
      val probe = TestProbe()
      val script = ExecuteScript("./script.sh", Seq())
      probe.send(actor, script)
      val result = probe.expectMsgType[ExecuteScriptResult](3.seconds)
      result.script must be(script)
      result.resultCode.isLeft must be(true)
      result.stdOut must be(Seq())
      result.stdError must be(Seq())
    }
  }
}