package csc.akka.state

import java.util.Date

import akka.actor.{ Props, ActorSystem }
import akka.testkit.{ TestProbe, TestKit }
import org.scalatest._

/**
 * Created by carlos on 18/03/16.
 */
class StatePersisterActorTest
  extends TestKit(ActorSystem("StatePersisterActorTest"))
  with WordSpecLike
  with MustMatchers
  with BeforeAndAfterAll {

  import StatePersisterActorTest._

  "StatePersisterActor" must {
    "replay all stored messages and in the same order they were received for the correct actors" in {

      val size = 10

      val actor = system.actorOf(Props(new StatePersisterActor()))
      val probe1 = TestProbe()
      val messages1 = generateList(size)
      val probe2 = TestProbe()
      val messages2 = generateList(size)

      for (i ← 0 until size) {
        probe1.send(actor, BackupState(messages1(i)))
        probe2.send(actor, BackupState(messages2(i)))
      }

      probe2.send(actor, ReplayState)
      checkMessages(probe2, messages2)

      probe1.send(actor, ReplayState)
      checkMessages(probe1, messages1)
    }
  }

  def checkMessages(probe: TestProbe, expected: List[AnyRef]): Unit = expected.foreach { e ⇒ probe.expectMsg(e) }

  override protected def afterAll(): Unit = {
    system.shutdown()
  }
}

object StatePersisterActorTest {

  type Generator = () ⇒ AnyRef

  def generate: AnyRef = {
    val rnd = math.random * generators.size
    generators(rnd.toInt).apply()
  }

  def generateList(size: Int): List[AnyRef] = (for (i ← 0 until size) yield generate).toList

  lazy val generators: List[Generator] = List(
    { () ⇒ CaseClass((math.random * 100000000).toLong) },
    { () ⇒ CaseObject },
    { () ⇒ new Date() },
    { () ⇒ new java.lang.Long(System.currentTimeMillis()) },
    { () ⇒ math.random.toString })

  case class CaseClass(value: Long = System.currentTimeMillis())
  case object CaseObject
}

