package csc.akka.remote

import org.scalatest.WordSpec
import org.scalatest._

/**
 * Created by carlos on 23/05/16.
 */
class SocketMessageSenderTest extends WordSpec with MustMatchers {

  val uri1 = """mina:tcp://localhost:12500?textline=true&sync=false"""
  val uri2 = """localhost:12501"""

  "SockerServer obj" must {

    "parse correctly a SockerServer from an uri with protocol" in {
      val server = SocketServer(uri1)
      server must be(Some(SocketServer("localhost", 12500)))
    }
    "parse correctly a SockerServer from a simple host:port uri" in {
      val server = SocketServer(uri2)
      server must be(Some(SocketServer("localhost", 12501)))
    }
  }

}
