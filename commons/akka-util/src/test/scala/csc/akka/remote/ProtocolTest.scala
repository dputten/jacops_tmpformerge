/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.akka.remote

import org.scalatest._
import csc.akka.remote.wire.Protocol
import scala.util.Random

class ProtocolTest extends Protocol with WordSpecLike with MustMatchers {
  val msg10 = "1234567890" * 1000
  val msg20 = msg10 * 2

  "Protocol" must {
    "correctly fragment and defragment messages" in {
      val start = System.currentTimeMillis()
      for (idx ← 0 to 1000) {
        val buf1 = makeRandomBytes(500)
        val fragments1 = makeFragments(buf1, 200)
        val buf2 = makeRandomBytes(500)
        val fragments2 = makeFragments(buf2, 200)

        val allFragments = merge(fragments1, fragments2)

        val result: Seq[Array[Byte]] = allFragments.map(s ⇒ addFragment(s.toString)).flatMap((s) ⇒ s)
        result.size must be(2)
        val success =
          java.util.Arrays.equals(result(0), buf1) && java.util.Arrays.equals(result(1), buf2) ||
            java.util.Arrays.equals(result(0), buf2) && java.util.Arrays.equals(result(1), buf1)
        success must be(true)
      }
      val duration_ms = System.currentTimeMillis() - start
      println("Duration: " + duration_ms + " ms")
    }
  }

  private def merge(a: Seq[Any], b: Seq[Any]): Seq[Any] = {
    def doMerge(a: Seq[Any], b: Seq[Any], accu: Seq[Any]): Seq[Any] = {
      (a, b) match {
        case (Nil, Nil)    ⇒ accu.reverse
        case (Nil, list_b) ⇒ (list_b.reverse ++ accu).reverse
        case (list_a, Nil) ⇒ (list_a.reverse ++ accu).reverse
        case (a_head :: a_tail, b_head :: b_tail) ⇒ {
          val newAccu = Seq(a_head, b_head) ++ accu
          doMerge(a_tail, b_tail, newAccu)
        }
      }
    }

    doMerge(a, b, Seq())
  }

  private def makeRandomBytes(maxSize: Int): Array[Byte] = {
    val rand = new Random(System.currentTimeMillis())
    val size = rand.nextInt(maxSize)
    val buf = new Array[Byte](size)
    rand.nextBytes(buf)
    buf
  }
}
