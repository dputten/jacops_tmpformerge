/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.akka.remote

import akka.testkit.{ TestProbe, TestKit }
import akka.actor.{ Props, ActorSystem }
import csc.akka.CamelClient
import org.scalatest._
import scala.concurrent.duration._

case class TestMessage(name: String, opt: Option[Int], obj: AnyRef)

class SerializerTest extends TestKit(ActorSystem("SerializerTest"))
  with WordSpecLike with MustMatchers with BeforeAndAfterAll with CamelClient {

  override def afterAll() {
    super.afterAll()
    system.shutdown()
  }

  "Serializers" must {
    "transfer objects" in {
      val prodURI = "mina:tcp://localhost:12555?textline=true&sync=false"
      val consURI = "mina:tcp://localhost:12555?textline=true&sync=false"
      val probe = TestProbe()
      val prod = system.actorOf(Props(new SerializerProducer(prodURI, 120)), "producer1")
      val cons = system.actorOf(Props(new SerializerConsumer(consURI, Set(probe.ref))), "consumer1")
      awaitActivation(cons, 100 seconds)

      val msg = TestMessage("root", Some(12), TestMessage("child", None, "bla"))
      prod ! msg
      probe.expectMsg(10.seconds, msg)
      system.stop(prod)
      system.stop(cons)
    }

    "transfer large objects" in {
      //      val prodURI = "mina:tcp://localhost:12555?textline=true&sync=false"
      val prodURI = "tcp://localhost:12556"
      val consURI = "mina:tcp://localhost:12556?textline=true&sync=false"
      val probe = TestProbe()
      val prod = system.actorOf(Props(new SerializerProducer2(prodURI, 120)), "producer2")
      val cons = system.actorOf(Props(new SerializerConsumer(consURI, Set(probe.ref))), "consumer2")
      awaitActivation(cons, 10 seconds)

      val msg = TestMessage("root-------------------------------------------------------", Some(12),
        TestMessage("child--------------------------------------------------------------", None,
          TestMessage("child-1------------------------------------------------------------", None,
            TestMessage("child-2------------------------------------------------------------", None,
              TestMessage("child-3------------------------------------------------------------", None,
                TestMessage("child-4------------------------------------------------------------", None, "bla"))))))

      prod ! msg

      probe.expectMsg(10.seconds, msg)
      system.stop(prod)
      system.stop(cons)
    }
  }

}