/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.akka.remote

import java.net.Socket
import csc.akka.logging.DirectLogging
import java.io.{ IOException, PrintWriter }
import java.util.Date
import collection.mutable.ListBuffer
import scala.concurrent.duration.Duration

/**
 * This class sends Text over a TCP connection separated by newlines.
 * When the connection can't be made the message is buffered until the
 * connection can be created. The order is kept when the text's is buffered.
 * After a successfully transmission the connection is closed. This is done to detect
 * when sending the text has failed.
 * @param hostName the hostName where the text has to be send to
 * @param hostPort the port number where the receiver listen
 */
class TextBasedTCPConnection(hostName: String, hostPort: Int, ttl: Duration) extends DirectLogging {
  private val queue = new ListBuffer[Tuple2[Date, String]]()

  def idle(): Unit = {
    sendText("") // Only processes elements in the queue
  }

  /**
   * Try to send the text
   * @param text the text to be send
   */
  def sendText(text: String): Unit = {
    sendText(text, new Date())
  }

  def sendText(text: String, now: Date): Unit = {
    var socket: Socket = null
    var writer: PrintWriter = null
    if (log.isDebugEnabled) {
      log.debug("Connect to%s:%d ".format(hostName, hostPort))
    }
    try {
      socket = new Socket(hostName, hostPort)
      socket.setKeepAlive(true)
      writer = new PrintWriter(socket.getOutputStream, false)
      if (text.length > 0) queue += {
        (now, text)
      }
      queue.foreach {
        case (time, text) ⇒ {
          //send only the new messages
          if (time.getTime + ttl.toMillis >= now.getTime) {
            writer.println(text.toString)
          }
        }
      }
      writer.flush()
      queue.clear()
    } catch {
      case ex: Exception ⇒ {
        log.error("ConnectToHost failed %s".format(ex.getMessage))
        log.debug("Cleanup queue")
        queue.dropWhile {
          case (time, str) ⇒ time.getTime + ttl.toMillis < now.getTime
        }
      }
    } finally {
      cleanUpSession(socket, writer)
    }
  }

  /**
   * Cleanup session
   * @param socket The socket used
   * @param writer The writer used
   */
  def cleanUpSession(socket: Socket, writer: PrintWriter): Unit = {
    log.debug("CleanUp connection")
    try {
      if (writer != null) {
        writer.flush()
        writer.close()
      }
    } finally {
      if (socket != null) {
        try {
          socket.close()
        } catch {
          case e: IOException ⇒ {
            log.warning("Socket close failed  %s".format(e.getMessage))
          }
        }
      }
    }
  }
}