/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.akka.remote

import akka.camel.Producer
import akka.actor.{ ActorLogging, Actor }
import akka.serialization.SerializationExtension
import csc.akka.remote.wire.{ Protocol, MessageFragmenter }

class SerializerProducer(uri: String, maxMessageLength: Int) extends Actor with Producer
  with Protocol with ActorLogging {

  def endpointUri = uri

  override def oneway = true
  val serialization = SerializationExtension.get(context.system)
  val serializer = serialization.findSerializerFor(RemoteMessage("", None, ""))

  override protected def transformOutgoingMessage(msg: Any) = {
    msg match {
      case message: AnyRef ⇒ {
        // Turn it into bytes
        try {
          makeFragments(serializer.toBinary(message), maxMessageLength).mkString("\n")
        } catch {
          case ex: Exception ⇒ {
            log.error(ex, "Could not serialize %s: %s".format(message.getClass.getName, message.toString))
            ""
          }
        }
      }
      case other ⇒ {
        log.error("Unexpected message received: '%s'".format(msg.toString()))
        ""
      }
    }
  }
}