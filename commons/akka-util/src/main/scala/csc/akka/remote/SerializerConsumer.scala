/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.akka.remote

import akka.actor.{ ActorRef, ActorLogging, Actor }
import akka.camel.{ CamelMessage, Consumer }
import akka.event.LoggingReceive
import akka.serialization.SerializationExtension
import csc.akka.remote.wire.Protocol

class SerializerConsumer(uri: String, recipients: Set[ActorRef]) extends Actor with Consumer
  with Protocol with ActorLogging {
  def endpointUri = uri
  val serialization = SerializationExtension.get(context.system)
  val serializer = serialization.findSerializerFor(RemoteMessage("", None, ""))

  def receive = LoggingReceive {
    case msg: CamelMessage ⇒ {
      val data = msg.bodyAs[String]
      if (data.trim.isEmpty) {
        log.info("Received an empty line")
      } else {
        addFragment(data) foreach (messageData ⇒ {
          try {
            val receivedObject = serializer.fromBinary(messageData)
            log.debug("Received message %s: %s".format(receivedObject.getClass.getName, receivedObject))
            recipients.foreach(_ ! receivedObject)
          } catch {
            case ex: Exception ⇒ log.error(ex, "Error deserializing message")
          }
        })
      }
    }
  }
}