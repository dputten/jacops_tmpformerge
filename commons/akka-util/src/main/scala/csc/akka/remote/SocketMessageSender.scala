package csc.akka.remote

import java.io.{ IOException, StringWriter, PrintWriter }
import java.net.Socket

import akka.event.LoggingAdapter

/**
 * Created by carlos on 28/04/16.
 */
trait SocketMessageSender {

  def server: SocketServer
  def log: LoggingAdapter

  private var queue: Option[StringWriter] = None

  def sendNotification(payload: String) {

    var socket: Socket = null
    var writer: PrintWriter = null
    log.debug("Connect to {}: {}", server.host, server.port)
    try {
      socket = new Socket(server.host, server.port)
      socket.setKeepAlive(true)
      writer = new PrintWriter(socket.getOutputStream, false)

      queue foreach { q ⇒
        writer.write(q.toString)
        writer.flush()
        queue = None
      }

      writer.println(payload)
    } catch {
      case ex: Exception ⇒ {
        log.error(ex, "ConnectToHost failed")
        if (queue == null) {
          queue = Some(new StringWriter(512))
        }

        queue.foreach { q ⇒
          log.debug("Use queue")
          q.write(payload)
          q.write("\n")
        }
      }
    } finally {
      cleanUpSession(socket, writer)
    }
  }

  def cleanUpSession(socket: Socket, writer: PrintWriter) {
    log.debug("CleanUp connection")
    try {
      if (writer != null) {
        writer.flush
        writer.close
      }
    } finally {
      if (socket != null) {
        try {
          socket.close
        } catch {
          case e: IOException ⇒ {
            log.warning("Socket close failed ", e)
          }
        }
      }
    }
  }
}

case class SocketServer(host: String, port: Int)

object SocketServer {

  val uriExpression1 = """(.+)://(.+):(\d+).*""".r
  val uriExpression2 = """(.+):(\d+)""".r

  def apply(uri: String): Option[SocketServer] = uri match {
    case uriExpression1(_, host, port) ⇒ Some(SocketServer(host, port.toInt))
    case uriExpression2(host, port)    ⇒ Some(SocketServer(host, port.toInt))
    case _                             ⇒ None
  }
}
