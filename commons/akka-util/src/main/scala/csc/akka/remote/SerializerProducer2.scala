/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.akka.remote

import akka.actor.{ ActorLogging, Actor }
import akka.serialization.SerializationExtension
import scala.concurrent.duration._
import java.net.URI
import csc.akka.remote.wire.Protocol

private case class SendLine(message: String)
private case class Tick()

class SerializerProducer2(uri: String, maxMessageLength: Int, tickInterval: FiniteDuration = 60.seconds) extends Actor
  with Protocol with ActorLogging {

  implicit val executor = context.system.dispatcher

  val serialization = SerializationExtension.get(context.system)
  val serializer = serialization.findSerializerFor(RemoteMessage("", None, ""))
  val connection = {
    val endpoint = new URI(uri)
    new TextBasedTCPConnection(endpoint.getHost, endpoint.getPort, 5.days)
  }
  context.system.scheduler.scheduleOnce(tickInterval, self, Tick)

  def receive = {
    case Tick ⇒ {
      connection.idle()
      context.system.scheduler.scheduleOnce(tickInterval, self, Tick)
    }

    case SendLine(line) ⇒ {
      connection.sendText(line)
      Thread.sleep(100)
    }

    case message: AnyRef ⇒ {
      // Turn it into bytes
      try {
        val msgLines = makeFragments(serializer.toBinary(message), maxMessageLength)
        log.debug("Start sending: {}", msgLines.head)
        for (line ← msgLines) {
          self ! SendLine(line)
        }
      } catch {
        case ex: Exception ⇒ {
          log.error(ex, "Could not serialize %s: %s".format(message.getClass.getName, message.toString))
        }
      }
    }
  }
}