/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.akka.remote

case class RemoteMessage(actorPathSender: String, replyTo: Option[String], message: AnyRef)