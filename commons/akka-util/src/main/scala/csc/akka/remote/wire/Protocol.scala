package csc.akka.remote.wire

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 2/25/15.
 */

import org.slf4j.LoggerFactory
import scala.collection.mutable
import sun.misc.{ BASE64Encoder, BASE64Decoder }

trait Protocol {
  val defragger = new MessageDefragmenter

  def makeFragments(bytes: Array[Byte], maxLineLength: Int): Seq[String] = MessageFragmenter.makeFragments(bytes, maxLineLength)
  def addFragment(message: String): Option[Array[Byte]] = defragger.addFragment(message)
}

/*
  Never, ever touch my private parts.
  Der Maschinemachercontrol ist nicht fuer gefingerpoken und mittengraben!!!
 */

protected object MessageFragmenter extends GZIPCompressor {
  val CHUNKPREFIXFORMAT = "Chunk %s %d/%d:%s"
  val encoder = new BASE64Encoder

  def makeFragments(bytes: Array[Byte], maxLineLength: Int): Seq[String] = {
    makeFragments(encoder.encode(compress(bytes)).replace("\n", ""), maxLineLength)
  }

  protected def makeFragments(message: String, maxLineLength: Int): Seq[String] = {
    val uuidString = java.util.UUID.randomUUID.toString
    val chunkList = message.grouped(maxLineLength).toList
    val size = chunkList.size
    var nr = 0
    chunkList.map {
      (s: String) ⇒
        {
          nr += 1
          CHUNKPREFIXFORMAT.format(uuidString, nr, size, s)
        }
    }
  }
}

protected object MessageDefragmenter {
  val uuidRegexStr = "[0-9a-zA-Z\\-]+"
  val ChunkRegex = ("Chunk (" + uuidRegexStr + ") (\\d+)/(\\d+)" + ":(.*)").r
}

protected class MessageDefragmenter extends GZIPCompressor {

  import MessageDefragmenter._

  val log = LoggerFactory.getLogger(getClass)

  def addFragment(message: String): Option[Array[Byte]] = {
    message match {
      case ChunkRegex(uuid, count, total, chunk) ⇒ addChunk(uuid, count.toInt, total.toInt, chunk)
      case other ⇒ {
        log.error("Message format unknown: %s".format(message))
        None
      }
    }
  }

  protected val messageMap = new mutable.HashMap[String, ChunkRegistration]
  protected val decoder = new BASE64Decoder

  protected def addChunk(uuid: String, count: Int, total: Int, chunk: String): Option[Array[Byte]] = {
    messageMap.get(uuid) match {
      case Some(chunkRegistration) ⇒ {

        if (total != chunkRegistration.toReceive) {
          //the totals in different messages are not equal (should never happen)
          log.error("Received different total (%d expected %d) for chunknr %d for uuid %s".format(total, chunkRegistration.toReceive, count, uuid))
        }

        val newChunks = chunkRegistration.chunks :+ Chunk(count, chunk)
        val duration = System.currentTimeMillis() - chunkRegistration.startTime
        log.debug("Received %d/%d for uuid %s, duration: %d ms".format(count, chunkRegistration.toReceive, uuid, duration))

        if (chunkRegistration.toReceive == newChunks.size) {
          messageMap.remove(uuid)
          val msg = newChunks.sortBy(_.nr).map(_.dataMsg).mkString
          log.debug("Deliver uuid %s, duration: %d ms".format(uuid, duration))
          deliver(msg)
        } else {
          val newRegistration = chunkRegistration.copy(chunks = newChunks)
          messageMap(uuid) = newRegistration
          None
        }
      }
      case None ⇒ {
        //first message
        if (total == 1) {
          log.debug("Deliver uuid %s".format(uuid))
          deliver(chunk)
        } else {
          // add uuid to messageMap
          messageMap(uuid) = ChunkRegistration(total, chunks = Seq(Chunk(count, chunk)))
          log.debug("Start receiving %d chunks for uuid %s. %d transfers in progress".format(total, uuid, messageMap.size))
          None
        }
      }
    }
  }

  protected def deliver(msg: String): Option[Array[Byte]] = {
    Some(decompress(decoder.decodeBuffer(msg)))
  }
}

protected case class Chunk(nr: Int, dataMsg: String)
protected case class ChunkRegistration(toReceive: Int, chunks: Seq[Chunk],
                                       startTime: Long = System.currentTimeMillis())
