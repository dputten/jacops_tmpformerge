/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.akka.process

import akka.actor.{ ActorLogging, Actor }
import java.io.{ InputStreamReader, BufferedReader }
import akka.event.LoggingAdapter

import collection.mutable.ListBuffer
import scala.concurrent.Future

case class ExecuteScript(script: String, arguments: Seq[String])

case class ExecuteScriptResult(script: ExecuteScript, resultCode: Either[Throwable, Int], stdOut: Seq[String], stdError: Seq[String])

class ExecuteScriptActor extends Actor with ActorLogging {
  implicit val exec = context.dispatcher

  def receive = {
    case script: ExecuteScript ⇒ {
      val replyTo = sender
      Future {
        replyTo ! ScriptExecutor.executeScript(script, log)
      }
    }
  }
}

object ScriptExecutor {

  def executeScript(script: ExecuteScript, log: LoggingAdapter): ExecuteScriptResult = {
    val list = (script.script +: script.arguments).toArray
    try {
      val process = Runtime.getRuntime.exec(list)
      val result = process.waitFor()
      if (result != 0) {
        log.warning("Process [%s] returned %d".format(script.script, result))
      }
      val outputResults = new BufferedReader(new InputStreamReader(process.getInputStream))
      val output = readAllFromStream(outputResults)
      val errorResults = new BufferedReader(new InputStreamReader(process.getErrorStream))
      val error = readAllFromStream(errorResults)

      try {
        outputResults.close()
        process.getInputStream.close()
      }
      try {
        errorResults.close()
        process.getErrorStream.close()
      }
      new ExecuteScriptResult(script = script, resultCode = Right(result), stdOut = output, stdError = error)
    } catch {
      case t: Throwable ⇒ {
        new ExecuteScriptResult(script = script, resultCode = Left(t), stdOut = Seq(), stdError = Seq())
      }
    }
  }

  private def readAllFromStream(rd: BufferedReader): Seq[String] = {
    val output = new ListBuffer[String]
    var line = rd.readLine()
    while (line != null) {
      output += line
      line = rd.readLine()
    }
    output
  }

}