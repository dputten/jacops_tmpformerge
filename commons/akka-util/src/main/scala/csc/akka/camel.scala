package csc.akka

import akka.actor.{ ActorSystem, ActorRef }
import akka.camel.CamelExtension
import akka.util.Timeout

import scala.concurrent.{ Future, Await }

trait CamelClient {

  def system: ActorSystem

  def awaitActivation(actor: ActorRef, to: Timeout): Unit = {
    //TODO csilva: check if this works
    val future = activationFutureFor(actor, to)
    Await.ready(future, to.duration)
  }

  def activationFutureFor(actor: ActorRef, to: Timeout): Future[ActorRef] =
    CamelExtension(system).activationFutureFor(actor)(to, system.dispatcher)

  def awaitDeactivation(actor: ActorRef, to: Timeout): Unit = {
    //TODO csilva: check if this works
    val future = CamelExtension(system).deactivationFutureFor(actor)(to, system.dispatcher)
    Await.ready(future, to.duration)
  }

}