/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.akka.config

import java.util.concurrent.TimeUnit

import collection.JavaConversions._
import com.typesafe.config._
import scala.concurrent.duration.{ FiniteDuration, Duration }
import scala.reflect.ClassTag
import scala.util.matching.Regex

/**
 * Support class to get values and be able to replace values with other values defined in the
 * configuration file, system properties of environment variables
 * @param config The configuration
 */
class ConfigUtil(config: Config) {
  def getReplacedString(key: String): String = {
    ConfigUtil.getReplacedString(config, key).get
  }

  def getReplacedString(key: String, default: String): String = {
    ConfigUtil.getReplacedString(config, key).getOrElse(default)
  }

  def getReplacedInteger(key: String): Int = {
    getReplacedString(key).toInt
  }

  def getReplacedInteger(key: String, default: Int): Int = {
    getReplacedString(key, default.toString).toInt
  }

  def getReplacedLong(key: String, default: Long): Long = {
    getReplacedString(key, default.toString).toLong
  }

  def getReplacedDouble(key: String): Double = {
    getReplacedString(key).toDouble
  }

  def getReplacedDouble(key: String, default: Double): Double = {
    getReplacedString(key, default.toString).toDouble
  }

  def getReplacedBoolean(key: String, default: Boolean = false): Boolean =
    getBoolean(getReplacedString(key, default.toString), default)

  val trueValues = List("true", "yes", "on")
  val falseValues = List("false", "no", "off")

  def getBoolean(str: String, fallback: Boolean): Boolean = {
    if (trueValues.contains(str)) true
    else if (falseValues.contains(str)) false
    else fallback
  }

  def getReplacedMilliseconds(key: String): Long = {
    val value = getReplacedString(key)
    parseMilliseconds(value)
  }

  def getReplacedMilliseconds(key: String, default: Long): Long = {
    val value = getReplacedString(key, default.toString)
    parseMilliseconds(value)
  }

  def getConfig(name: String): Option[Config] = configOpt(name)

  def configOpt(name: String): Option[Config] = {
    if (config.hasPath(name)) {
      Some(config.getConfig(name))
    } else None
  }

  private def parseMilliseconds(value: String): Long = {
    val config = ConfigFactory.parseString("duration = " + value)
    config.getDuration("duration", TimeUnit.MILLISECONDS)
  }

  def getFiniteDuration(key: String): FiniteDuration = optFiniteDuration(key).get

  def optFiniteDuration(key: String): Option[FiniteDuration] = getStringOpt(key) map { s ⇒
    val duration = Duration(s)
    FiniteDuration(duration.toMillis, TimeUnit.MILLISECONDS)
  }

  def getKeySet(): Set[String] = {
    val keys = config.entrySet().toSet map {
      k: java.util.Map.Entry[String, ConfigValue] ⇒ k.getKey.split('.').head
    }
    keys.toSet
  }

  private def configOf(value: ConfigValue): Option[Config] = value match {
    case o: ConfigObject ⇒ Some(o.toConfig)
    case _               ⇒ None
  }

  def configMap: Map[String, Config] = {
    val list = for (key ← getKeySet(); cfg ← configOf(config.getValue(key))) yield (key, cfg)
    list.toMap
  }

  def opt(key: String): Option[ConfigValue] = if (config.hasPath(key)) Some(config.getValue(key)) else None

  def list[T](key: String)(implicit tag: ClassTag[T]): List[T] = opt(key) match {
    case Some(list: ConfigList) ⇒ list.unwrapped().toList map (_.asInstanceOf[T])
    case _                      ⇒ Nil
  }

  def getStringOpt(key: String): Option[String] = {
    if (config.hasPath(key))
      Some(config.getString(key))
    else
      None
  }

  def getStringListOpt(key: String): Option[List[String]] = {
    if (config.hasPath(key))
      Some(config.getStringList(key).toList)
    else
      None
  }

  def getIntOpt(key: String): Option[Int] = {
    if (config.hasPath(key))
      Some(config.getInt(key))
    else
      None
  }

  def getLongOpt(key: String): Option[Long] = {
    if (config.hasPath(key))
      Some(config.getLong(key))
    else
      None
  }

  override def toString: String = "ConfigUtil(%s)".format(config)
}

/**
 * Support replacing values
 */
object ConfigUtil {
  implicit def configUtil(config: Config) = new ConfigUtil(config)

  def apply(config: Config) = new ConfigUtil(config)

  val pattern = """\$\{([a-zA-Z0-9._-]+)\}""".r

  /**
   * Returns the value for the key, if the value refers to other keys it will replace thee keys with the values for those keys,
   * if the key could be found in the config, the key itself will be returned
   */
  def getReplacedString(conf: Config, key: String): Option[String] = {
    def getReplacedValue(m: Regex.Match): String = {
      // get the value of the regex match group, check to see it this refers to an element in the config
      // create a global key based on the key being replaced. (is there a key next to it with this name,
      // or is it a global key)
      val newkey = m.group(1)
      val firstPart = key.split('.').init.mkString(".")
      var globalKey = newkey
      if (!firstPart.isEmpty) {
        globalKey = firstPart + "." + newkey
      }
      getReplacedString(conf.root().toConfig, globalKey)
        .orElse(Option(System.getenv(globalKey))).orElse(Option(System.getenv(newkey)))
        .orElse(Option(System.getProperty(globalKey))).orElse(Option(System.getProperty(newkey)))
        .orElse(getReplacedString(conf, newkey)).getOrElse(newkey)
    }
    try {
      val value = conf.getString(key)
      val replaced = pattern.replaceAllIn(value, m ⇒ getReplacedValue(m))
      return Some(replaced)
    } catch {
      case ex: ConfigException ⇒ {
        return None
      }
    }

  }

}

case class ConfigWrapper(config: Config) {

  def string(key: String): String = config.getString(key)

  def stringOpt(key: String): Option[String] = {
    if (config.hasPath(key))
      Some(string(key))
    else
      None
  }
}