package csc.akka.state

import akka.actor.{ ActorRef, ActorPath, ActorLogging, Actor }

/**
 * Created by carlos on 18/03/16.
 */
class StatePersisterActor extends Actor with ActorLogging {

  var stateMap: Map[ActorPath, List[Any]] = Map() //keeping state as a field, so it is accessible in a heap dump

  override def receive: Receive = {
    case BackupState(msg) ⇒ backup(sender, msg)
    case ReplayState      ⇒ replay(sender)
    case other            ⇒ log.info("Don' know how to handle {}", other)
  }

  private def backup(sender: ActorRef, msg: Any): Unit = {
    val path = sender.path
    stateMap = stateMap.updated(path, stateFor(path) :+ msg)
  }

  private def stateFor(path: ActorPath): List[Any] = stateMap.get(path) match {
    case None       ⇒ Nil
    case Some(list) ⇒ list
  }

  private def replay(sender: ActorRef): Unit = {
    val path = sender.path
    stateFor(path) match {
      case Nil ⇒ log.warning("No state message were found for actor with path {}", path)
      case list ⇒
        println("For " + path + " replaying " + list)
        list.foreach { sender ! _ }
    }
  }

}

case class BackupState(msg: AnyRef)
case object ReplayState
