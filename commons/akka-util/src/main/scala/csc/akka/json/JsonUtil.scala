package csc.akka.json

/**
 * Utility to obtain optional elements from a JSON structure.
 * The path can have dots (.) separating the elements in the structure
 *
 * Created by carlos on 26/08/16.
 */
class JsonUtil(root: Map[_, _]) {

  def stringOpt(path: String): Option[String] = opt(path) map (_.toString)

  def doubleOpt(path: String): Option[Double] = stringOpt(path) map (_.toDouble)

  def longOpt(path: String): Option[Long] = doubleOpt(path) map (_.toLong)

  def opt(path: String): Option[Any] = get(root, path.split("\\.").toList)

  def get(obj: Any, path: List[String]): Option[Any] = path match {
    case Nil ⇒ Some(obj)
    case head :: tail ⇒ obj match {
      case map: Map[Any, _] ⇒ map.get(head) match {
        case Some(value) ⇒ get(value, tail)
        case None        ⇒ None
      }
      case other ⇒ None
    }
  }

}
