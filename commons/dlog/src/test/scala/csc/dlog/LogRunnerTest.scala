package csc.dlog

/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

import hbase.EventLog
import org.scalatest.MustMatchers
import csc.json.lift.{ ByteArraySerialization, LiftSerialization }
import net.liftweb.json.DefaultFormats
import org.scalatest.{ WordSpec }
import csc.config.Path
import org.apache.zookeeper.CreateMode
import org.apache.curator.framework.CuratorFramework
import csc.curator.CuratorTestServer
import org.apache.hadoop.hbase.client.{ Delete, Scan, HTable }
import org.apache.hadoop.hbase.HBaseConfiguration

/**
 * Test for LogRunner
 * @author Raymond Roestenburg
 */
class LogRunnerTest extends WordSpec with MustMatchers with CuratorTestServer {
  private var serialization: ByteArraySerialization = _
  val secret = "secret"
  val queuePath = "/queue"
  val simpleQueuePath = "/squeue"
  val events = List.fill[SomeEvent](100)(SomeEvent("testing", "Info", System.currentTimeMillis()))

  private lazy val hbaseConfig = {
    val config = HBaseConfiguration.create()
    config.set("hbase.curator.quorum", zkServers)
    config
  }

  private var logTable: HTable = _
  val logTableName = "EventLogTest"

  override def beforeEach() = {
    super.beforeEach()
    Thread.sleep(1000)
    serialization = new LiftSerialization {
      implicit def formats = DefaultFormats
    }
    logTable = new HTable(hbaseConfig, logTableName)
    truncate(logTable)
  }

  def createEventLogAppender(logPath: Path): EventLogAppender = {
    EventLog.createLocalAppender(logTable, keyPrefix = "A2", serialization)
  }

  // Actual tests are disabled to avoid pulling Hbase testing infrastructure into the project
  // Tests can be run when local hbase and curator servers are running and hbase has a table
  // called "EventLogTest" with a column family "log"
  if (false) {
    "The LogRunner" must {
      "take leadership and listen to a simple distributed queue and write to one EventLog" in {
        for (client ← clientScope) {
          val qTaker = new SimpleDistributedQueueTaker[SomeEvent](client, simpleQueuePath, serialization)
          doListenTest(events, Path("/ctes/slog"), qTaker, client, e ⇒ client.create().withMode(CreateMode.PERSISTENT_SEQUENTIAL).forPath(simpleQueuePath + "/qn-", serialization.serialize(e)), 0, 100)
        }
      }

      "take leadership and listen to a distributed queue and write to one EventLog" in {
        for (client ← clientScope) {
          val qTaker = new DistributedQueueTaker[SomeEvent](client, queuePath, 1000, serialization)
          def put(e: Event) = {
            //DistributedQueue has an ItemSerializer which uses it's own format

            val bytes = serialization.serialize(e)
            val size = bytes.size
            //size in bytes
            def intToBytes(i: Int) = Array[Byte]((i >>> 24).toByte, (i >> 16 & 0xff).toByte, (i >> 8 & 0xff).toByte, (i & 0xff).toByte)

            val bsize = intToBytes(size).toList
            val version = (0x00: Byte) :: (0x01: Byte) :: (0x00: Byte) :: (0x01: Byte) :: Nil
            val item_opcode = (0x01: Byte) :: Nil
            val element_bytes = bytes.toList
            val eof_code = (0x02: Byte) :: Nil

            //[VERSION(0x00010001),SIZE_OF_ELEMENT,ITEM_OPCODE, element_bytes,EOF_OPCODE]
            val dQElem = version ::: item_opcode ::: bsize ::: element_bytes ::: eof_code
            client.create().withMode(CreateMode.PERSISTENT_SEQUENTIAL).forPath(queuePath + "/queue-", dQElem.toArray[Byte])
          }
          doListenTest(events, Path("/ctes/dlog"), qTaker, client, e ⇒ put(e), 0, 100)
        }
      }
      "not log CloseLog events" in {
        for (client ← clientScope) {
          val qTaker = new SimpleDistributedQueueTaker[SomeEvent](client, simpleQueuePath, serialization)
          val oneEvent = new SomeEvent("testing", "Info", System.currentTimeMillis())
          val closeEvent = new SomeEvent("close", "CloseLog", System.currentTimeMillis())
          doListenTest(List(oneEvent, closeEvent), Path("/ctes/slog"), qTaker, client,
            e ⇒ client.create().withMode(CreateMode.PERSISTENT_SEQUENTIAL).forPath(simpleQueuePath + "/qn-", serialization.serialize(e)), 0, 1)
        }
      }

    }
  }

  def doListenTest(events: List[Event], logPath: Path, qTaker: QueueTaker[SomeEvent], client: CuratorFramework, fPut: (Event ⇒ Unit), start: Int, expectedAmount: Int): Int = {
    val logrunner = new LogRunner[SomeEvent](createEventLogAppender(logPath), qTaker, "thishost", client, "/mutex")
    try {
      logrunner.start()
      events.foreach {
        e ⇒ fPut(e)
      }
      val timeout = 20000
      def now = System.currentTimeMillis()
      val start = now
      while (logrunner.logged < expectedAmount && (now - start < timeout)) {
        Thread.sleep(1000)
      }
    } finally {
      logrunner.stop()
    }
    // check that events were written
    val reader = EventLog.createReader(logTable, keyPrefix = "A2", serialization)
    val entries = reader.iterator[SomeEvent].toList
    entries.size must be(expectedAmount)

    entries.foldLeft(start) {
      (acc, e) ⇒
        e.event.name must be("testing")
        e.logEntryId must be(acc)
        acc + 1
    }
  }

  def truncate(table: HTable) {
    import collection.JavaConversions._

    val scan = new Scan()
    val scanner = table.getScanner(scan)
    try {
      scanner.foreach { r ⇒
        val delete = new Delete(r.getRow)
        table.delete(delete)
      }
    } finally {
      scanner.close()
      table.flushCommits()
    }
  }

}

case class SomeEvent(name: String, eventType: String, timestamp: Long) extends Event
