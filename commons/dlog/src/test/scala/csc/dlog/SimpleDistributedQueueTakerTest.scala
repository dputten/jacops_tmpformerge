/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.dlog

import net.liftweb.json.DefaultFormats
import csc.json.lift.LiftSerialization
import org.scalatest.WordSpec
import org.scalatest.MustMatchers
import csc.curator.CuratorTestServer

class SimpleDistributedQueueTakerTest extends WordSpec with MustMatchers with CuratorTestServer {
  val simpleQueuePath = "/squeue"
  val defaultLiftSerialization = new LiftSerialization {
    implicit def formats = DefaultFormats
  }

  "The SimpleDistributedQueueTaker" must {
    "listen to a queue" in {
      for (client ← clientScope) {
        val qTaker = new SimpleDistributedQueueTaker[QueueEvent](client, simpleQueuePath, defaultLiftSerialization)

        //add event
        val qProd = new SimpleDistributedQueueProducer[QueueEvent](client, simpleQueuePath, defaultLiftSerialization)
        val now = System.currentTimeMillis()

        val msg1 = new QueueEvent(eventType = "test", timestamp = now)
        qProd.put(msg1)
        val msg2 = new QueueEvent(eventType = "test1", timestamp = now + 100)
        qProd.put(msg2)
        val msg3 = new QueueEvent(eventType = "test2", timestamp = now + 200)
        qProd.put(msg3)

        //read events
        val recv1 = qTaker.take()
        recv1.get must be(msg1)
        val recv2 = qTaker.take()
        recv2.get must be(msg2)
        val recv3 = qTaker.take()
        recv3.get must be(msg3)

      }
    }
  }

}

case class QueueEvent(eventType: String, timestamp: Long) extends Event