/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.dlog.hbase

import csc.config.Path
import csc.json.lift.LiftSerialization
import net.liftweb.json.DefaultFormats
import org.scalatest.MustMatchers
import org.scalatest.{ BeforeAndAfterEach, WordSpec }
import csc.dlog.Event
import org.apache.hadoop.hbase.HBaseConfiguration
import org.apache.hadoop.hbase.client.{ Delete, Scan, HTable }

/**
 * Tests for the Event Log.
 * The Event Log is used by one writer process to write logs
 */
class EventLogTest extends WordSpec with MustMatchers with BeforeAndAfterEach {

  val path = Path("/test-events/path")
  val logTableName = "EventLogTest"
  val zkServers = "10.4.2.107:2181"

  //  var zkClient: ZkClient = _
  val serialization = new LiftSerialization {
    implicit def formats = DefaultFormats
  }
  //  var zkStore: ZkPathStorage = _

  private lazy val hbaseConfig = {
    val config = HBaseConfiguration.create()
    config.set("hbase.curator.quorum", zkServers)
    config
  }
  private var logTable: HTable = _

  override def beforeEach() {
    super.beforeEach()
    /*
    zkClient = new ZkClient(zkServers)
    zkStore = new ZkPathStorage(zkClient) {
      def serialize(obj: AnyRef) = serialization.serialize(obj)
      def deserialize[T](bytes: Array[Byte])(implicit mf: Manifest[T]) = serialization.deserialize[T](bytes)
    }
    zkStore.deleteRecursive(path)
    */

    logTable = new HTable(hbaseConfig, logTableName)
    truncate(logTable)
  }

  def truncate(table: HTable) {
    import collection.JavaConversions._

    val scan = new Scan()
    val scanner = table.getScanner(scan)
    try {
      scanner.foreach { r ⇒
        val delete = new Delete(r.getRow)
        table.delete(delete)
      }
    } finally {
      scanner.close()
      table.flushCommits()
    }
  }

  override def afterEach() {
    try {
      //     zkClient.close()
      logTable.close()
    } finally {
      super.afterEach()
    }
  }

  // Actual tests are disabled to avoid pulling Hbase testing infrastructure into the project
  // Tests can be run when local hbase and curator servers are running and hbase has a table
  // called "EventLogTest" with a column family "log"
  if (false) {
    "The Event Log" must {
      "log one event" in {
        val eventLog = new HbEventLog(logTable, "", serialization)
        val event = MyEvent(1, "Info", "some-event")
        eventLog.append(event)
        eventLog.closeChunk()
        val myEventEntries = eventLog.iterator[MyEvent]
        myEventEntries.size must be(1)
        for (event ← myEventEntries) {
          event.event must be(event)
          event.seqnr must (be > (0L))
        }
        eventLog.close()
      }
      def mkFive: List[MyEvent] = {
        List.iterate[MyEvent](new MyEvent(1, "Info", "some-event_1"), 5)(prev ⇒ new MyEvent(prev.id + 1, "Info", "some-event_" + (prev.id + 1)))
      }

      "log many events to an open chunk, and allow for reading the events when the chunk is closed" in {
        val eventLog = new HbEventLog(logTable, "", serialization)
        val events = mkFive
        events.foreach { event ⇒ eventLog.append(event) }
        eventLog.closeChunk()
        val myEvents = eventLog.iterator[MyEvent].map(_.event).toList
        myEvents.size must equal(events.size)
        events.diff[MyEvent](myEvents).isEmpty must be(true)
        eventLog.close()
      }
      def mkEvents(now: Long, amount: Int): List[MyEvent] = {
        List.iterate[MyEvent](new MyEvent(1, "Info", "some-event_1", now), 10)(prev ⇒ new MyEvent(prev.id + 1, "Info", "some-event_" + (prev.id + 1), now + prev.id * 10000))
      }
      "Return an iterator from a timestamp till a timestamp" in {
        val eventLog = new HbEventLog(logTable, "", serialization)
        // create a list of events with steps of 10 seconds in the future.
        val now = System.currentTimeMillis()
        val events = mkEvents(now, 10)

        events.dropRight(5).foreach { event ⇒ eventLog.append(event) }
        eventLog.closeChunk()
        events.drop(5).foreach { event ⇒ eventLog.append(event) }
        eventLog.closeChunk()
        val firstFiveResult = eventLog.iterator[MyEvent](now, now + 50000).map(_.event).toList
        val firstFive = events.dropRight(5)
        firstFive.diff[MyEvent](firstFiveResult).isEmpty must be(true)
        val lastFiveResult = eventLog.iterator[MyEvent](now + 50000, now + 100000).map(_.event).toList
        val lastFive = events.drop(5)
        lastFive.diff[MyEvent](lastFiveResult).isEmpty must be(true)
        val firstTwoResult = eventLog.iterator[MyEvent](now - 100000, now + 20000).map(_.event).toList
        val firstTwo = events.dropRight(8)
        firstTwo.diff[MyEvent](firstTwoResult).isEmpty must be(true)
        eventLog.close()
      }
      "Return an iterator for all closed chunks" in {
        val eventLog = new HbEventLog(logTable, "", serialization)
        // create a list of events with steps of 10 seconds in the future.
        val now = System.currentTimeMillis()
        val events = mkEvents(now, 10)
        events.foreach { event ⇒ eventLog.append(event) }
        eventLog.closeChunk()
        val results = eventLog.iterator[MyEvent].map(_.event).toList
        events.diff[MyEvent](results).isEmpty must be(true)
        eventLog.close()
      }
      "Auto-close chunks when a close strategy is provided" in {
        // close the chunk at every append. to test, we will try to read every event right after we append, which
        // would not be possible if the chunk was not closed.
        val eventLog = new HbEventLog(logTable, "", serialization)
        // create a list of events with steps of 10 seconds in the future.
        val now = System.currentTimeMillis()
        val events = mkEvents(now, 10)
        events.foldLeft(1) { (acc, event) ⇒
          eventLog.append(event)
          eventLog.iterator[MyEvent].map(_.event).toList.size must be(acc)
          acc + 1
        }
        val results = eventLog.iterator[MyEvent].map(_.event).toList
        events.diff[MyEvent](results).isEmpty must be(true)
        eventLog.close()
      }
      "Have an increasing sequence number accross chunks" in {
        val eventLog1 = new HbEventLog(logTable, "pre", serialization)
        // create a list of events with steps of 10 seconds in the future.
        val now = System.currentTimeMillis()
        val events = mkEvents(now, 10)
        events.foreach { event ⇒ eventLog1.append(event) }
        eventLog1.close()
        logTable = new HTable(hbaseConfig, logTableName)

        val eventLog2 = new HbEventLog(logTable, "pre", serialization)
        val eventsNextChunk = mkEvents(now + 10 * 10000, 10)
        eventsNextChunk.foreach { event ⇒ eventLog2.append(event) }
        eventLog2.close()
        logTable = new HTable(hbaseConfig, logTableName)

        val eventLog = new HbEventLogReader(logTable, "pre", serialization)
        val results = eventLog.iterator[MyEvent].toList
        results.foldLeft(0L) { (acc, el) ⇒
          el.seqnr must be(acc)
          el.seqnr + 1
        }
        results.size must be(20)

        val halfResults = eventLog.iterator[MyEvent](now, now + 10 * 10000 - 1).toList
        halfResults.foldLeft(0L) { (acc, el) ⇒
          el.seqnr must be(acc)
          el.seqnr + 1
        }
        halfResults.size must be(10)

        val restOfResults = eventLog.iterator[MyEvent](now + 10 * 10000, now + 20 * 10000 - 1).toList
        restOfResults.foldLeft(10L) { (acc, el) ⇒
          el.seqnr must be(acc)
          el.seqnr + 1
        }
        restOfResults.size must be(10)
      }
      "Have multiple prefixes in same table" in {
        val eventLog1 = new HbEventLog(logTable, "pre", serialization)
        // create a list of events with steps of 10 seconds in the future.
        val now = System.currentTimeMillis()
        val events = mkEvents(now, 10)
        events.foreach { event ⇒ eventLog1.append(event.copy(reason = "pre")) }
        eventLog1.close()
        logTable = new HTable(hbaseConfig, logTableName)

        val eventLog2 = new HbEventLog(logTable, "second", serialization)
        val eventsNextChunk = mkEvents(now + 10 * 10000, 10)
        eventsNextChunk.foreach { event ⇒ eventLog2.append(event.copy(reason = "second")) }
        eventLog2.close()
        logTable = new HTable(hbaseConfig, logTableName)

        val reader1 = new HbEventLogReader(logTable, "pre", serialization)
        //test with period
        val preResults = reader1.iterator[MyEvent](now + 5 * 10000, now + 20 * 10000 - 1)
        preResults.size must be(5)

        //test iterator
        val preResultsAll = reader1.iterator[MyEvent].toList
        preResultsAll.size must be(10)

        preResultsAll.foldLeft(0L) { (acc, el) ⇒
          el.event.reason must be("pre")
          el.seqnr must be(acc)
          el.seqnr + 1
        }
        //test second prefix
        val reader2 = new HbEventLogReader(logTable, "second", serialization)

        val secondResults = reader2.iterator[MyEvent](now + 5 * 10000, now + 15 * 10000 - 1)
        secondResults.size must be(5)

        val secondResultsAll = reader2.iterator[MyEvent].toList
        secondResultsAll.size must be(10)

        secondResultsAll.foldLeft(0L) { (acc, el) ⇒
          el.event.reason must be("second")
          el.seqnr must be(acc)
          el.seqnr + 1
        }
      }
    }
  }
}

case class _MyEvent(id: Int, eventType: String, reason: String, timestamp: Long = System.currentTimeMillis()) extends Event
