/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.dlog.hbase

import csc.json.lift.LiftSerialization
import net.liftweb.json.DefaultFormats
import csc.dlog.Event
import org.apache.hadoop.hbase.HBaseConfiguration
import org.apache.hadoop.hbase.client.HTable

/**
 * Test app to see if the event log can be read
 */
object ReadEventLog extends App {
  val serialization = new LiftSerialization {
    implicit def formats = DefaultFormats
  }

  private lazy val hbaseConfig = {
    val config = HBaseConfiguration.create()
    config.set("hbase.curator.quorum", "localhost:2181")
    config
  }

  private lazy val logTable = new HTable(hbaseConfig, "EventLogTest")

  val reader = EventLog.createReader(logTable, "", serialization)
  val logEntries = reader.iterator[MyEvent](0, System.currentTimeMillis())
  logEntries.foreach { entry ⇒
    println(entry.event.eventType)
  }
}

case class MyEvent(id: Int, eventType: String, reason: String, timestamp: Long = System.currentTimeMillis()) extends Event

case class SystemEvent(componentId: String,
                       timestamp: Long,
                       systemId: String,
                       eventType: String,
                       userId: String,
                       reason: Option[String] = None,
                       token: Option[SystemEventToken] = None) extends Event
case class SystemEventToken(token: String, last: Boolean)
