package csc.dlog

/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

import org.apache.curator.framework.recipes.leader.{ LeaderSelectorListener, LeaderSelector }
import java.util.concurrent.atomic.AtomicReference
import org.apache.curator.framework.state.ConnectionState
import org.slf4j.LoggerFactory
import org.apache.curator.framework.CuratorFramework

/**
 * Runs the Log Appender on one of the machines.
 * Consumes event entries from a distributed curator queue and writes this to bookkeeper ledgers.
 * Uses a Leader Selector, because there can always only be one bookkeeper writer.
 * This LogRunner only processes events when it is the Leader.
 * Clients write JSON events to the distributed queue, the consumer reads these and writes them to BookKeeper.
 * The client os not closed when logrunner is closed, since it could be shared by others.
 * @author Raymond Roestenburg
 */
class LogRunner[T <: Event: Manifest](eventLogCreator: ⇒ EventLogAppender, queueTaker: QueueTaker[T], host: String,
                                      client: CuratorFramework, mutexPath: String, blockWaitDuration: Int = 100) {
  private val log = LoggerFactory.getLogger(this.getClass)
  @volatile private var exitLeadership = false
  @volatile private var suspended = false
  private var logCount = 0L
  private val eventLogRef = new AtomicReference[Option[EventLogAppender]](None)
  log.info("Created logrunner")
  log.info("Started distributed Q for logrunner")
  val listener = new LeaderSelectorListener {
    def takeLeadership(client: CuratorFramework) {
      log.info(String.format("%s LogRunner taking leadership:", host))
      // only set if the existing value is none.
      // release leadership if not set (while loop is only executed if compareAndSet succeeds)
      if (eventLogRef.compareAndSet(None, Some(eventLogCreator))) {
        log.info(String.format("%s Created eventLog appender.", host))
        try {
          while (!Thread.currentThread().isInterrupted && !exitLeadership) {
            if (!suspended) {
              for (event ← queueTaker.take()) {
                log.debug(String.format("LogRunner received event: %s", event.toString))
                try {
                  if (event.eventType == "CloseLog") {
                    log.info(String.format("Received CloseLog event"))
                    eventLogRef.get().foreach { eventLog ⇒
                      log.info("Closing log chunk")
                      eventLog.closeChunk()
                      log.info("Closed log chunk")
                    }
                  } else {
                    logEvent(event)
                  }
                } catch {
                  case e: Exception ⇒ {
                    // some error, try to force close the chunk
                    try {
                      log.error("Releasing leadership, exception occurred in LogRunner:" + e.getMessage, e)
                    } finally {
                      try eventLogRef.get().foreach(_.closeChunk())
                      finally throw e
                    }
                  }
                }
              }
            }
            Thread.sleep(blockWaitDuration)
          }
        } catch {
          case e: InterruptedException ⇒ {
            log.info(String.format("%s LogRunner interrupted", host))
            throw e
          }
        }
      } else {
        // this is maybe not the eventLog we thought, so force a close
        eventLogRef.get.foreach(_.closeChunk())
      }
    }

    def stateChanged(client: CuratorFramework, newState: ConnectionState) {
      newState match {
        case ConnectionState.LOST ⇒
          log.info(String.format("%s LogRunner Connection Lost", host))
          try {
            eventLogRef.get().foreach(_.closeChunk())
          } finally {
            eventLogRef.set(None)
            exitLeadership = true
          }
        case ConnectionState.SUSPENDED ⇒
          log.info(String.format("%s LogRunner Suspended", host))
          suspended = true
        case ConnectionState.RECONNECTED ⇒
          log.info(String.format("%s LogRunner Reconnected", host))
          suspended = false
        case ConnectionState.CONNECTED ⇒
          log.info(String.format("%s LogRunner Connected", host))
        // assuming that takeLeadership will be called.
        case ConnectionState.READ_ONLY ⇒
          log.info(String.format("%s LogRunner ReadOnly", host))
      }
    }
  }

  def logEvent(event: T) {
    log.debug(String.format("logging event to %s", eventLogRef.get.toString))
    eventLogRef.get.foreach {
      log ⇒
        log.append(event)
        logCount = logCount + 1
    }
  }

  val leaderSelector = new LeaderSelector(client, mutexPath, listener)

  def logged = logCount

  def start() {
    queueTaker.start()
    leaderSelector.start()
  }

  def stop() {
    try {
      queueTaker.close()
    } finally {
      try {
        leaderSelector.close()
      } finally {
        eventLogRef.get().foreach(_.closeChunk())
        eventLogRef.set(None)
      }
    }
  }
}