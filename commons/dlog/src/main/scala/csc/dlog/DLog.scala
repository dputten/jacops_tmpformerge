/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.dlog
//TODO Add an Actor that subscribes to eventStream and appends to the event
/**
 * An Event that can be stored in the EventLog
 */
trait Event {
  def timestamp: Long
  def eventType: String
}

/**
 * An [[csc.dlog.Event]] that has been stored in the EventLog with information about the storage.
 * @param logId       the id of the log chunk where the event is stored
 * @param logEntryId  the id of the entry where the event is stored in the log chunk
 * @param seqnr       the sequence nr of the stored event
 * @param event       the event
 */
case class LogEntry[T <: Event: Manifest](logId: Long, logEntryId: Long, seqnr: Long, timestamp: Long, event: T)

/**
 * An append only log which writes events and gives access to the written events, in a chunk stored somewhere.
 * A [[csc.dlog.EventLog]] consists of `LogChunks`.
 */
trait EventLogAppender {
  /**
   * Appends an event to the log
   * @param event the event
   */
  def append(event: Event): Unit

  /**
   * Closes a chunk (forced)
   */
  def closeChunk(): Unit

  def close(): Unit
}

/**
 * Reads from the event log. Closed log chunks can be read from.
 */
trait EventLogReader {
  /**
   * An iterator from a timestamp till a timestamp. Returns [[csc.dlog.LogEntry[T]], which contains the event `T`.
   * @param fromTimestamp  from time
   * @param tillTimestamp  till time
   * @tparam T type of Event
   * @return an Iterator on the found events in the closed logchunks
   */
  def iterator[T <: Event: Manifest](fromTimestamp: Long, tillTimestamp: Long = System.currentTimeMillis()): Iterator[LogEntry[T]]

  /**
   * An  iterator on all the events in the log
   * @tparam T type of the Event
   * @return an Iterator on all events in closed log chunks
   */
  def iterator[T <: Event: Manifest]: Iterator[LogEntry[T]]

  /**
   * Closes the reader
   */
  def close(): Unit
}

/**
 * A chunk of a log that is open or closed, can write to this chunk when the chunk is open, read from it when it is closed. LogChunks itself can be closed via a strategy (time lapsed etc). Event logs form
 * a transaction log. the transaction log metadata is kept in curator and consists of a
 * list of logId's of the EventLogs which are part of the
 */
trait LogChunk {
  /**
   * the id of the log chunk
   */
  def logId: Long

  /**
   * The last entry id written to the log
   * @return
   */
  def lastEntryId: Long
  /**
   * the creation timestamp of the chunk (epoch)
   */
  def timestamp: Long

  /**
   * The timestamp when the log was closed, and ready for reading.
   * @return
   */
  def closedTimestamp: Option[Long]

  /**
   * true if open. can write to chunk when open, can read from chunk when closed.
   */
  def isOpenForWriting: Boolean

  /**
   * The interval, from timestamp till timestamp. if this logchunk is not closed it returns the till timestamp as the current time.
   */
  def interval: (Long, Long)
  /**
   * Closes the chunk for writing. Only a closed chunk can be read. Only an open chunk can be written to, by one party.
   */
  def close()
  /**
   * Fire and forget append of the event.
   * @param event the event
   */
  def append(event: Event)

  /**
   * Iterator on log entries
   * @param fromLogEntryId from id
   * @tparam T type of Event
   * @return Iterator on log entries from log entry Id specified
   */
  def iterator[T <: Event: Manifest](fromLogEntryId: Long): Iterator[LogEntry[T]]

  /**
   * Iterator on log entries
   * @tparam T type of Event
   * @return Iterator
   */
  def iterator[T <: Event: Manifest]: Iterator[LogEntry[T]]
}

/**
 * Info about the log chunk.
 * @param logId the id of the log chunk
 * @param timestamp the creation timestamp
 * @param closedTimestamp the timestamp of the close of the logchunk.
 */
case class LogChunkInfo(logId: Long, timestamp: Long, startSequence: Long, closedTimestamp: Option[Long], lastEntry: Option[Long])