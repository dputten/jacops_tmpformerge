/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.dlog

import org.apache.curator.framework.CuratorFramework
import csc.json.lift.ByteArraySerialization
import org.apache.curator.utils.ZKPaths
import org.apache.zookeeper.CreateMode
import org.slf4j.LoggerFactory
import org.apache.curator.framework.state.{ ConnectionState, ConnectionStateListener }
import org.apache.curator.framework.recipes.queue.QueueBuilder

/**
 * Produces to a Q
 * @tparam T element type
 * @author Raymond Roestenburg
 */
trait QueueProducer[T] {
  /**
   * Starts the producer
   */
  def start()

  /**
   * Closes the producer
   */
  def close(): this.type

  /**
   * Puts the element in the Q
   * @param elem the element
   */
  def put(elem: T)
}

/**
 * QueueProducer to a SimpleDistributedQueue
 * @param client curator client
 * @param queuePath queue path in curator
 * @param byteArraySerialization serializer
 * @tparam T element type
 */
class SimpleDistributedQueueProducer[T <: AnyRef: Manifest](client: CuratorFramework, queuePath: String, byteArraySerialization: ByteArraySerialization) extends QueueProducer[T] {
  ZKPaths.mkdirs(client.getZookeeperClient.getZooKeeper, queuePath)
  def start() {}
  def put(elem: T) {
    client.create().withMode(CreateMode.PERSISTENT_SEQUENTIAL).forPath(queuePath + "/qn-", byteArraySerialization.serialize(elem))
  }
  def close(): this.type = {
    this
  }
}

/**
 * QueueProducer to a DistributedQueue
 * @param client curator client
 * @param queuePath queue path in curator
 * @param byteArraySerialization serializer
 * @tparam T element type
 */
class DistributedQueueProducer[T <: AnyRef: Manifest](client: CuratorFramework, queuePath: String, byteArraySerialization: ByteArraySerialization) extends QueueProducer[T] {
  private val log = LoggerFactory.getLogger(this.getClass)
  private val connectionStateListener = new ConnectionStateListener {
    def stateChanged(client: CuratorFramework, newState: ConnectionState) {
      log.trace(String.format("Q State changed: %s", newState))
    }
  }
  client.getConnectionStateListenable.addListener(connectionStateListener)

  private val serializer = new ByteQueueSerializer[T](byteArraySerialization)

  private val qb = QueueBuilder.builder(client, null, serializer, queuePath)
  private val queue = qb.buildQueue()
  def put(elem: T) {
    queue.put(elem)
  }
  def start() {
    queue.start()
  }
  def close(): this.type = {
    try client.getConnectionStateListenable.removeListener(connectionStateListener) finally queue.close()
    this
  }
}

/**
 * QueueProducer to a SimpleDistributedQueue
 * @param client curator client
 * @param queuePath queue path in curator
 * @param byteArraySerialization serializer
 * @tparam T element type
 */
class DynamicPathQueueProducer[T <: AnyRef: Manifest](client: CuratorFramework, queuePath: (T) ⇒ String, byteArraySerialization: ByteArraySerialization) extends QueueProducer[T] {
  def start() {}
  def put(elem: T) {
    val path = queuePath(elem)
    ZKPaths.mkdirs(client.getZookeeperClient.getZooKeeper, path)
    client.create().withMode(CreateMode.PERSISTENT_SEQUENTIAL).forPath(path + "/qn-", byteArraySerialization.serialize(elem))
  }
  def close(): this.type = {
    this
  }
}