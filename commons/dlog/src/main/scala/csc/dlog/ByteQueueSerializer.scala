/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.dlog

import org.apache.curator.framework.recipes.queue.QueueSerializer
import csc.json.lift.ByteArraySerialization
import org.slf4j.LoggerFactory

/**
 * QueueSerializer that uses ByteSerialization
 * @author Raymond Roestenburg
 */
class ByteQueueSerializer[T <: AnyRef: Manifest](byteSerialization: ByteArraySerialization) extends QueueSerializer[T] {
  private val log = LoggerFactory.getLogger(this.getClass)
  def serialize(item: T) = {
    try {
      byteSerialization.serialize(item)
    } catch {
      case e: Exception ⇒
        log.error(String.format("Error serializing item %s to Q:%s", item, e.getMessage), e)
        throw e
    }
  }

  def deserialize(bytes: Array[Byte]) = {
    def pad0(s: String) = if (s.length == 1) "0" + s else s
    def btoh(byte: Byte) = byte.toInt.toHexString
    def toHex(bytes: Array[Byte]): String = {
      bytes.map(b ⇒ pad0(btoh(b))).mkString(" ")
    }

    try {
      byteSerialization.deserialize[T](bytes)
    } catch {
      case e: Exception ⇒
        log.error(String.format("Error deserializing bytes (hex) %s from Q:%s", toHex(bytes), e.getMessage), e)
        throw e
    }
  }
}
