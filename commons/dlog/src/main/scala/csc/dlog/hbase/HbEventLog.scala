/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.dlog.hbase

import csc.json.lift.ByteArraySerialization
import java.text.SimpleDateFormat
import csc.dlog._
import org.apache.curator.framework.CuratorFramework
import org.slf4j.LoggerFactory
import org.apache.hadoop.hbase.client.{ Result, Scan, Put, HTable }
import org.apache.hadoop.hbase.util.Bytes
import java.util.TimeZone

/**
 * Factory for public BookKeeper EventLog components
 */
object EventLog {
  /**
   * Creates a Reader on the Event Log
   * @param serialization serialization
   */
  def createReader(logTable: HTable, keyPrefix: String, serialization: ByteArraySerialization): EventLogReader = {
    new HbEventLogReader(logTable, keyPrefix, serialization)
  }

  /**
   * Creates a LogRunner
   * @param eventLogPath path for event log from which the LogRunner reads log events
   * @param mutexPath path for lock on events so that only one will do the job (extra safe)
   * @tparam T type of events
   * @return LogRunner
   */
  def createLogRunner[T <: Event: Manifest](logTable: HTable, keyPrefix: String, client: CuratorFramework, eventLogPath: String, mutexPath: String, serialization: ByteArraySerialization) = {
    new LogRunner[T](createLocalAppender(logTable, keyPrefix, serialization),
      new SimpleDistributedQueueTaker[T](client, eventLogPath, serialization), java.net.InetAddress.getLocalHost.getHostName, client, mutexPath)
  }

  private[dlog] def createLocalAppender(logTable: HTable, keyPrefix: String, serialization: ByteArraySerialization): EventLogAppender = {
    new HbEventLog(logTable, keyPrefix, serialization)
  }
}

/**
 * An HBase Event log reader.
 *
 * @param serialization serialization
 */
private[hbase] class HbEventLogReader(logTable: HTable, keyPrefix: String, serialization: ByteArraySerialization) extends EventLogReader {
  private[hbase] val logColumnFamily = Bytes.toBytes("log")
  private[hbase] val logColumnName = Bytes.toBytes("log")
  private[hbase] val metaColumnName = Bytes.toBytes("meta")
  private[hbase] def now = System.currentTimeMillis()

  private val keyFormat = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss.SSS_")
  keyFormat.setTimeZone(TimeZone.getTimeZone("UTC"))
  private[hbase] def makeKey(timestamp: Long, eventType: String = ""): Array[Byte] = {
    Bytes.toBytes(keyPrefix + keyFormat.format(timestamp) + eventType)
  }

  def close() {
    logTable.close()
  }

  private def iterator[T <: Event: Manifest](from: Option[Long], until: Option[Long]): Iterator[LogEntry[T]] = {
    import collection.JavaConversions._

    val scan = new Scan()

    scan.setStartRow(from.map(makeKey(_)).getOrElse(Bytes.toBytes(keyPrefix)))
    scan.setStopRow(until.map(makeKey(_)).getOrElse(Bytes.toBytes(keyPrefix + "zzzz")))
    scan.addColumn(logColumnFamily, logColumnName)
    scan.addColumn(logColumnFamily, metaColumnName)
    val scanner = logTable.getScanner(scan)
    try {
      scanner.map(r ⇒ {
        val event = serialization.deserialize[T](r.getValue(logColumnFamily, logColumnName))
        val meta = serialization.deserialize[LogMeta](r.getValue(logColumnFamily, metaColumnName))
        LogEntry(1L, meta.sequenceNr, meta.sequenceNr, event.timestamp, event)
      }).toIterator
    } finally {
      scanner.close()
    }
  }

  def iterator[T <: Event: Manifest](fromTimestamp: Long, tillTimestamp: Long = now): Iterator[LogEntry[T]] =
    iterator[T](Some(fromTimestamp), Some(tillTimestamp))

  def iterator[T <: Event: Manifest]: Iterator[LogEntry[T]] = iterator[T](None, None)
}

/**
 * An HBase Event log.
 *
 * @param serialization serialization
 */
private[hbase] class HbEventLog(logTable: HTable, keyPrefix: String, serialization: ByteArraySerialization)
  extends HbEventLogReader(logTable, keyPrefix, serialization) with EventLogAppender {
  private val log = LoggerFactory.getLogger(this.getClass)

  private lazy val seqGenerator = new SeqGenerator(loadMaxSeqNr() + 1)
  private lazy val finalRowKey = Bytes.toBytes(keyPrefix + "zzzz")

  def append(event: Event) {
    val seqNr = seqGenerator.nextSeqNr
    log.debug(String.format("HbEventLog append event %s with seq nr %s", event.toString, seqNr.toString))
    val meta = LogMeta(seqNr, now)
    val keyBytes = makeKey(event.timestamp, seqNr.toString)
    val put = new Put(keyBytes)
    put.add(logColumnFamily, logColumnName, serialization.serialize(event))
    put.add(logColumnFamily, metaColumnName, serialization.serialize(meta))
    logTable.put(put)
    logTable.flushCommits()
  }

  def closeChunk() {
    // not needed
  }

  private def loadMaxSeqNr() = {
    val lastRow = logTable.getRowOrBefore(finalRowKey, logColumnFamily)
    if (isLastRow(lastRow)) {
      val lastMeta = lastRow.getValue(logColumnFamily, metaColumnName)
      log.debug(keyPrefix + ": loadMaxSeqNr found last row " + Bytes.toString(lastRow.getRow) + " containing " + Bytes.toString(lastMeta))
      try {
        serialization.deserialize[LogMeta](lastMeta).sequenceNr
      } catch {
        case e: Exception ⇒
          log.debug(keyPrefix + ": failed to parse last row: " + e.getMessage)
          -1L
      }
    } else {
      log.debug(keyPrefix + ": loadMaxSeqNr did not find last row.")
      -1L
    }
  }
  private def isLastRow(lastRow: Result): Boolean = {
    if (lastRow != null) {
      val str = new String(lastRow.getRow)
      (str.startsWith(keyPrefix))
    } else {
      false
    }
  }

  private class SeqGenerator(initial: Long) {
    private def from(n: Long): Stream[Long] = Stream.cons(n, from(n + 1))
    private var stream: Stream[Long] = from(initial)
    def nextSeqNr: Long = { synchronized { val seqnr = stream.head; stream = stream.tail; seqnr } }
  }
}

private case class LogMeta(sequenceNr: Long, logTime: Long)