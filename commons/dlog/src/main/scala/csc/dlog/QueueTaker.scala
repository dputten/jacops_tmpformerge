/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.dlog

import org.apache.curator.framework.CuratorFramework
import csc.json.lift.ByteArraySerialization
import org.apache.curator.utils.ZKPaths
import java.util.concurrent.TimeUnit
import org.slf4j.LoggerFactory
import org.apache.curator.framework.state.{ ConnectionState, ConnectionStateListener }
import org.apache.curator.framework.recipes.queue.{ QueueBuilder, BlockingQueueConsumer, SimpleDistributedQueue }

/**
 * Allows taking from the Queue
 * @tparam T element type of the Queue
 * @author Raymond Roestenburg
 */
trait QueueTaker[T] {
  /**
   * Starts the Q
   */
  def start()

  /**
   * Takes from the Q. throws Exception on error
   * @return option of element
   */
  def take(): Option[T]

  /**
   * Closes the Q
   */
  def close(): this.type
}

/**
 * Allows taking from the queue. Uses a SimpleDistributedQueue
 * @param client the curator client
 * @param queuePath path to the queue, will be created if not existing
 * @param byteArraySerialization serializer for elements in the queue
 * @tparam T type of element
 */
class SimpleDistributedQueueTaker[T <: AnyRef: Manifest](client: CuratorFramework, queuePath: String, byteArraySerialization: ByteArraySerialization) extends QueueTaker[T] {
  private val queue = new SimpleDistributedQueue(client, queuePath)
  ZKPaths.mkdirs(client.getZookeeperClient.getZooKeeper, queuePath)
  def start() {}

  def take() = {
    Option(queue.poll()).map(byteArraySerialization.deserialize[T](_))
  }

  def close(): this.type = { this }
}

/**
 * Allows taking from the queue. Uses a DistributedQueue[T]
 * @param client the curator client
 * @param queuePath path to the queue, will be created if not existing
 * @param byteArraySerialization serializer for elements in the queue
 * @tparam T type of element
 */
class DistributedQueueTaker[T <: AnyRef: Manifest](client: CuratorFramework, queuePath: String, blockWaitDuration: Int, byteArraySerialization: ByteArraySerialization) extends QueueTaker[T] {
  private val log = LoggerFactory.getLogger(this.getClass)
  private val connectionStateListener = new ConnectionStateListener {
    def stateChanged(client: CuratorFramework, newState: ConnectionState) {
      log.trace(String.format("Q State changed: %s", newState))
    }
  }
  client.getConnectionStateListenable.addListener(connectionStateListener)

  private val consumer = new BlockingQueueConsumer[T](connectionStateListener)
  private val serializer = new ByteQueueSerializer[T](byteArraySerialization)

  private val qb = QueueBuilder.builder(client, consumer, serializer, queuePath)
  private val queue = qb.buildQueue()

  def take(): Option[T] = {
    Option(consumer.take(blockWaitDuration, TimeUnit.MILLISECONDS))
  }
  def start() {
    queue.start()
  }
  def close(): this.type = {
    queue.close()
    this
  }
}
