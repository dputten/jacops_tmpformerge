/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.dlog.actor

import org.scalatest.MustMatchers
import csc.curator.CuratorTestServer
import csc.json.lift.LiftSerialization
import net.liftweb.json.DefaultFormats
import akka.testkit.{ TestLatch, TestKit }
import scala.concurrent.Await
import java.util.concurrent.TimeUnit
import org.apache.curator.retry.RetryOneTime
import org.apache.curator.framework.CuratorFramework
import csc.dlog._
import org.apache.curator.framework.state.ConnectionState
import org.apache.curator.framework.recipes.queue.{ QueueBuilder, QueueConsumer, SimpleDistributedQueue }
import scala.concurrent.duration._
import akka.actor._
import akka.util.Timeout
import akka.pattern._
import org.scalatest.{ BeforeAndAfterAll, WordSpecLike }

/**
 * Test for EventLogger
 * @author Raymond Roestenburg
 */
class EventLoggerTest extends TestKit(ActorSystem()) with WordSpecLike with MustMatchers with CuratorTestServer with BeforeAndAfterAll {
  var msg: Option[Event] = None
  val queuePath = "/queue"
  implicit val timeout = Timeout(1 second)
  val timeoutDuration = 1 second
  val serialization = new LiftSerialization {
    implicit def formats = DefaultFormats
  }

  "The EventLogger Actor" must {
    "log events to the distributed queue" in {
      for (c ← clientScope) {
        val now = System.currentTimeMillis()
        val event = MySystemEvent(1, "Info", "reason", now)

        def creator(client: CuratorFramework) = new DistributedQueueProducer[MySystemEvent](client, queuePath, serialization)
        doLogEventTest(createEventLoggerActorRef(creator), event, creator)
        val latch = TestLatch()
        createConsumerOnQueue(serialization, latch, c, queuePath)
        Await.ready(latch, 5 seconds)
        // should be there
        msg.isEmpty must be(false)
        msg.foreach(_ must be(event))
      }
    }
    "log events to the simple distributed queue" in {
      for (c ← clientScope) {
        val now = System.currentTimeMillis()
        val event = MySystemEvent(1, "Info", "reason", now)
        def creator(client: CuratorFramework) = new SimpleDistributedQueueProducer[MySystemEvent](client, queuePath, serialization)
        val ref = createEventLoggerActorRef(creator)
        doLogEventTest(ref, event, creator)
        val result = serialization.deserialize[MySystemEvent](new SimpleDistributedQueue(c, queuePath).poll(2, TimeUnit.SECONDS))
        result must be(event)

        val event2 = MySystemEvent(2, "Info", "reason2", now + 1000)
        doLogEventTest(ref, event2, creator)
        val result2 = serialization.deserialize[MySystemEvent](new SimpleDistributedQueue(c, queuePath).poll(2, TimeUnit.SECONDS))
        result2 must be(event2)
      }
    }
    "log events to the dynamicPath distributed queue" in {
      for (c ← clientScope) {
        val now = System.currentTimeMillis()
        val event = MySystemEvent(1, "Info", "reason", now)
        val queuePathFunc = (ev: MySystemEvent) ⇒ {
          queuePath
        }
        def creator(client: CuratorFramework) = new DynamicPathQueueProducer[MySystemEvent](client, queuePathFunc, serialization)
        doLogEventTest(createEventLoggerActorRef(creator), event, creator)
        val result = serialization.deserialize[MySystemEvent](new SimpleDistributedQueue(c, queuePath).poll(2, TimeUnit.SECONDS))
        result must be(event)
      }
    }

    "log with the default eventLogger" in {
      for (c ← clientScope) {
        val now = System.currentTimeMillis()
        val event = MySystemEvent(1, "Info", "reason", now)
        val actorRef = createDefaultEventLoggerActorRef(creator)
        actorRef ! event
        val queue = new SimpleDistributedQueue(c, queuePath)
        val result = Option(queue.poll(2, TimeUnit.SECONDS)).map {
          serialization.deserialize[MySystemEvent](_)
        }
        result.map { r ⇒
          r must be(event)
          r
        }.isEmpty must be(false)
      }
    }
    "survive exceptions through preRestart and postStop hooks in supervision" in {
      val startLatch = TestLatch(2)
      val stopLatch = TestLatch(1)
      def creator(client: CuratorFramework) = new QueueProducer[MySystemEvent] {
        def start() {
          startLatch.countDown()
        }
        def close(): this.type = {
          stopLatch.countDown()
          this
        }
        def put(elem: MySystemEvent) {
          println("put, throw")
          throw new Exception("fault")
        }
      }

      val supervisor = system.actorOf(Props(new Actor {
        //default supervision will do
        def receive = {
          case p: Props ⇒ {
            val eventLogger = context.actorOf(p)
            context.watch(eventLogger)
            sender ! eventLogger
          }
          case Terminated(actorRef) ⇒ {
            println("Terminated")
          }
        }
      }))
      val eventLoggerRef = Await.result[ActorRef](supervisor.ask(Props(new EventLogger[MySystemEvent](zkServers,
        creator, new RetryOneTime(1000), serialization) with ReplyAfterProcessing)).mapTo[ActorRef], timeoutDuration)
      val now = System.currentTimeMillis()
      val event = MySystemEvent(1, "Info", "reason", now)
      eventLoggerRef ! event
      // stop is called once, start twice. start, error, postStop->stop, preRestart->start
      Await.ready(stopLatch, timeoutDuration)
      Await.ready(startLatch, timeoutDuration)
    }
  }

  def creator(client: CuratorFramework) = new SimpleDistributedQueueProducer[MySystemEvent](client, queuePath, serialization)

  def createDefaultEventLoggerActorRef(creator: CuratorFramework ⇒ QueueProducer[MySystemEvent]) =
    system.actorOf(Props(EventLogger.createDefaultEventLogger(zkServers, queuePath, serialization)))

  def createEventLoggerActorRef(creator: CuratorFramework ⇒ QueueProducer[MySystemEvent]) =
    system.actorOf(Props(new EventLogger[MySystemEvent](zkServers, creator, new RetryOneTime(1000), serialization) with ReplyAfterProcessing))

  def doLogEventTest(actorRef: ActorRef, event: MySystemEvent, creator: CuratorFramework ⇒ QueueProducer[MySystemEvent]) {
    // fake an ask, by stacking the ReplyAfterProcessing trait
    val reply = actorRef.ask(event)
    // wait for the Actor to put
    Await.ready(reply, 5 seconds)
  }

  def createConsumerOnQueue(serialization: LiftSerialization { def formats: DefaultFormats.type }, latch: TestLatch, c: CuratorFramework, queuePath: String) {
    val serializer = new ByteQueueSerializer[MySystemEvent](serialization)
    val consumer = new QueueConsumer[MySystemEvent] {
      def consumeMessage(message: MySystemEvent) {
        msg = Some(message)
        latch.countDown()
      }

      def stateChanged(client: CuratorFramework, newState: ConnectionState) {}
    }
    val q = QueueBuilder.builder(c, consumer, serializer, queuePath).buildQueue()
    q.start()
  }

  override def afterAll() {
    system.shutdown()
    super.afterAll()
  }
}

case class MySystemEvent(id: Int, eventType: String, reason: String, timestamp: Long) extends Event

trait ReplyAfterProcessing extends Actor {

  abstract override def receive = {
    super.receive andThen {
      case msg ⇒ sender ! (msg)
    }
  }
}