package csc.dlog.actor

/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

import org.apache.curator.framework.state.{ ConnectionState, ConnectionStateListener }
import org.apache.curator.framework.{ CuratorFrameworkFactory, CuratorFramework }
import org.apache.curator.RetryPolicy
import csc.json.lift.ByteArraySerialization
import org.apache.curator.framework.recipes.queue.{ DistributedQueue, QueueBuilder }
import akka.actor.{ ActorLogging, Actor }
import org.apache.curator.retry.{ ExponentialBackoffRetry, RetryNTimes }
import csc.dlog.{ SimpleDistributedQueueProducer, QueueProducer, Event }
import csc.json.lift.LiftSerialization
import net.liftweb.json.DefaultFormats

import scala.reflect.ClassTag

/**
 * An Actor that logs events to the Distributed Queue. Subscribes to `Event` type on the event bus
 * @author Raymond Roestenburg
 */
class EventLogger[T <: Event: Manifest](zkServers: String,
                                        qProducerCreator: CuratorFramework ⇒ QueueProducer[T],
                                        retryPolicy: RetryPolicy,
                                        byteSerialization: ByteArraySerialization) extends Actor with ActorLogging {
  private var clientScope: Option[CuratorFramework] = _
  private var qProducerScope: Option[QueueProducer[T]] = _

  override def preStart() {
    context.system.eventStream.subscribe(self, manifest[T].runtimeClass)
    clientScope = Some(CuratorFrameworkFactory.newClient(zkServers, retryPolicy))
    clientScope.foreach {
      c ⇒
        c.start()
        qProducerScope = Some(qProducerCreator(c))
        qProducerScope.foreach(_.start)
    }
  }

  override def postStop() {
    for (c ← clientScope) {
      // producer also closes client, unless there was no producer
      try qProducerScope foreach (_.close()) finally {
        c.close()
        qProducerScope = None
        clientScope = None
      }
    }
  }

  /**
   * Forwards events to distributed queue
   */
  def receive = {
    case event: T     ⇒ qProducerScope.foreach(_.put(event)) //doesn't match if parameter type is not specified
    case event: Event ⇒ qProducerScope.foreach(_.put(event.asInstanceOf[T])) //so we added this one to catch it
    case _            ⇒ ()
  }
}

/**
 * Companion object with convenience default event logger
 */
object EventLogger {
  val defaultLiftSerialization = new LiftSerialization {
    implicit def formats = DefaultFormats
  }

  /**
   * Creates a default configured EventLogger Actor, which you need to pass to a Props object in system.actorOf
   * @param zkServers zk server string
   * @param queuePath path to log to
   * @param byteArraySerialization serialization
   * @tparam T element to log
   * @return the event logger actor
   */
  def createDefaultEventLogger[T <: Event: Manifest](zkServers: String, queuePath: String, byteArraySerialization: ByteArraySerialization = defaultLiftSerialization): Actor = {
    val retry = new ExponentialBackoffRetry(500, 100)

    def createQueueProducer(zkServers: String, queuePath: String): CuratorFramework ⇒ QueueProducer[T] = {
      client ⇒ new SimpleDistributedQueueProducer[T](client, queuePath, byteArraySerialization)
    }
    new EventLogger[T](zkServers, createQueueProducer(zkServers, queuePath), retry, byteArraySerialization)
  }
}