package csc.json.lift

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 9/26/14.
 */

/**
 * Serialization of objects to byte arrays and de-serialization of byte arrays to objects.
 * It is expected that you use case classes for the objects to serialize and de-serialize
 */
trait ByteArraySerialization {
  /**
   * Serializes the object into an array of bytes
   * @param obj the object
   * @return the serialized byte array
   */
  def serialize(obj: AnyRef): Array[Byte]

  /**
   * Deserializes the byte array into an object of type <code>T</code>
   * @param bytes the byte array to deserialize
   * @return the object of requested type <code>T</code>
   */
  def deserialize[T](bytes: Array[Byte])(implicit mf: Manifest[T]): T
}
