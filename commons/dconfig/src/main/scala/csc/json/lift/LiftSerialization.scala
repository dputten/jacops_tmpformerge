/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.json.lift

import net.liftweb.json.{ Formats, Serialization }

trait LiftSerialization extends ByteArraySerialization {
  implicit def formats: Formats

  val charsetName = "UTF8"
  /**
   * Serializes the object into an array of bytes, in JSON format.
   */
  def serialize(obj: AnyRef) = {
    val json = Serialization.write(obj)
    json.getBytes(charsetName)
  }

  /**
   * Deserializes the bytes array containing JSON, into an object.
   */
  def deserialize[T](bytes: Array[Byte])(implicit mf: Manifest[T]) = {
    Serialization.read[T](new String(bytes, charsetName))
  }
}
