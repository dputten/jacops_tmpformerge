/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.config

/**
 * A part of a <code>Path</code>, a <code>Node</code> (root, or node that has a next node) or a <code>Leaf</code> ( a node with no child).
 * A Path is a string/chain of <code>PathElement</code>s.
 */
abstract class PathElement {
  val name: String
  val next: Option[PathElement]

  def toPath: Path
}

/**
 * A <code>Path</code> to a value in the storage. The Path consists of <code>PathElement</code>s, Nodes and Leaves.
 * The last path element in the path is always a <code>Leaf</code>.
 * A <code>Path</code> is immutable. new Paths can be created based on a Path, using the methods on this class.
 */
case class Path(nodes: List[PathElement]) {
  require(nodes != null)
  require(nodes.size > 0)

  override def toString = nodes.foldLeft("")((path, node) ⇒ path + "/" + node.name)

  /**
   * Returns a node in the path by index
   * @param index the index in the path
   */
  def apply(index: Int) = nodes(index)

  private def appendedList(child: PathElement): List[PathElement] = {
    val init = List[PathElement](child)
    val nodeList = nodes.foldRight(init)((el, list) ⇒ {
      new Node(el.name, list.head) +: list
    })
    nodeList
  }

  /**
   * Create a new Path, with the child node appended.
   * @param child the child to append
   * @return the new path with the child appended
   */
  def /(child: PathElement): Path = {
    Path(appendedList(child))
  }

  /**
   * Creates a new path with the child argument appended
   * @param child the child name to append
   * @return the new path with the child appended to this path
   */
  def /(child: String): Path = {
    require(child != null)
    require(!child.contains("/"))
    this / Leaf(child)
  }

  /**
   * creates a new concatenated path of this path and the argument path
   * @param path the path to concatenate with
   * @return a new path that is the concatenation of this path and the argument path
   */
  def /(path: Path): Path = {
    require(path.nodes.size > 0)
    Path(appendedList(path(0)) ++ path.nodes.tail)
  }

  /**
   * Returns the parent path, meaning the full path to, the parent of the last node in this path.
   */
  def parent: Path = {
    require(nodes.size > 1)
    val initial = List[PathElement](Leaf(nodes.init.last.name))
    val nodeList = nodes.init.init.foldRight(initial)((el, list) ⇒ {
      new Node(el.name, list.head) +: list
    })
    Path(nodeList)
  }
}

/**
 * A node in the path that has a child (a next node).
 * No distinction is made between a root node or a sub node.
 * This class is immutable
 */
case class Node(name: String, child: PathElement) extends PathElement {
  override val next = Some(child)

  /**
   * Turn the node into a leaf
   * @return the new leaf with the same name as this node.
   */
  def toLeaf: Leaf = Leaf(name)

  /**
   * Turn the node into a path
   * @return the path based on this node and it's next nodes.
   */
  def toPath: Path = {
    val list = List[PathElement]()
    val newList = foldLeft(list)((l, el) ⇒ { l :+ el })
    Path(newList)
  }

  /**
   * foldLeft on this node. traverses the next nodes from this node
   * @param z the initial value for the fold
   * @param f the function that is folded
   */
  def foldLeft[B](z: B)(f: (B, PathElement) ⇒ B): B = {
    var acc = z
    var these: PathElement = this
    while (None != these.next) {
      acc = f(acc, these)
      these = these.next.get
    }
    acc = f(acc, these)
    acc
  }

}

/**
 * A node that has no child/next node.
 */
case class Leaf(name: String) extends PathElement {
  override val next = None

  /**
   * Turn this leaf into a node.
   * @param child the child for the node that is created
   * @return the new node with the same name as this leaf
   */
  def toNode(child: PathElement): Node = Node(name, child)

  /**
   * Turn this leaf into a path.
   * @return the path which only includes this leaf
   */
  def toPath: Path = Path(this)
}

/**
 * Object for creating paths
 */
object Path {

  /**
   * Creates a Path from elements
   */
  def apply(elements: PathElement*): Path = {
    new Path(elements.toList)
  }

  /**
   * Creates a Path of nodes from a string.
   */
  def apply(pathString: String): Path = {
    require(pathString != null)
    require(pathString.startsWith("/"))
    val elements = pathString.split("/").tail
    val leaf: PathElement = Leaf(elements.last)
    val init = List(leaf)
    val nodes = elements.init.foldRight[List[PathElement]](init)((node, list) ⇒ {
      val p = new Node(node, list.head)
      p +: list
    })
    Path(nodes)
  }

}

/**
 * A version of some data
 */
case class VersionOf[T <: AnyRef: Manifest](data: T, version: Long)

