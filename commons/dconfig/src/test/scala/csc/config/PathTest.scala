package csc.config

import org.scalatest.MustMatchers
import org.scalatest.WordSpec

/**
 * Test for paths
 * @author Raymond Roestenburg
 */
class PathTest extends WordSpec with MustMatchers {
  "A Path" must {
    "be constructed from a well-formed string" in {
      val s = "/some/valid/path"
      val pathList = Path(s)
      pathList.nodes(0).name must be("some")
      pathList.nodes(1).name must be("valid")
      pathList.nodes(2).name must be("path")
      pathList.nodes(0).next.get.name must be("valid")
      pathList.nodes(0).next.get.next.get.name must be("path")
    }
    "convert from and to a string representation with toString" in {
      val expected = "/root/node1/node2/leaf1"
      val path = Path(expected)
      path(0).name must be("root")
      path(1).name must be("node1")
      path(2).name must be("node2")
      path(3).name must be("leaf1")
      val stringRep = path.toString
      stringRep must be(expected)
    }
    "append a path element with / " in {
      val root = "/root"
      val p = Path(root)
      p(0).name must be("root")
      val node1Path = p / Leaf("node1")
      node1Path(0).name must be("root")
      node1Path(1).name must be("node1")
    }
    "append a string with / " in {
      val p = Path("/root") / "node1" / "node2" / "leaf"
      p must be(Path("/root/node1/node2/leaf"))
    }
    "append a path with a path" in {
      val p = Path("/root/some/node")
      val p2 = Path("/and/something/else")
      val concat = p / p2
      concat must be(Path("/root/some/node/and/something/else"))
    }
    "create a path from a node" in {
      val path = Path("/root/some/node/and/then/what")
      path(0).toPath must be(Path("/root/some/node/and/then/what"))
      path(2).toPath must be(Path("/node/and/then/what"))
      path(3).toPath must be(Path("/and/then/what"))
      path(5).toPath must be(Path("/what"))
    }
    "return a parent path from a path" in {
      val path = Path("/root/some/node/and/then/what")
      path.parent must be(Path("/root/some/node/and/then"))
      path.parent.parent must be(Path("/root/some/node/and"))
      path.parent.parent.parent must be(Path("/root/some/node"))
      path.parent.parent.parent.parent must be(Path("/root/some"))
      path.parent.parent.parent.parent.parent must be(Path("/root"))
    }
  }

}