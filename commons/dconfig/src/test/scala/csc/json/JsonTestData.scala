/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.json

/**
 * some helper methods for the tests that need some case class objects to serialize and deserialize
 */
object JsonTestData {
  def newCamera = TCamera("host1", 1200, "CAM1", TCameraType.HiRes)
  def newRadar = TRadar(measurementAngle = 35, height = 5.0f, surfaceReflection = 20, port = 10)
  def newLd = TLengthDetector("ld1")
  def newLane = TLane(nr = 1, lat = 5.3, lon = 59.2, camera = newCamera, radar = newRadar, ld = newLd)
  def newPortal = TPortal(1, List(newLane))
  def newRoute = TRoute("A2", List(newPortal))
}

/**
 * Some testclasses for serialization tests,
 * that use typeclass based SJSON
 * @author Raymond Roestenburg
 */
case class TRoute(name: String, portals: List[TPortal])

case class TPortal(id: Int, lanes: List[TLane])

case class TLane(nr: Int, lat: Double, lon: Double, camera: TCamera, radar: TRadar, ld: TLengthDetector)

case class TCamera(lcHost: String, lcPort: Int, imageDir: String, ctype: TCameraType.Value)

case class TRadar(measurementAngle: Float, height: Float, surfaceReflection: Float, port: Int)

case class TLengthDetector(id: String)

object TCameraType extends Enumeration {
  type TCameraType = Value
  val HiRes = Value("HiRes")
  val LoRes = Value("LoRes")
}
