package csc.json.lift

import net.liftweb.json.Serialization._
import net.liftweb.json.{ NoTypeHints, Serialization }
import org.scalatest.WordSpec
import org.scalatest.MustMatchers

/**
 * Created by carlos on 06.07.15.
 */
class EnumerationSerializerTest extends WordSpec with MustMatchers {

  "EnumerationSerializer" must {
    "serialize and deserialize enum values, classified" in {
      implicit val formats = Serialization.formats(NoTypeHints) + new EnumerationSerializer(Enum1, Enum2)

      val source = Wrapper(Enum1.HiRes, Enum2.HiRes)
      val json = write(source)

      val valueFromJson = read[Wrapper](json)
      source must be(valueFromJson)
    }

    "deserialize enum values, legacy" in {
      implicit val formats = Serialization.formats(NoTypeHints) + new EnumerationSerializer(Enum1, Enum2)

      val expected = Wrapper(Enum1.LoRes, Enum2.None)
      val json = """{"value1":"Enum1$LoRes","value2":"None"}"""

      val valueFromJson = read[Wrapper](json)
      expected must be(valueFromJson)
    }

  }
}

case class Wrapper(value1: Enum1.Value, value2: Enum2.Value)

object Enum1 extends Enumeration {
  type Enum1 = Value
  val HiRes = Value
  val LoRes = Value
}

object Enum2 extends Enumeration {
  type Enum2 = Value
  val HiRes = Value
  val LoRes = Value
  val None = Value
}
