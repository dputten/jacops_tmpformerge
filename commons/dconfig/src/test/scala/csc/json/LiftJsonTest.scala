/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.json

import org.scalatest.MustMatchers
import org.scalatest.WordSpec
import net.liftweb.json.Serialization._
import net.liftweb.json._
import csc.json.lift.EnumerationSerializer

/**
 * Lift-Json test
 * @author Raymond Roestenburg
 */
class LiftJsonTest extends WordSpec with MustMatchers {
  "Lift-json " must {
    "serialize and deserialize standard case classes without modification" in {
      import JsonTestData._
      implicit val formats = Serialization.formats(NoTypeHints) + new EnumerationSerializer(TCameraType)

      val route = newRoute
      val json = write(route)
      val routeFromJson = read[TRoute](json)
      route must be(routeFromJson)
    }
  }
}

