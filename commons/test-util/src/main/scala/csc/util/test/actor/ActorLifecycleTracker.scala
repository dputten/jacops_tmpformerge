package csc.util.test.actor

import akka.actor.Actor
import akka.event.Logging.Debug
import csc.util.test.actor.LifecycleType._

/**
 * Utility actor that tracks the system actor lifecycle events.
 *
 * Created by carlos on 18.11.15.
 */
class ActorLifecycleTracker extends Actor {

  var events: Seq[LifecycleEvent] = Nil
  var types: Set[LifecycleType] = Set.empty

  override def receive: Receive = {
    case ClearLifecycleStats       ⇒ clearStats
    case SubscribeLifecycle(types) ⇒ subscribe(types)
    case LifecycleStatsRequest     ⇒ sender ! LifecycleStats(events)
    case msg: Debug                ⇒ handle(msg)
  }

  def add(event: LifecycleEvent): Unit = events = events.+:(event)

  def getCreationEvent(msg: Debug): Option[ActorCreation] = {
    if (msg.message.toString.startsWith("started (")) {
      Some(ActorCreation(msg.logSource, msg.logClass, System.currentTimeMillis()))
    } else None
  }

  def handle(msg: Debug): Unit = {
    if (types(Creation)) getCreationEvent(msg) map { e ⇒ add(e) }
  }

  def clearStats: Unit = events = Nil

  def subscribe(newTypes: Set[LifecycleType]): Unit = {
    types = newTypes
    clearStats
  }

}
