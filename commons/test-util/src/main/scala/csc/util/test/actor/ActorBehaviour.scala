package csc.util.test.actor

import akka.actor.ActorRef
import akka.testkit.TestActor.AutoPilot

/**
 * Created by carlos on 29/09/16.
 */
object ActorBehaviour {

  type ActorBehaviour = PartialFunction[Any, Option[Any]]

  def toAutoPilot(behaviour: ActorBehaviour): AutoPilot = new AutoPilot {
    override def run(sender: ActorRef, msg: Any): AutoPilot = {
      behaviour.isDefinedAt(msg) match {
        case true  ⇒ behaviour(msg) foreach (sender ! _)
        case false ⇒ println("No behaviour for message " + msg)
      }
      this
    }
  }
}

