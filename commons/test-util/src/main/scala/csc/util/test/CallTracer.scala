package csc.util.test

import java.lang.reflect.{ InvocationHandler, InvocationTargetException, Method, Proxy }

import akka.event.LoggingAdapter

import scala.reflect.ClassTag

/**
 * Created by carlos on 19/10/16.
 */
object CallTracer {

  private def createWriter(log: Option[LoggingAdapter]): String ⇒ Unit = log match {
    case None    ⇒ { msg ⇒ println(msg) }
    case Some(l) ⇒ { msg ⇒ l.info(msg) }
  }

  def create[T](target: AnyRef, log: Option[LoggingAdapter] = None)(implicit tag: ClassTag[T]): T = {

    val write = createWriter(log)

    val handler = new InvocationHandler {
      override def invoke(obj: scala.Any, method: Method, args: Array[AnyRef]): AnyRef = {
        val argsStr = Option(args) map { a ⇒ a map (makeString(_)) mkString (sep) } getOrElse ("")
        val callStr = s".${method.getName}($argsStr)"
        try {
          val m = target.getClass.getDeclaredMethod(method.getName, method.getParameterTypes: _*)
          val result = m.invoke(target, args: _*)

          if (m.getReturnType == Void.TYPE)
            write(s"$callStr")
          else
            write(s"$callStr: ${makeString(result)}")

          result
        } catch {
          case ex: InvocationTargetException ⇒
            write(s"$callStr threw ${ex.getTargetException}")
            throw ex
          case ex: NoSuchMethodException ⇒
            write("No method: " + method)
            throw ex
          case ex: Exception ⇒
            write("Unexpected exception: " + ex)
            throw ex
        }
      }
    }
    val cl = tag.runtimeClass
    Proxy.newProxyInstance(cl.getClassLoader, Array(cl), handler).asInstanceOf[T]
  }

  val sep = ","

  private def makeString(obj: Any): String = {
    obj match {
      case null ⇒ null
      case array: Array[_] ⇒
        val parts = array.toList map { e ⇒ makeString(e) }
        "[" + parts.mkString(sep) + "]"
      case other ⇒ other.toString
    }
  }

  //    def main(args: Array[String]): Unit = {
  //      val foo = create[Foo](new FooImpl)
  //      val array = Array("a", "b", "c")
  //      foo.convert(array)
  //      foo.convert("aaa")
  //      foo.convert(1)
  //    }
  //
  //    trait Foo {
  //      def convert(obj: Any): Any
  //    }
  //
  //    class FooImpl extends Foo {
  //      def convert(obj: Any): Any = obj
  //    }

}
