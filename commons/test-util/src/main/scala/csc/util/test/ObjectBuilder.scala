package csc.util.test

import java.lang.reflect.{ Constructor, InvocationTargetException, Modifier }
import java.util
import java.util.Date

import scala.concurrent.duration.Duration
import scala.collection.mutable.ListBuffer
import scala.reflect.ClassTag

/**
 * Created by carlos on 28.07.15.
 */
/*
object BuilderMain extends ObjectBuilder {

  addClass[SectionControlConfig]

  def main(args: Array[String]): Unit = {
    println(create[ZkCorridorInfo])
    println(create[CorridorConfig])
  }
}
*/
class ObjectCache extends ObjectBuilder {

  val implementations: ListBuffer[Object] = ListBuffer()

  override def using(objects: Object*): ObjectCache = {
    implementations.append(objects: _*)
    this
  }

  override protected def getImpl[T](cl: Class[T]): T = implementations.find(cl.isInstance(_)) match {
    case Some(impl) ⇒ impl.asInstanceOf[T]
    case None       ⇒ super.getImpl(cl)
  }

  override def hasCustomImpl(cl: Class[_]): Boolean = implementations.exists(cl.isInstance(_))
}

trait ObjectBuilder {

  val classes: ListBuffer[Class[_]] = ListBuffer()

  val valueMap: Map[Class[_], Object] = Map(
    classOf[Byte] -> Byte.box(1),
    classOf[Short] -> Short.box(1),
    classOf[Int] -> Int.box(1),
    classOf[Long] -> Long.box(1),
    classOf[Double] -> Double.box(1),
    classOf[Boolean] -> Boolean.box(false),
    classOf[Date] -> new Date(0), //01-01-1970
    classOf[String] -> "dummy", classOf[Option[_]] -> None,
    classOf[Enumeration#Value] -> DummyEnum.dummy,
    classOf[Set[_]] -> Set.empty,
    classOf[Map[_, _]] -> Map.empty,
    classOf[List[_]] -> Nil,
    classOf[Seq[_]] -> Nil,
    classOf[Duration] -> Duration("1 second"))

  def using(objects: Object*): ObjectCache = {
    val cache = new ObjectCache
    cache.classes.appendAll(this.classes)
    cache.using(objects: _*)
    cache
  }

  def addClass[T](implicit tag: ClassTag[T]): ObjectBuilder = {
    classes.append(tag.runtimeClass)
    this
  }

  protected def getImpl[T](cl: Class[T]): T = {
    classes.find(cl.isAssignableFrom(_)) match {
      case Some(impl) ⇒ return internalCreate(impl).asInstanceOf[T]
      case None       ⇒ throw ObjectCreationException(s"dont know how to instantiate $cl")
    }
  }

  def create[T](implicit tag: ClassTag[T]): T = internalCreate(tag.runtimeClass).asInstanceOf[T]

  def hasCustomImpl(cl: Class[_]): Boolean = false

  private def internalCreate[T](cl: Class[T]): T = {
    if (cl.isInterface || Modifier.isAbstract(cl.getModifiers) || hasCustomImpl(cl)) {
      return getImpl(cl)
    }

    val cons = getBestConstructor(cl)
    createInstance(cons)
  }

  def getBestConstructor[T](cl: Class[T]): Constructor[T] = cl.getConstructors.toList match {
    case Nil        ⇒ throw ObjectCreationException(s"No constructors for $cl")
    case List(cons) ⇒ cons.asInstanceOf[Constructor[T]]
    case _          ⇒ throw ObjectCreationException(s"Cannot create instance of $cl : Too many constructors. Please add this class to the valueMap, or set a customImpl")
  }

  def createInstance[T](cons: Constructor[T]): T = {
    val values = cons.getParameterTypes.map { tp ⇒
      valueMap.get(tp) match {
        case Some(value) ⇒ value
        case None ⇒
          try {
            internalCreate(tp).asInstanceOf[Object]
          } catch {
            case ex: ObjectCreationException ⇒ throw ex
            case ex: Exception               ⇒ throw ObjectCreationException(s"Unable to create instance of $tp", ex)
          }
      }
    }

    try {
      cons.newInstance(values: _*)
    } catch {
      case ex: ObjectCreationException ⇒ throw ex
      case ex: Exception ⇒
        throw ObjectCreationException(s"Unable to create instance with constructor '$cons' with arguments ${util.Arrays.toString(values)}", ex)
    }
  }
}

object DummyEnum extends Enumeration {
  val dummy = Value
}

class ObjectCreationException(message: String, cause: Throwable) extends RuntimeException(message, cause)

object ObjectCreationException {

  def apply(msg: String, cause: Throwable = null): ObjectCreationException = {
    val realCause: Throwable = cause match {
      case ex: InvocationTargetException ⇒ ex.getTargetException
      case _                             ⇒ cause
    }
    return new ObjectCreationException(msg, realCause)
  }
}
