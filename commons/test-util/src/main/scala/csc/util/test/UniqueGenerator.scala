package csc.util.test

/**
 * Created by carlos on 08/02/16.
 */
trait UniqueGenerator {

  def generatorBase: Long = System.currentTimeMillis()

  private var counter: Long = generatorBase

  def nextLong: Long = {
    counter = counter + 1
    counter
  }

  def nextUnique: String = nextLong.toString

}

class UniqueIntGenerator(start: Long) extends UniqueGenerator {
  override def generatorBase: Long = start
  def nextInt: Int = nextLong.toInt
}
