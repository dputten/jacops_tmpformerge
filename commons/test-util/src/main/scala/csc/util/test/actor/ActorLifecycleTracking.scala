package csc.util.test.actor

import java.util
import java.util.concurrent.TimeUnit

import akka.actor.{ ActorRef, ActorSystem, PoisonPill, Props }
import scala.concurrent.Await
import akka.event.Logging.Debug
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import com.typesafe.config.ConfigFactory
import csc.util.test.actor.LifecycleType.LifecycleType

/**
 * Utility trait for tracking actor lifecycle, such as actor creation.
 * To use this, the tracker must be initialized with ''initializeLifecycleTracker''.
 * Then we can obtain the stats with ''getLifecycleStats'', and clear them (between tests or when needed) with ''clearLifecycleStats''
 * Method ''subscribeToLifecycle'' can be used at any moment to modify the lifecycle subscription
 *
 * Created by carlos on 19.11.15.
 */
trait ActorLifecycleTracking {

  def system: ActorSystem

  implicit val lifecycleTrackerTimeout = Timeout(FiniteDuration(5, TimeUnit.SECONDS))

  private var lifecycleTracker: ActorRef = null

  require(system.settings.DebugLifecycle)

  /**
   * Clears the actor lifecycle stats collected until this moment.
   */
  def clearLifecycleStats(): Unit = {
    require(lifecycleTracker != null)
    lifecycleTracker ! ClearLifecycleStats
  }

  /**
   * Creates and initializes the lifecycle tracker
   * @param types the initial lifecycle subscription
   */
  def initializeLifecycleTracker(types: Set[LifecycleType]): Unit = {
    if (lifecycleTracker != null) {
      system.eventStream.unsubscribe(lifecycleTracker)
      lifecycleTracker ! PoisonPill
    }

    lifecycleTracker = system.actorOf(Props(new ActorLifecycleTracker))
    subscribeToLifecycle(types)
    system.eventStream.subscribe(lifecycleTracker, classOf[Debug])
  }

  /**
   * Modifies the lifecycle subscription to the new one, also clearing the stats
   * @param types the new lifecycle subscription
   */
  def subscribeToLifecycle(types: Set[LifecycleType]): Unit = {
    require(lifecycleTracker != null)
    lifecycleTracker ! SubscribeLifecycle(types)
  }

  /**
   * @return current actor lifecycle stats
   */
  def getLifecycleStats(): LifecycleStats = {
    require(lifecycleTracker != null)
    val future = lifecycleTracker ? LifecycleStatsRequest
    val result = Await.result(future, lifecycleTrackerTimeout.duration)
    result.asInstanceOf[LifecycleStats]
  }

}

object ActorLifecycleTracking {

  lazy val akkaOverride: util.Map[String, Object] = {
    val map = new util.HashMap[String, Object]()
    map.put("akka.actor.debug.lifecycle", "on")
    map
  }

  lazy val akkaConfig = ConfigFactory.parseMap(akkaOverride).withFallback(ConfigFactory.defaultReference())

}

case class SubscribeLifecycle(types: Set[LifecycleType])
case object ClearLifecycleStats
case object LifecycleStatsRequest

case class LifecycleStats(events: Seq[LifecycleEvent]) {
  def count(clazz: Class[_]) = events.count(_.clazz == clazz)
}

trait LifecycleEvent {
  def path: String
  def clazz: Class[_]
  def time: Long
}

case class ActorCreation(path: String, clazz: Class[_], time: Long) extends LifecycleEvent

object LifecycleType extends Enumeration {
  type LifecycleType = Value

  val Creation = Value
}
