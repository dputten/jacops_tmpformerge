/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package testframe

import java.io.File

/**
 * Support class used to get test resources
 */
object Resources {
  def getResourcePath(resource: String): String = {
    getClass.getClassLoader.getResource(resource).getPath
  }
  def getResourceFile(resource: String): File = {
    new File(getResourcePath(resource))
  }
  def getResourceDirPath(): File = {
    val file = getResourceFile("resourcedir.txt")
    file.getParentFile
  }
}