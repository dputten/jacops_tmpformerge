package testframe
//copyright CSC 2010

import akka.actor.Actor

/**
 * This class can be used to test actors which only processes messages and doesn't generate any messages.
 * This class implements the function of sending the original message back to the sender. This way the test can wait
 * for the reply and knows the processing is done. This reduces the use of sleeps.
 * Example to use this trait:
 * val testActor = Actor.actorOf(new SystemMonitor(...) with ReplyAfterProcessing)
 * var reply = testActor !! (message, 30000)
 * reply.isDefined must be(true)
 *
 */
trait ReplyAfterProcessing extends Actor {
  abstract override def receive = {
    super.receive andThen {
      case msg ⇒ sender.tell(msg, self)
    }
  }
}