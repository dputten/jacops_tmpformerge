/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package testframe

import org.scalatest._

object Compare extends MustMatchers {
  def compareArrays(decoded: Array[Byte], src: Array[Byte]) {
    if (decoded == null) {
      if (src != null) {
        fail("decoded is null")
      }
    }
    if (src == null) {
      if (decoded != null) {
        fail("src is null")
      }
    }

    decoded.length must be(src.length)
    for (i ← 0 until src.length) {
      decoded(i) must be(src(i))
    }
  }
}