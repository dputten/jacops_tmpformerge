
import sbt._
import commons._
import csc.sbt._
import csc.sbt.dep._
import csc.sbt.CommonSettings._

import CommonsBuild._


lazy val defaultSettings: Seq[sbt.Def.Setting[_]] = commonSettings ++ Seq(

//    resolvers += "Cloudera Repo" at "https://repository.cloudera.com/artifactory/cloudera-repos/",
  resolvers := getResolvers("group_config"),
  externalResolvers <<= resolvers map { rs => Resolver.withDefaultResolvers(rs, mavenCentral = useMavenCentral) },
  offline := isOffline,

  //offline := true,
  //  parallelExecution in Test := System.getProperty("ctsSensor.parallelExecution", "false").toBoolean,

  // for excluding tests by name (or use system property: -csc.test.names.exclude=TimingSpec)
  excludeTestNames := {
    val exclude = System.getProperty("csc.test.names.exclude", "")
    if (exclude.isEmpty) Seq.empty else exclude.split(",").toSeq
  },

  // for excluding tests by tag (or use system property: -csc.tags.exclude=timing)
  excludeTestTags := {
    val exclude = System.getProperty("csc.test.tags.exclude", "")
    if (exclude.isEmpty) defaultExcludedTags else exclude.split(",").toSeq
  },

  // for including tests by tag (or use system property: -csc.test.tags.include=timing)
  includeTestTags := {
    val include = System.getProperty("csc.test.tags.include", "")
    if (include.isEmpty) Seq.empty else include.split(",").toSeq
  },

  topProjectFolder := commons.base
)

lazy val builder = new ProjectBuilder(defaultSettings)

lazy val IntegrationTest = config("it") extend(Test)

lazy val commons = Project(
  id = "commons",
  base = file("."),
  settings = parentSettings ++ Publish.versionSettings ++ Seq(
//    parallelExecution in GlobalScope := System.getProperty("ctsSensor.parallelExecution", "false").toBoolean,
    Publish.defaultPublishTo in ThisBuild <<= crossTarget / "repository"
  ),
  aggregate = Seq(akkaUtil, testUtil, amqpUtil, dconfig, dlog, dlogActor, curatorTestUtils, curatorUtils)
)

lazy val r = CommonsRuntimeDeps
lazy val t = CommonsTestDeps

import CommonsDeps._

lazy val akkaUtil = builder.proj(
  "akka-util",
  Seq(akkaActor, akkaKernel, akkaCamel, r.camelMina, r.mina, netty, t.akkaTestKit, t.scalatest, akkaSlf4j)
)

lazy val amqpUtil = builder.proj(
  "amqp-util",
  Seq(amqpClient, liftJson, t.junit, akkaKernel, akkaActor, t.akkaTestKit, slf4jLog4j, logback, akkaCamel)
) dependsOn(akkaUtil, testUtil)

lazy val testUtil = builder.proj(
  "test-util",
  Seq(t.scalatestC, t.junitC, t.transactC, t.persistenceC, akkaActor, t.akkaTestKitC, commonsIO)
)

lazy val dconfig = builder.proj(
  "dconfig",
  Seq(zookeeper, liftjson, commonsCodec, commonsIO, slf4jApi, t.junit, t.scalatest)
) dependsOn(curatorTestUtils)

lazy val dlog = builder.proj(
  "dlog",
  Seq(zookeeper, curator, logback, commonsLogging, t.junit,
    hbaseCommon, hbaseClient, hadoopCommon, hadoopHdfs, t.scalatest)
) dependsOn(dconfig, curatorUtils, curatorTestUtils)

lazy val dlogActor = builder.proj(
  "dlog-actor",
  Seq(akkaActor, t.akkaTestKit)
) dependsOn(dlog, curatorTestUtils)

lazy val curatorTestUtils = builder.proj(
  "curator-test-utils",
  Seq(zookeeper, netty, liftjson, commonsCodec, commonsIO, slf4jApi, t.junitC, curatorTest, curator, t.scalatestC)
)

lazy val curatorUtils = builder.proj(
  "curator-utils",
  Seq(akkaActor, liftjson, t.scalatest, t.akkaTestKit, curator)
) dependsOn(curatorTestUtils, dconfig)
