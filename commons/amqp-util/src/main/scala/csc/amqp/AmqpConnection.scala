package csc.amqp

import scala.concurrent.duration._

import akka.actor.{ Props, ActorRef, ActorSystem }
import com.rabbitmq.client.{ Channel, DefaultConsumer, ConnectionFactory }
import scala.concurrent.duration.FiniteDuration
import com.github.sstone.amqp.Amqp.ChannelParameters
import com.github.sstone.amqp._
import com.typesafe.config.Config
import csc.akka.remote.SocketServer
import csc.akka.config.ConfigUtil._

/**
 * Created by carlos on 19/08/16.
 */
class MQConnection(host: String = "localhost",
                   port: Int = 5672,
                   vhost: String = "/",
                   user: String = "guest",
                   password: String = "guest",
                   name: String,
                   reconnectionDelay: FiniteDuration = 10000 millis)(implicit system: ActorSystem) {

  private lazy val connFactory: ConnectionFactory = {
    val factory = new ConnectionFactory()
    factory.setHost(host)
    factory.setPort(port)
    factory.setUsername(user)
    factory.setPassword(password)
    factory.setVirtualHost(vhost)
    factory
  }

  private lazy val conn: ActorRef =
    system.actorOf(ConnectionOwner.props(connFactory, reconnectionDelay))

  lazy val producer: ActorRef = {
    val prod = ConnectionOwner.createChildActor(conn, ChannelOwner.props())
    Amqp.waitForConnection(system, prod).await()
    prod
  }

  def createSimpleConsumer(queueName: String, listener: ActorRef, channelParams: Option[ChannelParameters], autoack: Boolean): ActorRef = {
    val props = Props(new SimpleConsumer(queueName, Some(listener), autoack, channelParams))
    createConsumer(props)
  }

  def createConsumer(props: Props): ActorRef = {
    val consumer = ConnectionOwner.createChildActor(conn, props)
    Amqp.waitForConnection(system, consumer).await()
    consumer
  }

}

class SimpleConsumer(queueName: String, listener: Option[ActorRef], autoack: Boolean, channelParams: Option[ChannelParameters])
  extends Consumer(listener, autoack, channelParams = channelParams) {

  private def bindToQueue(consumer: DefaultConsumer, queueName: String) = {
    val channel = consumer.getChannel
    channel.basicConsume(queueName, autoack, consumer)
  }

  override def onChannel(channel: Channel, forwarder: ActorRef): Unit = {
    super.onChannel(channel, forwarder)
    bindToQueue(consumer.get, queueName)
  }
}

object MQConnection {
  def create(actorName: String, uriString: String, reconnectDelay: FiniteDuration)(implicit system: ActorSystem): MQConnection = {
    val cf = new ConnectionFactory()
    cf.setUri(uriString)
    if (cf.getVirtualHost == "") cf.setVirtualHost("/")
    new MQConnection(host = cf.getHost, port = cf.getPort, vhost = cf.getVirtualHost, user = cf.getUsername,
      password = cf.getPassword, name = actorName, reconnectionDelay = reconnectDelay)
  }
}

class AmqpConnection(config: ConnectionConfig)(implicit system: ActorSystem) {

  lazy val connection = MQConnection.create(config.actorName, config.amqpUri, config.reconnectDelay)

  def queue(svcConfig: ServicesConfig): QueueConnection =
    new QueueConnection(connection, svcConfig)(system)

  def init(): Unit = {
    connection.producer //just triggers initialization of connection/producer
  }
}

class QueueConnection(connection: MQConnection, serviceConfig: ServicesConfig)(implicit system: ActorSystem) {

  lazy val channelParams = Some(ChannelParameters(serviceConfig.serversCount))

  def wireConsumer(actor: ActorRef, autoAck: Boolean = false): Unit = {
    connection.createSimpleConsumer(serviceConfig.consumerQueue, actor, channelParams, autoAck)
  }

}

trait ConnectionConfig {

  def actorName: String

  def amqpUri: String

  def reconnectDelay: FiniteDuration

}

trait ServicesConfig {

  def consumerQueue: String

  def producerEndpoint: String

  def producerRouteKey: String

  def serversCount: Int

}

case class AmqpConfiguration(actorName: String, amqpUri: String, reconnectDelay: FiniteDuration) extends ConnectionConfig {
  def this(config: Config) = this(
    config.getString("actor-name"),
    config.getString("amqp-uri"),
    config.getFiniteDuration("reconnect-delay"))

  lazy val server = SocketServer(amqpUri)

}

object AmqpConfiguration {

  def fromLegacyConfig(config: Config): AmqpConfiguration = AmqpConfiguration(
    config.getString("actorName"),
    config.getString("amqpUri"),
    config.getLong("reconnectDelay_ms").millis)

}

case class ServicesConfiguration(consumerQueue: String,
                                 producerEndpoint: String,
                                 producerRouteKey: String,
                                 serversCount: Int) extends ServicesConfig {
  def this(config: Config) = this(
    config.getString("consumer-queue"),
    config.getStringOpt("producer-endpoint").getOrElse("undefined"),
    config.getStringOpt("producer-route-key").getOrElse("undefined"),
    config.getIntOpt("servers-count").getOrElse(1))

}
