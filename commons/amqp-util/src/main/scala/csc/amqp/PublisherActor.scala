/*
 * Copyright (C) 2015. CSC <http://www.csc.com>
 */

package csc.amqp

import java.util.concurrent.TimeUnit

import akka.actor._
import akka.pattern.ask
import akka.util.Timeout
import com.github.sstone.amqp.Amqp.{ Ok, Publish }
import com.rabbitmq.client.AMQP
import akka.event.{ LoggingAdapter, LoggingReceive }

import scala.concurrent.ExecutionContext
import scala.util.{ Failure, Success }

/**
 * Base actor class for sending AMQP messages with logging. It has two helper functions which can be used to send
 * a message through the `producer` passed in the ''constructor''. Using [[akka.pattern.ask]] it will wait for the
 * outcome of message publishing and log error if any
 * @author csomogyi
 */
abstract class PublisherActor extends Actor with ActorLogging with AmqpSerializers with Publisher {

  val producer: ActorRef
  val producerEndpoint: String
  def producerRouteKey(msg: AnyRef): String
  def messageToSend(msg: AnyRef): Option[AnyRef] = Some(msg)

  implicit val executor: ExecutionContext = context.dispatcher

  def receive: Receive = {
    case msg: AnyRef ⇒ handlePublishMessage(msg)
  }

  /**
   * Handles the given message, trying to publish it.
   * @param msg
   * @return true if message was published
   */
  def handlePublishMessage(msg: AnyRef): Boolean = messageToSend(msg) match {
    case None ⇒
      log.warning("Message {} was not considered for publishing. Discarded", msg)
      false
    case Some(response) ⇒ {
      val payload = toPublishPayload[response.type](response)
      val responseName = response.getClass.getName
      val routeKey = producerRouteKey(msg)
      publishOrError(producerEndpoint, routeKey, payload, responseName)
      payload match {
        case Right((_, props)) ⇒ log.debug("Response {} ({}) sent with routing key {}", props.getMessageId, responseName, routeKey)
        case Left(error)       ⇒ log.warning("Failed to create payload for {}: {}", responseName, error)
      }
      payload.isRight
    }
  }
}

trait Publisher {

  val producerTimeout = Timeout(5, TimeUnit.SECONDS) // 5 seconds should be enough to the producer

  def log: LoggingAdapter

  def producer: ActorRef

  implicit val executor: ExecutionContext

  /**
   * Publishes a response via the producer
   *
   * @param producerEndpoint the endpoint / exchange parameter of the message publishing
   * @param producerRouteKey the route key parameter of the message publishing
   * @param body the body part of the message to be published
   * @param properties the properties part of the message to be published
   */
  def publishResponse(producerEndpoint: String, producerRouteKey: String,
                      body: Array[Byte], properties: AMQP.BasicProperties): Unit = {

    val msgId = properties.getMessageId
    val publish = Publish(producerEndpoint, producerRouteKey, body, Some(properties))
    implicit val timeout = producerTimeout
    producer ? publish onComplete {
      case Success(response) ⇒ {
        response match {
          case _: Ok    ⇒ log.debug("Message {} ({}) sent successfully", msgId, properties.getType)
          case msg: Any ⇒ log.error("Failed to send response {} ({}) - {}", msgId, properties.getType, msg)
        }
      }
      case Failure(e) ⇒ {
        log.error(e, "Failed to send response {} ({}) - producer timeout", msgId, properties.getType)
      }
    }
  }

  /**
   * Expects a `toPublishPayload()` result from the serializer which can be Either payload or error String.
   * Upon error it logs an error with the message name specified in responseName field
   *
   * @param producerEndpoint the endpoint / exchange parameter of the message publishing
   * @param producerRouteKey the route key parameter of the message publishing
   * @param payload the payload to be sent or log error upon error
   * @param responseName the name of the response to be logged upon error
   */
  def publishOrError(producerEndpoint: String, producerRouteKey: String,
                     payload: AmqpSerializers.PayloadType,
                     responseName: String): Unit = {
    payload match {
      case Right(pl) ⇒ {
        val (body, properties) = pl
        publishResponse(producerEndpoint, producerRouteKey, body, properties)
      }
      case Left(error) ⇒ log.error("Cannot send {} / serialization error - {}", responseName, error)
    }
  }
}
