/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.amqp

import net.liftweb.json.JsonAST.JString
import net.liftweb.json._

/**
 * Serializer for Enumeration types, taken from the mighty internet.
 */
class EnumerationSerializer(enums: Enumeration*) extends Serializer[Enumeration#Value] {

  val EnumerationClass = classOf[Enumeration#Value]

  val formats = Serialization.formats(NoTypeHints)

  val classifierSeparator = "$"

  def deserialize(implicit format: Formats): PartialFunction[(TypeInfo, JValue), Enumeration#Value] = {
    case (TypeInfo(EnumerationClass, _), json) ⇒ json match {
      case JString(value) ⇒ {
        val idx = value.indexOf(classifierSeparator)

        if (idx < 0) {
          //not classified: legacy
          legacyDeserializeString(value)
        } else {
          //classified (<enum><separator><value>)
          val targetEnum = value.substring(0, idx) + "$" //enum classes always end with $
          enums.find(_.getClass.getSimpleName == targetEnum) match {
            case None       ⇒ throw new MappingException("Could not find enum class for " + value)
            case Some(enum) ⇒ enum.withName(value.substring(idx + 1))
          }
        }
      }
      case value ⇒ throw new MappingException("Can't convert " + value + " to " + EnumerationClass)
    }
  }

  def serialize(implicit format: Formats): PartialFunction[Any, JValue] = {
    case i: Enumeration#Value ⇒ JString(nameOf(i))
  }

  private def legacyDeserializeString(value: String) = enums.find(_.values.exists(_.toString == value)).get.withName(value)

  /**
   * @param value
   * @return string for enumeration value
   */
  private def nameOf(value: Enumeration#Value): String = {
    //Using reflection to retrieve enumeration class from a value.
    //Verified it works in the most recent scala version (2.11.7)
    val field = value.getClass.getField("$outer")
    field.get(value) + classifierSeparator + value.toString //fully classified name will have the form enum$value
  }
}
