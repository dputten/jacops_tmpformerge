/*
 * Copyright (C) 2015. CSC <http://www.csc.com>
 */

package csc.amqp

import java.nio.charset.Charset
import java.util.{ UUID, Date }

import com.github.sstone.amqp.Amqp.Delivery
import com.rabbitmq.client.AMQP
import akka.event.LoggingAdapter

/**
 * Trait for serializing (`toPublishPayload`) and deserializing (`toMessage`) AMQP messages. [[AmqpSerializers.ApplicationId]]
 * must be overridden. It's used to specify the app-id of the outgoing message.
 *
 * The message format:
 * - '''content-type''' is or should be set to `application/json`
 * - the '''content-encoding''' is `UTF-8` by default but it can be over-defined by overriding [[AmqpSerializers.DefaultEncoding]]
 *   otherwise '''content-encoding''' field is respected during deserialization
 * - '''application id''' is set to [[AmqpSerializers.ApplicationId]]
 * - '''timestamp''' is set to the current time on sending
 * - '''type''' must be the simple name of the class being serialized
 * - the '''payload / body''' is a string containing the JSON serialization of the class object; the string is encoded into
 *   byte array using the ''content-encoding''
 *
 * @author csomogyi
 */
trait AmqpSerializers extends JsonSerializers {
  //val log: LoggingAdapter
  def log: LoggingAdapter
  val ApplicationId: String

  val DefaultEncoding = "UTF-8"

  import AmqpSerializers.PayloadType

  /**
   * Builds standard properties. ApplicationId will be assigned as application ID and Content-Type and Encoding is set
   * to "application/json" and DefaultEncoding respectively.
   *
   * @param messageType
   * @return
   */
  protected def buildProperties(messageType: String): AMQP.BasicProperties.Builder = {
    new AMQP.BasicProperties.Builder().appId(ApplicationId)
      .contentType("application/json").contentEncoding(DefaultEncoding)
      .timestamp(new Date()).`type`(messageType).messageId(UUID.randomUUID().toString).deliveryMode(2)
  }

  /**
   * Properties must contain the expected message type and Content-Type should be "application/json"
   *
   * @param delivery
   * @param messageType
   * @return
   */
  protected def checkProperties(delivery: Delivery, messageType: String): Option[String] = {
    val properties = delivery.properties
    if (properties.getType != messageType)
      return Some("Invalid message type '" + properties.getType + "'")
    if (properties.getContentType != "application/json")
      return Some("Invalid message content type '" + properties.getContentType + "'")
    None
  }

  def extractContentEncoding(delivery: Delivery): Either[String, Charset] = {
    val rawEncoding = delivery.properties.getContentEncoding
    // assuming default encoding
    val encoding = if (rawEncoding != null && !rawEncoding.isEmpty) rawEncoding else DefaultEncoding
    try {
      Right(Charset.forName(encoding))
    } catch {
      case e: Exception ⇒ Left("Unsupported character encoding '" + encoding + "'")
    }
  }

  def extractMessageType(delivery: Delivery): Option[String] = {
    val rawType = delivery.properties.getType
    if (rawType != null && !rawType.isEmpty) Some(rawType) else None
  }

  /**
   * Creates a publish message payload from the specified type or error message upon failure of serialization
   *
   * @param response
   * @tparam T
   * @return
   */
  def toPublishPayload[T <: AnyRef: Manifest](response: T): PayloadType = {
    val builder = buildProperties(manifest[T].runtimeClass.getSimpleName)
    toJson[T](response) match {
      case Left(error) ⇒ Left(error)
      case Right(json) ⇒ Right(json.getBytes(DefaultEncoding), builder.build())
    }
  }

  /**
   * Deserializes AMQP Delivery into message or returns error message upon failure
   *
   * @param delivery
   * @tparam T
   * @return
   */
  def toMessage[T <: AnyRef: Manifest](delivery: Delivery): Either[String, T] = {
    checkProperties(delivery, manifest[T].runtimeClass.getSimpleName) match {
      case Some(error) ⇒ Left(error)
      case None ⇒ {
        extractContentEncoding(delivery) match {
          case Left(error) ⇒ Left(error)
          case Right(encoding) ⇒ {
            fromJson[T](new String(delivery.body, encoding)) match {
              case Left(error)    ⇒ Left(error)
              case Right(message) ⇒ Right(message)
            }
          }
        }
      }
    }
  }

  /**
   * Abstract class used to create the message extractors
   * @param t manifest of generic class
   * @tparam T tne generic class
   */
  abstract class MessageExtractor[T <: AnyRef](implicit t: reflect.Manifest[T]) {
    def unapply(delivery: Delivery): Option[T] = {
      val className = t.runtimeClass.getSimpleName
      delivery.properties.getType match {
        case `className` ⇒ {
          toMessage[T](delivery) match {
            case Right(message) ⇒ Some(message)
            case Left(error) ⇒ {
              log.error("Cannot decode request ({}): {}", delivery.envelope.getDeliveryTag, error)
              None
            }
          }
        }
        case other ⇒ None
      }
    }
  }

}

object AmqpSerializers {
  type PayloadType = Either[String, (Array[Byte], AMQP.BasicProperties)]
  type JsonDeserializer = String ⇒ Either[String, AnyRef] //matches any call to fromJson
  type Deserializer = Delivery ⇒ Either[String, AnyRef] //matches any call to toMessage
}