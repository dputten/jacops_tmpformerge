package csc.amqp.adapter

import akka.actor.{ Actor, ActorLogging, ActorRef }
import com.github.sstone.amqp.Amqp.{ Ack, Delivery, Reject }
import csc.amqp.AmqpSerializers

/**
 * Created by carlos on 01/09/16.
 */
trait MQListener extends Actor with ActorLogging with AmqpSerializers {

  type Request <: CorrelatedMessage
  type Response <: CorrelatedMessage

  def handler: ActorRef
  def producer: ActorRef
  def ownReceive: Receive

  implicit val system = context.system

  var pendingAcks: Map[String, Item] = Map.empty

  override val ApplicationId: String = getClass.getSimpleName

  override def receive: Receive = ownReceive orElse fallbackReceive

  def handleRequest(req: Request, delivery: Delivery): Unit = {
    log.info("Handing request {}", req)
    handler ! req
    pendingAcks = pendingAcks.updated(req.msgId, Item(delivery.envelope.getDeliveryTag, sender)) //add to pending acks
  }

  def handleResponse(res: Response): Unit = {
    pendingAcks.get(res.msgId) foreach { item ⇒
      pendingAcks = pendingAcks - res.msgId //remove from pending acks
      item.ackTo ! Ack(item.deliveryTag) //ack the request
    }
    log.info("Sending response {}", res)
    producer ! res //send the response to MQ
  }

  def fallbackReceive: Receive = {
    case delivery: Delivery ⇒ handleUnmatched(delivery)
  }

  def handleUnmatched(delivery: Delivery): Unit = {
    log.error("Dropping message with tag {} - cannot determine message type", delivery.envelope.getDeliveryTag)
    sender ! Reject(delivery.envelope.getDeliveryTag, false)
  }

  case class Item(deliveryTag: Long, ackTo: ActorRef)
}
