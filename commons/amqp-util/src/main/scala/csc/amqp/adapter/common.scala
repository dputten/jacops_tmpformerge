package csc.amqp.adapter

import akka.actor.{ Actor, ActorLogging, ActorRef, OneForOneStrategy }
import akka.actor.SupervisorStrategy.Restart
import com.github.sstone.amqp.Amqp.Delivery
import csc.amqp.{ AmqpSerializers, PublisherActor }

import scala.concurrent.duration.FiniteDuration

/**
 * A simple MQ producer.
 *
 * Make sure to override formats if your messages to send contain Enumeration fields
 */

object MQProducer {
  type RouteKeyFunction = AnyRef ⇒ String

  def prefixedRoute(prefix: String): RouteKeyFunction = { obj ⇒
    s"$prefix.${obj.getClass.getSimpleName}"
  }

  object ClassnameRoute extends RouteKeyFunction {
    override def apply(obj: AnyRef): String = obj.getClass.getSimpleName
  }

}

case class HeartbeatMessage(component: String, time: Long)
case object Ping
case object Pong

import MQProducer._

trait MQProducer extends PublisherActor with ActorLogging {

  def routeKeyFunction: RouteKeyFunction

  override val ApplicationId = getClass.getSimpleName

  override val supervisorStrategy = OneForOneStrategy() {
    case _: Exception ⇒ Restart
  }

  override def producerRouteKey(msg: AnyRef): String = routeKeyFunction(msg)
}

trait HeartbeatAwareMQProducer extends MQProducer {

  def handler: ActorRef
  def componentName: String
  def heartbeatFreq: FiniteDuration

  context.system.scheduler.schedule(heartbeatFreq, heartbeatFreq, self, CheckAlive) //set the rhythm, keep the beat

  override def receive: Receive = ownReceive orElse super.receive

  def ownReceive: Receive = {
    case CheckAlive ⇒ handler ! Ping //ask for a Pong
    case Pong       ⇒ self ! HeartbeatMessage(componentName, now) //publishing heartbeat
  }

  def now = System.currentTimeMillis()

  object CheckAlive
}

trait MQConsumer extends Actor with ActorLogging with AmqpSerializers {

  implicit val system = context.system
  override val ApplicationId = getClass.getSimpleName

  def target: ActorRef
  def ownReceive: Receive

  override def receive: Receive = ownReceive orElse fallbackReceive

  def fallbackReceive: Receive = {
    case delivery: Delivery ⇒ handleUnknownDelivery(delivery)
  }

  def handleUnknownDelivery(delivery: Delivery): Unit = {
    log.error("Dropping message with tag {} - cannot determine message type", delivery.envelope.getDeliveryTag)
    log.debug("Dropped message content is {}", new String(delivery.body))
  }
}

class PrefixedRouteKeyFunction(prefix: String) extends Function[AnyRef, String] {
  override def apply(obj: AnyRef): String = s"${prefix}.${obj.getClass.getName}"
}

trait CorrelatedMessage {
  def msgId: String
  def time: Long
}
