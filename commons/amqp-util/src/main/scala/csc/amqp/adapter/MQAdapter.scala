package csc.amqp.adapter

import akka.actor.{ Actor, ActorLogging, ActorRef }
import com.github.sstone.amqp.Amqp.Delivery

import scala.concurrent.duration._
import scala.concurrent.duration.FiniteDuration

trait MQAdapter extends Actor with ActorLogging {

  implicit val executor = context.system.dispatcher

  def producer: ActorRef
  def ownReceive: Receive
  def cleanupFrequency: FiniteDuration
  def dropTarget: Option[ActorRef] = None

  var senderStore = Map[String, Store]()

  context.system.scheduler.schedule(0 millis, cleanupFrequency, self, StoreCleanup)

  def dropped(msg: Any): Unit = dropTarget match {
    case None        ⇒ context.system.deadLetters ! msg
    case Some(actor) ⇒ actor ! msg
  }

  override def receive: Receive = ownReceive orElse fallbackReceive

  def fallbackReceive: Receive = {
    case StoreCleanup       ⇒ cleanupStore(System.currentTimeMillis())
    case delivery: Delivery ⇒ handleUnmatched(delivery)
  }

  def handleUnmatched(delivery: Delivery): Unit = {
    log.error("Dropping message with tag {} - cannot determine message type", delivery.envelope.getDeliveryTag)
    //sender ! Reject(delivery.envelope.getDeliveryTag, false)
  }

  def handleRequest(req: CorrelatedMessage): Unit = {
    senderStore = senderStore.updated(req.msgId, Store(sender, req.msgId, System.currentTimeMillis()))
    log.debug("Publishing request {}. SenderStore size is now {}", req, senderStore.size)
    producer ! req
  }

  def handleResponse(res: CorrelatedMessage): Unit = {
    log.debug("Handling response {}", res)

    //heartbeat foreach { t ⇒ context.system.eventStream.publish(RecognizerHeartbeat(t)) }

    senderStore.get(res.msgId) match {
      case None ⇒
        log.warning("Didn't find request with id {}. Discarding response {}", res.msgId, res)
        dropped(res)

      case Some(store) ⇒
        store.client ! res
        senderStore = senderStore - res.msgId

        val timeDiff = res.time - store.time
        log.info("Recognizer response for message [{}] took {} millis", res.msgId, timeDiff)
    }

    log.debug("SenderStore size is now {}", senderStore.size)
  }

  def cleanupStore(time: Long) {
    val tooOldTimeLimit = time - cleanupFrequency.toMillis
    val (del, current) = senderStore.partition(_._2.time < tooOldTimeLimit)

    del foreach { s ⇒
      dropped(DroppedMessage(s._1, s._2.client.path.toString))
    }

    log.info("Deleting old requests without reply: {}", del)

    senderStore = current
  }

  case class Store(client: ActorRef, id: String, time: Long)

  case object StoreCleanup

}

case class DroppedMessage(msgId: String, clientPath: String)
