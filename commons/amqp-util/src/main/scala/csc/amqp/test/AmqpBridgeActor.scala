package csc.amqp.test

import java.nio.charset.Charset
import java.util.Date
import java.util.concurrent.TimeUnit

import akka.actor.{ ActorLogging, Actor, ActorRef }
import akka.pattern.ask
import akka.util.Timeout
import com.github.sstone.amqp.Amqp.{ Delivery, Ack, Reject, Publish }
import com.rabbitmq.client.{ AMQP, Envelope }
import csc.amqp.AmqpSerializers.JsonDeserializer
import csc.amqp.JsonSerializers
import csc.amqp.test.AmqpBridgeActor.MatcherLogic
import net.liftweb.json.Formats

import scala.concurrent.ExecutionContext
import scala.util.{ Failure, Success }

/**
 * This utility actor behaves as bridge between a client actor and any actor (the target) receiving AMQP deliveries.
 * To the client, it hides all the MQ setup by mocking it, appearing as the target actor we want to test
 * To the target actor, this one behaves as both the consumer (wrapping original messages inside Delivery)
 * and producer (extracting messages from Publish).
 *
 * To use it, send the plain message to this actor, and expect an Outcome message as response.
 * The Outcome can be Acked, Rejected or Failure, depending if the target actor sent an Ack, a Reject or some error
 *
 * This actor can also handle requests/responses.
 * So if the target actor also produces responses, you should pass the MatcherLogic and deserializers.
 *
 * MatcherLogic is a function responsible for identifying responses, requests, and when they are fulfilled
 * Deserializers is a map of response type to the deserializer (json String -> Message)
 *
 * Created by carlos on 16.10.15.
 */
class AmqpBridgeActor(target: ⇒ ActorRef,
                      jsonFormats: Formats,
                      matcher: MatcherLogic = AmqpBridgeActor.noMatcher,
                      deserializers: Map[String, JsonDeserializer] = Map.empty,
                      autoAck: Boolean = false)
  extends Actor with ActorLogging with JsonSerializers {

  val encoding = "UTF-8"
  val contentType = "application/json"

  implicit val timeout: Timeout = Timeout(1, TimeUnit.SECONDS)
  implicit def executor: ExecutionContext = context.dispatcher

  override implicit val formats = jsonFormats

  override def receive: Receive = receiveWithState(State(0, Map.empty))

  def receiveWithState(state: State): Receive = {
    case msg: Publish ⇒ {
      val newState = handlePublish(msg, state)
      context.become(receiveWithState(newState))
    }
    case msg: AnyRef ⇒
      val newState = handleMessage(msg, state)
      context.become(receiveWithState(newState))
  }

  /**
   * Handles a publish, extracting the message inside and checking if it matches a pending request.
   * If so, sends the message to the client.
   *
   * @param msg
   * @param state
   * @return new state
   */
  def handlePublish(msg: Publish, state: State): State = {
    var requestMap = state.requestMap

    extractMessage(msg) match {
      case Right(response) ⇒ {
        matcher(response) match {
          case Some((ref, _, last)) ⇒
            requestMap.get(ref) match {
              case Some(ReqEntry(multi, client)) ⇒ {
                client ! response
                if (last || !multi) {
                  requestMap = requestMap - ref //last response: remove request from map
                }
              }
              case None ⇒ log.warning("Could not find client for {}. Response lost: {}", ref, response.getClass)
            }
          case None ⇒ log.warning("Message is not matcheable (not a response?). Lost: {}", response)
        }
      }
      case Left(error) ⇒ log.error("Error processing response: " + error)
    }

    state.copy(requestMap = requestMap)
  }

  /**
   * Handles any message, by creating a Delivery and asking it to the target actor.
   * Handles the ask response (Ack/Reject or failure) by replying to the client accordingly
   *
   * @param msg
   * @param state
   * @return
   */
  def handleMessage(msg: AnyRef, state: State): State = {
    val client = sender
    val tag = state.lastDeliveryTag + 1
    var requestMap = state.requestMap

    createDelivery(msg, tag) match {
      case Right(delivery) ⇒ {
        //check if message is matching a response
        matcher(msg) match {
          case Some((ref, multi, _)) ⇒
            requestMap = requestMap.updated(ref, ReqEntry(multi, client)) //add to request map
          case other ⇒
        }

        if (autoAck) {
          target ! delivery
          client ! Acked
        } else {
          val future = target ? delivery
          future.onComplete {
            case Success(_: Ack)    ⇒ client ! Acked
            case Success(_: Reject) ⇒ client ! Rejected
            case Failure(ex)        ⇒ client ! Failed(ex.getMessage)
            case other              ⇒ log.warning("Unexpectd outcome: {}", other)
          }
          //scala 2.9.1
          //          future onSuccess {
          //            case _: Ack    ⇒ client ! Acked
          //            case _: Reject ⇒ client ! Rejected
          //          } onFailure {
          //            case error: Throwable ⇒ client ! Failure(error.getMessage)
          //          }
        }
      }
      case Left(error) ⇒ client ! Failed(error)
    }
    //TODO csilva: clean requestMap of requests rejected or failed (will never get a response to clear it)
    state.copy(lastDeliveryTag = tag, requestMap = requestMap)
  }

  def extractMessage(msg: Publish): Either[String, AnyRef] = {
    msg.properties match {
      case Some(props) ⇒ {
        if (props.getContentType != contentType) Left("Unexpected content type: " + props.getContentType)
        else Option(props.getType) match {
          case None ⇒ Left("No message type defined")
          case Some(tp) ⇒ {
            deserializers.get(tp) match {
              case None               ⇒ Left("No deserializer defined for type " + tp)
              case Some(deserializer) ⇒ deserializer(bodyAsString(msg))
            }
          }
        }
      }
      case None ⇒ Left("No properties")
    }
  }

  def bodyAsString(msg: Publish): String = {
    val enc = Option(msg.properties.get.getContentEncoding).getOrElse(encoding)
    new String(msg.body, Charset.forName(enc))
  }

  def createDelivery(msg: AnyRef, tag: Long): Either[String, Delivery] = {
    try {
      val envelope: Envelope = new Envelope(tag, false, "exchange", "routingKey")
      val properties = buildProperties(msg.getClass.getSimpleName).build()
      toJson(msg) match {
        case Right(json) ⇒ Right(Delivery(tag.toString, envelope, properties, json.getBytes(encoding)))
        case Left(error) ⇒ Left(error)
      }
    } catch {
      case ex: Throwable ⇒ Left(ex.getMessage)
    }
  }

  def buildProperties(messageType: String): AMQP.BasicProperties.Builder = {
    new AMQP.BasicProperties.Builder().appId("applicationId")
      .contentType(contentType).contentEncoding(encoding)
      .timestamp(new Date()).`type`(messageType)
  }

  case class ReqEntry(multiResponse: Boolean, client: ActorRef)
  case class State(lastDeliveryTag: Long, requestMap: Map[String, ReqEntry])

}

trait Outcome
case class Failed(error: String) extends Outcome
object Acked extends Outcome
object Rejected extends Outcome

object AmqpBridgeActor {
  type MatcherLogic = AnyRef ⇒ Option[Tuple3[String, Boolean, Boolean]] //tuple contains refId, multiResponse, lastResponse
  val noMatcher: MatcherLogic = _ ⇒ None
}
