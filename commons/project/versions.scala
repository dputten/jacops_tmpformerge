package commons

import csc.sbt.dep._
import sbt._

object CommonsVersions extends CommonVersions {

  //override val project: String = "jacops-1.0.0.2"

}

trait CommonsVersionContext extends VersionContext {
  def v = CommonsVersions
}

object CommonsDeps
  extends CommonsVersionContext
    with Dependency
    with CalderaDependency
    with UnclassifiedDependency

// Runtime
object CommonsRuntimeDeps extends CommonsVersionContext with RuntimeDependency

// Test
object CommonsTestDeps extends CommonsVersionContext with TestDependency {

  val akkaTestKitC = akkaTestKitUnclassified

}

