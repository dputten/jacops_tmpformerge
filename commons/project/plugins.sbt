
offline := NexusHelper.isOffline

resolvers ++= NexusHelper.getResolvers("group_config")

//resolvers += "CSC traffic" at "http://cscappamd911.nl.emea.csc.com:8082/nexus/content/groups/group_config"

//resolvers += Resolver.url("artifactory", url("http://cscappamd911.nl.emea.csc.com:8082/nexus/content/groups/ivy-release/"))(Resolver.ivyStylePatterns)

addSbtPlugin("com.csc.sbt" % "sbt-csc-base" % "1.0.0-SNAPSHOT")

//addSbtPlugin("com.typesafe.sbt" % "sbt-multi-jvm" % "0.3.11")

addSbtPlugin("com.typesafe.sbt" % "sbt-scalariform" % "1.3.0")

addSbtPlugin("com.github.gseitz" % "sbt-release" % "1.0.0")

addSbtPlugin("com.github.mpeltonen" % "sbt-idea" % "1.6.0")

//addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "0.8.0") //this is the highest version supported by java 6

addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.8.2")

//addSbtPlugin("com.typesafe.schoir" % "schoir" % "0.1.1")

//addSbtPlugin("com.typesafe.sbteclipse" % "sbteclipse" % "1.5.0")

//addSbtPlugin("me.lessis" % "ls-sbt" % "0.1.1")




