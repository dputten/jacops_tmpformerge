package commons

import sbt._
import sbt.Keys._
import csc.sbt._
import com.typesafe.sbt.SbtNativePackager._
import com.typesafe.sbt.SbtScalariform
import com.typesafe.sbt.SbtScalariform.ScalariformKeys

object CommonsBuild extends Build with CommonSettings with NexusHelper {

  lazy val buildSettings = Seq(
    organization := "com.csc.traffic",
    version      := CommonsVersions.project,
    scalaVersion := CommonsVersions.scala
  )

  // Settings
  override lazy val settings = super.settings ++ buildSettings ++ Seq(
    resolvers += Resolver.defaultLocal
  )

  override def formatSettings: Seq[Setting[_]] = SbtScalariform.scalariformSettings ++ Seq(
    ScalariformKeys.preferences in Compile := formattingPreferences,
    ScalariformKeys.preferences in Test    := formattingPreferences
  )

  lazy val formattingPreferences = {
    import scalariform.formatter.preferences._
    FormattingPreferences()
      .setPreference(RewriteArrowSymbols, true)
      .setPreference(AlignParameters, true)
      .setPreference(AlignSingleLineCaseStatements, true)
  }

}
