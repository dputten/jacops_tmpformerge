package csc.curator.utils

import akka.actor.ActorSystem
import net.liftweb.json.{ DefaultFormats }
import csc.json.lift.EnumerationSerializer
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import csc.curator.CuratorTestServer

import org.scalatest.MustMatchers
import akka.event.{ Logging }
import scala.Some
import org.apache.curator.retry.RetryUntilElapsed
import org.apache.curator.framework.CuratorFrameworkFactory

class CuratorToolsWithDirectCuratorToolsImplTest extends WordSpec with CuratorTestServer with BeforeAndAfterAll with MustMatchers {

  implicit val testSystem = ActorSystem("CuratorToolsTest")
  //protected val zkServers = "localhost:2181"
  var curator: Curator = _
  val defaultFormats = DefaultFormats + new EnumerationSerializer(TestEnum)

  override def beforeEach() {

    super.beforeEach()
    val logger = Logging.getLogger(testSystem, this)
    val curatorFramework = getCuratorFramework

    curatorFramework.start()

    /**
     * Below is how on some places the instance of the curator is used.
     * Important is the alternative way of providing extra formats for liftweb
     */
    curator = new CuratorToolsImpl(Some(curatorFramework), logger, defaultFormats)

    CuratorToolsTestHelper.prepareEnvironment(curator)

  }

  private def getCuratorFramework = {
    val retryPolicy = new RetryUntilElapsed(1000 * 60 * 60 * 24 * 3, 5000)
    CuratorFrameworkFactory.newClient(zkServers, retryPolicy)
  }

  override def afterEach() {
    curator.curator.foreach { f ⇒ f.close() }
    super.afterAll()
  }

  "CuratorToolsWithDirectCuratorToolsImplTest" must {

    "must be able to de-serialize a serialized Person into an ExtendedPerson when using CuratorToolsImpl directly" in {
      val extendedPerson = PersonExtended("John", 34, "lytse dyk 2")
      val retrievedPerson = curator.get[PersonExtended](Seq(TestPaths.personRoot / "john"), extendedPerson)
      retrievedPerson must be(Some(extendedPerson))
    }

    "properly merge nested case classes" in {
      curator.get[Test3](Seq(TestPaths.root1 / "test3", TestPaths.root2 / "test3")) must
        be(Some(Test3(1, Some(Test1(1, 1, 1)))))
    }

    "properly handle default values" in {
      curator.get[Test2](Seq(TestPaths.root1 / "test2"), Test2(2, Some("test"), None)) must
        be(Some(Test2(1, Some("test"), None)))
    }

    "properly handle enumerations" in {
      val refValue = Test4(TestEnum.ENUM1, TestEnum.ENUM2)
      val result = curator.get[Test4](Seq(TestPaths.root1 / "test4"))
      result must be(Some(refValue))
    }

  }

}
