package csc.curator.utils

import csc.config.Path

object TestEnum extends Enumeration {
  val ENUM1, ENUM2, ENUM3 = Value
}

case class Test1(v1: Int, v2: Int, v3: Int)

case class Test2(v1: Int, v2: Option[String], v3: Option[String])

case class Test3(v1: Int, v2: Option[Test1])

case class Test4(v1: TestEnum.Value, v2: TestEnum.Value)

case class Test5(v1: java.util.Date)

case class VersionedObject(someValue: String)

case class Neighbours(neighbours: List[(String, String)])

case class Person(name: String, age: Int)

case class PersonExtended(name: String, age: Int, address: String)

object TestPaths {
  def globalPath = Path("/ctes")
  def systemPath(systemId: String) = globalPath / "system" / systemId
  def lanePath(systemId: String, laneId: String) = systemPath(systemId) / laneId
  def root1 = Path("/ctes")
  def root2 = systemPath("3201")
  def root3 = root2 / "RD1"
  def versionedPath = systemPath("versioned") / "versionedObject"
  def personRoot = systemPath("person")
  def bogusPath = systemPath("bogus")
  def eventPath = systemPath("events")
}

object CuratorToolsTestHelper {

  def prepareEnvironment(curator: Curator) {

    if (!curator.exists(TestPaths.personRoot)) curator.createEmptyPath(TestPaths.personRoot)
    if (!curator.exists(TestPaths.root1)) curator.createEmptyPath(TestPaths.root1)
    if (!curator.exists(TestPaths.root2)) curator.createEmptyPath(TestPaths.root2)
    if (!curator.exists(TestPaths.root3)) curator.createEmptyPath(TestPaths.root3)
    if (!curator.exists(TestPaths.personRoot)) curator.createEmptyPath(TestPaths.personRoot)
    if (!curator.exists(TestPaths.versionedPath)) curator.createEmptyPath(TestPaths.versionedPath)
    curator.put(TestPaths.root1 / "test1", Map("v1" -> 1, "v2" -> 1, "v3" -> 1))
    curator.put(TestPaths.root2 / "test1", Map("v2" -> 2))
    curator.put(TestPaths.root3 / "test1", Map("v1" -> 3, "v3" -> 3))

    curator.put(TestPaths.root1 / "test2", Test2(1, None, None))
    curator.put(TestPaths.root2 / "test2", Test2(1, Some("2"), None))
    curator.put(TestPaths.root3 / "test2", Test2(1, None, Some("3")))

    curator.put(TestPaths.root1 / "test3", Test3(1, None))
    curator.put(TestPaths.root2 / "test3", Test3(1, Some(Test1(1, 1, 1))))
    curator.put(TestPaths.root3 / "test3", Test3(2, None))

    curator.set[VersionedObject](TestPaths.versionedPath, VersionedObject("this-is-a-versioned-object"), 0)
    curator.set[VersionedObject](TestPaths.versionedPath, VersionedObject("this-is-a-changed-versioned-object"), 1)

    curator.put[Person](TestPaths.personRoot / "john", Person("John", 34))

    curator.put(TestPaths.root1 / "test4", Test4(TestEnum.ENUM1, TestEnum.ENUM2))
    val neighbours = Neighbours(List(("LA1", "RD1"), ("RD1", "RD2"), ("RD2", "RA1")))
    curator.put(TestPaths.root3 / "list", neighbours)
  }
}