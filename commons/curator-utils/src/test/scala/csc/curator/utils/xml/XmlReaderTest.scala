package csc.curator.utils.xml

import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import org.scalatest.MustMatchers
import csc.json.lift.EnumerationSerializer
import org.apache.log4j.{ BasicConfigurator, LogManager }
import csc.config.Path
import scala.Left
import scala.Right
import scala.Some
import net.liftweb.json._
import csc.curator.utils.{ CuratorToolsTestHelper, Curator }
import akka.event.Logging
import akka.actor.ActorSystem

object TestEnum extends Enumeration {
  val ENUM1, ENUM2, ENUM3 = Value
}

case class Test1(v1: Int, v2: Int, v3: Int)

case class Test2(v1: Int, v2: Option[String], v3: Option[String])

case class Test3(v1: Int, v2: Option[Test1])

case class Test4(v1: TestEnum.Value, v2: TestEnum.Value)

case class Test5(v1: java.util.Date)

case class VersionedObject(someValue: String)

case class Neighbours(neighbours: List[(String, String)])

case class Person(name: String, age: Int)

case class PersonExtended(name: String, age: Int, address: String)

trait Constants {
  def filePath = getClass.getClassLoader.getResource("zookeeper_export.xml").getPath
  def filePath_paths = getClass.getClassLoader.getResource("zookeeper_export_paths.xml").getPath
  def filePath_non_existing = "non_existing"
  def filePath_double_key = getClass.getClassLoader.getResource("zookeeper_export_double_key.xml").getPath
  def nodeResult1 = "{\"id\":\"f8b31640-ad4f-4750-9ebe-2619a2d57afb\",\"reportingOfficerId\":\"AA0000\",\"phone\":\"0123456789\",\"ipAddress\":\"\",\"name\":\"CSC\",\"username\":\"ctcs\",\"email\":\"csc@csc.com\",\"passwordHash\":\"rr54qDh2UkCeOWvb1QddtYlEeLOAEVZ8NHaziIx3aTA=\",\"roleId\":\"93fff9e1-7bac-43a7-bb11-e95e95dd7594\",\"sectionIds\":[\"A4\"],\"prevPasswordHash1\":\"\",\"prevPasswordHash2\":\"\",\"badLoginCount\":0}"
  def nodeKey1 = "/ctes/users/f8b31640-ad4f-4750-9ebe-2619a2d57afb"
  def nodeKey2 = "/ctes/zookeeper/quota"
  def nodeKeySection = "/ctes/systems/A4/sections/1/config"
  def nodeKeyDouble = "/ctes/users/e3add550-4ae5-4ebb-a21a-e9d8ba829c1f"
}

object TestPaths {
  def globalPath = Path("/ctes")
  def systemPath(systemId: String) = globalPath / "system" / systemId
  def lanePath(systemId: String, laneId: String) = systemPath(systemId) / laneId
  def root1 = Path("/ctes")
  def root2 = systemPath("3201")
  def root3 = root2 / "RD1"
  def versionedPath = systemPath("versioned") / "versionedObject"
  def personRoot = systemPath("person")
  def bogusPath = systemPath("bogus")
  def eventPath = systemPath("events")
}

// Classes for the tests to deserialize
case class ZkMsiIdentification(os_comm_id: Int, msi_rijstr_pos: String, omschrijving: String = "")
case class ZkSection(id: String,
                     systemId: String,
                     name: String,
                     length: Double,
                     startGantryId: String,
                     endGantryId: String,
                     matrixBoards: Seq[String],
                     msiBlackList: Seq[ZkMsiIdentification])

case class ZkUser(id: String,
                  reportingOfficerId: String,
                  phone: String,
                  ipAddress: String,
                  name: String,
                  username: String,
                  email: String,
                  passwordHash: String,
                  roleId: String,
                  sectionIds: List[String])

class XmlReaderTest extends WordSpec with BeforeAndAfterAll with MustMatchers with Constants {
  implicit val testSystem = ActorSystem("XmlReaderTest")
  val logger = Logging.getLogger(testSystem, this)

  "XmlReader.getDirectChildElements(nodes: Seq[String], parentPath: String): Seq[String]" must {
    "only return the direct children" in {
      val xmlReader = new XmlReader(filePath, logger)
      val nodes: Seq[String] = Seq("/ctes/configuration/systems/3210/reports", "/ctes/configuration/systems/3210/reports/20140115",
        "/ctes/configuration/systems/3210/reports", "/ctes/configuration/systems/3210/reports/20140116",
        "/ctes/configuration/systems/3210/reports", "/ctes/configuration/systems/3210/reports/20140115/test")

      val result = xmlReader.getDirectChildElements(nodes, "/ctes/configuration/systems/3210/reports")
      result.length must be(2)
      result.contains("/ctes/configuration/systems/3210/reports/20140115") must be(true)
      result.contains("/ctes/configuration/systems/3210/reports/20140116") must be(true)
    }

    "return an empty list when no children available" in {
      val xmlReader = new XmlReader(filePath, logger)
      val nodes: Seq[String] = Seq("/ctes/configuration/systems/3210/reports", "/ctes/configuration/systems/3210/reports/20140115/test")

      val result = xmlReader.getDirectChildElements(nodes, "/ctes/configuration/systems/3210/reports")
      result.length must be(0)
    }
  }

  "XmlReader.getElement(String)" must {
    "return the value of the correct xml element" in {
      val xmlReader = new XmlReader(filePath, logger)
      val result = xmlReader.getElement(nodeKey1)
      result must be(Right(Some(nodeResult1)))
    }

    "return emptyString when nothing is present in the correct xml element" in {
      val xmlReader = new XmlReader(filePath, logger)
      val result = xmlReader.getElement(nodeKey2)
      result must be(Right(Some("")))
    }

    "return none when the xml element is not present" in {
      val xmlReader = new XmlReader(filePath, logger)
      val result = xmlReader.getElement("none")
      result must be(Right(None))
    }

    "return left containing an exception for the double key" in {
      val xmlReader = new XmlReader(filePath_double_key, logger)
      val result = xmlReader.getElement(nodeKeyDouble) match {
        case Left(exception) ⇒ true
        case _               ⇒ false
      }

      result must be(true)
    }

    "return left containing an exception when the file is not present" in {
      val xmlReader = new XmlReader(filePath_non_existing, logger)
      val result = xmlReader.getElement(nodeKeyDouble) match {
        case Left(exception) ⇒ true
        case _               ⇒ false
      }

      result must be(true)
    }
  }

  "XmlReader.getDirectChildElements(nodePath: String): Either[Exception, Seq[String]]" must {
    "only return the direct children in a either" in {
      val xmlReader = new XmlReader(filePath, logger)
      val result = xmlReader.getDirectChildElements("/ctes/users")

      val right = result.right
      right.get.length must be(12)

      right.get.contains("/ctes/users/65541187-81f2-495c-945f-a651a13d6cf4") must be(true)
      right.get.contains("/ctes/users/f8b31640-ad4f-4750-9ebe-2619a2d57afb") must be(true)
    }

    "return left containing an exception when the file is not present" in {
      val xmlReader = new XmlReader(filePath_non_existing, logger)
      val result = xmlReader.getDirectChildElements("/ctes/users") match {
        case Left(exception) ⇒ true
        case _               ⇒ false
      }

      result must be(true)
    }
  }
}

class CuratorToolsImplXmlTest extends WordSpec with BeforeAndAfterAll with MustMatchers with Constants {
  implicit val testSystem = ActorSystem("XmlReaderTest")
  val logger = Logging.getLogger(testSystem, this)

  "CuratorToolsImplXml.get" must {
    "return the expected object when using .get(String)" in {
      val defaultFormats = DefaultFormats + new EnumerationSerializer(TestEnum)
      val XmlReader = new XmlReader(filePath, logger)
      val curatorToolsImplXml = new CuratorToolsImplXml(XmlReader, logger, defaultFormats)
      val zkUser = curatorToolsImplXml.get[ZkUser](nodeKey1)
      zkUser.get.name must be("CSC")
      zkUser.get.reportingOfficerId must be("AA0000")
    }
    "return the None when path is no data using .get(String)" in {
      val defaultFormats = DefaultFormats + new EnumerationSerializer(TestEnum)
      val XmlReader = new XmlReader(filePath, logger)
      val curatorToolsImplXml = new CuratorToolsImplXml(XmlReader, logger, defaultFormats)
      val emptyKey = "/ctes/zookeeper"
      curatorToolsImplXml.get[ZkUser](emptyKey) must be(None)
    }

    "return the expected object when using .get(Path)" in {
      val defaultFormats = DefaultFormats + new EnumerationSerializer(TestEnum)
      val path: Path = Path(nodeKey1)
      val XmlReader = new XmlReader(filePath, logger)
      val curatorToolsImplXml = new CuratorToolsImplXml(XmlReader, logger, defaultFormats)
      val zkUser = curatorToolsImplXml.get[ZkUser](path)
      zkUser.get.name must be("CSC")
      zkUser.get.reportingOfficerId must be("AA0000")
    }

    "return the expected composed object in .get(String)" in {
      val defaultFormats = DefaultFormats + new EnumerationSerializer(TestEnum)
      val XmlReader = new XmlReader(filePath, logger)
      val curatorToolsImplXml = new CuratorToolsImplXml(XmlReader, logger, defaultFormats)
      val zkSection = curatorToolsImplXml.get[ZkSection](nodeKeySection)
      zkSection.get.systemId must be("N062")
      zkSection.get.msiBlackList.head.msi_rijstr_pos must be("1")
    }

    "must throw an exception when the file could not be found .get(String)" in {
      val defaultFormats = DefaultFormats + new EnumerationSerializer(TestEnum)
      val XmlReader = new XmlReader("dummy", logger)
      val curatorToolsImplXml = new CuratorToolsImplXml(XmlReader, logger, defaultFormats)
      val exception = intercept[Exception] {
        curatorToolsImplXml.get[ZkUser](nodeKeyDouble)
      }
      exception.getMessage must be("dummy (No such file or directory)")
    }

    "must throw an exception when double keys are found in .get(String)" in {
      val defaultFormats = DefaultFormats + new EnumerationSerializer(TestEnum)
      val XmlReader = new XmlReader(filePath_double_key, logger)
      val curatorToolsImplXml = new CuratorToolsImplXml(XmlReader, logger, defaultFormats)
      val exception = intercept[Exception] {
        curatorToolsImplXml.get[ZkUser](nodeKeyDouble)
      }
      exception.getMessage must be("There are multiple result found for the unique key. The result could not be determined.")
    }

  }

  "CuratorToolsImplXml.exists(String)" must {
    "return true when the element exists with .exists(String)" in {
      val defaultFormats = DefaultFormats + new EnumerationSerializer(TestEnum)
      val XmlReader = new XmlReader(filePath, logger)
      val curatorToolsImplXml = new CuratorToolsImplXml(XmlReader, logger, defaultFormats)
      curatorToolsImplXml.exists(nodeKey1) must be(true)
    }
    "return true when the element exists and has no data with .exists(String)" in {
      val defaultFormats = DefaultFormats + new EnumerationSerializer(TestEnum)
      val XmlReader = new XmlReader(filePath, logger)
      val curatorToolsImplXml = new CuratorToolsImplXml(XmlReader, logger, defaultFormats)
      val emptyKey = "/ctes/zookeeper"
      curatorToolsImplXml.exists(emptyKey) must be(true)
    }

    "return false when the element does not exists with .exists(String)" in {
      val defaultFormats = DefaultFormats + new EnumerationSerializer(TestEnum)
      val XmlReader = new XmlReader(filePath, logger)
      val curatorToolsImplXml = new CuratorToolsImplXml(XmlReader, logger, defaultFormats)
      curatorToolsImplXml.exists("non_existing_key") must be(false)
    }

    "return true when the element exists with .exists(Path)" in {
      val defaultFormats = DefaultFormats + new EnumerationSerializer(TestEnum)
      val XmlReader = new XmlReader(filePath, logger)
      val curatorToolsImplXml = new CuratorToolsImplXml(XmlReader, logger, defaultFormats)
      val path: Path = Path(nodeKey1)
      curatorToolsImplXml.exists(path) must be(true)
    }

    "must throw an exception when the file could not be found in .exists(String)" in {
      val defaultFormats = DefaultFormats + new EnumerationSerializer(TestEnum)
      val XmlReader = new XmlReader("dummy", logger)
      val curatorToolsImplXml = new CuratorToolsImplXml(XmlReader, logger, defaultFormats)
      val exception = intercept[Exception] {
        curatorToolsImplXml.exists(nodeKey1) must be(false)
      }
      exception.getMessage must be("dummy (No such file or directory)")
    }
  }

  "getChildren(parent: String): Seq[String]" must {
    "return the direct child paths when present" in {
      val defaultFormats = DefaultFormats + new EnumerationSerializer(TestEnum)
      val XmlReader = new XmlReader(filePath, logger)
      val curatorToolsImplXml = new CuratorToolsImplXml(XmlReader, logger, defaultFormats)

      val result = curatorToolsImplXml.getChildren("/ctes/users")

      result.length must be(12)
      result.contains("/ctes/users/65541187-81f2-495c-945f-a651a13d6cf4") must be(true)
      result.contains("/ctes/users/f8b31640-ad4f-4750-9ebe-2619a2d57afb") must be(true)
    }

    "return the direct child paths when present using the overload with Path" in {
      val defaultFormats = DefaultFormats + new EnumerationSerializer(TestEnum)
      val XmlReader = new XmlReader(filePath, logger)
      val curatorToolsImplXml = new CuratorToolsImplXml(XmlReader, logger, defaultFormats)

      val result = curatorToolsImplXml.getChildren(Path("/ctes/users"))

      result.length must be(12)
      result.contains(Path("/ctes/users/65541187-81f2-495c-945f-a651a13d6cf4")) must be(true)
      result.contains(Path("/ctes/users/f8b31640-ad4f-4750-9ebe-2619a2d57afb")) must be(true)
    }

    "must throw an exception when the file could not be found" in {
      val defaultFormats = DefaultFormats + new EnumerationSerializer(TestEnum)
      val XmlReader = new XmlReader("dummy", logger)
      val curatorToolsImplXml = new CuratorToolsImplXml(XmlReader, logger, defaultFormats)
      val exception = intercept[Exception] {
        curatorToolsImplXml.getChildren("/ctes/users")
      }
      exception.getMessage must be("dummy (No such file or directory)")
    }
  }

  "CuratorToolsImplXml.getChildNames(String)" must {
    "return the childnames when present" in {
      val defaultFormats = DefaultFormats + new EnumerationSerializer(TestEnum)
      val XmlReader = new XmlReader(filePath, logger)
      val curatorToolsImplXml = new CuratorToolsImplXml(XmlReader, logger, defaultFormats)

      val result = curatorToolsImplXml.getChildNames("/ctes/users")
      result.length must be(12)
      result.contains("e3add550-4ae5-4ebb-a21a-e9d8ba829c1f") must be(true)
      result.contains("1bc2d004-2dfd-42b2-ac22-4c8c5e04f7ec") must be(true)
    }

    "return the childnames when present using overload Path" in {
      val defaultFormats = DefaultFormats + new EnumerationSerializer(TestEnum)
      val XmlReader = new XmlReader(filePath, logger)
      val curatorToolsImplXml = new CuratorToolsImplXml(XmlReader, logger, defaultFormats)

      val result = curatorToolsImplXml.getChildNames(Path("/ctes/users"))
      result.length must be(12)
      result.contains("e3add550-4ae5-4ebb-a21a-e9d8ba829c1f") must be(true)
      result.contains("1bc2d004-2dfd-42b2-ac22-4c8c5e04f7ec") must be(true)
    }

    "must throw an exception when the file could not be found" in {
      val defaultFormats = DefaultFormats + new EnumerationSerializer(TestEnum)
      val XmlReader = new XmlReader("dummy", logger)
      val curatorToolsImplXml = new CuratorToolsImplXml(XmlReader, logger, defaultFormats)
      val exception = intercept[Exception] {
        curatorToolsImplXml.getChildNames("/ctes/users")
      }
      exception.getMessage must be("dummy (No such file or directory)")
    }
  }

  "CuratorToolsImplXml.get must run the same tests as the curator ZooKeeper variant" must {
    "retrieve case class instance with a single element path" in {
      val defaultFormats = DefaultFormats + new EnumerationSerializer(TestEnum)
      val XmlReader = new XmlReader(filePath_paths, logger)
      val curatorToolsImplXml = new CuratorToolsImplXml(XmlReader, logger, defaultFormats)

      curatorToolsImplXml.get[Test1](Seq(TestPaths.root1 / "test1")) must be(Some(Test1(1, 1, 1)))
    }
    "must be able to deserialize an serialzed Person into an ExtendedPerson" in {
      val defaultFormats = DefaultFormats + new EnumerationSerializer(TestEnum)
      val XmlReader = new XmlReader(filePath_paths, logger)
      val curatorToolsImplXml = new CuratorToolsImplXml(XmlReader, logger, defaultFormats)

      val extendedPerson = PersonExtended("John", 34, "lytse dyk 2")
      val retrievedPerson = curatorToolsImplXml.get[PersonExtended](Seq(TestPaths.personRoot / "john"), extendedPerson)
      retrievedPerson must be(Some(extendedPerson))
    }

    "return None if no nodes found in ZooKeeper" in {
      val defaultFormats = DefaultFormats + new EnumerationSerializer(TestEnum)
      val XmlReader = new XmlReader(filePath_paths, logger)
      val curatorToolsImplXml = new CuratorToolsImplXml(XmlReader, logger, defaultFormats)
      curatorToolsImplXml.get[Test1](Seq(TestPaths.root1 / "bogus")) must be(None)
    }

    "succesfully retrieve neighbours" in {
      val defaultFormats = DefaultFormats + new EnumerationSerializer(TestEnum)
      val XmlReader = new XmlReader(filePath_paths, logger)
      val curatorToolsImplXml = new CuratorToolsImplXml(XmlReader, logger, defaultFormats)

      // println(curatorToolsImplXml.get[Neighbours]("/ctes/system/3201/RD1/list"))
      val neighbours = Neighbours(List(("LA1", "RD1"), ("RD1", "RD2"), ("RD2", "RA1")))

      //val p = Person("Alvin Alexander", 12)
      // create a JSON string from the Person, then print it
      //implicit val formats = DefaultFormats
      //val jsonString = write(neighbours)
      //println(jsonString)

      curatorToolsImplXml.get[Neighbours](Seq(TestPaths.root3 / "list")) must be(Some(neighbours))
    }

    "merge two instances from ZooKeeper into a single case class" in {
      val defaultFormats = DefaultFormats + new EnumerationSerializer(TestEnum)
      val XmlReader = new XmlReader(filePath_paths, logger)
      val curatorToolsImplXml = new CuratorToolsImplXml(XmlReader, logger, defaultFormats)
      curatorToolsImplXml.get[Test1](Seq(TestPaths.root1 / "test1", TestPaths.root2 / "test1")) must
        be(Some(Test1(1, 2, 1)))
    }

    "ignore instances missing in ZooKeeper" in {
      val defaultFormats = DefaultFormats + new EnumerationSerializer(TestEnum)
      val XmlReader = new XmlReader(filePath_paths, logger)
      val curatorToolsImplXml = new CuratorToolsImplXml(XmlReader, logger, defaultFormats)
      curatorToolsImplXml.get[Test1](Seq(TestPaths.bogusPath / "test1", TestPaths.root1 / "test1")) must
        be(Some(Test1(1, 1, 1)))
    }

    "properly handle case class with option of a simple type" in {
      val defaultFormats = DefaultFormats + new EnumerationSerializer(TestEnum)
      val XmlReader = new XmlReader(filePath_paths, logger)
      val curatorToolsImplXml = new CuratorToolsImplXml(XmlReader, logger, defaultFormats)
      curatorToolsImplXml.get[Test2](Seq(TestPaths.root1 / "test2")) must
        be(Some(Test2(1, None, None)))

      curatorToolsImplXml.get[Test2](Seq(TestPaths.root2 / "test2")) must
        be(Some(Test2(1, Some("2"), None)))

      curatorToolsImplXml.get[Test2](Seq(TestPaths.root1 / "test2", TestPaths.root2 / "test2")) must
        be(Some(Test2(1, Some("2"), None)))

      curatorToolsImplXml.get[Test2](Seq(TestPaths.root2 / "test2", TestPaths.root3 / "test2")) must
        be(Some(Test2(1, Some("2"), Some("3"))))
    }

    "properly handle nested case classes" in {
      val defaultFormats = DefaultFormats + new EnumerationSerializer(TestEnum)
      val XmlReader = new XmlReader(filePath_paths, logger)
      val curatorToolsImplXml = new CuratorToolsImplXml(XmlReader, logger, defaultFormats)

      curatorToolsImplXml.get[Test3](Seq(TestPaths.root1 / "test3")) must
        be(Some(Test3(1, None)))
      curatorToolsImplXml.get[Test3](Seq(TestPaths.root2 / "test3")) must
        be(Some(Test3(1, Some(Test1(1, 1, 1)))))
    }

    "properly merge nested case classes" in {
      val defaultFormats = DefaultFormats + new EnumerationSerializer(TestEnum)
      val XmlReader = new XmlReader(filePath_paths, logger)
      val curatorToolsImplXml = new CuratorToolsImplXml(XmlReader, logger, defaultFormats)
      curatorToolsImplXml.get[Test3](Seq(TestPaths.root1 / "test3", TestPaths.root2 / "test3")) must
        be(Some(Test3(1, Some(Test1(1, 1, 1)))))
    }

    "properly handle default values" in {
      val defaultFormats = DefaultFormats + new EnumerationSerializer(TestEnum)
      val XmlReader = new XmlReader(filePath_paths, logger)
      val curatorToolsImplXml = new CuratorToolsImplXml(XmlReader, logger, defaultFormats)
      curatorToolsImplXml.get[Test2](Seq(TestPaths.root1 / "test2"), Test2(2, Some("test"), None)) must
        be(Some(Test2(1, Some("test"), None)))
    }

    "properly handle enumerations" in {
      val defaultFormats = DefaultFormats + new EnumerationSerializer(TestEnum)
      val XmlReader = new XmlReader(filePath_paths, logger)
      val curatorToolsImplXml = new CuratorToolsImplXml(XmlReader, logger, defaultFormats)
      val refValue = Test4(TestEnum.ENUM1, TestEnum.ENUM2)
      val result = curatorToolsImplXml.get[Test4](Seq(TestPaths.root1 / "test4"))
      result must be(Some(refValue))
    }

    "properly handle dates" in {
      val defaultFormats = DefaultFormats + new EnumerationSerializer(TestEnum)
      val XmlReader = new XmlReader(filePath_paths, logger)
      val curatorToolsImplXml = new CuratorToolsImplXml(XmlReader, logger, defaultFormats)
      val refValue = Test5(new java.util.Date(150000))
      println(refValue)
      val path = TestPaths.root1 / "test5"
      curatorToolsImplXml.get[Test5](Seq(path)) must be(Some(refValue))
    }
  }
}
