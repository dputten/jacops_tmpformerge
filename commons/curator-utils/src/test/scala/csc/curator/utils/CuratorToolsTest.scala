/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.curator.utils

import org.scalatest.{ WordSpec }
import org.scalatest.MustMatchers
import akka.actor.ActorSystem
import csc.json.lift.EnumerationSerializer
import net.liftweb.json.{ DefaultFormats }
import csc.curator.CuratorTestServer
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }

class CuratorToolsTest extends WordSpec with BeforeAndAfterAll with CuratorTestServer with MustMatchers {
  implicit val testSystem = ActorSystem("CuratorToolsTest")
  var curator: Curator = _
  private val defaultFormats = DefaultFormats + new EnumerationSerializer(TestEnum)

  /*  override protected def beforeAll() {
    super.beforeAll()
    curator = Curator.createCurator(this, testSystem, formats = defaultFormats)
    CuratorToolsTestHelper.prepareEnvironment(curator)
  }

  override def afterAll() {
    super.afterAll()
  }*/

  override def beforeEach() {
    super.beforeEach()
    curator = Curator.createCurator(this, testSystem, zkServers = zkServers, formats = defaultFormats)
    CuratorToolsTestHelper.prepareEnvironment(curator)
  }

  override def afterEach() {
    curator.curator.foreach { f ⇒ f.close() }
    super.afterEach()
  }

  "CuratorToolsTest" must {
    "be able to append an event" in {
      //ctes/system/events/qn-452120565226842"
      val prefix = "qn-"
      val person = Person(name = "Name", age = 100)
      val eventPath = TestPaths.eventPath
      val eventPathWithPrefix = eventPath / prefix

      //clean all events
      curator.getChildren(eventPath).foreach(curator.delete)

      //check that no previous events are present
      val nrEventsBeforeTest = curator.getChildren(eventPath).length
      nrEventsBeforeTest must be(0)

      //append event
      curator.appendEvent(eventPathWithPrefix, person)
      val eventsAfterTest = curator.getChildren(eventPath)

      //only 1 event must be appended
      eventsAfterTest.length must be(1)

      //full path must start with 'ctes/system/events/qn-'
      val event = eventsAfterTest(0).toString()
      event must startWith(eventPathWithPrefix.toString())

      //check that the data is stored correctly
      curator.get[Person](event) must be(Some(person))
    }
    "must be able to deserialize an serialzed Person into an ExtendedPerson" in {
      val extendedPerson = PersonExtended("John", 34, "lytse dyk 2")
      val retrievedPerson = curator.get[PersonExtended](Seq(TestPaths.personRoot / "john"), extendedPerson)
      retrievedPerson must be(Some(extendedPerson))
    }

    "retrieve case class instance with a single element path" in {
      curator.get[Test1](Seq(TestPaths.root1 / "test1")) must
        be(Some(Test1(1, 1, 1)))
    }

    "return None if no nodes found in ZooKeeper" in {
      curator.get[Test1](Seq(TestPaths.root1 / "bogus")) must
        be(None)
    }

    "succesfully retrieve neighbours" in {
      val neighbours = Neighbours(List(("LA1", "RD1"), ("RD1", "RD2"), ("RD2", "RA1")))
      curator.get[Neighbours](Seq(TestPaths.root3 / "list")) must
        be(Some(neighbours))
    }
    "merge two instances from ZooKeeper into a single case class" in {
      curator.get[Test1](Seq(TestPaths.root1 / "test1", TestPaths.root2 / "test1")) must
        be(Some(Test1(1, 2, 1)))
    }

    "ignore instances missing in ZooKeeper" in {
      curator.get[Test1](Seq(TestPaths.bogusPath / "test1", TestPaths.root1 / "test1")) must
        be(Some(Test1(1, 1, 1)))
    }

    "properly handle case class with option of a simple type" in {
      curator.get[Test2](Seq(TestPaths.root1 / "test2")) must
        be(Some(Test2(1, None, None)))

      curator.get[Test2](Seq(TestPaths.root2 / "test2")) must
        be(Some(Test2(1, Some("2"), None)))

      curator.get[Test2](Seq(TestPaths.root1 / "test2", TestPaths.root2 / "test2")) must
        be(Some(Test2(1, Some("2"), None)))

      curator.get[Test2](Seq(TestPaths.root2 / "test2", TestPaths.root3 / "test2")) must
        be(Some(Test2(1, Some("2"), Some("3"))))
    }

    "properly handle nested case classes" in {
      curator.get[Test3](Seq(TestPaths.root1 / "test3")) must
        be(Some(Test3(1, None)))
      curator.get[Test3](Seq(TestPaths.root2 / "test3")) must
        be(Some(Test3(1, Some(Test1(1, 1, 1)))))
    }

    "properly merge nested case classes" in {
      curator.get[Test3](Seq(TestPaths.root1 / "test3", TestPaths.root2 / "test3")) must
        be(Some(Test3(1, Some(Test1(1, 1, 1)))))
    }

    "properly handle default values" in {
      curator.get[Test2](Seq(TestPaths.root1 / "test2"), Test2(2, Some("test"), None)) must
        be(Some(Test2(1, Some("test"), None)))
    }

    "properly handle enumerations" in {
      val refValue = Test4(TestEnum.ENUM1, TestEnum.ENUM2)
      val result = curator.get[Test4](Seq(TestPaths.root1 / "test4"))
      result must be(Some(refValue))
    }

    "properly handle dates" in {
      val refValue = Test5(new java.util.Date(150000))
      val path = TestPaths.root1 / "test5"
      curator.put(path, refValue)
      curator.get[Test5](Seq(path)) must
        be(Some(refValue))
    }

    "properly handle versioned objects" in {
      val refValue = VersionedObject("this-is-a-changed-versioned-object")
      val path = TestPaths.versionedPath
      curator.get[VersionedObject](Seq(path)) must
        be(Some(refValue))

      val versionedResult = curator.getVersioned(Seq(path), VersionedObject("some default")).get
      versionedResult.data must be(refValue)
      versionedResult.version must be(2)
    }

  }

}

