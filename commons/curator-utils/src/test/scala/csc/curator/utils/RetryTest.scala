package csc.curator.utils

import org.scalatest.MustMatchers
import org.scalatest.{ BeforeAndAfterEach, WordSpec }

class RetryTest extends WordSpec with BeforeAndAfterEach with MustMatchers {
  var nrOfCalls = 0
  var totalExceptions = 0

  def print(txt: String, retriesNeeded: Int = 0) = {
    txt + ", retries=" + retriesNeeded + ", nrOfCalls=" + nrOfCalls + ", totalExceptions=" + totalExceptions
  }

  def createException(nrExceptionsToThrow: Int) = {
    nrOfCalls = nrOfCalls + 1
    if (nrExceptionsToThrow > totalExceptions) {
      totalExceptions = totalExceptions + 1
      throw new Exception("blabal")
    }
    ""
  }

  override protected def beforeEach() {
    nrOfCalls = 0
    totalExceptions = 0
  }

  "RetryTest" must {

    "fail when exception occured and no retries required" in {
      retry(0)(createException(nrExceptionsToThrow = 1)) match { //must fail
        case Left(exception)           ⇒ //this is were we want to be. failed after 5 tries
        case Right((_, retriesNeeded)) ⇒ fail(print("expected the function to fai", retriesNeeded))
      }
    }

    "fail when exception occured after max retries" in {
      retry(5)(createException(nrExceptionsToThrow = 6)) match { //must fail
        case Left(exception)           ⇒ //this is were we want to be. failed after 5 tries
        case Right((_, retriesNeeded)) ⇒ fail(print("expected the function to fail", retriesNeeded))
      }
    }

    "succeed without any retries when no exception occurs given no nr of retries" in {
      retry(0)(createException(nrExceptionsToThrow = 0)) match { //0 retry needed
        case Left(exception)                                   ⇒ fail(print("expected the function to succeed, exception=" + exception.getMessage))
        case Right((_, retriesNeeded)) if (retriesNeeded == 0) ⇒ //this is were we want to be. Success on first call
        case Right((_, retriesNeeded))                         ⇒ fail(print("expected the fuction to succeed with 0 retry", retriesNeeded))
      }
    }

    "succeed without any retries when no exception occurs given nr of retries" in {
      retry(5)(createException(nrExceptionsToThrow = 0)) match { //0 retry needed
        case Left(exception)                                   ⇒ fail(print("expected the fuction to succeed, exception=" + exception.getMessage))
        case Right((_, retriesNeeded)) if (retriesNeeded == 0) ⇒ //this is were we want to be. Success on first call
        case Right((_, retriesNeeded))                         ⇒ fail(print("expected the fuction to succeed with 0 retry", retriesNeeded))
      }
    }

    "succeed with one retry" in {
      retry(5)(createException(nrExceptionsToThrow = 1)) match { //1 retry needed
        case Left(exception)                                   ⇒ fail(print("expected the fuction to succeed, exception=" + exception.getMessage))
        case Right((_, retriesNeeded)) if (retriesNeeded == 1) ⇒ //this is were we want to be. Success on first call
        case Right((_, retriesNeeded))                         ⇒ fail(print("expected the fuction to succeed with 1 retry", retriesNeeded))
      }
    }

    "succeed with 4 retries" in {
      retry(5)(createException(nrExceptionsToThrow = 4)) match { //4 retry needed
        case Left(exception)                                   ⇒ fail(print("expected the fuction to succeed, exception=" + exception.getMessage))
        case Right((_, retriesNeeded)) if (retriesNeeded == 4) ⇒ //this is were we want to be. Success on first call
        case Right((_, retriesNeeded))                         ⇒ fail(print("expected the fuction to succeed with 3 retries", retriesNeeded))
      }
    }

    "succeed with max nr of retries" in {
      retry(5)(createException(nrExceptionsToThrow = 5)) match { //5 retry needed
        case Left(exception)                                   ⇒ fail(print("expected the fuction to succeed, exception=" + exception.getMessage))
        case Right((_, retriesNeeded)) if (retriesNeeded == 5) ⇒ //this is were we want to be. Success on first call
        case Right((_, retriesNeeded))                         ⇒ fail(print("expected the fuction to succeed with 3 retries", retriesNeeded))
      }
    }
  }
}
