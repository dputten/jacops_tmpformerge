package csc.curator.utils

import org.scalatest.MustMatchers
import csc.json.lift.EnumerationSerializer
import net.liftweb.json.{ DefaultFormats }
import csc.curator.CuratorTestServer
import akka.testkit.TestActorRef
import akka.actor.{ Actor, ActorSystem }
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpecLike }

class ActorAppendEventTest extends WordSpecLike with BeforeAndAfterAll with CuratorTestServer with MustMatchers {
  implicit val testSystem = ActorSystem("CuratorToolsTest")
  var curator: Curator = _
  private val defaultFormats = DefaultFormats + new EnumerationSerializer(TestEnum)

  override def afterEach() {
    curator.curator.foreach { f ⇒ f.close() }
    super.afterEach()
  }

  override def beforeEach() {
    super.beforeEach()
    curator = Curator.createCurator(this, testSystem, zkServers = zkServers, formats = defaultFormats)
    CuratorToolsTestHelper.prepareEnvironment(curator)
    //clean all events
    curator.getChildren(TestPaths.eventPath).foreach(curator.delete)
  }

  case class Message(data: String, actorId: Int, counter: Int)
  class AppendEventActor(curator: Curator) extends Actor {
    def receive = {
      case msg: Message ⇒ curator.appendEvent(TestPaths.eventPath / "qn-", msg)
    }
  }

  "ActorAppendEventTest" must {
    "append all events sequential" in {
      val eventPathWithPrefix = TestPaths.eventPath / "qn-"
      val person = Person(name = "Name", age = 100)

      (1 to 100).foreach(x ⇒ curator.appendEvent(eventPathWithPrefix, person))
      (1 to 100).foreach(x ⇒ curator.appendEvent(eventPathWithPrefix, person))

      val eventsAfterTest = curator.getChildren(TestPaths.eventPath)
      eventsAfterTest.length must be(200)
    }
    "append all events in parallel" in {
      val actor1 = TestActorRef(new AppendEventActor(curator))
      val actor2 = TestActorRef(new AppendEventActor(curator))
      val actor3 = TestActorRef(new AppendEventActor(curator))
      val actor4 = TestActorRef(new AppendEventActor(curator))

      (1 to 20).foreach {
        x ⇒
          actor1 ! Message(data = "Hello 1", actorId = 1, counter = x)
          actor2 ! Message(data = "World 2", actorId = 2, counter = x)
          actor3 ! Message(data = "Hello 3", actorId = 3, counter = x)
          actor4 ! Message(data = "World 4", actorId = 4, counter = x)
      }

      val eventsAfterTest = curator.getChildren(TestPaths.eventPath)
      eventsAfterTest.length must be(80)

      val messages = eventsAfterTest.flatMap(path ⇒ curator.get[Message](path))

      val actor1Messages = messages.filter(x ⇒ x.actorId == 1)
      val actor2Messages = messages.filter(x ⇒ x.actorId == 2)
      val actor3Messages = messages.filter(x ⇒ x.actorId == 3)
      val actor4Messages = messages.filter(x ⇒ x.actorId == 4)

      actor1Messages.length must be(20)
      actor2Messages.length must be(20)
      actor3Messages.length must be(20)
      actor4Messages.length must be(20)
    }
  }
}
