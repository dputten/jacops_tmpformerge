/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.curator.utils

import akka.actor.{ ActorLogging, Actor }

trait CuratorActor extends Actor with ActorLogging {
  protected def curator: Curator
}

