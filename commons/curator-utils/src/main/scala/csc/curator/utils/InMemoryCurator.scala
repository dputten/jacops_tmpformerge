package csc.curator.utils

import java.io.File
import java.lang.reflect.{ InvocationHandler, Method, Proxy }

import akka.event.LoggingAdapter
import csc.config.{ Leaf, Path, PathElement }
import csc.curator.utils.xml.Exporter
import csc.json.lift.LiftSerialization
import net.liftweb.json._
import org.apache.curator.framework.CuratorFramework
import org.apache.curator.framework.api._
import org.apache.zookeeper.data.Stat

import scala.reflect.ClassTag
import scala.xml.{ Elem, NodeSeq, XML }

/**
 * Stub implementation of Curator that keeps all the values in memory.
 * Created by carlos on 06/01/16.
 */
class InMemoryCurator(formats: Formats, val log: LoggingAdapter, initial: Option[File], collector: Option[Curator] = None)
  extends Curator with CuratorTools with MapMerger {

  var root = new Node(null, "", None)

  initial map { f ⇒ importFile(f) }

  implicit def myFormats = formats

  override val curator: Option[CuratorFramework] = Some(createProxy[CuratorFramework]) //None //Some(internal)

  override def serialization: LiftSerialization = new LiftSerialization {
    implicit def formats = myFormats
  }

  override def mapAt(path: Path, versioned: Boolean, stats: Option[Stat]): Option[AnyMap] =
    getContent(path) map { c ⇒ parse(new String(c)).values.asInstanceOf[AnyMap] }

  def importFile(file: File): Unit = {
    val xml = XML.loadFile(file.getPath)
    val allNodes: NodeSeq = xml \\ "export" \\ "znodes" \ "znode"

    for (node ← allNodes) node match {
      case elem: Elem ⇒ {
        val path = Path((elem.child \\ "path").text)
        val value = (elem.child \\ "data").text
        val content = if (value.isEmpty) None else Some(value.getBytes)
        setNode(path, content)
      }
    }
  }

  def exportFile(file: File): Unit = {
    val xml = Exporter.exportConfig(this, Path("/ctes"), None)
    XML.save(file.getPath, xml)
  }

  override def set[T <: AnyRef](path: String, value: T, storedVersion: Int): Unit = set(Path(path), value, storedVersion)

  override def set[T <: AnyRef](path: Path, value: T, storedVersion: Int): Unit = put(path, value)

  override def get[T <: AnyRef](path: String)(implicit manifest: Manifest[T]): Option[T] = get(Path(path))

  override def get[T <: AnyRef](path: Path)(implicit manifest: Manifest[T]): Option[T] = {
    val result = getContent(path) map { c ⇒ serialization.deserialize[T](c) }
    //println(s"--get $path")
    //log.debug("get({})[{}] = {}", path, manifest, result)
    collector map { c ⇒
      result match {
        case None        ⇒ c.createEmptyPath(path)
        case Some(value) ⇒ c.set[T](path, value, 0)
      }
    }

    result
  }

  override def get[T <: AnyRef](pathList: Seq[Path], default: T)(implicit manifest: Manifest[T]): Option[T] = {
    val defaultMap = objectToMap[T](default)
    mapToSomeObject[T](mergeMaps(Option(defaultMap) +: getMaps(pathList)))
  }

  override def get[T <: AnyRef](pathList: Seq[Path])(implicit manifest: Manifest[T]): Option[T] = {
    if (pathList == Nil)
      None
    else
      mapToSomeObject[T](mergeMaps(getMaps(pathList)))
  }

  override def get[T <: AnyRef](pathList: Seq[Path], defaults: Map[String, Any])(implicit manifest: Manifest[T]): Option[T] =
    mapToSomeObject[T](mergeMaps(Option(defaults) +: getMaps(pathList)))

  override def appendEvent[T <: AnyRef](path: String, value: T, retries: Int): Unit = appendEvent(Path(path), value)

  override def appendEvent[T <: AnyRef](path: String, value: T): Unit = appendEvent(Path(path), value)

  override def appendEvent[T <: AnyRef](path: Path, value: T): Unit = {
    val newPath = appendEventWithUUID(path.toString())
    put(newPath, value)
  }

  override def appendEvent[T <: AnyRef](path: Path, value: T, retries: Int): Unit = ???

  override def put[T <: AnyRef](path: String, value: T, ephemeral: Boolean): Unit = put(Path(path), value)

  override def put[T <: AnyRef](path: Path, value: T): Unit = {
    ensureNode(Some(path(0)), root).content = Some(serialization.serialize(value))
    //log.debug("put({},{})", path, value)
    //println(s"--put $path = $value")
  }

  override def getVersioned[T <: AnyRef](path: String)(implicit manifest: Manifest[T]): Option[Versioned[T]] =
    getVersioned(Path(path))

  override def getVersioned[T <: AnyRef](path: Path)(implicit manifest: Manifest[T]): Option[Versioned[T]] =
    get(path) map { e ⇒ Versioned(e.asInstanceOf[T], 0) }

  override def getVersioned[T <: AnyRef](pathList: Seq[Path], default: T)(implicit manifest: Manifest[T]): Option[Versioned[T]] =
    get(pathList, default) map { v ⇒ Versioned(v, 0) }

  override def getChildNames(parent: String): Seq[String] = getChildNames(Path(parent))

  override def getChildNames(parent: Path): Seq[String] = {
    val result = getNode(parent(0), root) match {
      case None    ⇒ Nil
      case Some(n) ⇒ n.getChildren.keySet.toList
    }
    //log.debug("getChildNames({}) = {}", parent, result)
    collector map { col ⇒ result.foreach { c ⇒ col.createEmptyPath(parent / c) } }
    result
  }

  override def createEmptyPath(path: String): Unit = createEmptyPath(Path(path))

  override def createEmptyPath(path: Path): Unit = {
    //log.debug("createEmptyPath({})", path)
    ensureNode(Some(path(0)), root)
  }

  override def safeDelete(path: String): Unit = safeDelete(Path(path))
  override def safeDelete(path: Path): Unit = delete(path)

  override def getChildren(parent: String): Seq[String] = getChildren(Path(parent)) map { _.toString() }
  override def getChildren(parent: Path): Seq[Path] = getNode(parent) match {
    case None    ⇒ Nil
    case Some(n) ⇒ n.getChildren.keys.toSeq map { c ⇒ parent / c }
  }

  override def delete(path: String): Unit = delete(Path(path))
  override def delete(path: Path): Unit = getNode(path) foreach (n ⇒ delete(n, false))

  private def delete(node: Node, recursive: Boolean): Unit = {
    node.parent.remove(node) //TODO: is delete a prune or just clear content? if is prune, what is deleteRecursive?
    //    if (recursive) for (c ← node.getChildren.values) delete(c, true)
    //    node.content = None
  }

  override def deleteRecursive(path: String): Unit = deleteRecursive(Path(path))
  override def deleteRecursive(path: Path): Unit = getNode(path) foreach (n ⇒ delete(n, true))

  override def exists(path: String): Boolean = exists(Path(path))
  override def exists(path: Path): Boolean = getNode(path).isDefined

  override def stat(path: String): Option[Stat] = stat(Path(path))
  override def stat(path: Path): Option[Stat] = ???

  private def getContent(path: Path): Option[Array[Byte]] = getNode(path) match {
    case None    ⇒ None
    case Some(n) ⇒ n.content
  }

  private def getNode(path: Path): Option[Node] = getNode(path(0), root)

  private def getNode(path: String): Option[Node] = getNode(Path(path))

  private def getNode(elem: PathElement, base: Node): Option[Node] = {
    val child = base.getChild(elem.name)
    (elem, child) match {
      case (l: Leaf, None)   ⇒ None
      case (l: Leaf, result) ⇒ result
      case (_, Some(c))      ⇒ getNode(elem.next.get, c)
      case other ⇒
        //println(other) //TODO csilva: investigate and fix this weird case (actually happens)
        None
    }
  }

  private def setNode(path: Path, content: Option[Array[Byte]]): Unit = ensureNode(Some(path(0)), root).content = content

  private def ensureNode(elem: Option[PathElement], node: Node): Node = elem match {
    case None ⇒ node
    case Some(e) ⇒ node.getChild(e.name) match {
      case None ⇒ {
        val n = new Node(node, e.name, None)
        node.add(n)
        ensureNode(e.next, n)
      }
      case Some(n) ⇒ ensureNode(e.next, n)
    }
  }

  private def appendEventWithUUID(path: String) = path + java.util.UUID.randomUUID().toString.replace("-", "")

  def thisCurator = this

  /**
   * To avoid implementing exhaustively big interfaces, some dynamic proxy wizardry:
   * each method will be diverted to the InMemoryCurator instance, if matches name and params.
   * This way we only implement the methods we need, when if see they are needed.
   *
   * @param tag
   * @tparam T
   * @return dynamic proxy on specified type with invocation diverted to this InMenoryCurator instance
   */
  def createProxy[T](implicit tag: ClassTag[T]): T = {
    val handler = new InvocationHandler {
      override def invoke(obj: scala.Any, method: Method, args: Array[AnyRef]): AnyRef = {
        try {
          val m = thisCurator.getClass.getDeclaredMethod(method.getName, method.getParameterTypes: _*)
          m.invoke(thisCurator, args: _*)
        } catch {
          case ex: NoSuchMethodException ⇒
            System.err.println("No method: " + method)
            throw ex
          case ex: Exception ⇒
            System.err.println(ex)
            throw ex
        }
      }
    }
    val cl = tag.runtimeClass
    Proxy.newProxyInstance(cl.getClassLoader, Array(cl), handler).asInstanceOf[T]
  }

  //needed by CuratorFramework proxy
  def getData: GetDataBuilder = createProxy[GetDataBuilder]

  //needed for GetDataBuilder proxy
  def forPath(path: String): Array[Byte] = getContent(Path(path)) match {
    case None        ⇒ Array()
    case Some(array) ⇒ array
  }

  class Node(val parent: Node, val name: String, var content: Option[Array[Byte]]) {

    private var children: Map[String, Node] = Map.empty
    private var version = 0

    def getChild(name: String) = children.get(name)

    def add(child: Node): Unit = children = children.updated(child.name, child)

    def remove(child: Node): Unit = children = children - child.name

    def getChildren: Map[String, Node] = children

    def isEmpty = children.size == 0

    override def toString: String = "Node[" + name + "," + children.keySet.toList + "]"
  }
}

trait MapMerger {

  type AnyMap = Map[String, Any]

  implicit def myFormats: Formats

  def serialization: LiftSerialization

  def mapAt(path: Path, versioned: Boolean, stats: Option[Stat] = None): Option[AnyMap]

  def getMaps(pathList: Seq[Path], versioned: Boolean = false, stats: Option[Stat] = None): Seq[Option[AnyMap]] = {
    pathList.map(path ⇒ mapAt(path, versioned, stats))
  }

  def objectToMap[T <: AnyRef: Manifest](obj: T): AnyMap = {
    val defaultData: Array[Byte] = serialization.serialize(obj)
    parse(new String(defaultData)).values.asInstanceOf[AnyMap]
  }

  def mapToSomeObject[T <: AnyRef: Manifest](aMap: AnyMap): Option[T] = {
    if (aMap.isEmpty)
      None
    else {
      val data = serialization.serialize(aMap)
      val parsedData = parse(new String(data))
      try {
        val tmp: T = parsedData.extract[T]
        Some(tmp)
      } catch {
        case e: Exception ⇒ None
      }
    }
  }

  def mergeMaps(maps: Seq[Option[AnyMap]]): AnyMap = {
    maps.flatten.foldLeft[AnyMap](Map[String, Any]()) {
      (accu: AnyMap, elem: AnyMap) ⇒ accu ++ elem
    }
  }
}
