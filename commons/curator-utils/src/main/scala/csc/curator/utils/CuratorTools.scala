/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.curator.utils

import csc.json.lift.{ LiftSerialization }
import org.apache.zookeeper.{ KeeperException, CreateMode }
import org.apache.zookeeper.KeeperException.NoNodeException
import org.apache.zookeeper.data.Stat
import csc.config.Path
import akka.actor.ActorSystem
import akka.event.{ Logging, LoggingAdapter }
import org.apache.curator.framework.{ CuratorFrameworkFactory, CuratorFramework }
import org.apache.curator.RetryPolicy
import org.apache.curator.retry.RetryUntilElapsed
import net.liftweb.json._

trait CuratorSerialization {

  protected def extendFormats(defaultFormats: Formats): Formats = defaultFormats
  def serialization = new LiftSerialization {
    implicit def formats = extendFormats(DefaultFormats)
  }
}

/**
 *
 * @author Maarten Hazewinkel
 */
trait CuratorTools extends CuratorSerialization {
  /**
   * override this to supply additional serializers or or type hints for storing objects in
   * Zookeeper via Lift-Json serialization.
   */

  protected def log: LoggingAdapter

  protected def curator: Option[CuratorFramework]

  def zkGetRawData[T <: AnyRef: Manifest](path: String, versioned: Boolean, stats: Option[Stat] = None): Option[Array[Byte]] = {
    curator.flatMap { c ⇒
      try {
        if (versioned && stats.isDefined) {
          Some(c.getData.storingStatIn(stats.get).forPath(path))
        } else {
          val data = c.getData.forPath(path)
          if (data.size > 0)
            Some(data)
          else
            None
        }
      } catch {
        case e: Exception ⇒
          log.warning("CuratorTools.zkGetRawData: " + e.getMessage)
          None
      }
    }
  }

  /**
   * Utility method to read values from curator
   */
  protected def zkGet[T <: AnyRef: Manifest](path: String): Option[T] = {
    val rawData = zkGetRawData(path, false)
    val r = rawData.map(serialization.deserialize[T](_))
    r match {
      case Some(t) ⇒ log.info("loaded from {}: {}", path, t.toString)
      case None    ⇒ log.warning("could not load value from {}", path)
    }
    r
  }

  /**
   * Utility method to read values from curator
   */
  protected def zkGetVersioned[T <: AnyRef: Manifest](path: String): Option[Versioned[T]] = {
    val stats = new Stat
    val rawData = curator.flatMap { c ⇒
      try {
        Some(c.getData.storingStatIn(stats).forPath(path))
      } catch {
        case e: NoNodeException ⇒
          log.warning("Node at {} does not exist", path)
          None
        case e: Exception ⇒
          log.error(e, "Failed to retrieve curator value at {}", path)
          None
      }
    }
    val r = rawData.map(serialization.deserialize[T](_))
    r match {
      case Some(t) ⇒ log.debug("loaded from {} version {}: {}", path, stats.getVersion, t.toString)
      case None    ⇒ log.debug("could not load value from {}", path)
    }
    r.map(t ⇒ Versioned(t, stats.getVersion))
  }

  /**
   * Utility method to check the existence of nodes in curator
   */
  protected def zkExists(path: String): Boolean = {
    curator.map { c ⇒
      try {
        c.checkExists().forPath(path) != null
      } catch {
        case e: Exception ⇒
          log.error(e, "Failed to retrieve curator value at {}", path)
          false
      }
    }.getOrElse(false)
  }

  /**
   * Utility method to read child nodes from curator
   */
  protected def zkGetChildren(parent: String): Seq[String] = {
    import collection.JavaConversions._
    curator.toSeq.flatMap { c ⇒
      try {
        c.getChildren.forPath(parent).map(child ⇒ parent + "/" + child)
      } catch {
        case noNode: KeeperException.NoNodeException ⇒
          log.info("Parent node {} does not exist", parent)
          Seq[String]()
        case e: Throwable ⇒
          log.warning("Failed to retrieve curator children at {}. Cause: {}", parent, e.getMessage)
          Seq[String]()
      }
    }
  }

  /**
   * Utility method to read child nodes from curator
   */
  protected def zkGetChildNames(parent: String): Seq[String] = {
    import collection.JavaConversions._
    curator.toSeq.flatMap { c ⇒
      try {
        c.getChildren.forPath(parent)
      } catch {
        case noNode: KeeperException.NoNodeException ⇒
          log.debug("Parent node {} does not exist", parent)
          Seq[String]()
        case e: Throwable ⇒
          log.warning("Failed to retrieve curator children at {}. Cause: {}", parent, e.getMessage)
          Seq[String]()
      }
    }
  }

  /**
   * Utility method to write values to curator
   */
  protected def zkPut[T <: AnyRef](path: String, value: T, ephemeral: Boolean = false) {
    val mode = if (ephemeral) CreateMode.EPHEMERAL else CreateMode.PERSISTENT
    val rawData = serialization.serialize(value)
    curator.foreach { c ⇒
      try {
        c.create().creatingParentsIfNeeded().withMode(mode).forPath(path, rawData)
      } catch {
        case e: Throwable ⇒ {
          // NB: Catching a throwable here only is ok because it's thrown again. If not thrown
          // again this will wreak havoc since we would be catching unrecoverable errors like
          // stackoverflow or out-of-memory.
          log.error(e, "Failed to store curator value {} at {}.",
            value, path)
          throw e
        }
      }
    }
  }

  /**
   * Utility method to create/write events to curator
   */
  protected def zkAppendEvent[T <: AnyRef](path: String, value: T, retries: Int) {
    val rawData = serialization.serialize(value)

    curator.foreach { c ⇒
      val result = retry(retries) {
        val newPath = appendEventWithUUID(path)
        c.create().creatingParentsIfNeeded().forPath(newPath, rawData)
        newPath
      }

      result match {
        case Left(ex)                          ⇒ log.error(ex, "Failed retrying {} times to append event to zookeeper value {}", retries, value)
        case Right((finalPath, retriesNeeded)) ⇒ log.debug("append event to zookeeper value {} at {}, retries needed {}", value, finalPath, retriesNeeded)
      }
    }
  }

  /**
   * Utility method to write values to curator
   */
  protected def zkCreateEmptyPath[T <: AnyRef](path: String) {
    curator.foreach { c ⇒
      try {
        c.create().creatingParentsIfNeeded().forPath(path, "".getBytes)
      } catch {
        case e: Throwable ⇒ {
          log.error(e, "Failed to create empty path at {}.", path)
          throw e
        }
      }
    }
  }

  /**
   * Utility method to write values to curator
   */
  protected def zkSet[T <: AnyRef](path: String, value: T, storedVersion: Int) {
    val rawData = serialization.serialize(value)
    curator.foreach { c ⇒
      try {
        c.setData().withVersion(storedVersion).forPath(path, rawData)
      } catch {
        case e: Throwable ⇒ {
          log.error(e, "Failed to store value {} at {} for version {}.",
            value, path, storedVersion)
          throw e
        }
      }
    }
  }

  /**
   * Utility method to delete values in curator
   */
  protected def zkDelete(path: String) {
    curator.foreach { c ⇒
      try {
        c.delete().forPath(path)
      } catch {
        case e: Throwable ⇒ {
          log.error(e, "Failed to delete curator value at {}.", path)
          throw e
        }
      }
    }
  }

  /**
   * Utility method to delete values in curator recursively
   */
  protected def zkDeleteRecursive(path: String) {
    import scala.collection.JavaConversions._
    curator.foreach(client ⇒ {
      client.getChildren.forPath(path).foreach(child ⇒ zkDeleteRecursive(path + "/" + child))
      client.delete().forPath(path)
    })
  }

  /**
   * Utility method to delete values in curator recursively. Does not fail if the path does not exist.
   */
  protected def zkSafeDelete(path: String) {
    curator.foreach(client ⇒ {
      if (client.checkExists().forPath(path) != null)
        zkDeleteRecursive(path)
    })
  }

  /**
   * Utility method to get the stats for a curator item
   */
  protected def zkStat(path: String): Option[Stat] = {
    curator.flatMap(client ⇒ {
      client.checkExists().forPath(path) match {
        case null ⇒ None
        case stat ⇒ Some(stat)
      }
    })
  }
}

object Curator {
  def createCurator(caller: AnyRef, system: ActorSystem, retryPolicy: RetryPolicy = new RetryUntilElapsed(1000 * 60 * 60 * 24 * 3, 5000), zkServers: String = "localhost:2181", formats: Formats = DefaultFormats): Curator = {
    val logger = Logging.getLogger(system, caller)
    createCuratorWithLogger(caller, logger, retryPolicy, zkServers, formats)
  }

  def createCuratorWithLogger(caller: AnyRef, logger: LoggingAdapter, retryPolicy: RetryPolicy = new RetryUntilElapsed(1000 * 60 * 60 * 24 * 3, 5000), zkServers: String = "localhost:2181", extraFormats: Formats = DefaultFormats): Curator = {
    val curator = CuratorFrameworkFactory.newClient(zkServers, retryPolicy)

    curator.start()

    new CuratorToolsImpl(Some(curator), logger, extraFormats) {
      override protected def extendFormats(defaultFormats: Formats) = formats
    }
  }
}

trait Curator {

  def curator: Option[CuratorFramework]
  def serialization: LiftSerialization

  /**
   * Utility method to get the stats for a curator item
   */
  def stat(path: String): Option[Stat]
  def stat(path: Path): Option[Stat]

  /**
   * Utility method to read values from curator
   */
  def get[T <: AnyRef: Manifest](path: String): Option[T]
  def get[T <: AnyRef: Manifest](path: Path): Option[T]

  def get[T <: AnyRef: Manifest](pathList: Seq[Path]): Option[T]
  def get[T <: AnyRef: Manifest](pathList: Seq[Path], default: T): Option[T]
  def get[T <: AnyRef: Manifest](pathList: Seq[Path], defaults: Map[String, Any]): Option[T]

  /**
   * Utility method to read values from curator
   */
  def getVersioned[T <: AnyRef: Manifest](path: String): Option[Versioned[T]]
  def getVersioned[T <: AnyRef: Manifest](path: Path): Option[Versioned[T]]
  def getVersioned[T <: AnyRef: Manifest](pathList: Seq[Path], default: T): Option[Versioned[T]]

  /**
   * Utility method to read child nodes from curator. Returns the full path of each child node.
   */
  def getChildren(parent: String): Seq[String]
  def getChildren(parent: Path): Seq[Path]

  /**
   * Utility method to read child nodes from curator. Returns the local name of each child node.
   */
  def getChildNames(parent: String): Seq[String]
  def getChildNames(parent: Path): Seq[String]

  def createEmptyPath(path: String)
  def createEmptyPath(path: Path)

  /**
   * Utility method to insert new values into curator
   */
  def put[T <: AnyRef](path: String, value: T, ephemeral: Boolean = false)
  def put[T <: AnyRef](path: Path, value: T)

  /**
   * Utility method to create/write events to curator
   */
  def appendEvent[T <: AnyRef](path: String, value: T)
  def appendEvent[T <: AnyRef](path: String, value: T, retries: Int)
  def appendEvent[T <: AnyRef](path: Path, value: T)
  def appendEvent[T <: AnyRef](path: Path, value: T, retries: Int)

  /**
   * Utility method to overwrite values in curator
   */
  def set[T <: AnyRef](path: String, value: T, storedVersion: Int)
  def set[T <: AnyRef](path: Path, value: T, storedVersion: Int)

  /**
   * Utility method to check the existence of nodes in curator
   */
  def exists(path: String): Boolean
  def exists(path: Path): Boolean

  /**
   * Utility method to delete values in curator
   */
  def delete(path: String)
  def delete(path: Path)

  /**
   * Utility method to delete values in curator recursively
   */
  def deleteRecursive(path: String)
  def deleteRecursive(path: Path)

  /**
   * Utility method to delete values in curator recursively. Does not fail if the path does not exist.
   */
  def safeDelete(path: String)
  def safeDelete(path: Path)
}

/**
 * Concrete implementation of the CuratorTools trait.
 * Can be used to compose instead of inherit the tools.
 *
 * @param curator The curator client instance
 * @param log A logging adapter. Use log from ActorLogging or from DirectLogging if not in an actor
 */
class CuratorToolsImpl(val curator: Option[CuratorFramework],
                       protected val log: LoggingAdapter, finalFormats: Formats = DefaultFormats) extends Curator with CuratorTools {

  type AnyMap = Map[String, Any]

  implicit val formats = finalFormats
  override protected def extendFormats(defaultFormats: Formats) = formats

  /**
   * Utility method to get the stats for a curator item
   */
  def stat(path: String): Option[Stat] = super.zkStat(path)
  def stat(path: Path): Option[Stat] = stat(path.toString())

  /**
   * Utility method to read values from curator
   */
  def get[T <: AnyRef: Manifest](path: String) = super.zkGet(path)
  def get[T <: AnyRef: Manifest](path: Path) = get(path.toString())

  /**
   * Utility method to read values from curator
   */
  def getVersioned[T <: AnyRef: Manifest](path: String) = super.zkGetVersioned(path)
  def getVersioned[T <: AnyRef: Manifest](path: Path) = getVersioned(path.toString())

  /**
   * Utility method to read child nodes from curator. Returns the full path of each child node.
   */
  def getChildren(parent: String): Seq[String] = super.zkGetChildren(parent)
  def getChildren(parent: Path): Seq[Path] = getChildren(parent.toString()).map(Path(_))

  /**
   * Utility method to read child nodes from curator. Returns the local name of each child node.
   */
  def getChildNames(parent: String): Seq[String] = super.zkGetChildNames(parent)
  def getChildNames(parent: Path): Seq[String] = getChildNames(parent.toString())

  def createEmptyPath(path: String) { super.zkCreateEmptyPath(path) }
  def createEmptyPath(path: Path) { createEmptyPath(path.toString()) }

  /**
   * Utility method to insert new values into curator
   */
  def put[T <: AnyRef](path: String, value: T, ephemeral: Boolean = false) {
    super.zkPut(path, value, ephemeral)
  }

  def put[T <: AnyRef](path: Path, value: T) {
    put(path.toString(), value)
  }

  /**
   * Utility method to create/write events to curator
   */
  def appendEvent[T <: AnyRef](path: String, value: T, retries: Int) {
    super.zkAppendEvent(path, value, retries)
  }
  def appendEvent[T <: AnyRef](path: String, value: T) {
    super.zkAppendEvent(path, value, 5)
  }

  def appendEvent[T <: AnyRef](path: Path, value: T, retries: Int) {
    appendEvent(path.toString(), value, retries)
  }
  def appendEvent[T <: AnyRef](path: Path, value: T) {
    appendEvent(path.toString(), value, 5)
  }

  /**
   * Utility method to overwrite values in curator
   */
  def set[T <: AnyRef](path: String, value: T, storedVersion: Int) {
    super.zkSet(path, value, storedVersion)
  }

  def set[T <: AnyRef](path: Path, value: T, storedVersion: Int) {
    set(path.toString(), value, storedVersion)
  }

  /**
   * Utility method to check the existence of nodes in curator
   */
  def exists(path: String) = super.zkExists(path)
  def exists(path: Path) = exists(path.toString())

  /**
   * Utility method to delete values in curator
   */
  def delete(path: String) {
    super.zkDelete(path)
  }

  def delete(path: Path) {
    delete(path.toString())
  }

  /**
   * Utility method to delete values in curator recursively
   */
  def deleteRecursive(path: String) {
    super.zkDeleteRecursive(path)
  }

  def deleteRecursive(path: Path) {
    deleteRecursive(path.toString())
  }

  /**
   * Utility method to delete values in curator recursively. Does not fail if the path does not exist.
   */
  def safeDelete(path: String) {
    super.zkSafeDelete(path)
  }

  def safeDelete(path: Path) {
    safeDelete(path.toString())
  }
  /**
   *
   * @param pathList a sequence of paths pointing to serialized (case) classes in ZooKeeper
   * @tparam T class of the data the caller wants to retrieve from ZooKeeper
   * @return Merged information from ZooKeeper
   *         For each path in the pathList, retrieve the info in ZooKeeper as Option[Map[String, Any]]
   *         Merge the maps we got from ZooKeeper
   *         if result is empty map return None
   *         else convert map to the type requested and return Some[T]
   */
  def get[T <: AnyRef: Manifest](pathList: Seq[Path]): Option[T] = {
    if (pathList == Nil)
      None
    else
      mapToSomeObject[T](mergeMaps(getMaps(pathList)))
  }

  def getVersioned[T <: AnyRef: Manifest](pathList: Seq[Path], default: T): Option[Versioned[T]] = {
    val stats = new Stat
    val defaultMap = objectToMap[T](default)
    val result = mapToSomeObject[T](mergeMaps(Option(defaultMap) +: getMaps(pathList, versioned = true, Some(stats))))
    result match {
      case Some(t) ⇒ log.debug("loaded from {} version {}: {}", pathList, stats.getVersion, t.toString)
      case None    ⇒ log.debug("could not load value from {}", pathList)
    }
    result.map(t ⇒ Versioned(t, stats.getVersion))
  }

  /**
   *
   * @param pathList a sequence of paths pointing to serialized (case) classes in ZooKeeper
   * @param default default value to be merged with values retrieved from ZooKeeper
   * @tparam T class of the data the caller wants to retrieve from ZooKeeper
   * @return case class constructed from the merged values
   */
  def get[T <: AnyRef: Manifest](pathList: Seq[Path], default: T): Option[T] = {
    val defaultMap = objectToMap[T](default)
    mapToSomeObject[T](mergeMaps(Option(defaultMap) +: getMaps(pathList)))
  }

  def get[T <: AnyRef: Manifest](pathList: Seq[Path], defaults: Map[String, Any]): Option[T] = {
    mapToSomeObject[T](mergeMaps(Option(defaults) +: getMaps(pathList)))
  }

  def getSomeMap(path: Path, versioned: Boolean, stats: Option[Stat] = None): Option[AnyMap] = {
    try {
      val rawData = super.zkGetRawData(path.toString, versioned, stats)

      rawData match {
        case None ⇒ None
        case Some(bytes) ⇒
          if (bytes.size > 0) {
            val parseResult = parse(new String(bytes))
            val map = parseResult.values.asInstanceOf[AnyMap]
            Some(map)
          } else {
            None
          }
      }
    } catch {
      case e: Exception ⇒ None
    }
  }

  private def getMaps(pathList: Seq[Path], versioned: Boolean = false, stats: Option[Stat] = None): Seq[Option[AnyMap]] = {
    pathList.map(path ⇒ getSomeMap(path, versioned, stats))
  }

  private def objectToMap[T <: AnyRef: Manifest](obj: T): AnyMap = {
    val defaultData: Array[Byte] = serialization.serialize(obj)
    parse(new String(defaultData)).values.asInstanceOf[AnyMap]
  }

  private def mapToSomeObject[T <: AnyRef: Manifest](aMap: AnyMap): Option[T] = {
    if (aMap.isEmpty)
      None
    else {
      val data = serialization.serialize(aMap)
      val parsedData = parse(new String(data))
      try {
        val tmp: T = parsedData.extract[T]
        Some(tmp)
      } catch {
        case e: Exception ⇒ None
      }
    }
  }

  private def mergeMaps(maps: Seq[Option[AnyMap]]): AnyMap = {
    maps.flatten.foldLeft[AnyMap](Map[String, Any]()) {
      (accu: AnyMap, elem: AnyMap) ⇒ accu ++ elem
    }
  }

}

case class Versioned[T <: AnyRef: Manifest](data: T, version: Int)
