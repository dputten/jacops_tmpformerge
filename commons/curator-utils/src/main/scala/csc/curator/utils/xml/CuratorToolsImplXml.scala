package csc.curator.utils.xml

import org.apache.curator.framework.CuratorFramework
import csc.curator.utils.{ CuratorSerialization, Versioned, Curator }
import scala.xml.{ NodeSeq, XML }
import org.apache.zookeeper.data.Stat
import csc.config.Path
import csc.json.lift.LiftSerialization
import scala.Exception
import net.liftweb.json._
import org.apache.log4j.Logger
import scala.Left
import csc.curator.utils.Versioned
import scala.Right
import scala.Some
import akka.event.LoggingAdapter

class XmlReader(filePathAndName: String, protected val log: LoggingAdapter) {

  private def path = "path"
  private def data = "data"
  private def backslash = "/"

  private def getRootNode(nodePath: String): NodeSeq = {
    val root = XML.loadFile(filePathAndName)
    root \\ "export" \\ "znodes" \ "znode"
  }

  def getElement(nodePath: String): Either[Exception, Option[String]] = {
    try {
      val node = getRootNode(filePathAndName).filter(znode ⇒ (znode \ "path").text == nodePath).toList
      node match {
        case Nil ⇒ {
          log.debug("Found no element with path:%s".format(nodePath))
          Right(None)
        }
        case head :: Nil ⇒ {
          val dataNode = head.map(znode ⇒ (znode \ data) text)
          dataNode.size match {
            case 0 ⇒ {
              Left(new Exception("Node %s is missing the data tag. The result could not be determined.".format(nodePath)))
            }
            case 1 ⇒ {
              log.debug("Found empty element with path:%s".format(nodePath))
              Right(Some(dataNode.head))
            }
            case other ⇒ {
              Left(new Exception("Node %s has multiple data tags. The result could not be determined.".format(nodePath)))
            }
          }
        }
        case _ ⇒ Left(new Exception("There are multiple result found for the unique key. The result could not be determined."))
      }
    } catch {
      case e: Exception ⇒ Left(e)
    }
  }

  def getDirectChildElements(nodes: Seq[String], parentPath: String): Seq[String] =
    {
      // Remove all children that have more than 1 node.
      nodes.filter(x ⇒ x.split(backslash).length == parentPath.split(backslash).length + 1)
    }

  def getDirectChildElements(nodePath: String): Either[Exception, Seq[String]] = {
    try {
      // Get all the nodes that are starting with the nodePath(the parent)
      val result = getRootNode(filePathAndName).filter(znode ⇒ (znode \ path).text.startsWith(nodePath)).map(znode ⇒ (znode \ path) text)

      Right(getDirectChildElements(result, nodePath))
    } catch {
      case e: Exception ⇒ Left(e)
    }
  }
}

class CuratorToolsImplXml(xmlReader: XmlReader, protected val log: LoggingAdapter, finalFormats: Formats = DefaultFormats) extends Curator with CuratorSerialization {
  implicit val formats = finalFormats
  override protected def extendFormats(defaultFormats: Formats) = formats
  type AnyMap = Map[String, Any]
  def curator: Option[CuratorFramework] = None
  val charsetName = "UTF8"

  /**
   * Utility method to get the stats for a curator item
   */
  def stat(path: String): Option[Stat] = throw new UnsupportedOperationException
  def stat(path: Path): Option[Stat] = throw new UnsupportedOperationException

  /**
   * Utility method to read values from curator
   */
  def get[T <: AnyRef: Manifest](path: String) =
    {
      val result: Either[Exception, Option[String]] = xmlReader.getElement(path)
      result match {
        case Right(z) ⇒
          z match {
            case Some(y) ⇒ {
              if (y.trim == "") {
                log.debug("Found empty value with path:%s".format(path))
                None
              } else {
                log.debug("Found value with path:%s and result:%s".format(path, y))
                Some(serialization.deserialize[T](y.getBytes(charsetName)))
              }
            }
            case None ⇒
              log.debug("Found no result with path:%s".format(path))
              None
          }
        case Left(e) ⇒
          log.error("An error occured with path:%s with exception:%s".format(path, e))
          throw e
      }
    }

  def get[T <: AnyRef: Manifest](path: Path) = {
    get(path.toString)
  }

  /**
   * Utility method to read values from curator
   */
  def getVersioned[T <: AnyRef: Manifest](path: String) = throw new UnsupportedOperationException
  def getVersioned[T <: AnyRef: Manifest](path: Path) = throw new UnsupportedOperationException

  /**
   * Utility method to read child nodes from curator. Returns the full path of each child node.
   * Only return the direct childs nodes.
   */
  def getChildren(parent: String): Seq[String] = {
    val result: Either[Exception, Seq[String]] = xmlReader.getDirectChildElements(parent)
    result match {
      case Right(children) ⇒
        log.debug("Found value with path:%s and result:%s".format(parent, children))
        children
      case Left(exception) ⇒
        log.debug("An error occured with path:%s with exception:%s".format(parent, exception))
        throw exception
    }
  }

  def getChildren(parent: Path): Seq[Path] = {
    getChildren(parent.toString()).map(Path(_))
  }

  /**
   * Utility method to read child nodes from curator. Returns the local name of each child node.
   * alleen de naam van de child
   */
  def getChildNames(parent: String): Seq[String] = {
    getChildren(parent).map(i ⇒ i.split("/").last)
  }

  def getChildNames(parent: Path): Seq[String] = {
    getChildNames(parent.toString)
  }

  def createEmptyPath(path: String) { throw new UnsupportedOperationException }
  def createEmptyPath(path: Path) { throw new UnsupportedOperationException }

  /**
   * Utility method to insert new values into curator
   */
  def put[T <: AnyRef](path: String, value: T, ephemeral: Boolean = false) {
    throw new UnsupportedOperationException
  }

  def put[T <: AnyRef](path: Path, value: T) {
    throw new UnsupportedOperationException
  }

  /**
   * Utility method to create/write events to curator
   */
  def appendEvent[T <: AnyRef](path: String, value: T) {
    throw new UnsupportedOperationException
  }
  def appendEvent[T <: AnyRef](path: String, value: T, retries: Int) {
    throw new UnsupportedOperationException
  }
  def appendEvent[T <: AnyRef](path: Path, value: T) {
    throw new UnsupportedOperationException
  }
  def appendEvent[T <: AnyRef](path: Path, value: T, retries: Int) {
    throw new UnsupportedOperationException
  }

  /**
   * Utility method to overwrite values in curator
   */
  def set[T <: AnyRef](path: String, value: T, storedVersion: Int) {
    throw new UnsupportedOperationException
  }

  def set[T <: AnyRef](path: Path, value: T, storedVersion: Int) {
    throw new UnsupportedOperationException
  }

  /**
   * Utility method to check the existence of nodes in curator
   */
  def exists(path: String) = {
    val result: Either[Exception, Option[String]] = xmlReader.getElement(path)
    result match {
      case Right(z) ⇒
        z match {
          case Some(y) ⇒
            true
          case None ⇒
            false
        }
      case Left(e) ⇒
        log.debug("An error occured with path:%s with exception:%s".format(path, e))
        throw e
    }
  }
  def exists(path: Path) = { exists(path.toString) }

  /**
   * Utility method to delete values in curator
   */
  def delete(path: String) {
    throw new UnsupportedOperationException
  }

  def delete(path: Path) {
    throw new UnsupportedOperationException
  }

  /**
   * Utility method to delete values in curator recursively
   */
  def deleteRecursive(path: String) {
    throw new UnsupportedOperationException
  }

  def deleteRecursive(path: Path) {
    throw new UnsupportedOperationException
  }

  /**
   * Utility method to delete values in curator recursively. Does not fail if the path does not exist.
   */
  def safeDelete(path: String) {
    throw new UnsupportedOperationException
  }

  def safeDelete(path: Path) {
    throw new UnsupportedOperationException
  }
  /**
   *
   * @param pathList a sequence of paths pointing to serialized (case) classes in ZooKeeper
   * @tparam T class of the data the caller wants to retrieve from ZooKeeper
   * @return Merged information from ZooKeeper
   *         For each path in the pathList, retrieve the info in ZooKeeper as Option[Map[String, Any]
   *         Merge the maps we got from ZooKeeper
   *         if result is empty map return None
   *         else convert map to the type requested and return Some[T]
   */
  def get[T <: AnyRef: Manifest](pathList: Seq[Path]): Option[T] = {
    if (pathList == Nil)
      None
    else
      mapToSomeObject[T](mergeMaps(getMaps(pathList)))
  }

  def getVersioned[T <: AnyRef: Manifest](pathList: Seq[Path], default: T): Option[Versioned[T]] = {
    throw new UnsupportedOperationException
  }

  /**
   *
   * @param pathList a sequence of paths pointing to serialized (case) classes in ZooKeeper
   * @param default default value to be merged with values retrieved from ZooKeeper
   * @tparam T class of the data the caller wants to retrieve from ZooKeeper
   * @return case class constructed from the merged values
   */
  def get[T <: AnyRef: Manifest](pathList: Seq[Path], default: T): Option[T] = {
    val defaultMap = objectToMap[T](default)
    mapToSomeObject[T](mergeMaps(Option(defaultMap) +: getMaps(pathList)))
  }

  def get[T <: AnyRef: Manifest](pathList: Seq[Path], defaults: Map[String, Any]): Option[T] = {
    mapToSomeObject[T](mergeMaps(Option(defaults) +: getMaps(pathList)))
  }

  def getSomeMap(path: Path, versioned: Boolean, stats: Option[Stat] = None): Option[AnyMap] = {
    try {
      val result: Either[Exception, Option[String]] = xmlReader.getElement(path.toString)
      result match {
        case Right(z) ⇒
          z match {
            case Some(y) ⇒
              log.debug("Found value with path:%s and result:%s".format(path, y))
              val bytes = y.getBytes(charsetName)
              if (bytes.size > 0) {
                val parseResult = parse(new String(bytes))
                val map = parseResult.values.asInstanceOf[AnyMap]
                Some(map)
              } else {
                None
              }
            case None ⇒
              log.debug("Found no result with path:%s".format(path))
              None
          }
        case Left(e) ⇒
          log.debug("An error occured with path:%s with exception:%".format(path, e))
          None
      }
    } catch {
      case e: Exception ⇒ None
    }
  }

  private def getMaps(pathList: Seq[Path], versioned: Boolean = false, stats: Option[Stat] = None): Seq[Option[AnyMap]] = {
    pathList.map(path ⇒ getSomeMap(path, versioned, stats))
  }

  private def objectToMap[T <: AnyRef: Manifest](obj: T): AnyMap = {
    val defaultData: Array[Byte] = serialization.serialize(obj)
    parse(new String(defaultData)).values.asInstanceOf[AnyMap]
  }

  private def mapToSomeObject[T <: AnyRef: Manifest](aMap: AnyMap): Option[T] = {
    if (aMap.isEmpty)
      None
    else {
      val data = serialization.serialize(aMap)
      val parsedData = parse(new String(data))
      try {
        val tmp: T = parsedData.extract[T]
        Some(tmp)
      } catch {
        case e: Exception ⇒ None
      }
    }
  }

  private def mergeMaps(maps: Seq[Option[AnyMap]]): AnyMap = {
    maps.flatten.foldLeft[AnyMap](Map[String, Any]()) {
      (accu: AnyMap, elem: AnyMap) ⇒ accu ++ elem
    }
  }
}
