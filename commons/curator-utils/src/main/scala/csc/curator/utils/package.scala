package csc.curator

package object utils {
  type RetriesNeeded = Int

  /**
   * Retry function that keeps calling a given function
   * a max number of times until no exception is returned
   * @param retries number of retries, default 5
   * @param body function to retry
   * @param T result value of given function
   * @return Either of Exception or T
   */
  def retry[T](retries: Int)(body: ⇒ T): Either[Exception, (T, RetriesNeeded)] = {

    def retryInternal(retriesToDo: Int): Either[Exception, (T, RetriesNeeded)] = {
      try {
        val retriesNeeded = retries - retriesToDo
        Right((body, retriesNeeded))
      } catch {
        case e: Exception ⇒
          if (retriesToDo > 0) retryInternal(retriesToDo - 1)
          else Left(e)
      }
    }

    retryInternal(retries)
  }

  /**
   * *
   * makes the path unique by adding a uuid to the path
   * @param path given path
   * @return unique path
   */
  def appendEventWithUUID(path: String) = path + java.util.UUID.randomUUID().toString.replace("-", "")
}