/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.curator.utils

import akka.actor.ActorRef
import org.apache.curator.framework.recipes.cache.{ PathChildrenCacheEvent, PathChildrenCacheListener, PathChildrenCache }
import java.util.concurrent.atomic.AtomicReference
import csc.config.Path
import org.apache.curator.framework.CuratorFramework

import scala.collection.JavaConversions._

/**
 * A child node was added
 * @param nodeName the child node that was added
 */
case class PathAdded(nodeName: String, parent: Path)

/**
 * A child node was removed
 * @param nodeName the child node that was removed
 */
case class PathRemoved(nodeName: String, parent: Path)

/**
 * This class watches a curator node. When it is created it sends
 * @param client the curator curator
 * @param parentPath  The parent path of the system nodes
 * @param watchActor the watch actor
 */
class WatchNode(client: CuratorFramework, parentPath: String, watchActor: ActorRef) extends PathChildrenCacheListener {

  private val cache = new AtomicReference[Option[PathChildrenCache]](None)
  //Path can't end with a / because otherwise the calls start and get children will hang
  private val path = if (parentPath.charAt(parentPath.size - 1) == '/') {
    parentPath.substring(0, parentPath.size - 1)
  } else {
    parentPath
  }

  /**
   * Starts the watch for changes on the systems
   */
  def start() {
    if (cache.compareAndSet(None, Some(new PathChildrenCache(client, path, false)))) {
      cache.get.foreach(pathCache ⇒ {
        pathCache.start()
        //add listener
        pathCache.getListenable().addListener(this)
        //add all known systems
        val childrenQuery = client.getChildren()
        val children = childrenQuery.forPath(path)
        for (nodeName ← children) {
          watchActor ! new PathAdded(nodeName, Path(path))
        }
      })
    }
  }

  /**
   * Stops the watch for changes on the systems
   */
  def stop() {
    cache.getAndSet(None).foreach(pathCache ⇒ {
      pathCache.getListenable.removeListener(this)
      pathCache.close()
    })
  }

  /**
   * One of the children has been changed
   * @param client the CuratorFramework
   * @param event the indication what has changed
   */
  def childEvent(client: CuratorFramework, event: PathChildrenCacheEvent) {
    if (event.getType == PathChildrenCacheEvent.Type.CHILD_ADDED) {
      val path = Path(event.getData.getPath)
      watchActor ! new PathAdded(path.nodes.last.name, path)
    }
    if (event.getType == PathChildrenCacheEvent.Type.CHILD_REMOVED) {
      val path = Path(event.getData.getPath)
      watchActor ! new PathRemoved(path.nodes.last.name, path)
    }
  }
}