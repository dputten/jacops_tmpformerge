package csc.curator.utils

import csc.json.lift.ByteArraySerialization
import net.liftweb.json._
import scala.Some

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 2/24/14.
 */

trait MultiLevelSettings extends ByteArraySerialization {

  type AnyMap = Map[String, Any]

  implicit def formats = extendFormats(DefaultFormats)

  protected def extendFormats(defaultFormats: Formats): Formats = defaultFormats

  def objectToMap[T <: AnyRef: Manifest](obj: T): AnyMap = {
    val defaultData: Array[Byte] = serialize(obj)
    parse(new String(defaultData)).values.asInstanceOf[AnyMap]
  }

  def mapToSomeObject[T <: AnyRef: Manifest](aMap: AnyMap): Option[T] = {
    if (aMap.isEmpty)
      None
    else {
      val data = serialize(aMap)
      val parsedData = parse(new String(data))
      try {
        val tmp: T = parsedData.extract[T]
        Some(tmp)
      } catch {
        case e: Exception ⇒ None
      }
    }
  }

  def mergeMaps(maps: Seq[Option[AnyMap]]): AnyMap = {
    maps.flatten.reduceLeft[AnyMap] {
      (accu: AnyMap, elem: AnyMap) ⇒ accu ++ elem
    }
  }
}
