#! /bin/sh

# input variables:
# $publish (optional)
# $ivy_home
# $sbt_home
# $JAVA_HOME
# $WORKSPACE

if [ "$publish" = "true" ] ;then
  publish='publish'
else
  publish='publish-local'
fi

rm -rf $ivy_home/cache/com.csc.sbt
rm -rf $ivy_home/local/com.csc.sbt

cd sbt-cts-base
../runSbt clean compile publish-local 

cd ../commons
../runSbt clean compile test:compile publish-local

cd ../cts_sensor
rm *.tar.gz
../runSbt clean compile dist $publish -Djacops-only

