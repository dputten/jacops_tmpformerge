#! /bin/sh

# input variables:
# $JOB_NAME
# $WORKSPACE

# turn off proxy
export http_proxy=

# Set java home
export JAVA_HOME=/home/jenkins/git/jdk1.6.0_21

# set sbt_home to right directory (that is to the repos you just cloned)
base='/home/jenkins/git'
export sbt_home=${base}/sbt_0.13.8/sbt
export ivy_home="/home/jenkins/.ivy2/${JOB_NAME}"

./buildAll.sh

